
-- FIND Record_Activity ROWS WITH INVALID Record_ID'S
SELECT
	*
FROM
	Record_Activity RA
LEFT JOIN
	Record R
	ON R.Record_ID = RA.Record_ID
WHERE
	R.Record_ID IS NULL
	AND RA.Record_ID IS NOT NULL;
	
-- FIND Record_Activity ROWS WITH INVALID User_ID'S
SELECT
	*
FROM
	Record_Activity RA
LEFT JOIN
	[User] U
	ON U.[User_ID] = RA.[User_ID]
WHERE
	U.[User_ID] IS NULL
	AND RA.[User_ID] IS NOT NULL;

--FIND Record_Activity ROWS WITH INVALID Plan_Transaction_ID
SELECT
	*
FROM
	Record_Activity RA
LEFT JOIN
	Plan_Transaction PT
	ON RA.Plan_Transaction_ID = PT.Plan_Transaction_ID
WHERE
	PT.Plan_Transaction_ID IS NULL
	AND RA.Plan_Transaction_ID IS NOT NULL;
	
-- FIND Plan ROWS WITH INVALID Carrier_ID'S
SELECT
	*
FROM
	[Plan] P
LEFT JOIN
	Carrier C
	ON P.Carrier_ID = C.Carrier_ID
WHERE
	C.Carrier_ID IS NULL
	AND P.Carrier_ID IS NOT NULL;

-- FIND Complaint_Activity ROWS WITH INVALID Complaint_ID
SELECT
	*
FROM
	Complaint_Activity CA
LEFT JOIN
	Complaint C
	ON C.Complaint_ID = CA.Complaint_ID
WHERE
	C.Complaint_ID IS NULL
	AND CA.Complaint_ID IS NOT NULL;
	
-- FIND Complaint_Activity ROWS WITH INVALID Record_Activity_ID
SELECT
	*
FROM
	Complaint_Activity CA
LEFT JOIN
	Record_Activity RA
	ON RA.Record_Activity_ID = CA.Record_Activity_ID
WHERE
	RA.Record_Activity_ID IS NULL
	AND CA.Record_Activity_ID IS NOT NULL;
	
--FIND Reconciliation ROWS WITH INVALID Record_Activity_ID
SELECT
	*
FROM
	Reconciliation R
LEFT JOIN
	Record_Activity RA
	ON RA.Record_Activity_ID = R.Record_Activity_ID
WHERE
	RA.Record_Activity_ID IS NULL
	AND R.Record_Activity_ID IS NOT NULL;

--FIND Plan_Transaction ROWS WITH INVALID Plan_ID

SELECT
	*
FROM
	Plan_Transaction PT
LEFT JOIN
	[Plan] P
	ON PT.Plan_ID = P.Plan_ID
WHERE
	P.Plan_ID IS NULL
	AND PT.Plan_ID IS NOT NULL;
	
SELECT
	*
FROM
	[Plan] P
LEFT JOIN
	Carrier C
	ON C.Carrier_ID = P.Carrier_ID
WHERE
	C.Carrier_ID IS NULL
	AND P.Carrier_ID IS NOT NULL;

SELECT
	*
FROM
	Agent A
LEFT JOIN
	[User] U
	ON U.[User_ID] = A.[User_ID]
WHERE
	U.[User_ID] IS NULL
	AND A.[User_ID] IS NOT NULL;
	
SELECT
	*
FROM
	Record_Activity RA
LEFT JOIN
	Auto_Email AE
	ON AE.Auto_Email_ID = RA.Auto_Email_ID
WHERE
	AE.Auto_Email_ID IS NULL
	AND RA.Auto_Email_ID IS NOT NULL;
	
SELECT
	*
FROM
	CallRating CR
LEFT JOIN
	Record_Activity RA
	ON RA.Record_Activity_ID = CR.Record_Activity_ID
WHERE
	RA.Record_Activity_ID IS NULL
	AND CR.Record_Activity_ID IS NOT NULL;
	
SELECT
	*
FROM
	Record_Activity RA
LEFT JOIN
	Campaign_Code CC
	ON CC.Campaign_Code_ID = RA.Campaign_Code_ID
WHERE
	CC.Campaign_Code_ID IS NULL
	AND RA.Campaign_Code_ID IS NOT NULL;
	
SELECT
	*
FROM
	Record_Activity RA
LEFT JOIN
	Record_Activity_Note RAN
	ON RAN.Record_Activity_Note_ID = RA.Record_Activity_Note_ID
WHERE
	RAN.Record_Activity_Note_ID IS NULL
	AND RA.Record_Activity_Note_ID IS NOT NULL;

SELECT
	*
FROM
	Record_Activity RA
LEFT JOIN
	Record_Activity_Status RAS
	ON RA.Record_Activity_Status_ID = RAS.Record_Activity_Status_ID
WHERE
	RAS.Record_Activity_Status_ID IS NULL
	AND RA.Record_Activity_Status_ID IS NOT NULL;
	
SELECT
	*
FROM
	Record R
LEFT JOIN
	[State] S
	ON S.State_ID = R.State_ID
WHERE
	S.State_ID IS NULL
	AND R.State_ID IS NOT NULL;
	
SELECT
	*
FROM
	Record R
LEFT JOIN
	[State] S
	ON S.State_ID = R.Alt_State_ID
WHERE
	S.State_ID IS NULL
	AND R.Alt_State_ID IS NOT NULL;

SELECT
	*
FROM
	Record R
LEFT JOIN
	Employment_Status ES
	ON ES.Employment_Status_ID = R.Employment_Status_ID
WHERE
	ES.Employment_Status_ID IS NULL
	AND R.Employment_Status_ID IS NOT NULL;
	
SELECT
	*
FROM
	Record R
LEFT JOIN
	Source_Code SC
	ON SC.Source_Code_ID = R.Source_Code_ID
WHERE
	SC.Source_Code_ID IS NULL
	AND R.Source_Code_ID IS NOT NULL;
	
SELECT
	R.Marital_Status_ID
FROM
	Record R
LEFT JOIN
	Marital_Status MS
	ON MS.Marital_Status_ID = R.Marital_Status_ID
WHERE
	MS.Marital_Status_ID IS NULL
	AND R.Marital_Status_ID IS NOT NULL;

SELECT
	*
FROM
	Record R
LEFT JOIN
	Record_Type RT
	ON RT.Record_Type_ID = R.Record_Type_ID
WHERE
	RT.Record_Type_ID IS NULL
	AND R.Record_Type_ID IS NOT NULL;
	
SELECT
	*
FROM
	User_Security_Level USL
LEFT JOIN
	[USER] U
	ON USL.[User_ID] = U.[User_ID]
WHERE
	U.[User_ID] IS NULL
	AND USL.[User_ID] IS NOT NULL;

SELECT
	R.Record_Current_Situation_ID
	--*
FROM
	Record R
LEFT JOIN
	Record_Current_Situation RCS
	ON RCS.Record_Current_Situation_ID = R.Record_Current_Situation_ID
WHERE
	RCS.Record_Current_Situation_ID IS NULL
	AND R.Record_Current_Situation_ID IS NOT NULL;