USE [LMS]
GO
/****** Object:  Table [dbo].[TypeofWealth]    Script Date: 08/12/2014 09:35:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TypeofWealth](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Description] [text] NULL,
 CONSTRAINT [PK_TypeofWealth] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TypeofRisk]    Script Date: 08/12/2014 09:35:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TypeofRisk](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Description] [text] NULL,
 CONSTRAINT [PK_TypeofRisk] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TypeofProviderPreference]    Script Date: 08/12/2014 09:35:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TypeofProviderPreference](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Description] [text] NULL,
 CONSTRAINT [PK_TypeofProviderPreference] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TypeofNote]    Script Date: 08/12/2014 09:35:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TypeofNote](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Description] [text] NULL,
 CONSTRAINT [PK_TypeofNote] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TypeofHealth]    Script Date: 08/12/2014 09:35:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TypeofHealth](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Description] [text] NULL,
 CONSTRAINT [PK_TypeofHealth] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TypeofDecision]    Script Date: 08/12/2014 09:35:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TypeofDecision](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Description] [text] NULL,
 CONSTRAINT [PK_TypeofDecision] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TypeofCostSensitivity]    Script Date: 08/12/2014 09:35:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TypeofCostSensitivity](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Description] [text] NULL,
 CONSTRAINT [PK_TypeofCostSensitivity] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Note]    Script Date: 08/12/2014 09:35:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Note](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TypeofNote] [int] NOT NULL,
	[NoteText] [text] NULL,
 CONSTRAINT [PK_Note] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CustomerProfile]    Script Date: 08/12/2014 09:35:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerProfile](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RecordId] [int] NOT NULL,
	[TypeofHealthId] [int] NULL,
	[TypeofWealthId] [int] NULL,
	[TypeofDecisionId] [int] NULL,
	[TypeofRiskId] [int] NULL,
	[TypeofCostSensitivityId] [int] NULL,
	[TypeofProviderPreferenceId] [int] NULL,
	[TravelerCommentsNoteId] [int] NULL,
	[BrandPreferenceCommentsNoteId] [int] NULL,
	[FutureHealthConcernsCommentsNoteId] [int] NULL,
 CONSTRAINT [PK_CustomerProfile] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  ForeignKey [FK_CustomerProfile_Note]    Script Date: 08/12/2014 09:35:36 ******/
ALTER TABLE [dbo].[CustomerProfile]  WITH CHECK ADD  CONSTRAINT [FK_CustomerProfile_Note] FOREIGN KEY([BrandPreferenceCommentsNoteId])
REFERENCES [dbo].[Note] ([Id])
GO
ALTER TABLE [dbo].[CustomerProfile] CHECK CONSTRAINT [FK_CustomerProfile_Note]
GO
/****** Object:  ForeignKey [FK_CustomerProfile_Note1]    Script Date: 08/12/2014 09:35:36 ******/
ALTER TABLE [dbo].[CustomerProfile]  WITH CHECK ADD  CONSTRAINT [FK_CustomerProfile_Note1] FOREIGN KEY([FutureHealthConcernsCommentsNoteId])
REFERENCES [dbo].[Note] ([Id])
GO
ALTER TABLE [dbo].[CustomerProfile] CHECK CONSTRAINT [FK_CustomerProfile_Note1]
GO
/****** Object:  ForeignKey [FK_CustomerProfile_Record]    Script Date: 08/12/2014 09:35:36 ******/
ALTER TABLE [dbo].[CustomerProfile]  WITH CHECK ADD  CONSTRAINT [FK_CustomerProfile_Record] FOREIGN KEY([TravelerCommentsNoteId])
REFERENCES [dbo].[TypeofNote] ([Id])
GO
ALTER TABLE [dbo].[CustomerProfile] CHECK CONSTRAINT [FK_CustomerProfile_Record]
GO
/****** Object:  ForeignKey [FK_CustomerProfile_TypeofCostSensitivity]    Script Date: 08/12/2014 09:35:36 ******/
ALTER TABLE [dbo].[CustomerProfile]  WITH CHECK ADD  CONSTRAINT [FK_CustomerProfile_TypeofCostSensitivity] FOREIGN KEY([TypeofCostSensitivityId])
REFERENCES [dbo].[TypeofCostSensitivity] ([Id])
GO
ALTER TABLE [dbo].[CustomerProfile] CHECK CONSTRAINT [FK_CustomerProfile_TypeofCostSensitivity]
GO
/****** Object:  ForeignKey [FK_CustomerProfile_TypeofDecision]    Script Date: 08/12/2014 09:35:36 ******/
ALTER TABLE [dbo].[CustomerProfile]  WITH CHECK ADD  CONSTRAINT [FK_CustomerProfile_TypeofDecision] FOREIGN KEY([TypeofDecisionId])
REFERENCES [dbo].[TypeofDecision] ([Id])
GO
ALTER TABLE [dbo].[CustomerProfile] CHECK CONSTRAINT [FK_CustomerProfile_TypeofDecision]
GO
/****** Object:  ForeignKey [FK_CustomerProfile_TypeofHealth]    Script Date: 08/12/2014 09:35:36 ******/
ALTER TABLE [dbo].[CustomerProfile]  WITH CHECK ADD  CONSTRAINT [FK_CustomerProfile_TypeofHealth] FOREIGN KEY([TypeofHealthId])
REFERENCES [dbo].[TypeofHealth] ([Id])
GO
ALTER TABLE [dbo].[CustomerProfile] CHECK CONSTRAINT [FK_CustomerProfile_TypeofHealth]
GO
/****** Object:  ForeignKey [FK_CustomerProfile_TypeofProviderPreference]    Script Date: 08/12/2014 09:35:36 ******/
ALTER TABLE [dbo].[CustomerProfile]  WITH CHECK ADD  CONSTRAINT [FK_CustomerProfile_TypeofProviderPreference] FOREIGN KEY([TypeofProviderPreferenceId])
REFERENCES [dbo].[TypeofProviderPreference] ([Id])
GO
ALTER TABLE [dbo].[CustomerProfile] CHECK CONSTRAINT [FK_CustomerProfile_TypeofProviderPreference]
GO
/****** Object:  ForeignKey [FK_CustomerProfile_TypeofRisk]    Script Date: 08/12/2014 09:35:36 ******/
ALTER TABLE [dbo].[CustomerProfile]  WITH CHECK ADD  CONSTRAINT [FK_CustomerProfile_TypeofRisk] FOREIGN KEY([TypeofRiskId])
REFERENCES [dbo].[TypeofRisk] ([Id])
GO
ALTER TABLE [dbo].[CustomerProfile] CHECK CONSTRAINT [FK_CustomerProfile_TypeofRisk]
GO
/****** Object:  ForeignKey [FK_CustomerProfile_TypeofWealth]    Script Date: 08/12/2014 09:35:36 ******/
ALTER TABLE [dbo].[CustomerProfile]  WITH CHECK ADD  CONSTRAINT [FK_CustomerProfile_TypeofWealth] FOREIGN KEY([TypeofWealthId])
REFERENCES [dbo].[TypeofWealth] ([Id])
GO
ALTER TABLE [dbo].[CustomerProfile] CHECK CONSTRAINT [FK_CustomerProfile_TypeofWealth]
GO

-- INSERT LOOKUP TABLE VALUES
SET IDENTITY_INSERT TypeofHealth ON
INSERT INTO TypeofHealth (Id, Name, Description) VALUES (5, 'Perfect', 'Perfect');
INSERT INTO TypeofHealth (Id, Name, Description) VALUES (4, 'VeryGood', 'Very Good');
INSERT INTO TypeofHealth (Id, Name, Description) VALUES (3, 'Good', 'Good');
INSERT INTO TypeofHealth (Id, Name, Description) VALUES (2, 'Fair', 'Fair');
INSERT INTO TypeofHealth (Id, Name, Description) VALUES (1, 'Poor', 'Poor');
SET IDENTITY_INSERT TypeofHealth OFF
SET IDENTITY_INSERT TypeofWealth ON
INSERT INTO TypeofWealth (Id, Name, Description) VALUES (5, 'Wealthy', 'Wealthy');
INSERT INTO TypeofWealth (Id, Name, Description) VALUES (4, 'WellOff', 'Well Off');
INSERT INTO TypeofWealth (Id, Name, Description) VALUES (3, 'Comfortable', 'Comfortable');
INSERT INTO TypeofWealth (Id, Name, Description) VALUES (2, 'GettingBy', 'Getting By');
INSERT INTO TypeofWealth (Id, Name, Description) VALUES (1, 'PovertyLevel', 'Poverty Level');
SET IDENTITY_INSERT TypeofWealth OFF
SET IDENTITY_INSERT TypeofDecision ON
INSERT INTO TypeofDecision (Id, Name, Description) VALUES (5, 'JusttheFactsIllDecide', 'Just the Facts; I''ll Decide');
INSERT INTO TypeofDecision (Id, Name, Description) VALUES (4, 'MoreDataLessVisual', 'More Data; Less Visual');
INSERT INTO TypeofDecision (Id, Name, Description) VALUES (3, 'VisualandData', 'Visual and Data');
INSERT INTO TypeofDecision (Id, Name, Description) VALUES (2, 'MoreVisualLessData', 'More Visual; Less Data');
INSERT INTO TypeofDecision (Id, Name, Description) VALUES (1, 'TellMeWhatToDo', 'Tell Me What To Do');
SET IDENTITY_INSERT TypeofDecision OFF
SET IDENTITY_INSERT TypeofRisk ON
INSERT INTO TypeofRisk (Id, Name, Description) VALUES (5, 'NoOutofPocket', 'No Out of Pocket');
INSERT INTO TypeofRisk (Id, Name, Description) VALUES (4, 'LowOutofPocket', 'Low Out of Pocket');
INSERT INTO TypeofRisk (Id, Name, Description) VALUES (3, 'ModerateOutofPocket', 'Moderate Out of Pocket');
INSERT INTO TypeofRisk (Id, Name, Description) VALUES (2, 'HighOutofPocket', 'High Out of Pocket');
INSERT INTO TypeofRisk (Id, Name, Description) VALUES (1, 'Nocap', 'No cap');
SET IDENTITY_INSERT TypeofRisk OFF
SET IDENTITY_INSERT TypeofCostSensitivity ON
INSERT INTO TypeofCostSensitivity (Id, Name, Description) VALUES (5, 'AllCostChanges', 'All Cost Changes');
INSERT INTO TypeofCostSensitivity (Id, Name, Description) VALUES (4, 'MostCostChanges', 'Most Cost Changes');
INSERT INTO TypeofCostSensitivity (Id, Name, Description) VALUES (3, 'ModerateCostChanges', 'Moderate Cost Changes');
INSERT INTO TypeofCostSensitivity (Id, Name, Description) VALUES (2, 'BigChangesOnly', 'Big Changes Only');
INSERT INTO TypeofCostSensitivity (Id, Name, Description) VALUES (1, 'DontBotherMeAboutPrice', 'Don''t Bother Me About Price');
SET IDENTITY_INSERT TypeofCostSensitivity OFF
SET IDENTITY_INSERT TypeofProviderPreference ON
INSERT INTO TypeofProviderPreference (Id, Name, Description) VALUES (5, 'IncludeAll', 'Include All');
INSERT INTO TypeofProviderPreference (Id, Name, Description) VALUES (4, 'Includeallbutone', 'Include all but one');
INSERT INTO TypeofProviderPreference (Id, Name, Description) VALUES (3, 'Includemost', 'Include most');
INSERT INTO TypeofProviderPreference (Id, Name, Description) VALUES (2, 'WillingtoChangemost', 'Willing to Change most');
INSERT INTO TypeofProviderPreference (Id, Name, Description) VALUES (1, 'WillingtoChangeall', 'Willing to Change all');
SET IDENTITY_INSERT TypeofProviderPreference OFF
SET IDENTITY_INSERT TypeofNote ON
INSERT INTO TypeofNote (Id, Name, Description) VALUES (1, 'TravelerComments', 'Traveler Comments');
INSERT INTO TypeofNote (Id, Name, Description) VALUES (2, 'BrandPreferenceComments', 'Brand Preference Comments');
INSERT INTO TypeofNote (Id, Name, Description) VALUES (3, 'FutureHealthConcernsComments', 'Future Health Concerns Comments');
SET IDENTITY_INSERT TypeofNote OFF