--#####################################################
--# CLEAN UP

--*****************************************************
--*TABLE: Record_Activity
--*****************************************************
-- REMOVE ROWS THAT DO NOT HAVE A VALID Record_ID
DELETE
	RA
FROM
	Record_Activity RA
LEFT JOIN
	Record R
	ON R.Record_ID = RA.Record_ID
WHERE
	R.Record_ID IS NULL
	AND RA.Record_ID IS NOT NULL;

-- CREATE UNKNOWN USER AND ASSIGN Record_Activity ROWS WITH INVALID [User_ID]
IF NOT EXISTS(SELECT * FROM [USER] WHERE [User_ID] = 0)
BEGIN
	SET IDENTITY_INSERT [USER] ON;
	
	INSERT INTO 
		[USER] ([User_ID], First_Name, Last_Name, User_Name, Password, Group_ID, Security_Level_ID, Auto_Assignment, Auto_Assignment_Life, Auto_Assignment_Pointer, Auto_Assignment_Life_Pointer, Assignment_Indicator, Assign_To, Auto_Territory_Indicator, Lead_Cap, In_AD)
	VALUES
		(0,'UNKNOWN', 'USER', 'UUSER', 'JD@&JFDOL*&U', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
	
	SET IDENTITY_INSERT [USER] OFF;
END

UPDATE
	RA
SET
	[User_ID] = 0
FROM
	Record_Activity RA
LEFT JOIN
	[User] U
	ON U.[User_ID] = RA.[User_ID]
WHERE
	U.[User_ID] IS NULL
	AND RA.[User_ID] IS NOT NULL;

--REMOVE ROWS THAT DO NOT HAVE A VALID Plan_Transaction_ID
DELETE
	RA
FROM
	Record_Activity RA
LEFT JOIN
	Plan_Transaction PT
	ON RA.Plan_Transaction_ID = PT.Plan_Transaction_ID
WHERE
	PT.Plan_Transaction_ID IS NULL
	AND RA.Plan_Transaction_ID IS NOT NULL;
	
--*****************************************************
--*TABLE: Plan
--*****************************************************
--REMOVE ROWS THAT DO NOT HAVE A VALID Carrier_ID
DELETE
	P
FROM
	[Plan] P
LEFT JOIN
	Carrier C
	ON P.Carrier_ID = C.Carrier_ID
WHERE
	C.Carrier_ID IS NULL
	AND P.Carrier_ID IS NOT NULL;


--*****************************************************
--*TABLE: Complaint_Activity
--*****************************************************
--REMOVE ROWS THAT DO NOT HAVE A VALID Complaint_ID
DELETE
	CA
FROM
	Complaint_Activity CA
LEFT JOIN
	Complaint C
	ON C.Complaint_ID = CA.Complaint_ID
WHERE
	C.Complaint_ID IS NULL
	AND CA.Complaint_ID IS NOT NULL;

--REMOVE ROWS THAT DO NOT HAVE A VALID Record_Activity_ID
DELETE
	CA
FROM
	Complaint_Activity CA
LEFT JOIN
	Record_Activity RA
	ON RA.Record_Activity_ID = CA.Record_Activity_ID
WHERE
	RA.Record_Activity_ID IS NULL
	AND CA.Record_Activity_ID IS NOT NULL;
	
--*****************************************************
--*TABLE: Reconciliation
--*****************************************************
--REMOVE ROWS THAT DO NOT HAVE A VALID Record_Activity_ID
DELETE
	R
FROM
	Reconciliation R
LEFT JOIN
	Record_Activity RA
	ON RA.Record_Activity_ID = R.Record_Activity_ID
WHERE
	RA.Record_Activity_ID IS NULL
	AND R.Record_Activity_ID IS NOT NULL;

--*****************************************************
--*TABLE: Plan_Transaction
--*****************************************************
--REMOVE ROWS THAT DO NOT HAVE A VALID Plan_ID

-- CREATE Opportunity AND ASSIGN Plan_Transaction ROWS WITH INVALID Plan_ID
IF NOT EXISTS(SELECT * FROM [Plan] WHERE Plan_ID = 0)
BEGIN
	SET IDENTITY_INSERT [Plan] ON;
	
	INSERT INTO 
		[Plan] (Plan_ID, Name, Carrier_ID, Active, Plan_Type_ID)
	VALUES
		(0, 'Opportunity', 0, 1, 0);
	
	SET IDENTITY_INSERT [Plan] OFF;
END

UPDATE
	PT
SET
	PT.Plan_ID = 0
FROM
	Plan_Transaction PT
LEFT JOIN
	[Plan] P
	ON PT.Plan_ID = P.Plan_ID
WHERE
	P.Plan_ID IS NULL
	AND PT.Plan_ID IS NOT NULL;

-- DUMMY CARRIER
IF NOT EXISTS(SELECT * FROM Carrier WHERE Carrier_ID = 0)
BEGIN
	SET IDENTITY_INSERT Carrier ON;
	
	INSERT INTO 
		Carrier (Carrier_ID, Name, Address_1, City, State_ID, Zip, Active, Need_Upload, LTC)
	VALUES
		(0, 'Dummy', '', '', 52, '', 1, 0, 0);
	
	SET IDENTITY_INSERT Carrier OFF;
END

-- SET Auto_Email_ID IN Record_Activity IF THE ID DOES NOT EXIST IN Auto_Email
UPDATE
	RA
SET 
	RA.Auto_Email_ID = NULL
FROM
	Record_Activity RA
LEFT JOIN
	Auto_Email AE
	ON AE.Auto_Email_ID = RA.Auto_Email_ID
WHERE
	AE.Auto_Email_ID IS NULL
	AND RA.Auto_Email_ID IS NOT NULL;
	
DELETE
	CR
FROM
	CallRating CR
LEFT JOIN
	Record_Activity RA
	ON RA.Record_Activity_ID = CR.Record_Activity_ID
WHERE
	RA.Record_Activity_ID IS NULL
	AND CR.Record_Activity_ID IS NOT NULL;

IF NOT EXISTS(SELECT * FROM Campaign_Code WHERE Campaign_Code_ID = 0)
BEGIN
	SET IDENTITY_INSERT Campaign_Code ON;
	
	INSERT INTO 
		Campaign_Code (Campaign_Code_ID, Campaign_Description, Active)
	VALUES
		(0, 'UNKNOWN', 1);
	
	SET IDENTITY_INSERT Campaign_Code OFF;
END

UPDATE
	RA
SET
	RA.Campaign_Code_ID = 0
FROM
	Record_Activity RA
LEFT JOIN
	Campaign_Code CC
	ON CC.Campaign_Code_ID = RA.Campaign_Code_ID
WHERE
	CC.Campaign_Code_ID IS NULL
	AND RA.Campaign_Code_ID IS NOT NULL;
	
UPDATE
	RA
SET
	RA.Record_Activity_Note_ID = NULL
FROM
	Record_Activity RA
LEFT JOIN
	Record_Activity_Note RAN
	ON RAN.Record_Activity_Note_ID = RA.Record_Activity_Note_ID
WHERE
	RAN.Record_Activity_Note_ID IS NULL
	AND RA.Record_Activity_Note_ID IS NOT NULL;

UPDATE
	RA
SET
	RA.Record_Activity_Status_ID = 1
FROM
	Record_Activity RA
LEFT JOIN
	Record_Activity_Status RAS
	ON RA.Record_Activity_Status_ID = RAS.Record_Activity_Status_ID
WHERE
	RAS.Record_Activity_Status_ID IS NULL
	AND RA.Record_Activity_Status_ID IS NOT NULL;

DELETE
	R
FROM
	Record R
LEFT JOIN
	[State] S
	ON S.State_ID = R.State_ID
WHERE
	S.State_ID IS NULL
	AND R.State_ID IS NOT NULL;

UPDATE
	R
SET
	R.Source_Code_ID = 602
FROM
	Record R
LEFT JOIN
	Source_Code SC
	ON SC.Source_Code_ID = R.Source_Code_ID
WHERE
	SC.Source_Code_ID IS NULL
	AND R.Source_Code_ID IS NOT NULL;

UPDATE
	R
SET
	R.Marital_Status_ID = 1
FROM
	Record R
LEFT JOIN
	Marital_Status MS
	ON MS.Marital_Status_ID = R.Marital_Status_ID
WHERE
	MS.Marital_Status_ID IS NULL
	AND R.Marital_Status_ID IS NOT NULL;
	
UPDATE
	R
SET 
	R.Record_Current_Situation_ID = 25 -- UNKNOWN
FROM
	RECORD R
LEFT JOIN
	Record_Current_Situation RCS
	ON RCS.Record_Current_Situation_ID = R.Record_Current_Situation_ID
WHERE
	RCS.Record_Current_Situation_ID IS NULL
	AND R.Record_Current_Situation_ID IS NOT NULL;

-- Create Compaint status table
/****** Object:  Table [dbo].[StatusofComplaint]    Script Date: 08/10/2014 10:51:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StatusofComplaint](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
 CONSTRAINT [PK_StatusofComplaint] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO

SET IDENTITY_INSERT [StatusofComplaint] ON

INSERT INTO [StatusofComplaint]
	([Id], [Description])
VALUES
	(1, 'Open');
	
INSERT INTO [StatusofComplaint]
	([Id], [Description])
VALUES
	(2, 'InProgress');
	
INSERT INTO [StatusofComplaint]
	([Id], [Description])
VALUES
	(3, 'Resoloved');
	
INSERT INTO [StatusofComplaint]
	([Id], [Description])
VALUES
	(4, 'All');

SET IDENTITY_INSERT [StatusofComplaint] OFF


SET IDENTITY_INSERT Reconciliation_Type ON

INSERT INTO 
	Reconciliation_Type
	(Reconciliation_Type_ID, [Description], Display_Order, Active)
VALUES
	(44, 'Unknown', 99, 0);

SET IDENTITY_INSERT Reconciliation_Type OFF

--*****************************************************
--*CREATE FK RELATIONSHIPS
--*****************************************************
ALTER TABLE
	Complaint_Activity
ADD CONSTRAINT 
	FK_Complaint_Activity_Complaint FOREIGN KEY (Complaint_ID)
REFERENCES 
	Complaint (Complaint_ID);

ALTER TABLE
	Complaint_Activity
ADD CONSTRAINT 
	FK_Complaint_Activity_Record_Activity FOREIGN KEY (Record_Activity_ID)
REFERENCES 
	Record_Activity (Record_Activity_ID);
	
ALTER TABLE
	Record_Activity
ADD CONSTRAINT 
	FK_Record_Activity_Record FOREIGN KEY (Record_ID)
REFERENCES 
	Record (Record_ID);

ALTER TABLE
	Record_Activity
ADD CONSTRAINT 
	FK_Record_Activity_User FOREIGN KEY ([User_ID])
REFERENCES 
	[User] ([User_ID]);

ALTER TABLE
	Record_Activity
ADD CONSTRAINT 
	FK_Record_Activity_Plan_Transaction FOREIGN KEY (Plan_Transaction_ID)
REFERENCES 
	Plan_Transaction (Plan_Transaction_ID);

ALTER TABLE
	Plan_Transaction
ADD CONSTRAINT 
	FK_Plan_Transaction_Plan FOREIGN KEY (Plan_ID)
REFERENCES 
	[Plan] (Plan_ID);
	
ALTER TABLE
	Reconciliation
ADD CONSTRAINT 
	FK_Reconciliation_Record_Activity FOREIGN KEY (Record_Activity_ID)
REFERENCES 
	Record_Activity (Record_Activity_ID);

ALTER TABLE
	[Plan]
ADD CONSTRAINT 
	FK_Plan_Carrier FOREIGN KEY (Carrier_ID)
REFERENCES 
	Carrier (Carrier_ID);

ALTER TABLE
	Agent
ADD CONSTRAINT 
	FK_Agent_User FOREIGN KEY ([User_ID])
REFERENCES 
	[User] ([User_ID]);
	
ALTER TABLE
	Record_Activity
ADD CONSTRAINT 
	FK_Record_Activity_Auto_Email FOREIGN KEY (Auto_Email_ID)
REFERENCES
	Auto_Email (Auto_Email_ID);

ALTER TABLE
	CallRating
ADD CONSTRAINT 
	FK_CallRating_Record_Activity FOREIGN KEY (Record_Activity_ID)
REFERENCES
	Record_Activity (Record_Activity_ID);

ALTER TABLE
	Record_Activity
ADD CONSTRAINT 
	FK_Record_Activity_Campaign_Code FOREIGN KEY (Campaign_Code_ID)
REFERENCES
	Campaign_Code (Campaign_Code_ID);

ALTER TABLE
	Record_Activity
ADD CONSTRAINT 
	FK_Record_Activity_Record_Activity_Note FOREIGN KEY (Record_Activity_Note_ID)
REFERENCES
	Record_Activity_Note (Record_Activity_Note_ID);


--*****************************************************
--*CREATE FK RELATIONSHIPS ON LOOKUP TABLES
--*****************************************************
ALTER TABLE
	Record_Activity
ADD CONSTRAINT 
	FK_Record_Activity_Record_Activity_Status FOREIGN KEY (Record_Activity_Status_ID)
REFERENCES
	Record_Activity_Status (Record_Activity_Status_ID);
	
ALTER TABLE
	Record
ADD CONSTRAINT 
	FK_Record_State FOREIGN KEY (State_ID)
REFERENCES
	[State] (State_ID);

ALTER TABLE
	Record
ADD CONSTRAINT 
	FK_Record_Alt_State FOREIGN KEY (Alt_State_ID)
REFERENCES
	[State] (State_ID);

ALTER TABLE
	Record
ADD CONSTRAINT 
	FK_Record_Employment_Status FOREIGN KEY (Employment_Status_ID)
REFERENCES
	Employment_Status (Employment_Status_ID);

ALTER TABLE
	Record
ADD CONSTRAINT 
	FK_Record_Source_Code FOREIGN KEY (Source_Code_ID)
REFERENCES
	Source_Code (Source_Code_ID);

ALTER TABLE
	Record
ADD CONSTRAINT 
	FK_Record_Marital_Status FOREIGN KEY (Marital_Status_ID)
REFERENCES
	Marital_Status (Marital_Status_ID);

ALTER TABLE
	Record
ADD CONSTRAINT 
	FK_Record_Record_Source FOREIGN KEY (Record_Source_ID)
REFERENCES
	Record_Source (Record_Source_ID);

ALTER TABLE
	Record
ADD CONSTRAINT 
	FK_Record_Record_Type FOREIGN KEY (Record_Type_ID)
REFERENCES
	Record_Type (Record_Type_ID);

ALTER TABLE
	User_Security_Level
ADD CONSTRAINT 
	FK_User_Security_Level_USER FOREIGN KEY ([User_ID])
REFERENCES
	[USER] ([User_ID]);

ALTER TABLE
	Record
ADD CONSTRAINT 
	FK_Record_Record_Current_Situation FOREIGN KEY (Record_Current_Situation_ID)
REFERENCES
	Record_Current_Situation (Record_Current_Situation_ID);
	
ALTER TABLE
	Reconciliation
ADD CONSTRAINT 
	FK_Reconciliation_Reconciliation_Type FOREIGN KEY (Reconciliation_Type_ID)
REFERENCES
	Reconciliation_Type (Reconciliation_Type_ID);