﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq.Mapping;
using System.Data.Linq;

namespace LMS.Data
{
    partial class Plan
    {
        //private EntityRef<Carrier> _Carrier;

        //[Association(Name = "Carrier", Storage = "_Carrier", ThisKey = "Carrier_ID", OtherKey = "Carrier_ID", IsUnique = true, IsForeignKey = false)]
        //public Carrier Carrier
        //{
        //    get
        //    {
        //        return this._Carrier.Entity;
        //    }
        //}

        public IQueryable<Plan_Document> Plan_Documents
        {
            get
            {
                LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
                return db.Plan_Documents.Where(pd => pd.Plan_ID == _Plan_ID);
            }
        }
       
    }
}
