﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq.Mapping;
using System.Data.Linq;


namespace LMS.Data
{
    public partial class Record
    {
        private LMSDataContext _db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
        private EntityRef<Record_Type> _Record_Type;
        private EntityRef<State> _State;
        private EntityRef<State> _Alt_State;
        private EntityRef<State> _Mailing_State;
        private EntityRef<Source_Code> _Source_Code;
        private EntityRef<Referral_Type> _Referral_Type;
        private EntityRef<User> _Agent;
        private EntityRef<Company> _Company;
        private EntityRef<User> _Assistant_Agent;
        private EntityRef<Buying_Period> _Buying_Period;
        private EntityRef<Buying_Period_Sub> _Buying_Period_Sub;
        private EntityRef<Employment_Status> _Employment_Status;
        private EntityRef<Marital_Status> _Marital_Status;
        //private EntityRef<Occupation> _Occupation;
        private EntityRef<Tobacco> _Current_Tobacco;
        private EntityRef<Tobacco> _Past_Tobacco;
        private EntityRef<Tobacco_Use> _Current_Tobacco_Use_Status;
        private EntityRef<Tobacco_Use> _Past_Tobacco_Use_Status;
        private EntityRef<Broker> _Broker;
        private EntitySet<Record_Activity> _Record_Activities;
        private EntitySet<Record_Address> _Record_Addresses;
        private EntityRef<Record_Eligibility> _Record_Eligibility;
        private EntityRef<Current_Insured_Type> _Current_Insured_Type;
        private EntityRef<Record_Current_Situation> _Record_Current_Situation;
        private EntityRef<Products_Of_Interest> _Products_Of_Interest;
        private EntityRef<Subsidy> _Subsidy;



        #region Properties

        public string FullName
        {
            get
            {
                string name = "";

                if (Salutation_ID > 0)
                {
                    name += _db.Salutations.First(s => s.Salutation_ID == _Salutation_ID).Description + " ";
                }

                name += _First_Name + " ";

                if (_Middle_Initial != null)
                    if (_Middle_Initial.Length > 0)
                        name += _Middle_Initial;


                return name + " " + _Last_Name + " " + _Suffix;
            }
        }

        public Record Partner
        {
            get
            {
                LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
                return db.Records.FirstOrDefault(r => r.Record_ID == _Partner_ID);
            }
        }

        public DateTime FollowUpDate()
        {
            DateTime followUp = new DateTime();

            var DueDates = this.Record_Activities.Where(ra => ra.Actual_Date.HasValue == false).OrderBy(ra => ra.Due_Date);

            if (DueDates.Count() > 0)
            {
                followUp = DueDates.First().Due_Date.Value;
            }
            else
            {
                if (_Follow_Up_Date.HasValue)
                {
                    followUp = _Follow_Up_Date.Value;
                }
            }

            return followUp;
        }

        public bool IsAlternateAddress()
        {
            bool value = false;

            if (_Alt_Address_From_Date.HasValue && Alt_Address_To_Date.HasValue)
            {
                if (_Alt_Address_From_Date < DateTime.Now && _Alt_Address_To_Date > DateTime.Now)
                {
                    value = true;
                }
            }

            return value;
        }

        public string LastAgent()
        {
            string value = "";

            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

            var Agents = db.Record_Assigneds.Where(r => r.Record_ID.Equals(this.Record_ID));
            
            if (Agents != null)
            {
                var orderedAgents = Agents.OrderByDescending(a => a.Date_Value);

                if (orderedAgents.Count() > 1)
                {
                    User agent = db.Users.Where(u => u.User_ID.Equals(orderedAgents.ToArray()[orderedAgents.Count()-1].User_ID)).First();

                    value = agent.Last_Name + ", " + agent.First_Name;

                }
            }

            return value;
        }

        public string OriginalAgent()
        {
            string value = "";

            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

            var Agents = db.Users.Where(u => u.User_ID.Equals(this.Original_Agent_ID)).FirstOrDefault();

            if (Agents != null)
            {
                 value = Agents.Last_Name + ", " + Agents.First_Name;
            }

            return value;
        }

        /*public string Record_Eligibility()
        {
            string value = "";

            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

            var Re = db.Record_Eligibilities.Where(u => u.Record_Eligibility_ID.Equals(this.Record_Eligibility_ID)).FirstOrDefault();

            if (Re != null)
            {
                value = Re.Category;
            }

            return value;
        }*/

        #endregion

        #region Functions

        public int NumberOfContactsBy(int Agent_ID)
        {
            return _db.Record_Activities.Where(r => r.Record_ID == _Record_ID && r.User_ID == Agent_ID).Count();
        }

        public int NumberOfFailedContactsBy(int Agent_ID)
        {
            return _db.Record_Activities.Where(r => r.Record_ID == _Record_ID && r.User_ID == Agent_ID && r.Actual_Date.HasValue == true && r.Made_Contact == false).Count();
        }

        #endregion

        #region References

        [Association(Name = "Broker", Storage = "_Broker", ThisKey = "Broker_ID", OtherKey = "Broker_ID", IsUnique = true, IsForeignKey = false)]
        public Broker Broker
        {
            get
            {
                return this._Broker.Entity;
            }
        }

        [Association(Name = "Subsidy", Storage = "_Subsidy", ThisKey = "Subsidy_ID", OtherKey = "Subsidy_ID", IsUnique = true, IsForeignKey = false)]
        public Subsidy Subsidy
        {
            get
            {
                return this._Subsidy.Entity;
            }
        }

        [Association(Name = "Products_Of_Interest", Storage = "_Products_Of_Interest", ThisKey = "Products_Of_Interest_ID", OtherKey = "Products_Of_Interest_ID", IsUnique = true, IsForeignKey = false)]
        public Products_Of_Interest Products_Of_Interest
        {
            get
            {
                return this._Products_Of_Interest.Entity;
            }
        }

        [Association(Name = "Record_Current_Situation", Storage = "_Record_Current_Situation", ThisKey = "Record_Current_Situation_ID", OtherKey = "Record_Current_Situation_ID", IsUnique = true, IsForeignKey = false)]
        public Record_Current_Situation Record_Current_Situation
        {
            get
            {
                return this._Record_Current_Situation.Entity;
            }
        }

        [Association(Name = "Current_Insured_Type", Storage = "_Current_Insured_Type", ThisKey = "Current_Insured_Type_ID", OtherKey = "Current_Insured_Type_ID", IsUnique = true, IsForeignKey = false)]
        public Current_Insured_Type Current_Insured_Type
        {
            get
            {
                return this._Current_Insured_Type.Entity;
            }
        }

        [Association(Name = "Record_Eligibility", Storage = "_Record_Eligibility", ThisKey = "Record_Eligibility_ID", OtherKey = "Record_Eligibility_ID", IsUnique = true, IsForeignKey = false)]
        public Record_Eligibility Record_Eligibility
        {
            get
            {
                return this._Record_Eligibility.Entity;
            }
        }

        [Association(Name = "Record_Type", Storage = "_Record_Type", ThisKey = "Record_Type_ID", OtherKey = "Record_Type_ID", IsUnique = true, IsForeignKey = false)]
        public Record_Type Record_Type
        {
            get
            {
                return this._Record_Type.Entity;
            }
        }

        public IQueryable<Plan_Transaction> Transactions
        {
            get
            {
                LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
                return db.Plan_Transactions.Where(p => p.Record_ID == _Record_ID);
            }
        }

        [Association(Name = "Company", Storage = "_Company", ThisKey = "Company_ID", OtherKey = "Company_ID", IsUnique = true, IsForeignKey = false)]
        public Company Company
        {
            get
            {
                return this._Company.Entity;
            }
        }

        [Association(Name = "Referral_Type", Storage = "_Referral_Type", ThisKey = "Referral_Type_ID", OtherKey = "Referral_Type_ID", IsUnique = true, IsForeignKey = false)]
        public Referral_Type Referral_Type
        {
            get
            {
                return this._Referral_Type.Entity;
            }
        }

        [Association(Name = "Source_Code", Storage = "_Source_Code", ThisKey = "Source_Code_ID", OtherKey = "Source_Code_ID", IsUnique = true, IsForeignKey = false)]
        public Source_Code Source_Code
        {
            get
            {
                return this._Source_Code.Entity;
            }
        }

        [Association(Name = "State", Storage = "_State", ThisKey = "State_ID", OtherKey = "State_ID", IsUnique = true, IsForeignKey = false)]
        public State State
        {
            get
            {
                return this._State.Entity;
            }
        }

        [Association(Name = "Alt_State", Storage = "_Alt_State", ThisKey = "Alt_State_ID", OtherKey = "State_ID", IsUnique = true, IsForeignKey = false)]
        public State Alt_State
        {
            get
            {
                return this._Alt_State.Entity;
            }
        }

        [Association(Name = "Mailing_State", Storage = "_Mailing_State", ThisKey = "Mailing_State_ID", OtherKey = "State_ID", IsUnique = true, IsForeignKey = false)]
        public State Mailing_State
        {
            get
            {
                return this._Mailing_State.Entity;
            }
        }

        [Association(Name = "Agent", Storage = "_Agent", ThisKey = "Agent_ID", OtherKey = "User_ID", IsUnique = true, IsForeignKey = false)]
        public User Agent
        {
            get
            {
                return this._Agent.Entity;
            }
        }

        [Association(Name = "Assistant_Agent", Storage = "_Assistant_Agent", ThisKey = "Assistant_Agent_ID", OtherKey = "User_ID", IsUnique = true, IsForeignKey = false)]
        public User Assistant_Agent
        {
            get
            {
                return this._Assistant_Agent.Entity;
            }
        }

        [Association(Name = "Buying_Period", Storage = "_Buying_Period", ThisKey = "Buying_Period_ID", OtherKey = "Buying_Period_ID", IsUnique = true, IsForeignKey = false)]
        public Buying_Period Buying_Period
        {
            get
            {
                return this._Buying_Period.Entity;
            }
        }

        [Association(Name = "Buying_Period_Sub", Storage = "_Buying_Period_Sub", ThisKey = "Buying_Period_Sub_ID", OtherKey = "Buying_Period_Sub_ID", IsUnique = true, IsForeignKey = false)]
        public Buying_Period_Sub Buying_Period_Sub
        {
            get
            {
                return this._Buying_Period_Sub.Entity;
            }
        }

        [Association(Name = "Employment_Status", Storage = "_Employment_Status", ThisKey = "Employment_Status_ID", OtherKey = "Employment_Status_ID", IsUnique = true, IsForeignKey = false)]
        public Employment_Status Employment_Status
        {
            get
            {
                return this._Employment_Status.Entity;
            }
        }
        [Association(Name = "Marital_Status", Storage = "_Marital_Status", ThisKey = "Marital_Status_ID", OtherKey = "Marital_Status_ID", IsUnique = true, IsForeignKey = false)]
        public Marital_Status Marital_Status
        {
            get
            {
                return this._Marital_Status.Entity;
            }
        }

        //[Association(Name = "Occupation", Storage = "_Occupation", ThisKey = "Occupation_ID", OtherKey = "Occupation_ID", IsUnique = true, IsForeignKey = false)]
        //public Occupation Occupation
        //{
        //    get
        //    {
        //        return this._Occupation.Entity;
        //    }
        //}

        [Association(Name = "Current_Tobacco", Storage = "_Current_Tobacco", ThisKey = "Current_Tobacco_ID", OtherKey = "Tobacco_ID", IsUnique = true, IsForeignKey = false)]
        public Tobacco Current_Tobacco
        {
            get
            {
                return this._Current_Tobacco.Entity;
            }
        }

        [Association(Name = "Past_Tobacco", Storage = "_Past_Tobacco", ThisKey = "Past_Tobacco_ID", OtherKey = "Tobacco_ID", IsUnique = true, IsForeignKey = false)]
        public Tobacco Past_Tobacco
        {
            get
            {
                return this._Past_Tobacco.Entity;
            }
        }

        [Association(Name = "Current_Tobacco_Use_Status", Storage = "_Current_Tobacco_Use_Status", ThisKey = "Current_Tobacco_Use_Status_ID", OtherKey = "Tobacco_Use_ID", IsUnique = true, IsForeignKey = false)]
        public Tobacco_Use Current_Tobacco_Use_Status
        {
            get
            {
                return this._Current_Tobacco_Use_Status.Entity;
            }
        }

        [Association(Name = "Past_Tobacco_Use_Status", Storage = "_Past_Tobacco_Use_Status", ThisKey = "Past_Tobacco_Use_Status_Id", OtherKey = "Tobacco_Use_ID", IsUnique = true, IsForeignKey = false)]
        public Tobacco_Use Past_Tobacco_Use_Status
        {
            get
            {
                return this._Past_Tobacco_Use_Status.Entity;
            }
        }


        public IQueryable<Record_Activity> Record_Activities
        {
            get
            {
                LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
                return db.Record_Activities.Where(ra => ra.Record_ID == _Record_ID);
            }
        }

        public IQueryable<Record_Address> Record_Addresses
        {
            get
            {
                LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
                return db.Record_Addresses.Where(ra => ra.Record_ID == _Record_ID);
            }
        }

        #endregion

        #region static Functions

        public Record Clone()
        {
            Record clone = new Record();

            try
            {
                clone.Address_1 = _Address_1;
                clone.Address_2 = this._Address_2;
                clone.Age = this._Age;
                clone.Agent_ID = this._Agent_ID;
                clone.Alt_Address_1 = this._Alt_Address_1;
                clone.Alt_Address_2 = _Alt_Address_2;
                clone.Alt_Address_From_Date = _Alt_Address_From_Date;
                clone.Alt_Address_To_Date = _Alt_Address_To_Date;
                clone.Alt_City = _Alt_City;
                clone.Alt_County = _Alt_County;
                clone.Alt_State_ID = _Alt_State_ID;
                clone.Alt_Zipcode = _Alt_Zipcode;
                clone.Annual_Income = _Annual_Income;
                clone.Assistant_Agent_ID = _Assistant_Agent_ID;
                clone.B2E = _B2E;
                clone.Buying_Period_ID = _Buying_Period_ID;
                clone.Buying_Period_Notes = _Buying_Period_Notes;
                clone.Buying_Period_Sub_ID = _Buying_Period_Sub_ID;
                clone.Cell_Phone = _Cell_Phone;
                clone.City = _City;
                clone.Client_Type_ID = _Client_Type_ID;
                clone.Company_ID = _Company_ID;
                clone.County = _County;
                clone.Created_On = _Created_On;
                clone.Current_Insured_Desc = _Current_Insured_Desc;
                clone.Current_Tobacco_ID = _Current_Tobacco_ID;
                clone.Current_Tobacco_Use = _Current_Tobacco_Use;
                clone.Currently_Insured = _Currently_Insured;
                clone.Customer_ID = _Customer_ID;
                clone.Do_Not_Contact = _Do_Not_Contact;
                clone.DOB = _DOB;
                clone.Drug_List_ID = _Drug_List_ID;
                clone.Email_Address = _Email_Address;
                clone.Employment_Status_ID = _Employment_Status_ID;
                clone.Fax = _Fax;
                clone.First_Name = _First_Name;
                clone.Follow_Up_Date = _Follow_Up_Date;
                clone.Height = _Height;
                clone.Home_Phone = _Home_Phone;
                clone.Imported = _Imported;
                clone.Initial_Contact_Date = _Initial_Contact_Date;
                clone.Last_Name = _Last_Name;
                clone.Last_Updated_Date = _Last_Updated_Date;
                clone.Marital_Status_ID = _Marital_Status_ID;
                clone.Medicare_Number = _Medicare_Number;
                clone.Middle_Initial = _Middle_Initial;
                clone.Momentum = _Momentum;
                clone.Notes = _Notes;
                //clone.Occupation_ID = _Occupation_ID;
                clone.Part_A_Effective_Date = _Part_A_Effective_Date;
                clone.Part_B_Effective_Date = _Part_B_Effective_Date;
                clone.Partner_ID = _Partner_ID;
                clone.Past_Tobacco_ID = _Past_Tobacco_ID;
                clone.Past_Tobacco_Use = _Past_Tobacco_Use;
                clone.Priority_Level = _Priority_Level;
                clone.Quit_Date = _Quit_Date;
                clone.Record_Source_ID = _Record_Source_ID;
                clone.Record_Type_ID = _Record_Type_ID;
                clone.Referral_Type_ID = _Referral_Type_ID;
                clone.Reffered_By = _Reffered_By;
                clone.Salutation_ID = _Salutation_ID;
                clone.Sex = _Sex;
                clone.Source_Code_ID = _Source_Code_ID;
                clone.SSN = _SSN;
                clone.State_ID = _State_ID;
                clone.Suffix = _Suffix;
                clone.Unsubscribed = _Unsubscribed;
                clone.Weight = _Weight;
                clone.Work_Phone = _Work_Phone;
                clone.Zipcode = _Zipcode;
                clone.Alt_Phone_Number = _Alt_Phone_Number;
                clone.Alt_First_Name = _Alt_First_Name;
                clone.Alt_Last_Name = _Alt_Last_Name;
                clone.Alt_Email_Address = _Alt_Email_Address;
                clone.Broker_ID = _Broker_ID;
                clone.Medicare_FirstName = _Medicare_FirstName;
                clone.Medicare_InitialName = _Medicare_InitialName;
                clone.Medicare_LastName = _Medicare_LastName;
                clone.Medical_Carrier = _Medical_Carrier;
                clone.Drug_Plan_Co = _Drug_Plan_Co;
                clone.Drug_Plan_Original_Start_Date = _Drug_Plan_Original_Start_Date;
                clone.Current_Tobacco_Use_Status_ID = _Current_Tobacco_Use_Status_ID;
                clone.Past_Tobacco_Use_Status_Id = Past_Tobacco_Use_Status_Id;
                clone.Occupation = _Occupation;
            }
            catch (Exception ex)
            {
                LMS.ErrorHandler.HandleError.EmailError(ex);
            }

            return clone;
        }

        #endregion

        #region Business Rules

        partial void OnMomentumChanging(bool? value)
        {
            
        }

        partial void OnAgent_IDChanging(int value)
        {
            // If agent was unassigned set agent assigned date
            //if (_Agent_ID == 49)
            //{
            //    _Agent_Assigned_Date = DateTime.Now;
            //}

        }


        partial void OnRecord_Type_IDChanging(int value)
        {
          // this._Status_Changed_Date = DateTime.Now;
        }


        partial void OnValidate(ChangeAction action)
        {
            _Status_Changed_Date = DateTime.Now;

            if (action == ChangeAction.Insert)
            {
                if ( _Partner_ID < 1)
                    _Created_On = DateTime.Now;
            }

            if (_Record_ID > 0)
            {
                int OldAgentID = GetOldAgentID(_Record_ID);

                if (OldAgentID != this._Agent_ID)
                {
                    _db.Insert_Record_Assigned(_Record_ID, _Agent_ID);
                }
            }

            
        }

        private int GetOldAgentID(int Record_ID)
        {
            Record r = _db.Records.FirstOrDefault(rc => rc.Record_ID == Record_ID);

            return r.Agent_ID;
        }
        #endregion

    }
}
