﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMS.Data
{
    public class Section
    {
        public int LeadTotal { get; set; }
        public int NumberAttempted { get; set; }
        public int NumberContact { get; set; }
        public int AppsSent { get; set; }
        public int AppsRecieved { get; set; }
        public int AppsPlaced { get; set; }
        public int Count { get; set; }


        public double ContactedPercent
        {
            get { return (double)Count / (double)LeadTotal; }
        }

    }
}
