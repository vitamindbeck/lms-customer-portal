﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace LMS.Data
{
    public partial class Plan_Transaction
    {
        private LMSDataContext _db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
        private EntityRef<Plan> _Plan;
        private EntityRef<Plan_Transaction_Status> _Plan_Transaction_Status;
        private EntityRef<Group> _Group;
        private EntityRef<Modal> _Modal;
        private EntityRef<Payment_Mode> _Payment_Mode;
        private EntityRef<User> _Agent;
        private EntityRef<Source_Code> _Source_Code;
        private EntityRef<Underwriting_Class> _Underwriting_Class;
        private EntityRef<Shipping_Method> _Outbound_Shipping_Method;
        private EntityRef<Shipping_Method> _Inbound_Shipping_Method;
        private EntityRef<Exam_Service> _Exam_Service;
        private EntityRef<Buying_Period_Sub> _Buying_Period_Sub;
        private EntityRef<Buying_Period> _Buying_Period;
    

        public int Activity_Count
        {
            get
            {
                return (from ra in _db.Record_Activities where ra.Record_ID == this.Record_ID select ra).Count();
            }
        }

        [Association(Name = "Plan", Storage = "_Plan", ThisKey = "Plan_ID", OtherKey = "Plan_ID", IsUnique = true, IsForeignKey = false)]
        public Plan Plan
        {
            get
            {
                return this._Plan.Entity;
            }
        }

        [Association(Name = "Exam_Service", Storage = "_Exam_Service", ThisKey = "Exam_Service_ID", OtherKey = "Exam_Service_ID", IsUnique = true, IsForeignKey = false)]
        public Exam_Service Exam_Service
        {
            get
            {
                return this._Exam_Service.Entity;
            }
        }
        [Association(Name = "Buying_Period_Sub", Storage = "_Buying_Period_Sub", ThisKey = "Buying_Period_Sub_ID", OtherKey = "Buying_Period_Sub_ID", IsUnique = true, IsForeignKey = false)]
        public Buying_Period_Sub Buying_Period_Sub
        {
            get
            {
                return this._Buying_Period_Sub.Entity;
            }
        }
        [Association(Name = "Buying_Period", Storage = "_Buying_Period", ThisKey = "Buying_Period_ID", OtherKey = "Buying_Period_ID", IsUnique = true, IsForeignKey = false)]
        public Buying_Period Buying_Period
        {
            get
            {
                return this._Buying_Period.Entity;
            }
        }

        [Association(Name = "Outbound_Shipping_Method", Storage = "_Outbound_Shipping_Method", ThisKey = "Outbound_Shipping_Method_ID", OtherKey = "Shipping_Method_ID", IsUnique = true, IsForeignKey = false)]
        public Shipping_Method Outbound_Shipping_Method
        {
            get
            {
                return this._Outbound_Shipping_Method.Entity;
            }
        }

        [Association(Name = "Inbound_Shipping_Method", Storage = "_Inbound_Shipping_Method", ThisKey = "Inbound_Shipping_Method_ID", OtherKey = "Shipping_Method_ID", IsUnique = true, IsForeignKey = false)]
        public Shipping_Method Inbound_Shipping_Method
        {
            get
            {
                return this._Inbound_Shipping_Method.Entity;
            }
        }

        [Association(Name = "Plan_Transaction_Status", Storage = "_Plan_Transaction_Status", ThisKey = "Plan_Transaction_Status_ID", OtherKey = "Plan_Transaction_Status_ID", IsUnique = true, IsForeignKey = false)]
        public Plan_Transaction_Status Plan_Transaction_Status
        {
            get
            {
                return this._Plan_Transaction_Status.Entity;
            }
        }

        [Association(Name = "Payment_Mode", Storage = "_Payment_Mode", ThisKey = "Payment_Mode_ID", OtherKey = "Payment_Mode_ID", IsUnique = true, IsForeignKey = false)]
        public Payment_Mode Payment_Mode
        {
            get
            {
                return this._Payment_Mode.Entity;
            }
        }

        [Association(Name = "Group", Storage = "_Group", ThisKey = "Group_ID", OtherKey = "Group_ID", IsUnique = true, IsForeignKey = false)]
        public Group Group
        {
            get
            {
                return this._Group.Entity;
            }
        }

        [Association(Name = "Modal", Storage = "_Modal", ThisKey = "Modal_ID", OtherKey = "Modal_ID", IsUnique = true, IsForeignKey = false)]
        public Modal Modal
        {
            get
            {
                return this._Modal.Entity;
            }
        }

        public string DropDownName
        {
            get
            {
                if (Plan == null || Plan_Transaction_Status_ID < 1)
                {
                    return Group.Description + " Opportunity";
                }
                else
                {//Mike Mc 12/22/2013 - added Plan_Transaction_Status.Description
                    return Plan.Name + " - " + Plan_Transaction_Status.Description;
                }
            }
        }

        [Association(Name = "Agent", Storage = "_Agent", ThisKey = "Agent_ID", OtherKey = "User_ID", IsUnique = true, IsForeignKey = false)]
        public User Agent
        {
            get
            {
                return this._Agent.Entity;
            }
        }

        [Association(Name = "Source_Code", Storage = "_Source_Code", ThisKey = "Source_Code_ID", OtherKey = "Source_Code_ID", IsUnique = true, IsForeignKey = false)]
        public Source_Code Source_Code
        {
            get
            {
                return this._Source_Code.Entity;
            }
        }

        [Association(Name = "Underwriting_Class", Storage = "_Underwriting_Class", ThisKey = "Underwriting_Class_ID", OtherKey = "Underwriting_ID", IsUnique = true, IsForeignKey = false)]
        public Underwriting_Class Underwriting_Class
        {
            get
            {
                return this._Underwriting_Class.Entity;
            }
        }

        public IEnumerable<Plan_Transaction_Date> Plan_Dates
        {
            get
            {
                return _db.Plan_Transaction_Dates.Where(ptd => ptd.Plan_Transaction_ID == _Plan_Transaction_ID);
            }
        }




        partial void OnValidate(ChangeAction action)
        {
            
            
        }

        partial void OnPlan_Transaction_Status_IDChanged()
        {
            this.Time_Stamp = DateTime.Now;
        }
    }
}
