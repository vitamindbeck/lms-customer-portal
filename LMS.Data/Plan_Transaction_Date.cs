﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace LMS.Data
{
    partial class Plan_Transaction_Date
    {
        private EntityRef<Transaction_Date_Type> _Transaction_Date_Type;

        [Association(Name = "Transaction_Date_Type", Storage = "_Transaction_Date_Type", ThisKey = "Transaction_Date_Type_ID", OtherKey = "Transaction_Date_Type_ID", IsUnique = true, IsForeignKey = false)]
        public Transaction_Date_Type Transaction_Date_Type
        {
            get
            {
                return this._Transaction_Date_Type.Entity;
            }
        }

        partial void OnValidate(ChangeAction action)
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

            if (action == ChangeAction.Update)
            {
                Plan_Transaction tran = db.Plan_Transactions.FirstOrDefault(p => p.Plan_Transaction_ID==_Plan_Transaction_ID);

                if (tran != null)
                {
                    Record record = db.Records.FirstOrDefault(r => r.Record_ID == tran.Record_ID);

                    if (record != null)
                    {
                        if (tran.Active.HasValue)
                        {
                            if (!tran.Active.Value && this._Transaction_Date_Type_ID != 8 && this._Transaction_Date_Type_ID !=11)
                            {
                                record.Record_Type_ID = 3;
                                db.SubmitChanges();
                            }
                        }
                    }
                }
            }
        }
    }
}
