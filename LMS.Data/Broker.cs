﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMS.Data
{
    partial class Broker
    {
        public string DropDownName
        {
            get
            {
                string name = _Last_Name + ", " + _First_Name;

                if (_Last_Name.StartsWith("--"))
                    name = _Last_Name;

                if (_First_Name.StartsWith("None"))
                    name = _First_Name;

                return name; 
            }
        }
    }
}
