﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq.Mapping;
using System.Data.Linq;

namespace LMS.Data
{
    /// <summary>
    /// The appointment.
    /// </summary>
    public partial class Appointment
    {


        private LMSDataContext _db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
        private EntityRef<Insurance_Line_Lookup> _Insurance_Line_Lookup;
        private EntityRef<Agent> _Agent;
        private EntityRef<Appointment_State> _Appointment_State;
        private EntityRef<Carrier> _Carrier;


        [Association(Name = "Insurance_Line_Lookup", Storage = "_Insurance_Line_Lookup", ThisKey = "Line_Of_Insurance_ID", OtherKey = "Line_Of_Insurance_ID", IsUnique = true, IsForeignKey = false)]
        public Insurance_Line_Lookup Insurance_Line_Lookup
        {
            get
            {
                return this._Insurance_Line_Lookup.Entity;
            }
        }

        [Association(Name = "Agent", Storage = "_Agent", ThisKey = "Agent_ID", OtherKey = "Agent_ID", IsUnique = true, IsForeignKey = false)]
        public Agent Agent
        {
            get
            {
                return this._Agent.Entity;
            }
        }

        [Association(Name = "Appointment_State", Storage = "_Appointment_State", ThisKey = "Appointment_ID", OtherKey = "Appointment_ID", IsUnique = true, IsForeignKey = false)]
        public Appointment_State Appointment_State
        {
            get
            {
                return this._Appointment_State.Entity;
            }
        }

        [Association(Name = "Carrier", Storage = "_Carrier", ThisKey = "Carrier_ID", OtherKey = "Carrier_ID", IsUnique = true, IsForeignKey = false)]
        public Carrier Carrier
        {
            get
            {
                return this._Carrier.Entity;
            }
        }
    }
    }

