﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq.Mapping;
using System.Data.Linq;

namespace LMS.Data
{
    public partial class Source_Code
    {
        private EntityRef<Group> _Group;
        private EntityRef<Affinity_Partner> _Affinity_Partner;

        public string DropDownName 
        { 
            get 
            {
                string name = _Source_Number + " " + _Description;

                
                if (name.Length > 50)
                {
                    name = name.Substring(0, 47) + "...";
                }

                return name; 
            }
        }

        #region References
        [Association(Name = "Group", Storage = "_Group", ThisKey = "Group_ID", OtherKey = "Group_ID", IsUnique = true, IsForeignKey = false)]
        public Group Group
        {
            get
            {
                return this._Group.Entity;
            }
        }

        [Association(Name = "Affinity_Partner", Storage = "_Affinity_Partner", ThisKey = "Affinity_Partner_ID", OtherKey = "Affinity_Partner_ID", IsUnique = true, IsForeignKey = false)]
        public Affinity_Partner Affinity_Partner
        {
            get
            {
                return this._Affinity_Partner.Entity;
            }
        }
        #endregion

    }
}
