﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq.Mapping;
using System.Data.Linq;

namespace LMS.Data
{
    public partial class User
    {

        public string FullName 
        { 
            get 
            {
                string name = _Last_Name + ", " + _First_Name;

                if (_Last_Name.StartsWith("--"))
                    name = _Last_Name;

                if (_Last_Name.StartsWith("Unassign"))
                    name = _Last_Name;

                return name; 
            } 
        }

    }
}
