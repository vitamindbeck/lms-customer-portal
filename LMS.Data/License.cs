﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq.Mapping;
using System.Data.Linq;

namespace LMS.Data
{
    public partial class License
    {
        private LMSDataContext _db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
        private EntityRef<Insurance_Line_Lookup> _Insurance_Line_Lookup;
        private EntityRef<Agent> _Agent;


        [Association(Name = "Insurance_Line_Lookup", Storage = "_Insurance_Line_Lookup", ThisKey = "Line_Of_Insurance_ID", OtherKey = "Line_Of_Insurance_ID", IsUnique = true, IsForeignKey = false)]
        public Insurance_Line_Lookup Insurance_Line_Lookup
        {
            get
            {
                return this._Insurance_Line_Lookup.Entity;
            }
        }

        [Association(Name = "Agent", Storage = "_Agent", ThisKey = "Agent_ID", OtherKey = "Agent_ID", IsUnique = true, IsForeignKey = false)]
        public Agent Agent
        {
            get
            {
                return this._Agent.Entity;
            }
        }
    }
}
