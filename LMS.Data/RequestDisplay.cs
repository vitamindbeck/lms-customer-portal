﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMS.Data
{
    public class RequestDisplay
    {
        public int Record_ID { get; set; }
        public DateTime? Requested_On { get; set; }
        public string DropDownName { get; set; }
        public string Name { get; set; }
        public string Question { get; set; }
        public int To_Employee_ID { get; set; }
    }
}
