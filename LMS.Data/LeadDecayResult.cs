﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMS.Data
{
    public class LeadDecayResult
    {
        public int Number { get; set; }
        public int AppsSent { get; set; }
        public int AppsRec { get; set; }
        public int AppsPlaced { get; set; }
    }
}
