﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMS.Data
{
    public class LeadDecayDetail
    {
        public string Customer { get; set; }
        public string Status { get; set; }
        public string Source_Name { get; set; }
        public string Agent { get; set; }
        public int NumberOfAttemptedContacts { get; set; }
        public int MinutesToContact { get; set; }
        public int Apps_Sent { get; set; }
        public int Apps_Received { get; set; }
        public int Apps_Submitted { get; set; }

    }
}
