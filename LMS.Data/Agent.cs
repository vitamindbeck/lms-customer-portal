﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq.Mapping;
using System.Data.Linq;

namespace LMS.Data
{
    public partial class Agent
    {
        private LMSDataContext _db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);


        private EntityRef<User> _User;
        private EntityRef<User_Security_Level> _User_Security_Level;
        private EntityRef<State> _State;


        [Association(Name = "User", Storage = "_User", ThisKey = "User_ID", OtherKey = "User_ID", IsUnique = true, IsForeignKey = false)]
        public User User
        {
            get
            {
                return this._User.Entity;
            }
        }
        [Association(Name = "User_Security_Level", Storage = "_User_Security_Level", ThisKey = "User_ID", OtherKey = "User_ID", IsUnique = true, IsForeignKey = false)]
        public User_Security_Level User_Security_Level
        {
            get
            {
                return this._User_Security_Level.Entity;
            }
        }
        [Association(Name = "State", Storage = "_State", ThisKey = "State_ID", OtherKey = "State_ID", IsUnique = true, IsForeignKey = false)]
        public State State
        {
            get
            {
                return this._State.Entity;
            }
        }
    }
}
