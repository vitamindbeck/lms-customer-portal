﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq.Mapping;
using System.Data.Linq;
using System.Reflection;

namespace LMS.Data
{
    partial class LMSDataContext : System.Data.Linq.DataContext
    {
        [Function(Name = "dbo.Search_Records")]
        public ISingleResult<Record> Search_Records(
                    [Parameter(Name = "FirstName", DbType = "VarChar(32)")] string firstName,
                    [Parameter(Name = "LastName", DbType = "VarChar(32)")] string lastName,
                    [Parameter(Name = "SourceCode", DbType = "Int")] System.Nullable<int> sourceCode,
                    [Parameter(Name = "AgentID", DbType = "Int")] System.Nullable<int> agentID,
                    [Parameter(Name = "AssistantAgentID", DbType = "Int")] System.Nullable<int> assistantAgentID,
                    [Parameter(Name = "PhoneNumber", DbType = "VarChar(32)")] string phoneNumber,
                    [Parameter(Name = "RecordStatus", DbType = "Int")] System.Nullable<int> recordStatus,
                    [Parameter(Name = "StateID", DbType = "Int")] System.Nullable<int> stateID,
                    [Parameter(Name = "ZipCode", DbType = "VarChar(32)")] string zipCode,
                    [Parameter(Name = "CreatedOnFrom", DbType = "VarChar(32)")] string createdOnFrom,
                    [Parameter(Name = "CreatedOnTo", DbType = "VarChar(32)")] string createdOnTo,
                    [Parameter(Name = "BuyingPeriodID", DbType = "Int")] System.Nullable<int> buyingPeriodID,
                    [Parameter(Name = "FollowUpFrom", DbType = "VarChar(32)")] string followUpFrom,
                    [Parameter(Name = "FollowUpTo", DbType = "VarChar(32)")] string followUpTo,
                    [Parameter(Name = "BuyingPeriodSubID", DbType = "Int")] System.Nullable<int> buyingPeriodSubID,
                    [Parameter(Name = "EmailAddress", DbType = "VarChar(64)")] string emailAddress,
                    [Parameter(Name = "FollowUpAgent", DbType = "Int")] System.Nullable<int> FollowUpAgent,
                    [Parameter(Name = "ContactName", DbType = "VarChar(32)")] string ContactName,
                    [Parameter(Name = "ContactEmail", DbType = "VarChar(100)")] string ContactEmail,
                    [Parameter(Name = "ContactPhone", DbType = "VarChar(32)")] string ContactPhone,
                    [Parameter(Name = "PlanStatus", DbType = "Int")] System.Nullable<int> planStatus,
                    [Parameter(Name = "Customer_ID", DbType = "VarChar(32)")] string Customer_ID,
                    [Parameter(Name = "BadTypes", DbType = "Bit")] bool BadTypes,
                    [Parameter(Name = "PlanSrcCodeID", DbType = "Int")] System.Nullable<int> PlanSrcCodeID,
                    [Parameter(Name = "ReconType", DbType = "Int")] System.Nullable<int> ReconType,
                    [Parameter(Name = "TranDateTypeID", DbType = "Int")] System.Nullable<int> TranDateTypeID,
                    [Parameter(Name = "OpenActivities", DbType = "Bit")] bool OpenActivities,
                    [Parameter(Name = "DateTypeFrom", DbType = "VarChar(32)")] string DateTypeFrom,
                    [Parameter(Name = "DateTypeTo", DbType = "VarChar(32)")] string DateTypeTo,
                    [Parameter(Name = "RecordActivityTypeID", DbType = "Int")]System.Nullable<int> ActivityTypeID,
                    [Parameter(Name = "RecordEligibilityID", DbType = "Int")]System.Nullable<int> RecordEligibilityID,
                    [Parameter(Name = "ActivityStatusID", DbType = "Int")]System.Nullable<int> ActivityStatusID,
                    [Parameter(Name = "CurrentSituationID", DbType = "Int")]System.Nullable<int> CurrentSituationID,
                    [Parameter(Name = "PriorityID", DbType = "Int")]System.Nullable<int> PriorityID,
                    [Parameter(Name = "DistributionList", DbType = "VarChar(50)")] string DistributionList
                   
            )
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), firstName, lastName, 
                sourceCode, agentID, assistantAgentID, phoneNumber, recordStatus, stateID, zipCode, createdOnFrom, createdOnTo, buyingPeriodID, 
                followUpFrom, followUpTo, buyingPeriodSubID, emailAddress, FollowUpAgent, ContactName, ContactEmail, ContactPhone, planStatus,
                Customer_ID, BadTypes, PlanSrcCodeID, ReconType, TranDateTypeID, OpenActivities, DateTypeFrom, DateTypeTo, ActivityTypeID, RecordEligibilityID,
                ActivityStatusID, CurrentSituationID, PriorityID, DistributionList);
            return ((ISingleResult<Record>)(result.ReturnValue));
        }

         [Function(Name = "dbo.Quick_Search_Records")]
        public ISingleResult<Record> Quick_Search_Records(
                    [Parameter(Name = "FirstName", DbType = "VarChar(32)")] string firstName,
                    [Parameter(Name = "MiddleName", DbType = "VarChar(32)")] string middleName,
                    [Parameter(Name = "LastName", DbType = "VarChar(32)")] string lastName,
                    [Parameter(Name = "StateID", DbType = "Int")] System.Nullable<int> stateID,
                    [Parameter(Name = "DOB", DbType = "VarChar(32)")] string dob,
                    [Parameter(Name = "EmailAddress", DbType = "VarChar(64)")] string emailAddress
                    
            )
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), firstName, middleName,lastName, stateID, dob, emailAddress);
            return ((ISingleResult<Record>)(result.ReturnValue));
        }

        public void CreatePlanTransactionDates(int PlanTransactionID)
        {
            ExecuteQuery<int>(@"EXEC Create_Plan_Transaction_Dates {0}", PlanTransactionID);
        }

        [Function(Name = "dbo.sp_LoginInfo_List")]
        public ISingleResult<sp_LoginInfo_ListResult> sp_LoginInfo_List([Parameter(Name = "Agent_ID", DbType = "Int")] System.Nullable<int> agent_ID, [Parameter(Name = "Carrier_ID", DbType = "Int")] System.Nullable<int> carrier_ID, [Parameter(Name = "Non_Carrier_ID", DbType = "Int")] System.Nullable<int> non_Carrier_ID, [Parameter(Name = "State_ID", DbType = "Int")] System.Nullable<int> state_ID)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), agent_ID, carrier_ID, non_Carrier_ID, state_ID);
            return ((ISingleResult<sp_LoginInfo_ListResult>)(result.ReturnValue));
        }
       
    }

    public partial class sp_LoginInfo_ListResult
    {

        private string _First_Name;

        private string _Last_Name;

        private int _User_Password_ID;

        private string _Carrier_Site_Link;

        private string _User_Name;

        private string _Agency_Name;

        private string _Password;

        private System.Nullable<System.DateTime> _Last_Updated_Date;

        private System.Nullable<int> _Updated_By;

        private string _Name;

        private string _Abbreviation;

        public sp_LoginInfo_ListResult()
        {
        }

        [Column(Storage = "_First_Name", DbType = "VarChar(32)")]
        public string First_Name
        {
            get
            {
                return this._First_Name;
            }
            set
            {
                if ((this._First_Name != value))
                {
                    this._First_Name = value;
                }
            }
        }

        [Column(Storage = "_Last_Name", DbType = "VarChar(32)")]
        public string Last_Name
        {
            get
            {
                return this._Last_Name;
            }
            set
            {
                if ((this._Last_Name != value))
                {
                    this._Last_Name = value;
                }
            }
        }

        [Column(Storage = "_Agency_Name", DbType = "VarChar(45)")]
        public string Agency_Name
        {
            get
            {
                return this._Agency_Name;
            }
            set
            {
                if ((this._Agency_Name != value))
                {
                    this._Agency_Name = value;
                }
            }
        }

        [Column(Storage = "_User_Password_ID", DbType = "Int NOT NULL")]
        public int User_Password_ID
        {
            get
            {
                return this._User_Password_ID;
            }
            set
            {
                if ((this._User_Password_ID != value))
                {
                    this._User_Password_ID = value;
                }
            }
        }

        [Column(Storage = "_Carrier_Site_Link", DbType = "VarChar(100)")]
        public string Carrier_Site_Link
        {
            get
            {
                return this._Carrier_Site_Link;
            }
            set
            {
                if ((this._Carrier_Site_Link != value))
                {
                    this._Carrier_Site_Link = value;
                }
            }
        }

        [Column(Storage = "_User_Name", DbType = "VarChar(MAX)")]
        public string User_Name
        {
            get
            {
                return this._User_Name;
            }
            set
            {
                if ((this._User_Name != value))
                {
                    this._User_Name = value;
                }
            }
        }

        [Column(Storage = "_Password", DbType = "VarChar(MAX)")]
        public string Password
        {
            get
            {
                return this._Password;
            }
            set
            {
                if ((this._Password != value))
                {
                    this._Password = value;
                }
            }
        }

        [Column(Storage = "_Last_Updated_Date", DbType = "DateTime")]
        public System.Nullable<System.DateTime> Last_Updated_Date
        {
            get
            {
                return this._Last_Updated_Date;
            }
            set
            {
                if ((this._Last_Updated_Date != value))
                {
                    this._Last_Updated_Date = value;
                }
            }
        }

        [Column(Storage = "_Updated_By", DbType = "Int")]
        public System.Nullable<int> Updated_By
        {
            get
            {
                return this._Updated_By;
            }
            set
            {
                if ((this._Updated_By != value))
                {
                    this._Updated_By = value;
                }
            }
        }

        [Column(Storage = "_Name", DbType = "VarChar(100)")]
        public string Name
        {
            get
            {
                return this._Name;
            }
            set
            {
                if ((this._Name != value))
                {
                    this._Name = value;
                }
            }
        }

        [Column(Storage = "_Abbreviation", DbType = "VarChar(100)")]
        public string Abbreviation
        {
            get
            {
                return this._Abbreviation;
            }
            set
            {
                if ((this._Abbreviation != value))
                {
                    this._Abbreviation = value;
                }
            }
        }
    }
}
