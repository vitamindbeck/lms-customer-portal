﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMS.Data
{
    partial class Plan_Document
    {
        private LMSDataContext _db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

        public State State
        {
            get 
            { 
                State val = _db.States.First(s => s.State_ID == _State_ID);
                if (val != null)
                {
                    return val;
                }
                else
                {
                    return _db.States.First(s => s.State_ID == 52);
                }
            }
        }
    }
}
