﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMS.Data
{
    public class FollowUpDataType
    {
        public int RecordID { get; set; }
        public string LeadName { get; set; }
        public DateTime DueDate { get; set; }
        public string State { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailAddress { get; set; }
        public int Priority { get; set; }
        public string Status { get; set; }
        public int RecordActivityID { get; set; }
        public string SourceCode { get; set; }
        public string NumberOfContacts { get; set; }
    }
}
