﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq.Mapping;
using System.Data.Linq;
namespace LMS.Data
{
    public partial class Certification
    {
        private LMSDataContext _db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

        private EntityRef<Carrier> _Carrier;
        private EntityRef<Certification_Status_Lookup> _Certification_Status_Lookup;
        private EntityRef<Certification_Type_Lookup> _Certification_Type_Lookup;


        [Association(Name = "Carrier", Storage = "_Carrier", ThisKey = "Carrier_ID", OtherKey = "Carrier_ID", IsUnique = true, IsForeignKey = false)]
        public Carrier Carrier
        {
            get
            {
                return this._Carrier.Entity;
            }
        }


        [Association(Name = "Certification_Status_Lookup", Storage = "_Certification_Status_Lookup", ThisKey = "Certification_Status_ID", OtherKey = "Certification_Status_ID", IsUnique = true, IsForeignKey = false)]
        public Certification_Status_Lookup Certification_Status_Lookup
        {
            get
            {
                return this._Certification_Status_Lookup.Entity;
            }
        }
        [Association(Name = "Certification_Type_Lookup", Storage = "_Certification_Type_Lookup", ThisKey = "Certification_Type_ID", OtherKey = "Certification_Type_ID", IsUnique = true, IsForeignKey = false)]
        public Certification_Type_Lookup Certification_Type_Lookup
        {
            get
            {
                return this._Certification_Type_Lookup.Entity;
            }
        }
    }
}
