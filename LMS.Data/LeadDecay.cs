﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMS.Data
{
    public class LeadDecay
    {
        public int Agent_ID { get; set; }
        public int Source_Code_ID { get; set; }
        public int Group_ID { get; set; }
        public string Source_Name { get; set; }
        public int NumberOfAttemptedContacts { get; set; }
        public int MinutesToContact { get; set; }
        public int Apps_Sent { get; set; }
        public int Apps_Received { get; set; }
        public int Apps_Submitted { get; set; }

    }
}
