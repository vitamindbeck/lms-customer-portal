﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMS.Data
{
    public class SupportDisplayType
    {
        public DateTime? RequestDate { get; set; }
        public string RecordName { get; set; }
        public int Record_ID { get; set; }
        public int Plan_Transaction_ID { get; set; }
        public int Record_Activity_ID { get; set; }
        public string SourceCode { get; set; }
        public string Carrier { get; set; }
        public string State { get; set; }
        public string Plan_Transaction_Status { get; set; }
        public string Record_Activity_Type { get; set; }
        public bool PotentialDuplicate { get; set; }
        public string OutboundShippingMethod { get; set; }
        public string RequestedBy { get; set; }
    }
}
