﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMS.Data.Repository
{
	public interface ICarrierRepository
	{
        IEnumerable<Carrier> GetAll();
        Carrier Get(int CarrierID);
        void Insert(Carrier carrier);
        void Delete(Carrier carrier);
        void Save();
	}
}
