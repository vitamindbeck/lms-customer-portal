﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq.Mapping;
using System.Data.Linq;
using System.Net.Mail;


namespace LMS.Data.Repository
{
    public class UserRepository : IUserRepository
    {
        private LMSDataContext _db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

        public User GetUser(int User_ID)
        {
            return _db.Users.SingleOrDefault(user => user.User_ID == User_ID);
        }

        public User GetUserFromADName(string name)
        {
            return _db.Users.SingleOrDefault(user => user.User_Name == name);
        }

        public IQueryable<User> GetActiveAgents()
        {
            return _db.Users.Where(user => user.Active == true && (user.Security_Level_ID.Equals(1) || user.Security_Level_ID.Equals(7) || user.Security_Level_ID.Equals(4) || user.Security_Level_ID.Equals(2))).OrderBy(a => a.Last_Name);
        }

        public void Update(User User)
        {
            _db.Users.InsertOnSubmit(User);

        }

       

        public void Delete(User User)
        {
            _db.Users.DeleteOnSubmit(User);
        }

        
        //Send email to notify user that account has been created.
        public void SendEmailDelete(User usr, string loggedUserEmail)
        {

            //create mail object
            MailMessage msg = new MailMessage();
            msg.Subject = "User Account Deleted";

            string body = "Please note that following user account has been deleted.\n\n";
            body += "First Name: " + usr.First_Name + "\n";
            body += "Last Name: " + usr.Last_Name + "\n";
            body += "User Name: " + usr.User_Name + "\n";
            body += "Email: " + usr.Email_Address + "\n\n"; 
           

            body += "Thanks,\n";

            body += "IT";

            msg.Body = body;


            msg.To.Add(new MailAddress(loggedUserEmail));

            msg.From = new MailAddress("it@longevityalliance.com");

            SmtpClient smt = new SmtpClient("192.168.1.202");

            smt.Send(msg);
        }

     
       //Send email to notify user that account has been update.
       public void SendEmailUpdate(User usr, string loggedUserEmail)
       {

           //create mail object
           MailMessage msg = new MailMessage();
           msg.Subject = "User Account Updated";


           string body = "Please note that following user account has been updated.\n\n";
           body += "First Name: " + usr.First_Name + "\n";
           body += "Last Name: " + usr.Last_Name + "\n";
           body += "User Name: " + usr.User_Name + "\n";
           body += "Email: " + usr.Email_Address + "\n\n"; 


           body += "Thanks,\n";

           body += "IT";

           msg.Body = body;


           msg.To.Add(new MailAddress(loggedUserEmail));

           msg.From = new MailAddress("it@longevityalliance.com");

           SmtpClient smt = new SmtpClient("192.168.1.202");

           smt.Send(msg);
       }

        public void Save()
        {
            _db.SubmitChanges();
        }

        public User GetUserByUserName(string name)
        {
            return _db.Users.Where(user => user.User_Name == name.Replace(@"LONGEVITY\", "")).FirstOrDefault();
        }

        public static User GetUser(string name)
        {
            LMSDataContext DB = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            return DB.Users.Where(user => user.User_Name == name.Replace(@"LONGEVITY\","")).FirstOrDefault();
        }

        public static User GetUserByID(int id)
        {
            LMSDataContext DB = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

            return DB.Users.Where(user => user.User_ID.Equals(id)).FirstOrDefault();
        }

        public static string GetUserName(string name)
        {
            return name.Replace(@"LONGEVITY\", "");
        }

        //Update user account by Administrator. Get the user information based on user name.
        public List<User> UserSearch(int User_ID)
        {
            try
            {
               LMSDataContext DB = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
               return DB.Users.Where(u => u.User_ID == User_ID).ToList();
            }
            catch (Exception ex)
            {
                LMS.ErrorHandler.HandleError.EmailError(ex);
                return null;
            }

        }

        public void UpdateUserOnlineStatus(string username, int statusid)
        {
            User u = GetUserByUserName(username);
            if (u != null)
            {
                u.Online_Status_ID = (short)statusid;
                _db.SubmitChanges();
            }
        }

        public bool HasSecurityLevel(int User_ID, int Security_Level_ID)
        {
            LMSDataContext DB = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            User_Security_Level user_security_level = new User_Security_Level();
            user_security_level = DB.User_Security_Levels.Where(usl => usl.User_ID == User_ID && usl.Security_Level_ID == Security_Level_ID).FirstOrDefault();
            if (user_security_level == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public static bool HasSecurityLevel(User user, int Security_Level_ID)
        {
            LMSDataContext DB = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            User_Security_Level user_security_level = new User_Security_Level();
            user_security_level = DB.User_Security_Levels.Where(usl => usl.User_ID == user.User_ID && usl.Security_Level_ID == Security_Level_ID).FirstOrDefault();
            if (user_security_level == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
