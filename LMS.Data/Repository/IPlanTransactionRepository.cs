﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMS.Data.Repository
{
    public interface IPlanTransactionRepository
    {
        Plan_Transaction Get(int Plan_Transaction_ID);
        List<Plan_Transaction> GetRecordPlans(int Record_ID);
        void Insert(Plan_Transaction transaction);
        void Update(Plan_Transaction transaction);
        void Delete(Plan_Transaction transaction);
        void Save();
        List<Transaction_Date_Type> GetDateTypes(int GroupID);
        void UpdateSendAppDate(int Plan_Transaction_ID);
        void UpdateSendInfoDate(int Plan_Transaction_ID);
        void UpdateAppCancelledDate(int Plan_Transaction_ID);
        void UpdatePreFillDate(int Plan_Transaction_ID);
        void UpdateAppSentDateAndAppRequestDate(int Plan_Transaction_ID);
    }
}
