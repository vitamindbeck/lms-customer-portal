﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMS.Data.Repository
{
   public class AppointmentRepository :IAppointmentRepository
    {
        LMSDataContext _db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

        public Appointment GetAppointmentByID(int Appointment_ID)
        {
            return _db.Appointments.Where(l => l.Appointment_ID == Appointment_ID).FirstOrDefault();
        }
    }
}
