﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMS.Data.Repository
{
    public class RecordRepository : IRecordRepository
    {
        LMSDataContext _db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
        PlanTransactionRepository tranRepository = new PlanTransactionRepository();
        ActivityRepository activtyRepository = new ActivityRepository();

        #region IRecordRepository Members

        public List<NewLeadsResult> GetNewLeads(int Agent_ID, int Security_Level)
        {
            _db.CommandTimeout = 0;
            return _db.NewLeads(Agent_ID, Security_Level).ToList();
        }

        //Get New leads by pods
        public List<NewLeadsByPodsResult> GetNewLeadsByPods(int Pod_ID, int Security_Level)
        {
            _db.CommandTimeout = 0;
            return _db.NewLeadsByPods(Pod_ID, Security_Level).ToList();

        }

        public List<FollowUpsResult> GetAgentFollowUps(int Agent_ID)
        {
            return _db.FollowUps(Agent_ID).ToList();
        }

        //Get FollowUps by Pods
        public List<FollowUpsByPodsResult> GetAgentFollowUpsByPods(int Pod_ID)
        {
            return _db.FollowUpsByPods(Pod_ID).ToList();
        }

        public List<InitialApplicationSentFollowUpResult> GetInitialApplicationSentFollowUps()
        {
            return _db.InitialApplicationSentFollowUp().ToList();
        }

        public List<ApplicationSentFollowUpAfterInitialResult> GetApplicationSentFollowUpsAfterInitial()
        {
            return _db.ApplicationSentFollowUpAfterInitial().ToList();
        }

        public Record GetRecord(int Record_ID)
        {
            return _db.Records.FirstOrDefault(rec => rec.Record_ID == Record_ID);
        }

        public User GetUserByID(int userID)
        {

            return _db.Users.SingleOrDefault(x => x.User_ID == userID);

        }

        public void Update(Record record)
        {
            _db.Records.InsertOnSubmit(record);
            _db.SubmitChanges();
            record.Customer_ID = "LMS" + record.Record_ID.ToString();
            _db.SubmitChanges();
        }

        public void Insert(Record record)
        {
            _db.Records.InsertOnSubmit(record);
            _db.SubmitChanges();
            _db.Insert_Record_Assigned(record.Record_ID, record.Agent_ID);
            record.Customer_ID = "LMS" + record.Record_ID.ToString();
            _db.SubmitChanges();

            Plan_Transaction plan = new Plan_Transaction();

            // Set Plan transaction variables
            plan.Active = false;
            plan.Agent_ID = record.Agent_ID;
            plan.Annual_Premium = 0;
            plan.Assistant_Agent_ID = 49;
            plan.Best_Time_For_Exam = "";
            plan.Cash_With_Application = false;
            plan.Cash_With_Application_Amount = 0;
            plan.Cash_With_Application_Check = false;
            plan.Comments = "";
            plan.Coverage_Amount = 0;
            plan.Created_On = DateTime.Now;
            plan.Deposit_Amount = 0;
            plan.eApp = false;
            plan.PreFillApp = false;
            plan.Exam_Details = "";
            plan.Exam_Ref_Number = "";
            plan.Exam_Service_ID = -1;
            plan.GBS_ID = "";
            plan.Group_ID = _db.Source_Codes.First(sc => sc.Source_Code_ID == record.Source_Code_ID).Group_ID;
            plan.Guaranteed_Issue = false;
            plan.Inbound_Shipping_Method_ID = -1;
            plan.Initial_Enrollment = false;
            plan.Modal_ID = -1;
            plan.Modal_Premium = 0;
            plan.Mode_ID = -1;
            plan.Non_Qualified_1035_Exchange = false;
            plan.Outbound_Shipping_Method_ID = -1;
            plan.Parent_Plan_ID = -1;
            plan.Payment_Mode_ID = -1;
            plan.Plan_ID = -1;
            plan.Plan_Transaction_Status_ID = 17;
            plan.Qualified_Transfer = false;
            plan.Record_ID = record.Record_ID;
            plan.Replacement = false;
            plan.Replacement_Details = "";
            plan.Source_Code_ID = record.Source_Code_ID;
            plan.Special_Instructions = "";
            plan.Tracking_Number_Inbound = "";
            plan.Tracking_Number_Outbound = "";
            plan.Transaction_Number = -1;
            plan.Underwriting_Class_ID = -1;

            tranRepository.Insert(plan);
        }

        public void Delete(Record record)
        {
            _db.Records.DeleteOnSubmit(record);
        }

        public void UpdateCompany(Company Company)
        {
            _db.Companies.InsertOnSubmit(Company);
        }

        public void DeleteCompany(Company Company)
        {
            _db.Companies.DeleteOnSubmit(Company);
        }

        public void Save()
        {
            _db.SubmitChanges();
        }

        public Company GetCompany()
        {
            return new Company();
        }

        public List<Record> Search(string FirstName, string LastName, int SourceCode,
            int AgentID, int AssistantAgentID, string PhoneNumber, int RecordStatus, int StateID,
            string ZipCode, string CreatedOnFrom, string CreatedOnTo, int BuyingPeriodID,
            string FollowUpFrom, string FollowUpTo, int BuyingPeriodSubID, string EmailAddress, int FollowUpAgentID,
            string ContactName, string ContactEmail, string ContactPhone, int PlanStatus, string Customer_ID, bool BadTypes, int PlanSrcCodeID, int ReconcilliationType,
            int TransactionDateType, bool OpenActivities, string TransactionDateFromValue, string TransactionDateToValue, int ActivityTypeID,
            int RecordEligibilityID, int ActivityStatusID, int CurrentSituationID, int PriorityID, string DistributionListIds)
        {
            try
            {
                var list = _db.Search_Records(FirstName, LastName, (int?)SourceCode, (int?)AgentID, (int?)AssistantAgentID, PhoneNumber, (int?)RecordStatus, (int?)StateID, ZipCode, CreatedOnFrom, CreatedOnTo, (int?)BuyingPeriodID, FollowUpFrom, FollowUpTo, (int?)BuyingPeriodSubID, EmailAddress, (int?)FollowUpAgentID, ContactName, ContactEmail, ContactPhone, (int?)PlanStatus, Customer_ID, BadTypes, (int?)PlanSrcCodeID, (int?)ReconcilliationType, (int?)TransactionDateType, OpenActivities, TransactionDateFromValue, TransactionDateToValue, (int?)ActivityTypeID, (int?)RecordEligibilityID, (int?)ActivityStatusID, (int?)CurrentSituationID, (int?)PriorityID, DistributionListIds).ToList();


                return list;
            }
            catch (Exception ex)
            {
                LMS.ErrorHandler.HandleError.EmailError(ex);
                return null;
            }

        }
        //Quick Search

        public List<Record> QuickSearch(string FirstName, string MiddleName, string LastName, int StateID, string DOB, string EmailAddress)
        {
            try
            {
                var quickList = _db.Quick_Search_Records(FirstName, MiddleName, LastName, StateID, DOB, EmailAddress).ToList();


                return quickList;
            }
            catch (Exception ex)
            {
                LMS.ErrorHandler.HandleError.EmailError(ex);
                return null;
            }

        }



        public List<UnassignedLeadsResult> GetUnassignedLeads()
        {
            return _db.UnassignedLeads().ToList();
            //return _db.Records.Where(record => record.Agent_ID == 49 && record.Record_Type_ID == 1).OrderBy(rec => rec.Created_On);
        }

        public List<MyUnassignedLeadsResult> GetMyUnassinedLeads(string username)
        {
            return _db.MyUnassignedLeads(username).ToList();
        }

        public List<OtherUnassignedLeadsResult> GetOtherUnassinedLeads(string username)
        {
            return _db.OtherUnassignedLeads(username).ToList();
        }

        public List<sp_Get_Record_Distribution_ListResult> GetRecordDistributionList(int Record_ID)
        {
            return _db.sp_Get_Record_Distribution_List(Record_ID).ToList();
        }

        public void AssignLead(int Record_ID, int Agent_ID)
        {
            Record rec = _db.Records.SingleOrDefault(r => r.Record_ID == Record_ID);

            rec.Agent_ID = Agent_ID;
            rec.Record_Type_ID = 1;
            rec.Agent_Assigned_Date = DateTime.Now;

            Plan_Transaction tran = _db.Plan_Transactions.Where(pt => pt.Record_ID == Record_ID).FirstOrDefault();
            if (tran != null)
                tran.Agent_ID = rec.Agent_ID;

            _db.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
            //_db.Insert_Record_Assigned(Record_ID, Agent_ID);
        }

        public void AssignSalesCoordinator(int Record_ID, string User_Name)
        {
            Record rec = _db.Records.SingleOrDefault(r => r.Record_ID == Record_ID);
            rec.Assistant_Agent_ID = _db.Users.SingleOrDefault(u => u.User_Name == User_Name).User_ID;
            _db.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
        }

        public List<GetLeadProspectResult> GetProspects(int Agent_ID)
        {
            //var prospects = from rec in _db.Records
            //                join act in _db.Record_Activities
            //                on rec.Record_ID equals act.Record_ID
            //                where rec.Agent_ID == Agent_ID && rec.Record_Type_ID == 3 && act.Actual_Date.HasValue==false
            //                select rec;
            var ps = _db.GetLeadProspect(Agent_ID, 3).ToList();

            // return _db.Records.Where(r => r.Agent_ID == Agent_ID && r.Record_Type_ID == 3).OrderBy(r => r.Created_On);
            return ps;
        }

        public List<GetLeadProspectResult> GetLeads(int Agent_ID)
        {
            //var records = from r in _db.Records
            //                where r.Agent_ID == Agent_ID &&
            //                    r.Record_Type_ID == 2 &&
            //                    (
            //                        from ra in _db.Record_Activities
            //                        select ra.Record_ID
            //                    ).Contains(r.Record_ID)
            //                orderby 
            //                    r.Created_On.Value
            //                select r;

            var ps = _db.GetLeadProspect(Agent_ID, 2).ToList();

            return ps;
        }

        public Record GetPartner(int Original_Record_ID)
        {
            Record originalrecord = GetRecord(Original_Record_ID);
            Record record = new Record();

            //Blank Input Fields
            record.Address_1 = originalrecord.Address_1;
            record.Address_2 = originalrecord.Address_2;
            record.City = originalrecord.City;
            record.State_ID = originalrecord.State_ID;
            record.Zipcode = originalrecord.Zipcode;
            record.County = originalrecord.County;
            record.IsMailing_Address = originalrecord.IsMailing_Address;
            record.Mailing_Address_1 = originalrecord.Mailing_Address_1;
            record.Mailing_Address_2 = originalrecord.Mailing_Address_2;
            record.Mailing_City = originalrecord.Mailing_City;
            record.Mailing_County = originalrecord.Mailing_County;
            record.Mailing_State_ID = originalrecord.Mailing_State_ID;
            record.Alt_Address_1 = originalrecord.Alt_Address_1;
            record.Alt_Address_2 = originalrecord.Alt_Address_2;
            record.Alt_City = originalrecord.Alt_City;
            record.Alt_State_ID = originalrecord.Alt_State_ID;
            record.Alt_Zipcode = originalrecord.Alt_Zipcode;
            record.Alt_Address_From_Date = originalrecord.Alt_Address_From_Date;
            record.Alt_Address_To_Date = originalrecord.Alt_Address_To_Date;
            record.Home_Phone = originalrecord.Home_Phone;
            record.Momentum = originalrecord.Momentum;
            record.Record_Type_ID = originalrecord.Record_Type_ID;
            record.Reffered_By = originalrecord.Reffered_By;
            record.Broker_ID = originalrecord.Broker_ID;
            record.Referral_Type_ID = originalrecord.Referral_Type_ID;
            record.Reffered_By = originalrecord.Reffered_By;
            record.Lead_Quality = originalrecord.Lead_Quality;
            record.Buying_Period_ID = originalrecord.Buying_Period_ID;
            record.Buying_Period_Sub_ID = originalrecord.Buying_Period_Sub_ID;
            record.Priority_Level = originalrecord.Priority_Level;
            record.Agent_ID = originalrecord.Agent_ID;
            record.Assistant_Agent_ID = originalrecord.Assistant_Agent_ID;
            record.Source_Code_ID = originalrecord.Source_Code_ID;
            record.Marital_Status_ID = originalrecord.Marital_Status_ID;
            record.Notes = originalrecord.Notes;
            record.Last_Name = originalrecord.Last_Name;
            record.Partner_ID = originalrecord.Record_ID;
            record.Created_On = originalrecord.Created_On;

            if (originalrecord.Sex == "M")
            {
                record.Sex = "F";
            }
            else
            {
                if (originalrecord.Sex == "F")
                {
                    record.Sex = "M";
                }
            }

            return record;
        }

        public void BadLead(int Record_ID)
        {
            Record record = GetRecord(Record_ID);
            record.Record_Type_ID = 11;
            _db.SubmitChanges();
        }

        public void DuplicateLead(int Record_ID)
        {
            Record record = GetRecord(Record_ID);
            record.Record_Type_ID = 12;
            _db.SubmitChanges();
        }
        public void RepeatLead(int Record_ID)
        {
            Record record = GetRecord(Record_ID);
            record.Record_Type_ID = 16;
            _db.SubmitChanges();
        }

        public List<SupportDisplayType> GetSupportLeads(int Agent_ID)
        {
            // Define Return Type
            List<SupportDisplayType> list = new List<SupportDisplayType>();

            // Get Plan Transactions for agent with status either Request Application or Request Info/Quote
            var transactions = _db.Plan_Transactions.Where(pt => (pt.Plan_Transaction_Status_ID == 20 || pt.Plan_Transaction_Status_ID == 21));//&& pt.Group_ID != 3)

            foreach (Plan_Transaction transaction in transactions)
            {
                // Get Record
                Record record = GetRecord(transaction.Record_ID);
                //Record_Activity recordActivity = activtyRepository.GetActivity(transaction.Record_ID);

                User user = GetUserByID((int)transaction.Agent_ID);

                if (record != null)
                {
                    // Create New Display Type Object
                    SupportDisplayType display = new SupportDisplayType();

                    if (transaction.Plan == null)
                    {
                        display.Carrier = "None";
                    }
                    else
                    {
                        display.Carrier = transaction.Plan.Carrier.Name;
                    }
                    display.Plan_Transaction_ID = transaction.Plan_Transaction_ID;
                    display.Plan_Transaction_Status = transaction.Plan_Transaction_Status.Description;
                    display.Record_Activity_Type = "";
                    display.Record_ID = record.Record_ID;
                    display.RecordName = record.Last_Name + ", " + record.First_Name;
                    //display.PotentialDuplicate = IsPotentialDuplicate(record);

                    //// If Plan Transtion Status is Application Sent Set Date else Info Date
                    //if (transaction.Plan_Transaction_Status_ID == 20)
                    //{
                    //    display.RequestDate = transaction.Plan_Dates.FirstOrDefault(d => d.Transaction_Date_Type_ID == 18).Date_Value;
                    //}
                    //else
                    //{
                    //    display.RequestDate = transaction.Plan_Dates.FirstOrDefault(d => d.Transaction_Date_Type_ID == 19).Date_Value;
                    //}

                    display.RequestDate = transaction.Time_Stamp;

                    display.OutboundShippingMethod = (transaction.Outbound_Shipping_Method == null ? "" : transaction.Outbound_Shipping_Method.Description);
                    display.SourceCode = record.Source_Code.Source_Number;
                    display.State = record.State.Abbreviation;
                    //if (user.First_Name)
                    //display.RequestedBy = user.First_Name.Trim().ToUpper()[0] + ". " + user.Last_Name.Trim();
                    display.RequestedBy = user.First_Name + ". " + user.Last_Name;

                    // Add to list
                    list.Add(display);
                }
            }

            // Return List
            return list;

        }

        private bool IsPotentialDuplicate(Record record)
        {
            var dups = from r in _db.Records
                       where r.First_Name.Equals(record.First_Name) && r.Last_Name.Equals(record.Last_Name)
                       select r;

            return dups.Count() > 0;
        }

        public List<SupportDisplayType> GetFulfillmentActivities(int User_ID)
        {
            // Define Return Type
            List<SupportDisplayType> list = new List<SupportDisplayType>();

            // Get All Incomplete Activites

            var activities = _db.Record_Activities.Where(pt => (pt.Actual_Date.HasValue == false && pt.User_ID == User_ID && pt.Due_Date < DateTime.Now.AddDays(1)) || (pt.Activity_Type_ID == 17 && pt.Actual_Date.HasValue == false)).OrderBy(pt => pt.Due_Date);

            foreach (Record_Activity activity in activities)
            {
                // Get Record
                Record record = GetRecord(activity.Record_ID);

                //User user = GetUserByID(activity.Created_By);

                // Create New Display Type Object
                SupportDisplayType display = new SupportDisplayType();

                if (activity.Plan_Transaction.Plan == null)
                {
                    display.Carrier = "None";
                }
                else
                {
                    display.Carrier = activity.Plan_Transaction.Plan.Carrier.Name;
                }
                display.Record_Activity_ID = activity.Record_Activity_ID;
                display.Plan_Transaction_ID = activity.Plan_Transaction.Plan_Transaction_ID;
                display.Plan_Transaction_Status = activity.Plan_Transaction.Plan_Transaction_Status.Description;
                display.Record_Activity_Type = activity.Activity_Type.Description;
                display.Record_ID = record.Record_ID;
                display.RecordName = record.Last_Name + ", " + record.First_Name;

                // If Plan Transtion Status is Application Sent Set Date else Info Date
                display.RequestDate = activity.Due_Date;

                display.OutboundShippingMethod = (activity.Plan_Transaction.Outbound_Shipping_Method == null ? "" : activity.Plan_Transaction.Outbound_Shipping_Method.Description);
                display.SourceCode = record.Source_Code.DropDownName;
                display.State = record.State.Abbreviation;
                //display.RequestedBy = user.First_Name.Trim().ToUpper()[0] + ". " + user.Last_Name.Trim();
                //record.

                // Add to list
                list.Add(display);
            }

            // Return List
            return list;
        }

        public List<GetLeadProspectResult> GetLeadsByType(int Agent_ID, string Type)
        {
            Record_Type type = _db.Record_Types.FirstOrDefault(t => t.Description.Equals(Type));

            if (type != null)
            {
                return _db.GetLeadProspect(Agent_ID, type.Record_Type_ID).ToList();
            }
            else
            {
                return null;
            }
        }

        public List<ReconciliationsResult> GetReconciliations()
        {
            return _db.GetReconciliations().ToList();
        }

        public List<GetAllOpenComplaintsResult> GetAllOpenComplaints()
        {
            return _db.GetAllOpenComplaints().ToList();
        }

        public List<GetRecordActivitiesDisplayResult> GetRecordActivitiesDisplay(int Record_ID)
        {
            return _db.GetRecordActivitiesDisplay(Record_ID).ToList();
        }

        public List<sp_Get_Distribution_ListResult> GetDistributionList(int Record_ID)
        {
            return _db.sp_Get_Distribution_List(Record_ID).ToList();
        }

        public List<Get_Plan_Transaction_Desc_Status_DateResult> GetPlanTransactionDescStatusDate(int Record_ID)
        {
            return _db.Get_Plan_Transaction_Desc_Status_Date(Record_ID).ToList();
        }

        public List<AppointmentFollowUpsResult> GetAgentAppointmentFollowUps(int Agent_ID)
        {
            return _db.AppointmentFollowUps(Agent_ID).ToList();
        }
        //AppointmentFollowUp by Pods
        public List<AppointmentFollowUpsByPodsResult> GetAgentAppointmentFollowUpsByPods(int Pod_ID)
        {
            return _db.AppointmentFollowUpsByPods(Pod_ID).ToList();
        }

        public List<AppSentOpenActivitiesResult> GetAppSentOpenActivities(int Agent_ID)
        {
            return _db.AppSentOpenActivities(Agent_ID).ToList();
        }

        //AppSentOpenActivities by Pod
        public List<AppSentOpenActivitiesByPodsResult> GetAppSentOpenActivitiesByPods(int Pod_ID)
        {
            return _db.AppSentOpenActivitiesByPods(Pod_ID).ToList();
        }


        public void SetRecordType(int RecordID, int RecordTypeID)
        {
            _db.Set_Record_Type(RecordID, RecordTypeID);
        }

        public List<Report_Lead_Time_ReportResult> GetLeadTimeReport(DateTime StartDate, DateTime EndDate)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Record> GetAppointmentLeads(int User_ID)
        {
            return _db.Records.Where(r => r.Assistant_Agent_ID.Equals(User_ID) && r.Agent_ID.Equals(49));
        }

        public void AssignAppointmentSetter(int Record_ID, int Agent_ID)
        {
            Record rec = _db.Records.SingleOrDefault(r => r.Record_ID == Record_ID);

            rec.Assistant_Agent_ID = Agent_ID;

            _db.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
        }

        public IQueryable<Record> NonBillable(int Agent_ID)
        {
            var data = from r in _db.Records
                       join pt in _db.Plan_Transactions on r.Record_ID equals pt.Record_ID
                       where pt.Plan_Transaction_Status_ID.Equals(27) && pt.Agent_ID.Equals(Agent_ID)
                       select r;

            return data;
        }

        public void Delete(int Record_ID)
        {
            _db.sp_Delete_Record(Record_ID);
        }

        public Complaint GetComplaint(int Record_ID) {
            var complaint = new Complaint();
            using (LMSDataContext _db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString)) {

                return complaint = _db.Complaints.Where(x => x.Record_ID == Record_ID && x.Resolved == false).FirstOrDefault();
            }
        }

        #endregion
    }
}
