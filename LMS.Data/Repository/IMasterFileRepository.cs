﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMS.Data.Repository
{
    public interface IMasterFileRepository
    {
        List<vw_MasterFile_v2> GetFilteredData(string filterExpression, List<string> stateList, List<int> departedAgents);
    }
}
