﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMS.Data.Repository
{
    public interface IUserRepository
    {
        User GetUser(int User_ID);
        User GetUserByUserName(string name);
        User GetUserFromADName(string name);
        List<User> UserSearch(int User_ID);
        IQueryable<User> GetActiveAgents();
        void UpdateUserOnlineStatus(string username, int statusid);
        void Update(User User);
        void Delete(User User);
        void SendEmailDelete(User User, string loggedUserEmail);
        void SendEmailUpdate(User User, string loggedUserEmail);
        void Save();
        bool HasSecurityLevel(int User_ID, int Security_Level_ID);
    }
}
