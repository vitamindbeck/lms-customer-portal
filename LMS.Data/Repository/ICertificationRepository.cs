﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMS.Data.Repository
{
    public interface ICertificationRepository
    {
       Certification GetCertificationByID(int Certification_ID);
    }
}
