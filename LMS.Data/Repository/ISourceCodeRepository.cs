﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMS.Data.Repository
{
    public interface ISourceCodeRepository
    {       
        List<sp_Source_Code_ListResult> GetSourceCodeList();
        Source_Code GetSourceCode(int Source_Code_ID);
        bool InWeb(int Source_Code_ID);
        Partner GetPartner(String Source_Number, bool LMS);
        void Save();
        void Insert(Source_Code sourcecode);
        void SavePartner();
        void InsertPartner(Partner partner);
        bool UniqueSourceNumber(string Source_Number);
        bool UniqueWebName(string WebName);

    } 

}
