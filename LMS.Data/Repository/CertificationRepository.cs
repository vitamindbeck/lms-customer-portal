﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMS.Data.Repository
{

    public class CertificationRepository : ICertificationRepository
    {
        private LMSDataContext _db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

        public Certification GetCertificationByID(int Certification_ID)
        {
            Certification certification = _db.Certifications.Where(c => c.Certification_ID == Certification_ID).FirstOrDefault();
            return certification;
        }
    }
}
