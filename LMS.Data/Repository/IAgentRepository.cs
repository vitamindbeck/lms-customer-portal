﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMS.Data.Repository
{
    public interface IAgentRepository
    {
        List<sp_Agent_ListResult> GetLicenseAgentList();
        Agent GetAgentByID(int Agent_ID);
        Agent GetAgencyByID(int Associated_Agency_ID);
        Agent GetAgentByUserID(int User_ID);
        List<Agent> GetAgencyList(bool includeBlank);
        Agent GetAssociatedAgencyDetails(int Associated_Agency_ID);
    }
}
