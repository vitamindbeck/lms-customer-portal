﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMS.Data.Repository
{
    public class MasterFileRepository : IMasterFileRepository
    {
        private LMSDataContext _db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
        public List<vw_MasterFile_v2> GetFilteredData(string filterExpression, List<string> stateList, List<int> departedAgents)
        {
            _db.CommandTimeout = 3 * 60;
            var theMainList = new List<vw_MasterFile_v2>();

            if (stateList.Count > 0)
            {
                theMainList = _db.vw_MasterFile_v2s.Where(item => stateList.Contains(item.Customer_State)).ToList();

                if (!String.IsNullOrWhiteSpace(filterExpression))
                {
                    theMainList = theMainList.AsQueryable().Where(filterExpression).ToList();
                }
            }
            else if (!String.IsNullOrWhiteSpace(filterExpression))
            {
                theMainList = _db.vw_MasterFile_v2s.Where(filterExpression).ToList();
            }

            if (departedAgents.Count > 0)
            {
                theMainList = theMainList.Where(x => departedAgents.Contains((int)x.AgentFinalUserID)).ToList();
            }

            return theMainList;
        }
    }
}
