﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMS.Data.Repository
{
    public interface IActivityRepository
    {
        IQueryable<Record_Activity> GetAgentsActivities(int Agent_ID);
        Record_Activity GetActivity(int id);
        List<GetRecordActivitiesDisplayResult> GetRecordActivitiesDisplay(int Record_ID);
        void Update(Record_Activity recordActivity);
        void Delete(Record_Activity recordActitity);
        void Save();
        bool HasOpenActivities(int Record_ID);
    }
}
