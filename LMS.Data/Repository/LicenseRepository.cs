﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMS.Data.Repository
{
    public class LicenseRepository : ILicenseRepository
    {
        LMSDataContext _db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

        public License GetLicenseByID(int License_ID)
        {
           
            return _db.Licenses.Where(l => l.License_ID == License_ID).FirstOrDefault();
        }
    }
}
