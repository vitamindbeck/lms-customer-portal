﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMS.Data.Repository
{
    public class SourceCodeRepository : ISourceCodeRepository
    {
        private LMSDataContext _db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
        private WEBDataContext _webdb = new WEBDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["WEBContextConnectionString"].ConnectionString);

        public List<sp_Source_Code_ListResult> GetSourceCodeList()
        {
            return _db.sp_Source_Code_List().ToList();
        }

        public Source_Code GetSourceCode(int Source_Code_ID)
        {
            return _db.Source_Codes.FirstOrDefault(s => s.Source_Code_ID == Source_Code_ID);
        }

        public bool InWeb(int Source_Code_ID)
        {
            Source_Code sc = GetSourceCode(Source_Code_ID);
            if(sc != null)
            {
                Partner p = _webdb.Partners.FirstOrDefault(par => par.Source_Number == sc.Source_Number && par.LMS != sc.LTC);
                if (p != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public Partner GetPartner(String Source_Number, bool LMS)
        {
            Partner p = _webdb.Partners.FirstOrDefault(par => par.Source_Number == Source_Number && par.LMS == LMS);
            return p;
        }

        public void Save()
        {
            _db.SubmitChanges();
        }

        public void Insert(Source_Code sourcecode)
        {
            _db.Source_Codes.InsertOnSubmit(sourcecode);
            _db.SubmitChanges();
        }

        public void SavePartner()
        {
            _webdb.SubmitChanges();
        }

        public void InsertPartner(Partner partner)
        {
            _webdb.Partners.InsertOnSubmit(partner);
            _webdb.SubmitChanges();
        }

        public bool UniqueSourceNumber(string Source_Number)
        {
            Source_Code sourcecode = _db.Source_Codes.Where(sc => sc.Source_Number == Source_Number).FirstOrDefault();
            if (sourcecode == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool UniqueWebName(string WebName)
        {
            Partner partner = _webdb.Partners.Where(p => p.WebName == WebName).FirstOrDefault();
            if (partner == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
