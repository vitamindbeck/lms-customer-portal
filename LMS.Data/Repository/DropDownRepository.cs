﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMS.Data.Repository
{
    public class DropDownRepository : IDropDownRepository
    {
        LMSDataContext _db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

        #region IDropDownRepository Members

        public List<Source_Code> GetSourceCodes(bool includeBlank)
        {
            var Sources = _db.Source_Codes.Where(sc => sc.LTC == false).OrderBy(s => s.Source_Number).ToList();
            if (includeBlank)
            {
                Source_Code blank = new Source_Code();
                blank.Description = "-- Choose Source Code --";
                blank.Source_Code_ID = -1;
                Sources.Insert(0, blank);
            }
            return Sources;
        }

        public List<User> GetAgents(bool includeBlank)
        {
            var Agents = _db.Users.Where(a => a.Active == true).OrderBy(s => s.Last_Name).ToList();
            if (includeBlank)
            {
                User blank = new User();
                blank.Last_Name = "-- Choose User --";
                blank.User_ID = -1;
                Agents.Insert(0, blank);
            }

            return Agents;
        }

        public List<Non_Carrier> GetNonCarriers(bool includeBlank)
        {
            var NonCarrier = _db.Non_Carriers.OrderBy(n => n.Non_Carrier_Name).ToList();
            if (includeBlank)
            {
                Non_Carrier blank = new Non_Carrier();
                blank.Non_Carrier_ID = -1;
                blank.Non_Carrier_Name = "--Choose One--";
                NonCarrier.Insert(0, blank);
            }
            return NonCarrier;
        }

        public List<Non_Carrier> GetNonCarriersAndAll(bool includeBlank)
        {
            var NonCarrier = _db.Non_Carriers.OrderBy(n => n.Non_Carrier_Name).ToList();
            Non_Carrier nc = new Non_Carrier();
            nc.Non_Carrier_ID = -2;
            nc.Non_Carrier_Name = "All Non-Carriers";
            NonCarrier.Insert(0, nc);
            if (includeBlank)
            {
                Non_Carrier blank = new Non_Carrier();
                blank.Non_Carrier_ID = -1;
                blank.Non_Carrier_Name = "--Choose One--";
                NonCarrier.Insert(0, blank);
            }
            return NonCarrier;
        }

        public List<State> GetStates(bool includeBlank)
        {
            var States = _db.States.OrderBy(s => s.Abbreviation).ToList();
            return States;
        }

        public List<State> GetStatesAndAll(bool includeBlank)
        {
            var States = _db.States.Where(s => s.State_ID != 52).OrderBy(s => s.Abbreviation).ToList();
            State state = new State();
            state.State_ID = -2;
            state.Abbreviation = "All States";
            state.Name = "";
            States.Insert(0, state);

            if (includeBlank)
            {
                State blank = new State();
                blank.State_ID = -1;
                blank.Abbreviation = "--Choose State--";
                blank.Name = "";
                States.Insert(0, blank);
            }
            return States;
        }

        public List<Record_Eligibility> GetRecordEligibility(bool includeBlank)
        {
            var RecordEligibilities = _db.Record_Eligibilities.OrderBy(e => e.Order).ToList();
            if (includeBlank)
            {
                Record_Eligibility blank = new Record_Eligibility();
                blank.Record_Eligibility_ID = -1;
                blank.Category = "- Select -";
                RecordEligibilities.Insert(0, blank);
            }
            return RecordEligibilities;
        }

        public Dictionary<int, string> GetPriorityLevels(bool includeBlank)
        {
            Dictionary<int, string> items = new Dictionary<int, string>();
            if (includeBlank)
            {
                items.Add(-1, "-- Choose Priority --");
            }
            items.Add(1, "1 (High)");
            items.Add(2, "2");
            items.Add(3, "3 (Medium)");
            items.Add(4, "4");
            items.Add(5, "5 (Low)");

            return items;
        }

        //GetUserAccess
        public Dictionary<int, string> GetUserAccess(bool includeBlank)
        {
            Dictionary<int, string> items = new Dictionary<int, string>();
            if (includeBlank)
            {
                items.Add(-1, "-- Choose One --");
            }
            items.Add(1, "Yes");
            items.Add(0, "No");

            return items;
        }
        public List<Pod> GetPods(bool includeBlank)
        {
            var pods = _db.Pods.OrderBy(pm => pm.Pod_ID).ToList();

            if (includeBlank)
            {
                Pod blank = new Pod();
                blank.Pod_Name = "-- Choose Pod --";
                blank.Pod_ID = -1;
                pods.Insert(0, blank);
            }
            return pods;

        }
        //GetActiveAgentsOnly
        public List<User> GetActiveAgentOnly(bool includeBlank)
        {
            var usr = _db.Users.Where(u => u.Active == true && u.Security_Level_ID == 1).OrderBy(o => o.User_Name).ToList();

            if (includeBlank)
            {
                User blank = new User();
                blank.User_Name = "-- Choose Agent --";
                blank.User_ID = -1;
                usr.Insert(0, blank);
            }
            return usr;

        }
        //GetAgencyOnly
        public List<Agent> GetAgencyOnly(bool includeBlank)
        {
            var ag = _db.Agents.Where(u => u.Agent_Type_ID == 2).OrderBy(o => o.Agency_Name).ToList();

            if (includeBlank)
            {
                Agent blank = new Agent();
                blank.Agency_Name = "-- Choose Agency --";
                blank.Agent_ID = -1;
                ag.Insert(0, blank);
            }
            return ag;

        }
        //GetAgentListFromUser
        public List<sp_Agent_List_OnlyResult> GetAgentListFromUser()
        {
            var usr = _db.sp_Agent_List_Only().ToList();
            return usr;

        }

        //GetUserNames
        public List<User> GetUserNames(bool includeBlank)
        {
            //var usr = _db.Users.Where(u => u.Active == true).OrderBy(pm => pm.User_Name).ToList();
            var userList = _db.Users.Where(c => c.User_Name != "Pod1" && c.User_Name != "Pod2" && c.User_Name != "Pod3" && c.User_Name != "Pod4" && c.Active == true).OrderBy(o => o.User_Name).ToList();
            if (includeBlank)
            {
                User blank = new User();
                blank.User_Name = "-- Choose User --";
                blank.User_ID = -1;
                userList.Insert(0, blank);
            }
            return userList;

        }
        //GetCertAgentOnly
        public List<sp_Cert_Agent_List_OnlyResult> GetCertAgentOnly(bool includeBlank)
        {

            var agList = _db.sp_Cert_Agent_List_Only().ToList();
            if (includeBlank)
            {
                sp_Cert_Agent_List_OnlyResult blank = new sp_Cert_Agent_List_OnlyResult();
                blank.Full_Name = "-- Choose Agent --";
                blank.User_ID = -1;
                agList.Insert(0, blank);
            }
            return agList;

        }

        public Dictionary<string, string> GetGender(bool includeBlank)
        {
            Dictionary<string, string> items = new Dictionary<string, string>();

            items.Add("", "-- Choose One --");
            items.Add("M", "M");
            items.Add("F", "F");

            return items;
        }

        public List<Buying_Period> GetBuyingPeriods(bool includeBlank)
        {
            var buyingPeriods = _db.Buying_Periods.OrderBy(b => b.Display_Order).ToList();
            if (includeBlank)
            {
                Buying_Period blank = new Buying_Period();
                blank.Description = "-- Choose Buying Period --";
                blank.Buying_Period_ID = -1;
                buyingPeriods.Insert(0, blank);
            }
            return buyingPeriods;
        }

        public List<Transaction_Date_Type> GetTransactionDates(bool includeBlank)
        {
            var tranDateTypes = _db.Transaction_Date_Types.OrderBy(b => b.Description).ToList();
            if (includeBlank)
            {
                Transaction_Date_Type blank = new Transaction_Date_Type();
                blank.Description = "-- Choose Date Type --";
                blank.Transaction_Date_Type_ID = -1;
                tranDateTypes.Insert(0, blank);
            }
            return tranDateTypes;
        }

        public List<Buying_Period_Sub> GetBuyingPeriodSubs(bool includeBlank)
        {
            var buyingPeriods = _db.Buying_Period_Subs.OrderBy(c => c.Description).ToList();
            if (includeBlank)
            {
                Buying_Period_Sub blank = new Buying_Period_Sub();
                blank.Description = "-- Choose Buying Period Sub --";
                blank.Buying_Period_Sub_ID = -1;
                buyingPeriods.Insert(0, blank);
            }
            return buyingPeriods;
        }

        public List<Marital_Status> GetMaritalStatuses(bool includeBlank)
        {
            var maritalStatuses = _db.Marital_Status.OrderBy(c => c.Description).ToList();
            if (includeBlank)
            {
                Marital_Status blank = new Marital_Status();
                blank.Description = "-- Choose One --";
                blank.Marital_Status_ID = -1;
                maritalStatuses.Insert(0, blank);

            }
            return maritalStatuses;
        }

        public List<Tobacco_Use> GetTobaccoStatuses(bool includeBlank)
        {
            var tobaccoUseStatus = _db.Tobacco_Uses.OrderBy(c => c.Display_Order).ToList();
            if (includeBlank)
            {
                Tobacco_Use blank = new Tobacco_Use();
                blank.Description = "-- Choose One --";
                blank.Tobacco_Use_ID = -1;
                tobaccoUseStatus.Insert(0, blank);

            }
            return tobaccoUseStatus;
        }

        public List<Tobacco> GetTobaccoTypes(bool includeBlank)
        {
            //      var tobaccoTypes = _db.Tobaccos.OrderBy(c => c.Display_Order).ToList();
            var tobaccoTypes = _db.Tobaccos.Where(a => a.Active == true).OrderBy(c => c.Display_Order).ToList();
            if (includeBlank)
            {
                Tobacco blank = new Tobacco();
                blank.Description = "-- Choose One --";
                blank.Tobacco_ID = -1;
                tobaccoTypes.Insert(0, blank);

            }
            return tobaccoTypes;
        }
        public List<Plan_Transaction_Status> GetTransactionStatus(bool includeBlank)
        {
            var buyingPeriods = _db.Plan_Transaction_Status.OrderBy(p => p.Display_Order).ToList();
            if (includeBlank)
            {
                Plan_Transaction_Status blank = new Plan_Transaction_Status();
                blank.Description = "-- Choose Status --";
                blank.Plan_Transaction_Status_ID = -1;
                buyingPeriods.Insert(0, blank);
            }
            return buyingPeriods;
        }

        public List<Record_Activity_Status> GetActivityStatus(bool includeBlank, Enums.ComplaintType? complaintStep)
        {
            var recordActivityStatus = _db.Record_Activity_Status.Where(x => x.Active == 1).OrderBy(p => p.Display_Order).ToList();
            if (includeBlank)
            {
                var blank = new Record_Activity_Status();
                blank.Description = "-- Choose Status --";
                blank.Record_Activity_Status_ID = -1;
                recordActivityStatus.Insert(0, blank);
            }
            var complaintOpen = recordActivityStatus.Where(x => x.Description == "Complaint - Open").FirstOrDefault();
            var complaintInProcess = recordActivityStatus.Where(x => x.Description == "Complaint - In Process").FirstOrDefault();
            var complaintResolved = recordActivityStatus.Where(x => x.Description == "Complaint - Resolved").FirstOrDefault();
            if (complaintStep != null)
            {
                if (complaintStep != Enums.ComplaintType.All)
                {
                    if (complaintStep == Enums.ComplaintType.Open)//SHow Open only
                    {
                        recordActivityStatus.Remove(complaintInProcess);
                        recordActivityStatus.Remove(complaintResolved);
                    }
                    if (complaintStep == Enums.ComplaintType.InProgress)//Show InProcess Only
                    {
                        recordActivityStatus.Remove(complaintOpen);
                        recordActivityStatus.Remove(complaintResolved);
                    }
                    if (complaintStep == Enums.ComplaintType.Resoloved)//Show Resolved and In Process only
                    {
                        recordActivityStatus.Remove(complaintOpen);
                    }

                }
            }
            else
            {
                recordActivityStatus.Remove(complaintOpen);
                recordActivityStatus.Remove(complaintInProcess);
                recordActivityStatus.Remove(complaintResolved);
            }

            return recordActivityStatus;
        }

        public List<Auto_Email> GetAutoEmail(bool includeBlank)
        {
            var AutoEmail = _db.Auto_Emails.Where(a => a.LMS_Display.Equals(true)).ToList();
            if (includeBlank)
            {
                Auto_Email AE = new Auto_Email();
                AE.Notes = "---Choose EMail---";
                AE.Auto_Email_ID = -1;
                AutoEmail.Insert(0, AE);
            }
            return AutoEmail;
        }

        public List<Activity_Type> GetFollowUpType(bool includeBlank)
        {
            //var FollowUpType = _db.Activity_Types.Where(c => c.Activity_Type_ID != 10 && c.Activity_Type_ID != 2 && c.Activity_Type_ID != 12 && c.Activity_Type_ID != 11 && c.Activity_Type_ID != 13 && c.Activity_Type_ID != 14 && c.Activity_Type_ID != 15 && c.Activity_Type_ID != 16 && c.Activity_Type_ID != 17 && c.Activity_Type_ID != 18 && c.Activity_Type_ID != 19 && c.Activity_Type_ID != 20 && c.Activity_Type_ID != 21 && c.Description != "SilverPop" && c.Description != "Prefilled App" && c.Description != "Received Voicemail" && c.Description != "Live Transfer").ToList();
            var FollowUpType = _db.Activity_Types.Where(x => x.Active_Followup == 1).OrderBy(x => x.Description).ToList();

            if (includeBlank)
            {
                Activity_Type blank = new Activity_Type();

                blank.Description = "-- Choose Activity --";
                blank.Activity_Type_ID = -1;
                FollowUpType.Insert(0, blank);
            }

            return FollowUpType;
        }

        public List<Activity_Type> GetActivityTypes(bool includeBlank)
        {
            //var ActivityTypes = _db.Activity_Types.Where(c => c.Activity_Type_ID!= 2 && c.Activity_Type_ID != 13 && c.Activity_Type_ID != 14 && c.Activity_Type_ID != 3 && c.Activity_Type_ID != 4 && c.Activity_Type_ID != 5 && c.Activity_Type_ID != 6).OrderBy(c => c.Description).ToList();
            //var ActivityTypes = _db.Activity_Types.Where(c => c.Activity_Type_ID > 0 && c.Activity_Type_ID != 2 && c.Activity_Type_ID != 20).OrderBy(c => c.Description).ToList();
            var ActivityTypes = _db.Activity_Types.Where(c => c.Active_Current == 1).OrderBy(c => c.Description).ToList();

            if (includeBlank)
            {
                Activity_Type blank = new Activity_Type();
                blank.Description = "-- Choose Activity --";
                blank.Activity_Type_ID = -1;
                ActivityTypes.Insert(0, blank);
            }
            return ActivityTypes;
        }

        public List<Activity_Type> GetEditActivityTypes(bool includeBlank)
        {
            //var EditActivityTypes = _db.Activity_Types.Where(c => c.Activity_Type_ID > 0).OrderBy(c => c.Description).ToList();
            var EditActivityTypes = _db.Activity_Types.Where(c => c.Active_Edit == 1).OrderBy(c => c.Description).ToList();

            if (includeBlank)
            {
                Activity_Type blank = new Activity_Type();
                blank.Description = "-- Choose Activity Type --";
                blank.Activity_Type_ID = -1;
                EditActivityTypes.Insert(0, blank);
            }
            return EditActivityTypes;
        }

        public List<Record_Type> GetRecordStatus(bool includeBlank)
        {
            var buyingPeriods = _db.Record_Types.ToList();
            if (includeBlank)
            {
                Record_Type blank = new Record_Type();
                blank.Description = "-- Choose Activity Type --";
                blank.Record_Type_ID = -1;
                buyingPeriods.Insert(0, blank);
            }
            return buyingPeriods;
        }

        public List<Plan_Transaction> GetOpportunities(int Record_ID)
        {
            //using (_db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString))
            //{
            //    var opportunities = from planTransaction in _db.Plan_Transactions.Where(p => p.Record_ID == Record_ID).ToList()
            //                        join planStatus in _db.Plan_Transaction_Status on planTransaction.Plan_Transaction_Status_ID equals planStatus.Plan_Transaction_Status_ID
            //                        select planTransaction;

            //    var test = opportunities.ToList();

            //    return opportunities.ToList();
            //}

            return _db.Plan_Transactions.Where(p => p.Record_ID == Record_ID).ToList();

        }


        public List<string> GetTimes()
        {
            List<string> times = new List<string>();

            times.Add(@"N\A");
            times.Add("6:00 AM");
            times.Add("6:15 AM");
            times.Add("6:30 AM");
            times.Add("6:45 AM");
            times.Add("7:00 AM");
            times.Add("7:15 AM");
            times.Add("7:30 AM");
            times.Add("7:45 AM");
            times.Add("8:00 AM");
            times.Add("8:15 AM");
            times.Add("8:30 AM");
            times.Add("8:45 AM");
            times.Add("9:00 AM");
            times.Add("9:15 AM");
            times.Add("9:30 AM");
            times.Add("9:45 AM");
            times.Add("10:00 AM");
            times.Add("10:15 AM");
            times.Add("10:30 AM");
            times.Add("10:45 AM");
            times.Add("11:00 AM");
            times.Add("11:15 AM");
            times.Add("11:30 AM");
            times.Add("11:45 AM");
            times.Add("12:00 PM");
            times.Add("12:15 PM");
            times.Add("12:30 PM");
            times.Add("12:45 PM");
            times.Add("1:00 PM");
            times.Add("1:15 PM");
            times.Add("1:30 PM");
            times.Add("1:45 PM");
            times.Add("2:00 PM");
            times.Add("2:15 PM");
            times.Add("2:30 PM");
            times.Add("2:45 PM");
            times.Add("3:00 PM");
            times.Add("3:15 PM");
            times.Add("3:30 PM");
            times.Add("3:45 PM");
            times.Add("4:00 PM");
            times.Add("4:15 PM");
            times.Add("4:30 PM");
            times.Add("4:45 PM");
            times.Add("5:00 PM");
            times.Add("5:15 PM");
            times.Add("5:30 PM");
            times.Add("5:45 PM");
            times.Add("6:00 PM");
            times.Add("6:15 PM");
            times.Add("6:30 PM");
            times.Add("6:45 PM");

            return times;
        }

        public List<User> GetAgentOnly()
        {
            return _db.Users.Where(u => u.Security_Level_ID == 1 || u.Security_Level_ID == 4 || u.Security_Level_ID == 7).OrderBy(u => u.Last_Name).ToList();
        }

        public List<Carrier> GetCarriers(bool includeBlank)
        {
            var Sources = _db.Carriers.Where(c => c.Active == true).OrderBy(c => c.Name).ToList();
            if (includeBlank)
            {
                Carrier blank = new Carrier();
                blank.Name = "-- Choose Carrier --";
                blank.Carrier_ID = -1;
                Sources.Insert(0, blank);
            }
            return Sources;
        }

        //GetCAseManagers
        public List<sp_Get_Case_ManagerResult> GetCaseManagers(bool includeBlank)
        {

            var list = _db.sp_Get_Case_Manager().ToList();
            if (includeBlank)
            {
                sp_Get_Case_ManagerResult blank = new sp_Get_Case_ManagerResult();
                blank.Name = "-- Choose Case Manager --";
                blank.Case_Manager_ID = -1;
                list.Insert(0, blank);
            }
            return list;
        }

        //GetHealthCarriers

        public List<Carrier> GetGBSCarriers(bool includeBlank)
        {
            var Sources = _db.Carriers.Where(c => c.Active == true && c.GBS_Admin_ID != null).OrderBy(c => c.Name).ToList();
            if (includeBlank)
            {
                Carrier blank = new Carrier();
                blank.Name = "-- Choose Carrier --";
                blank.Carrier_ID = -1;
                Sources.Insert(0, blank);
            }
            return Sources;
        }

        public List<Carrier> GetGBSCarriersAndAll(bool includeBlank)
        {
            var Sources = _db.Carriers.Where(c => c.Active == true && c.GBS_Admin_ID != null).OrderBy(c => c.Name).ToList();
            Carrier carrier = new Carrier();
            carrier.Name = "All Carriers";
            carrier.Carrier_ID = -2;
            Sources.Insert(0, carrier);
            if (includeBlank)
            {
                Carrier blank = new Carrier();
                blank.Name = "-- Choose Carrier --";
                blank.Carrier_ID = -1;
                Sources.Insert(0, blank);
            }
            return Sources;
        }

        public List<sp_Get_State_Authorized_CarriersResult> GetStateAuthorizedCarriersAndAll(bool includeBlank)
        {
            var list = _db.sp_Get_State_Authorized_Carriers().ToList();
            sp_Get_State_Authorized_CarriersResult all = new sp_Get_State_Authorized_CarriersResult();
            all.Carrier_ID = -2;
            all.Name = "All Carriers";
            list.Insert(0, all);
            if (includeBlank)
            {
                sp_Get_State_Authorized_CarriersResult blank = new sp_Get_State_Authorized_CarriersResult();
                blank.Name = "-- Choose Carrier --";
                blank.Carrier_ID = -1;
                list.Insert(0, blank);
            }
            return list;
        }

        public List<sp_Get_State_Authorized_CarriersResult> GetStateAuthorizedCarriers(bool includeBlank)
        {
            var list = _db.sp_Get_State_Authorized_Carriers().ToList();
            if (includeBlank)
            {
                sp_Get_State_Authorized_CarriersResult blank = new sp_Get_State_Authorized_CarriersResult();
                blank.Name = "-- Choose Carrier --";
                blank.Carrier_ID = -1;
                list.Insert(0, blank);
            }
            return list;
        }

        public List<Certification_Status_Lookup> GetCertificationStatus(bool includeBlank)
        {
            var Sources = _db.Certification_Status_Lookups.OrderBy(c => c.Certification_Status).ToList();
            if (includeBlank)
            {
                Certification_Status_Lookup blank = new Certification_Status_Lookup();
                blank.Certification_Status = "-- Choose Status --";
                blank.Certification_Status_ID = -1;
                Sources.Insert(0, blank);
            }
            return Sources;
        }

        public List<Certification_Type_Lookup> GetCertificationType(bool includeBlank)
        {
            var Sources = _db.Certification_Type_Lookups.OrderBy(c => c.Certification_Type).ToList();
            if (includeBlank)
            {
                Certification_Type_Lookup blank = new Certification_Type_Lookup();
                blank.Certification_Type = "-- Choose Type --";
                blank.Certification_Type_ID = -1;
                Sources.Insert(0, blank);
            }
            return Sources;
        }

        public List<Carrier> GetCarriers(int GroupID, bool includeBlank)
        {
            var Sources = _db.Carriers.Where(c => c.Active == true && (c.Group_ID == GroupID || c.Group_ID == 0 || c.Group_ID == 5)).OrderBy(c => c.Name).ToList();

            if (includeBlank)
            {
                Carrier blank = new Carrier();
                blank.Name = "-- Choose Carrier --";
                blank.Carrier_ID = -1;
                Sources.Insert(0, blank);
            }
            return Sources;
        }

        public List<Plan> GetPlans(bool includeBlank)
        {
            var Sources = _db.Plans.Where(c => c.Active == true).OrderBy(p => p.Name).ToList();
            if (includeBlank)
            {
                Plan blank = new Plan();
                blank.Name = "-- Choose Plan --";
                blank.Carrier_ID = -1;
                Sources.Insert(0, blank);
            }
            return Sources;
        }

        public List<Group> GetActiveGroups(bool includeBlank)
        {
            var Sources = _db.Groups.Where(g => g.Active == '1').OrderBy(g => g.Description).ToList();
            if (includeBlank)
            {
                Group blank = new Group();
                blank.Description = "-- Choose Group --";
                blank.Group_ID = -1;
                Sources.Insert(0, blank);
            }
            return Sources;
        }

        public List<Group> GetGroups(bool includeBlank)
        {
            var Sources = _db.Groups.OrderBy(g => g.Description).ToList();
            if (includeBlank)
            {
                Group blank = new Group();
                blank.Description = "-- Choose Group --";
                blank.Group_ID = -1;
                Sources.Insert(0, blank);
            }
            return Sources;
        }

        //Security Level
        public List<Security_Level> GetSecurityLevels(bool includeBlank)
        {
            var seclevel = _db.Security_Levels.Where(sec => sec.In_AD == false).OrderBy(g => g.Description).ToList();
            if (includeBlank)
            {
                Security_Level blank = new Security_Level();
                blank.Description = "-- Choose Security Level --";
                blank.Security_Level_ID = -1;
                seclevel.Insert(0, blank);
            }
            return seclevel;
        }

        public List<Affinity_Partner> GetAffinityPartners(bool includeBlank)
        {
            var Sources = _db.Affinity_Partners.OrderBy(g => g.Name).ToList();
            if (includeBlank)
            {
                Affinity_Partner blank = new Affinity_Partner();
                blank.Name = "-- Choose Affinity Partner --";
                blank.Affinity_Partner_ID = -1;
                Sources.Insert(0, blank);
            }
            return Sources;
        }

        public List<Marketing_Group> GetMarketingGroups(bool includeBlank)
        {
            var Sources = _db.Marketing_Groups.OrderBy(g => g.Description).ToList();
            if (includeBlank)
            {
                Marketing_Group blank = new Marketing_Group();
                blank.Description = "-- Choose Group --";
                blank.Marketing_Group_ID = -1;
                Sources.Insert(0, blank);
            }
            return Sources;
        }

        public List<Media_Type> GetMediaTypes(bool includeBlank)
        {
            var Sources = _db.Media_Types.OrderBy(g => g.Media_Type_Description).ToList();
            if (includeBlank)
            {
                Media_Type blank = new Media_Type();
                blank.Media_Type_Description = "-- Choose Media --";
                blank.Media_Type_ID = -1;
                Sources.Insert(0, blank);
            }
            return Sources;
        }

        public List<Shipping_Method> GetShippingMethods(bool includeBlank)
        {
            var Sources = _db.Shipping_Methods.OrderBy(g => g.Display_Order).ToList();
            if (includeBlank)
            {
                Shipping_Method blank = new Shipping_Method();
                blank.Description = "-- Choose Shipping Method --";
                blank.Shipping_Method_ID = -1;
                Sources.Insert(0, blank);
            }
            return Sources;
        }

        public List<Modal> GetModals(bool includeBlank)
        {
            var Sources = _db.Modals.OrderBy(g => g.Display_Order).ToList();
            if (includeBlank)
            {
                Modal blank = new Modal();
                blank.Description = "-- Choose Modal --";
                blank.Modal_ID = -1;
                Sources.Insert(0, blank);
            }
            return Sources;
        }

        public List<Note_Helper> GetUserNotes(int User_ID)
        {
            return _db.Note_Helpers.Where(n => n.User_ID == User_ID || n.User_ID == 0).OrderBy(n => n.Note).ToList();
        }

        public List<Underwriting_Class> GetUnderwritingClass(bool includeBlank)
        {
            var Sources = _db.Underwriting_Classes.Where(u => u.Active == true).OrderBy(u => u.Display_Order).ToList();
            if (includeBlank)
            {
                Underwriting_Class blank = new Underwriting_Class();
                blank.Description = "-- Choose Underwriting Class --";
                blank.Underwriting_ID = -1;
                Sources.Insert(0, blank);
            }
            return Sources;
        }

        public List<Exam_Service> GetExamService(bool includeBlank)
        {
            var Sources = _db.Exam_Services.ToList();
            if (includeBlank)
            {
                Exam_Service blank = new Exam_Service();
                blank.Description = "-- Choose Exam Service --";
                blank.Exam_Service_ID = -1;
                Sources.Insert(0, blank);
            }
            return Sources;
        }

        public List<Payment_Mode> GetPaymentModes(bool includeBlank)
        {
            var Sources = _db.Payment_Modes.OrderBy(pm => pm.Display_Order).ToList();
            if (includeBlank)
            {
                Payment_Mode blank = new Payment_Mode();
                blank.Description = "-- Choose Payment Type --";
                blank.Payment_Mode_ID = -1;
                Sources.Insert(0, blank);
            }
            return Sources;
        }

        public List<Record_Type> GetRecordType(bool includeBlank)
        {
            var RecordType = _db.Record_Types.ToList();

            return RecordType;
        }

        public List<string> GetSuffix()
        {
            List<string> data = new List<string>();

            data.Add("Jr.");
            data.Add("Sr.");
            data.Add("II");
            data.Add("III");
            data.Add("IV");
            data.Add("");

            return data;
        }

        public List<Salutation> GetSalutations()
        {
            var Sources = _db.Salutations.Where(s => s.Active == true).OrderBy(pm => pm.Display_Order).ToList();

            return Sources;
        }

        public List<Broker> GetBrokers(bool includeBlank)
        {
            var Sources = _db.Brokers.OrderBy(pm => pm.Last_Name).ToList();

            if (includeBlank)
            {
                Broker blank = new Broker();
                blank.Last_Name = "-- Choose Broker Type --";
                blank.Broker_ID = -1;
                Sources.Insert(0, blank);
            }
            return Sources;
        }

        public List<Record_Activity_Note> GetActivityNotes(int User_ID, int noteType)
        {
            return _db.Record_Activity_Notes.Where(n => n.User_ID == User_ID || n.User_ID == 0 && n.Record_Activity_Note_Type == noteType && n.Active == true).OrderBy(n => n.Description).ToList();
        }

        public List<Letter> GetLetters(bool includeBlank)
        {
            var Sources = _db.Letters.Where(pm => pm.Active.Value == true).OrderBy(pm => pm.Display_Order).ToList();

            if (includeBlank)
            {
                Letter blank = new Letter();
                blank.Description = "-- Choose Letter --";
                blank.Letter_ID = -1;
                Sources.Insert(0, blank);
            }
            return Sources;
        }

        public List<Employment_Status> GetEmploymentStatus(bool includeBlank)
        {
            throw new NotImplementedException();
        }

        public List<Reconciliation_Type> GetReconciliationTypes(bool includeBlank, Enums.ComplaintType? complaintStep)
        {
            var Sources = _db.Reconciliation_Types.Where(pm => pm.Active == true).OrderBy(p => p.Display_Order).ToList();

            if (includeBlank)
            {
                Reconciliation_Type blank = new Reconciliation_Type();
                blank.Description = "-- Choose Type --";
                blank.Reconciliation_Type_ID = -1;
                Sources.Insert(0, blank);
            }

            var complaintOpen = Sources.Where(x => x.Description == "Complaint - Open").FirstOrDefault();
            var complaintInProcess = Sources.Where(x => x.Description == "Complaint - In Process").FirstOrDefault();
            var complaintResolved = Sources.Where(x => x.Description == "Complaint - Resolved").FirstOrDefault();
            if (complaintStep != null)
            {
                if (complaintStep != Enums.ComplaintType.All)
                {
                    if (complaintStep == Enums.ComplaintType.Open)//SHow Open only
                    {
                        Sources.Remove(complaintInProcess);
                        Sources.Remove(complaintResolved);
                    }
                    if (complaintStep == Enums.ComplaintType.InProgress)//Show InProcess Only
                    {
                        Sources.Remove(complaintOpen);
                        Sources.Remove(complaintResolved);
                    }
                    if (complaintStep == Enums.ComplaintType.Resoloved)//Show Resolved and In Process only
                    {
                        //Sources = new List<Reconciliation_Type>();
                        //Sources = _db.Reconciliation_Types.Where(x => x.Active == true && (x.Description == "Complaint - In Process" || x.Description == "Complaint - Resolved")).OrderBy(p => p.Display_Order).ToList();

                        Sources.Remove(complaintOpen);
                    }

                }
            }
            else
            {
                Sources.Remove(complaintOpen);
                Sources.Remove(complaintInProcess);
                Sources.Remove(complaintResolved);
            }
            
            return Sources;
        }

        public List<User> GetAppointmentSetters(bool includeBlank)
        {
            var Agents = _db.Users.Where(u => u.Assignment_Indicator.Equals(true)).OrderBy(u => u.Last_Name).ToList();

            if (includeBlank)
            {
                User blank = new User();
                blank.Last_Name = "-- Choose User --";
                blank.User_ID = -1;
                Agents.Insert(0, blank);
            }

            return Agents;
        }

        public List<Subsidy> GetSubsidies(bool includeBlank)
        {
            var Sub = _db.Subsidies.Where(s => s.Active.Equals(true)).OrderBy(s => s.Display_Order).ToList();
            if (includeBlank)
            {
                Subsidy subsidy = new Subsidy();
                subsidy.Subsidy_ID = -1;
                subsidy.Subsidy_Desc = "";
                Sub.Insert(0, subsidy);
            }
            return Sub;
        }

        public List<Products_Of_Interest> GetProductsOfInterest(bool includeBlank)
        {
            var Pois = _db.Products_Of_Interests.Where(s => s.Active.Equals(true)).OrderBy(s => s.Order).ToList();
            if (includeBlank)
            {
                Products_Of_Interest poi = new Products_Of_Interest();
                poi.Products_Of_Interest_ID = -1;
                poi.Products_Of_Interest_Desc = "";
                Pois.Insert(0, poi);
            }
            return Pois;
        }

        public List<Record_Current_Situation> GetRecordCurrentSituations(bool includeBlank)
        {
            var Situations = _db.Record_Current_Situations.Where(s => s.Active.Equals(true)).OrderBy(s => s.Order).ToList();
            if (includeBlank)
            {
                Record_Current_Situation situation = new Record_Current_Situation();
                situation.Record_Current_Situation_ID = -1;
                situation.Record_Current_Situation_Desc = "";
                Situations.Insert(0, situation);
            }
            return Situations;
        }

        public List<Current_Insured_Type> GetCurrentInsuredTypes(bool includeBlank)
        {
            var Types = _db.Current_Insured_Types.Where(s => s.Active.Equals(true)).OrderBy(s => s.Display_Order).ToList();
            if (includeBlank)
            {
                Current_Insured_Type type = new Current_Insured_Type();
                type.Current_Insured_Type_ID = -1;
                type.Current_Insured = "-- Choose One --";
                Types.Insert(0, type);
            }
            return Types;

        }

        public Dictionary<int, string> GetCallRatings(bool includeBlank)
        {
            Dictionary<int, string> RatingValues = new Dictionary<int, string>();

            if (includeBlank)
            {
                RatingValues.Add(-1, "");

            }

            RatingValues.Add(1, "1");
            RatingValues.Add(2, "2");
            RatingValues.Add(3, "3");
            RatingValues.Add(4, "4");
            RatingValues.Add(5, "5");

            return RatingValues;
        }

        public List<sp_Get_Pod_ListResult> GetPodLIst(int User_ID, bool includeBlank)
        {
            List<sp_Get_Pod_ListResult> pods = _db.sp_Get_Pod_List(User_ID).ToList();
            if (includeBlank)
            {
                sp_Get_Pod_ListResult pod = new sp_Get_Pod_ListResult();
                pod.First_Name = "";
                pod.Last_Name = "";
                pod.User_ID = -1;
                pod.User_Name = "";
                pods.Insert(0, pod);
            }
            return pods;

        }

        public List<Insurance_Line_Lookup> GetInsuranceLine(bool includeBlank)
        {
            var list = _db.Insurance_Line_Lookups.OrderBy(i => i.Description).ToList();
            if (includeBlank)
            {
                Insurance_Line_Lookup item = new Insurance_Line_Lookup();
                item.Description = "-Choose one-";
                item.Line_Of_Insurance_ID = -1;
                list.Insert(0, item);
            }
            return list;
        }

        public List<sp_Agent_ListResult> GetAgentAgencyAndAll(bool includeBlank)
        {
            List<sp_Agent_ListResult> list = _db.sp_Agent_List().ToList();
            sp_Agent_ListResult rs1 = new sp_Agent_ListResult();
            rs1.Agent_ID = -2;
            rs1.Name = "All Active Agents";
            rs1.NPN = "";
            list.Insert(0, rs1);
            sp_Agent_ListResult rs2 = new sp_Agent_ListResult();
            rs2.Agent_ID = -3;
            rs2.Name = "All Active Agents/Agencies";
            rs2.NPN = "";
            list.Insert(0, rs2);
            if (includeBlank)
            {
                sp_Agent_ListResult rs = new sp_Agent_ListResult();
                rs.Agent_ID = -1;
                rs.Name = "-Please choose-";
                rs.NPN = "";
                list.Insert(0, rs);
            }
            return list;
        }

        public List<sp_Get_Active_AgentsResult> GetActiveAgentAgency(bool includeBlank)
        {
            List<sp_Get_Active_AgentsResult> list = _db.sp_Get_Active_Agents().ToList();
            if (includeBlank)
            {
                sp_Get_Active_AgentsResult rs = new sp_Get_Active_AgentsResult();
                rs.Agent_ID = -1;
                rs.Full_Name = "-Please choose-";
                rs.NPN = "";
                list.Insert(0, rs);
            }
            return list;
        }

        public List<sp_Cert_Agent_List_OnlyResult> GetAgentAndAll(bool includeBlank)
        {
            List<sp_Cert_Agent_List_OnlyResult> list = _db.sp_Cert_Agent_List_Only().ToList();
            sp_Cert_Agent_List_OnlyResult rs1 = new sp_Cert_Agent_List_OnlyResult();
            rs1.Agent_ID = -2;
            rs1.Full_Name = "All Active Agents";

            list.Insert(0, rs1);
            if (includeBlank)
            {
                sp_Cert_Agent_List_OnlyResult rs = new sp_Cert_Agent_List_OnlyResult();
                rs.Agent_ID = -1;
                rs.Full_Name = "--Please choose--";
                list.Insert(0, rs);
            }
            return list;
        }

        public List<CategoryDriver> GetCatagoryDriver(bool includeBlank)
        {
            var list = _db.CategoryDrivers.OrderBy(x => x.CategoryDisplayOrder).ToList();
            if (includeBlank)
            {
                var item = new CategoryDriver
                {
                    CategoryDisplayOrder = 0,
                    CategoryActive = true,
                    CategoryDriver1 = "Choose",
                    CategoryDriverDesc = string.Empty,
                    CategoryDriverDetails = string.Empty,
                    CategoryDriverID = -1,
                    CategoryPriorityLevel = 0
                };
                list.Insert(0, item);
            }

            return list;
        }

        public List<Group> GetAgentGroup(bool includeBlank)
        {
            var list = _db.Groups.OrderBy(x => x.Description).ToList();
            if (includeBlank)
            {
                var item = new Group
                {
                    Description = "Choose",
                    Group_ID = -1
                };
                list.Insert(0, item);
            }

            return list;
        }

        public Dictionary<string, string> GetAgentType(bool includeBlank)
        {
            var List = new Dictionary<string, string>();

            if (includeBlank)
            {
                List.Add(string.Empty, "Choose Type");
            }

            //List.Add("Unknown", "Unknown");
            List.Add("CallCenterAgent", "Call Center");
            List.Add("FieldAgent", "Field Agent");

            return List;
        }

        public List<User> GetAgentFinal(bool includeBlank)
        {
            var list = _db.Users.Where(x => x.Employee == true).OrderBy(s => s.Last_Name).ToList();
            foreach (User item in list)
            {//Overwriting Username for display purpose
                item.User_Name = item.First_Name + " " + item.Last_Name.Trim();
            }

            if (includeBlank)
            {
                var item = new User
                {
                    User_Name = "-- Choose User --",
                    User_ID = -1
                };
                list.Insert(0, item);
            }



            return list;
        }

        public List<User> GetAgentsDeparted(bool includeBlank)
        {
            var list = _db.Users.Where(x => x.Employee == false).OrderBy(s => s.Last_Name).ToList();
            foreach (User item in list)
            {//Overwriting Username for display purpose
                item.User_Name = item.First_Name + " " + item.Last_Name.Trim();
            }

            if (includeBlank)
            {
                var item = new User
                {
                    User_Name = "-- Choose User --",
                    User_ID = -1
                };
                list.Insert(0, item);
            }



            return list;
        }

        public Dictionary<int, string> GetAllMonths(bool includeBlank)
        {
            var MonthsList = new Dictionary<int, string>();

            if (includeBlank)
                MonthsList.Add(0, "Choose");

            string[] monthNames = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.MonthNames;
            var x = 1;
            foreach (string m in monthNames)
            {
                if (!string.IsNullOrEmpty(m))
                {
                    MonthsList.Add(x, m);
                }
                x++;
            }


            return MonthsList;
        }

        public Dictionary<int, string> GetAllYears(bool includeBlank)
        {
            var List = new Dictionary<int, string>();

            if (includeBlank)
                List.Add(0, "Choose");

            for (int i = 1900; i < DateTime.Today.Year + 1; i++)
            {
                List.Add(i, i.ToString());
            }


            return List;
        }

        public Dictionary<int, string> GetPriority(bool includeBlank)
        {
            var List = new Dictionary<int, string>();

            if (includeBlank)
                List.Add(0, "Choose");

            List.Add(1, "1 (High)");
            List.Add(2, "2");
            List.Add(3, "3");
            List.Add(4, "4");
            List.Add(5, "5 (Low)");

            //for (int i = 1; i < 11; i++)
            //{
            //    List.Add(i, i.ToString());
            //}

            return List;
        }

        public Dictionary<string, string> GetAgentStatus(bool includedBlank)
        {
            var List = new Dictionary<string, string>();

            if (includedBlank)
                List.Add(string.Empty, "Choose");

            //List.Add("Unknown", "Unknown");
            List.Add("Departed", "Departed");
            List.Add("Current", "Current");

            return List;
        }

        public Dictionary<string, string> GetYesNo(bool includedBlank)
        {
            var List = new Dictionary<string, string>();

            if (includedBlank)
                List.Add(string.Empty, "Choose");

            List.Add("Y", "Y");
            List.Add("N", "N");

            return List;
        }

        //public List<GBSCarriersResult> GetGBSCarrier(bool includeBlank)
        //{
        //    var list = _db.GBSCarriers().OrderBy(x => x.Name).ToList();

        //    if (includeBlank)
        //    {
        //        var item = new GBSCarriersResult
        //        {
        //            Name = "-- Choose User --"
        //        };

        //        list.Insert(0, item);
        //    }

        //    return list;
        //}

        public List<GBSProductsResult> GetGBSProducts(bool includeBlank)
        {
            var list = _db.GBSProducts().OrderBy(x => x.ProductName).ToList();

            if (includeBlank)
            {
                var item = new GBSProductsResult
                {
                    ProductName = "-- Choose User --"
                };

                list.Insert(0, item);
            }

            return list;
        }

        public List<GBSProductTypeLookupResult> GetGBSProductTypeLookup(bool includeBlank)
        {
            var list = _db.GBSProductTypeLookup().OrderBy(x => x.ProductTypeDescription).ToList();

            if (includeBlank)
            {
                var item = new GBSProductTypeLookupResult
                {
                    ProductTypeDescription = "-- Choose Product --"
                };

                list.Insert(0, item);
            }

            return list;
        }

        public Dictionary<string, string> GetCustomRecordStatus(bool includedBlank)
        {
            var List = new Dictionary<string, string>();

            if (includedBlank)
                List.Add(string.Empty, "Choose");

            List.Add("Unattempted", "Unattempted");
            List.Add("Working Uncontacted/AppropriateFU", "Working Uncontacted/AppropriateFU");
            List.Add("Working - In Progress", "Working - In Progress");
            List.Add("Working SentInfo/Quote", "Working SentInfo/Quote");
            List.Add("Done", "Done");
            List.Add("Unknown", "Unknown");

            return List;
        }

        public Dictionary<int, string> PlaceHolder()
        {
            var List = new Dictionary<int, string>();

            List.Add(0, "PLACEHOLDER");
            List.Add(1, "PLACEHOLDER");
            List.Add(2, "PLACEHOLDER");
            return List;
        }

        public List<Category_Situation> GetCategorySituation(bool includeBlank)
        {
            var list = _db.Category_Situations.OrderBy(x => x.CategorySituationID).ToList();
            if (includeBlank)
            {
                var item = new Category_Situation
                {
                    CategorySituation = "Choose",
                    CategorySituationDesc = "Choose",
                    CategorySituationID = 0
                };
                list.Insert(0, item);
            }

            return list;
        }

        public Dictionary<int, string> GetPriorityOpAdmin(bool includeBlank)
        {
            var List = new Dictionary<int, string>();

            if (includeBlank)
                List.Add(-1, "Choose");

            List.Add(1, "1 (High)");
            List.Add(2, "2");
            List.Add(3, "3");
            List.Add(4, "4");
            List.Add(5, "5 (Low)");
            List.Add(0, "0 (No Priority)");

            //for (int i = 1; i < 11; i++)
            //{
            //    List.Add(i, i.ToString());
            //}

            return List;
        }

        public List<CriticalTopicCoverage> GetCriticalTopicCoverage(bool includeBlank)
        {
            var list = _db.CriticalTopicCoverages.OrderBy(x => x.CTCID).ToList();

            if (includeBlank)
            {
                var item = new CriticalTopicCoverage
                {
                    CTCID = 0,
                    Description = "-- Choose --"
                };

                list.Insert(0, item);
            }

            return list;
        }

        public List<SalesRating> GetSalesRating(bool includeBlank)
        {
            var list = _db.SalesRatings.OrderBy(x => x.SEEID).ToList();

            if (includeBlank)
            {
                var item = new SalesRating
                {
                    SEEID = 0,
                    Description = "-- Choose --"
                };

                list.Insert(0, item);
            }

            return list;
        }

        public List<AdminDocumentationRating> GetAdminDocumentationRating(bool includeBlank)
        {
            var list = _db.AdminDocumentationRatings.OrderBy(x => x.AdminDocumentationID).ToList();

            if (includeBlank)
            {
                var item = new AdminDocumentationRating
                {
                    AdminDocumentationID = 0,
                    Description = "-- Choose --"
                };

                list.Insert(0, item);
            }

            return list;
        }

        public List<Campaign_Code> GetCampaignCodes(bool includeBlank)
        {
            var list = _db.Campaign_Codes.Where(x=> x.Active == true).OrderBy(x => x.Campaign_Code_ID).ToList();

            if (includeBlank)
            {
                var item = new Campaign_Code
                {
                    Campaign_Code_ID = 0,
                    Campaign_Number=string.Empty,
                    Campaign_Description = "-- Choose --"
                };

                list.Insert(0, item);
            }

            return list;
        }

        #endregion
    }
}
