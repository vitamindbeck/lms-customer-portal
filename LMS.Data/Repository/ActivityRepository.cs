﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMS.Data.Repository
{
    public class ActivityRepository : IActivityRepository
    {
        private LMSDataContext _db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

        #region IActivityRepository Members

        public IQueryable<Record_Activity> GetAgentsActivities(int Agent_ID)
        {
            return _db.Record_Activities.Where(act => act.User_ID == Agent_ID);
        }

        public Record_Activity GetActivity(int id)
        {
            return _db.Record_Activities.SingleOrDefault(act => act.Record_Activity_ID == id);
        }

        public void Update(Record_Activity recordActivity)
        {
            _db.Record_Activities.InsertOnSubmit(recordActivity);
        }

        public void Delete(Record_Activity recordActitity)
        {
            _db.Record_Activities.DeleteOnSubmit(recordActitity);
        }
        public List<GetRecordActivitiesDisplayResult> GetRecordActivitiesDisplay(int Record_ID)
        {
            return _db.GetRecordActivitiesDisplay(Record_ID).ToList();
        }

        public void Save()
        {
            _db.SubmitChanges();
        }

        public bool HasOpenActivities(int Record_ID)
        {
            Record_Activity activity = _db.Record_Activities.Where(act => act.Record_ID == Record_ID && act.Actual_Date == null).FirstOrDefault();
            if (activity != null)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        #endregion
    }
}
