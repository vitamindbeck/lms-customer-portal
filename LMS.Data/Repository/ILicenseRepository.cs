﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMS.Data.Repository
{
    public interface ILicenseRepository
    {
        License GetLicenseByID(int License_ID);
    }
}
