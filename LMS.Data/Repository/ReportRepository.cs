﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMS.Data.Repository
{
    public class ReportRepository : IReportRepository
    {
        private LMSDataContext _db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

        #region IReportRepository Members

        public Dictionary<string, int> GetLeadsByMonth(int month, int year)
        {
            Dictionary<string, int> LeadsByMonth = new Dictionary<string, int>();

            DateTime startDate = new DateTime(year, month, 1);
            DateTime endDate = new DateTime((month == 12 ? year+1 : year), (month == 12 ? 1 : month + 1), 1);

            List<string> sourceCodesThisMonth = new List<string>();

            var sources = from r in _db.Records
                          where r.Created_On > startDate && r.Created_On < endDate
                          group r by r.Source_Code.Source_Number into rs
                          select new { Sourcecode = rs.Key, Records = rs };

            foreach (var r in sources)
            {
                LeadsByMonth.Add(r.Sourcecode.ToString(), r.Records.Count());
            }

            return LeadsByMonth;
        }

        public List<Agent_App_Sent_ReportResult> GetAgentAppSentReport(DateTime FromDate, DateTime ToDate, int Agent_ID)
        {
            return _db.Agent_App_Sent_Report(FromDate, ToDate, Agent_ID).ToList();
        }

        public List<Agent_App_Received_ReportResult> GetAgentAppReceivedReport(DateTime FromDate, DateTime ToDate, int Agent_ID)
        {
            return _db.Agent_App_Received_Report(FromDate, ToDate, Agent_ID).ToList();
        }

        public List<Agent_Leads_By_Source_ReportResult> GetAgentLeadBySourceReport(DateTime FromDate, DateTime ToDate, int Agent_ID)
        {
            return _db.Agent_Leads_By_Source_Report(FromDate, ToDate, Agent_ID).ToList();
        }

        public string RunReport(DateTime StartDate, DateTime EndDate, int StartHour, int EndHour, string SourceCodeIDList, string GroupIDList, string AgentList)
        {
            string _FILEPATH = "";

            var _ReportData = _db.Report_Advanced_Lead_Decay(StartDate, EndDate);

            return _FILEPATH;
        }

        public List<Agent_App_Placed_ReportResult> GetAgentAppPlacedReport(DateTime FromDate, DateTime ToDate, int Agent_ID)
        {
            return _db.Agent_App_Placed_Report(FromDate, ToDate, Agent_ID).ToList();
        }

        public List<sp_Agent_Call_of_Week_ReportResult> GetAgentCOWs(DateTime FromDate, DateTime ToDate, int Agent_ID, Boolean COW)
        {
            return _db.sp_Agent_Call_of_Week_Report(FromDate, ToDate, Agent_ID,COW).ToList();
        }

        #endregion
    }
}
