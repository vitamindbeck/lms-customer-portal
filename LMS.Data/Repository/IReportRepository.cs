﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMS.Data.Repository
{
    public interface IReportRepository
    {
        Dictionary<string, int> GetLeadsByMonth(int month, int year);
        List<Agent_App_Sent_ReportResult> GetAgentAppSentReport(DateTime FromDate, DateTime ToDate, int Agent_ID);
        List<Agent_App_Received_ReportResult> GetAgentAppReceivedReport(DateTime FromDate, DateTime ToDate, int Agent_ID);
        List<Agent_App_Placed_ReportResult> GetAgentAppPlacedReport(DateTime FromDate, DateTime ToDate, int Agent_ID);
        List<Agent_Leads_By_Source_ReportResult> GetAgentLeadBySourceReport(DateTime FromDate, DateTime ToDate, int Agent_ID);
        List<sp_Agent_Call_of_Week_ReportResult> GetAgentCOWs(DateTime FromDate, DateTime ToDate, int Agent_ID,Boolean COW);
        
        string RunReport(DateTime StartDate, DateTime EndDate, int StartHour, int EndHour, string SourceCodeIDList, string GroupIDList, string AgentList);
    }
}
