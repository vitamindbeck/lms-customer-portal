﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMS.Data.Repository
{
   public interface ICallRatingRepository
    {
        void Save();
        void Update(CallRating callRating);
        CallRating GetCallRatingByActivityID(int activityID);
    }


}
