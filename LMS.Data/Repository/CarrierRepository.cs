﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMS.Data.Repository
{
    public class CarrierRepository : ICarrierRepository
    {
        private LMSDataContext _db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

        #region ICarrierRepository Members

        public Carrier Get(int CarrierID)
        {
            return _db.Carriers.SingleOrDefault(c => c.Carrier_ID == CarrierID);
        }

        public void Insert(Carrier carrier)
        {
            _db.Carriers.InsertOnSubmit(carrier);
        }

        public void Delete(Carrier carrier)
        {
            _db.Carriers.DeleteOnSubmit(carrier);
        }

        public void Save()
        {
            _db.SubmitChanges();
        }

        public IEnumerable<Carrier> GetAll()
        {
            return _db.Carriers.OrderBy(c => c.Name);
        }

        #endregion
    }
}
