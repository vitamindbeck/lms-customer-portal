﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMS.Data.Repository
{


    public class AgentRepository : IAgentRepository
    {
        LMSDataContext _db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
        

        #region IAgentRepository Members

        public List<sp_Agent_ListResult> GetLicenseAgentList()
        {
            _db.CommandTimeout = 0;
            return _db.sp_Agent_List().ToList();
        }

        public Agent GetAgentByID(int Agent_ID)
        {
            return _db.Agents.Where(a => a.Agent_ID == Agent_ID).FirstOrDefault();
        }

        public Agent GetAgencyByID(int Associated_Agency_ID)
        {
            return _db.Agents.Where(a => a.Agent_ID  == Associated_Agency_ID).FirstOrDefault();

            
        }

        public Agent GetAgentByUserID(int User_ID)
        {
            return _db.Agents.Where(a => a.User_ID == User_ID).FirstOrDefault();
        }

        public Agent GetAssociatedAgencyDetails(int Associated_Agency_ID)
        {
            return _db.Agents.Where(a => a.Associated_Agency_ID == Associated_Agency_ID).FirstOrDefault();
        }

        //Get agency List
        public List<Agent> GetAgencyList(bool includeBlank)
        {
            var agencyList = _db.Agents.Where(sc => sc.Agent_Type_ID == 2).OrderBy(s => s.Agency_Name).ToList();
            if (includeBlank)
            {
                Agent blank = new Agent();
                blank.Agency_Name = "-- Choose Agency --";
                blank.Associated_Agency_ID = -1;
                agencyList.Insert(0, blank);
            }
            return agencyList;
        }
        #endregion
    }
}
