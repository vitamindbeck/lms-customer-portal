﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMS.Data.Repository
{
    public interface IRecordRepository
    {
        List<NewLeadsResult> GetNewLeads(int Agent_ID, int Security_Level);
        List<NewLeadsByPodsResult> GetNewLeadsByPods(int Pod_ID, int Security_Level);
        List<UnassignedLeadsResult> GetUnassignedLeads();
        List<MyUnassignedLeadsResult> GetMyUnassinedLeads(string username);
        List<OtherUnassignedLeadsResult> GetOtherUnassinedLeads(string username);
        List<FollowUpsResult> GetAgentFollowUps(int Agent_ID);
        List<FollowUpsByPodsResult> GetAgentFollowUpsByPods(int Agent_ID);
        List<AppointmentFollowUpsResult> GetAgentAppointmentFollowUps(int Agent_ID);
        List<AppointmentFollowUpsByPodsResult> GetAgentAppointmentFollowUpsByPods(int Pod_ID);
        List<InitialApplicationSentFollowUpResult> GetInitialApplicationSentFollowUps();
        List<ApplicationSentFollowUpAfterInitialResult> GetApplicationSentFollowUpsAfterInitial();
        List<AppSentOpenActivitiesResult> GetAppSentOpenActivities(int Agent_ID);
        List<AppSentOpenActivitiesByPodsResult> GetAppSentOpenActivitiesByPods(int Pod_ID);
        List<GetRecordActivitiesDisplayResult> GetRecordActivitiesDisplay(int Record_ID);
        List<sp_Get_Record_Distribution_ListResult> GetRecordDistributionList(int Record_ID);
        List<sp_Get_Distribution_ListResult> GetDistributionList(int Record_ID);
        Record GetRecord(int Record_ID);
        void Update(Record record);
        void Insert(Record record);
        void Delete(Record record);
        void Delete(int Record_ID);
        Company GetCompany();
        void UpdateCompany(Company Company);
        void DeleteCompany(Company Company);
        void Save();
        List<Record> Search(string FirstName, string LastName, int SourceCode, int AgentID, 
            int AssistantAgentID, string PhoneNumber, int RecordStatus, int StateID, 
            string ZipCode, string CreatedOnFrom, string CreatedOnTo, int BuyingPeriodID, string FollowUpFrom,
            string FollowUpTo, int BuyingPeriodSubID, string EmailAddress, int FollowUpAgentID, string ContactName,
            string ContactEmail, string ContactPhone, int PlanStatus, string Customer_ID, bool BadTypes, int PlanSrcCodeID, int ReconcilliationType,
            int TransactionDateType, bool OpenActivities, string TransactionDateFromValue, string TransactionDateToValue,
            int ActivityTypeID, int RecordEligibilityID, int ActivityStatusID, int CurrentSituationID, int PriorityID, string DistributionListIds);
        List<Record> QuickSearch(string FirstName, string MiddleName ,string LastName, int StateID, string DOB, string EmailAddress);
        List<GetLeadProspectResult> GetProspects(int Agent_ID);
        List<GetLeadProspectResult> GetLeads(int Agent_ID);
        void AssignLead(int Record_ID, int Agent_ID);
        void AssignSalesCoordinator(int Record_ID, string User_Name);
        Record GetPartner(int Original_Record_ID);
        void BadLead(int Record_ID);
        void DuplicateLead(int Record_ID);
        void RepeatLead(int Record_ID);
        List<SupportDisplayType> GetSupportLeads(int Agent_ID);
        List<SupportDisplayType> GetFulfillmentActivities(int User_ID);
        List<GetLeadProspectResult> GetLeadsByType(int Agent_ID, string Type);
        List<ReconciliationsResult> GetReconciliations();
        List<Get_Plan_Transaction_Desc_Status_DateResult> GetPlanTransactionDescStatusDate(int Record_ID);
        void SetRecordType(int RecordID, int RecordTypeID);
        List<Report_Lead_Time_ReportResult> GetLeadTimeReport(DateTime StartDate, DateTime EndDate);
        IQueryable<Record> GetAppointmentLeads(int User_ID);
        void AssignAppointmentSetter(int Record_ID, int Agent_ID);
        IQueryable<Record> NonBillable(int Agent_ID);
        User GetUserByID(int userID);
        Complaint GetComplaint(int Record_ID);
        List<GetAllOpenComplaintsResult> GetAllOpenComplaints();

    }
}
