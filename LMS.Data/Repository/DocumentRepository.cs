﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMS.Data.Repository
{
    public class DocumentRepository
    {
        private LMSDataContext _DB = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

        public IEnumerable<Plan_Document> GetAllByPlan(int Plan_ID, int State_ID)
        {
            var docs = from d in _DB.Plan_Documents
                       where d.Plan_ID == Plan_ID && d.State_ID == State_ID
                       select d;
            return docs;
        }

    }
}
