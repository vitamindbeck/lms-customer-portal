﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMS.Data.Repository
{
    public class PlanTransactionRepository : IPlanTransactionRepository
    {
        LMSDataContext _db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

        #region IPlanTransactionRepository Members

        public Plan_Transaction Get(int Plan_Transaction_ID)
        {
            return _db.Plan_Transactions.SingleOrDefault(pt => pt.Plan_Transaction_ID == Plan_Transaction_ID);
        }

        public void Insert(Plan_Transaction transaction)
        {
            _db.Plan_Transactions.InsertOnSubmit(transaction);
            _db.SubmitChanges();
            _db.CreatePlanTransactionDates(transaction.Plan_Transaction_ID);

        }

        public void Update(Plan_Transaction transaction)
        {
            _db.Plan_Transactions.InsertOnSubmit(transaction);
        }

        public void Delete(Plan_Transaction transaction)
        {
            _db.Plan_Transactions.DeleteOnSubmit(transaction);
        }

        public void Save()
        {
            _db.SubmitChanges();
        }

        public List<Transaction_Date_Type> GetDateTypes(int GroupID)
        {
            List<Transaction_Date_Type> Types = new List<Transaction_Date_Type>();

            var planids = from c in _db.Group_Transaction_Date_Types
                          where c.Group_ID == GroupID || c.Group_ID==5
                          orderby c.Display_Order
                          select c.Transaction_Date_Type_ID;

            foreach (int pid in planids)
            {
                Transaction_Date_Type dateType = _db.Transaction_Date_Types.FirstOrDefault(tdt => tdt.Transaction_Date_Type_ID == pid);
                Types.Add(dateType);
            }

          
            return Types;

            

        }

        public void UpdateSendAppDate(int Plan_Transaction_ID)
        {
            Plan_Transaction transaction = _db.Plan_Transactions.First(p => p.Plan_Transaction_ID == Plan_Transaction_ID);

            Plan_Transaction_Date date = _db.Plan_Transaction_Dates.First(d => d.Plan_Transaction_ID == Plan_Transaction_ID && d.Transaction_Date_Type_ID == 18);

            transaction.Plan_Transaction_Status_ID = 20;
            date.Date_Value = DateTime.Now;

            _db.SubmitChanges();

        }

        public void UpdateAppSentDateAndAppRequestDate(int Plan_Transaction_ID)
        {
            Plan_Transaction transaction = _db.Plan_Transactions.First(p => p.Plan_Transaction_ID == Plan_Transaction_ID);

            Plan_Transaction_Date date1 = _db.Plan_Transaction_Dates.First(d => d.Plan_Transaction_ID == Plan_Transaction_ID && d.Transaction_Date_Type_ID == 2);
            Plan_Transaction_Date date2 = _db.Plan_Transaction_Dates.First(d => d.Plan_Transaction_ID == Plan_Transaction_ID && d.Transaction_Date_Type_ID == 18);

            transaction.Plan_Transaction_Status_ID = 1; //application sent
            date1.Date_Value = DateTime.Now;
            date2.Date_Value = DateTime.Now;

            _db.SubmitChanges();

        }

        // For PreFill App Option
        public void UpdatePreFillDate(int Plan_Transaction_ID)
        {
            Plan_Transaction transaction = _db.Plan_Transactions.First(p => p.Plan_Transaction_ID == Plan_Transaction_ID);

            //Transaction date Set as Application Request Date
            Plan_Transaction_Date date = _db.Plan_Transaction_Dates.First(d => d.Plan_Transaction_ID == Plan_Transaction_ID && d.Transaction_Date_Type_ID == 18);

            //Status set as Request Application
            transaction.Plan_Transaction_Status_ID = 20;
            transaction.PreFillApp = true;
            date.Date_Value = DateTime.Now;

            _db.SubmitChanges();
        }


        public void UpdateSendInfoDate(int Plan_Transaction_ID)
        {
            Plan_Transaction transaction = _db.Plan_Transactions.First(p => p.Plan_Transaction_ID == Plan_Transaction_ID);

            Plan_Transaction_Date date = _db.Plan_Transaction_Dates.First(d => d.Plan_Transaction_ID == Plan_Transaction_ID && d.Transaction_Date_Type_ID == 19);

            transaction.Plan_Transaction_Status_ID = 21;
            date.Date_Value = DateTime.Now;

            _db.SubmitChanges();
        }

        public void UpdateAppCancelledDate(int Plan_Transaction_ID)
        {
            Plan_Transaction transaction = _db.Plan_Transactions.First(p => p.Plan_Transaction_ID == Plan_Transaction_ID);

            Plan_Transaction_Date date = _db.Plan_Transaction_Dates.First(d => d.Plan_Transaction_ID == Plan_Transaction_ID && d.Transaction_Date_Type_ID == 8);

            if (transaction != null)
                transaction.Plan_Transaction_Status_ID = 18;

            if (date != null)
                date.Date_Value = DateTime.Now;

            _db.SubmitChanges();
        }

        public List<Plan_Transaction> GetRecordPlans(int Record_ID)
        {
            return _db.Plan_Transactions.Where(pt => pt.Record_ID == Record_ID).ToList();
        }

        #endregion
    }
}
