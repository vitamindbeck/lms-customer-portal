﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMS.Data.Repository
{
    public class CallRatingRepository: ICallRatingRepository 
    {

        private LMSDataContext _db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
        public void Update(CallRating callRating)
        {
            _db.CallRatings.InsertOnSubmit(callRating);
        }


        public void Save()
        {
            _db.SubmitChanges();
        }

        public CallRating GetCallRatingByActivityID(int activityID)
        {

            return _db.CallRatings.FirstOrDefault(c => c.Record_Activity_ID == activityID);


        }


    }
}
