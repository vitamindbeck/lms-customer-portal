﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMS.Data.Repository
{
    public interface IAppointmentRepository
    {
        Appointment GetAppointmentByID(int Appointment_ID);
    }
}
