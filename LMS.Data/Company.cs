﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq.Mapping;
using System.Data.Linq;
namespace LMS.Data
{
   public partial class Company
    {
       private EntityRef<State> _State;
       public Industry Industry
       {
           get
           {
               LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
               return db.Industries.First(i => i.Industry_ID == _Industry_ID);
           }
       }
       
       [Association(Name = "State", Storage = "_State", ThisKey = "State_ID", OtherKey = "State_ID", IsUnique = true, IsForeignKey = false)]
       public State State
       {
           get
           {
               return this._State.Entity;
           }
       }

    }
}
