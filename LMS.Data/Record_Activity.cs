﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq.Mapping;
using System.Data.Linq;

namespace LMS.Data
{
    public partial class Record_Activity
    {
        private EntityRef<Plan_Transaction> _Plan_Transaction;
        private EntityRef<Activity_Type> _Activity_Type;
        private EntityRef<Record_Activity_Status> _Record_Activity_Status;
        
        [Association(Name = "Plan_Transaction", Storage = "_Plan_Transaction", ThisKey = "Plan_Transaction_ID", OtherKey = "Plan_Transaction_ID", IsUnique = true, IsForeignKey = false)]
        public Plan_Transaction Plan_Transaction
        {
            get
            {
                return this._Plan_Transaction.Entity;
            }
        }

        [Association(Name = "Record_Activity_Status", Storage = "_Record_Activity_Status", ThisKey = "Record_Activity_Status_ID", OtherKey = "Record_Activity_Status_ID", IsUnique = true, IsForeignKey = false)]
        public Record_Activity_Status Record_Activity_Status
        {
            get
            {
                return this._Record_Activity_Status.Entity;
            }
        }

        public string CompletedBy
        {
            get
            {
                LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
                if ( User_ID > 0 )
                {
                    return db.Users.First(u => u.User_ID == _User_ID).FullName;
                }
                else
                {
                    return "";
                }
            }
        }

        public string Reconciliation_Type
        {
            get
            {
                
                LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
                Reconciliation R = db.Reconciliations.Where(n => n.Record_Activity_ID == _Record_Activity_ID).FirstOrDefault();
                if ( R == null)
                {
                    return "";
                }
                else
                {
                    Reconciliation_Type RT = db.Reconciliation_Types.Where(n => n.Reconciliation_Type_ID == R.Reconciliation_Type_ID).FirstOrDefault();
                    return RT.Description;
                }
            }
        }

        [Association(Name = "Activity_Type", Storage = "_Activity_Type", ThisKey = "Activity_Type_ID", OtherKey = "Activity_Type_ID", IsUnique = true, IsForeignKey = false)]
        public Activity_Type Activity_Type
        {
            get
            {
                return this._Activity_Type.Entity;
            }
        }


        #region Events

        partial void OnMade_ContactChanging(bool value)
        {
            
        }

        partial void OnValidate(ChangeAction action)
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

            Activity_Type actType = db.Activity_Types.Where(a => a.Activity_Type_ID == _Activity_Type_ID).FirstOrDefault();

            if ( actType != null )
                if (actType.Assign_To > 0)
                    _Activity_Type_ID = actType.Assign_To;

            Record r = db.Records.SingleOrDefault(rec => rec.Record_ID == _Record_ID);

            if ((_Record_Activity_Status_ID == 14 & r.Record_Eligibility_ID != 99) ||
                (_Record_Activity_Status_ID == 17 & r.Record_Eligibility_ID != 99) || 
                (_Record_Activity_Status_ID == 15 & r.Record_Eligibility_ID != 99) ||
                (_Record_Activity_Status_ID == 16 & r.Record_Eligibility_ID != 99) ||
                ( _Record_Activity_Status_ID == 18 & r.Record_Eligibility_ID != 99)
                )
                {
                    _Made_Contact = true;
                    db.Set_Record_Type(r.Record_ID,3);
                }
           
                else 
                {
                    _Made_Contact = false;
                }

            if (action == ChangeAction.Update || action == ChangeAction.Insert)
            {



                if (this.Made_Contact)
                {

                    //Get Status
                    int Status = r.Record_Type_ID;

                    //If status was Response Change to Prospect
                    if (Status == 1 || Status == 2 || Status == 9)
                    {
                        r.Record_Type_ID = 3;
                        r.Last_Updated_Date = DateTime.Now;

                    }
                }
                else
                {
                    //Get Status
                    int Status = r.Record_Type_ID;

                    //If status Live Transfer, Contacted, Set Appointment and Eligibility Unknown Set to record type to response
                    if (Status == 1)
                    {
                        if ((_Record_Activity_Status_ID == 14 & r.Record_Eligibility_ID == 99) ||                            
                            (_Record_Activity_Status_ID == 15 & r.Record_Eligibility_ID == 99) ||
                            (_Record_Activity_Status_ID == 16 & r.Record_Eligibility_ID == 99))
                        {
                            r.Record_Type_ID = 1;
                        }

                        else
                        {
                            r.Record_Type_ID = 2;
                            r.Last_Updated_Date = DateTime.Now;

                        }
                        
                    }
                }

                if (action == ChangeAction.Insert)
                {
                    r.Follow_Up_Date = this.Due_Date;
                }
            }

            r.Last_Updated_Date = DateTime.Now;

            db.SubmitChanges();
        }
                
        #endregion
    }
}
