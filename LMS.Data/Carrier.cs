﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMS.Data
{
    partial class Carrier
    {
        private LMSDataContext _db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

        public State State
        {
            get { return _db.States.FirstOrDefault(s => s.State_ID==_State_ID); } 
        }


    }
}
