using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Record_Distribution_Type
    {
        public int Record_Distribution_Type_ID { get; set; }
        public string Record_Distribution_Type_Description { get; set; }
    }
}
