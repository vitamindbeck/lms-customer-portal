using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Affinity_Partner
    {
        public int Affinity_Partner_ID { get; set; }
        public string Name { get; set; }
        public string Address_1 { get; set; }
        public string Address_2 { get; set; }
        public string City { get; set; }
        public int State_ID { get; set; }
        public string Phone { get; set; }
        public string Contact_Name { get; set; }
        public Nullable<int> Parent_ID { get; set; }
        public string Short_Name { get; set; }
        public bool Active { get; set; }
        public int Co_Branded_Site_ID { get; set; }
        public bool Application_Submit_Indicator { get; set; }
        public decimal Application_Submit_Commision { get; set; }
        public bool Display_Member_Indicator { get; set; }
        public bool Has_Plan_Preference { get; set; }
        public bool LTC { get; set; }
    }
}
