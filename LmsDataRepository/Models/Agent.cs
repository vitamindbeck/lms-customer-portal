using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Agent
    {
        public int Agent_ID { get; set; }
        public int Agent_Type_ID { get; set; }
        public int User_ID { get; set; }
        public string Email { get; set; }
        public string Agency_Name { get; set; }
        public Nullable<System.DateTime> Agent_Start_Date { get; set; }
        public Nullable<System.DateTime> Agent_End_Date { get; set; }
        public string NPN { get; set; }
        public byte[] SSN { get; set; }
        public string Tax_ID { get; set; }
        public bool Active { get; set; }
        public string Address_1 { get; set; }
        public string Address_2 { get; set; }
        public string City { get; set; }
        public string County { get; set; }
        public Nullable<int> State_ID { get; set; }
        public string Zip_Code { get; set; }
        public string Phone_Number { get; set; }
        public Nullable<int> Associated_Agency_ID { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public Nullable<System.DateTime> Last_Updated_Date { get; set; }
        public Nullable<int> Updated_By { get; set; }
        public Nullable<int> GBS_Agent_ID { get; set; }
        public virtual User User { get; set; }
    }
}
