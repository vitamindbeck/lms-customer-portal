using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Exam_Service
    {
        public int Exam_Service_ID { get; set; }
        public string Description { get; set; }
    }
}
