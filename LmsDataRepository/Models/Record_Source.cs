using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Record_Source
    {
        public Record_Source()
        {
            this.Records = new List<Record>();
        }

        public int Record_Source_ID { get; set; }
        public string Description { get; set; }
        public virtual ICollection<Record> Records { get; set; }
    }
}
