using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class vw_App_Received_2010_Temp
    {
        public string Agent_Group { get; set; }
        public string Agent { get; set; }
        public string Sales_Coord { get; set; }
        public int Record_ID { get; set; }
        public string Customer_ID { get; set; }
        public string State { get; set; }
        public string SourceCode { get; set; }
        public string Policy_ID { get; set; }
        public string Policy_Number { get; set; }
        public string Current_Plan_Status { get; set; }
        public string Carrier { get; set; }
        public string Plan_Name { get; set; }
        public string Plan_Type { get; set; }
        public Nullable<System.DateTime> Created_On { get; set; }
        public Nullable<System.DateTime> AppReceivedDate { get; set; }
        public Nullable<System.DateTime> AppSubmitDate { get; set; }
        public Nullable<System.DateTime> AppDeliveryDate { get; set; }
        public Nullable<System.DateTime> AppPlacedDate { get; set; }
        public Nullable<System.DateTime> FirstAppRec { get; set; }
        public Nullable<System.DateTime> LastAppRec { get; set; }
        public string New_Customer { get; set; }
        public string Primary_Lead { get; set; }
    }
}
