using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Feature_Category
    {
        public int Feature_Category_ID { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }
        public int Display_Order { get; set; }
        public string Single_Plan_Description { get; set; }
        public string Category_Code { get; set; }
    }
}
