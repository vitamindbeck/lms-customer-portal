using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class la_News_Section
    {
        public int News_Section_ID { get; set; }
        public string Title { get; set; }
        public string Title_URL { get; set; }
        public string Description { get; set; }
        public int Display_Order { get; set; }
        public bool Active { get; set; }
    }
}
