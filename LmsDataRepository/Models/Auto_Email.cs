using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Auto_Email
    {
        public Auto_Email()
        {
            this.Record_Activity = new List<Record_Activity>();
        }

        public int Auto_Email_ID { get; set; }
        public Nullable<int> Source_Code_ID { get; set; }
        public string Source_Code_IDs { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string From { get; set; }
        public Nullable<bool> HTML { get; set; }
        public bool Include_Agent { get; set; }
        public bool Active { get; set; }
        public string Source_Number { get; set; }
        public string Notes { get; set; }
        public Nullable<bool> LMS_Display { get; set; }
        public virtual ICollection<Record_Activity> Record_Activity { get; set; }
    }
}
