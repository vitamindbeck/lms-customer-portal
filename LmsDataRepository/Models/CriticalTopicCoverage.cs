using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class CriticalTopicCoverage
    {
        public int CTCID { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }
    }
}
