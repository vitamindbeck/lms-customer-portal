using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class PDF
    {
        public int PDFID { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public int PlanID { get; set; }
        public int StateID { get; set; }
    }
}
