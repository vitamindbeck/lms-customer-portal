using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Buying_Period
    {
        public int Buying_Period_ID { get; set; }
        public string Description { get; set; }
        public Nullable<int> Display_Order { get; set; }
        public Nullable<bool> Active { get; set; }
    }
}
