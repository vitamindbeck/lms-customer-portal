using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class CallRating
    {
        public int Call_Rating_ID { get; set; }
        public Nullable<bool> Call_of_Week { get; set; }
        public int Agent_Rating { get; set; }
        public Nullable<int> Manager_Rating { get; set; }
        public int Record_Activity_ID { get; set; }
        public virtual Record_Activity Record_Activity { get; set; }
    }
}
