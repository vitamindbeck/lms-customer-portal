using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Plan_Transaction
    {
        public Plan_Transaction()
        {
            this.Record_Activity = new List<Record_Activity>();
        }

        public int Plan_Transaction_ID { get; set; }
        public int Plan_ID { get; set; }
        public int Record_ID { get; set; }
        public Nullable<int> Transaction_Number { get; set; }
        public Nullable<System.DateTime> Created_On { get; set; }
        public Nullable<bool> Active { get; set; }
        public Nullable<int> Agent_ID { get; set; }
        public Nullable<int> Assistant_Agent_ID { get; set; }
        public Nullable<int> Source_Code_ID { get; set; }
        public Nullable<bool> Cash_With_Application { get; set; }
        public Nullable<bool> Cash_With_Application_Check { get; set; }
        public Nullable<decimal> Cash_With_Application_Amount { get; set; }
        public Nullable<int> Outbound_Shipping_Method_ID { get; set; }
        public string Tracking_Number_Inbound { get; set; }
        public string Tracking_Number_Outbound { get; set; }
        public Nullable<int> Payment_Mode_ID { get; set; }
        public Nullable<decimal> Annual_Premium { get; set; }
        public Nullable<decimal> Modal_Premium { get; set; }
        public Nullable<int> Exam_Service_ID { get; set; }
        public string Comments { get; set; }
        public Nullable<decimal> Deposit_Amount { get; set; }
        public Nullable<decimal> Coverage_Amount { get; set; }
        public Nullable<int> Plan_Transaction_Status_ID { get; set; }
        public Nullable<int> Parent_Plan_ID { get; set; }
        public int Group_ID { get; set; }
        public string GBS_ID { get; set; }
        public int Mode_ID { get; set; }
        public Nullable<int> Inbound_Shipping_Method_ID { get; set; }
        public bool eApp { get; set; }
        public Nullable<int> Underwriting_Class_ID { get; set; }
        public int Modal_ID { get; set; }
        public bool Replacement { get; set; }
        public string Replacement_Details { get; set; }
        public string Best_Time_For_Exam { get; set; }
        public string Exam_Details { get; set; }
        public string Special_Instructions { get; set; }
        public bool Qualified_Transfer { get; set; }
        public bool Non_Qualified_1035_Exchange { get; set; }
        public string Exam_Ref_Number { get; set; }
        public bool Initial_Enrollment { get; set; }
        public bool Guaranteed_Issue { get; set; }
        public byte[] Policy_Number { get; set; }
        public string Sub_Plan { get; set; }
        public Nullable<int> Buying_Period_ID { get; set; }
        public Nullable<int> Buying_Period_Sub_ID { get; set; }
        public Nullable<System.DateTime> Buying_Period_Effective_Date { get; set; }
        public Nullable<System.DateTime> Time_Stamp { get; set; }
        public bool PreFillApp { get; set; }
        public Nullable<int> Case_Manager_ID { get; set; }
        public Nullable<System.DateTime> Status_Date { get; set; }
        public virtual Plan Plan { get; set; }
        public virtual ICollection<Record_Activity> Record_Activity { get; set; }
    }
}
