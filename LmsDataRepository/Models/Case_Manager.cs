using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Case_Manager
    {
        public int Case_Manager_ID { get; set; }
        public string Name { get; set; }
    }
}
