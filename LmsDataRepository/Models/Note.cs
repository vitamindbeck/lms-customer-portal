using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Note
    {
        public Note()
        {
            this.CustomerProfiles = new List<CustomerProfile>();
            this.CustomerProfiles1 = new List<CustomerProfile>();
        }

        public int Id { get; set; }
        public int TypeofNote { get; set; }
        public string NoteText { get; set; }
        public virtual ICollection<CustomerProfile> CustomerProfiles { get; set; }
        public virtual ICollection<CustomerProfile> CustomerProfiles1 { get; set; }
    }
}
