using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class la_Press_Release
    {
        public int la_Press_Release_ID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string File_URL { get; set; }
        public bool Active { get; set; }
        public System.DateTime Release_Date { get; set; }
    }
}
