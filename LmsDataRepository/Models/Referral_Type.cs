using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Referral_Type
    {
        public int Referral_Type_ID { get; set; }
        public string Referral_Description { get; set; }
        public int Display_Order { get; set; }
        public Nullable<bool> Active { get; set; }
    }
}
