using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class vw_NextFollowUpbyPlanTransaction
    {
        public int Plan_Transaction_ID { get; set; }
        public Nullable<int> Record_Activity_ID { get; set; }
    }
}
