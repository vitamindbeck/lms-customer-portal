using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class vw_Policies_Placed
    {
        public int Plan_Transaction_ID { get; set; }
        public Nullable<System.DateTime> StatusDate { get; set; }
    }
}
