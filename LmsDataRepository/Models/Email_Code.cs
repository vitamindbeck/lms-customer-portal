using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Email_Code
    {
        public int Email_Code_ID { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public bool Include_Disclaimer { get; set; }
        public bool Agent_Specific { get; set; }
        public string Name { get; set; }
        public string Email_Address { get; set; }
        public bool HTML { get; set; }
        public string Email_Date_Type { get; set; }
    }
}
