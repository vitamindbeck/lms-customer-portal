using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Media_Type
    {
        public int Media_Type_ID { get; set; }
        public string Media_Type_Description { get; set; }
    }
}
