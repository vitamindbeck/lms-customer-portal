using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Plan_Type
    {
        public int Plan_Type_ID { get; set; }
        public string Description { get; set; }
        public int Display_Order { get; set; }
        public bool Active { get; set; }
        public Nullable<int> GBS_Product_Type_ID { get; set; }
    }
}
