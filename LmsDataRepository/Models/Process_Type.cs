using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Process_Type
    {
        public int Process_Type_ID { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
    }
}
