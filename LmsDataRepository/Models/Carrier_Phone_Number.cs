using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Carrier_Phone_Number
    {
        public int Carrier_Phone_Number_ID { get; set; }
        public int Carrier_ID { get; set; }
        public int Phone_Type_ID { get; set; }
        public string Phone_Number { get; set; }
        public int Display_Order { get; set; }
    }
}
