using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class la_Industry_SectionMap : EntityTypeConfiguration<la_Industry_Section>
    {
        public la_Industry_SectionMap()
        {
            // Primary Key
            this.HasKey(t => t.la_Industry_Section_ID);

            // Properties
            this.Property(t => t.Title)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("la_Industry_Section");
            this.Property(t => t.la_Industry_Section_ID).HasColumnName("la_Industry_Section_ID");
            this.Property(t => t.Title).HasColumnName("Title");
            this.Property(t => t.Display_Order).HasColumnName("Display_Order");
            this.Property(t => t.Active).HasColumnName("Active");
        }
    }
}
