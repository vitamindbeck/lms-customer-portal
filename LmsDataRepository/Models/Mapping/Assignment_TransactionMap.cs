using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Assignment_TransactionMap : EntityTypeConfiguration<Assignment_Transaction>
    {
        public Assignment_TransactionMap()
        {
            // Primary Key
            this.HasKey(t => t.Assignment_Transaction_ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("Assignment_Transaction");
            this.Property(t => t.Assignment_Transaction_ID).HasColumnName("Assignment_Transaction_ID");
            this.Property(t => t.Client_ID).HasColumnName("Client_ID");
            this.Property(t => t.Previous_Agent_ID).HasColumnName("Previous_Agent_ID");
            this.Property(t => t.Current_Agent_ID).HasColumnName("Current_Agent_ID");
            this.Property(t => t.Transaction_Date).HasColumnName("Transaction_Date");
        }
    }
}
