using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Record_AddressMap : EntityTypeConfiguration<Record_Address>
    {
        public Record_AddressMap()
        {
            // Primary Key
            this.HasKey(t => t.Record_Address_ID);

            // Properties
            this.Property(t => t.Address_Line_1)
                .HasMaxLength(200);

            this.Property(t => t.Address_Line_2)
                .HasMaxLength(200);

            this.Property(t => t.City)
                .HasMaxLength(100);

            this.Property(t => t.Zipcode)
                .HasMaxLength(50);

            this.Property(t => t.County)
                .HasMaxLength(100);

            this.Property(t => t.Header)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("Record_Address");
            this.Property(t => t.Record_Address_ID).HasColumnName("Record_Address_ID");
            this.Property(t => t.Record_ID).HasColumnName("Record_ID");
            this.Property(t => t.Address_Line_1).HasColumnName("Address_Line_1");
            this.Property(t => t.Address_Line_2).HasColumnName("Address_Line_2");
            this.Property(t => t.City).HasColumnName("City");
            this.Property(t => t.State_ID).HasColumnName("State_ID");
            this.Property(t => t.Zipcode).HasColumnName("Zipcode");
            this.Property(t => t.County).HasColumnName("County");
            this.Property(t => t.IsMailingAddress).HasColumnName("IsMailingAddress");
            this.Property(t => t.Header).HasColumnName("Header");
        }
    }
}
