using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Plan_TypeMap : EntityTypeConfiguration<Plan_Type>
    {
        public Plan_TypeMap()
        {
            // Primary Key
            this.HasKey(t => t.Plan_Type_ID);

            // Properties
            this.Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("Plan_Type");
            this.Property(t => t.Plan_Type_ID).HasColumnName("Plan_Type_ID");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Display_Order).HasColumnName("Display_Order");
            this.Property(t => t.Active).HasColumnName("Active");
            this.Property(t => t.GBS_Product_Type_ID).HasColumnName("GBS_Product_Type_ID");
        }
    }
}
