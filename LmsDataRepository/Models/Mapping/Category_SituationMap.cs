using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Category_SituationMap : EntityTypeConfiguration<Category_Situation>
    {
        public Category_SituationMap()
        {
            // Primary Key
            this.HasKey(t => t.CategorySituationID);

            // Properties
            this.Property(t => t.CategorySituation)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Category_Situation");
            this.Property(t => t.CategorySituationID).HasColumnName("CategorySituationID");
            this.Property(t => t.CategorySituation).HasColumnName("CategorySituation");
            this.Property(t => t.CategorySituationDesc).HasColumnName("CategorySituationDesc");
        }
    }
}
