using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class AgentMap : EntityTypeConfiguration<Agent>
    {
        public AgentMap()
        {
            // Primary Key
            this.HasKey(t => t.Agent_ID);

            // Properties
            this.Property(t => t.Email)
                .HasMaxLength(100);

            this.Property(t => t.Agency_Name)
                .HasMaxLength(45);

            this.Property(t => t.NPN)
                .HasMaxLength(35);

            this.Property(t => t.SSN)
                .HasMaxLength(128);

            this.Property(t => t.Tax_ID)
                .HasMaxLength(20);

            this.Property(t => t.Address_1)
                .HasMaxLength(50);

            this.Property(t => t.Address_2)
                .HasMaxLength(50);

            this.Property(t => t.City)
                .HasMaxLength(50);

            this.Property(t => t.County)
                .HasMaxLength(50);

            this.Property(t => t.Zip_Code)
                .HasMaxLength(10);

            this.Property(t => t.Phone_Number)
                .HasMaxLength(25);

            // Table & Column Mappings
            this.ToTable("Agent");
            this.Property(t => t.Agent_ID).HasColumnName("Agent_ID");
            this.Property(t => t.Agent_Type_ID).HasColumnName("Agent_Type_ID");
            this.Property(t => t.User_ID).HasColumnName("User_ID");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.Agency_Name).HasColumnName("Agency_Name");
            this.Property(t => t.Agent_Start_Date).HasColumnName("Agent_Start_Date");
            this.Property(t => t.Agent_End_Date).HasColumnName("Agent_End_Date");
            this.Property(t => t.NPN).HasColumnName("NPN");
            this.Property(t => t.SSN).HasColumnName("SSN");
            this.Property(t => t.Tax_ID).HasColumnName("Tax_ID");
            this.Property(t => t.Active).HasColumnName("Active");
            this.Property(t => t.Address_1).HasColumnName("Address_1");
            this.Property(t => t.Address_2).HasColumnName("Address_2");
            this.Property(t => t.City).HasColumnName("City");
            this.Property(t => t.County).HasColumnName("County");
            this.Property(t => t.State_ID).HasColumnName("State_ID");
            this.Property(t => t.Zip_Code).HasColumnName("Zip_Code");
            this.Property(t => t.Phone_Number).HasColumnName("Phone_Number");
            this.Property(t => t.Associated_Agency_ID).HasColumnName("Associated_Agency_ID");
            this.Property(t => t.DOB).HasColumnName("DOB");
            this.Property(t => t.Last_Updated_Date).HasColumnName("Last_Updated_Date");
            this.Property(t => t.Updated_By).HasColumnName("Updated_By");
            this.Property(t => t.GBS_Agent_ID).HasColumnName("GBS_Agent_ID");

            // Relationships
            this.HasRequired(t => t.User)
                .WithMany(t => t.Agents)
                .HasForeignKey(d => d.User_ID);

        }
    }
}
