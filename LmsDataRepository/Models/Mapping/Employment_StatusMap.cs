using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Employment_StatusMap : EntityTypeConfiguration<Employment_Status>
    {
        public Employment_StatusMap()
        {
            // Primary Key
            this.HasKey(t => t.Employment_Status_ID);

            // Properties
            this.Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("Employment_Status");
            this.Property(t => t.Employment_Status_ID).HasColumnName("Employment_Status_ID");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Display_Order).HasColumnName("Display_Order");
        }
    }
}
