using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class PlanMap : EntityTypeConfiguration<Plan>
    {
        public PlanMap()
        {
            // Primary Key
            this.HasKey(t => t.Plan_ID);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(150);

            this.Property(t => t.About)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.Plan_Code)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Plan");
            this.Property(t => t.Plan_ID).HasColumnName("Plan_ID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Carrier_ID).HasColumnName("Carrier_ID");
            this.Property(t => t.Active).HasColumnName("Active");
            this.Property(t => t.About).HasColumnName("About");
            this.Property(t => t.GBS_Plan_ID).HasColumnName("GBS_Plan_ID");
            this.Property(t => t.Plan_Code).HasColumnName("Plan_Code");
            this.Property(t => t.Plan_Type_ID).HasColumnName("Plan_Type_ID");

            // Relationships
            this.HasRequired(t => t.Carrier)
                .WithMany(t => t.Plans)
                .HasForeignKey(d => d.Carrier_ID);

        }
    }
}
