using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Group_Transaction_Date_TypeMap : EntityTypeConfiguration<Group_Transaction_Date_Type>
    {
        public Group_Transaction_Date_TypeMap()
        {
            // Primary Key
            this.HasKey(t => t.Group_Transaction_Date_Type_ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("Group_Transaction_Date_Type");
            this.Property(t => t.Group_Transaction_Date_Type_ID).HasColumnName("Group_Transaction_Date_Type_ID");
            this.Property(t => t.Group_ID).HasColumnName("Group_ID");
            this.Property(t => t.Transaction_Date_Type_ID).HasColumnName("Transaction_Date_Type_ID");
            this.Property(t => t.Display_Order).HasColumnName("Display_Order");
            this.Property(t => t.Active).HasColumnName("Active");
        }
    }
}
