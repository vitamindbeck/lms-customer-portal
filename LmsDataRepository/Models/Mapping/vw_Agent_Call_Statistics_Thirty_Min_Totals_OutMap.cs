using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class vw_Agent_Call_Statistics_Thirty_Min_Totals_OutMap : EntityTypeConfiguration<vw_Agent_Call_Statistics_Thirty_Min_Totals_Out>
    {
        public vw_Agent_Call_Statistics_Thirty_Min_Totals_OutMap()
        {
            // Primary Key
            this.HasKey(t => t.Direction);

            // Properties
            this.Property(t => t.answer_ext)
                .HasMaxLength(10);

            this.Property(t => t.Direction)
                .IsRequired()
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("vw_Agent_Call_Statistics_Thirty_Min_Totals_Out");
            this.Property(t => t.answer_ext).HasColumnName("answer_ext");
            this.Property(t => t.CallTime).HasColumnName("CallTime");
            this.Property(t => t.MinTime).HasColumnName("MinTime");
            this.Property(t => t.MaxTime).HasColumnName("MaxTime");
            this.Property(t => t.Direction).HasColumnName("Direction");
        }
    }
}
