using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Note_HelperMap : EntityTypeConfiguration<Note_Helper>
    {
        public Note_HelperMap()
        {
            // Primary Key
            this.HasKey(t => t.Note_Helper_ID);

            // Properties
            this.Property(t => t.Note)
                .HasMaxLength(2000);

            // Table & Column Mappings
            this.ToTable("Note_Helper");
            this.Property(t => t.Note_Helper_ID).HasColumnName("Note_Helper_ID");
            this.Property(t => t.Note).HasColumnName("Note");
            this.Property(t => t.User_ID).HasColumnName("User_ID");
            this.Property(t => t.Note_Type).HasColumnName("Note_Type");
        }
    }
}
