using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class BrokerMap : EntityTypeConfiguration<Broker>
    {
        public BrokerMap()
        {
            // Primary Key
            this.HasKey(t => t.Broker_ID);

            // Properties
            this.Property(t => t.First_Name)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Address_1)
                .HasMaxLength(100);

            this.Property(t => t.Address_2)
                .HasMaxLength(100);

            this.Property(t => t.City)
                .HasMaxLength(100);

            this.Property(t => t.Zipcode)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.Email_Address)
                .HasMaxLength(100);

            this.Property(t => t.Last_Name)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("Broker");
            this.Property(t => t.Broker_ID).HasColumnName("Broker_ID");
            this.Property(t => t.First_Name).HasColumnName("First_Name");
            this.Property(t => t.Address_1).HasColumnName("Address_1");
            this.Property(t => t.Address_2).HasColumnName("Address_2");
            this.Property(t => t.City).HasColumnName("City");
            this.Property(t => t.State_ID).HasColumnName("State_ID");
            this.Property(t => t.Zipcode).HasColumnName("Zipcode");
            this.Property(t => t.Email_Address).HasColumnName("Email_Address");
            this.Property(t => t.Last_Name).HasColumnName("Last_Name");
        }
    }
}
