using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class vw_Screener_Dash_NC_NotEligibleMap : EntityTypeConfiguration<vw_Screener_Dash_NC_NotEligible>
    {
        public vw_Screener_Dash_NC_NotEligibleMap()
        {
            // Primary Key
            this.HasKey(t => t.Agent_ID);

            // Properties
            this.Property(t => t.Count_Date)
                .HasMaxLength(15);

            this.Property(t => t.Agent_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vw_Screener_Dash_NC_NotEligible");
            this.Property(t => t.Count_Date).HasColumnName("Count_Date");
            this.Property(t => t.IA_UL_NE).HasColumnName("IA_UL_NE");
            this.Property(t => t.Call_Status_NE).HasColumnName("Call_Status_NE");
            this.Property(t => t.Contacted_NE).HasColumnName("Contacted_NE");
            this.Property(t => t.Live_Transfer_NE).HasColumnName("Live_Transfer_NE");
            this.Property(t => t.Set_Apointment_NE).HasColumnName("Set_Apointment_NE");
            this.Property(t => t.Follow_Up_Screener_NE).HasColumnName("Follow_Up_Screener_NE");
            this.Property(t => t.Momentum).HasColumnName("Momentum");
            this.Property(t => t.Agent_ID).HasColumnName("Agent_ID");
        }
    }
}
