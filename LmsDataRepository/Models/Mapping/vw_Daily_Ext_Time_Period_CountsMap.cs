using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class vw_Daily_Ext_Time_Period_CountsMap : EntityTypeConfiguration<vw_Daily_Ext_Time_Period_Counts>
    {
        public vw_Daily_Ext_Time_Period_CountsMap()
        {
            // Primary Key
            this.HasKey(t => t.Agent_ID);

            // Properties
            this.Property(t => t.Call_Date)
                .HasMaxLength(30);

            this.Property(t => t.Agent_Ext)
                .HasMaxLength(10);

            this.Property(t => t.Agent_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vw_Daily_Ext_Time_Period_Counts");
            this.Property(t => t.Call_Date).HasColumnName("Call_Date");
            this.Property(t => t.Agent_Ext).HasColumnName("Agent_Ext");
            this.Property(t => t.Agent_ID).HasColumnName("Agent_ID");
            this.Property(t => t.Inbound_under_2min).HasColumnName("Inbound_under_2min");
            this.Property(t => t.Inbound_2_to_5min).HasColumnName("Inbound_2_to_5min");
            this.Property(t => t.Inbound_6_to_15min).HasColumnName("Inbound_6_to_15min");
            this.Property(t => t.Inbound_16_to_30min).HasColumnName("Inbound_16_to_30min");
            this.Property(t => t.Inbound_Over_30min).HasColumnName("Inbound_Over_30min");
            this.Property(t => t.Outbound_under_2min).HasColumnName("Outbound_under_2min");
            this.Property(t => t.Outbound_2_to_5min).HasColumnName("Outbound_2_to_5min");
            this.Property(t => t.Outbound_6_to_15min).HasColumnName("Outbound_6_to_15min");
            this.Property(t => t.Outbound_16_to_30min).HasColumnName("Outbound_16_to_30min");
            this.Property(t => t.Outbound_Over_30min).HasColumnName("Outbound_Over_30min");
        }
    }
}
