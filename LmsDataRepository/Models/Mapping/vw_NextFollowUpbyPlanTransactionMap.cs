using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class vw_NextFollowUpbyPlanTransactionMap : EntityTypeConfiguration<vw_NextFollowUpbyPlanTransaction>
    {
        public vw_NextFollowUpbyPlanTransactionMap()
        {
            // Primary Key
            this.HasKey(t => t.Plan_Transaction_ID);

            // Properties
            this.Property(t => t.Plan_Transaction_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vw_NextFollowUpbyPlanTransaction");
            this.Property(t => t.Plan_Transaction_ID).HasColumnName("Plan_Transaction_ID");
            this.Property(t => t.Record_Activity_ID).HasColumnName("Record_Activity_ID");
        }
    }
}
