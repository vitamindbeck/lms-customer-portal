using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Appointment_StateMap : EntityTypeConfiguration<Appointment_State>
    {
        public Appointment_StateMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Appointment_ID, t.State_ID });

            // Properties
            this.Property(t => t.Appointment_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.State_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("Appointment_State");
            this.Property(t => t.Appointment_ID).HasColumnName("Appointment_ID");
            this.Property(t => t.State_ID).HasColumnName("State_ID");
            this.Property(t => t.Created_On).HasColumnName("Created_On");
        }
    }
}
