using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Plan_Transaction_StatusMap : EntityTypeConfiguration<Plan_Transaction_Status>
    {
        public Plan_Transaction_StatusMap()
        {
            // Primary Key
            this.HasKey(t => t.Plan_Transaction_Status_ID);

            // Properties
            this.Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("Plan_Transaction_Status");
            this.Property(t => t.Plan_Transaction_Status_ID).HasColumnName("Plan_Transaction_Status_ID");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Display_Order).HasColumnName("Display_Order");
            this.Property(t => t.Transaction_Date_Type_ID).HasColumnName("Transaction_Date_Type_ID");
            this.Property(t => t.GBS_ID).HasColumnName("GBS_ID");
        }
    }
}
