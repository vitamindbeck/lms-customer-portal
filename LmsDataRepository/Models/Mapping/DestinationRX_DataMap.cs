using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class DestinationRX_DataMap : EntityTypeConfiguration<DestinationRX_Data>
    {
        public DestinationRX_DataMap()
        {
            // Primary Key
            this.HasKey(t => t.DestinationRx_Data_ID);

            // Properties
            this.Property(t => t.ConfirmationNumber)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.ContractID)
                .HasMaxLength(50);

            this.Property(t => t.PlanID)
                .HasMaxLength(50);

            this.Property(t => t.SegmentID)
                .HasMaxLength(50);

            this.Property(t => t.ApplicantTitle)
                .HasMaxLength(18);

            this.Property(t => t.ApplicantFirstName)
                .HasMaxLength(50);

            this.Property(t => t.ApplicantMiddleInitial)
                .HasMaxLength(15);

            this.Property(t => t.ApplicantLastName)
                .HasMaxLength(50);

            this.Property(t => t.ApplicantGender)
                .HasMaxLength(10);

            this.Property(t => t.ApplicantAddress1)
                .HasMaxLength(100);

            this.Property(t => t.ApplicantAddress2)
                .HasMaxLength(50);

            this.Property(t => t.ApplicantAddress3)
                .HasMaxLength(25);

            this.Property(t => t.ApplicantCity)
                .HasMaxLength(50);

            this.Property(t => t.ApplicantState)
                .IsRequired()
                .HasMaxLength(15);

            this.Property(t => t.ApplicantZip)
                .HasMaxLength(32);

            this.Property(t => t.ApplicantPhone)
                .HasMaxLength(50);

            this.Property(t => t.ApplicantEmailAddress)
                .HasMaxLength(100);

            this.Property(t => t.ApplicantHICN)
                .HasMaxLength(100);

            this.Property(t => t.ApplicantSSN)
                .HasMaxLength(50);

            this.Property(t => t.MailingAddress1)
                .HasMaxLength(100);

            this.Property(t => t.MailingAddress2)
                .HasMaxLength(50);

            this.Property(t => t.MailingAddress3)
                .HasMaxLength(25);

            this.Property(t => t.MailingCity)
                .HasMaxLength(50);

            this.Property(t => t.MailingState)
                .HasMaxLength(15);

            this.Property(t => t.MailingZip)
                .HasMaxLength(32);

            this.Property(t => t.SubmitTime)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("DestinationRX_Data");
            this.Property(t => t.DestinationRx_Data_ID).HasColumnName("DestinationRx_Data_ID");
            this.Property(t => t.ConfirmationNumber).HasColumnName("ConfirmationNumber");
            this.Property(t => t.SubmitDate).HasColumnName("SubmitDate");
            this.Property(t => t.ContractID).HasColumnName("ContractID");
            this.Property(t => t.PlanID).HasColumnName("PlanID");
            this.Property(t => t.SegmentID).HasColumnName("SegmentID");
            this.Property(t => t.ApplicantTitle).HasColumnName("ApplicantTitle");
            this.Property(t => t.ApplicantFirstName).HasColumnName("ApplicantFirstName");
            this.Property(t => t.ApplicantMiddleInitial).HasColumnName("ApplicantMiddleInitial");
            this.Property(t => t.ApplicantLastName).HasColumnName("ApplicantLastName");
            this.Property(t => t.ApplicantBirthDate).HasColumnName("ApplicantBirthDate");
            this.Property(t => t.ApplicantGender).HasColumnName("ApplicantGender");
            this.Property(t => t.ApplicantAddress1).HasColumnName("ApplicantAddress1");
            this.Property(t => t.ApplicantAddress2).HasColumnName("ApplicantAddress2");
            this.Property(t => t.ApplicantAddress3).HasColumnName("ApplicantAddress3");
            this.Property(t => t.ApplicantCity).HasColumnName("ApplicantCity");
            this.Property(t => t.ApplicantState).HasColumnName("ApplicantState");
            this.Property(t => t.ApplicantZip).HasColumnName("ApplicantZip");
            this.Property(t => t.ApplicantPhone).HasColumnName("ApplicantPhone");
            this.Property(t => t.ApplicantEmailAddress).HasColumnName("ApplicantEmailAddress");
            this.Property(t => t.ApplicantHICN).HasColumnName("ApplicantHICN");
            this.Property(t => t.ApplicantSSN).HasColumnName("ApplicantSSN");
            this.Property(t => t.MailingAddress1).HasColumnName("MailingAddress1");
            this.Property(t => t.MailingAddress2).HasColumnName("MailingAddress2");
            this.Property(t => t.MailingAddress3).HasColumnName("MailingAddress3");
            this.Property(t => t.MailingCity).HasColumnName("MailingCity");
            this.Property(t => t.MailingState).HasColumnName("MailingState");
            this.Property(t => t.MailingZip).HasColumnName("MailingZip");
            this.Property(t => t.MedicarePartA).HasColumnName("MedicarePartA");
            this.Property(t => t.MedicarePartB).HasColumnName("MedicarePartB");
            this.Property(t => t.SubmitTime).HasColumnName("SubmitTime");
            this.Property(t => t.Record_ID).HasColumnName("Record_ID");
            this.Property(t => t.DateTimeFileDownloaded).HasColumnName("DateTimeFileDownloaded");
            this.Property(t => t.IsDownloadSucess).HasColumnName("IsDownloadSucess");
            this.Property(t => t.IsPartialUpload).HasColumnName("IsPartialUpload");
        }
    }
}
