using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Time_ZoneMap : EntityTypeConfiguration<Time_Zone>
    {
        public Time_ZoneMap()
        {
            // Primary Key
            this.HasKey(t => new { t.State, t.Zone, t.Plus });

            // Properties
            this.Property(t => t.State)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2);

            this.Property(t => t.Zone)
                .IsRequired()
                .HasMaxLength(4);

            // Table & Column Mappings
            this.ToTable("Time_Zone");
            this.Property(t => t.State).HasColumnName("State");
            this.Property(t => t.Zone).HasColumnName("Zone");
            this.Property(t => t.Plus).HasColumnName("Plus");
            this.Property(t => t.Plus_hour).HasColumnName("Plus_hour");
        }
    }
}
