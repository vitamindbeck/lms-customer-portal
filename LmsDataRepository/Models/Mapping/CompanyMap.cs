using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class CompanyMap : EntityTypeConfiguration<Company>
    {
        public CompanyMap()
        {
            // Primary Key
            this.HasKey(t => t.Company_ID);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(128);

            this.Property(t => t.Address_1)
                .HasMaxLength(200);

            this.Property(t => t.Address_2)
                .HasMaxLength(200);

            this.Property(t => t.City)
                .HasMaxLength(100);

            this.Property(t => t.Zipcode)
                .HasMaxLength(10);

            this.Property(t => t.Company_President)
                .HasMaxLength(200);

            this.Property(t => t.Company_Phone)
                .HasMaxLength(50);

            this.Property(t => t.Company_Size)
                .HasMaxLength(50);

            this.Property(t => t.Primary_Email)
                .HasMaxLength(200);

            this.Property(t => t.Primary_Contact)
                .HasMaxLength(100);

            this.Property(t => t.Title)
                .HasMaxLength(100);

            this.Property(t => t.Email_Address)
                .HasMaxLength(100);

            this.Property(t => t.Primary_Phone)
                .HasMaxLength(50);

            this.Property(t => t.Web_Address)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("Company");
            this.Property(t => t.Company_ID).HasColumnName("Company_ID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Address_1).HasColumnName("Address_1");
            this.Property(t => t.Address_2).HasColumnName("Address_2");
            this.Property(t => t.City).HasColumnName("City");
            this.Property(t => t.State_ID).HasColumnName("State_ID");
            this.Property(t => t.Zipcode).HasColumnName("Zipcode");
            this.Property(t => t.Company_President).HasColumnName("Company_President");
            this.Property(t => t.Company_Phone).HasColumnName("Company_Phone");
            this.Property(t => t.Company_Size).HasColumnName("Company_Size");
            this.Property(t => t.Primary_Email).HasColumnName("Primary_Email");
            this.Property(t => t.Industry_ID).HasColumnName("Industry_ID");
            this.Property(t => t.Primary_Contact).HasColumnName("Primary_Contact");
            this.Property(t => t.Title).HasColumnName("Title");
            this.Property(t => t.Email_Address).HasColumnName("Email_Address");
            this.Property(t => t.Primary_Phone).HasColumnName("Primary_Phone");
            this.Property(t => t.Web_Address).HasColumnName("Web_Address");
        }
    }
}
