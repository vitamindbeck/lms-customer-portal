using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Daily_Open_CaseMap : EntityTypeConfiguration<Daily_Open_Case>
    {
        public Daily_Open_CaseMap()
        {
            // Primary Key
            this.HasKey(t => t.Open_Case_ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("Daily_Open_Case");
            this.Property(t => t.Open_Case_ID).HasColumnName("Open_Case_ID");
            this.Property(t => t.Record_Activity_ID).HasColumnName("Record_Activity_ID");
            this.Property(t => t.Record_ID).HasColumnName("Record_ID");
            this.Property(t => t.Agent_ID).HasColumnName("Agent_ID");
            this.Property(t => t.Created_On).HasColumnName("Created_On");
        }
    }
}
