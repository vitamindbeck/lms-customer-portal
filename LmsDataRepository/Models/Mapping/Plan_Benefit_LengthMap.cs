using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Plan_Benefit_LengthMap : EntityTypeConfiguration<Plan_Benefit_Length>
    {
        public Plan_Benefit_LengthMap()
        {
            // Primary Key
            this.HasKey(t => t.Plan_Benefit_Length_ID);

            // Properties
            this.Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Code)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Plan_Benefit_Length");
            this.Property(t => t.Plan_Benefit_Length_ID).HasColumnName("Plan_Benefit_Length_ID");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Code).HasColumnName("Code");
        }
    }
}
