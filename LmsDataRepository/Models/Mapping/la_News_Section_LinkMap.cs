using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class la_News_Section_LinkMap : EntityTypeConfiguration<la_News_Section_Link>
    {
        public la_News_Section_LinkMap()
        {
            // Primary Key
            this.HasKey(t => t.News_Section_Link_ID);

            // Properties
            this.Property(t => t.Link_URL)
                .HasMaxLength(500);

            this.Property(t => t.Link_Text)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("la_News_Section_Link");
            this.Property(t => t.News_Section_Link_ID).HasColumnName("News_Section_Link_ID");
            this.Property(t => t.News_Section_ID).HasColumnName("News_Section_ID");
            this.Property(t => t.Link_URL).HasColumnName("Link_URL");
            this.Property(t => t.Link_Text).HasColumnName("Link_Text");
            this.Property(t => t.Display_Order).HasColumnName("Display_Order");
            this.Property(t => t.Active).HasColumnName("Active");
        }
    }
}
