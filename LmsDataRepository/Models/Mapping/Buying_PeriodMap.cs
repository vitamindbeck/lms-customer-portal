using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Buying_PeriodMap : EntityTypeConfiguration<Buying_Period>
    {
        public Buying_PeriodMap()
        {
            // Primary Key
            this.HasKey(t => t.Buying_Period_ID);

            // Properties
            this.Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("Buying_Period");
            this.Property(t => t.Buying_Period_ID).HasColumnName("Buying_Period_ID");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Display_Order).HasColumnName("Display_Order");
            this.Property(t => t.Active).HasColumnName("Active");
        }
    }
}
