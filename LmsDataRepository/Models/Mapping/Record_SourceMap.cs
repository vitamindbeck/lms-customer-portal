using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Record_SourceMap : EntityTypeConfiguration<Record_Source>
    {
        public Record_SourceMap()
        {
            // Primary Key
            this.HasKey(t => t.Record_Source_ID);

            // Properties
            this.Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("Record_Source");
            this.Property(t => t.Record_Source_ID).HasColumnName("Record_Source_ID");
            this.Property(t => t.Description).HasColumnName("Description");
        }
    }
}
