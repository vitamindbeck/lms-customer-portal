using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Affinity_PartnerMap : EntityTypeConfiguration<Affinity_Partner>
    {
        public Affinity_PartnerMap()
        {
            // Primary Key
            this.HasKey(t => t.Affinity_Partner_ID);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Address_1)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Address_2)
                .HasMaxLength(100);

            this.Property(t => t.City)
                .HasMaxLength(50);

            this.Property(t => t.Phone)
                .HasMaxLength(50);

            this.Property(t => t.Contact_Name)
                .HasMaxLength(100);

            this.Property(t => t.Short_Name)
                .IsRequired()
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("Affinity_Partner");
            this.Property(t => t.Affinity_Partner_ID).HasColumnName("Affinity_Partner_ID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Address_1).HasColumnName("Address_1");
            this.Property(t => t.Address_2).HasColumnName("Address_2");
            this.Property(t => t.City).HasColumnName("City");
            this.Property(t => t.State_ID).HasColumnName("State_ID");
            this.Property(t => t.Phone).HasColumnName("Phone");
            this.Property(t => t.Contact_Name).HasColumnName("Contact_Name");
            this.Property(t => t.Parent_ID).HasColumnName("Parent_ID");
            this.Property(t => t.Short_Name).HasColumnName("Short_Name");
            this.Property(t => t.Active).HasColumnName("Active");
            this.Property(t => t.Co_Branded_Site_ID).HasColumnName("Co_Branded_Site_ID");
            this.Property(t => t.Application_Submit_Indicator).HasColumnName("Application_Submit_Indicator");
            this.Property(t => t.Application_Submit_Commision).HasColumnName("Application_Submit_Commision");
            this.Property(t => t.Display_Member_Indicator).HasColumnName("Display_Member_Indicator");
            this.Property(t => t.Has_Plan_Preference).HasColumnName("Has_Plan_Preference");
            this.Property(t => t.LTC).HasColumnName("LTC");
        }
    }
}
