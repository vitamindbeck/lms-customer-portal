using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Email_Agent_CodeMap : EntityTypeConfiguration<Email_Agent_Code>
    {
        public Email_Agent_CodeMap()
        {
            // Primary Key
            this.HasKey(t => t.Email_Agent_Code_ID);

            // Properties
            this.Property(t => t.Code)
                .HasMaxLength(500);

            this.Property(t => t.Subject)
                .IsRequired()
                .HasMaxLength(500);

            this.Property(t => t.Message)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("Email_Agent_Code");
            this.Property(t => t.Email_Agent_Code_ID).HasColumnName("Email_Agent_Code_ID");
            this.Property(t => t.Code).HasColumnName("Code");
            this.Property(t => t.User_ID).HasColumnName("User_ID");
            this.Property(t => t.Subject).HasColumnName("Subject");
            this.Property(t => t.Message).HasColumnName("Message");
            this.Property(t => t.HTML).HasColumnName("HTML");
        }
    }
}
