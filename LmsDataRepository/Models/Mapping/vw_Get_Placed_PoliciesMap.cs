using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class vw_Get_Placed_PoliciesMap : EntityTypeConfiguration<vw_Get_Placed_Policies>
    {
        public vw_Get_Placed_PoliciesMap()
        {
            // Primary Key
            this.HasKey(t => t.PolicyID);

            // Properties
            this.Property(t => t.UserDefinedString41)
                .HasMaxLength(35);

            this.Property(t => t.PolicyID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vw_Get_Placed_Policies");
            this.Property(t => t.LMS_Agent_ID).HasColumnName("LMS_Agent_ID");
            this.Property(t => t.UserDefinedString41).HasColumnName("UserDefinedString41");
            this.Property(t => t.PolicyID).HasColumnName("PolicyID");
            this.Property(t => t.Placed_Date).HasColumnName("Placed_Date");
            this.Property(t => t.Closed_Date).HasColumnName("Closed_Date");
            this.Property(t => t.Status).HasColumnName("Status");
        }
    }
}
