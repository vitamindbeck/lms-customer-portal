using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Feature_CategoryMap : EntityTypeConfiguration<Feature_Category>
    {
        public Feature_CategoryMap()
        {
            // Primary Key
            this.HasKey(t => t.Feature_Category_ID);

            // Properties
            this.Property(t => t.Category_Code)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Feature_Category");
            this.Property(t => t.Feature_Category_ID).HasColumnName("Feature_Category_ID");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Active).HasColumnName("Active");
            this.Property(t => t.Display_Order).HasColumnName("Display_Order");
            this.Property(t => t.Single_Plan_Description).HasColumnName("Single_Plan_Description");
            this.Property(t => t.Category_Code).HasColumnName("Category_Code");
        }
    }
}
