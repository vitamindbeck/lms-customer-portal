using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class vw_Record_Activity_Initial_AttemptMap : EntityTypeConfiguration<vw_Record_Activity_Initial_Attempt>
    {
        public vw_Record_Activity_Initial_AttemptMap()
        {
            // Primary Key
            this.HasKey(t => t.Record_ID);

            // Properties
            this.Property(t => t.Record_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vw_Record_Activity_Initial_Attempt");
            this.Property(t => t.Record_ID).HasColumnName("Record_ID");
            this.Property(t => t.Initial_Attempt_RA_ID).HasColumnName("Initial_Attempt_RA_ID");
        }
    }
}
