using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Certification_Type_LookupMap : EntityTypeConfiguration<Certification_Type_Lookup>
    {
        public Certification_Type_LookupMap()
        {
            // Primary Key
            this.HasKey(t => t.Certification_Type_ID);

            // Properties
            this.Property(t => t.Certification_Type_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Certification_Type)
                .IsRequired()
                .HasMaxLength(30);

            // Table & Column Mappings
            this.ToTable("Certification_Type_Lookup");
            this.Property(t => t.Certification_Type_ID).HasColumnName("Certification_Type_ID");
            this.Property(t => t.Certification_Type).HasColumnName("Certification_Type");
        }
    }
}
