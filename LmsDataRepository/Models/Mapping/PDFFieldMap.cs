using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class PDFFieldMap : EntityTypeConfiguration<PDFField>
    {
        public PDFFieldMap()
        {
            // Primary Key
            this.HasKey(t => t.PDFFieldID);

            // Properties
            this.Property(t => t.FieldName)
                .IsRequired()
                .HasMaxLength(500);

            this.Property(t => t.PropertyName)
                .IsRequired()
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("PDFField");
            this.Property(t => t.PDFFieldID).HasColumnName("PDFFieldID");
            this.Property(t => t.PDFID).HasColumnName("PDFID");
            this.Property(t => t.FieldName).HasColumnName("FieldName");
            this.Property(t => t.PropertyName).HasColumnName("PropertyName");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
        }
    }
}
