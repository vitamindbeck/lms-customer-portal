using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Transaction_Date_TypeMap : EntityTypeConfiguration<Transaction_Date_Type>
    {
        public Transaction_Date_TypeMap()
        {
            // Primary Key
            this.HasKey(t => t.Transaction_Date_Type_ID);

            // Properties
            this.Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("Transaction_Date_Type");
            this.Property(t => t.Transaction_Date_Type_ID).HasColumnName("Transaction_Date_Type_ID");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Display_Order).HasColumnName("Display_Order");
            this.Property(t => t.Active).HasColumnName("Active");
        }
    }
}
