using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class vw_Screener_Dash_Live_Transfer_AttemptMap : EntityTypeConfiguration<vw_Screener_Dash_Live_Transfer_Attempt>
    {
        public vw_Screener_Dash_Live_Transfer_AttemptMap()
        {
            // Primary Key
            this.HasKey(t => new { t.TimePeriod, t.Agent_ID });

            // Properties
            this.Property(t => t.TimePeriod)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.Agent_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vw_Screener_Dash_Live_Transfer_Attempt");
            this.Property(t => t.MinGroupTime).HasColumnName("MinGroupTime");
            this.Property(t => t.MaxGroupTime).HasColumnName("MaxGroupTime");
            this.Property(t => t.TimePeriod).HasColumnName("TimePeriod");
            this.Property(t => t.Under_5).HasColumnName("Under_5");
            this.Property(t => t.FivetoFifteen).HasColumnName("FivetoFifteen");
            this.Property(t => t.Over_15).HasColumnName("Over_15");
            this.Property(t => t.Agent_ID).HasColumnName("Agent_ID");
        }
    }
}
