using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class vw_LMS_Open_ActivitiesMap : EntityTypeConfiguration<vw_LMS_Open_Activities>
    {
        public vw_LMS_Open_ActivitiesMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Record_ID, t.Plan_Transaction_ID, t.Record_Activity_ID, t.First_Name, t.Last_Name, t.State, t.PlanDesc, t.SourceName, t.Activity_Type_ID, t.Record_Activity_Status_ID, t.User_ID });

            // Properties
            this.Property(t => t.Record_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Plan_Transaction_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.GBS_ID)
                .HasMaxLength(50);

            this.Property(t => t.Record_Activity_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.First_Name)
                .IsRequired()
                .HasMaxLength(32);

            this.Property(t => t.Last_Name)
                .IsRequired()
                .HasMaxLength(32);

            this.Property(t => t.State)
                .IsRequired()
                .HasMaxLength(32);

            this.Property(t => t.Phone)
                .HasMaxLength(32);

            this.Property(t => t.Email_Address)
                .HasMaxLength(100);

            this.Property(t => t.PlanDesc)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.SourceName)
                .IsRequired()
                .HasMaxLength(251);

            this.Property(t => t.EligibilityCategory)
                .HasMaxLength(15);

            this.Property(t => t.BuyingPeriod)
                .HasMaxLength(100);

            this.Property(t => t.BuyingPeriodSub)
                .HasMaxLength(100);

            this.Property(t => t.Activity_Type_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Record_Activity_Status_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.User_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vw_LMS_Open_Activities");
            this.Property(t => t.Record_ID).HasColumnName("Record_ID");
            this.Property(t => t.Plan_Transaction_ID).HasColumnName("Plan_Transaction_ID");
            this.Property(t => t.GBS_ID).HasColumnName("GBS_ID");
            this.Property(t => t.Record_Activity_ID).HasColumnName("Record_Activity_ID");
            this.Property(t => t.First_Name).HasColumnName("First_Name");
            this.Property(t => t.Last_Name).HasColumnName("Last_Name");
            this.Property(t => t.State).HasColumnName("State");
            this.Property(t => t.Phone).HasColumnName("Phone");
            this.Property(t => t.Email_Address).HasColumnName("Email_Address");
            this.Property(t => t.PlanDesc).HasColumnName("PlanDesc");
            this.Property(t => t.Priority_Level).HasColumnName("Priority_Level");
            this.Property(t => t.Due_Date).HasColumnName("Due_Date");
            this.Property(t => t.SourceName).HasColumnName("SourceName");
            this.Property(t => t.EligibilityCategory).HasColumnName("EligibilityCategory");
            this.Property(t => t.BuyingPeriod).HasColumnName("BuyingPeriod");
            this.Property(t => t.BuyingPeriodSub).HasColumnName("BuyingPeriodSub");
            this.Property(t => t.Buying_Period_Effective_Date).HasColumnName("Buying_Period_Effective_Date");
            this.Property(t => t.Activity_Type_ID).HasColumnName("Activity_Type_ID");
            this.Property(t => t.Record_Activity_Status_ID).HasColumnName("Record_Activity_Status_ID");
            this.Property(t => t.Actual_Date).HasColumnName("Actual_Date");
            this.Property(t => t.User_ID).HasColumnName("User_ID");
            this.Property(t => t.Key).HasColumnName("Key");
        }
    }
}
