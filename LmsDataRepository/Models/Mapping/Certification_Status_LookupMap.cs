using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Certification_Status_LookupMap : EntityTypeConfiguration<Certification_Status_Lookup>
    {
        public Certification_Status_LookupMap()
        {
            // Primary Key
            this.HasKey(t => t.Certification_Status_ID);

            // Properties
            this.Property(t => t.Certification_Status_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Certification_Status)
                .IsRequired()
                .HasMaxLength(30);

            // Table & Column Mappings
            this.ToTable("Certification_Status_Lookup");
            this.Property(t => t.Certification_Status_ID).HasColumnName("Certification_Status_ID");
            this.Property(t => t.Certification_Status).HasColumnName("Certification_Status");
        }
    }
}
