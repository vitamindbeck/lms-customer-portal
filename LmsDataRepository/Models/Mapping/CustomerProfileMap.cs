using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class CustomerProfileMap : EntityTypeConfiguration<CustomerProfile>
    {
        public CustomerProfileMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("CustomerProfile");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.RecordId).HasColumnName("RecordId");
            this.Property(t => t.TypeofHealthId).HasColumnName("TypeofHealthId");
            this.Property(t => t.TypeofWealthId).HasColumnName("TypeofWealthId");
            this.Property(t => t.TypeofDecisionId).HasColumnName("TypeofDecisionId");
            this.Property(t => t.TypeofRiskId).HasColumnName("TypeofRiskId");
            this.Property(t => t.TypeofCostSensitivityId).HasColumnName("TypeofCostSensitivityId");
            this.Property(t => t.TypeofProviderPreferenceId).HasColumnName("TypeofProviderPreferenceId");
            this.Property(t => t.TravelerCommentsNoteId).HasColumnName("TravelerCommentsNoteId");
            this.Property(t => t.BrandPreferenceCommentsNoteId).HasColumnName("BrandPreferenceCommentsNoteId");
            this.Property(t => t.FutureHealthConcernsCommentsNoteId).HasColumnName("FutureHealthConcernsCommentsNoteId");

            // Relationships
            this.HasOptional(t => t.Note)
                .WithMany(t => t.CustomerProfiles)
                .HasForeignKey(d => d.BrandPreferenceCommentsNoteId);
            this.HasOptional(t => t.Note1)
                .WithMany(t => t.CustomerProfiles1)
                .HasForeignKey(d => d.FutureHealthConcernsCommentsNoteId);
            this.HasOptional(t => t.TypeofNote)
                .WithMany(t => t.CustomerProfiles)
                .HasForeignKey(d => d.TravelerCommentsNoteId);
            this.HasOptional(t => t.TypeofCostSensitivity)
                .WithMany(t => t.CustomerProfiles)
                .HasForeignKey(d => d.TypeofCostSensitivityId);
            this.HasOptional(t => t.TypeofDecision)
                .WithMany(t => t.CustomerProfiles)
                .HasForeignKey(d => d.TypeofDecisionId);
            this.HasOptional(t => t.TypeofHealth)
                .WithMany(t => t.CustomerProfiles)
                .HasForeignKey(d => d.TypeofHealthId);
            this.HasOptional(t => t.TypeofProviderPreference)
                .WithMany(t => t.CustomerProfiles)
                .HasForeignKey(d => d.TypeofProviderPreferenceId);
            this.HasOptional(t => t.TypeofRisk)
                .WithMany(t => t.CustomerProfiles)
                .HasForeignKey(d => d.TypeofRiskId);
            this.HasOptional(t => t.TypeofWealth)
                .WithMany(t => t.CustomerProfiles)
                .HasForeignKey(d => d.TypeofWealthId);

        }
    }
}
