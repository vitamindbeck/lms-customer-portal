using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class vw_Follow_Up_Live_Transfer_AttemptsMap : EntityTypeConfiguration<vw_Follow_Up_Live_Transfer_Attempts>
    {
        public vw_Follow_Up_Live_Transfer_AttemptsMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Record_Activity_ID, t.Record_ID, t.Activity_Type_ID, t.Created_On, t.Created_By, t.User_ID, t.Made_Contact, t.Record_Activity_Status_ID, t.Plan_Transaction_ID, t.Priority_Level });

            // Properties
            this.Property(t => t.Record_Activity_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.Record_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Activity_Type_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Created_By)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.User_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Record_Activity_Status_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Plan_Transaction_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Priority_Level)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vw_Follow_Up_Live_Transfer_Attempts");
            this.Property(t => t.Record_Activity_ID).HasColumnName("Record_Activity_ID");
            this.Property(t => t.Record_ID).HasColumnName("Record_ID");
            this.Property(t => t.Activity_Type_ID).HasColumnName("Activity_Type_ID");
            this.Property(t => t.Due_Date).HasColumnName("Due_Date");
            this.Property(t => t.Actual_Date).HasColumnName("Actual_Date");
            this.Property(t => t.Created_On).HasColumnName("Created_On");
            this.Property(t => t.Created_By).HasColumnName("Created_By");
            this.Property(t => t.User_ID).HasColumnName("User_ID");
            this.Property(t => t.Notes).HasColumnName("Notes");
            this.Property(t => t.Made_Contact).HasColumnName("Made_Contact");
            this.Property(t => t.Record_Activity_Status_ID).HasColumnName("Record_Activity_Status_ID");
            this.Property(t => t.Plan_Transaction_ID).HasColumnName("Plan_Transaction_ID");
            this.Property(t => t.Priority_Level).HasColumnName("Priority_Level");
            this.Property(t => t.Auto_Email_ID).HasColumnName("Auto_Email_ID");
        }
    }
}
