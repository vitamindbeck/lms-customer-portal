using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Record_Eligibility_HistoryMap : EntityTypeConfiguration<Record_Eligibility_History>
    {
        public Record_Eligibility_HistoryMap()
        {
            // Primary Key
            this.HasKey(t => t.Record_Eligibility_History_ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("Record_Eligibility_History");
            this.Property(t => t.Record_Eligibility_History_ID).HasColumnName("Record_Eligibility_History_ID");
            this.Property(t => t.Record_ID).HasColumnName("Record_ID");
            this.Property(t => t.Record_Eligibility_ID_Old).HasColumnName("Record_Eligibility_ID_Old");
            this.Property(t => t.Record_Eligibility_ID_New).HasColumnName("Record_Eligibility_ID_New");
            this.Property(t => t.Created_On).HasColumnName("Created_On");
        }
    }
}
