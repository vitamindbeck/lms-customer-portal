using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Campaign_HistoryMap : EntityTypeConfiguration<Campaign_History>
    {
        public Campaign_HistoryMap()
        {
            // Primary Key
            this.HasKey(t => t.Campaign_History_ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("Campaign_History");
            this.Property(t => t.Campaign_History_ID).HasColumnName("Campaign_History_ID");
            this.Property(t => t.Campaign_Code_ID).HasColumnName("Campaign_Code_ID");
            this.Property(t => t.Record_ID).HasColumnName("Record_ID");
            this.Property(t => t.Date_Added).HasColumnName("Date_Added");
        }
    }
}
