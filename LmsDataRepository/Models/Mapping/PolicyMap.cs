using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class PolicyMap : EntityTypeConfiguration<Policy>
    {
        public PolicyMap()
        {
            // Primary Key
            this.HasKey(t => t.PolicyID);

            // Properties
            this.Property(t => t.ClientName)
                .HasMaxLength(70);

            this.Property(t => t.CUSIP)
                .HasMaxLength(9);

            this.Property(t => t.FundCode)
                .HasMaxLength(7);

            this.Property(t => t.ImportFlag)
                .HasMaxLength(20);

            this.Property(t => t.IndividualOrBusiness)
                .HasMaxLength(1);

            this.Property(t => t.InsuredType)
                .HasMaxLength(1);

            this.Property(t => t.InvestmentOrPolicy)
                .HasMaxLength(1);

            this.Property(t => t.KeyField)
                .HasMaxLength(100);

            this.Property(t => t.LinkedToPolicyNumber)
                .HasMaxLength(15);

            this.Property(t => t.PolicyNumber)
                .HasMaxLength(55);

            this.Property(t => t.SocialCode)
                .HasMaxLength(3);

            this.Property(t => t.TickerSymbol)
                .HasMaxLength(35);

            this.Property(t => t.UserDefinedListBox1)
                .HasMaxLength(35);

            this.Property(t => t.UserDefinedListBox2)
                .HasMaxLength(35);

            this.Property(t => t.UserDefinedListBox3)
                .HasMaxLength(35);

            this.Property(t => t.UserDefinedListBox4)
                .HasMaxLength(35);

            this.Property(t => t.UserDefinedString1)
                .HasMaxLength(35);

            this.Property(t => t.UserDefinedString10)
                .HasMaxLength(35);

            this.Property(t => t.UserDefinedString11)
                .HasMaxLength(35);

            this.Property(t => t.UserDefinedString12)
                .HasMaxLength(35);

            this.Property(t => t.UserDefinedString13)
                .HasMaxLength(35);

            this.Property(t => t.UserDefinedString14)
                .HasMaxLength(35);

            this.Property(t => t.UserDefinedString15)
                .HasMaxLength(35);

            this.Property(t => t.UserDefinedString16)
                .HasMaxLength(35);

            this.Property(t => t.UserDefinedString17)
                .HasMaxLength(35);

            this.Property(t => t.UserDefinedString18)
                .HasMaxLength(35);

            this.Property(t => t.UserDefinedString19)
                .HasMaxLength(35);

            this.Property(t => t.UserDefinedString2)
                .HasMaxLength(35);

            this.Property(t => t.UserDefinedString20)
                .HasMaxLength(35);

            this.Property(t => t.UserDefinedString21)
                .HasMaxLength(35);

            this.Property(t => t.UserDefinedString22)
                .HasMaxLength(35);

            this.Property(t => t.UserDefinedString23)
                .HasMaxLength(35);

            this.Property(t => t.UserDefinedString24)
                .HasMaxLength(35);

            this.Property(t => t.UserDefinedString25)
                .HasMaxLength(35);

            this.Property(t => t.UserDefinedString26)
                .HasMaxLength(35);

            this.Property(t => t.UserDefinedString27)
                .HasMaxLength(35);

            this.Property(t => t.UserDefinedString28)
                .HasMaxLength(35);

            this.Property(t => t.UserDefinedString29)
                .HasMaxLength(35);

            this.Property(t => t.UserDefinedString3)
                .HasMaxLength(35);

            this.Property(t => t.UserDefinedString30)
                .HasMaxLength(35);

            this.Property(t => t.UserDefinedString31)
                .HasMaxLength(35);

            this.Property(t => t.UserDefinedString32)
                .HasMaxLength(35);

            this.Property(t => t.UserDefinedString33)
                .HasMaxLength(35);

            this.Property(t => t.UserDefinedString34)
                .HasMaxLength(35);

            this.Property(t => t.UserDefinedString35)
                .HasMaxLength(35);

            this.Property(t => t.UserDefinedString36)
                .HasMaxLength(35);

            this.Property(t => t.UserDefinedString37)
                .HasMaxLength(35);

            this.Property(t => t.UserDefinedString38)
                .HasMaxLength(35);

            this.Property(t => t.UserDefinedString39)
                .HasMaxLength(35);

            this.Property(t => t.UserDefinedString4)
                .HasMaxLength(35);

            this.Property(t => t.UserDefinedString40)
                .HasMaxLength(35);

            this.Property(t => t.UserDefinedString41)
                .HasMaxLength(35);

            this.Property(t => t.UserDefinedString42)
                .HasMaxLength(35);

            this.Property(t => t.UserDefinedString43)
                .HasMaxLength(35);

            this.Property(t => t.UserDefinedString44)
                .HasMaxLength(35);

            this.Property(t => t.UserDefinedString45)
                .HasMaxLength(35);

            this.Property(t => t.UserDefinedString5)
                .HasMaxLength(35);

            this.Property(t => t.UserDefinedString6)
                .HasMaxLength(35);

            this.Property(t => t.UserDefinedString7)
                .HasMaxLength(35);

            this.Property(t => t.UserDefinedString8)
                .HasMaxLength(35);

            this.Property(t => t.UserDefinedString9)
                .HasMaxLength(35);

            // Table & Column Mappings
            this.ToTable("Policies");
            this.Property(t => t.AdministratorID).HasColumnName("AdministratorID");
            this.Property(t => t.AgentID).HasColumnName("AgentID");
            this.Property(t => t.CarrierID).HasColumnName("CarrierID");
            this.Property(t => t.ClientName).HasColumnName("ClientName");
            this.Property(t => t.CommissionStartDate).HasColumnName("CommissionStartDate");
            this.Property(t => t.CurrentValue).HasColumnName("CurrentValue");
            this.Property(t => t.CurrentValueDate).HasColumnName("CurrentValueDate");
            this.Property(t => t.CUSIP).HasColumnName("CUSIP");
            this.Property(t => t.CustomerNumber).HasColumnName("CustomerNumber");
            this.Property(t => t.DateTimeLastModified).HasColumnName("DateTimeLastModified");
            this.Property(t => t.EffectiveDateOrPurchaseDate).HasColumnName("EffectiveDateOrPurchaseDate");
            this.Property(t => t.FaceAmountOrPurchasePrice).HasColumnName("FaceAmountOrPurchasePrice");
            this.Property(t => t.FundCode).HasColumnName("FundCode");
            this.Property(t => t.ImportFlag).HasColumnName("ImportFlag");
            this.Property(t => t.IndividualOrBusiness).HasColumnName("IndividualOrBusiness");
            this.Property(t => t.InsuranceLine).HasColumnName("InsuranceLine");
            this.Property(t => t.InsuredID).HasColumnName("InsuredID");
            this.Property(t => t.InsuredType).HasColumnName("InsuredType");
            this.Property(t => t.InterestRate).HasColumnName("InterestRate");
            this.Property(t => t.InvestmentOrPolicy).HasColumnName("InvestmentOrPolicy");
            this.Property(t => t.IsAutoCheck).HasColumnName("IsAutoCheck");
            this.Property(t => t.IsDivCGNotReinvest).HasColumnName("IsDivCGNotReinvest");
            this.Property(t => t.IsPolicyFeeCommissionalble).HasColumnName("IsPolicyFeeCommissionalble");
            this.Property(t => t.KeyField).HasColumnName("KeyField");
            this.Property(t => t.LicenseType).HasColumnName("LicenseType");
            this.Property(t => t.LinkedToPolicyNumber).HasColumnName("LinkedToPolicyNumber");
            this.Property(t => t.LoanAmount).HasColumnName("LoanAmount");
            this.Property(t => t.NextPaidDate).HasColumnName("NextPaidDate");
            this.Property(t => t.NumberOfLives).HasColumnName("NumberOfLives");
            this.Property(t => t.PolicyFee).HasColumnName("PolicyFee");
            this.Property(t => t.PolicyGroup).HasColumnName("PolicyGroup");
            this.Property(t => t.PolicyID).HasColumnName("PolicyID");
            this.Property(t => t.PolicyLinkTo).HasColumnName("PolicyLinkTo");
            this.Property(t => t.PolicyNumber).HasColumnName("PolicyNumber");
            this.Property(t => t.Premium).HasColumnName("Premium");
            this.Property(t => t.PremiumMode).HasColumnName("PremiumMode");
            this.Property(t => t.PremModeEffectiveDate).HasColumnName("PremModeEffectiveDate");
            this.Property(t => t.ProductID).HasColumnName("ProductID");
            this.Property(t => t.ProductType).HasColumnName("ProductType");
            this.Property(t => t.ReceivedDate).HasColumnName("ReceivedDate");
            this.Property(t => t.RenewalDate).HasColumnName("RenewalDate");
            this.Property(t => t.RequestedEffectiveDate).HasColumnName("RequestedEffectiveDate");
            this.Property(t => t.SocialCode).HasColumnName("SocialCode");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.StatusDate).HasColumnName("StatusDate");
            this.Property(t => t.StatusType).HasColumnName("StatusType");
            this.Property(t => t.SubmittedDate).HasColumnName("SubmittedDate");
            this.Property(t => t.SurrenderValue).HasColumnName("SurrenderValue");
            this.Property(t => t.TargetPremium).HasColumnName("TargetPremium");
            this.Property(t => t.TickerSymbol).HasColumnName("TickerSymbol");
            this.Property(t => t.Underwriting).HasColumnName("Underwriting");
            this.Property(t => t.UnitsDivPurchased).HasColumnName("UnitsDivPurchased");
            this.Property(t => t.UnitsPurchased).HasColumnName("UnitsPurchased");
            this.Property(t => t.UserDefinedDate1).HasColumnName("UserDefinedDate1");
            this.Property(t => t.UserDefinedDate2).HasColumnName("UserDefinedDate2");
            this.Property(t => t.UserDefinedDate3).HasColumnName("UserDefinedDate3");
            this.Property(t => t.UserDefinedDate4).HasColumnName("UserDefinedDate4");
            this.Property(t => t.UserDefinedListBox1).HasColumnName("UserDefinedListBox1");
            this.Property(t => t.UserDefinedListBox2).HasColumnName("UserDefinedListBox2");
            this.Property(t => t.UserDefinedListBox3).HasColumnName("UserDefinedListBox3");
            this.Property(t => t.UserDefinedListBox4).HasColumnName("UserDefinedListBox4");
            this.Property(t => t.UserDefinedLogic1).HasColumnName("UserDefinedLogic1");
            this.Property(t => t.UserDefinedLogic2).HasColumnName("UserDefinedLogic2");
            this.Property(t => t.UserDefinedLogic3).HasColumnName("UserDefinedLogic3");
            this.Property(t => t.UserDefinedLogic4).HasColumnName("UserDefinedLogic4");
            this.Property(t => t.UserDefinedNumeric1).HasColumnName("UserDefinedNumeric1");
            this.Property(t => t.UserDefinedNumeric2).HasColumnName("UserDefinedNumeric2");
            this.Property(t => t.UserDefinedNumeric3).HasColumnName("UserDefinedNumeric3");
            this.Property(t => t.UserDefinedNumeric4).HasColumnName("UserDefinedNumeric4");
            this.Property(t => t.UserDefinedString1).HasColumnName("UserDefinedString1");
            this.Property(t => t.UserDefinedString10).HasColumnName("UserDefinedString10");
            this.Property(t => t.UserDefinedString11).HasColumnName("UserDefinedString11");
            this.Property(t => t.UserDefinedString12).HasColumnName("UserDefinedString12");
            this.Property(t => t.UserDefinedString13).HasColumnName("UserDefinedString13");
            this.Property(t => t.UserDefinedString14).HasColumnName("UserDefinedString14");
            this.Property(t => t.UserDefinedString15).HasColumnName("UserDefinedString15");
            this.Property(t => t.UserDefinedString16).HasColumnName("UserDefinedString16");
            this.Property(t => t.UserDefinedString17).HasColumnName("UserDefinedString17");
            this.Property(t => t.UserDefinedString18).HasColumnName("UserDefinedString18");
            this.Property(t => t.UserDefinedString19).HasColumnName("UserDefinedString19");
            this.Property(t => t.UserDefinedString2).HasColumnName("UserDefinedString2");
            this.Property(t => t.UserDefinedString20).HasColumnName("UserDefinedString20");
            this.Property(t => t.UserDefinedString21).HasColumnName("UserDefinedString21");
            this.Property(t => t.UserDefinedString22).HasColumnName("UserDefinedString22");
            this.Property(t => t.UserDefinedString23).HasColumnName("UserDefinedString23");
            this.Property(t => t.UserDefinedString24).HasColumnName("UserDefinedString24");
            this.Property(t => t.UserDefinedString25).HasColumnName("UserDefinedString25");
            this.Property(t => t.UserDefinedString26).HasColumnName("UserDefinedString26");
            this.Property(t => t.UserDefinedString27).HasColumnName("UserDefinedString27");
            this.Property(t => t.UserDefinedString28).HasColumnName("UserDefinedString28");
            this.Property(t => t.UserDefinedString29).HasColumnName("UserDefinedString29");
            this.Property(t => t.UserDefinedString3).HasColumnName("UserDefinedString3");
            this.Property(t => t.UserDefinedString30).HasColumnName("UserDefinedString30");
            this.Property(t => t.UserDefinedString31).HasColumnName("UserDefinedString31");
            this.Property(t => t.UserDefinedString32).HasColumnName("UserDefinedString32");
            this.Property(t => t.UserDefinedString33).HasColumnName("UserDefinedString33");
            this.Property(t => t.UserDefinedString34).HasColumnName("UserDefinedString34");
            this.Property(t => t.UserDefinedString35).HasColumnName("UserDefinedString35");
            this.Property(t => t.UserDefinedString36).HasColumnName("UserDefinedString36");
            this.Property(t => t.UserDefinedString37).HasColumnName("UserDefinedString37");
            this.Property(t => t.UserDefinedString38).HasColumnName("UserDefinedString38");
            this.Property(t => t.UserDefinedString39).HasColumnName("UserDefinedString39");
            this.Property(t => t.UserDefinedString4).HasColumnName("UserDefinedString4");
            this.Property(t => t.UserDefinedString40).HasColumnName("UserDefinedString40");
            this.Property(t => t.UserDefinedString41).HasColumnName("UserDefinedString41");
            this.Property(t => t.UserDefinedString42).HasColumnName("UserDefinedString42");
            this.Property(t => t.UserDefinedString43).HasColumnName("UserDefinedString43");
            this.Property(t => t.UserDefinedString44).HasColumnName("UserDefinedString44");
            this.Property(t => t.UserDefinedString45).HasColumnName("UserDefinedString45");
            this.Property(t => t.UserDefinedString5).HasColumnName("UserDefinedString5");
            this.Property(t => t.UserDefinedString6).HasColumnName("UserDefinedString6");
            this.Property(t => t.UserDefinedString7).HasColumnName("UserDefinedString7");
            this.Property(t => t.UserDefinedString8).HasColumnName("UserDefinedString8");
            this.Property(t => t.UserDefinedString9).HasColumnName("UserDefinedString9");
            this.Property(t => t.UserID).HasColumnName("UserID");
            this.Property(t => t.InitialEnterDate).HasColumnName("InitialEnterDate");
            this.Property(t => t.OtherPremium).HasColumnName("OtherPremium");
            this.Property(t => t.AnnualTimes).HasColumnName("AnnualTimes");
        }
    }
}
