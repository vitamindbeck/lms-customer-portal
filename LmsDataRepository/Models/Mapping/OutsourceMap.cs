using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class OutsourceMap : EntityTypeConfiguration<Outsource>
    {
        public OutsourceMap()
        {
            // Primary Key
            this.HasKey(t => t.Outsource_ID);

            // Properties
            this.Property(t => t.Outsource_Name)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Outsource");
            this.Property(t => t.Outsource_ID).HasColumnName("Outsource_ID");
            this.Property(t => t.Outsource_Name).HasColumnName("Outsource_Name");
            this.Property(t => t.Created_On).HasColumnName("Created_On");
            this.Property(t => t.Active).HasColumnName("Active");
        }
    }
}
