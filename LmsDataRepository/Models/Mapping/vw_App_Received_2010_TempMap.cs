using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class vw_App_Received_2010_TempMap : EntityTypeConfiguration<vw_App_Received_2010_Temp>
    {
        public vw_App_Received_2010_TempMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Agent_Group, t.Agent, t.Sales_Coord, t.Record_ID, t.State, t.SourceCode, t.Carrier, t.Plan_Name, t.Plan_Type, t.New_Customer, t.Primary_Lead });

            // Properties
            this.Property(t => t.Agent_Group)
                .IsRequired()
                .HasMaxLength(32);

            this.Property(t => t.Agent)
                .IsRequired()
                .HasMaxLength(65);

            this.Property(t => t.Sales_Coord)
                .IsRequired()
                .HasMaxLength(65);

            this.Property(t => t.Record_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Customer_ID)
                .HasMaxLength(32);

            this.Property(t => t.State)
                .IsRequired()
                .HasMaxLength(32);

            this.Property(t => t.SourceCode)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.Policy_ID)
                .HasMaxLength(50);

            this.Property(t => t.Policy_Number)
                .HasMaxLength(50);

            this.Property(t => t.Current_Plan_Status)
                .HasMaxLength(100);

            this.Property(t => t.Carrier)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Plan_Name)
                .IsRequired()
                .HasMaxLength(150);

            this.Property(t => t.Plan_Type)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.New_Customer)
                .IsRequired()
                .HasMaxLength(3);

            this.Property(t => t.Primary_Lead)
                .IsRequired()
                .HasMaxLength(7);

            // Table & Column Mappings
            this.ToTable("vw_App_Received_2010_Temp");
            this.Property(t => t.Agent_Group).HasColumnName("Agent_Group");
            this.Property(t => t.Agent).HasColumnName("Agent");
            this.Property(t => t.Sales_Coord).HasColumnName("Sales_Coord");
            this.Property(t => t.Record_ID).HasColumnName("Record_ID");
            this.Property(t => t.Customer_ID).HasColumnName("Customer_ID");
            this.Property(t => t.State).HasColumnName("State");
            this.Property(t => t.SourceCode).HasColumnName("SourceCode");
            this.Property(t => t.Policy_ID).HasColumnName("Policy_ID");
            this.Property(t => t.Policy_Number).HasColumnName("Policy_Number");
            this.Property(t => t.Current_Plan_Status).HasColumnName("Current_Plan_Status");
            this.Property(t => t.Carrier).HasColumnName("Carrier");
            this.Property(t => t.Plan_Name).HasColumnName("Plan_Name");
            this.Property(t => t.Plan_Type).HasColumnName("Plan_Type");
            this.Property(t => t.Created_On).HasColumnName("Created_On");
            this.Property(t => t.AppReceivedDate).HasColumnName("AppReceivedDate");
            this.Property(t => t.AppSubmitDate).HasColumnName("AppSubmitDate");
            this.Property(t => t.AppDeliveryDate).HasColumnName("AppDeliveryDate");
            this.Property(t => t.AppPlacedDate).HasColumnName("AppPlacedDate");
            this.Property(t => t.FirstAppRec).HasColumnName("FirstAppRec");
            this.Property(t => t.LastAppRec).HasColumnName("LastAppRec");
            this.Property(t => t.New_Customer).HasColumnName("New_Customer");
            this.Property(t => t.Primary_Lead).HasColumnName("Primary_Lead");
        }
    }
}
