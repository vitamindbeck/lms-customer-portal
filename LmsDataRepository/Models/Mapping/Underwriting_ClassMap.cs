using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Underwriting_ClassMap : EntityTypeConfiguration<Underwriting_Class>
    {
        public Underwriting_ClassMap()
        {
            // Primary Key
            this.HasKey(t => t.Underwriting_ID);

            // Properties
            this.Property(t => t.Underwriting_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Description)
                .HasMaxLength(35);

            // Table & Column Mappings
            this.ToTable("Underwriting_Class");
            this.Property(t => t.Underwriting_ID).HasColumnName("Underwriting_ID");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.IsRemovable).HasColumnName("IsRemovable");
            this.Property(t => t.Display_Order).HasColumnName("Display_Order");
            this.Property(t => t.Active).HasColumnName("Active");
        }
    }
}
