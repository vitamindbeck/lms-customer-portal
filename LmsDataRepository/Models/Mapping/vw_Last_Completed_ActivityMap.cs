using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class vw_Last_Completed_ActivityMap : EntityTypeConfiguration<vw_Last_Completed_Activity>
    {
        public vw_Last_Completed_ActivityMap()
        {
            // Primary Key
            this.HasKey(t => t.Record_ID);

            // Properties
            this.Property(t => t.Record_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vw_Last_Completed_Activity");
            this.Property(t => t.Record_ID).HasColumnName("Record_ID");
            this.Property(t => t.Record_Activity_ID).HasColumnName("Record_Activity_ID");
        }
    }
}
