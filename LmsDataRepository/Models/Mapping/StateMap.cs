using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class StateMap : EntityTypeConfiguration<State>
    {
        public StateMap()
        {
            // Primary Key
            this.HasKey(t => t.State_ID);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(32);

            this.Property(t => t.Abbreviation)
                .IsRequired()
                .HasMaxLength(32);

            // Table & Column Mappings
            this.ToTable("State");
            this.Property(t => t.State_ID).HasColumnName("State_ID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Abbreviation).HasColumnName("Abbreviation");
            this.Property(t => t.Partnership).HasColumnName("Partnership");
            this.Property(t => t.Web_Quote).HasColumnName("Web_Quote");
            this.Property(t => t.Odd_Pay).HasColumnName("Odd_Pay");
        }
    }
}
