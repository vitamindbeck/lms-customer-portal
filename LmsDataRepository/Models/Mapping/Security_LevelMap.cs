using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Security_LevelMap : EntityTypeConfiguration<Security_Level>
    {
        public Security_LevelMap()
        {
            // Primary Key
            this.HasKey(t => t.Security_Level_ID);

            // Properties
            this.Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(32);

            // Table & Column Mappings
            this.ToTable("Security_Level");
            this.Property(t => t.Security_Level_ID).HasColumnName("Security_Level_ID");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.In_AD).HasColumnName("In_AD");
        }
    }
}
