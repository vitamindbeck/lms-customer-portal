using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class GlossaryMap : EntityTypeConfiguration<Glossary>
    {
        public GlossaryMap()
        {
            // Primary Key
            this.HasKey(t => t.Glossary_ID);

            // Properties
            this.Property(t => t.Term)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.Definition)
                .IsRequired();

            this.Property(t => t.Special_Flag)
                .IsRequired()
                .HasMaxLength(8);

            // Table & Column Mappings
            this.ToTable("Glossary");
            this.Property(t => t.Glossary_ID).HasColumnName("Glossary_ID");
            this.Property(t => t.Term).HasColumnName("Term");
            this.Property(t => t.Definition).HasColumnName("Definition");
            this.Property(t => t.Show_On_Master).HasColumnName("Show_On_Master");
            this.Property(t => t.Special_Flag).HasColumnName("Special_Flag");
        }
    }
}
