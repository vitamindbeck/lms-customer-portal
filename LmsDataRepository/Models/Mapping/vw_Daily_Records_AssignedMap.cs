using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class vw_Daily_Records_AssignedMap : EntityTypeConfiguration<vw_Daily_Records_Assigned>
    {
        public vw_Daily_Records_AssignedMap()
        {
            // Primary Key
            this.HasKey(t => new { t.First_Name, t.Last_Name, t.User_ID });

            // Properties
            this.Property(t => t.Assigned_Date)
                .HasMaxLength(30);

            this.Property(t => t.First_Name)
                .IsRequired()
                .HasMaxLength(32);

            this.Property(t => t.Last_Name)
                .IsRequired()
                .HasMaxLength(32);

            this.Property(t => t.User_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vw_Daily_Records_Assigned");
            this.Property(t => t.Assigned_Date).HasColumnName("Assigned_Date");
            this.Property(t => t.First_Name).HasColumnName("First_Name");
            this.Property(t => t.Last_Name).HasColumnName("Last_Name");
            this.Property(t => t.Records_Assigned).HasColumnName("Records_Assigned");
            this.Property(t => t.User_ID).HasColumnName("User_ID");
        }
    }
}
