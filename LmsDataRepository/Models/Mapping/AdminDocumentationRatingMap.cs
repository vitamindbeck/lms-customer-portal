using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class AdminDocumentationRatingMap : EntityTypeConfiguration<AdminDocumentationRating>
    {
        public AdminDocumentationRatingMap()
        {
            // Primary Key
            this.HasKey(t => new { t.AdminDocumentationID, t.Description, t.Active });

            // Properties
            this.Property(t => t.AdminDocumentationID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.Description)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("AdminDocumentationRating");
            this.Property(t => t.AdminDocumentationID).HasColumnName("AdminDocumentationID");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Active).HasColumnName("Active");
        }
    }
}
