using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Products_Of_InterestMap : EntityTypeConfiguration<Products_Of_Interest>
    {
        public Products_Of_InterestMap()
        {
            // Primary Key
            this.HasKey(t => t.Products_Of_Interest_ID);

            // Properties
            this.Property(t => t.Products_Of_Interest_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Products_Of_Interest_Desc)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("Products_Of_Interest");
            this.Property(t => t.Products_Of_Interest_ID).HasColumnName("Products_Of_Interest_ID");
            this.Property(t => t.Products_Of_Interest_Desc).HasColumnName("Products_Of_Interest_Desc");
            this.Property(t => t.Active).HasColumnName("Active");
            this.Property(t => t.Order).HasColumnName("Order");
        }
    }
}
