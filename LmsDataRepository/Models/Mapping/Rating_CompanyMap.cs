using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Rating_CompanyMap : EntityTypeConfiguration<Rating_Company>
    {
        public Rating_CompanyMap()
        {
            // Primary Key
            this.HasKey(t => t.Rating_Company_ID);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("Rating_Company");
            this.Property(t => t.Rating_Company_ID).HasColumnName("Rating_Company_ID");
            this.Property(t => t.Name).HasColumnName("Name");
        }
    }
}
