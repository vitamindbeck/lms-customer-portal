using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class vw_Screener_Dash_TotalsMap : EntityTypeConfiguration<vw_Screener_Dash_Totals>
    {
        public vw_Screener_Dash_TotalsMap()
        {
            // Primary Key
            this.HasKey(t => t.TimePeriod);

            // Properties
            this.Property(t => t.TimePeriod)
                .IsRequired()
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("vw_Screener_Dash_Totals");
            this.Property(t => t.TimePeriod).HasColumnName("TimePeriod");
            this.Property(t => t.Made_Contact_Under_5).HasColumnName("Made_Contact_Under_5");
            this.Property(t => t.Made_Contact_5to15).HasColumnName("Made_Contact_5to15");
            this.Property(t => t.Made_Contact_Over_15).HasColumnName("Made_Contact_Over_15");
            this.Property(t => t.Initial_Attempt_Under_5).HasColumnName("Initial_Attempt_Under_5");
            this.Property(t => t.Initial_Attempt_5to15).HasColumnName("Initial_Attempt_5to15");
            this.Property(t => t.Initial_Attempt_Over_15).HasColumnName("Initial_Attempt_Over_15");
            this.Property(t => t.Live_Transfer_Under_5).HasColumnName("Live_Transfer_Under_5");
            this.Property(t => t.Live_Transfer_5to15).HasColumnName("Live_Transfer_5to15");
            this.Property(t => t.Live_Transfer_Over_15).HasColumnName("Live_Transfer_Over_15");
            this.Property(t => t.No_Contact_Under_5).HasColumnName("No_Contact_Under_5");
            this.Property(t => t.No_Contact_5to15).HasColumnName("No_Contact_5to15");
            this.Property(t => t.No_Contact_Over_15).HasColumnName("No_Contact_Over_15");
            this.Property(t => t.Live_Transfer_Attempt_Under_5).HasColumnName("Live_Transfer_Attempt_Under_5");
            this.Property(t => t.Live_Transfer_Attempt_5to15).HasColumnName("Live_Transfer_Attempt_5to15");
            this.Property(t => t.Live_Transfer_Attempt_15).HasColumnName("Live_Transfer_Attempt_15");
            this.Property(t => t.Contacted_Set_Follow_Up_Under_5).HasColumnName("Contacted_Set_Follow_Up_Under_5");
            this.Property(t => t.Contacted_Set_Follow_Up_5to15).HasColumnName("Contacted_Set_Follow_Up_5to15");
            this.Property(t => t.Contacted_Set_Follow_Up_15).HasColumnName("Contacted_Set_Follow_Up_15");
            this.Property(t => t.Responses).HasColumnName("Responses");
            this.Property(t => t.Expr1).HasColumnName("Expr1");
        }
    }
}
