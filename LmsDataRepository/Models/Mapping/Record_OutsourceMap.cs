using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Record_OutsourceMap : EntityTypeConfiguration<Record_Outsource>
    {
        public Record_OutsourceMap()
        {
            // Primary Key
            this.HasKey(t => t.Record_Outsource_ID);

            // Properties
            this.Property(t => t.Campaign_ID)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Record_Outsource");
            this.Property(t => t.Record_Outsource_ID).HasColumnName("Record_Outsource_ID");
            this.Property(t => t.Record_ID).HasColumnName("Record_ID");
            this.Property(t => t.Outsource_ID).HasColumnName("Outsource_ID");
            this.Property(t => t.Campaign_ID).HasColumnName("Campaign_ID");
            this.Property(t => t.Outsourced).HasColumnName("Outsourced");
            this.Property(t => t.Outsource_Date).HasColumnName("Outsource_Date");
            this.Property(t => t.Record_Outsource_Created_On).HasColumnName("Record_Outsource_Created_On");
        }
    }
}
