using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Document_FormMap : EntityTypeConfiguration<Document_Form>
    {
        public Document_FormMap()
        {
            // Primary Key
            this.HasKey(t => t.Document_Form_ID);

            // Properties
            this.Property(t => t.Table_Name)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Join_Name)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Alias)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Join_Statement)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("Document_Form");
            this.Property(t => t.Document_Form_ID).HasColumnName("Document_Form_ID");
            this.Property(t => t.Document_ID).HasColumnName("Document_ID");
            this.Property(t => t.Table_Name).HasColumnName("Table_Name");
            this.Property(t => t.Display_Order).HasColumnName("Display_Order");
            this.Property(t => t.Join_Name).HasColumnName("Join_Name");
            this.Property(t => t.Alias).HasColumnName("Alias");
            this.Property(t => t.Join_Statement).HasColumnName("Join_Statement");
        }
    }
}
