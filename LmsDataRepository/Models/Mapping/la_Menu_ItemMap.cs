using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class la_Menu_ItemMap : EntityTypeConfiguration<la_Menu_Item>
    {
        public la_Menu_ItemMap()
        {
            // Primary Key
            this.HasKey(t => t.Menu_Item_ID);

            // Properties
            this.Property(t => t.Page_Name)
                .HasMaxLength(100);

            this.Property(t => t.Link_URL)
                .HasMaxLength(200);

            this.Property(t => t.Link_Text)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("la_Menu_Item");
            this.Property(t => t.Menu_Item_ID).HasColumnName("Menu_Item_ID");
            this.Property(t => t.Page_Name).HasColumnName("Page_Name");
            this.Property(t => t.Link_URL).HasColumnName("Link_URL");
            this.Property(t => t.Link_Text).HasColumnName("Link_Text");
            this.Property(t => t.Display_Order).HasColumnName("Display_Order");
            this.Property(t => t.Active).HasColumnName("Active");
        }
    }
}
