using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class la_Press_ReleaseMap : EntityTypeConfiguration<la_Press_Release>
    {
        public la_Press_ReleaseMap()
        {
            // Primary Key
            this.HasKey(t => t.la_Press_Release_ID);

            // Properties
            this.Property(t => t.Title)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(500);

            this.Property(t => t.File_URL)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("la_Press_Release");
            this.Property(t => t.la_Press_Release_ID).HasColumnName("la_Press_Release_ID");
            this.Property(t => t.Title).HasColumnName("Title");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.File_URL).HasColumnName("File_URL");
            this.Property(t => t.Active).HasColumnName("Active");
            this.Property(t => t.Release_Date).HasColumnName("Release_Date");
        }
    }
}
