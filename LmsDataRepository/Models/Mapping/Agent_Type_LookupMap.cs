using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Agent_Type_LookupMap : EntityTypeConfiguration<Agent_Type_Lookup>
    {
        public Agent_Type_LookupMap()
        {
            // Primary Key
            this.HasKey(t => t.Agent_Type_ID);

            // Properties
            this.Property(t => t.Agent_Type_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(30);

            // Table & Column Mappings
            this.ToTable("Agent_Type_Lookup");
            this.Property(t => t.Agent_Type_ID).HasColumnName("Agent_Type_ID");
            this.Property(t => t.Description).HasColumnName("Description");
        }
    }
}
