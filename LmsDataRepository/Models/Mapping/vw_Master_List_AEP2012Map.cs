using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class vw_Master_List_AEP2012Map : EntityTypeConfiguration<vw_Master_List_AEP2012>
    {
        public vw_Master_List_AEP2012Map()
        {
            // Primary Key
            this.HasKey(t => new { t.CustomerNumber, t.AgentGroup, t.LMS_Partner });

            // Properties
            this.Property(t => t.CustomerNumber)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.FirstAndMiddleName)
                .HasMaxLength(35);

            this.Property(t => t.LastName)
                .HasMaxLength(35);

            this.Property(t => t.Customer_ID)
                .HasMaxLength(32);

            this.Property(t => t.AgentGroup)
                .IsRequired()
                .HasMaxLength(6);

            this.Property(t => t.GBS_Agent)
                .HasMaxLength(35);

            this.Property(t => t.LMSRecAgent)
                .HasMaxLength(65);

            this.Property(t => t.LMSPolicyAgent)
                .HasMaxLength(65);

            this.Property(t => t.Product_Type)
                .HasMaxLength(35);

            this.Property(t => t.PolicyNumber)
                .HasMaxLength(55);

            this.Property(t => t.LMS_Partner)
                .IsRequired()
                .HasMaxLength(5);

            this.Property(t => t.Carrier)
                .HasMaxLength(50);

            this.Property(t => t.PlanName)
                .HasMaxLength(50);

            this.Property(t => t.PlanSelected)
                .HasMaxLength(35);

            this.Property(t => t.Address1)
                .HasMaxLength(35);

            this.Property(t => t.City)
                .HasMaxLength(35);

            this.Property(t => t.State)
                .HasMaxLength(2);

            this.Property(t => t.ZipCode)
                .HasMaxLength(10);

            this.Property(t => t.County)
                .HasMaxLength(35);

            this.Property(t => t.EMailAddress)
                .HasMaxLength(100);

            this.Property(t => t.Home_Phone)
                .HasMaxLength(32);

            this.Property(t => t.Work_Phone)
                .HasMaxLength(32);

            // Table & Column Mappings
            this.ToTable("vw_Master_List_AEP2012");
            this.Property(t => t.PolicyID).HasColumnName("PolicyID");
            this.Property(t => t.CustomerNumber).HasColumnName("CustomerNumber");
            this.Property(t => t.Record_ID).HasColumnName("Record_ID");
            this.Property(t => t.Plan_Transaction_ID).HasColumnName("Plan_Transaction_ID");
            this.Property(t => t.FirstAndMiddleName).HasColumnName("FirstAndMiddleName");
            this.Property(t => t.LastName).HasColumnName("LastName");
            this.Property(t => t.Customer_ID).HasColumnName("Customer_ID");
            this.Property(t => t.AgentGroup).HasColumnName("AgentGroup");
            this.Property(t => t.GBS_Agent).HasColumnName("GBS_Agent");
            this.Property(t => t.LMSRecAgent).HasColumnName("LMSRecAgent");
            this.Property(t => t.LMSPolicyAgent).HasColumnName("LMSPolicyAgent");
            this.Property(t => t.Product_Type).HasColumnName("Product_Type");
            this.Property(t => t.PolicyNumber).HasColumnName("PolicyNumber");
            this.Property(t => t.LMS_Partner).HasColumnName("LMS_Partner");
            this.Property(t => t.EffectiveDateOrPurchaseDate).HasColumnName("EffectiveDateOrPurchaseDate");
            this.Property(t => t.plantypeModernStandard).HasColumnName("plantypeModernStandard");
            this.Property(t => t.Carrier).HasColumnName("Carrier");
            this.Property(t => t.PlanName).HasColumnName("PlanName");
            this.Property(t => t.PlanSelected).HasColumnName("PlanSelected");
            this.Property(t => t.DOB).HasColumnName("DOB");
            this.Property(t => t.Age).HasColumnName("Age");
            this.Property(t => t.Address1).HasColumnName("Address1");
            this.Property(t => t.City).HasColumnName("City");
            this.Property(t => t.State).HasColumnName("State");
            this.Property(t => t.ZipCode).HasColumnName("ZipCode");
            this.Property(t => t.County).HasColumnName("County");
            this.Property(t => t.EMailAddress).HasColumnName("EMailAddress");
            this.Property(t => t.PriorityStatus).HasColumnName("PriorityStatus");
            this.Property(t => t.Home_Phone).HasColumnName("Home_Phone");
            this.Property(t => t.Work_Phone).HasColumnName("Work_Phone");
        }
    }
}
