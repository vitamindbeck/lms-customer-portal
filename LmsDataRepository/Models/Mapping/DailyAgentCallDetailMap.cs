using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class DailyAgentCallDetailMap : EntityTypeConfiguration<DailyAgentCallDetail>
    {
        public DailyAgentCallDetailMap()
        {
            // Primary Key
            this.HasKey(t => t.DailyAgentCallDetailsID);

            // Properties
            this.Property(t => t.Agent_ext)
                .HasMaxLength(10);

            this.Property(t => t.Logged_In_ext)
                .HasMaxLength(10);

            this.Property(t => t.Agent_Status)
                .HasMaxLength(25);

            this.Property(t => t.Hunt_Group)
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("DailyAgentCallDetails");
            this.Property(t => t.DailyAgentCallDetailsID).HasColumnName("DailyAgentCallDetailsID");
            this.Property(t => t.Status_Changed_On).HasColumnName("Status_Changed_On");
            this.Property(t => t.Agent_ext).HasColumnName("Agent_ext");
            this.Property(t => t.Logged_In_ext).HasColumnName("Logged_In_ext");
            this.Property(t => t.Agent_Status).HasColumnName("Agent_Status");
            this.Property(t => t.Hunt_Group).HasColumnName("Hunt_Group");
            this.Property(t => t.Created_On).HasColumnName("Created_On");
        }
    }
}
