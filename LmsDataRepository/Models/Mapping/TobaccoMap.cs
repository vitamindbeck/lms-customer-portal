using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class TobaccoMap : EntityTypeConfiguration<Tobacco>
    {
        public TobaccoMap()
        {
            // Primary Key
            this.HasKey(t => t.Tobacco_ID);

            // Properties
            this.Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("Tobacco");
            this.Property(t => t.Tobacco_ID).HasColumnName("Tobacco_ID");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Display_Order).HasColumnName("Display_Order");
            this.Property(t => t.Active).HasColumnName("Active");
        }
    }
}
