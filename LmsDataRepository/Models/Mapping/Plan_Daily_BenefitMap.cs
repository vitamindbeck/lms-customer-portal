using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Plan_Daily_BenefitMap : EntityTypeConfiguration<Plan_Daily_Benefit>
    {
        public Plan_Daily_BenefitMap()
        {
            // Primary Key
            this.HasKey(t => t.Plan_Daily_Benefit_ID);

            // Properties
            this.Property(t => t.Description)
                .HasMaxLength(50);

            this.Property(t => t.HC_Description)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Plan_Daily_Benefit");
            this.Property(t => t.Plan_Daily_Benefit_ID).HasColumnName("Plan_Daily_Benefit_ID");
            this.Property(t => t.Plan_ID).HasColumnName("Plan_ID");
            this.Property(t => t.Low_Range).HasColumnName("Low_Range");
            this.Property(t => t.High_Range).HasColumnName("High_Range");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.HC_Description).HasColumnName("HC_Description");
            this.Property(t => t.Start_Age).HasColumnName("Start_Age");
            this.Property(t => t.End_Age).HasColumnName("End_Age");
        }
    }
}
