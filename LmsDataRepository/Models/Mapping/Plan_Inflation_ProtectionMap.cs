using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Plan_Inflation_ProtectionMap : EntityTypeConfiguration<Plan_Inflation_Protection>
    {
        public Plan_Inflation_ProtectionMap()
        {
            // Primary Key
            this.HasKey(t => t.Plan_Inflation_Protection_ID);

            // Properties
            this.Property(t => t.Code)
                .HasMaxLength(50);

            this.Property(t => t.Description)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Plan_Inflation_Protection");
            this.Property(t => t.Plan_Inflation_Protection_ID).HasColumnName("Plan_Inflation_Protection_ID");
            this.Property(t => t.Code).HasColumnName("Code");
            this.Property(t => t.Description).HasColumnName("Description");
        }
    }
}
