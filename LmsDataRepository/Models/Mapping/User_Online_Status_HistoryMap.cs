using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class User_Online_Status_HistoryMap : EntityTypeConfiguration<User_Online_Status_History>
    {
        public User_Online_Status_HistoryMap()
        {
            // Primary Key
            this.HasKey(t => t.User_Online_Status_History_ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("User_Online_Status_History");
            this.Property(t => t.User_Online_Status_History_ID).HasColumnName("User_Online_Status_History_ID");
            this.Property(t => t.User_ID).HasColumnName("User_ID");
            this.Property(t => t.User_Online_Status_Old).HasColumnName("User_Online_Status_Old");
            this.Property(t => t.User_Online_Status_New).HasColumnName("User_Online_Status_New");
            this.Property(t => t.Created_On).HasColumnName("Created_On");
        }
    }
}
