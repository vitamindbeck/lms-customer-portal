using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Record_DistributionMap : EntityTypeConfiguration<Record_Distribution>
    {
        public Record_DistributionMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Record_Distribution_Type_ID, t.Record_ID });

            // Properties
            this.Property(t => t.Record_Distribution_Type_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Record_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("Record_Distribution");
            this.Property(t => t.Record_Distribution_Type_ID).HasColumnName("Record_Distribution_Type_ID");
            this.Property(t => t.Record_ID).HasColumnName("Record_ID");
        }
    }
}
