using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Carrier_State_AuthorizedMap : EntityTypeConfiguration<Carrier_State_Authorized>
    {
        public Carrier_State_AuthorizedMap()
        {
            // Primary Key
            this.HasKey(t => t.Carrier_State_Authorized_ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("Carrier_State_Authorized");
            this.Property(t => t.Carrier_State_Authorized_ID).HasColumnName("Carrier_State_Authorized_ID");
            this.Property(t => t.State_ID).HasColumnName("State_ID");
            this.Property(t => t.Carrier_ID).HasColumnName("Carrier_ID");
            this.Property(t => t.Created_On).HasColumnName("Created_On");
        }
    }
}
