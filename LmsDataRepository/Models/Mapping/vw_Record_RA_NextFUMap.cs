using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class vw_Record_RA_NextFUMap : EntityTypeConfiguration<vw_Record_RA_NextFU>
    {
        public vw_Record_RA_NextFUMap()
        {
            // Primary Key
            this.HasKey(t => t.Record_ID);

            // Properties
            this.Property(t => t.Record_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vw_Record_RA_NextFU");
            this.Property(t => t.Record_ID).HasColumnName("Record_ID");
            this.Property(t => t.Record_RA_Due_Date).HasColumnName("Record_RA_Due_Date");
        }
    }
}
