using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Marital_StatusMap : EntityTypeConfiguration<Marital_Status>
    {
        public Marital_StatusMap()
        {
            // Primary Key
            this.HasKey(t => t.Marital_Status_ID);

            // Properties
            this.Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("Marital_Status");
            this.Property(t => t.Marital_Status_ID).HasColumnName("Marital_Status_ID");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Display_Order).HasColumnName("Display_Order");
            this.Property(t => t.GBS_ID).HasColumnName("GBS_ID");
        }
    }
}
