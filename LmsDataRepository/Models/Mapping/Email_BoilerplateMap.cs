using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Email_BoilerplateMap : EntityTypeConfiguration<Email_Boilerplate>
    {
        public Email_BoilerplateMap()
        {
            // Primary Key
            this.HasKey(t => t.Email_Boilerplate_ID);

            // Properties
            this.Property(t => t.Subject)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("Email_Boilerplate");
            this.Property(t => t.Email_Boilerplate_ID).HasColumnName("Email_Boilerplate_ID");
            this.Property(t => t.Body_Text).HasColumnName("Body_Text");
            this.Property(t => t.Body_HTML).HasColumnName("Body_HTML");
            this.Property(t => t.Subject).HasColumnName("Subject");
            this.Property(t => t.Email_Number).HasColumnName("Email_Number");
        }
    }
}
