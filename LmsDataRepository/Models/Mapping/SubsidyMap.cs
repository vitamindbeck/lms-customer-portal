using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class SubsidyMap : EntityTypeConfiguration<Subsidy>
    {
        public SubsidyMap()
        {
            // Primary Key
            this.HasKey(t => t.Subsidy_ID);

            // Properties
            this.Property(t => t.Subsidy_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Subsidy_Desc)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Subsidy");
            this.Property(t => t.Subsidy_ID).HasColumnName("Subsidy_ID");
            this.Property(t => t.Subsidy_Desc).HasColumnName("Subsidy_Desc");
            this.Property(t => t.Active).HasColumnName("Active");
            this.Property(t => t.Display_Order).HasColumnName("Display_Order");
        }
    }
}
