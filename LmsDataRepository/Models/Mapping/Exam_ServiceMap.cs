using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Exam_ServiceMap : EntityTypeConfiguration<Exam_Service>
    {
        public Exam_ServiceMap()
        {
            // Primary Key
            this.HasKey(t => t.Exam_Service_ID);

            // Properties
            this.Property(t => t.Description)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("Exam_Service");
            this.Property(t => t.Exam_Service_ID).HasColumnName("Exam_Service_ID");
            this.Property(t => t.Description).HasColumnName("Description");
        }
    }
}
