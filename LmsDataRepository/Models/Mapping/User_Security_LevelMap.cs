using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class User_Security_LevelMap : EntityTypeConfiguration<User_Security_Level>
    {
        public User_Security_LevelMap()
        {
            // Primary Key
            this.HasKey(t => new { t.User_ID, t.Security_Level_ID });

            // Properties
            this.Property(t => t.User_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Security_Level_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("User_Security_Level");
            this.Property(t => t.User_ID).HasColumnName("User_ID");
            this.Property(t => t.Security_Level_ID).HasColumnName("Security_Level_ID");
            this.Property(t => t.Created_On).HasColumnName("Created_On");

            // Relationships
            this.HasRequired(t => t.User)
                .WithMany(t => t.User_Security_Level)
                .HasForeignKey(d => d.User_ID);

        }
    }
}
