using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class vw_AEP_2013_Uncontacted_Plan_TransactionsMap : EntityTypeConfiguration<vw_AEP_2013_Uncontacted_Plan_Transactions>
    {
        public vw_AEP_2013_Uncontacted_Plan_TransactionsMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Plan_Transaction_ID, t.Uncontacted, t.Expr1 });

            // Properties
            this.Property(t => t.Plan_Transaction_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Uncontacted)
                .IsRequired()
                .HasMaxLength(11);

            this.Property(t => t.Expr1)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vw_AEP_2013_Uncontacted_Plan_Transactions");
            this.Property(t => t.Plan_Transaction_ID).HasColumnName("Plan_Transaction_ID");
            this.Property(t => t.Uncontacted).HasColumnName("Uncontacted");
            this.Property(t => t.Expr1).HasColumnName("Expr1");
        }
    }
}
