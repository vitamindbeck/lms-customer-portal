using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class StuffMap : EntityTypeConfiguration<Stuff>
    {
        public StuffMap()
        {
            // Primary Key
            this.HasKey(t => t.StuffID);

            // Properties
            this.Property(t => t.SSN)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Stuff");
            this.Property(t => t.StuffID).HasColumnName("StuffID");
            this.Property(t => t.Vertex).HasColumnName("Vertex");
            this.Property(t => t.SSN).HasColumnName("SSN");
        }
    }
}
