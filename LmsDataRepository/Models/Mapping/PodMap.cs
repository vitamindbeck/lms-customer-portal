using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class PodMap : EntityTypeConfiguration<Pod>
    {
        public PodMap()
        {
            // Primary Key
            this.HasKey(t => t.Pod_ID);

            // Properties
            this.Property(t => t.Pod_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Pod_Name)
                .IsRequired()
                .HasMaxLength(25);

            // Table & Column Mappings
            this.ToTable("Pod");
            this.Property(t => t.Pod_ID).HasColumnName("Pod_ID");
            this.Property(t => t.Pod_Name).HasColumnName("Pod_Name");
        }
    }
}
