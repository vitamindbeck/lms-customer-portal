using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class CertificationMap : EntityTypeConfiguration<Certification>
    {
        public CertificationMap()
        {
            // Primary Key
            this.HasKey(t => t.Certification_ID);

            // Properties
            this.Property(t => t.Certification_Link)
                .HasMaxLength(500);

            this.Property(t => t.Certification_User_Name)
                .HasMaxLength(500);

            this.Property(t => t.Certification_Password)
                .HasMaxLength(500);

            this.Property(t => t.Special_Instructions)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("Certification");
            this.Property(t => t.Certification_ID).HasColumnName("Certification_ID");
            this.Property(t => t.Agent_ID).HasColumnName("Agent_ID");
            this.Property(t => t.Carrier_ID).HasColumnName("Carrier_ID");
            this.Property(t => t.Non_Carrier_ID).HasColumnName("Non_Carrier_ID");
            this.Property(t => t.Notified).HasColumnName("Notified");
            this.Property(t => t.Registered_Date).HasColumnName("Registered_Date");
            this.Property(t => t.Complete_Date).HasColumnName("Complete_Date");
            this.Property(t => t.Certification_Status_ID).HasColumnName("Certification_Status_ID");
            this.Property(t => t.Certification_Link).HasColumnName("Certification_Link");
            this.Property(t => t.Certification_User_Name).HasColumnName("Certification_User_Name");
            this.Property(t => t.Certification_Password).HasColumnName("Certification_Password");
            this.Property(t => t.Special_Instructions).HasColumnName("Special_Instructions");
            this.Property(t => t.Due_Date).HasColumnName("Due_Date");
            this.Property(t => t.Certification_Type_ID).HasColumnName("Certification_Type_ID");
            this.Property(t => t.Last_Updated_Date).HasColumnName("Last_Updated_Date");
            this.Property(t => t.Updated_By).HasColumnName("Updated_By");
        }
    }
}
