using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Online_StatusMap : EntityTypeConfiguration<Online_Status>
    {
        public Online_StatusMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Online_Status_ID, t.Online_Status1 });

            // Properties
            this.Property(t => t.Online_Status_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Online_Status1)
                .IsRequired()
                .HasMaxLength(25);

            // Table & Column Mappings
            this.ToTable("Online_Status");
            this.Property(t => t.Online_Status_ID).HasColumnName("Online_Status_ID");
            this.Property(t => t.Online_Status1).HasColumnName("Online_Status");
        }
    }
}
