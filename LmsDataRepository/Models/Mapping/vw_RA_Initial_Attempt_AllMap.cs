using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class vw_RA_Initial_Attempt_AllMap : EntityTypeConfiguration<vw_RA_Initial_Attempt_All>
    {
        public vw_RA_Initial_Attempt_AllMap()
        {
            // Primary Key
            this.HasKey(t => t.Record_ID);

            // Properties
            this.Property(t => t.Record_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vw_RA_Initial_Attempt_All");
            this.Property(t => t.Record_ID).HasColumnName("Record_ID");
            this.Property(t => t.Initial_Attempt_RA_ID).HasColumnName("Initial_Attempt_RA_ID");
        }
    }
}
