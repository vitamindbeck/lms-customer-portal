using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Process_TypeMap : EntityTypeConfiguration<Process_Type>
    {
        public Process_TypeMap()
        {
            // Primary Key
            this.HasKey(t => t.Process_Type_ID);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("Process_Type");
            this.Property(t => t.Process_Type_ID).HasColumnName("Process_Type_ID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Active).HasColumnName("Active");
        }
    }
}
