using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Email_HistoryMap : EntityTypeConfiguration<Email_History>
    {
        public Email_HistoryMap()
        {
            // Primary Key
            this.HasKey(t => t.Email_History_ID);

            // Properties
            this.Property(t => t.Customer_ID)
                .HasMaxLength(32);

            this.Property(t => t.Email_Address)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("Email_History");
            this.Property(t => t.Email_History_ID).HasColumnName("Email_History_ID");
            this.Property(t => t.Record_ID).HasColumnName("Record_ID");
            this.Property(t => t.Customer_ID).HasColumnName("Customer_ID");
            this.Property(t => t.Email_Boilerplate_ID).HasColumnName("Email_Boilerplate_ID");
            this.Property(t => t.Sent_Date).HasColumnName("Sent_Date");
            this.Property(t => t.Email_Address).HasColumnName("Email_Address");
        }
    }
}
