using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class System_LockMap : EntityTypeConfiguration<System_Lock>
    {
        public System_LockMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Table_Name, t.Key, t.Expires_On, t.Created_On, t.User_ID });

            // Properties
            this.Property(t => t.Table_Name)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.Key)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.User_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("System_Lock");
            this.Property(t => t.Table_Name).HasColumnName("Table_Name");
            this.Property(t => t.Key).HasColumnName("Key");
            this.Property(t => t.Expires_On).HasColumnName("Expires_On");
            this.Property(t => t.Created_On).HasColumnName("Created_On");
            this.Property(t => t.User_ID).HasColumnName("User_ID");
        }
    }
}
