using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class TransactionPaymentItemMap : EntityTypeConfiguration<TransactionPaymentItem>
    {
        public TransactionPaymentItemMap()
        {
            // Primary Key
            this.HasKey(t => t.ItemID);

            // Properties
            this.Property(t => t.AdminCheckNumber)
                .HasMaxLength(12);

            this.Property(t => t.BillingPeriod)
                .HasMaxLength(50);

            this.Property(t => t.Comment)
                .HasMaxLength(70);

            this.Property(t => t.RecordType)
                .HasMaxLength(10);

            this.Property(t => t.Comment2)
                .HasMaxLength(70);

            // Table & Column Mappings
            this.ToTable("TransactionPaymentItems");
            this.Property(t => t.AdminCheckAmount).HasColumnName("AdminCheckAmount");
            this.Property(t => t.AdminCheckNumber).HasColumnName("AdminCheckNumber");
            this.Property(t => t.AdministratorID).HasColumnName("AdministratorID");
            this.Property(t => t.AgentID).HasColumnName("AgentID");
            this.Property(t => t.AmountEntered).HasColumnName("AmountEntered");
            this.Property(t => t.BillingPeriod).HasColumnName("BillingPeriod");
            this.Property(t => t.BillingPeriodDate).HasColumnName("BillingPeriodDate");
            this.Property(t => t.CheckAdjustment).HasColumnName("CheckAdjustment");
            this.Property(t => t.CheckAmount).HasColumnName("CheckAmount");
            this.Property(t => t.CheckNumber).HasColumnName("CheckNumber");
            this.Property(t => t.Comment).HasColumnName("Comment");
            this.Property(t => t.CommissionType).HasColumnName("CommissionType");
            this.Property(t => t.DateCheckReceived).HasColumnName("DateCheckReceived");
            this.Property(t => t.GAID).HasColumnName("GAID");
            this.Property(t => t.IsBalanced).HasColumnName("IsBalanced");
            this.Property(t => t.IsPosted).HasColumnName("IsPosted");
            this.Property(t => t.IsUnPosted).HasColumnName("IsUnPosted");
            this.Property(t => t.ItemID).HasColumnName("ItemID");
            this.Property(t => t.NumberOfItems).HasColumnName("NumberOfItems");
            this.Property(t => t.NumberOfLives).HasColumnName("NumberOfLives");
            this.Property(t => t.PayingAgencyID).HasColumnName("PayingAgencyID");
            this.Property(t => t.PaymentAmount).HasColumnName("PaymentAmount");
            this.Property(t => t.PaymentType).HasColumnName("PaymentType");
            this.Property(t => t.PolicyID).HasColumnName("PolicyID");
            this.Property(t => t.PostedDate).HasColumnName("PostedDate");
            this.Property(t => t.PremiumAmount).HasColumnName("PremiumAmount");
            this.Property(t => t.RecordAmount).HasColumnName("RecordAmount");
            this.Property(t => t.RecordType).HasColumnName("RecordType");
            this.Property(t => t.Report1099).HasColumnName("Report1099");
            this.Property(t => t.SplitID).HasColumnName("SplitID");
            this.Property(t => t.TransactionID).HasColumnName("TransactionID");
            this.Property(t => t.TransactionPaymentID).HasColumnName("TransactionPaymentID");
            this.Property(t => t.Comment2).HasColumnName("Comment2");
            this.Property(t => t.IsNoCheck).HasColumnName("IsNoCheck");
        }
    }
}
