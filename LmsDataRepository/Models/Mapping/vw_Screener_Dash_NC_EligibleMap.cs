using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class vw_Screener_Dash_NC_EligibleMap : EntityTypeConfiguration<vw_Screener_Dash_NC_Eligible>
    {
        public vw_Screener_Dash_NC_EligibleMap()
        {
            // Primary Key
            this.HasKey(t => t.Agent_ID);

            // Properties
            this.Property(t => t.Count_Date)
                .HasMaxLength(15);

            this.Property(t => t.Agent_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vw_Screener_Dash_NC_Eligible");
            this.Property(t => t.Count_Date).HasColumnName("Count_Date");
            this.Property(t => t.IA_UL_E).HasColumnName("IA_UL_E");
            this.Property(t => t.Call_Status_E).HasColumnName("Call_Status_E");
            this.Property(t => t.Contacted_E).HasColumnName("Contacted_E");
            this.Property(t => t.Live_Transfer_E).HasColumnName("Live_Transfer_E");
            this.Property(t => t.Set_Appointment_E).HasColumnName("Set_Appointment_E");
            this.Property(t => t.Follow_Up_Screener_E).HasColumnName("Follow_Up_Screener_E");
            this.Property(t => t.Momentum).HasColumnName("Momentum");
            this.Property(t => t.Agent_ID).HasColumnName("Agent_ID");
        }
    }
}
