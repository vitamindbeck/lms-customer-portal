using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class ComplaintMap : EntityTypeConfiguration<Complaint>
    {
        public ComplaintMap()
        {
            // Primary Key
            this.HasKey(t => t.Complaint_ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("Complaint");
            this.Property(t => t.Complaint_ID).HasColumnName("Complaint_ID");
            this.Property(t => t.Record_ID).HasColumnName("Record_ID");
            this.Property(t => t.Current_Record_Activity_Status_ID).HasColumnName("Current_Record_Activity_Status_ID");
            this.Property(t => t.Resolved).HasColumnName("Resolved");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
        }
    }
}
