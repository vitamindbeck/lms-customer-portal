using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Source_CodeMap : EntityTypeConfiguration<Source_Code>
    {
        public Source_CodeMap()
        {
            // Primary Key
            this.HasKey(t => t.Source_Code_ID);

            // Properties
            this.Property(t => t.Source_Number)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.Notes)
                .HasMaxLength(1000);

            this.Property(t => t.Source_Phone_Number)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Source_Code");
            this.Property(t => t.Source_Code_ID).HasColumnName("Source_Code_ID");
            this.Property(t => t.Source_Number).HasColumnName("Source_Number");
            this.Property(t => t.Group_ID).HasColumnName("Group_ID");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Launch_Date).HasColumnName("Launch_Date");
            this.Property(t => t.Affinity_Partner_ID).HasColumnName("Affinity_Partner_ID");
            this.Property(t => t.Paid).HasColumnName("Paid");
            this.Property(t => t.Cost).HasColumnName("Cost");
            this.Property(t => t.Notes).HasColumnName("Notes");
            this.Property(t => t.Web_Select).HasColumnName("Web_Select");
            this.Property(t => t.Active).HasColumnName("Active");
            this.Property(t => t.Marketing_Group_ID).HasColumnName("Marketing_Group_ID");
            this.Property(t => t.Media_Type_ID).HasColumnName("Media_Type_ID");
            this.Property(t => t.Circulation).HasColumnName("Circulation");
            this.Property(t => t.Allow_Mailing).HasColumnName("Allow_Mailing");
            this.Property(t => t.LTC).HasColumnName("LTC");
            this.Property(t => t.Source_Phone_Number).HasColumnName("Source_Phone_Number");
        }
    }
}
