using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Plan_DocumentMap : EntityTypeConfiguration<Plan_Document>
    {
        public Plan_DocumentMap()
        {
            // Primary Key
            this.HasKey(t => t.Plan_Document_ID);

            // Properties
            this.Property(t => t.Title)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Location)
                .IsRequired()
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("Plan_Document");
            this.Property(t => t.Plan_Document_ID).HasColumnName("Plan_Document_ID");
            this.Property(t => t.Plan_ID).HasColumnName("Plan_ID");
            this.Property(t => t.State_ID).HasColumnName("State_ID");
            this.Property(t => t.Title).HasColumnName("Title");
            this.Property(t => t.Location).HasColumnName("Location");
            this.Property(t => t.Active).HasColumnName("Active");
            this.Property(t => t.Display_Order).HasColumnName("Display_Order");
            this.Property(t => t.Parent_Plan_Document_ID).HasColumnName("Parent_Plan_Document_ID");
            this.Property(t => t.User_ID).HasColumnName("User_ID");
        }
    }
}
