using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Reconciliation_TypeMap : EntityTypeConfiguration<Reconciliation_Type>
    {
        public Reconciliation_TypeMap()
        {
            // Primary Key
            this.HasKey(t => t.Reconciliation_Type_ID);

            // Properties
            this.Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("Reconciliation_Type");
            this.Property(t => t.Reconciliation_Type_ID).HasColumnName("Reconciliation_Type_ID");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Display_Order).HasColumnName("Display_Order");
            this.Property(t => t.Active).HasColumnName("Active");
            this.Property(t => t.Plan_Transaction_Status_ID).HasColumnName("Plan_Transaction_Status_ID");
            this.Property(t => t.Record_Type_ID).HasColumnName("Record_Type_ID");
            this.Property(t => t.Resolved_Record_Type_ID).HasColumnName("Resolved_Record_Type_ID");
            this.Property(t => t.GBSTypeId).HasColumnName("GBSTypeId");
        }
    }
}
