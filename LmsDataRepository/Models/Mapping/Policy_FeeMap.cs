using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Policy_FeeMap : EntityTypeConfiguration<Policy_Fee>
    {
        public Policy_FeeMap()
        {
            // Primary Key
            this.HasKey(t => t.Policy_Fee_ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("Policy_Fee");
            this.Property(t => t.Policy_Fee_ID).HasColumnName("Policy_Fee_ID");
            this.Property(t => t.Plan_ID).HasColumnName("Plan_ID");
            this.Property(t => t.Age_Lowest).HasColumnName("Age_Lowest");
            this.Property(t => t.Age_Highest).HasColumnName("Age_Highest");
            this.Property(t => t.Coverage_Low).HasColumnName("Coverage_Low");
            this.Property(t => t.Coverage_High).HasColumnName("Coverage_High");
            this.Property(t => t.Fee).HasColumnName("Fee");
            this.Property(t => t.Commisionable).HasColumnName("Commisionable");
        }
    }
}
