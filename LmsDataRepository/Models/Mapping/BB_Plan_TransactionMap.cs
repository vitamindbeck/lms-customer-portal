using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class BB_Plan_TransactionMap : EntityTypeConfiguration<BB_Plan_Transaction>
    {
        public BB_Plan_TransactionMap()
        {
            // Primary Key
            this.HasKey(t => t.BB_Plan_Transaction_ID);

            // Properties
            this.Property(t => t.BB_Plan_Transaction_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("BB_Plan_Transaction");
            this.Property(t => t.BB_Plan_Transaction_ID).HasColumnName("BB_Plan_Transaction_ID");
            this.Property(t => t.Plan_Transaction_ID).HasColumnName("Plan_Transaction_ID");
            this.Property(t => t.Created_On).HasColumnName("Created_On");
        }
    }
}
