using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Activity_TypeMap : EntityTypeConfiguration<Activity_Type>
    {
        public Activity_TypeMap()
        {
            // Primary Key
            this.HasKey(t => t.Activity_Type_ID);

            // Properties
            this.Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("Activity_Type");
            this.Property(t => t.Activity_Type_ID).HasColumnName("Activity_Type_ID");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Assign_To).HasColumnName("Assign_To");
            this.Property(t => t.Active_Current).HasColumnName("Active_Current");
            this.Property(t => t.Active_Followup).HasColumnName("Active_Followup");
            this.Property(t => t.Active_Edit).HasColumnName("Active_Edit");
        }
    }
}
