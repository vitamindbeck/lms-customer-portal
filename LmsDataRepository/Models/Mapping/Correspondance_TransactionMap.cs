using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Correspondance_TransactionMap : EntityTypeConfiguration<Correspondance_Transaction>
    {
        public Correspondance_TransactionMap()
        {
            // Primary Key
            this.HasKey(t => t.Correspondence_ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("Correspondance_Transaction");
            this.Property(t => t.Correspondence_ID).HasColumnName("Correspondence_ID");
            this.Property(t => t.Record_ID).HasColumnName("Record_ID");
            this.Property(t => t.User_ID).HasColumnName("User_ID");
            this.Property(t => t.Letter_ID).HasColumnName("Letter_ID");
            this.Property(t => t.Sent_Date).HasColumnName("Sent_Date");
        }
    }
}
