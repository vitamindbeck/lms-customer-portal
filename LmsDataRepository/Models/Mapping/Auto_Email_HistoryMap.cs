using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Auto_Email_HistoryMap : EntityTypeConfiguration<Auto_Email_History>
    {
        public Auto_Email_HistoryMap()
        {
            // Primary Key
            this.HasKey(t => t.Auto_Email_History_ID);

            // Properties
            this.Property(t => t.To_Address)
                .IsRequired()
                .HasMaxLength(1000);

            // Table & Column Mappings
            this.ToTable("Auto_Email_History");
            this.Property(t => t.Auto_Email_History_ID).HasColumnName("Auto_Email_History_ID");
            this.Property(t => t.Auto_Email_ID).HasColumnName("Auto_Email_ID");
            this.Property(t => t.To_Address).HasColumnName("To_Address");
            this.Property(t => t.Sent_Date).HasColumnName("Sent_Date");
            this.Property(t => t.Success).HasColumnName("Success");
            this.Property(t => t.Record_ID).HasColumnName("Record_ID");
        }
    }
}
