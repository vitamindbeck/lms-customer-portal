using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class BB_RecordMap : EntityTypeConfiguration<BB_Record>
    {
        public BB_RecordMap()
        {
            // Primary Key
            this.HasKey(t => t.BB_Record_ID);

            // Properties
            this.Property(t => t.BB_Record_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("BB_Record");
            this.Property(t => t.Record_ID).HasColumnName("Record_ID");
            this.Property(t => t.BB_Record_ID).HasColumnName("BB_Record_ID");
            this.Property(t => t.Created_On).HasColumnName("Created_On");
        }
    }
}
