using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Plan_Transaction_RequestMap : EntityTypeConfiguration<Plan_Transaction_Request>
    {
        public Plan_Transaction_RequestMap()
        {
            // Primary Key
            this.HasKey(t => t.Plan_Transaction_Request_ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("Plan_Transaction_Request");
            this.Property(t => t.Plan_Transaction_Request_ID).HasColumnName("Plan_Transaction_Request_ID");
            this.Property(t => t.Plan_Transaction_ID).HasColumnName("Plan_Transaction_ID");
            this.Property(t => t.To_Employee_ID).HasColumnName("To_Employee_ID");
            this.Property(t => t.From_Employee_ID).HasColumnName("From_Employee_ID");
            this.Property(t => t.Question).HasColumnName("Question");
            this.Property(t => t.Answer).HasColumnName("Answer");
            this.Property(t => t.Requested_On).HasColumnName("Requested_On");
            this.Property(t => t.Answered_On).HasColumnName("Answered_On");
            this.Property(t => t.Answered_By).HasColumnName("Answered_By");
        }
    }
}
