using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class QuoteMap : EntityTypeConfiguration<Quote>
    {
        public QuoteMap()
        {
            // Primary Key
            this.HasKey(t => t.Quote_ID);

            // Properties
            this.Property(t => t.Quote_Number)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Quote");
            this.Property(t => t.Quote_ID).HasColumnName("Quote_ID");
            this.Property(t => t.Record_ID).HasColumnName("Record_ID");
            this.Property(t => t.Quote_Number).HasColumnName("Quote_Number");
            this.Property(t => t.Quote_Elimination_Period_ID).HasColumnName("Quote_Elimination_Period_ID");
            this.Property(t => t.Quote_Benefit_Length_ID).HasColumnName("Quote_Benefit_Length_ID");
            this.Property(t => t.Quote_Inflation_Protection_ID).HasColumnName("Quote_Inflation_Protection_ID");
            this.Property(t => t.Quote_Health_Code_ID).HasColumnName("Quote_Health_Code_ID");
            this.Property(t => t.Daily_Max).HasColumnName("Daily_Max");
            this.Property(t => t.Nonforfeiture).HasColumnName("Nonforfeiture");
            this.Property(t => t.Created_On).HasColumnName("Created_On");
            this.Property(t => t.Quote_Method_ID).HasColumnName("Quote_Method_ID");
            this.Property(t => t.Per_Application_Cap).HasColumnName("Per_Application_Cap");
            this.Property(t => t.Number_Of_Quotes).HasColumnName("Number_Of_Quotes");
        }
    }
}
