using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class vw_Completed_Record_Activities_PriorityMap : EntityTypeConfiguration<vw_Completed_Record_Activities_Priority>
    {
        public vw_Completed_Record_Activities_PriorityMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Record_ID, t.Record_Activity_ID, t.Plan_Transaction_ID });

            // Properties
            this.Property(t => t.Record_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Record_Activity_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Plan_Transaction_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vw_Completed_Record_Activities_Priority");
            this.Property(t => t.Record_ID).HasColumnName("Record_ID");
            this.Property(t => t.Record_Activity_ID).HasColumnName("Record_Activity_ID");
            this.Property(t => t.Plan_Transaction_ID).HasColumnName("Plan_Transaction_ID");
            this.Property(t => t.Priority_Level).HasColumnName("Priority_Level");
        }
    }
}
