using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class vw_Plan_Transaction_DatesMap : EntityTypeConfiguration<vw_Plan_Transaction_Dates>
    {
        public vw_Plan_Transaction_DatesMap()
        {
            // Primary Key
            this.HasKey(t => t.Plan_Transaction_ID);

            // Properties
            this.Property(t => t.Plan_Transaction_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vw_Plan_Transaction_Dates");
            this.Property(t => t.Plan_Transaction_ID).HasColumnName("Plan_Transaction_ID");
            this.Property(t => t.PolicyPlacedDate).HasColumnName("PolicyPlacedDate");
            this.Property(t => t.ApplicationRecievedDate).HasColumnName("ApplicationRecievedDate");
            this.Property(t => t.CoverageEffectiveDate).HasColumnName("CoverageEffectiveDate");
            this.Property(t => t.PolicyDeliveredDate).HasColumnName("PolicyDeliveredDate");
            this.Property(t => t.ApplicationSubmittedDate).HasColumnName("ApplicationSubmittedDate");
        }
    }
}
