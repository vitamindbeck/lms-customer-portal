using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class vw_Screener_Dash_NC_TotalsMap : EntityTypeConfiguration<vw_Screener_Dash_NC_Totals>
    {
        public vw_Screener_Dash_NC_TotalsMap()
        {
            // Primary Key
            this.HasKey(t => t.Agent_ID);

            // Properties
            this.Property(t => t.Count_Date)
                .HasMaxLength(15);

            this.Property(t => t.Agent_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vw_Screener_Dash_NC_Totals");
            this.Property(t => t.Count_Date).HasColumnName("Count_Date");
            this.Property(t => t.IA_UL).HasColumnName("IA_UL");
            this.Property(t => t.Call_Status_Total).HasColumnName("Call_Status_Total");
            this.Property(t => t.Contacted_Total).HasColumnName("Contacted_Total");
            this.Property(t => t.Live_Transfer_Total).HasColumnName("Live_Transfer_Total");
            this.Property(t => t.Set_Appointment_Totals).HasColumnName("Set_Appointment_Totals");
            this.Property(t => t.Follow_Up_Screener_Total).HasColumnName("Follow_Up_Screener_Total");
            this.Property(t => t.Momentum).HasColumnName("Momentum");
            this.Property(t => t.Agent_ID).HasColumnName("Agent_ID");
        }
    }
}
