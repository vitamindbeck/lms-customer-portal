using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Plan_TransactionMap : EntityTypeConfiguration<Plan_Transaction>
    {
        public Plan_TransactionMap()
        {
            // Primary Key
            this.HasKey(t => t.Plan_Transaction_ID);

            // Properties
            this.Property(t => t.Tracking_Number_Inbound)
                .HasMaxLength(32);

            this.Property(t => t.Tracking_Number_Outbound)
                .HasMaxLength(32);

            this.Property(t => t.GBS_ID)
                .HasMaxLength(50);

            this.Property(t => t.Replacement_Details)
                .HasMaxLength(1000);

            this.Property(t => t.Best_Time_For_Exam)
                .HasMaxLength(100);

            this.Property(t => t.Exam_Details)
                .HasMaxLength(1000);

            this.Property(t => t.Special_Instructions)
                .HasMaxLength(2000);

            this.Property(t => t.Exam_Ref_Number)
                .HasMaxLength(50);

            this.Property(t => t.Policy_Number)
                .HasMaxLength(6400);

            this.Property(t => t.Sub_Plan)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Plan_Transaction");
            this.Property(t => t.Plan_Transaction_ID).HasColumnName("Plan_Transaction_ID");
            this.Property(t => t.Plan_ID).HasColumnName("Plan_ID");
            this.Property(t => t.Record_ID).HasColumnName("Record_ID");
            this.Property(t => t.Transaction_Number).HasColumnName("Transaction_Number");
            this.Property(t => t.Created_On).HasColumnName("Created_On");
            this.Property(t => t.Active).HasColumnName("Active");
            this.Property(t => t.Agent_ID).HasColumnName("Agent_ID");
            this.Property(t => t.Assistant_Agent_ID).HasColumnName("Assistant_Agent_ID");
            this.Property(t => t.Source_Code_ID).HasColumnName("Source_Code_ID");
            this.Property(t => t.Cash_With_Application).HasColumnName("Cash_With_Application");
            this.Property(t => t.Cash_With_Application_Check).HasColumnName("Cash_With_Application_Check");
            this.Property(t => t.Cash_With_Application_Amount).HasColumnName("Cash_With_Application_Amount");
            this.Property(t => t.Outbound_Shipping_Method_ID).HasColumnName("Outbound_Shipping_Method_ID");
            this.Property(t => t.Tracking_Number_Inbound).HasColumnName("Tracking_Number_Inbound");
            this.Property(t => t.Tracking_Number_Outbound).HasColumnName("Tracking_Number_Outbound");
            this.Property(t => t.Payment_Mode_ID).HasColumnName("Payment_Mode_ID");
            this.Property(t => t.Annual_Premium).HasColumnName("Annual_Premium");
            this.Property(t => t.Modal_Premium).HasColumnName("Modal_Premium");
            this.Property(t => t.Exam_Service_ID).HasColumnName("Exam_Service_ID");
            this.Property(t => t.Comments).HasColumnName("Comments");
            this.Property(t => t.Deposit_Amount).HasColumnName("Deposit_Amount");
            this.Property(t => t.Coverage_Amount).HasColumnName("Coverage_Amount");
            this.Property(t => t.Plan_Transaction_Status_ID).HasColumnName("Plan_Transaction_Status_ID");
            this.Property(t => t.Parent_Plan_ID).HasColumnName("Parent_Plan_ID");
            this.Property(t => t.Group_ID).HasColumnName("Group_ID");
            this.Property(t => t.GBS_ID).HasColumnName("GBS_ID");
            this.Property(t => t.Mode_ID).HasColumnName("Mode_ID");
            this.Property(t => t.Inbound_Shipping_Method_ID).HasColumnName("Inbound_Shipping_Method_ID");
            this.Property(t => t.eApp).HasColumnName("eApp");
            this.Property(t => t.Underwriting_Class_ID).HasColumnName("Underwriting_Class_ID");
            this.Property(t => t.Modal_ID).HasColumnName("Modal_ID");
            this.Property(t => t.Replacement).HasColumnName("Replacement");
            this.Property(t => t.Replacement_Details).HasColumnName("Replacement_Details");
            this.Property(t => t.Best_Time_For_Exam).HasColumnName("Best_Time_For_Exam");
            this.Property(t => t.Exam_Details).HasColumnName("Exam_Details");
            this.Property(t => t.Special_Instructions).HasColumnName("Special_Instructions");
            this.Property(t => t.Qualified_Transfer).HasColumnName("Qualified_Transfer");
            this.Property(t => t.Non_Qualified_1035_Exchange).HasColumnName("Non_Qualified_1035_Exchange");
            this.Property(t => t.Exam_Ref_Number).HasColumnName("Exam_Ref_Number");
            this.Property(t => t.Initial_Enrollment).HasColumnName("Initial_Enrollment");
            this.Property(t => t.Guaranteed_Issue).HasColumnName("Guaranteed_Issue");
            this.Property(t => t.Policy_Number).HasColumnName("Policy_Number");
            this.Property(t => t.Sub_Plan).HasColumnName("Sub_Plan");
            this.Property(t => t.Buying_Period_ID).HasColumnName("Buying_Period_ID");
            this.Property(t => t.Buying_Period_Sub_ID).HasColumnName("Buying_Period_Sub_ID");
            this.Property(t => t.Buying_Period_Effective_Date).HasColumnName("Buying_Period_Effective_Date");
            this.Property(t => t.Time_Stamp).HasColumnName("Time_Stamp");
            this.Property(t => t.PreFillApp).HasColumnName("PreFillApp");
            this.Property(t => t.Case_Manager_ID).HasColumnName("Case_Manager_ID");
            this.Property(t => t.Status_Date).HasColumnName("Status_Date");

            // Relationships
            this.HasRequired(t => t.Plan)
                .WithMany(t => t.Plan_Transaction)
                .HasForeignKey(d => d.Plan_ID);

        }
    }
}
