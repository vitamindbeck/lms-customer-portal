using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class vw_Screener_Initial_CountsMap : EntityTypeConfiguration<vw_Screener_Initial_Counts>
    {
        public vw_Screener_Initial_CountsMap()
        {
            // Primary Key
            this.HasKey(t => t.Agent_ID);

            // Properties
            this.Property(t => t.Count_Date)
                .HasMaxLength(15);

            this.Property(t => t.Status)
                .HasMaxLength(128);

            this.Property(t => t.Agent_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vw_Screener_Initial_Counts");
            this.Property(t => t.Count_Date).HasColumnName("Count_Date");
            this.Property(t => t.timeperiod).HasColumnName("timeperiod");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.Responses).HasColumnName("Responses");
            this.Property(t => t.InitialAttempts).HasColumnName("InitialAttempts");
            this.Property(t => t.NoContacts).HasColumnName("NoContacts");
            this.Property(t => t.MadeContact).HasColumnName("MadeContact");
            this.Property(t => t.LiveTransfers).HasColumnName("LiveTransfers");
            this.Property(t => t.Live_Transfer_Attempt).HasColumnName("Live_Transfer_Attempt");
            this.Property(t => t.SetFollowUpScreener).HasColumnName("SetFollowUpScreener");
            this.Property(t => t.Agent_ID).HasColumnName("Agent_ID");
        }
    }
}
