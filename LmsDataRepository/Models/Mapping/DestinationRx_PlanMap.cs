using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class DestinationRx_PlanMap : EntityTypeConfiguration<DestinationRx_Plan>
    {
        public DestinationRx_PlanMap()
        {
            // Primary Key
            this.HasKey(t => t.DestinationRx_Plan_Code);

            // Properties
            this.Property(t => t.DestinationRx_Plan_Code)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.GBS_Plan_ID)
                .HasMaxLength(50);

            this.Property(t => t.Destination_Plan_Name)
                .HasMaxLength(100);

            this.Property(t => t.GBS_Product_Name)
                .HasMaxLength(100);

            this.Property(t => t.Client_Name)
                .HasMaxLength(100);

            this.Property(t => t.Plan_Type)
                .HasMaxLength(35);

            this.Property(t => t.State)
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("DestinationRx_Plan");
            this.Property(t => t.DestinationRx_Plan_Code).HasColumnName("DestinationRx_Plan_Code");
            this.Property(t => t.GBS_Plan_ID).HasColumnName("GBS_Plan_ID");
            this.Property(t => t.Destination_Plan_Name).HasColumnName("Destination_Plan_Name");
            this.Property(t => t.GBS_Product_Name).HasColumnName("GBS_Product_Name");
            this.Property(t => t.Client_Name).HasColumnName("Client_Name");
            this.Property(t => t.Plan_Type).HasColumnName("Plan_Type");
            this.Property(t => t.State).HasColumnName("State");
        }
    }
}
