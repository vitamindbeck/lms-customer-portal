using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Email_CodeMap : EntityTypeConfiguration<Email_Code>
    {
        public Email_CodeMap()
        {
            // Primary Key
            this.HasKey(t => t.Email_Code_ID);

            // Properties
            this.Property(t => t.Code)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.Subject)
                .HasMaxLength(255);

            this.Property(t => t.Name)
                .HasMaxLength(255);

            this.Property(t => t.Email_Address)
                .HasMaxLength(255);

            this.Property(t => t.Email_Date_Type)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Email_Code");
            this.Property(t => t.Email_Code_ID).HasColumnName("Email_Code_ID");
            this.Property(t => t.Code).HasColumnName("Code");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Subject).HasColumnName("Subject");
            this.Property(t => t.Message).HasColumnName("Message");
            this.Property(t => t.Include_Disclaimer).HasColumnName("Include_Disclaimer");
            this.Property(t => t.Agent_Specific).HasColumnName("Agent_Specific");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Email_Address).HasColumnName("Email_Address");
            this.Property(t => t.HTML).HasColumnName("HTML");
            this.Property(t => t.Email_Date_Type).HasColumnName("Email_Date_Type");
        }
    }
}
