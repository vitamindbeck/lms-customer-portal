using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class DailyPhoneStatisticsDetailMap : EntityTypeConfiguration<DailyPhoneStatisticsDetail>
    {
        public DailyPhoneStatisticsDetailMap()
        {
            // Primary Key
            this.HasKey(t => t.DailyPhoneStatisticsDetailsID);

            // Properties
            this.Property(t => t.AgentName)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("DailyPhoneStatisticsDetails");
            this.Property(t => t.DailyPhoneStatisticsDetailsID).HasColumnName("DailyPhoneStatisticsDetailsID");
            this.Property(t => t.DailyPhoneStatisticsID).HasColumnName("DailyPhoneStatisticsID");
            this.Property(t => t.AgentID).HasColumnName("AgentID");
            this.Property(t => t.AgentName).HasColumnName("AgentName");
            this.Property(t => t.CallsIn).HasColumnName("CallsIn");
            this.Property(t => t.CallsInCompleted).HasColumnName("CallsInCompleted");
            this.Property(t => t.CallsOut).HasColumnName("CallsOut");
            this.Property(t => t.CallsOutCompleted).HasColumnName("CallsOutCompleted");
            this.Property(t => t.CallsHandled).HasColumnName("CallsHandled");
            this.Property(t => t.CallsAbandoned).HasColumnName("CallsAbandoned");
            this.Property(t => t.TotalTalkTime).HasColumnName("TotalTalkTime");
            this.Property(t => t.InTalkTime).HasColumnName("InTalkTime");
            this.Property(t => t.OutTalkTime).HasColumnName("OutTalkTime");
            this.Property(t => t.AvgTalkTime).HasColumnName("AvgTalkTime");
            this.Property(t => t.TimeOnDuty).HasColumnName("TimeOnDuty");
            this.Property(t => t.PercentFree).HasColumnName("PercentFree");
            this.Property(t => t.PercectBusy).HasColumnName("PercectBusy");
            this.Property(t => t.PrecentDND).HasColumnName("PrecentDND");
            this.Property(t => t.DNDTime).HasColumnName("DNDTime");
            this.Property(t => t.AvgCallTime).HasColumnName("AvgCallTime");

            // Relationships
            this.HasRequired(t => t.DailyPhoneStatistic)
                .WithMany(t => t.DailyPhoneStatisticsDetails)
                .HasForeignKey(d => d.DailyPhoneStatisticsID);

        }
    }
}
