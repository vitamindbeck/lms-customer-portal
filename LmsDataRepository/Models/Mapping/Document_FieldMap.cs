using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Document_FieldMap : EntityTypeConfiguration<Document_Field>
    {
        public Document_FieldMap()
        {
            // Primary Key
            this.HasKey(t => t.Document_Field_ID);

            // Properties
            this.Property(t => t.Form_Name)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.DB_Name)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("Document_Field");
            this.Property(t => t.Document_Field_ID).HasColumnName("Document_Field_ID");
            this.Property(t => t.Document_ID).HasColumnName("Document_ID");
            this.Property(t => t.Display_Order).HasColumnName("Display_Order");
            this.Property(t => t.Form_Name).HasColumnName("Form_Name");
            this.Property(t => t.DB_Name).HasColumnName("DB_Name");
        }
    }
}
