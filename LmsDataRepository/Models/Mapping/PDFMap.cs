using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class PDFMap : EntityTypeConfiguration<PDF>
    {
        public PDFMap()
        {
            // Primary Key
            this.HasKey(t => t.PDFID);

            // Properties
            this.Property(t => t.FileName)
                .IsRequired()
                .HasMaxLength(500);

            this.Property(t => t.FilePath)
                .IsRequired()
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("PDF");
            this.Property(t => t.PDFID).HasColumnName("PDFID");
            this.Property(t => t.FileName).HasColumnName("FileName");
            this.Property(t => t.FilePath).HasColumnName("FilePath");
            this.Property(t => t.PlanID).HasColumnName("PlanID");
            this.Property(t => t.StateID).HasColumnName("StateID");
        }
    }
}
