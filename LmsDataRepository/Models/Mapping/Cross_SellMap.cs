using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Cross_SellMap : EntityTypeConfiguration<Cross_Sell>
    {
        public Cross_SellMap()
        {
            // Primary Key
            this.HasKey(t => t.Cross_Sell_ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("Cross_Sell");
            this.Property(t => t.Cross_Sell_ID).HasColumnName("Cross_Sell_ID");
            this.Property(t => t.Client_ID).HasColumnName("Client_ID");
            this.Property(t => t.Group_ID).HasColumnName("Group_ID");
            this.Property(t => t.Rank).HasColumnName("Rank");
        }
    }
}
