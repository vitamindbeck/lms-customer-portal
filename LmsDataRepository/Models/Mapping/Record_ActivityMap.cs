using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Record_ActivityMap : EntityTypeConfiguration<Record_Activity>
    {
        public Record_ActivityMap()
        {
            // Primary Key
            this.HasKey(t => t.Record_Activity_ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("Record_Activity");
            this.Property(t => t.Record_Activity_ID).HasColumnName("Record_Activity_ID");
            this.Property(t => t.Record_ID).HasColumnName("Record_ID");
            this.Property(t => t.Activity_Type_ID).HasColumnName("Activity_Type_ID");
            this.Property(t => t.Due_Date).HasColumnName("Due_Date");
            this.Property(t => t.Actual_Date).HasColumnName("Actual_Date");
            this.Property(t => t.Created_On).HasColumnName("Created_On");
            this.Property(t => t.Created_By).HasColumnName("Created_By");
            this.Property(t => t.User_ID).HasColumnName("User_ID");
            this.Property(t => t.Notes).HasColumnName("Notes");
            this.Property(t => t.Made_Contact).HasColumnName("Made_Contact");
            this.Property(t => t.Record_Activity_Status_ID).HasColumnName("Record_Activity_Status_ID");
            this.Property(t => t.Plan_Transaction_ID).HasColumnName("Plan_Transaction_ID");
            this.Property(t => t.Priority_Level).HasColumnName("Priority_Level");
            this.Property(t => t.Auto_Email_ID).HasColumnName("Auto_Email_ID");
            this.Property(t => t.Record_Activity_Note_ID).HasColumnName("Record_Activity_Note_ID");
            this.Property(t => t.GBS_Notes_ID).HasColumnName("GBS_Notes_ID");
            this.Property(t => t.CTCID).HasColumnName("CTCID");
            this.Property(t => t.SEEID).HasColumnName("SEEID");
            this.Property(t => t.AdminDocumentationID).HasColumnName("AdminDocumentationID");
            this.Property(t => t.Campaign_Code_ID).HasColumnName("Campaign_Code_ID");

            // Relationships
            this.HasOptional(t => t.Auto_Email)
                .WithMany(t => t.Record_Activity)
                .HasForeignKey(d => d.Auto_Email_ID);
            this.HasRequired(t => t.Campaign_Code)
                .WithMany(t => t.Record_Activity)
                .HasForeignKey(d => d.Campaign_Code_ID);
            this.HasRequired(t => t.Plan_Transaction)
                .WithMany(t => t.Record_Activity)
                .HasForeignKey(d => d.Plan_Transaction_ID);
            this.HasRequired(t => t.Record)
                .WithMany(t => t.Record_Activity)
                .HasForeignKey(d => d.Record_ID);
            this.HasOptional(t => t.Record_Activity_Note)
                .WithMany(t => t.Record_Activity)
                .HasForeignKey(d => d.Record_Activity_Note_ID);
            this.HasRequired(t => t.Record_Activity_Status)
                .WithMany(t => t.Record_Activity)
                .HasForeignKey(d => d.Record_Activity_Status_ID);
            this.HasRequired(t => t.User)
                .WithMany(t => t.Record_Activity)
                .HasForeignKey(d => d.User_ID);

        }
    }
}
