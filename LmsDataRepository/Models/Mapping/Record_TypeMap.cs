using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Record_TypeMap : EntityTypeConfiguration<Record_Type>
    {
        public Record_TypeMap()
        {
            // Primary Key
            this.HasKey(t => t.Record_Type_ID);

            // Properties
            this.Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("Record_Type");
            this.Property(t => t.Record_Type_ID).HasColumnName("Record_Type_ID");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Display_Order).HasColumnName("Display_Order");
            this.Property(t => t.GBS_ID).HasColumnName("GBS_ID");
        }
    }
}
