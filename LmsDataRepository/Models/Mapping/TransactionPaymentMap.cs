using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class TransactionPaymentMap : EntityTypeConfiguration<TransactionPayment>
    {
        public TransactionPaymentMap()
        {
            // Primary Key
            this.HasKey(t => t.TransactionPaymentID);

            // Properties
            this.Property(t => t.BillingPeriod)
                .HasMaxLength(50);

            this.Property(t => t.Comment)
                .HasMaxLength(70);

            this.Property(t => t.Comment2)
                .HasMaxLength(70);

            // Table & Column Mappings
            this.ToTable("TransactionPayments");
            this.Property(t => t.AgentID).HasColumnName("AgentID");
            this.Property(t => t.BillingPeriod).HasColumnName("BillingPeriod");
            this.Property(t => t.BillingPeriodDate).HasColumnName("BillingPeriodDate");
            this.Property(t => t.Comment).HasColumnName("Comment");
            this.Property(t => t.CommissionType).HasColumnName("CommissionType");
            this.Property(t => t.DateTimeLastModified).HasColumnName("DateTimeLastModified");
            this.Property(t => t.IsUserChanged).HasColumnName("IsUserChanged");
            this.Property(t => t.NumberOfLives).HasColumnName("NumberOfLives");
            this.Property(t => t.PaymentAmount).HasColumnName("PaymentAmount");
            this.Property(t => t.PaymentType).HasColumnName("PaymentType");
            this.Property(t => t.PolicyID).HasColumnName("PolicyID");
            this.Property(t => t.PremiumAmount).HasColumnName("PremiumAmount");
            this.Property(t => t.SplitID).HasColumnName("SplitID");
            this.Property(t => t.TransactionID).HasColumnName("TransactionID");
            this.Property(t => t.TransactionPaymentID).HasColumnName("TransactionPaymentID");
            this.Property(t => t.UserID).HasColumnName("UserID");
            this.Property(t => t.Comment2).HasColumnName("Comment2");
        }
    }
}
