using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Carrier_RatingMap : EntityTypeConfiguration<Carrier_Rating>
    {
        public Carrier_RatingMap()
        {
            // Primary Key
            this.HasKey(t => t.Carrier_Rating_ID);

            // Properties
            this.Property(t => t.Rating)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Description)
                .HasMaxLength(2000);

            // Table & Column Mappings
            this.ToTable("Carrier_Rating");
            this.Property(t => t.Carrier_Rating_ID).HasColumnName("Carrier_Rating_ID");
            this.Property(t => t.Carrier_ID).HasColumnName("Carrier_ID");
            this.Property(t => t.Rating_Company_ID).HasColumnName("Rating_Company_ID");
            this.Property(t => t.Rating).HasColumnName("Rating");
            this.Property(t => t.Rating_Date).HasColumnName("Rating_Date");
            this.Property(t => t.Description).HasColumnName("Description");
        }
    }
}
