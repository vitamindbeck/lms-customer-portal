using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Shipping_MethodMap : EntityTypeConfiguration<Shipping_Method>
    {
        public Shipping_MethodMap()
        {
            // Primary Key
            this.HasKey(t => t.Shipping_Method_ID);

            // Properties
            this.Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("Shipping_Method");
            this.Property(t => t.Shipping_Method_ID).HasColumnName("Shipping_Method_ID");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Display_Order).HasColumnName("Display_Order");
            this.Property(t => t.Active).HasColumnName("Active");
        }
    }
}
