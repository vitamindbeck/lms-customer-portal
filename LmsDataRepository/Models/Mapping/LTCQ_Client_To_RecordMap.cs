using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class LTCQ_Client_To_RecordMap : EntityTypeConfiguration<LTCQ_Client_To_Record>
    {
        public LTCQ_Client_To_RecordMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Record_ID, t.Client_Number });

            // Properties
            this.Property(t => t.Record_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Client_Number)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("LTCQ_Client_To_Record");
            this.Property(t => t.Record_ID).HasColumnName("Record_ID");
            this.Property(t => t.Client_Number).HasColumnName("Client_Number");
        }
    }
}
