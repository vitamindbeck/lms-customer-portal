using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Carrier_Phone_NumberMap : EntityTypeConfiguration<Carrier_Phone_Number>
    {
        public Carrier_Phone_NumberMap()
        {
            // Primary Key
            this.HasKey(t => t.Carrier_Phone_Number_ID);

            // Properties
            this.Property(t => t.Phone_Number)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Carrier_Phone_Number");
            this.Property(t => t.Carrier_Phone_Number_ID).HasColumnName("Carrier_Phone_Number_ID");
            this.Property(t => t.Carrier_ID).HasColumnName("Carrier_ID");
            this.Property(t => t.Phone_Type_ID).HasColumnName("Phone_Type_ID");
            this.Property(t => t.Phone_Number).HasColumnName("Phone_Number");
            this.Property(t => t.Display_Order).HasColumnName("Display_Order");
        }
    }
}
