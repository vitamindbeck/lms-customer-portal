using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Non_CarrierMap : EntityTypeConfiguration<Non_Carrier>
    {
        public Non_CarrierMap()
        {
            // Primary Key
            this.HasKey(t => t.Non_Carrier_ID);

            // Properties
            this.Property(t => t.Non_Carrier_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Non_Carrier_Name)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Web_Link)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Non_Carrier");
            this.Property(t => t.Non_Carrier_ID).HasColumnName("Non_Carrier_ID");
            this.Property(t => t.Non_Carrier_Name).HasColumnName("Non_Carrier_Name");
            this.Property(t => t.Web_Link).HasColumnName("Web_Link");
        }
    }
}
