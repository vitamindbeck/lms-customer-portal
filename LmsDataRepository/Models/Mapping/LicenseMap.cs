using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class LicenseMap : EntityTypeConfiguration<License>
    {
        public LicenseMap()
        {
            // Primary Key
            this.HasKey(t => t.License_ID);

            // Properties
            this.Property(t => t.License_Number)
                .HasMaxLength(15);

            this.Property(t => t.States)
                .IsRequired()
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("License");
            this.Property(t => t.License_ID).HasColumnName("License_ID");
            this.Property(t => t.Agent_ID).HasColumnName("Agent_ID");
            this.Property(t => t.Effective_Date).HasColumnName("Effective_Date");
            this.Property(t => t.Line_Of_Insurance_ID).HasColumnName("Line_Of_Insurance_ID");
            this.Property(t => t.License_Number).HasColumnName("License_Number");
            this.Property(t => t.Renewal_Date).HasColumnName("Renewal_Date");
            this.Property(t => t.States).HasColumnName("States");
            this.Property(t => t.GBS_License_ID).HasColumnName("GBS_License_ID");
            this.Property(t => t.Last_Updated_Date).HasColumnName("Last_Updated_Date");
            this.Property(t => t.Updated_By).HasColumnName("Updated_By");
            this.Property(t => t.Active).HasColumnName("Active");
        }
    }
}
