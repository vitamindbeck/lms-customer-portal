using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Media_TypeMap : EntityTypeConfiguration<Media_Type>
    {
        public Media_TypeMap()
        {
            // Primary Key
            this.HasKey(t => t.Media_Type_ID);

            // Properties
            this.Property(t => t.Media_Type_Description)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("Media_Type");
            this.Property(t => t.Media_Type_ID).HasColumnName("Media_Type_ID");
            this.Property(t => t.Media_Type_Description).HasColumnName("Media_Type_Description");
        }
    }
}
