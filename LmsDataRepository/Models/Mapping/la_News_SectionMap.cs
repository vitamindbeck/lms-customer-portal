using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class la_News_SectionMap : EntityTypeConfiguration<la_News_Section>
    {
        public la_News_SectionMap()
        {
            // Primary Key
            this.HasKey(t => t.News_Section_ID);

            // Properties
            this.Property(t => t.Title)
                .HasMaxLength(200);

            this.Property(t => t.Title_URL)
                .HasMaxLength(500);

            this.Property(t => t.Description)
                .HasMaxLength(2000);

            // Table & Column Mappings
            this.ToTable("la_News_Section");
            this.Property(t => t.News_Section_ID).HasColumnName("News_Section_ID");
            this.Property(t => t.Title).HasColumnName("Title");
            this.Property(t => t.Title_URL).HasColumnName("Title_URL");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Display_Order).HasColumnName("Display_Order");
            this.Property(t => t.Active).HasColumnName("Active");
        }
    }
}
