using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Record_EligibilityMap : EntityTypeConfiguration<Record_Eligibility>
    {
        public Record_EligibilityMap()
        {
            // Primary Key
            this.HasKey(t => t.Record_Eligibility_ID);

            // Properties
            this.Property(t => t.Record_Eligibility_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Category)
                .IsRequired()
                .HasMaxLength(15);

            // Table & Column Mappings
            this.ToTable("Record_Eligibility");
            this.Property(t => t.Record_Eligibility_ID).HasColumnName("Record_Eligibility_ID");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Category).HasColumnName("Category");
            this.Property(t => t.Active).HasColumnName("Active");
            this.Property(t => t.Order).HasColumnName("Order");
        }
    }
}
