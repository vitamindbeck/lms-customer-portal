using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class ReconciliationMap : EntityTypeConfiguration<Reconciliation>
    {
        public ReconciliationMap()
        {
            // Primary Key
            this.HasKey(t => t.Reconciliation_ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("Reconciliation");
            this.Property(t => t.Reconciliation_ID).HasColumnName("Reconciliation_ID");
            this.Property(t => t.Record_ID).HasColumnName("Record_ID");
            this.Property(t => t.Reconciliation_Type_ID).HasColumnName("Reconciliation_Type_ID");
            this.Property(t => t.Placed_Date).HasColumnName("Placed_Date");
            this.Property(t => t.Placed_By).HasColumnName("Placed_By");
            this.Property(t => t.Resolved_Date).HasColumnName("Resolved_Date");
            this.Property(t => t.Resolved_By).HasColumnName("Resolved_By");
            this.Property(t => t.Notes).HasColumnName("Notes");
            this.Property(t => t.Record_Activity_ID).HasColumnName("Record_Activity_ID");

            // Relationships
            this.HasRequired(t => t.Reconciliation_Type)
                .WithMany(t => t.Reconciliations)
                .HasForeignKey(d => d.Reconciliation_Type_ID);
            this.HasOptional(t => t.Record_Activity)
                .WithMany(t => t.Reconciliations)
                .HasForeignKey(d => d.Record_Activity_ID);

        }
    }
}
