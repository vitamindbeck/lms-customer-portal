using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class ProcessMap : EntityTypeConfiguration<Process>
    {
        public ProcessMap()
        {
            // Primary Key
            this.HasKey(t => t.Process_ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("Process");
            this.Property(t => t.Process_ID).HasColumnName("Process_ID");
            this.Property(t => t.Process_Type_ID).HasColumnName("Process_Type_ID");
            this.Property(t => t.Value).HasColumnName("Value");
            this.Property(t => t.OtherValue).HasColumnName("OtherValue");
            this.Property(t => t.Process_Date).HasColumnName("Process_Date");
        }
    }
}
