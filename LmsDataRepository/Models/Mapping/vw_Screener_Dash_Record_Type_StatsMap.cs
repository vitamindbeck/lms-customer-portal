using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class vw_Screener_Dash_Record_Type_StatsMap : EntityTypeConfiguration<vw_Screener_Dash_Record_Type_Stats>
    {
        public vw_Screener_Dash_Record_Type_StatsMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Grp_ID, t.Agent_ID });

            // Properties
            this.Property(t => t.Grp_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Count_Date)
                .HasMaxLength(15);

            this.Property(t => t.Agent_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vw_Screener_Dash_Record_Type_Stats");
            this.Property(t => t.Grp_ID).HasColumnName("Grp_ID");
            this.Property(t => t.Count_Date).HasColumnName("Count_Date");
            this.Property(t => t.Disqualified).HasColumnName("Disqualified");
            this.Property(t => t.Duplicates).HasColumnName("Duplicates");
            this.Property(t => t.Prospects).HasColumnName("Prospects");
            this.Property(t => t.Suspects).HasColumnName("Suspects");
            this.Property(t => t.Leads).HasColumnName("Leads");
            this.Property(t => t.EN).HasColumnName("EN");
            this.Property(t => t.E30).HasColumnName("E30");
            this.Property(t => t.E60).HasColumnName("E60");
            this.Property(t => t.E90).HasColumnName("E90");
            this.Property(t => t.E901).HasColumnName("E901");
            this.Property(t => t.EU).HasColumnName("EU");
            this.Property(t => t.NE).HasColumnName("NE");
            this.Property(t => t.Uncontacted).HasColumnName("Uncontacted");
            this.Property(t => t.Unattempted).HasColumnName("Unattempted");
            this.Property(t => t.Agent_ID).HasColumnName("Agent_ID");
        }
    }
}
