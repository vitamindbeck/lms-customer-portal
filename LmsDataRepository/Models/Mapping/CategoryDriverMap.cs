using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class CategoryDriverMap : EntityTypeConfiguration<CategoryDriver>
    {
        public CategoryDriverMap()
        {
            // Primary Key
            this.HasKey(t => t.CategoryDriverID);

            // Properties
            // Table & Column Mappings
            this.ToTable("CategoryDriver");
            this.Property(t => t.CategoryDriverID).HasColumnName("CategoryDriverID");
            this.Property(t => t.CategoryDriver1).HasColumnName("CategoryDriver");
            this.Property(t => t.CategoryDriverDesc).HasColumnName("CategoryDriverDesc");
            this.Property(t => t.CategoryDriverDetails).HasColumnName("CategoryDriverDetails");
            this.Property(t => t.CategoryPriorityLevel).HasColumnName("CategoryPriorityLevel");
            this.Property(t => t.CategoryDisplayOrder).HasColumnName("CategoryDisplayOrder");
            this.Property(t => t.CategoryActive).HasColumnName("CategoryActive");
        }
    }
}
