using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Record_Distribution_HistoryMap : EntityTypeConfiguration<Record_Distribution_History>
    {
        public Record_Distribution_HistoryMap()
        {
            // Primary Key
            this.HasKey(t => t.Record_Distribution_History_ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("Record_Distribution_History");
            this.Property(t => t.Record_Distribution_History_ID).HasColumnName("Record_Distribution_History_ID");
            this.Property(t => t.Record_ID).HasColumnName("Record_ID");
            this.Property(t => t.Record_Distribution_Type_ID).HasColumnName("Record_Distribution_Type_ID");
            this.Property(t => t.Added_On).HasColumnName("Added_On");
            this.Property(t => t.Removed_On).HasColumnName("Removed_On");
        }
    }
}
