using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Payment_ModeMap : EntityTypeConfiguration<Payment_Mode>
    {
        public Payment_ModeMap()
        {
            // Primary Key
            this.HasKey(t => t.Payment_Mode_ID);

            // Properties
            this.Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(64);

            // Table & Column Mappings
            this.ToTable("Payment_Mode");
            this.Property(t => t.Payment_Mode_ID).HasColumnName("Payment_Mode_ID");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Display_Order).HasColumnName("Display_Order");
            this.Property(t => t.Active).HasColumnName("Active");
        }
    }
}
