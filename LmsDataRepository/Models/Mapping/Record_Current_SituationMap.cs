using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Record_Current_SituationMap : EntityTypeConfiguration<Record_Current_Situation>
    {
        public Record_Current_SituationMap()
        {
            // Primary Key
            this.HasKey(t => t.Record_Current_Situation_ID);

            // Properties
            this.Property(t => t.Record_Current_Situation_Desc)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("Record_Current_Situation");
            this.Property(t => t.Record_Current_Situation_ID).HasColumnName("Record_Current_Situation_ID");
            this.Property(t => t.Record_Current_Situation_Desc).HasColumnName("Record_Current_Situation_Desc");
            this.Property(t => t.Active).HasColumnName("Active");
            this.Property(t => t.Order).HasColumnName("Order");
        }
    }
}
