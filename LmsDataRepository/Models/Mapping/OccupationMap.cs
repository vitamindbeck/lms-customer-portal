using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class OccupationMap : EntityTypeConfiguration<Occupation>
    {
        public OccupationMap()
        {
            // Primary Key
            this.HasKey(t => t.Occupation_ID);

            // Properties
            this.Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("Occupation");
            this.Property(t => t.Occupation_ID).HasColumnName("Occupation_ID");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Display_Order).HasColumnName("Display_Order");
            this.Property(t => t.Active).HasColumnName("Active");
        }
    }
}
