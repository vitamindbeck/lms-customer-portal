using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class SalesRatingMap : EntityTypeConfiguration<SalesRating>
    {
        public SalesRatingMap()
        {
            // Primary Key
            this.HasKey(t => new { t.SEEID, t.Description, t.Active });

            // Properties
            this.Property(t => t.SEEID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.Description)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("SalesRating");
            this.Property(t => t.SEEID).HasColumnName("SEEID");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Active).HasColumnName("Active");
        }
    }
}
