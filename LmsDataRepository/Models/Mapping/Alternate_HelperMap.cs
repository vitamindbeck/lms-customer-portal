using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Alternate_HelperMap : EntityTypeConfiguration<Alternate_Helper>
    {
        public Alternate_HelperMap()
        {
            // Primary Key
            this.HasKey(t => t.Alternate_Helper_ID);

            // Properties
            this.Property(t => t.Alternate_Helper_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Alternate_Helper");
            this.Property(t => t.Alternate_Helper_ID).HasColumnName("Alternate_Helper_ID");
            this.Property(t => t.Alternate_Indicator).HasColumnName("Alternate_Indicator");
            this.Property(t => t.Description).HasColumnName("Description");
        }
    }
}
