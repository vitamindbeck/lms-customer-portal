using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Plan_oldMap : EntityTypeConfiguration<Plan_old>
    {
        public Plan_oldMap()
        {
            // Primary Key
            this.HasKey(t => t.Plan_ID);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.CompuProd)
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("Plan_old");
            this.Property(t => t.Plan_ID).HasColumnName("Plan_ID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Carrier_ID).HasColumnName("Carrier_ID");
            this.Property(t => t.Active).HasColumnName("Active");
            this.Property(t => t.About).HasColumnName("About");
            this.Property(t => t.CompuProd).HasColumnName("CompuProd");
            this.Property(t => t.Modal_Factor_Semiannual).HasColumnName("Modal_Factor_Semiannual");
            this.Property(t => t.Modal_Factor_Quarterly).HasColumnName("Modal_Factor_Quarterly");
            this.Property(t => t.Modal_Factor_Monthly).HasColumnName("Modal_Factor_Monthly");
            this.Property(t => t.Modal_Factor_EFT).HasColumnName("Modal_Factor_EFT");
            this.Property(t => t.Nearest_Age).HasColumnName("Nearest_Age");
            this.Property(t => t.GBS_ProductID).HasColumnName("GBS_ProductID");
        }
    }
}
