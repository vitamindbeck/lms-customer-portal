using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Record_AssignedMap : EntityTypeConfiguration<Record_Assigned>
    {
        public Record_AssignedMap()
        {
            // Primary Key
            this.HasKey(t => t.Record_Assigned_ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("Record_Assigned");
            this.Property(t => t.Record_Assigned_ID).HasColumnName("Record_Assigned_ID");
            this.Property(t => t.Record_ID).HasColumnName("Record_ID");
            this.Property(t => t.User_ID).HasColumnName("User_ID");
            this.Property(t => t.Date_Value).HasColumnName("Date_Value");
        }
    }
}
