using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class TransactionHeaderMap : EntityTypeConfiguration<TransactionHeader>
    {
        public TransactionHeaderMap()
        {
            // Primary Key
            this.HasKey(t => t.TransactionID);

            // Properties
            this.Property(t => t.CheckNumber)
                .HasMaxLength(12);

            this.Property(t => t.ImportFlag)
                .HasMaxLength(20);

            this.Property(t => t.KeyField)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("TransactionHeaders");
            this.Property(t => t.AdministratorID).HasColumnName("AdministratorID");
            this.Property(t => t.AmountEntered).HasColumnName("AmountEntered");
            this.Property(t => t.CheckAdjustment).HasColumnName("CheckAdjustment");
            this.Property(t => t.CheckAmount).HasColumnName("CheckAmount");
            this.Property(t => t.CheckNumber).HasColumnName("CheckNumber");
            this.Property(t => t.DateCheckReceived).HasColumnName("DateCheckReceived");
            this.Property(t => t.DateTimeLastModified).HasColumnName("DateTimeLastModified");
            this.Property(t => t.GAID).HasColumnName("GAID");
            this.Property(t => t.ImportFlag).HasColumnName("ImportFlag");
            this.Property(t => t.IsBalanced).HasColumnName("IsBalanced");
            this.Property(t => t.IsPosted).HasColumnName("IsPosted");
            this.Property(t => t.KeyField).HasColumnName("KeyField");
            this.Property(t => t.NumberOfItems).HasColumnName("NumberOfItems");
            this.Property(t => t.TransactionID).HasColumnName("TransactionID");
            this.Property(t => t.UserID).HasColumnName("UserID");
        }
    }
}
