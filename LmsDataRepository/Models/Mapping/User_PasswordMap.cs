using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class User_PasswordMap : EntityTypeConfiguration<User_Password>
    {
        public User_PasswordMap()
        {
            // Primary Key
            this.HasKey(t => t.User_Password_ID);

            // Properties
            this.Property(t => t.User_Name)
                .HasMaxLength(500);

            this.Property(t => t.Password)
                .HasMaxLength(500);

            this.Property(t => t.Carrier_Site_Link)
                .HasMaxLength(1000);

            // Table & Column Mappings
            this.ToTable("User_Password");
            this.Property(t => t.User_Password_ID).HasColumnName("User_Password_ID");
            this.Property(t => t.Appointment_ID).HasColumnName("Appointment_ID");
            this.Property(t => t.License_ID).HasColumnName("License_ID");
            this.Property(t => t.Certification_ID).HasColumnName("Certification_ID");
            this.Property(t => t.Agent_ID).HasColumnName("Agent_ID");
            this.Property(t => t.State_ID).HasColumnName("State_ID");
            this.Property(t => t.Non_Carrier_ID).HasColumnName("Non_Carrier_ID");
            this.Property(t => t.User_Name).HasColumnName("User_Name");
            this.Property(t => t.Password).HasColumnName("Password");
            this.Property(t => t.Carrier_ID).HasColumnName("Carrier_ID");
            this.Property(t => t.Carrier_Site_Link).HasColumnName("Carrier_Site_Link");
            this.Property(t => t.Last_Updated_Date).HasColumnName("Last_Updated_Date");
            this.Property(t => t.Updated_By).HasColumnName("Updated_By");
        }
    }
}
