using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Longevity_Manual_MasterFile_DataSetMap : EntityTypeConfiguration<Longevity_Manual_MasterFile_DataSet>
    {
        public Longevity_Manual_MasterFile_DataSetMap()
        {
            // Primary Key
            this.HasKey(t => t.MasterFileID);

            // Properties
            this.Property(t => t.PolicyID)
                .HasMaxLength(32);

            this.Property(t => t.Agent_Group)
                .HasMaxLength(128);

            this.Property(t => t.Agent_Working_Status)
                .HasMaxLength(128);

            this.Property(t => t.Agent_Clean)
                .HasMaxLength(128);

            this.Property(t => t.SourceCode)
                .HasMaxLength(32);

            this.Property(t => t.Plan_Type)
                .HasMaxLength(128);

            // Table & Column Mappings
            this.ToTable("Longevity_Manual_MasterFile_DataSet");
            this.Property(t => t.MasterFileID).HasColumnName("MasterFileID");
            this.Property(t => t.Insertion_Date).HasColumnName("Insertion_Date");
            this.Property(t => t.PolicyID).HasColumnName("PolicyID");
            this.Property(t => t.LMSID).HasColumnName("LMSID");
            this.Property(t => t.Agent_Group).HasColumnName("Agent_Group");
            this.Property(t => t.Sales_Agent).HasColumnName("Sales_Agent");
            this.Property(t => t.Agent_Working_Status).HasColumnName("Agent_Working_Status");
            this.Property(t => t.Agent_Clean).HasColumnName("Agent_Clean");
            this.Property(t => t.Assigned_Agent).HasColumnName("Assigned_Agent");
            this.Property(t => t.Agent_Final).HasColumnName("Agent_Final");
            this.Property(t => t.Product_Type).HasColumnName("Product_Type");
            this.Property(t => t.Policy_Number).HasColumnName("Policy_Number");
            this.Property(t => t.SourceCode).HasColumnName("SourceCode");
            this.Property(t => t.firstname).HasColumnName("firstname");
            this.Property(t => t.lastname).HasColumnName("lastname");
            this.Property(t => t.Partners).HasColumnName("Partners");
            this.Property(t => t.Policy_Effective_Date).HasColumnName("Policy_Effective_Date");
            this.Property(t => t.Plan_Type).HasColumnName("Plan_Type");
            this.Property(t => t.Carrier).HasColumnName("Carrier");
            this.Property(t => t.Plan_Name).HasColumnName("Plan_Name");
            this.Property(t => t.Plan_Selected).HasColumnName("Plan_Selected");
            this.Property(t => t.DOB).HasColumnName("DOB");
            this.Property(t => t.Age).HasColumnName("Age");
            this.Property(t => t.Customer_Address).HasColumnName("Customer_Address");
            this.Property(t => t.Customer_City).HasColumnName("Customer_City");
            this.Property(t => t.Customer_State).HasColumnName("Customer_State");
            this.Property(t => t.Customer_Zip).HasColumnName("Customer_Zip");
            this.Property(t => t.Customer_County).HasColumnName("Customer_County");
            this.Property(t => t.Customer_Email).HasColumnName("Customer_Email");
            this.Property(t => t.CategoryDriver).HasColumnName("CategoryDriver");
            this.Property(t => t.Losin).HasColumnName("Losin");
            this.Property(t => t.Priority).HasColumnName("Priority");
            this.Property(t => t.DispositionStatus).HasColumnName("DispositionStatus");
            this.Property(t => t.NextFollowUp).HasColumnName("NextFollowUp");
            this.Property(t => t.CurrentRecordStatus).HasColumnName("CurrentRecordStatus");
        }
    }
}
