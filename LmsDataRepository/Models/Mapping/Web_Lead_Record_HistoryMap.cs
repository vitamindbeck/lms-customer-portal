using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Web_Lead_Record_HistoryMap : EntityTypeConfiguration<Web_Lead_Record_History>
    {
        public Web_Lead_Record_HistoryMap()
        {
            // Primary Key
            this.HasKey(t => t.Web_Lead_Record_History_ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("Web_Lead_Record_History");
            this.Property(t => t.Web_Lead_Record_History_ID).HasColumnName("Web_Lead_Record_History_ID");
            this.Property(t => t.Record_ID).HasColumnName("Record_ID");
            this.Property(t => t.Web_Lead_ID).HasColumnName("Web_Lead_ID");
            this.Property(t => t.Date_Of_Import).HasColumnName("Date_Of_Import");
        }
    }
}
