using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class SalutationMap : EntityTypeConfiguration<Salutation>
    {
        public SalutationMap()
        {
            // Primary Key
            this.HasKey(t => t.Salutation_ID);

            // Properties
            this.Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(16);

            // Table & Column Mappings
            this.ToTable("Salutation");
            this.Property(t => t.Salutation_ID).HasColumnName("Salutation_ID");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Display_Order).HasColumnName("Display_Order");
            this.Property(t => t.Active).HasColumnName("Active");
        }
    }
}
