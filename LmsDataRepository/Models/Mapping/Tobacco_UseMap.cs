using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Tobacco_UseMap : EntityTypeConfiguration<Tobacco_Use>
    {
        public Tobacco_UseMap()
        {
            // Primary Key
            this.HasKey(t => t.Tobacco_Use_ID);

            // Properties
            this.Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Tobacco_Use");
            this.Property(t => t.Tobacco_Use_ID).HasColumnName("Tobacco_Use_ID");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Display_Order).HasColumnName("Display_Order");
            this.Property(t => t.Active).HasColumnName("Active");
        }
    }
}
