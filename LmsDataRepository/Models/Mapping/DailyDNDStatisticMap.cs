using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class DailyDNDStatisticMap : EntityTypeConfiguration<DailyDNDStatistic>
    {
        public DailyDNDStatisticMap()
        {
            // Primary Key
            this.HasKey(t => t.DailyDNDStatisticsID);

            // Properties
            this.Property(t => t.AgentName)
                .HasMaxLength(100);

            this.Property(t => t.TotalCallInTime)
                .HasMaxLength(25);

            this.Property(t => t.TotalCallOutTime)
                .HasMaxLength(25);

            this.Property(t => t.AfterCallWork)
                .HasMaxLength(25);

            this.Property(t => t.Break)
                .HasMaxLength(25);

            this.Property(t => t.Lunch)
                .HasMaxLength(25);

            this.Property(t => t.Meeting)
                .HasMaxLength(25);

            this.Property(t => t.DataEntry)
                .HasMaxLength(25);

            this.Property(t => t.Training)
                .HasMaxLength(25);

            this.Property(t => t.Supervisor)
                .HasMaxLength(25);

            this.Property(t => t.Personal)
                .HasMaxLength(25);

            this.Property(t => t.OutBound)
                .HasMaxLength(25);

            this.Property(t => t.TotalTimeOnDuty)
                .HasMaxLength(25);

            this.Property(t => t.TimeInDND)
                .HasMaxLength(25);

            // Table & Column Mappings
            this.ToTable("DailyDNDStatistics");
            this.Property(t => t.DailyDNDStatisticsID).HasColumnName("DailyDNDStatisticsID");
            this.Property(t => t.Device).HasColumnName("Device");
            this.Property(t => t.AgentName).HasColumnName("AgentName");
            this.Property(t => t.CallsHandled).HasColumnName("CallsHandled");
            this.Property(t => t.CallsIn).HasColumnName("CallsIn");
            this.Property(t => t.TotalCallInTime).HasColumnName("TotalCallInTime");
            this.Property(t => t.CallsOut).HasColumnName("CallsOut");
            this.Property(t => t.TotalCallOutTime).HasColumnName("TotalCallOutTime");
            this.Property(t => t.AfterCallWork).HasColumnName("AfterCallWork");
            this.Property(t => t.Break).HasColumnName("Break");
            this.Property(t => t.Lunch).HasColumnName("Lunch");
            this.Property(t => t.Meeting).HasColumnName("Meeting");
            this.Property(t => t.DataEntry).HasColumnName("DataEntry");
            this.Property(t => t.Training).HasColumnName("Training");
            this.Property(t => t.Supervisor).HasColumnName("Supervisor");
            this.Property(t => t.Personal).HasColumnName("Personal");
            this.Property(t => t.OutBound).HasColumnName("OutBound");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.TotalTimeOnDuty).HasColumnName("TotalTimeOnDuty");
            this.Property(t => t.TimeInDND).HasColumnName("TimeInDND");
        }
    }
}
