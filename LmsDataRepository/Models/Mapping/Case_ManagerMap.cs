using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Case_ManagerMap : EntityTypeConfiguration<Case_Manager>
    {
        public Case_ManagerMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Case_Manager_ID, t.Name });

            // Properties
            this.Property(t => t.Case_Manager_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(65);

            // Table & Column Mappings
            this.ToTable("Case_Manager");
            this.Property(t => t.Case_Manager_ID).HasColumnName("Case_Manager_ID");
            this.Property(t => t.Name).HasColumnName("Name");
        }
    }
}
