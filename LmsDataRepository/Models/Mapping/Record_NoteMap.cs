using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Record_NoteMap : EntityTypeConfiguration<Record_Note>
    {
        public Record_NoteMap()
        {
            // Primary Key
            this.HasKey(t => t.Record_Note_ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("Record_Note");
            this.Property(t => t.Record_Note_ID).HasColumnName("Record_Note_ID");
            this.Property(t => t.Record_ID).HasColumnName("Record_ID");
            this.Property(t => t.Display_Order).HasColumnName("Display_Order");
            this.Property(t => t.Notes).HasColumnName("Notes");
            this.Property(t => t.Created_On).HasColumnName("Created_On");
            this.Property(t => t.Created_By).HasColumnName("Created_By");
        }
    }
}
