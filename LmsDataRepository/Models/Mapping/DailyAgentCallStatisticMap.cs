using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class DailyAgentCallStatisticMap : EntityTypeConfiguration<DailyAgentCallStatistic>
    {
        public DailyAgentCallStatisticMap()
        {
            // Primary Key
            this.HasKey(t => t.DailyAgentCallStatisticsID);

            // Properties
            this.Property(t => t.Direction)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.Phone)
                .HasMaxLength(20);

            this.Property(t => t.First_Rang_ext)
                .HasMaxLength(10);

            this.Property(t => t.Last_Rang_ext)
                .HasMaxLength(10);

            this.Property(t => t.Answer_ext)
                .HasMaxLength(10);

            this.Property(t => t.Finished_ext)
                .HasMaxLength(10);

            this.Property(t => t.Call_Time)
                .HasMaxLength(10);

            this.Property(t => t.Talk_Time)
                .HasMaxLength(10);

            this.Property(t => t.Did_Digits)
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("DailyAgentCallStatistics");
            this.Property(t => t.DailyAgentCallStatisticsID).HasColumnName("DailyAgentCallStatisticsID");
            this.Property(t => t.Start_Time).HasColumnName("Start_Time");
            this.Property(t => t.Direction).HasColumnName("Direction");
            this.Property(t => t.Phone).HasColumnName("Phone");
            this.Property(t => t.Answered).HasColumnName("Answered");
            this.Property(t => t.First_Rang_ext).HasColumnName("First_Rang_ext");
            this.Property(t => t.Last_Rang_ext).HasColumnName("Last_Rang_ext");
            this.Property(t => t.Answer_ext).HasColumnName("Answer_ext");
            this.Property(t => t.Finished_ext).HasColumnName("Finished_ext");
            this.Property(t => t.Call_Time).HasColumnName("Call_Time");
            this.Property(t => t.Call_Time_Second).HasColumnName("Call_Time_Second");
            this.Property(t => t.Talk_Time).HasColumnName("Talk_Time");
            this.Property(t => t.Talk_Time_Second).HasColumnName("Talk_Time_Second");
            this.Property(t => t.Did_Digits).HasColumnName("Did_Digits");
            this.Property(t => t.Call_Segment).HasColumnName("Call_Segment");
            this.Property(t => t.Created_On).HasColumnName("Created_On");
        }
    }
}
