using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class vw_MasterFile_v2Map : EntityTypeConfiguration<vw_MasterFile_v2>
    {
        public vw_MasterFile_v2Map()
        {
            // Primary Key
            this.HasKey(t => new { t.MasterFileID, t.AgentGroup, t.Agent_Status, t.Agent_Type, t.Partners, t.Plan_Type, t.Priority, t.LMS_Current_Record_Status, t.Plan_Transaction_Status_ID, t.EffectiveDateOrPurchaseDateMonth });

            // Properties
            this.Property(t => t.MasterFileID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.GBSPolicyID)
                .HasMaxLength(50);

            this.Property(t => t.LMSID)
                .HasMaxLength(32);

            this.Property(t => t.AgentGroup)
                .IsRequired()
                .HasMaxLength(7);

            this.Property(t => t.Agent_Final)
                .HasMaxLength(65);

            this.Property(t => t.Agent_Status)
                .IsRequired()
                .HasMaxLength(8);

            this.Property(t => t.Agent_Type)
                .IsRequired()
                .HasMaxLength(15);

            this.Property(t => t.Product_Type)
                .HasMaxLength(100);

            this.Property(t => t.PolicyNumber)
                .HasMaxLength(55);

            this.Property(t => t.SourceNumber)
                .HasMaxLength(50);

            this.Property(t => t.FirstName)
                .HasMaxLength(32);

            this.Property(t => t.LastName)
                .HasMaxLength(32);

            this.Property(t => t.Partners)
                .IsRequired()
                .HasMaxLength(7);

            this.Property(t => t.Plan_Type)
                .IsRequired()
                .HasMaxLength(8);

            this.Property(t => t.Plan_Name)
                .HasMaxLength(150);

            this.Property(t => t.Plan_Selected)
                .HasMaxLength(50);

            this.Property(t => t.Customer_Address)
                .HasMaxLength(200);

            this.Property(t => t.Customer_City)
                .HasMaxLength(64);

            this.Property(t => t.Customer_State)
                .HasMaxLength(32);

            this.Property(t => t.Customer_Zip)
                .HasMaxLength(32);

            this.Property(t => t.Customer_County)
                .HasMaxLength(32);

            this.Property(t => t.Customer_Email)
                .HasMaxLength(100);

            this.Property(t => t.Priority)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.LMS_Current_Disposition_Status)
                .HasMaxLength(200);

            this.Property(t => t.LatestActivityAssignedTo)
                .HasMaxLength(65);

            this.Property(t => t.LMS_Current_Record_Status)
                .IsRequired()
                .HasMaxLength(33);

            this.Property(t => t.CategorySituation)
                .HasMaxLength(50);

            this.Property(t => t.Plan_Transaction_Status_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Plan_Transaction_Status_Description)
                .HasMaxLength(100);

            this.Property(t => t.EffectiveDateOrPurchaseDateMonth)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vw_MasterFile_v2");
            this.Property(t => t.MasterFileID).HasColumnName("MasterFileID");
            this.Property(t => t.GBSPolicyID).HasColumnName("GBSPolicyID");
            this.Property(t => t.LMSID).HasColumnName("LMSID");
            this.Property(t => t.AgentGroup).HasColumnName("AgentGroup");
            this.Property(t => t.Original_Agent).HasColumnName("Original_Agent");
            this.Property(t => t.Current_Record_Agent).HasColumnName("Current_Record_Agent");
            this.Property(t => t.Plan_Agent).HasColumnName("Plan_Agent");
            this.Property(t => t.Agent_Final).HasColumnName("Agent_Final");
            this.Property(t => t.Agent_Status).HasColumnName("Agent_Status");
            this.Property(t => t.Agent_Type).HasColumnName("Agent_Type");
            this.Property(t => t.Product_Type).HasColumnName("Product_Type");
            this.Property(t => t.PolicyNumber).HasColumnName("PolicyNumber");
            this.Property(t => t.SourceNumber).HasColumnName("SourceNumber");
            this.Property(t => t.Source_Code_ID).HasColumnName("Source_Code_ID");
            this.Property(t => t.FirstName).HasColumnName("FirstName");
            this.Property(t => t.LastName).HasColumnName("LastName");
            this.Property(t => t.Partners).HasColumnName("Partners");
            this.Property(t => t.EffectiveDateOrPurchaseDate).HasColumnName("EffectiveDateOrPurchaseDate");
            this.Property(t => t.Plan_Type).HasColumnName("Plan_Type");
            this.Property(t => t.Carrier).HasColumnName("Carrier");
            this.Property(t => t.CarrierID).HasColumnName("CarrierID");
            this.Property(t => t.Plan_Name).HasColumnName("Plan_Name");
            this.Property(t => t.ProductTypeID).HasColumnName("ProductTypeID");
            this.Property(t => t.Plan_Selected).HasColumnName("Plan_Selected");
            this.Property(t => t.DOB).HasColumnName("DOB");
            this.Property(t => t.Age).HasColumnName("Age");
            this.Property(t => t.Customer_Address).HasColumnName("Customer_Address");
            this.Property(t => t.Customer_City).HasColumnName("Customer_City");
            this.Property(t => t.Customer_State).HasColumnName("Customer_State");
            this.Property(t => t.Customer_Zip).HasColumnName("Customer_Zip");
            this.Property(t => t.Customer_County).HasColumnName("Customer_County");
            this.Property(t => t.Customer_Email).HasColumnName("Customer_Email");
            this.Property(t => t.Category_Driver).HasColumnName("Category_Driver");
            this.Property(t => t.Category_Driver_Desc).HasColumnName("Category_Driver_Desc");
            this.Property(t => t.Priority).HasColumnName("Priority");
            this.Property(t => t.LMS_Current_Disposition_Status).HasColumnName("LMS_Current_Disposition_Status");
            this.Property(t => t.NextFollowUp).HasColumnName("NextFollowUp");
            this.Property(t => t.NextFollowUpType).HasColumnName("NextFollowUpType");
            this.Property(t => t.NextFollowUpFor).HasColumnName("NextFollowUpFor");
            this.Property(t => t.LatestActivityType).HasColumnName("LatestActivityType");
            this.Property(t => t.LatestActivityAssignedTo).HasColumnName("LatestActivityAssignedTo");
            this.Property(t => t.LatestActivityDate).HasColumnName("LatestActivityDate");
            this.Property(t => t.LMS_Current_Record_Status).HasColumnName("LMS_Current_Record_Status");
            this.Property(t => t.AgentFinalUserID).HasColumnName("AgentFinalUserID");
            this.Property(t => t.DOB_Month).HasColumnName("DOB_Month");
            this.Property(t => t.Record_Type_ID).HasColumnName("Record_Type_ID");
            this.Property(t => t.CategorySituation).HasColumnName("CategorySituation");
            this.Property(t => t.CategorySituationDesc).HasColumnName("CategorySituationDesc");
            this.Property(t => t.CategorySituationID).HasColumnName("CategorySituationID");
            this.Property(t => t.Record_ID).HasColumnName("Record_ID");
            this.Property(t => t.Plan_Transaction_ID).HasColumnName("Plan_Transaction_ID");
            this.Property(t => t.Plan_Transaction_Status_ID).HasColumnName("Plan_Transaction_Status_ID");
            this.Property(t => t.Plan_Transaction_Status_Description).HasColumnName("Plan_Transaction_Status_Description");
            this.Property(t => t.EffectiveDateOrPurchaseDateMonth).HasColumnName("EffectiveDateOrPurchaseDateMonth");
        }
    }
}
