using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Plan_Transaction_DateMap : EntityTypeConfiguration<Plan_Transaction_Date>
    {
        public Plan_Transaction_DateMap()
        {
            // Primary Key
            this.HasKey(t => t.Plan_Transaction_Date_ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("Plan_Transaction_Date");
            this.Property(t => t.Plan_Transaction_Date_ID).HasColumnName("Plan_Transaction_Date_ID");
            this.Property(t => t.Plan_Transaction_ID).HasColumnName("Plan_Transaction_ID");
            this.Property(t => t.Transaction_Date_Type_ID).HasColumnName("Transaction_Date_Type_ID");
            this.Property(t => t.Date_Value).HasColumnName("Date_Value");
        }
    }
}
