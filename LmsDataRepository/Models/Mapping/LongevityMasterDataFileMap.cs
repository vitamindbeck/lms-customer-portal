using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class LongevityMasterDataFileMap : EntityTypeConfiguration<LongevityMasterDataFile>
    {
        public LongevityMasterDataFileMap()
        {
            // Primary Key
            this.HasKey(t => t.MasterFileID);

            // Properties
            // Table & Column Mappings
            this.ToTable("LongevityMasterDataFile");
            this.Property(t => t.MasterFileID).HasColumnName("MasterFileID");
            this.Property(t => t.CreatedOnDate).HasColumnName("CreatedOnDate");
            this.Property(t => t.GBSPolicyID).HasColumnName("GBSPolicyID");
            this.Property(t => t.LMSRecordID).HasColumnName("LMSRecordID");
            this.Property(t => t.CategoryDriverID).HasColumnName("CategoryDriverID");
            this.Property(t => t.AgentFinalUserID).HasColumnName("AgentFinalUserID");
            this.Property(t => t.Priority).HasColumnName("Priority");
            this.Property(t => t.CategorySituationID).HasColumnName("CategorySituationID");
        }
    }
}
