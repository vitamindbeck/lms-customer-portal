using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class CriticalTopicCoverageMap : EntityTypeConfiguration<CriticalTopicCoverage>
    {
        public CriticalTopicCoverageMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CTCID, t.Description, t.Active });

            // Properties
            this.Property(t => t.CTCID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.Description)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("CriticalTopicCoverage");
            this.Property(t => t.CTCID).HasColumnName("CTCID");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Active).HasColumnName("Active");
        }
    }
}
