using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Quote_Benefit_LengthMap : EntityTypeConfiguration<Quote_Benefit_Length>
    {
        public Quote_Benefit_LengthMap()
        {
            // Primary Key
            this.HasKey(t => t.Quote_Benefit_Length_ID);

            // Properties
            this.Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("Quote_Benefit_Length");
            this.Property(t => t.Quote_Benefit_Length_ID).HasColumnName("Quote_Benefit_Length_ID");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Monthly_Factor).HasColumnName("Monthly_Factor");
        }
    }
}
