using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Quote_Elimination_PeriodMap : EntityTypeConfiguration<Quote_Elimination_Period>
    {
        public Quote_Elimination_PeriodMap()
        {
            // Primary Key
            this.HasKey(t => t.Quote_Elimination_Period_ID);

            // Properties
            this.Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Quote_Elimination_Period");
            this.Property(t => t.Quote_Elimination_Period_ID).HasColumnName("Quote_Elimination_Period_ID");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Low).HasColumnName("Low");
            this.Property(t => t.High).HasColumnName("High");
        }
    }
}
