using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Phone_Number_TypeMap : EntityTypeConfiguration<Phone_Number_Type>
    {
        public Phone_Number_TypeMap()
        {
            // Primary Key
            this.HasKey(t => t.Phone_Number_Type_ID);

            // Properties
            this.Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Phone_Number_Type");
            this.Property(t => t.Phone_Number_Type_ID).HasColumnName("Phone_Number_Type_ID");
            this.Property(t => t.Description).HasColumnName("Description");
        }
    }
}
