using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class la_Industry_News_ArticleMap : EntityTypeConfiguration<la_Industry_News_Article>
    {
        public la_Industry_News_ArticleMap()
        {
            // Primary Key
            this.HasKey(t => t.la_Industry_News_Article_ID);

            // Properties
            this.Property(t => t.Title)
                .HasMaxLength(1000);

            this.Property(t => t.Link_URL)
                .HasMaxLength(1000);

            this.Property(t => t.Image_URL)
                .HasMaxLength(1000);

            this.Property(t => t.Image_Alt)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("la_Industry_News_Article");
            this.Property(t => t.la_Industry_News_Article_ID).HasColumnName("la_Industry_News_Article_ID");
            this.Property(t => t.la_Industry_Section_ID).HasColumnName("la_Industry_Section_ID");
            this.Property(t => t.Title).HasColumnName("Title");
            this.Property(t => t.Link_URL).HasColumnName("Link_URL");
            this.Property(t => t.Image_URL).HasColumnName("Image_URL");
            this.Property(t => t.Image_Alt).HasColumnName("Image_Alt");
            this.Property(t => t.Article_Date).HasColumnName("Article_Date");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Active).HasColumnName("Active");
        }
    }
}
