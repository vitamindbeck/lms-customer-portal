using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class AppointmentMap : EntityTypeConfiguration<Appointment>
    {
        public AppointmentMap()
        {
            // Primary Key
            this.HasKey(t => t.Appointment_ID);

            // Properties
            this.Property(t => t.Agent_Number)
                .HasMaxLength(128);

            // Table & Column Mappings
            this.ToTable("Appointment");
            this.Property(t => t.Appointment_ID).HasColumnName("Appointment_ID");
            this.Property(t => t.Agent_ID).HasColumnName("Agent_ID");
            this.Property(t => t.Carrier_ID).HasColumnName("Carrier_ID");
            this.Property(t => t.Agent_Number).HasColumnName("Agent_Number");
            this.Property(t => t.Last_Updated_Date).HasColumnName("Last_Updated_Date");
            this.Property(t => t.Appointment_Date).HasColumnName("Appointment_Date");
            this.Property(t => t.Terminate_Date).HasColumnName("Terminate_Date");
            this.Property(t => t.Line_Of_Insurance_ID).HasColumnName("Line_Of_Insurance_ID");
            this.Property(t => t.Updated_By).HasColumnName("Updated_By");
            this.Property(t => t.GBS_Appointment_ID).HasColumnName("GBS_Appointment_ID");
        }
    }
}
