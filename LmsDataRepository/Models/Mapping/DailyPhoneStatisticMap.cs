using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class DailyPhoneStatisticMap : EntityTypeConfiguration<DailyPhoneStatistic>
    {
        public DailyPhoneStatisticMap()
        {
            // Primary Key
            this.HasKey(t => t.DailyPhoneStatisticsID);

            // Properties
            // Table & Column Mappings
            this.ToTable("DailyPhoneStatistics");
            this.Property(t => t.DailyPhoneStatisticsID).HasColumnName("DailyPhoneStatisticsID");
            this.Property(t => t.ReportDate).HasColumnName("ReportDate");
            this.Property(t => t.RunTime).HasColumnName("RunTime");

            // Relationships
            this.HasRequired(t => t.DailyPhoneStatistic1)
                .WithOptional(t => t.DailyPhoneStatistics1);

        }
    }
}
