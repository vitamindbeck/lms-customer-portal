using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Current_Insured_TypeMap : EntityTypeConfiguration<Current_Insured_Type>
    {
        public Current_Insured_TypeMap()
        {
            // Primary Key
            this.HasKey(t => t.Current_Insured_Type_ID);

            // Properties
            this.Property(t => t.Current_Insured_Type_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Current_Insured)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Current_Insured_Type");
            this.Property(t => t.Current_Insured_Type_ID).HasColumnName("Current_Insured_Type_ID");
            this.Property(t => t.Current_Insured).HasColumnName("Current_Insured");
            this.Property(t => t.Active).HasColumnName("Active");
            this.Property(t => t.Display_Order).HasColumnName("Display_Order");
        }
    }
}
