using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Marketing_GroupMap : EntityTypeConfiguration<Marketing_Group>
    {
        public Marketing_GroupMap()
        {
            // Primary Key
            this.HasKey(t => t.Marketing_Group_ID);

            // Properties
            this.Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Marketing_Group");
            this.Property(t => t.Marketing_Group_ID).HasColumnName("Marketing_Group_ID");
            this.Property(t => t.Description).HasColumnName("Description");
        }
    }
}
