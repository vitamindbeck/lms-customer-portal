using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class vw_Record_Activity_Latest_FUMap : EntityTypeConfiguration<vw_Record_Activity_Latest_FU>
    {
        public vw_Record_Activity_Latest_FUMap()
        {
            // Primary Key
            this.HasKey(t => t.Record_ID);

            // Properties
            this.Property(t => t.Record_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vw_Record_Activity_Latest_FU");
            this.Property(t => t.Record_ID).HasColumnName("Record_ID");
            this.Property(t => t.Record_Activity_ID).HasColumnName("Record_Activity_ID");
        }
    }
}
