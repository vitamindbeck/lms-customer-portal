using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class NoteMap : EntityTypeConfiguration<Note>
    {
        public NoteMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("Note");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.TypeofNote).HasColumnName("TypeofNote");
            this.Property(t => t.NoteText).HasColumnName("NoteText");
        }
    }
}
