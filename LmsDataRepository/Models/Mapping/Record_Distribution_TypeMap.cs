using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Record_Distribution_TypeMap : EntityTypeConfiguration<Record_Distribution_Type>
    {
        public Record_Distribution_TypeMap()
        {
            // Primary Key
            this.HasKey(t => t.Record_Distribution_Type_ID);

            // Properties
            this.Property(t => t.Record_Distribution_Type_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Record_Distribution_Type_Description)
                .IsRequired()
                .HasMaxLength(30);

            // Table & Column Mappings
            this.ToTable("Record_Distribution_Type");
            this.Property(t => t.Record_Distribution_Type_ID).HasColumnName("Record_Distribution_Type_ID");
            this.Property(t => t.Record_Distribution_Type_Description).HasColumnName("Record_Distribution_Type_Description");
        }
    }
}
