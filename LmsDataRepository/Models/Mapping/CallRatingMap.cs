using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class CallRatingMap : EntityTypeConfiguration<CallRating>
    {
        public CallRatingMap()
        {
            // Primary Key
            this.HasKey(t => t.Call_Rating_ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("CallRating");
            this.Property(t => t.Call_Rating_ID).HasColumnName("Call_Rating_ID");
            this.Property(t => t.Call_of_Week).HasColumnName("Call_of_Week");
            this.Property(t => t.Agent_Rating).HasColumnName("Agent_Rating");
            this.Property(t => t.Manager_Rating).HasColumnName("Manager_Rating");
            this.Property(t => t.Record_Activity_ID).HasColumnName("Record_Activity_ID");

            // Relationships
            this.HasRequired(t => t.Record_Activity)
                .WithMany(t => t.CallRatings)
                .HasForeignKey(d => d.Record_Activity_ID);

        }
    }
}
