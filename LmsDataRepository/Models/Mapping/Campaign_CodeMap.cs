using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Campaign_CodeMap : EntityTypeConfiguration<Campaign_Code>
    {
        public Campaign_CodeMap()
        {
            // Primary Key
            this.HasKey(t => t.Campaign_Code_ID);

            // Properties
            this.Property(t => t.Campaign_Number)
                .HasMaxLength(50);

            this.Property(t => t.Campaign_Description)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("Campaign_Code");
            this.Property(t => t.Campaign_Code_ID).HasColumnName("Campaign_Code_ID");
            this.Property(t => t.Campaign_Number).HasColumnName("Campaign_Number");
            this.Property(t => t.Campaign_Description).HasColumnName("Campaign_Description");
            this.Property(t => t.Campaign_Start_Date).HasColumnName("Campaign_Start_Date");
            this.Property(t => t.Active).HasColumnName("Active");
        }
    }
}
