using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Record_Activity_StatusMap : EntityTypeConfiguration<Record_Activity_Status>
    {
        public Record_Activity_StatusMap()
        {
            // Primary Key
            this.HasKey(t => t.Record_Activity_Status_ID);

            // Properties
            this.Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("Record_Activity_Status");
            this.Property(t => t.Record_Activity_Status_ID).HasColumnName("Record_Activity_Status_ID");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Display_Order).HasColumnName("Display_Order");
            this.Property(t => t.Active).HasColumnName("Active");
        }
    }
}
