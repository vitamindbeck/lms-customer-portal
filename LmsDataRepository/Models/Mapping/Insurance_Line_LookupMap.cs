using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Insurance_Line_LookupMap : EntityTypeConfiguration<Insurance_Line_Lookup>
    {
        public Insurance_Line_LookupMap()
        {
            // Primary Key
            this.HasKey(t => t.Line_Of_Insurance_ID);

            // Properties
            this.Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(35);

            // Table & Column Mappings
            this.ToTable("Insurance_Line_Lookup");
            this.Property(t => t.Line_Of_Insurance_ID).HasColumnName("Line_Of_Insurance_ID");
            this.Property(t => t.Description).HasColumnName("Description");
        }
    }
}
