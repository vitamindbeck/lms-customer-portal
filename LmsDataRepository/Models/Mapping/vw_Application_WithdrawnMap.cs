using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class vw_Application_WithdrawnMap : EntityTypeConfiguration<vw_Application_Withdrawn>
    {
        public vw_Application_WithdrawnMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Plan_Transaction_ID, t.Transaction_Date_Type_ID });

            // Properties
            this.Property(t => t.Plan_Transaction_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Transaction_Date_Type_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vw_Application_Withdrawn");
            this.Property(t => t.Plan_Transaction_ID).HasColumnName("Plan_Transaction_ID");
            this.Property(t => t.Transaction_Date_Type_ID).HasColumnName("Transaction_Date_Type_ID");
            this.Property(t => t.Date_Value).HasColumnName("Date_Value");
        }
    }
}
