using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Buying_Period_SubMap : EntityTypeConfiguration<Buying_Period_Sub>
    {
        public Buying_Period_SubMap()
        {
            // Primary Key
            this.HasKey(t => t.Buying_Period_Sub_ID);

            // Properties
            this.Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("Buying_Period_Sub");
            this.Property(t => t.Buying_Period_Sub_ID).HasColumnName("Buying_Period_Sub_ID");
            this.Property(t => t.Buying_Period_ID).HasColumnName("Buying_Period_ID");
            this.Property(t => t.Description).HasColumnName("Description");
        }
    }
}
