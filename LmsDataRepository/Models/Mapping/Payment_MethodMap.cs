using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Payment_MethodMap : EntityTypeConfiguration<Payment_Method>
    {
        public Payment_MethodMap()
        {
            // Primary Key
            this.HasKey(t => t.Payment_Method_ID);

            // Properties
            this.Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Abbreviation)
                .IsRequired()
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("Payment_Method");
            this.Property(t => t.Payment_Method_ID).HasColumnName("Payment_Method_ID");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Abbreviation).HasColumnName("Abbreviation");
            this.Property(t => t.Include_Renewal).HasColumnName("Include_Renewal");
            this.Property(t => t.Weight).HasColumnName("Weight");
            this.Property(t => t.Default).HasColumnName("Default");
            this.Property(t => t.Active).HasColumnName("Active");
        }
    }
}
