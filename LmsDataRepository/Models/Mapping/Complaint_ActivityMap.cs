using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Complaint_ActivityMap : EntityTypeConfiguration<Complaint_Activity>
    {
        public Complaint_ActivityMap()
        {
            // Primary Key
            this.HasKey(t => t.Complaint_Activity_ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("Complaint_Activity");
            this.Property(t => t.Complaint_Activity_ID).HasColumnName("Complaint_Activity_ID");
            this.Property(t => t.Complaint_ID).HasColumnName("Complaint_ID");
            this.Property(t => t.Record_Activity_ID).HasColumnName("Record_Activity_ID");
            this.Property(t => t.Record_Activity_Status_ID).HasColumnName("Record_Activity_Status_ID");

            // Relationships
            this.HasRequired(t => t.Complaint)
                .WithMany(t => t.Complaint_Activity)
                .HasForeignKey(d => d.Complaint_ID);
            this.HasRequired(t => t.Record_Activity)
                .WithMany(t => t.Complaint_Activity)
                .HasForeignKey(d => d.Record_Activity_ID);

        }
    }
}
