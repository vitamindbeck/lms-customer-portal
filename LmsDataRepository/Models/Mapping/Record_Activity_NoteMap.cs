using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Record_Activity_NoteMap : EntityTypeConfiguration<Record_Activity_Note>
    {
        public Record_Activity_NoteMap()
        {
            // Primary Key
            this.HasKey(t => t.Record_Activity_Note_ID);

            // Properties
            this.Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(1000);

            // Table & Column Mappings
            this.ToTable("Record_Activity_Note");
            this.Property(t => t.Record_Activity_Note_ID).HasColumnName("Record_Activity_Note_ID");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.User_ID).HasColumnName("User_ID");
            this.Property(t => t.Active).HasColumnName("Active");
            this.Property(t => t.Record_Activity_Note_Type).HasColumnName("Record_Activity_Note_Type");
            this.Property(t => t.Assign_To).HasColumnName("Assign_To");
        }
    }
}
