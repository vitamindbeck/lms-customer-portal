using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class vw_Record_Activity_Phone_CountsMap : EntityTypeConfiguration<vw_Record_Activity_Phone_Counts>
    {
        public vw_Record_Activity_Phone_CountsMap()
        {
            // Primary Key
            this.HasKey(t => t.Record_ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("vw_Record_Activity_Phone_Counts");
            this.Property(t => t.Record_ID).HasColumnName("Record_ID");
            this.Property(t => t.Created_On).HasColumnName("Created_On");
            this.Property(t => t.Call_1).HasColumnName("Call_1");
            this.Property(t => t.Call_2).HasColumnName("Call_2");
            this.Property(t => t.Call_3).HasColumnName("Call_3");
            this.Property(t => t.Call_4).HasColumnName("Call_4");
            this.Property(t => t.Call_5).HasColumnName("Call_5");
        }
    }
}
