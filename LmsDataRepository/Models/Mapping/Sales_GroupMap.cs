using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Sales_GroupMap : EntityTypeConfiguration<Sales_Group>
    {
        public Sales_GroupMap()
        {
            // Primary Key
            this.HasKey(t => t.Sales_Group_ID);

            // Properties
            this.Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Sales_Group");
            this.Property(t => t.Sales_Group_ID).HasColumnName("Sales_Group_ID");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Manager_User_ID).HasColumnName("Manager_User_ID");
            this.Property(t => t.Admin_User_ID).HasColumnName("Admin_User_ID");
        }
    }
}
