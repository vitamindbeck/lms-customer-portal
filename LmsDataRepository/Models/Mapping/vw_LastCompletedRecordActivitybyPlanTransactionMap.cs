using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class vw_LastCompletedRecordActivitybyPlanTransactionMap : EntityTypeConfiguration<vw_LastCompletedRecordActivitybyPlanTransaction>
    {
        public vw_LastCompletedRecordActivitybyPlanTransactionMap()
        {
            // Primary Key
            this.HasKey(t => t.Plan_Transaction_ID);

            // Properties
            this.Property(t => t.Plan_Transaction_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vw_LastCompletedRecordActivitybyPlanTransaction");
            this.Property(t => t.Plan_Transaction_ID).HasColumnName("Plan_Transaction_ID");
            this.Property(t => t.Record_Activity_ID).HasColumnName("Record_Activity_ID");
        }
    }
}
