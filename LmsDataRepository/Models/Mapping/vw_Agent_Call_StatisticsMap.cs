using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class vw_Agent_Call_StatisticsMap : EntityTypeConfiguration<vw_Agent_Call_Statistics>
    {
        public vw_Agent_Call_StatisticsMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Start_Time, t.Direction });

            // Properties
            this.Property(t => t.Answer_ext)
                .HasMaxLength(10);

            this.Property(t => t.Direction)
                .IsRequired()
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("vw_Agent_Call_Statistics");
            this.Property(t => t.Answer_ext).HasColumnName("Answer_ext");
            this.Property(t => t.Start_Time).HasColumnName("Start_Time");
            this.Property(t => t.EndTime).HasColumnName("EndTime");
            this.Property(t => t.FirstMinGroupTime).HasColumnName("FirstMinGroupTime");
            this.Property(t => t.FirstMaxGroupTime).HasColumnName("FirstMaxGroupTime");
            this.Property(t => t.Call_Time_Second).HasColumnName("Call_Time_Second");
            this.Property(t => t.Elapsed).HasColumnName("Elapsed");
            this.Property(t => t.FirstThirty).HasColumnName("FirstThirty");
            this.Property(t => t.LastThirty).HasColumnName("LastThirty");
            this.Property(t => t.LastMinGroupTime).HasColumnName("LastMinGroupTime");
            this.Property(t => t.LastMaxGroupTime).HasColumnName("LastMaxGroupTime");
            this.Property(t => t.Direction).HasColumnName("Direction");
        }
    }
}
