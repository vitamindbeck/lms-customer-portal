using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class ModalMap : EntityTypeConfiguration<Modal>
    {
        public ModalMap()
        {
            // Primary Key
            this.HasKey(t => t.Modal_ID);

            // Properties
            this.Property(t => t.Modal_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("Modal");
            this.Property(t => t.Modal_ID).HasColumnName("Modal_ID");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Display_Order).HasColumnName("Display_Order");
            this.Property(t => t.Active).HasColumnName("Active");
            this.Property(t => t.Pay_Period).HasColumnName("Pay_Period");
            this.Property(t => t.GBS_ID).HasColumnName("GBS_ID");
        }
    }
}
