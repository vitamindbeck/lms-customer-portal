using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class RecordMap : EntityTypeConfiguration<Record>
    {
        public RecordMap()
        {
            // Primary Key
            this.HasKey(t => t.Record_ID);

            // Properties
            this.Property(t => t.Customer_ID)
                .HasMaxLength(32);

            this.Property(t => t.First_Name)
                .IsRequired()
                .HasMaxLength(32);

            this.Property(t => t.Middle_Initial)
                .HasMaxLength(8);

            this.Property(t => t.Last_Name)
                .IsRequired()
                .HasMaxLength(32);

            this.Property(t => t.Suffix)
                .HasMaxLength(32);

            this.Property(t => t.Address_1)
                .HasMaxLength(200);

            this.Property(t => t.Address_2)
                .HasMaxLength(200);

            this.Property(t => t.City)
                .HasMaxLength(64);

            this.Property(t => t.County)
                .HasMaxLength(32);

            this.Property(t => t.Zipcode)
                .HasMaxLength(32);

            this.Property(t => t.Alt_Address_1)
                .HasMaxLength(200);

            this.Property(t => t.Alt_Address_2)
                .HasMaxLength(200);

            this.Property(t => t.Alt_City)
                .HasMaxLength(64);

            this.Property(t => t.Alt_County)
                .HasMaxLength(32);

            this.Property(t => t.Alt_Zipcode)
                .HasMaxLength(32);

            this.Property(t => t.Home_Phone)
                .HasMaxLength(32);

            this.Property(t => t.Work_Phone)
                .HasMaxLength(32);

            this.Property(t => t.Cell_Phone)
                .HasMaxLength(32);

            this.Property(t => t.Fax)
                .HasMaxLength(32);

            this.Property(t => t.Email_Address)
                .HasMaxLength(100);

            this.Property(t => t.Sex)
                .IsRequired()
                .HasMaxLength(8);

            this.Property(t => t.SSN)
                .HasMaxLength(128);

            this.Property(t => t.Reffered_By)
                .HasMaxLength(128);

            this.Property(t => t.Medicare_Number)
                .HasMaxLength(512);

            this.Property(t => t.Current_Insured_Desc)
                .HasMaxLength(200);

            this.Property(t => t.Drug_List_ID)
                .HasMaxLength(512);

            this.Property(t => t.Buying_Period_Notes)
                .HasMaxLength(1000);

            this.Property(t => t.Quit_Date)
                .HasMaxLength(200);

            this.Property(t => t.Alt_Email_Address)
                .HasMaxLength(128);

            this.Property(t => t.Alt_First_Name)
                .HasMaxLength(64);

            this.Property(t => t.Alt_Last_Name)
                .HasMaxLength(64);

            this.Property(t => t.Alt_Phone_Number)
                .HasMaxLength(50);

            this.Property(t => t.Email_Address_2)
                .HasMaxLength(100);

            this.Property(t => t.Lead_Quality)
                .HasMaxLength(100);

            this.Property(t => t.Other_Key)
                .HasMaxLength(50);

            this.Property(t => t.CurrentSituation)
                .HasMaxLength(500);

            this.Property(t => t.ProductsOfInterest)
                .HasMaxLength(500);

            this.Property(t => t.Medicare_FirstName)
                .HasMaxLength(32);

            this.Property(t => t.Medicare_InitialName)
                .HasMaxLength(8);

            this.Property(t => t.Medicare_LastName)
                .HasMaxLength(32);

            this.Property(t => t.Medical_Carrier)
                .HasMaxLength(32);

            this.Property(t => t.Drug_Plan_Co)
                .HasMaxLength(32);

            this.Property(t => t.Mailing_Address_1)
                .HasMaxLength(200);

            this.Property(t => t.Mailing_Address_2)
                .HasMaxLength(200);

            this.Property(t => t.Mailing_City)
                .HasMaxLength(64);

            this.Property(t => t.Mailing_ZipCode)
                .HasMaxLength(32);

            this.Property(t => t.Mailing_County)
                .HasMaxLength(64);

            this.Property(t => t.Work_Phone_Ext)
                .HasMaxLength(10);

            this.Property(t => t.NickName)
                .HasMaxLength(128);

            this.Property(t => t.Contact_First_Name)
                .HasMaxLength(32);

            this.Property(t => t.Contact_Middle_Initial)
                .HasMaxLength(1);

            this.Property(t => t.Contact_Last_Name)
                .HasMaxLength(32);

            this.Property(t => t.Contact_Suffix)
                .HasMaxLength(2);

            this.Property(t => t.MedicareId)
                .HasMaxLength(50);

            this.Property(t => t.MedicareGovId)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Record");
            this.Property(t => t.Record_ID).HasColumnName("Record_ID");
            this.Property(t => t.Customer_ID).HasColumnName("Customer_ID");
            this.Property(t => t.Salutation_ID).HasColumnName("Salutation_ID");
            this.Property(t => t.First_Name).HasColumnName("First_Name");
            this.Property(t => t.Middle_Initial).HasColumnName("Middle_Initial");
            this.Property(t => t.Last_Name).HasColumnName("Last_Name");
            this.Property(t => t.Suffix).HasColumnName("Suffix");
            this.Property(t => t.Address_1).HasColumnName("Address_1");
            this.Property(t => t.Address_2).HasColumnName("Address_2");
            this.Property(t => t.City).HasColumnName("City");
            this.Property(t => t.State_ID).HasColumnName("State_ID");
            this.Property(t => t.County).HasColumnName("County");
            this.Property(t => t.Zipcode).HasColumnName("Zipcode");
            this.Property(t => t.Alt_Address_1).HasColumnName("Alt_Address_1");
            this.Property(t => t.Alt_Address_2).HasColumnName("Alt_Address_2");
            this.Property(t => t.Alt_City).HasColumnName("Alt_City");
            this.Property(t => t.Alt_State_ID).HasColumnName("Alt_State_ID");
            this.Property(t => t.Alt_County).HasColumnName("Alt_County");
            this.Property(t => t.Alt_Zipcode).HasColumnName("Alt_Zipcode");
            this.Property(t => t.Alt_Address_From_Date).HasColumnName("Alt_Address_From_Date");
            this.Property(t => t.Alt_Address_To_Date).HasColumnName("Alt_Address_To_Date");
            this.Property(t => t.Home_Phone).HasColumnName("Home_Phone");
            this.Property(t => t.Work_Phone).HasColumnName("Work_Phone");
            this.Property(t => t.Cell_Phone).HasColumnName("Cell_Phone");
            this.Property(t => t.Fax).HasColumnName("Fax");
            this.Property(t => t.DOB).HasColumnName("DOB");
            this.Property(t => t.Height).HasColumnName("Height");
            this.Property(t => t.Weight).HasColumnName("Weight");
            this.Property(t => t.Email_Address).HasColumnName("Email_Address");
            this.Property(t => t.Imported).HasColumnName("Imported");
            this.Property(t => t.Initial_Contact_Date).HasColumnName("Initial_Contact_Date");
            this.Property(t => t.Current_Tobacco_Use).HasColumnName("Current_Tobacco_Use");
            this.Property(t => t.Employment_Status_ID).HasColumnName("Employment_Status_ID");
            this.Property(t => t.Source_Code_ID).HasColumnName("Source_Code_ID");
            this.Property(t => t.Marital_Status_ID).HasColumnName("Marital_Status_ID");
            this.Property(t => t.Sex).HasColumnName("Sex");
            this.Property(t => t.SSN).HasColumnName("SSN");
            this.Property(t => t.Client_Type_ID).HasColumnName("Client_Type_ID");
            this.Property(t => t.Annual_Income).HasColumnName("Annual_Income");
            this.Property(t => t.Agent_ID).HasColumnName("Agent_ID");
            this.Property(t => t.Assistant_Agent_ID).HasColumnName("Assistant_Agent_ID");
            this.Property(t => t.Reffered_By).HasColumnName("Reffered_By");
            this.Property(t => t.Medicare_Number).HasColumnName("Medicare_Number");
            this.Property(t => t.Part_A_Effective).HasColumnName("Part_A_Effective");
            this.Property(t => t.Part_B_Effective).HasColumnName("Part_B_Effective");
            this.Property(t => t.Part_A_Effective_Date).HasColumnName("Part_A_Effective_Date");
            this.Property(t => t.Part_B_Effective_Date).HasColumnName("Part_B_Effective_Date");
            this.Property(t => t.Currently_Insured).HasColumnName("Currently_Insured");
            this.Property(t => t.Current_Insured_Type_ID).HasColumnName("Current_Insured_Type_ID");
            this.Property(t => t.Current_Insured_Desc).HasColumnName("Current_Insured_Desc");
            this.Property(t => t.Drug_List_ID).HasColumnName("Drug_List_ID");
            this.Property(t => t.Partner_ID).HasColumnName("Partner_ID");
            this.Property(t => t.Momentum).HasColumnName("Momentum");
            this.Property(t => t.Last_Updated_Date).HasColumnName("Last_Updated_Date");
            this.Property(t => t.Do_Not_Contact).HasColumnName("Do_Not_Contact");
            this.Property(t => t.Opt_Out_Email).HasColumnName("Opt_Out_Email");
            this.Property(t => t.Created_On).HasColumnName("Created_On");
            this.Property(t => t.Record_Source_ID).HasColumnName("Record_Source_ID");
            this.Property(t => t.Record_Type_ID).HasColumnName("Record_Type_ID");
            this.Property(t => t.Follow_Up_Date).HasColumnName("Follow_Up_Date");
            this.Property(t => t.Buying_Period_ID).HasColumnName("Buying_Period_ID");
            this.Property(t => t.Buying_Period_Sub_ID).HasColumnName("Buying_Period_Sub_ID");
            this.Property(t => t.Buying_Period_Notes).HasColumnName("Buying_Period_Notes");
            this.Property(t => t.Priority_Level).HasColumnName("Priority_Level");
            this.Property(t => t.Notes).HasColumnName("Notes");
            this.Property(t => t.Company_ID).HasColumnName("Company_ID");
            this.Property(t => t.Unsubscribed).HasColumnName("Unsubscribed");
            this.Property(t => t.B2E).HasColumnName("B2E");
            this.Property(t => t.Occupation_ID).HasColumnName("Occupation_ID");
            this.Property(t => t.Age).HasColumnName("Age");
            this.Property(t => t.Current_Tobacco_ID).HasColumnName("Current_Tobacco_ID");
            this.Property(t => t.Referral_Type_ID).HasColumnName("Referral_Type_ID");
            this.Property(t => t.Past_Tobacco_Use).HasColumnName("Past_Tobacco_Use");
            this.Property(t => t.Past_Tobacco_ID).HasColumnName("Past_Tobacco_ID");
            this.Property(t => t.Quit_Date).HasColumnName("Quit_Date");
            this.Property(t => t.Alt_Email_Address).HasColumnName("Alt_Email_Address");
            this.Property(t => t.Alt_First_Name).HasColumnName("Alt_First_Name");
            this.Property(t => t.Alt_Last_Name).HasColumnName("Alt_Last_Name");
            this.Property(t => t.Broker_ID).HasColumnName("Broker_ID");
            this.Property(t => t.Alt_Phone_Number).HasColumnName("Alt_Phone_Number");
            this.Property(t => t.Email_Address_2).HasColumnName("Email_Address_2");
            this.Property(t => t.Lead_Quality).HasColumnName("Lead_Quality");
            this.Property(t => t.Status_Changed_Date).HasColumnName("Status_Changed_Date");
            this.Property(t => t.Agent_Assigned_Date).HasColumnName("Agent_Assigned_Date");
            this.Property(t => t.Individual_Client_ID).HasColumnName("Individual_Client_ID");
            this.Property(t => t.Other_Key).HasColumnName("Other_Key");
            this.Property(t => t.Uses_Tobacco).HasColumnName("Uses_Tobacco");
            this.Property(t => t.Buying_Period_Effective_Date).HasColumnName("Buying_Period_Effective_Date");
            this.Property(t => t.Eligibility_Date).HasColumnName("Eligibility_Date");
            this.Property(t => t.Original_Start_Date).HasColumnName("Original_Start_Date");
            this.Property(t => t.Record_Eligibility_ID).HasColumnName("Record_Eligibility_ID");
            this.Property(t => t.Original_Agent_ID).HasColumnName("Original_Agent_ID");
            this.Property(t => t.CurrentSituation).HasColumnName("CurrentSituation");
            this.Property(t => t.Record_Current_Situation_ID).HasColumnName("Record_Current_Situation_ID");
            this.Property(t => t.RequestedStartDate).HasColumnName("RequestedStartDate");
            this.Property(t => t.ProductsOfInterest).HasColumnName("ProductsOfInterest");
            this.Property(t => t.Products_Of_Interest_ID).HasColumnName("Products_Of_Interest_ID");
            this.Property(t => t.Subsidy_ID).HasColumnName("Subsidy_ID");
            this.Property(t => t.Web_Lead_ID).HasColumnName("Web_Lead_ID");
            this.Property(t => t.Opt_Out_Newsletter).HasColumnName("Opt_Out_Newsletter");
            this.Property(t => t.Medicare_FirstName).HasColumnName("Medicare_FirstName");
            this.Property(t => t.Medicare_InitialName).HasColumnName("Medicare_InitialName");
            this.Property(t => t.Medicare_LastName).HasColumnName("Medicare_LastName");
            this.Property(t => t.Medical_Carrier).HasColumnName("Medical_Carrier");
            this.Property(t => t.Drug_Plan_Co).HasColumnName("Drug_Plan_Co");
            this.Property(t => t.Drug_Plan_Original_Start_Date).HasColumnName("Drug_Plan_Original_Start_Date");
            this.Property(t => t.Current_Tobacco_Use_Status_ID).HasColumnName("Current_Tobacco_Use_Status_ID");
            this.Property(t => t.Past_Tobacco_Use_Status_Id).HasColumnName("Past_Tobacco_Use_Status_Id");
            this.Property(t => t.Mailing_Address_1).HasColumnName("Mailing_Address_1");
            this.Property(t => t.Mailing_Address_2).HasColumnName("Mailing_Address_2");
            this.Property(t => t.Mailing_City).HasColumnName("Mailing_City");
            this.Property(t => t.Mailing_State_ID).HasColumnName("Mailing_State_ID");
            this.Property(t => t.Mailing_ZipCode).HasColumnName("Mailing_ZipCode");
            this.Property(t => t.Mailing_County).HasColumnName("Mailing_County");
            this.Property(t => t.IsMailing_Address).HasColumnName("IsMailing_Address");
            this.Property(t => t.Medicare_Address).HasColumnName("Medicare_Address");
            this.Property(t => t.Occupation).HasColumnName("Occupation");
            this.Property(t => t.Work_Phone_Ext).HasColumnName("Work_Phone_Ext");
            this.Property(t => t.NickName).HasColumnName("NickName");
            this.Property(t => t.Contact_First_Name).HasColumnName("Contact_First_Name");
            this.Property(t => t.Contact_Middle_Initial).HasColumnName("Contact_Middle_Initial");
            this.Property(t => t.Contact_Last_Name).HasColumnName("Contact_Last_Name");
            this.Property(t => t.Contact_Suffix).HasColumnName("Contact_Suffix");
            this.Property(t => t.Last_Accessed).HasColumnName("Last_Accessed");
            this.Property(t => t.Contact_Preference_Type_Id).HasColumnName("Contact_Preference_Type_Id");
            this.Property(t => t.Call_Preference_Time).HasColumnName("Call_Preference_Time");
            this.Property(t => t.Call_Preference_Number_Id).HasColumnName("Call_Preference_Number_Id");
            this.Property(t => t.MedicareGovDate).HasColumnName("MedicareGovDate");
            this.Property(t => t.MedicareId).HasColumnName("MedicareId");
            this.Property(t => t.DrugListFlag).HasColumnName("DrugListFlag");
            this.Property(t => t.MedicareGovId).HasColumnName("MedicareGovId");

            // Relationships
            this.HasRequired(t => t.Employment_Status)
                .WithMany(t => t.Records)
                .HasForeignKey(d => d.Employment_Status_ID);
            this.HasRequired(t => t.Marital_Status)
                .WithMany(t => t.Records)
                .HasForeignKey(d => d.Marital_Status_ID);
            this.HasRequired(t => t.State)
                .WithMany(t => t.Records)
                .HasForeignKey(d => d.Alt_State_ID);
            this.HasOptional(t => t.Record_Current_Situation)
                .WithMany(t => t.Records)
                .HasForeignKey(d => d.Record_Current_Situation_ID);
            this.HasOptional(t => t.Record_Source)
                .WithMany(t => t.Records)
                .HasForeignKey(d => d.Record_Source_ID);
            this.HasRequired(t => t.Record_Type)
                .WithMany(t => t.Records)
                .HasForeignKey(d => d.Record_Type_ID);
            this.HasRequired(t => t.Source_Code)
                .WithMany(t => t.Records)
                .HasForeignKey(d => d.Source_Code_ID);
            this.HasRequired(t => t.State1)
                .WithMany(t => t.Records1)
                .HasForeignKey(d => d.State_ID);

        }
    }
}
