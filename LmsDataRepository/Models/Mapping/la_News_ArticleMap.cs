using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class la_News_ArticleMap : EntityTypeConfiguration<la_News_Article>
    {
        public la_News_ArticleMap()
        {
            // Primary Key
            this.HasKey(t => t.News_Article_ID);

            // Properties
            this.Property(t => t.Title)
                .HasMaxLength(200);

            this.Property(t => t.Description)
                .HasMaxLength(1000);

            this.Property(t => t.Link_URL)
                .HasMaxLength(500);

            this.Property(t => t.Link_Text)
                .HasMaxLength(500);

            this.Property(t => t.Logo_URL)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("la_News_Article");
            this.Property(t => t.News_Article_ID).HasColumnName("News_Article_ID");
            this.Property(t => t.Title).HasColumnName("Title");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Link_URL).HasColumnName("Link_URL");
            this.Property(t => t.Link_Text).HasColumnName("Link_Text");
            this.Property(t => t.Display_Order).HasColumnName("Display_Order");
            this.Property(t => t.Active).HasColumnName("Active");
            this.Property(t => t.News_Article_Date).HasColumnName("News_Article_Date");
            this.Property(t => t.Logo_URL).HasColumnName("Logo_URL");
        }
    }
}
