using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Plan_StateMap : EntityTypeConfiguration<Plan_State>
    {
        public Plan_StateMap()
        {
            // Primary Key
            this.HasKey(t => t.Plan_State_ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("Plan_State");
            this.Property(t => t.Plan_State_ID).HasColumnName("Plan_State_ID");
            this.Property(t => t.Plan_ID).HasColumnName("Plan_ID");
            this.Property(t => t.State_ID).HasColumnName("State_ID");
        }
    }
}
