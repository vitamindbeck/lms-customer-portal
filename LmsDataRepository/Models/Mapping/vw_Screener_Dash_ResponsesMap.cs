using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class vw_Screener_Dash_ResponsesMap : EntityTypeConfiguration<vw_Screener_Dash_Responses>
    {
        public vw_Screener_Dash_ResponsesMap()
        {
            // Primary Key
            this.HasKey(t => new { t.TimePeriod, t.Agent_ID });

            // Properties
            this.Property(t => t.TimePeriod)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.Agent_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vw_Screener_Dash_Responses");
            this.Property(t => t.MinGroupTime).HasColumnName("MinGroupTime");
            this.Property(t => t.MaxGroupTime).HasColumnName("MaxGroupTime");
            this.Property(t => t.TimePeriod).HasColumnName("TimePeriod");
            this.Property(t => t.Responses).HasColumnName("Responses");
            this.Property(t => t.Agent_ID).HasColumnName("Agent_ID");
        }
    }
}
