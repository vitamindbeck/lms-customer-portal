using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Co_Branded_SiteMap : EntityTypeConfiguration<Co_Branded_Site>
    {
        public Co_Branded_SiteMap()
        {
            // Primary Key
            this.HasKey(t => t.Co_Branded_Site_ID);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Logo_URL)
                .HasMaxLength(100);

            this.Property(t => t.Logo_Alt_Text)
                .HasMaxLength(100);

            this.Property(t => t.Title)
                .HasMaxLength(100);

            this.Property(t => t.Header_Text)
                .HasMaxLength(200);

            this.Property(t => t.Footer_Text)
                .HasMaxLength(200);

            this.Property(t => t.Copyright_Info)
                .HasMaxLength(500);

            this.Property(t => t.Phone_Number)
                .HasMaxLength(25);

            this.Property(t => t.Email_Address)
                .HasMaxLength(100);

            this.Property(t => t.Email_Text)
                .HasMaxLength(100);

            this.Property(t => t.CSS_URL)
                .HasMaxLength(100);

            this.Property(t => t.Admin_Phone_Number)
                .HasMaxLength(15);

            this.Property(t => t.Admin_Email_Address)
                .HasMaxLength(100);

            this.Property(t => t.P_CSS_URL)
                .HasMaxLength(100);

            this.Property(t => t.URL)
                .HasMaxLength(100);

            this.Property(t => t.Splash_Image_URL)
                .HasMaxLength(50);

            this.Property(t => t.Optional_Logo_URL)
                .HasMaxLength(100);

            this.Property(t => t.Optional_Splash_Image_URL)
                .HasMaxLength(100);

            this.Property(t => t.Meta_Description)
                .HasMaxLength(1000);

            this.Property(t => t.Meta_Keywords)
                .HasMaxLength(1000);

            this.Property(t => t.Contact_Phone_Number)
                .HasMaxLength(400);

            this.Property(t => t.Contact_Fax)
                .HasMaxLength(400);

            this.Property(t => t.Contact_Mail)
                .HasMaxLength(400);

            this.Property(t => t.Slogan)
                .HasMaxLength(200);

            this.Property(t => t.Splash_Image_Text)
                .HasMaxLength(200);

            this.Property(t => t.Folder_Name)
                .HasMaxLength(50);

            this.Property(t => t.Site_Header)
                .HasMaxLength(200);

            this.Property(t => t.Site_URL)
                .HasMaxLength(200);

            this.Property(t => t.Source_Number)
                .HasMaxLength(6);

            // Table & Column Mappings
            this.ToTable("Co_Branded_Site");
            this.Property(t => t.Co_Branded_Site_ID).HasColumnName("Co_Branded_Site_ID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Logo_URL).HasColumnName("Logo_URL");
            this.Property(t => t.Logo_Alt_Text).HasColumnName("Logo_Alt_Text");
            this.Property(t => t.Title).HasColumnName("Title");
            this.Property(t => t.Header_Text).HasColumnName("Header_Text");
            this.Property(t => t.Footer_Text).HasColumnName("Footer_Text");
            this.Property(t => t.Copyright_Info).HasColumnName("Copyright_Info");
            this.Property(t => t.Phone_Number).HasColumnName("Phone_Number");
            this.Property(t => t.Email_Address).HasColumnName("Email_Address");
            this.Property(t => t.Email_Text).HasColumnName("Email_Text");
            this.Property(t => t.CSS_URL).HasColumnName("CSS_URL");
            this.Property(t => t.Admin_Phone_Number).HasColumnName("Admin_Phone_Number");
            this.Property(t => t.Admin_Email_Address).HasColumnName("Admin_Email_Address");
            this.Property(t => t.P_CSS_URL).HasColumnName("P_CSS_URL");
            this.Property(t => t.URL).HasColumnName("URL");
            this.Property(t => t.Splash_Image_URL).HasColumnName("Splash_Image_URL");
            this.Property(t => t.Optional_Logo_URL).HasColumnName("Optional_Logo_URL");
            this.Property(t => t.Optional_Splash_Image_URL).HasColumnName("Optional_Splash_Image_URL");
            this.Property(t => t.Meta_Description).HasColumnName("Meta_Description");
            this.Property(t => t.Meta_Keywords).HasColumnName("Meta_Keywords");
            this.Property(t => t.Contact_Phone_Number).HasColumnName("Contact_Phone_Number");
            this.Property(t => t.Contact_Fax).HasColumnName("Contact_Fax");
            this.Property(t => t.Contact_Mail).HasColumnName("Contact_Mail");
            this.Property(t => t.Contact_In_Person).HasColumnName("Contact_In_Person");
            this.Property(t => t.What_Is).HasColumnName("What_Is");
            this.Property(t => t.Slogan).HasColumnName("Slogan");
            this.Property(t => t.Splash_Text).HasColumnName("Splash_Text");
            this.Property(t => t.Splash_Image_Text).HasColumnName("Splash_Image_Text");
            this.Property(t => t.Source_Code_ID).HasColumnName("Source_Code_ID");
            this.Property(t => t.Folder_Name).HasColumnName("Folder_Name");
            this.Property(t => t.Viewable).HasColumnName("Viewable");
            this.Property(t => t.Site_Header).HasColumnName("Site_Header");
            this.Property(t => t.Is_Partnership).HasColumnName("Is_Partnership");
            this.Property(t => t.Splash_Bottom_Text).HasColumnName("Splash_Bottom_Text");
            this.Property(t => t.Splash_Mid_Text).HasColumnName("Splash_Mid_Text");
            this.Property(t => t.Disclaimer).HasColumnName("Disclaimer");
            this.Property(t => t.Site_URL).HasColumnName("Site_URL");
            this.Property(t => t.Source_Number).HasColumnName("Source_Number");
        }
    }
}
