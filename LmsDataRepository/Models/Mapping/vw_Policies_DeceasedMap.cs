using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class vw_Policies_DeceasedMap : EntityTypeConfiguration<vw_Policies_Deceased>
    {
        public vw_Policies_DeceasedMap()
        {
            // Primary Key
            this.HasKey(t => t.Plan_Transaction_ID);

            // Properties
            this.Property(t => t.Plan_Transaction_ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("vw_Policies_Deceased");
            this.Property(t => t.Plan_Transaction_ID).HasColumnName("Plan_Transaction_ID");
            this.Property(t => t.StatusDate).HasColumnName("StatusDate");
        }
    }
}
