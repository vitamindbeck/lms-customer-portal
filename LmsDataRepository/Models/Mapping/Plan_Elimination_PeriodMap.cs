using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Plan_Elimination_PeriodMap : EntityTypeConfiguration<Plan_Elimination_Period>
    {
        public Plan_Elimination_PeriodMap()
        {
            // Primary Key
            this.HasKey(t => t.Plan_Elimination_Period_ID);

            // Properties
            this.Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Plan_Elimination_Period");
            this.Property(t => t.Plan_Elimination_Period_ID).HasColumnName("Plan_Elimination_Period_ID");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Low).HasColumnName("Low");
            this.Property(t => t.High).HasColumnName("High");
        }
    }
}
