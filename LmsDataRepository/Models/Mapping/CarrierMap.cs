using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class CarrierMap : EntityTypeConfiguration<Carrier>
    {
        public CarrierMap()
        {
            // Primary Key
            this.HasKey(t => t.Carrier_ID);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Address_1)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Address_2)
                .HasMaxLength(100);

            this.Property(t => t.City)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Zip)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.Code)
                .HasMaxLength(50);

            this.Property(t => t.CompuComp)
                .HasMaxLength(20);

            this.Property(t => t.Address_3)
                .HasMaxLength(100);

            this.Property(t => t.Insurer_Number)
                .HasMaxLength(50);

            this.Property(t => t.Contact_Name)
                .HasMaxLength(1000);

            // Table & Column Mappings
            this.ToTable("Carrier");
            this.Property(t => t.Carrier_ID).HasColumnName("Carrier_ID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Address_1).HasColumnName("Address_1");
            this.Property(t => t.Address_2).HasColumnName("Address_2");
            this.Property(t => t.City).HasColumnName("City");
            this.Property(t => t.State_ID).HasColumnName("State_ID");
            this.Property(t => t.Zip).HasColumnName("Zip");
            this.Property(t => t.Active).HasColumnName("Active");
            this.Property(t => t.About_Carrier).HasColumnName("About_Carrier");
            this.Property(t => t.Need_Upload).HasColumnName("Need_Upload");
            this.Property(t => t.Service_Level).HasColumnName("Service_Level");
            this.Property(t => t.Service_Level_Description).HasColumnName("Service_Level_Description");
            this.Property(t => t.Assets).HasColumnName("Assets");
            this.Property(t => t.Code).HasColumnName("Code");
            this.Property(t => t.CompuComp).HasColumnName("CompuComp");
            this.Property(t => t.GBS_Admin_ID).HasColumnName("GBS_Admin_ID");
            this.Property(t => t.Address_3).HasColumnName("Address_3");
            this.Property(t => t.Group_ID).HasColumnName("Group_ID");
            this.Property(t => t.Insurer_Number).HasColumnName("Insurer_Number");
            this.Property(t => t.Contact_Name).HasColumnName("Contact_Name");
            this.Property(t => t.LTC).HasColumnName("LTC");
        }
    }
}
