using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Auto_EmailMap : EntityTypeConfiguration<Auto_Email>
    {
        public Auto_EmailMap()
        {
            // Primary Key
            this.HasKey(t => t.Auto_Email_ID);

            // Properties
            this.Property(t => t.Source_Code_IDs)
                .HasMaxLength(500);

            this.Property(t => t.Subject)
                .IsRequired()
                .HasMaxLength(500);

            this.Property(t => t.Body)
                .IsRequired();

            this.Property(t => t.From)
                .HasMaxLength(500);

            this.Property(t => t.Source_Number)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Notes)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Auto_Email");
            this.Property(t => t.Auto_Email_ID).HasColumnName("Auto_Email_ID");
            this.Property(t => t.Source_Code_ID).HasColumnName("Source_Code_ID");
            this.Property(t => t.Source_Code_IDs).HasColumnName("Source_Code_IDs");
            this.Property(t => t.Subject).HasColumnName("Subject");
            this.Property(t => t.Body).HasColumnName("Body");
            this.Property(t => t.From).HasColumnName("From");
            this.Property(t => t.HTML).HasColumnName("HTML");
            this.Property(t => t.Include_Agent).HasColumnName("Include_Agent");
            this.Property(t => t.Active).HasColumnName("Active");
            this.Property(t => t.Source_Number).HasColumnName("Source_Number");
            this.Property(t => t.Notes).HasColumnName("Notes");
            this.Property(t => t.LMS_Display).HasColumnName("LMS_Display");
        }
    }
}
