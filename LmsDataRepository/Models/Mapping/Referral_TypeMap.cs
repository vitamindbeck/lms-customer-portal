using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Referral_TypeMap : EntityTypeConfiguration<Referral_Type>
    {
        public Referral_TypeMap()
        {
            // Primary Key
            this.HasKey(t => t.Referral_Type_ID);

            // Properties
            this.Property(t => t.Referral_Description)
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("Referral_Type");
            this.Property(t => t.Referral_Type_ID).HasColumnName("Referral_Type_ID");
            this.Property(t => t.Referral_Description).HasColumnName("Referral_Description");
            this.Property(t => t.Display_Order).HasColumnName("Display_Order");
            this.Property(t => t.Active).HasColumnName("Active");
        }
    }
}
