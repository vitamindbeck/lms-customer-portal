using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class LetterMap : EntityTypeConfiguration<Letter>
    {
        public LetterMap()
        {
            // Primary Key
            this.HasKey(t => t.Letter_ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("Letter");
            this.Property(t => t.Letter_ID).HasColumnName("Letter_ID");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Body).HasColumnName("Body");
            this.Property(t => t.Last_Updated).HasColumnName("Last_Updated");
            this.Property(t => t.Active).HasColumnName("Active");
            this.Property(t => t.Display_Order).HasColumnName("Display_Order");
        }
    }
}
