using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class Plan_Transaction_HistoryMap : EntityTypeConfiguration<Plan_Transaction_History>
    {
        public Plan_Transaction_HistoryMap()
        {
            // Primary Key
            this.HasKey(t => t.Plan_Transaction_History_ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("Plan_Transaction_History");
            this.Property(t => t.Plan_Transaction_History_ID).HasColumnName("Plan_Transaction_History_ID");
            this.Property(t => t.Plan_Transaction_ID).HasColumnName("Plan_Transaction_ID");
            this.Property(t => t.Case_Manager_ID).HasColumnName("Case_Manager_ID");
            this.Property(t => t.Created_On).HasColumnName("Created_On");
        }
    }
}
