using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LmsDataRepository.Models.Mapping
{
    public class UserMap : EntityTypeConfiguration<User>
    {
        public UserMap()
        {
            // Primary Key
            this.HasKey(t => t.User_ID);

            // Properties
            this.Property(t => t.First_Name)
                .IsRequired()
                .HasMaxLength(32);

            this.Property(t => t.Last_Name)
                .IsRequired()
                .HasMaxLength(32);

            this.Property(t => t.User_Name)
                .IsRequired()
                .HasMaxLength(32);

            this.Property(t => t.Password)
                .IsRequired()
                .HasMaxLength(32);

            this.Property(t => t.Phone)
                .HasMaxLength(32);

            this.Property(t => t.Extension)
                .HasMaxLength(8);

            this.Property(t => t.Email_Address)
                .HasMaxLength(100);

            this.Property(t => t.Recommend_Line)
                .HasMaxLength(200);

            this.Property(t => t.SessionId)
                .HasMaxLength(88);

            // Table & Column Mappings
            this.ToTable("User");
            this.Property(t => t.User_ID).HasColumnName("User_ID");
            this.Property(t => t.First_Name).HasColumnName("First_Name");
            this.Property(t => t.Last_Name).HasColumnName("Last_Name");
            this.Property(t => t.User_Name).HasColumnName("User_Name");
            this.Property(t => t.Password).HasColumnName("Password");
            this.Property(t => t.Group_ID).HasColumnName("Group_ID");
            this.Property(t => t.Security_Level_ID).HasColumnName("Security_Level_ID");
            this.Property(t => t.Phone).HasColumnName("Phone");
            this.Property(t => t.Extension).HasColumnName("Extension");
            this.Property(t => t.Email_Address).HasColumnName("Email_Address");
            this.Property(t => t.Active).HasColumnName("Active");
            this.Property(t => t.Recommend_Line).HasColumnName("Recommend_Line");
            this.Property(t => t.Auto_Assignment).HasColumnName("Auto_Assignment");
            this.Property(t => t.Auto_Assignment_Life).HasColumnName("Auto_Assignment_Life");
            this.Property(t => t.Auto_Assignment_Pointer).HasColumnName("Auto_Assignment_Pointer");
            this.Property(t => t.Auto_Assignment_Life_Pointer).HasColumnName("Auto_Assignment_Life_Pointer");
            this.Property(t => t.Assignment_Indicator).HasColumnName("Assignment_Indicator");
            this.Property(t => t.Assign_To).HasColumnName("Assign_To");
            this.Property(t => t.Auto_Territory_Indicator).HasColumnName("Auto_Territory_Indicator");
            this.Property(t => t.Lead_Cap).HasColumnName("Lead_Cap");
            this.Property(t => t.Sales_Group_ID).HasColumnName("Sales_Group_ID");
            this.Property(t => t.Online_Status_ID).HasColumnName("Online_Status_ID");
            this.Property(t => t.SessionId).HasColumnName("SessionId");
            this.Property(t => t.In_AD).HasColumnName("In_AD");
            this.Property(t => t.Pod_ID).HasColumnName("Pod_ID");
            this.Property(t => t.GBS_User_ID).HasColumnName("GBS_User_ID");
            this.Property(t => t.Employee).HasColumnName("Employee");
        }
    }
}
