using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class BB_Record
    {
        public int Record_ID { get; set; }
        public int BB_Record_ID { get; set; }
        public System.DateTime Created_On { get; set; }
    }
}
