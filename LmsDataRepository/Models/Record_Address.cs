using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Record_Address
    {
        public int Record_Address_ID { get; set; }
        public int Record_ID { get; set; }
        public string Address_Line_1 { get; set; }
        public string Address_Line_2 { get; set; }
        public string City { get; set; }
        public Nullable<int> State_ID { get; set; }
        public string Zipcode { get; set; }
        public string County { get; set; }
        public bool IsMailingAddress { get; set; }
        public string Header { get; set; }
    }
}
