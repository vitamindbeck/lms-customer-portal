using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Document_Form
    {
        public int Document_Form_ID { get; set; }
        public int Document_ID { get; set; }
        public string Table_Name { get; set; }
        public int Display_Order { get; set; }
        public string Join_Name { get; set; }
        public string Alias { get; set; }
        public string Join_Statement { get; set; }
    }
}
