using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Assignment_Transaction
    {
        public int Assignment_Transaction_ID { get; set; }
        public int Client_ID { get; set; }
        public int Previous_Agent_ID { get; set; }
        public int Current_Agent_ID { get; set; }
        public System.DateTime Transaction_Date { get; set; }
    }
}
