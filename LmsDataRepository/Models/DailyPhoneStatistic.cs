using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class DailyPhoneStatistic
    {
        public DailyPhoneStatistic()
        {
            this.DailyPhoneStatisticsDetails = new List<DailyPhoneStatisticsDetail>();
        }

        public int DailyPhoneStatisticsID { get; set; }
        public System.DateTime ReportDate { get; set; }
        public System.DateTime RunTime { get; set; }
        public virtual DailyPhoneStatistic DailyPhoneStatistics1 { get; set; }
        public virtual DailyPhoneStatistic DailyPhoneStatistic1 { get; set; }
        public virtual ICollection<DailyPhoneStatisticsDetail> DailyPhoneStatisticsDetails { get; set; }
    }
}
