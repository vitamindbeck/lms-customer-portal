using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class LTCQ_Client_To_Record
    {
        public int Record_ID { get; set; }
        public decimal Client_Number { get; set; }
    }
}
