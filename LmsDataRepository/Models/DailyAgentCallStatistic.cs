using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class DailyAgentCallStatistic
    {
        public int DailyAgentCallStatisticsID { get; set; }
        public System.DateTime Start_Time { get; set; }
        public string Direction { get; set; }
        public string Phone { get; set; }
        public Nullable<bool> Answered { get; set; }
        public string First_Rang_ext { get; set; }
        public string Last_Rang_ext { get; set; }
        public string Answer_ext { get; set; }
        public string Finished_ext { get; set; }
        public string Call_Time { get; set; }
        public Nullable<int> Call_Time_Second { get; set; }
        public string Talk_Time { get; set; }
        public Nullable<int> Talk_Time_Second { get; set; }
        public string Did_Digits { get; set; }
        public Nullable<short> Call_Segment { get; set; }
        public System.DateTime Created_On { get; set; }
    }
}
