using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class vw_Screener_Dash_Uncontacted_Data
    {
        public string Count_Date { get; set; }
        public Nullable<int> IA_UL { get; set; }
        public Nullable<int> IA_UL_E { get; set; }
        public Nullable<int> IA_UL_NE { get; set; }
        public Nullable<int> Call_Status_Total { get; set; }
        public Nullable<int> Call_Status_E { get; set; }
        public Nullable<int> Call_Status_NE { get; set; }
        public Nullable<int> Contacted_Total { get; set; }
        public Nullable<int> Contacted_E { get; set; }
        public Nullable<int> Contacted_NE { get; set; }
        public Nullable<int> Live_Transfer_Total { get; set; }
        public Nullable<int> Live_Transfer_E { get; set; }
        public Nullable<int> Live_Transfer_NE { get; set; }
        public Nullable<int> Set_Appointment_Totals { get; set; }
        public Nullable<int> Set_Appointment_E { get; set; }
        public Nullable<int> Set_Apointment_NE { get; set; }
        public Nullable<int> Follow_Up_Screener_Total { get; set; }
        public Nullable<int> Follow_Up_Screener_E { get; set; }
        public Nullable<int> Follow_Up_Screener_NE { get; set; }
        public Nullable<int> Momentum { get; set; }
        public int Agent_ID { get; set; }
    }
}
