using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Record_Current_Situation
    {
        public Record_Current_Situation()
        {
            this.Records = new List<Record>();
        }

        public int Record_Current_Situation_ID { get; set; }
        public string Record_Current_Situation_Desc { get; set; }
        public bool Active { get; set; }
        public short Order { get; set; }
        public virtual ICollection<Record> Records { get; set; }
    }
}
