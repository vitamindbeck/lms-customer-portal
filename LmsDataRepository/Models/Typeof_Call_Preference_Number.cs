using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Typeof_Call_Preference_Number
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
