using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class BB_Plan_Transaction
    {
        public int BB_Plan_Transaction_ID { get; set; }
        public int Plan_Transaction_ID { get; set; }
        public System.DateTime Created_On { get; set; }
    }
}
