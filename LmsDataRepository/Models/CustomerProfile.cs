using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class CustomerProfile
    {
        public int Id { get; set; }
        public int RecordId { get; set; }
        public Nullable<int> TypeofHealthId { get; set; }
        public Nullable<int> TypeofWealthId { get; set; }
        public Nullable<int> TypeofDecisionId { get; set; }
        public Nullable<int> TypeofRiskId { get; set; }
        public Nullable<int> TypeofCostSensitivityId { get; set; }
        public Nullable<int> TypeofProviderPreferenceId { get; set; }
        public Nullable<int> TravelerCommentsNoteId { get; set; }
        public Nullable<int> BrandPreferenceCommentsNoteId { get; set; }
        public Nullable<int> FutureHealthConcernsCommentsNoteId { get; set; }
        public virtual Note Note { get; set; }
        public virtual Note Note1 { get; set; }
        public virtual TypeofNote TypeofNote { get; set; }
        public virtual TypeofCostSensitivity TypeofCostSensitivity { get; set; }
        public virtual TypeofDecision TypeofDecision { get; set; }
        public virtual TypeofHealth TypeofHealth { get; set; }
        public virtual TypeofProviderPreference TypeofProviderPreference { get; set; }
        public virtual TypeofRisk TypeofRisk { get; set; }
        public virtual TypeofWealth TypeofWealth { get; set; }
    }
}
