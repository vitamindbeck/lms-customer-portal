using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class TypeofHealth
    {
        public TypeofHealth()
        {
            this.CustomerProfiles = new List<CustomerProfile>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public virtual ICollection<CustomerProfile> CustomerProfiles { get; set; }
    }
}
