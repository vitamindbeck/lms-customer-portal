using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Employment_Status
    {
        public Employment_Status()
        {
            this.Records = new List<Record>();
        }

        public int Employment_Status_ID { get; set; }
        public string Description { get; set; }
        public int Display_Order { get; set; }
        public virtual ICollection<Record> Records { get; set; }
    }
}
