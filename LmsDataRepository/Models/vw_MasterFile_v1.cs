using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class vw_MasterFile_v1
    {
        public string GBSPolicyID { get; set; }
        public string LMSID { get; set; }
        public string AgentGroup { get; set; }
        public string Original_Agent { get; set; }
        public string Current_Record_Agent { get; set; }
        public string Plan_Agent { get; set; }
        public string Agent_Final { get; set; }
        public string Agent_Status { get; set; }
        public string Agent_Type { get; set; }
        public string Product_Type { get; set; }
        public string PolicyNumber { get; set; }
        public string SourceNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Partners { get; set; }
        public Nullable<System.DateTime> EffectiveDateOrPurchaseDate { get; set; }
        public string Plan_Type { get; set; }
        public string Carrier { get; set; }
        public string Plan_Name { get; set; }
        public string Plan_Selected { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public Nullable<int> Age { get; set; }
        public string Customer_Address { get; set; }
        public string Customer_City { get; set; }
        public string Customer_State { get; set; }
        public string Customer_Zip { get; set; }
        public string Customer_County { get; set; }
        public string Customer_Email { get; set; }
        public string Category_Driver { get; set; }
        public Nullable<int> Category_Driver_Desc { get; set; }
        public string Priority { get; set; }
        public string LMS_Current_Disposition_Status { get; set; }
        public Nullable<System.DateTime> NextFollowUp { get; set; }
        public string NextFollowUpType { get; set; }
        public string NextFollowUpFor { get; set; }
        public string LatestActivityType { get; set; }
        public string LatestActivityAssignedTo { get; set; }
        public Nullable<System.DateTime> LatestActivityDate { get; set; }
        public string LMS_Current_Record_Status { get; set; }
    }
}
