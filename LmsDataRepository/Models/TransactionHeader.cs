using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class TransactionHeader
    {
        public Nullable<int> AdministratorID { get; set; }
        public Nullable<decimal> AmountEntered { get; set; }
        public Nullable<decimal> CheckAdjustment { get; set; }
        public Nullable<decimal> CheckAmount { get; set; }
        public string CheckNumber { get; set; }
        public Nullable<System.DateTime> DateCheckReceived { get; set; }
        public Nullable<System.DateTime> DateTimeLastModified { get; set; }
        public Nullable<int> GAID { get; set; }
        public string ImportFlag { get; set; }
        public Nullable<byte> IsBalanced { get; set; }
        public Nullable<byte> IsPosted { get; set; }
        public string KeyField { get; set; }
        public Nullable<short> NumberOfItems { get; set; }
        public int TransactionID { get; set; }
        public Nullable<int> UserID { get; set; }
    }
}
