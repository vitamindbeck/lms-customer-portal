using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Certification_Status_Lookup
    {
        public int Certification_Status_ID { get; set; }
        public string Certification_Status { get; set; }
    }
}
