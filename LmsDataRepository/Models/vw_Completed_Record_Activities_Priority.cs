using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class vw_Completed_Record_Activities_Priority
    {
        public int Record_ID { get; set; }
        public int Record_Activity_ID { get; set; }
        public int Plan_Transaction_ID { get; set; }
        public Nullable<int> Priority_Level { get; set; }
    }
}
