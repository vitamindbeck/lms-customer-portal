using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Activity_Type
    {
        public int Activity_Type_ID { get; set; }
        public string Description { get; set; }
        public int Assign_To { get; set; }
        public int Active_Current { get; set; }
        public int Active_Followup { get; set; }
        public int Active_Edit { get; set; }
    }
}
