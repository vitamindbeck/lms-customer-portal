using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Record
    {
        public Record()
        {
            this.Record_Activity = new List<Record_Activity>();
        }

        public int Record_ID { get; set; }
        public string Customer_ID { get; set; }
        public Nullable<int> Salutation_ID { get; set; }
        public string First_Name { get; set; }
        public string Middle_Initial { get; set; }
        public string Last_Name { get; set; }
        public string Suffix { get; set; }
        public string Address_1 { get; set; }
        public string Address_2 { get; set; }
        public string City { get; set; }
        public int State_ID { get; set; }
        public string County { get; set; }
        public string Zipcode { get; set; }
        public string Alt_Address_1 { get; set; }
        public string Alt_Address_2 { get; set; }
        public string Alt_City { get; set; }
        public int Alt_State_ID { get; set; }
        public string Alt_County { get; set; }
        public string Alt_Zipcode { get; set; }
        public Nullable<System.DateTime> Alt_Address_From_Date { get; set; }
        public Nullable<System.DateTime> Alt_Address_To_Date { get; set; }
        public string Home_Phone { get; set; }
        public string Work_Phone { get; set; }
        public string Cell_Phone { get; set; }
        public string Fax { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public Nullable<int> Height { get; set; }
        public Nullable<int> Weight { get; set; }
        public string Email_Address { get; set; }
        public bool Imported { get; set; }
        public Nullable<System.DateTime> Initial_Contact_Date { get; set; }
        public bool Current_Tobacco_Use { get; set; }
        public int Employment_Status_ID { get; set; }
        public int Source_Code_ID { get; set; }
        public int Marital_Status_ID { get; set; }
        public string Sex { get; set; }
        public byte[] SSN { get; set; }
        public int Client_Type_ID { get; set; }
        public Nullable<decimal> Annual_Income { get; set; }
        public int Agent_ID { get; set; }
        public int Assistant_Agent_ID { get; set; }
        public string Reffered_By { get; set; }
        public byte[] Medicare_Number { get; set; }
        public Nullable<bool> Part_A_Effective { get; set; }
        public Nullable<bool> Part_B_Effective { get; set; }
        public Nullable<System.DateTime> Part_A_Effective_Date { get; set; }
        public Nullable<System.DateTime> Part_B_Effective_Date { get; set; }
        public bool Currently_Insured { get; set; }
        public Nullable<int> Current_Insured_Type_ID { get; set; }
        public string Current_Insured_Desc { get; set; }
        public byte[] Drug_List_ID { get; set; }
        public int Partner_ID { get; set; }
        public Nullable<bool> Momentum { get; set; }
        public Nullable<System.DateTime> Last_Updated_Date { get; set; }
        public bool Do_Not_Contact { get; set; }
        public Nullable<bool> Opt_Out_Email { get; set; }
        public Nullable<System.DateTime> Created_On { get; set; }
        public Nullable<int> Record_Source_ID { get; set; }
        public int Record_Type_ID { get; set; }
        public Nullable<System.DateTime> Follow_Up_Date { get; set; }
        public int Buying_Period_ID { get; set; }
        public int Buying_Period_Sub_ID { get; set; }
        public string Buying_Period_Notes { get; set; }
        public int Priority_Level { get; set; }
        public byte[] Notes { get; set; }
        public int Company_ID { get; set; }
        public bool Unsubscribed { get; set; }
        public bool B2E { get; set; }
        public int Occupation_ID { get; set; }
        public Nullable<int> Age { get; set; }
        public Nullable<int> Current_Tobacco_ID { get; set; }
        public int Referral_Type_ID { get; set; }
        public bool Past_Tobacco_Use { get; set; }
        public int Past_Tobacco_ID { get; set; }
        public string Quit_Date { get; set; }
        public string Alt_Email_Address { get; set; }
        public string Alt_First_Name { get; set; }
        public string Alt_Last_Name { get; set; }
        public int Broker_ID { get; set; }
        public string Alt_Phone_Number { get; set; }
        public string Email_Address_2 { get; set; }
        public string Lead_Quality { get; set; }
        public Nullable<System.DateTime> Status_Changed_Date { get; set; }
        public Nullable<System.DateTime> Agent_Assigned_Date { get; set; }
        public Nullable<int> Individual_Client_ID { get; set; }
        public string Other_Key { get; set; }
        public int Uses_Tobacco { get; set; }
        public Nullable<System.DateTime> Buying_Period_Effective_Date { get; set; }
        public Nullable<System.DateTime> Eligibility_Date { get; set; }
        public Nullable<System.DateTime> Original_Start_Date { get; set; }
        public Nullable<int> Record_Eligibility_ID { get; set; }
        public Nullable<int> Original_Agent_ID { get; set; }
        public string CurrentSituation { get; set; }
        public Nullable<int> Record_Current_Situation_ID { get; set; }
        public Nullable<System.DateTime> RequestedStartDate { get; set; }
        public string ProductsOfInterest { get; set; }
        public Nullable<int> Products_Of_Interest_ID { get; set; }
        public Nullable<int> Subsidy_ID { get; set; }
        public Nullable<int> Web_Lead_ID { get; set; }
        public bool Opt_Out_Newsletter { get; set; }
        public string Medicare_FirstName { get; set; }
        public string Medicare_InitialName { get; set; }
        public string Medicare_LastName { get; set; }
        public string Medical_Carrier { get; set; }
        public string Drug_Plan_Co { get; set; }
        public Nullable<System.DateTime> Drug_Plan_Original_Start_Date { get; set; }
        public Nullable<int> Current_Tobacco_Use_Status_ID { get; set; }
        public Nullable<int> Past_Tobacco_Use_Status_Id { get; set; }
        public string Mailing_Address_1 { get; set; }
        public string Mailing_Address_2 { get; set; }
        public string Mailing_City { get; set; }
        public Nullable<int> Mailing_State_ID { get; set; }
        public string Mailing_ZipCode { get; set; }
        public string Mailing_County { get; set; }
        public Nullable<bool> IsMailing_Address { get; set; }
        public Nullable<bool> Medicare_Address { get; set; }
        public string Occupation { get; set; }
        public string Work_Phone_Ext { get; set; }
        public string NickName { get; set; }
        public string Contact_First_Name { get; set; }
        public string Contact_Middle_Initial { get; set; }
        public string Contact_Last_Name { get; set; }
        public string Contact_Suffix { get; set; }
        public Nullable<System.DateTime> Last_Accessed { get; set; }
        public Nullable<int> Contact_Preference_Type_Id { get; set; }
        public Nullable<System.TimeSpan> Call_Preference_Time { get; set; }
        public Nullable<int> Call_Preference_Number_Id { get; set; }
        public Nullable<System.DateTime> MedicareGovDate { get; set; }
        public string MedicareId { get; set; }
        public Nullable<System.DateTime> DrugListFlag { get; set; }
        public string MedicareGovId { get; set; }
        public virtual Employment_Status Employment_Status { get; set; }
        public virtual Marital_Status Marital_Status { get; set; }
        public virtual ICollection<Record_Activity> Record_Activity { get; set; }
        public virtual State State { get; set; }
        public virtual Record_Current_Situation Record_Current_Situation { get; set; }
        public virtual Record_Source Record_Source { get; set; }
        public virtual Record_Type Record_Type { get; set; }
        public virtual Source_Code Source_Code { get; set; }
        public virtual State State1 { get; set; }
    }
}
