using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class CategoryDriver
    {
        public int CategoryDriverID { get; set; }
        public string CategoryDriver1 { get; set; }
        public string CategoryDriverDesc { get; set; }
        public string CategoryDriverDetails { get; set; }
        public Nullable<int> CategoryPriorityLevel { get; set; }
        public Nullable<int> CategoryDisplayOrder { get; set; }
        public Nullable<bool> CategoryActive { get; set; }
    }
}
