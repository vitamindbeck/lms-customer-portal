using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Marketing_Group
    {
        public int Marketing_Group_ID { get; set; }
        public string Description { get; set; }
    }
}
