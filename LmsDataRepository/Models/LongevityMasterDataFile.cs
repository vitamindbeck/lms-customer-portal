using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class LongevityMasterDataFile
    {
        public int MasterFileID { get; set; }
        public Nullable<System.DateTime> CreatedOnDate { get; set; }
        public Nullable<int> GBSPolicyID { get; set; }
        public Nullable<int> LMSRecordID { get; set; }
        public Nullable<int> CategoryDriverID { get; set; }
        public Nullable<int> AgentFinalUserID { get; set; }
        public Nullable<int> Priority { get; set; }
        public Nullable<int> CategorySituationID { get; set; }
    }
}
