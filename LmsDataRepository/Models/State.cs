using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class State
    {
        public State()
        {
            this.Records = new List<Record>();
            this.Records1 = new List<Record>();
        }

        public int State_ID { get; set; }
        public string Name { get; set; }
        public string Abbreviation { get; set; }
        public bool Partnership { get; set; }
        public bool Web_Quote { get; set; }
        public bool Odd_Pay { get; set; }
        public virtual ICollection<Record> Records { get; set; }
        public virtual ICollection<Record> Records1 { get; set; }
    }
}
