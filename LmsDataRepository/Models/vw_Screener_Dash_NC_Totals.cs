using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class vw_Screener_Dash_NC_Totals
    {
        public string Count_Date { get; set; }
        public Nullable<int> IA_UL { get; set; }
        public Nullable<int> Call_Status_Total { get; set; }
        public Nullable<int> Contacted_Total { get; set; }
        public Nullable<int> Live_Transfer_Total { get; set; }
        public Nullable<int> Set_Appointment_Totals { get; set; }
        public Nullable<int> Follow_Up_Screener_Total { get; set; }
        public Nullable<int> Momentum { get; set; }
        public int Agent_ID { get; set; }
    }
}
