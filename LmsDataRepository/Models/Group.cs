using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Group
    {
        public int Group_ID { get; set; }
        public string Description { get; set; }
        public string Active { get; set; }
    }
}
