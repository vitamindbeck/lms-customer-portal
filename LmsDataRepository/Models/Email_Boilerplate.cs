using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Email_Boilerplate
    {
        public int Email_Boilerplate_ID { get; set; }
        public string Body_Text { get; set; }
        public string Body_HTML { get; set; }
        public string Subject { get; set; }
        public Nullable<int> Email_Number { get; set; }
    }
}
