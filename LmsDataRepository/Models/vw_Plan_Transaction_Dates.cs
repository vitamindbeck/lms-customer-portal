using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class vw_Plan_Transaction_Dates
    {
        public int Plan_Transaction_ID { get; set; }
        public Nullable<System.DateTime> PolicyPlacedDate { get; set; }
        public Nullable<System.DateTime> ApplicationRecievedDate { get; set; }
        public Nullable<System.DateTime> CoverageEffectiveDate { get; set; }
        public Nullable<System.DateTime> PolicyDeliveredDate { get; set; }
        public Nullable<System.DateTime> ApplicationSubmittedDate { get; set; }
    }
}
