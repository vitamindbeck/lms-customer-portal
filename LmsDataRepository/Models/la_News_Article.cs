using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class la_News_Article
    {
        public int News_Article_ID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Link_URL { get; set; }
        public string Link_Text { get; set; }
        public int Display_Order { get; set; }
        public bool Active { get; set; }
        public Nullable<System.DateTime> News_Article_Date { get; set; }
        public string Logo_URL { get; set; }
    }
}
