using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Company
    {
        public int Company_ID { get; set; }
        public string Name { get; set; }
        public string Address_1 { get; set; }
        public string Address_2 { get; set; }
        public string City { get; set; }
        public int State_ID { get; set; }
        public string Zipcode { get; set; }
        public string Company_President { get; set; }
        public string Company_Phone { get; set; }
        public string Company_Size { get; set; }
        public string Primary_Email { get; set; }
        public int Industry_ID { get; set; }
        public string Primary_Contact { get; set; }
        public string Title { get; set; }
        public string Email_Address { get; set; }
        public string Primary_Phone { get; set; }
        public string Web_Address { get; set; }
    }
}
