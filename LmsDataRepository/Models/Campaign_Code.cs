using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Campaign_Code
    {
        public Campaign_Code()
        {
            this.Record_Activity = new List<Record_Activity>();
        }

        public int Campaign_Code_ID { get; set; }
        public string Campaign_Number { get; set; }
        public string Campaign_Description { get; set; }
        public Nullable<System.DateTime> Campaign_Start_Date { get; set; }
        public Nullable<bool> Active { get; set; }
        public virtual ICollection<Record_Activity> Record_Activity { get; set; }
    }
}
