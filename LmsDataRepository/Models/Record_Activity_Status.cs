using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Record_Activity_Status
    {
        public Record_Activity_Status()
        {
            this.Record_Activity = new List<Record_Activity>();
        }

        public int Record_Activity_Status_ID { get; set; }
        public string Description { get; set; }
        public int Display_Order { get; set; }
        public int Active { get; set; }
        public virtual ICollection<Record_Activity> Record_Activity { get; set; }
    }
}
