using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Carrier_State_Authorized
    {
        public int Carrier_State_Authorized_ID { get; set; }
        public int State_ID { get; set; }
        public int Carrier_ID { get; set; }
        public System.DateTime Created_On { get; set; }
    }
}
