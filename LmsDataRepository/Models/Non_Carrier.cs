using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Non_Carrier
    {
        public int Non_Carrier_ID { get; set; }
        public string Non_Carrier_Name { get; set; }
        public string Web_Link { get; set; }
    }
}
