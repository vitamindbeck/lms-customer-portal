using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Payment_Method
    {
        public int Payment_Method_ID { get; set; }
        public string Description { get; set; }
        public string Abbreviation { get; set; }
        public bool Include_Renewal { get; set; }
        public int Weight { get; set; }
        public bool Default { get; set; }
        public bool Active { get; set; }
    }
}
