using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class vw_Master_List_AEP2012
    {
        public Nullable<int> PolicyID { get; set; }
        public int CustomerNumber { get; set; }
        public Nullable<int> Record_ID { get; set; }
        public Nullable<int> Plan_Transaction_ID { get; set; }
        public string FirstAndMiddleName { get; set; }
        public string LastName { get; set; }
        public string Customer_ID { get; set; }
        public string AgentGroup { get; set; }
        public string GBS_Agent { get; set; }
        public string LMSRecAgent { get; set; }
        public string LMSPolicyAgent { get; set; }
        public string Product_Type { get; set; }
        public string PolicyNumber { get; set; }
        public string LMS_Partner { get; set; }
        public Nullable<System.DateTime> EffectiveDateOrPurchaseDate { get; set; }
        public Nullable<int> plantypeModernStandard { get; set; }
        public string Carrier { get; set; }
        public string PlanName { get; set; }
        public string PlanSelected { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public Nullable<int> Age { get; set; }
        public string Address1 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string County { get; set; }
        public string EMailAddress { get; set; }
        public Nullable<int> PriorityStatus { get; set; }
        public string Home_Phone { get; set; }
        public string Work_Phone { get; set; }
    }
}
