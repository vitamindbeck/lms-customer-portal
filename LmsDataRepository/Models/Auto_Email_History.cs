using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Auto_Email_History
    {
        public int Auto_Email_History_ID { get; set; }
        public int Auto_Email_ID { get; set; }
        public string To_Address { get; set; }
        public Nullable<System.DateTime> Sent_Date { get; set; }
        public bool Success { get; set; }
        public Nullable<int> Record_ID { get; set; }
    }
}
