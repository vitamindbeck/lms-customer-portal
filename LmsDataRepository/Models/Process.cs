using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Process
    {
        public int Process_ID { get; set; }
        public Nullable<int> Process_Type_ID { get; set; }
        public Nullable<int> Value { get; set; }
        public string OtherValue { get; set; }
        public Nullable<System.DateTime> Process_Date { get; set; }
    }
}
