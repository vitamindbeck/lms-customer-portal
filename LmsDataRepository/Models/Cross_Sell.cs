using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Cross_Sell
    {
        public int Cross_Sell_ID { get; set; }
        public int Client_ID { get; set; }
        public int Group_ID { get; set; }
        public int Rank { get; set; }
    }
}
