using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class DailyPhoneStatisticsDetail
    {
        public int DailyPhoneStatisticsDetailsID { get; set; }
        public int DailyPhoneStatisticsID { get; set; }
        public int AgentID { get; set; }
        public string AgentName { get; set; }
        public int CallsIn { get; set; }
        public int CallsInCompleted { get; set; }
        public int CallsOut { get; set; }
        public int CallsOutCompleted { get; set; }
        public int CallsHandled { get; set; }
        public int CallsAbandoned { get; set; }
        public int TotalTalkTime { get; set; }
        public int InTalkTime { get; set; }
        public int OutTalkTime { get; set; }
        public int AvgTalkTime { get; set; }
        public int TimeOnDuty { get; set; }
        public double PercentFree { get; set; }
        public double PercectBusy { get; set; }
        public double PrecentDND { get; set; }
        public int DNDTime { get; set; }
        public int AvgCallTime { get; set; }
        public virtual DailyPhoneStatistic DailyPhoneStatistic { get; set; }
    }
}
