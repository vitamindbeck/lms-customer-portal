using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class vw_Follow_Up_Live_Transfer_Attempts
    {
        public int Record_Activity_ID { get; set; }
        public int Record_ID { get; set; }
        public int Activity_Type_ID { get; set; }
        public Nullable<System.DateTime> Due_Date { get; set; }
        public Nullable<System.DateTime> Actual_Date { get; set; }
        public System.DateTime Created_On { get; set; }
        public int Created_By { get; set; }
        public int User_ID { get; set; }
        public byte[] Notes { get; set; }
        public bool Made_Contact { get; set; }
        public int Record_Activity_Status_ID { get; set; }
        public int Plan_Transaction_ID { get; set; }
        public int Priority_Level { get; set; }
        public Nullable<int> Auto_Email_ID { get; set; }
    }
}
