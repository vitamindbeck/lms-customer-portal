using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Current_Insured_Type
    {
        public int Current_Insured_Type_ID { get; set; }
        public string Current_Insured { get; set; }
        public bool Active { get; set; }
        public int Display_Order { get; set; }
    }
}
