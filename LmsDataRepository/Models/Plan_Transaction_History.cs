using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Plan_Transaction_History
    {
        public int Plan_Transaction_History_ID { get; set; }
        public Nullable<int> Plan_Transaction_ID { get; set; }
        public Nullable<int> Case_Manager_ID { get; set; }
        public Nullable<System.DateTime> Created_On { get; set; }
    }
}
