using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class vw_Record_Activity_Initial_Attempt
    {
        public int Record_ID { get; set; }
        public Nullable<int> Initial_Attempt_RA_ID { get; set; }
    }
}
