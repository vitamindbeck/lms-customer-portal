using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class DailyDNDStatistic
    {
        public int DailyDNDStatisticsID { get; set; }
        public Nullable<int> Device { get; set; }
        public string AgentName { get; set; }
        public Nullable<int> CallsHandled { get; set; }
        public Nullable<int> CallsIn { get; set; }
        public string TotalCallInTime { get; set; }
        public Nullable<int> CallsOut { get; set; }
        public string TotalCallOutTime { get; set; }
        public string AfterCallWork { get; set; }
        public string Break { get; set; }
        public string Lunch { get; set; }
        public string Meeting { get; set; }
        public string DataEntry { get; set; }
        public string Training { get; set; }
        public string Supervisor { get; set; }
        public string Personal { get; set; }
        public string OutBound { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string TotalTimeOnDuty { get; set; }
        public string TimeInDND { get; set; }
    }
}
