using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class User_Password
    {
        public int User_Password_ID { get; set; }
        public Nullable<int> Appointment_ID { get; set; }
        public Nullable<int> License_ID { get; set; }
        public Nullable<int> Certification_ID { get; set; }
        public Nullable<int> Agent_ID { get; set; }
        public Nullable<int> State_ID { get; set; }
        public Nullable<int> Non_Carrier_ID { get; set; }
        public byte[] User_Name { get; set; }
        public byte[] Password { get; set; }
        public Nullable<int> Carrier_ID { get; set; }
        public string Carrier_Site_Link { get; set; }
        public Nullable<System.DateTime> Last_Updated_Date { get; set; }
        public Nullable<int> Updated_By { get; set; }
    }
}
