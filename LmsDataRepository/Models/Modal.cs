using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Modal
    {
        public int Modal_ID { get; set; }
        public string Description { get; set; }
        public int Display_Order { get; set; }
        public bool Active { get; set; }
        public Nullable<int> Pay_Period { get; set; }
        public Nullable<int> GBS_ID { get; set; }
    }
}
