using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class DailyAgentCallDetail
    {
        public int DailyAgentCallDetailsID { get; set; }
        public System.DateTime Status_Changed_On { get; set; }
        public string Agent_ext { get; set; }
        public string Logged_In_ext { get; set; }
        public string Agent_Status { get; set; }
        public string Hunt_Group { get; set; }
        public System.DateTime Created_On { get; set; }
    }
}
