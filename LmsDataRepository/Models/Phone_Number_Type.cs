using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Phone_Number_Type
    {
        public int Phone_Number_Type_ID { get; set; }
        public string Description { get; set; }
    }
}
