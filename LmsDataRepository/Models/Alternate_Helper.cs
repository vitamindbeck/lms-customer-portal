using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Alternate_Helper
    {
        public int Alternate_Helper_ID { get; set; }
        public bool Alternate_Indicator { get; set; }
        public string Description { get; set; }
    }
}
