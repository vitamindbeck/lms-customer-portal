using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class User
    {
        public User()
        {
            this.Agents = new List<Agent>();
            this.Record_Activity = new List<Record_Activity>();
            this.User_Security_Level = new List<User_Security_Level>();
        }

        public int User_ID { get; set; }
        public string First_Name { get; set; }
        public string Last_Name { get; set; }
        public string User_Name { get; set; }
        public string Password { get; set; }
        public int Group_ID { get; set; }
        public int Security_Level_ID { get; set; }
        public string Phone { get; set; }
        public string Extension { get; set; }
        public string Email_Address { get; set; }
        public Nullable<bool> Active { get; set; }
        public string Recommend_Line { get; set; }
        public bool Auto_Assignment { get; set; }
        public bool Auto_Assignment_Life { get; set; }
        public bool Auto_Assignment_Pointer { get; set; }
        public bool Auto_Assignment_Life_Pointer { get; set; }
        public bool Assignment_Indicator { get; set; }
        public int Assign_To { get; set; }
        public bool Auto_Territory_Indicator { get; set; }
        public int Lead_Cap { get; set; }
        public Nullable<int> Sales_Group_ID { get; set; }
        public Nullable<short> Online_Status_ID { get; set; }
        public string SessionId { get; set; }
        public bool In_AD { get; set; }
        public Nullable<int> Pod_ID { get; set; }
        public Nullable<int> GBS_User_ID { get; set; }
        public Nullable<bool> Employee { get; set; }
        public virtual ICollection<Agent> Agents { get; set; }
        public virtual ICollection<Record_Activity> Record_Activity { get; set; }
        public virtual ICollection<User_Security_Level> User_Security_Level { get; set; }
    }
}
