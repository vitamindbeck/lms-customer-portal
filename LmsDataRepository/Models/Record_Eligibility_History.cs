using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Record_Eligibility_History
    {
        public int Record_Eligibility_History_ID { get; set; }
        public int Record_ID { get; set; }
        public int Record_Eligibility_ID_Old { get; set; }
        public int Record_Eligibility_ID_New { get; set; }
        public System.DateTime Created_On { get; set; }
    }
}
