using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Longevity_Manual_MasterFile_DataSet
    {
        public int MasterFileID { get; set; }
        public Nullable<System.DateTime> Insertion_Date { get; set; }
        public string PolicyID { get; set; }
        public string LMSID { get; set; }
        public string Agent_Group { get; set; }
        public string Sales_Agent { get; set; }
        public string Agent_Working_Status { get; set; }
        public string Agent_Clean { get; set; }
        public string Assigned_Agent { get; set; }
        public string Agent_Final { get; set; }
        public string Product_Type { get; set; }
        public string Policy_Number { get; set; }
        public string SourceCode { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string Partners { get; set; }
        public string Policy_Effective_Date { get; set; }
        public string Plan_Type { get; set; }
        public string Carrier { get; set; }
        public string Plan_Name { get; set; }
        public string Plan_Selected { get; set; }
        public string DOB { get; set; }
        public string Age { get; set; }
        public string Customer_Address { get; set; }
        public string Customer_City { get; set; }
        public string Customer_State { get; set; }
        public string Customer_Zip { get; set; }
        public string Customer_County { get; set; }
        public string Customer_Email { get; set; }
        public string CategoryDriver { get; set; }
        public string Losin { get; set; }
        public string Priority { get; set; }
        public string DispositionStatus { get; set; }
        public string NextFollowUp { get; set; }
        public string CurrentRecordStatus { get; set; }
    }
}
