using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Subsidy
    {
        public int Subsidy_ID { get; set; }
        public string Subsidy_Desc { get; set; }
        public bool Active { get; set; }
        public int Display_Order { get; set; }
    }
}
