using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class vw_Screener_Dash_Record_Type_Stats
    {
        public int Grp_ID { get; set; }
        public string Count_Date { get; set; }
        public Nullable<int> Disqualified { get; set; }
        public Nullable<int> Duplicates { get; set; }
        public Nullable<int> Prospects { get; set; }
        public Nullable<int> Suspects { get; set; }
        public Nullable<int> Leads { get; set; }
        public Nullable<int> EN { get; set; }
        public Nullable<int> E30 { get; set; }
        public Nullable<int> E60 { get; set; }
        public Nullable<int> E90 { get; set; }
        public Nullable<int> E901 { get; set; }
        public Nullable<int> EU { get; set; }
        public Nullable<int> NE { get; set; }
        public Nullable<int> Uncontacted { get; set; }
        public Nullable<int> Unattempted { get; set; }
        public int Agent_ID { get; set; }
    }
}
