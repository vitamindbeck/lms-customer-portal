using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Correspondance_Transaction
    {
        public int Correspondence_ID { get; set; }
        public int Record_ID { get; set; }
        public int User_ID { get; set; }
        public Nullable<int> Letter_ID { get; set; }
        public Nullable<System.DateTime> Sent_Date { get; set; }
    }
}
