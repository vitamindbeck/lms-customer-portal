using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Plan_Daily_Benefit
    {
        public int Plan_Daily_Benefit_ID { get; set; }
        public int Plan_ID { get; set; }
        public Nullable<int> Low_Range { get; set; }
        public Nullable<int> High_Range { get; set; }
        public string Description { get; set; }
        public string HC_Description { get; set; }
        public Nullable<int> Start_Age { get; set; }
        public Nullable<int> End_Age { get; set; }
    }
}
