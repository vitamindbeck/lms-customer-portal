using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class vw_Screener_Dash_Totals
    {
        public string TimePeriod { get; set; }
        public Nullable<int> Made_Contact_Under_5 { get; set; }
        public Nullable<int> Made_Contact_5to15 { get; set; }
        public Nullable<int> Made_Contact_Over_15 { get; set; }
        public Nullable<int> Initial_Attempt_Under_5 { get; set; }
        public Nullable<int> Initial_Attempt_5to15 { get; set; }
        public Nullable<int> Initial_Attempt_Over_15 { get; set; }
        public Nullable<int> Live_Transfer_Under_5 { get; set; }
        public Nullable<int> Live_Transfer_5to15 { get; set; }
        public Nullable<int> Live_Transfer_Over_15 { get; set; }
        public Nullable<int> No_Contact_Under_5 { get; set; }
        public Nullable<int> No_Contact_5to15 { get; set; }
        public Nullable<int> No_Contact_Over_15 { get; set; }
        public Nullable<int> Live_Transfer_Attempt_Under_5 { get; set; }
        public Nullable<int> Live_Transfer_Attempt_5to15 { get; set; }
        public Nullable<int> Live_Transfer_Attempt_15 { get; set; }
        public Nullable<int> Contacted_Set_Follow_Up_Under_5 { get; set; }
        public Nullable<int> Contacted_Set_Follow_Up_5to15 { get; set; }
        public Nullable<int> Contacted_Set_Follow_Up_15 { get; set; }
        public Nullable<int> Responses { get; set; }
        public Nullable<int> Expr1 { get; set; }
    }
}
