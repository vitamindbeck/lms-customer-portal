using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Plan_Transaction_Date
    {
        public int Plan_Transaction_Date_ID { get; set; }
        public int Plan_Transaction_ID { get; set; }
        public int Transaction_Date_Type_ID { get; set; }
        public Nullable<System.DateTime> Date_Value { get; set; }
    }
}
