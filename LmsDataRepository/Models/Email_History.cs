using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Email_History
    {
        public int Email_History_ID { get; set; }
        public int Record_ID { get; set; }
        public string Customer_ID { get; set; }
        public Nullable<int> Email_Boilerplate_ID { get; set; }
        public Nullable<System.DateTime> Sent_Date { get; set; }
        public string Email_Address { get; set; }
    }
}
