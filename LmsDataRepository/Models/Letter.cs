using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Letter
    {
        public int Letter_ID { get; set; }
        public string Description { get; set; }
        public string Body { get; set; }
        public Nullable<System.DateTime> Last_Updated { get; set; }
        public Nullable<bool> Active { get; set; }
        public Nullable<int> Display_Order { get; set; }
    }
}
