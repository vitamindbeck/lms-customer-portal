using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Category_Situation
    {
        public int CategorySituationID { get; set; }
        public string CategorySituation { get; set; }
        public string CategorySituationDesc { get; set; }
    }
}
