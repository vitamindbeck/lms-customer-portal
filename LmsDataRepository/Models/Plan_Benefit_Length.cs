using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Plan_Benefit_Length
    {
        public int Plan_Benefit_Length_ID { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
    }
}
