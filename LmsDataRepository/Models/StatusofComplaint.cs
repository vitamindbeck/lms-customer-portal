using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class StatusofComplaint
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}
