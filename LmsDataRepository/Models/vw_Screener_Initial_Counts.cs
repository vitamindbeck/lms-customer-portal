using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class vw_Screener_Initial_Counts
    {
        public string Count_Date { get; set; }
        public string timeperiod { get; set; }
        public string Status { get; set; }
        public string Responses { get; set; }
        public string InitialAttempts { get; set; }
        public Nullable<int> NoContacts { get; set; }
        public string MadeContact { get; set; }
        public Nullable<int> LiveTransfers { get; set; }
        public Nullable<int> Live_Transfer_Attempt { get; set; }
        public Nullable<int> SetFollowUpScreener { get; set; }
        public int Agent_ID { get; set; }
    }
}
