using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class vw_Agent_Call_Statistics_Thirty_Min_Totals_Out
    {
        public string answer_ext { get; set; }
        public Nullable<int> CallTime { get; set; }
        public Nullable<System.DateTime> MinTime { get; set; }
        public Nullable<System.DateTime> MaxTime { get; set; }
        public string Direction { get; set; }
    }
}
