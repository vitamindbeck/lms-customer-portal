using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Plan_old
    {
        public int Plan_ID { get; set; }
        public string Name { get; set; }
        public int Carrier_ID { get; set; }
        public bool Active { get; set; }
        public string About { get; set; }
        public string CompuProd { get; set; }
        public float Modal_Factor_Semiannual { get; set; }
        public float Modal_Factor_Quarterly { get; set; }
        public float Modal_Factor_Monthly { get; set; }
        public float Modal_Factor_EFT { get; set; }
        public bool Nearest_Age { get; set; }
        public Nullable<int> GBS_ProductID { get; set; }
    }
}
