using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Record_Activity_Note
    {
        public Record_Activity_Note()
        {
            this.Record_Activity = new List<Record_Activity>();
        }

        public int Record_Activity_Note_ID { get; set; }
        public string Description { get; set; }
        public int User_ID { get; set; }
        public bool Active { get; set; }
        public int Record_Activity_Note_Type { get; set; }
        public int Assign_To { get; set; }
        public virtual ICollection<Record_Activity> Record_Activity { get; set; }
    }
}
