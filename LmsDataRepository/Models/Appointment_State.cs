using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Appointment_State
    {
        public int Appointment_ID { get; set; }
        public int State_ID { get; set; }
        public Nullable<System.DateTime> Created_On { get; set; }
    }
}
