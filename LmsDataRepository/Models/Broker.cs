using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Broker
    {
        public int Broker_ID { get; set; }
        public string First_Name { get; set; }
        public string Address_1 { get; set; }
        public string Address_2 { get; set; }
        public string City { get; set; }
        public int State_ID { get; set; }
        public string Zipcode { get; set; }
        public string Email_Address { get; set; }
        public string Last_Name { get; set; }
    }
}
