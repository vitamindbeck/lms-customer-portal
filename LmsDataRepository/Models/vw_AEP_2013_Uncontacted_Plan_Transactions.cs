using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class vw_AEP_2013_Uncontacted_Plan_Transactions
    {
        public int Plan_Transaction_ID { get; set; }
        public string Uncontacted { get; set; }
        public int Expr1 { get; set; }
    }
}
