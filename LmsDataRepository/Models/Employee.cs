using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Employee
    {
        public int Employee_ID { get; set; }
        public string First_Name { get; set; }
        public string Last_Name { get; set; }
        public string User_Name { get; set; }
        public string Password { get; set; }
        public int Group_ID { get; set; }
        public int Security_Level_ID { get; set; }
        public string Phone { get; set; }
        public string Extension { get; set; }
        public string Email_Address { get; set; }
        public Nullable<bool> Active { get; set; }
        public string Recommend_Line { get; set; }
        public bool Assignment_Indicator { get; set; }
        public int Assign_To { get; set; }
        public bool Auto_Territory_Indicator { get; set; }
        public int Lead_Cap { get; set; }
    }
}
