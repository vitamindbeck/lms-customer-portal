using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class vw_MasterFile_v2
    {
        public int MasterFileID { get; set; }
        public string GBSPolicyID { get; set; }
        public string LMSID { get; set; }
        public string AgentGroup { get; set; }
        public Nullable<int> Original_Agent { get; set; }
        public Nullable<int> Current_Record_Agent { get; set; }
        public Nullable<int> Plan_Agent { get; set; }
        public string Agent_Final { get; set; }
        public string Agent_Status { get; set; }
        public string Agent_Type { get; set; }
        public string Product_Type { get; set; }
        public string PolicyNumber { get; set; }
        public string SourceNumber { get; set; }
        public Nullable<int> Source_Code_ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Partners { get; set; }
        public Nullable<System.DateTime> EffectiveDateOrPurchaseDate { get; set; }
        public string Plan_Type { get; set; }
        public Nullable<int> Carrier { get; set; }
        public Nullable<int> CarrierID { get; set; }
        public string Plan_Name { get; set; }
        public Nullable<int> ProductTypeID { get; set; }
        public string Plan_Selected { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public Nullable<int> Age { get; set; }
        public string Customer_Address { get; set; }
        public string Customer_City { get; set; }
        public string Customer_State { get; set; }
        public string Customer_Zip { get; set; }
        public string Customer_County { get; set; }
        public string Customer_Email { get; set; }
        public string Category_Driver { get; set; }
        public string Category_Driver_Desc { get; set; }
        public int Priority { get; set; }
        public string LMS_Current_Disposition_Status { get; set; }
        public Nullable<System.DateTime> NextFollowUp { get; set; }
        public Nullable<int> NextFollowUpType { get; set; }
        public Nullable<int> NextFollowUpFor { get; set; }
        public Nullable<int> LatestActivityType { get; set; }
        public string LatestActivityAssignedTo { get; set; }
        public Nullable<System.DateTime> LatestActivityDate { get; set; }
        public string LMS_Current_Record_Status { get; set; }
        public Nullable<int> AgentFinalUserID { get; set; }
        public Nullable<int> DOB_Month { get; set; }
        public Nullable<int> Record_Type_ID { get; set; }
        public string CategorySituation { get; set; }
        public string CategorySituationDesc { get; set; }
        public Nullable<int> CategorySituationID { get; set; }
        public Nullable<int> Record_ID { get; set; }
        public Nullable<int> Plan_Transaction_ID { get; set; }
        public int Plan_Transaction_Status_ID { get; set; }
        public string Plan_Transaction_Status_Description { get; set; }
        public int EffectiveDateOrPurchaseDateMonth { get; set; }
    }
}
