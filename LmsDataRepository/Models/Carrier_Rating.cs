using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Carrier_Rating
    {
        public int Carrier_Rating_ID { get; set; }
        public int Carrier_ID { get; set; }
        public int Rating_Company_ID { get; set; }
        public string Rating { get; set; }
        public Nullable<System.DateTime> Rating_Date { get; set; }
        public string Description { get; set; }
    }
}
