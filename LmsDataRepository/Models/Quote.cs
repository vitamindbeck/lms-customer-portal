using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Quote
    {
        public int Quote_ID { get; set; }
        public int Record_ID { get; set; }
        public string Quote_Number { get; set; }
        public int Quote_Elimination_Period_ID { get; set; }
        public int Quote_Benefit_Length_ID { get; set; }
        public int Quote_Inflation_Protection_ID { get; set; }
        public int Quote_Health_Code_ID { get; set; }
        public decimal Daily_Max { get; set; }
        public bool Nonforfeiture { get; set; }
        public System.DateTime Created_On { get; set; }
        public int Quote_Method_ID { get; set; }
        public decimal Per_Application_Cap { get; set; }
        public int Number_Of_Quotes { get; set; }
    }
}
