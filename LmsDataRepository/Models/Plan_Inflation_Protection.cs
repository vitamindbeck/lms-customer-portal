using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Plan_Inflation_Protection
    {
        public int Plan_Inflation_Protection_ID { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
