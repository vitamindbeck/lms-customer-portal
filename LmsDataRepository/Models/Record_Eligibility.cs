using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Record_Eligibility
    {
        public int Record_Eligibility_ID { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
        public bool Active { get; set; }
        public Nullable<short> Order { get; set; }
    }
}
