using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Source_Code
    {
        public Source_Code()
        {
            this.Records = new List<Record>();
        }

        public int Source_Code_ID { get; set; }
        public string Source_Number { get; set; }
        public int Group_ID { get; set; }
        public string Description { get; set; }
        public Nullable<System.DateTime> Launch_Date { get; set; }
        public int Affinity_Partner_ID { get; set; }
        public bool Paid { get; set; }
        public Nullable<decimal> Cost { get; set; }
        public string Notes { get; set; }
        public bool Web_Select { get; set; }
        public bool Active { get; set; }
        public int Marketing_Group_ID { get; set; }
        public Nullable<int> Media_Type_ID { get; set; }
        public Nullable<int> Circulation { get; set; }
        public bool Allow_Mailing { get; set; }
        public bool LTC { get; set; }
        public string Source_Phone_Number { get; set; }
        public virtual ICollection<Record> Records { get; set; }
    }
}
