using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Plan_Document
    {
        public int Plan_Document_ID { get; set; }
        public int Plan_ID { get; set; }
        public int State_ID { get; set; }
        public string Title { get; set; }
        public string Location { get; set; }
        public bool Active { get; set; }
        public int Display_Order { get; set; }
        public Nullable<int> Parent_Plan_Document_ID { get; set; }
        public Nullable<int> User_ID { get; set; }
    }
}
