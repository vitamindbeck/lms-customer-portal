using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Underwriting_Class
    {
        public int Underwriting_ID { get; set; }
        public string Description { get; set; }
        public Nullable<int> IsRemovable { get; set; }
        public int Display_Order { get; set; }
        public bool Active { get; set; }
    }
}
