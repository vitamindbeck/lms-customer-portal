using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Campaign_History
    {
        public int Campaign_History_ID { get; set; }
        public int Campaign_Code_ID { get; set; }
        public int Record_ID { get; set; }
        public Nullable<System.DateTime> Date_Added { get; set; }
    }
}
