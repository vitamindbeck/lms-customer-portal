using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class TransactionAll
    {
        public Nullable<int> AdjustmentItemID { get; set; }
        public Nullable<decimal> AdminCheckAmount { get; set; }
        public string AdminCheckNumber { get; set; }
        public Nullable<int> AdministratorID { get; set; }
        public Nullable<int> AgentID { get; set; }
        public Nullable<decimal> AmountEntered { get; set; }
        public string BillingPeriod { get; set; }
        public Nullable<System.DateTime> BillingPeriodDate { get; set; }
        public Nullable<decimal> CheckAdjustment { get; set; }
        public Nullable<decimal> CheckAmount { get; set; }
        public Nullable<int> CheckNumber { get; set; }
        public string Comment { get; set; }
        public Nullable<int> CommissionType { get; set; }
        public Nullable<System.DateTime> DateCheckReceived { get; set; }
        public Nullable<int> GAID { get; set; }
        public Nullable<byte> IsBalanced { get; set; }
        public Nullable<byte> IsPosted { get; set; }
        public Nullable<byte> IsUnPosted { get; set; }
        public Nullable<short> NumberOfItems { get; set; }
        public Nullable<int> NumberOfLives { get; set; }
        public Nullable<int> PayingAgencyID { get; set; }
        public Nullable<decimal> PaymentAmount { get; set; }
        public int PaymentItemID { get; set; }
        public Nullable<byte> PaymentType { get; set; }
        public int PolicyID { get; set; }
        public Nullable<System.DateTime> PostedDate { get; set; }
        public Nullable<decimal> PremiumAmount { get; set; }
        public Nullable<float> RecordAmount { get; set; }
        public string RecordType { get; set; }
        public Nullable<byte> Report1099 { get; set; }
        public Nullable<int> SplitID { get; set; }
        public int TransactionID { get; set; }
        public int TransactionPaymentID { get; set; }
        public string Comment2 { get; set; }
        public Nullable<byte> IsNoCheck { get; set; }
    }
}
