using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Typeof_Contact_Preference_Type
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
