using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Quote_Benefit_Length
    {
        public int Quote_Benefit_Length_ID { get; set; }
        public string Description { get; set; }
        public int Monthly_Factor { get; set; }
    }
}
