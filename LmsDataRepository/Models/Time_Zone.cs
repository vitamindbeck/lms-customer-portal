using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Time_Zone
    {
        public string State { get; set; }
        public string Zone { get; set; }
        public System.DateTime Plus { get; set; }
        public Nullable<int> Plus_hour { get; set; }
    }
}
