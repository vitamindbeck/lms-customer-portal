using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class vw_Record_Activity_Phone_Counts
    {
        public int Record_ID { get; set; }
        public Nullable<System.DateTime> Created_On { get; set; }
        public Nullable<int> Call_1 { get; set; }
        public Nullable<int> Call_2 { get; set; }
        public Nullable<int> Call_3 { get; set; }
        public Nullable<int> Call_4 { get; set; }
        public Nullable<int> Call_5 { get; set; }
    }
}
