using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using LmsDataRepository.Models.Mapping;

namespace LmsDataRepository.Models
{
    public partial class LMSContext : DbContext
    {
        static LMSContext()
        {
            Database.SetInitializer<LMSContext>(null);
        }

        public LMSContext()
            : base("Name=LMSContext")
        {
        }

        public DbSet<Activity_Type> Activity_Type { get; set; }
        public DbSet<AdminDocumentationRating> AdminDocumentationRatings { get; set; }
        public DbSet<Affinity_Partner> Affinity_Partner { get; set; }
        public DbSet<Agent> Agents { get; set; }
        public DbSet<Agent_Type_Lookup> Agent_Type_Lookup { get; set; }
        public DbSet<Alternate_Helper> Alternate_Helper { get; set; }
        public DbSet<Appointment> Appointments { get; set; }
        public DbSet<Appointment_State> Appointment_State { get; set; }
        public DbSet<aspnet_Applications> aspnet_Applications { get; set; }
        public DbSet<aspnet_Membership> aspnet_Membership { get; set; }
        public DbSet<aspnet_Paths> aspnet_Paths { get; set; }
        public DbSet<aspnet_PersonalizationAllUsers> aspnet_PersonalizationAllUsers { get; set; }
        public DbSet<aspnet_PersonalizationPerUser> aspnet_PersonalizationPerUser { get; set; }
        public DbSet<aspnet_Profile> aspnet_Profile { get; set; }
        public DbSet<aspnet_Roles> aspnet_Roles { get; set; }
        public DbSet<aspnet_SchemaVersions> aspnet_SchemaVersions { get; set; }
        public DbSet<aspnet_Users> aspnet_Users { get; set; }
        public DbSet<Assignment_Transaction> Assignment_Transaction { get; set; }
        public DbSet<Auto_Email> Auto_Email { get; set; }
        public DbSet<Auto_Email_History> Auto_Email_History { get; set; }
        public DbSet<BB_Plan_Transaction> BB_Plan_Transaction { get; set; }
        public DbSet<BB_Record> BB_Record { get; set; }
        public DbSet<Broker> Brokers { get; set; }
        public DbSet<Buying_Period> Buying_Period { get; set; }
        public DbSet<Buying_Period_Sub> Buying_Period_Sub { get; set; }
        public DbSet<CallRating> CallRatings { get; set; }
        public DbSet<Campaign_Code> Campaign_Code { get; set; }
        public DbSet<Campaign_History> Campaign_History { get; set; }
        public DbSet<Carrier> Carriers { get; set; }
        public DbSet<Carrier_Phone_Number> Carrier_Phone_Number { get; set; }
        public DbSet<Carrier_Rating> Carrier_Rating { get; set; }
        public DbSet<Carrier_State_Authorized> Carrier_State_Authorized { get; set; }
        public DbSet<Category_Situation> Category_Situation { get; set; }
        public DbSet<CategoryDriver> CategoryDrivers { get; set; }
        public DbSet<Certification> Certifications { get; set; }
        public DbSet<Certification_Status_Lookup> Certification_Status_Lookup { get; set; }
        public DbSet<Certification_Type_Lookup> Certification_Type_Lookup { get; set; }
        public DbSet<Co_Branded_Site> Co_Branded_Site { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<Complaint> Complaints { get; set; }
        public DbSet<Complaint_Activity> Complaint_Activity { get; set; }
        public DbSet<Correspondance_Transaction> Correspondance_Transaction { get; set; }
        public DbSet<CriticalTopicCoverage> CriticalTopicCoverages { get; set; }
        public DbSet<Cross_Sell> Cross_Sell { get; set; }
        public DbSet<Current_Insured_Type> Current_Insured_Type { get; set; }
        public DbSet<CustomerProfile> CustomerProfiles { get; set; }
        public DbSet<Daily_Open_Case> Daily_Open_Case { get; set; }
        public DbSet<DailyAgentCallDetail> DailyAgentCallDetails { get; set; }
        public DbSet<DailyAgentCallStatistic> DailyAgentCallStatistics { get; set; }
        public DbSet<DailyDNDStatistic> DailyDNDStatistics { get; set; }
        public DbSet<DailyPhoneStatistic> DailyPhoneStatistics { get; set; }
        public DbSet<DailyPhoneStatisticsDetail> DailyPhoneStatisticsDetails { get; set; }
        public DbSet<DestinationRX_Data> DestinationRX_Data { get; set; }
        public DbSet<DestinationRx_Plan> DestinationRx_Plan { get; set; }
        public DbSet<Document> Documents { get; set; }
        public DbSet<Document_Field> Document_Field { get; set; }
        public DbSet<Document_Form> Document_Form { get; set; }
        public DbSet<Email_Agent_Code> Email_Agent_Code { get; set; }
        public DbSet<Email_Boilerplate> Email_Boilerplate { get; set; }
        public DbSet<Email_Code> Email_Code { get; set; }
        public DbSet<Email_History> Email_History { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Employment_Status> Employment_Status { get; set; }
        public DbSet<Exam_Service> Exam_Service { get; set; }
        public DbSet<Feature_Category> Feature_Category { get; set; }
        public DbSet<Glossary> Glossaries { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Group_Transaction_Date_Type> Group_Transaction_Date_Type { get; set; }
        public DbSet<Industry> Industries { get; set; }
        public DbSet<Insurance_Line_Lookup> Insurance_Line_Lookup { get; set; }
        public DbSet<la_Industry_News_Article> la_Industry_News_Article { get; set; }
        public DbSet<la_Industry_Section> la_Industry_Section { get; set; }
        public DbSet<la_Menu_Item> la_Menu_Item { get; set; }
        public DbSet<la_News_Article> la_News_Article { get; set; }
        public DbSet<la_News_Section> la_News_Section { get; set; }
        public DbSet<la_News_Section_Link> la_News_Section_Link { get; set; }
        public DbSet<la_Press_Release> la_Press_Release { get; set; }
        public DbSet<Letter> Letters { get; set; }
        public DbSet<License> Licenses { get; set; }
        public DbSet<Longevity_Manual_MasterFile_DataSet> Longevity_Manual_MasterFile_DataSet { get; set; }
        public DbSet<LongevityMasterDataFile> LongevityMasterDataFiles { get; set; }
        public DbSet<LTCQ_Client_To_Record> LTCQ_Client_To_Record { get; set; }
        public DbSet<Marital_Status> Marital_Status { get; set; }
        public DbSet<Marketing_Group> Marketing_Group { get; set; }
        public DbSet<Media_Type> Media_Type { get; set; }
        public DbSet<Modal> Modals { get; set; }
        public DbSet<Non_Carrier> Non_Carrier { get; set; }
        public DbSet<Note> Notes { get; set; }
        public DbSet<Note_Helper> Note_Helper { get; set; }
        public DbSet<Occupation> Occupations { get; set; }
        public DbSet<Online_Status> Online_Status { get; set; }
        public DbSet<Outsource> Outsources { get; set; }
        public DbSet<Payment_Method> Payment_Method { get; set; }
        public DbSet<Payment_Mode> Payment_Mode { get; set; }
        public DbSet<PDF> PDFs { get; set; }
        public DbSet<PDFField> PDFFields { get; set; }
        public DbSet<Phone_Number_Type> Phone_Number_Type { get; set; }
        public DbSet<Plan> Plans { get; set; }
        public DbSet<Plan_Benefit_Length> Plan_Benefit_Length { get; set; }
        public DbSet<Plan_Daily_Benefit> Plan_Daily_Benefit { get; set; }
        public DbSet<Plan_Document> Plan_Document { get; set; }
        public DbSet<Plan_Elimination_Period> Plan_Elimination_Period { get; set; }
        public DbSet<Plan_Inflation_Protection> Plan_Inflation_Protection { get; set; }
        public DbSet<Plan_old> Plan_old { get; set; }
        public DbSet<Plan_State> Plan_State { get; set; }
        public DbSet<Plan_Transaction> Plan_Transaction { get; set; }
        public DbSet<Plan_Transaction_Date> Plan_Transaction_Date { get; set; }
        public DbSet<Plan_Transaction_History> Plan_Transaction_History { get; set; }
        public DbSet<Plan_Transaction_Request> Plan_Transaction_Request { get; set; }
        public DbSet<Plan_Transaction_Status> Plan_Transaction_Status { get; set; }
        public DbSet<Plan_Type> Plan_Type { get; set; }
        public DbSet<Pod> Pods { get; set; }
        public DbSet<Policy> Policies { get; set; }
        public DbSet<Policy_Fee> Policy_Fee { get; set; }
        public DbSet<Process> Processes { get; set; }
        public DbSet<Process_Type> Process_Type { get; set; }
        public DbSet<Products_Of_Interest> Products_Of_Interest { get; set; }
        public DbSet<Quote> Quotes { get; set; }
        public DbSet<Quote_Benefit_Length> Quote_Benefit_Length { get; set; }
        public DbSet<Quote_Elimination_Period> Quote_Elimination_Period { get; set; }
        public DbSet<Rating_Company> Rating_Company { get; set; }
        public DbSet<Reconciliation> Reconciliations { get; set; }
        public DbSet<Reconciliation_Type> Reconciliation_Type { get; set; }
        public DbSet<Record> Records { get; set; }
        public DbSet<Record_Activity> Record_Activity { get; set; }
        public DbSet<Record_Activity_Note> Record_Activity_Note { get; set; }
        public DbSet<Record_Activity_Status> Record_Activity_Status { get; set; }
        public DbSet<Record_Address> Record_Address { get; set; }
        public DbSet<Record_Assigned> Record_Assigned { get; set; }
        public DbSet<Record_Current_Situation> Record_Current_Situation { get; set; }
        public DbSet<Record_Distribution> Record_Distribution { get; set; }
        public DbSet<Record_Distribution_History> Record_Distribution_History { get; set; }
        public DbSet<Record_Distribution_Type> Record_Distribution_Type { get; set; }
        public DbSet<Record_Eligibility> Record_Eligibility { get; set; }
        public DbSet<Record_Eligibility_History> Record_Eligibility_History { get; set; }
        public DbSet<Record_Note> Record_Note { get; set; }
        public DbSet<Record_Outsource> Record_Outsource { get; set; }
        public DbSet<Record_Source> Record_Source { get; set; }
        public DbSet<Record_Type> Record_Type { get; set; }
        public DbSet<Referral_Type> Referral_Type { get; set; }
        public DbSet<Sales_Group> Sales_Group { get; set; }
        public DbSet<SalesRating> SalesRatings { get; set; }
        public DbSet<Salutation> Salutations { get; set; }
        public DbSet<Security_Level> Security_Level { get; set; }
        public DbSet<Shipping_Method> Shipping_Method { get; set; }
        public DbSet<Source_Code> Source_Code { get; set; }
        public DbSet<State> States { get; set; }
        public DbSet<StatusofComplaint> StatusofComplaints { get; set; }
        public DbSet<Stuff> Stuffs { get; set; }
        public DbSet<Subsidy> Subsidies { get; set; }
        public DbSet<sysdiagram> sysdiagrams { get; set; }
        public DbSet<System_Lock> System_Lock { get; set; }
        public DbSet<Time_Zone> Time_Zone { get; set; }
        public DbSet<Tobacco> Tobacco { get; set; }
        public DbSet<Tobacco_Use> Tobacco_Use { get; set; }
        public DbSet<Transaction_Date_Type> Transaction_Date_Type { get; set; }
        public DbSet<TransactionAll> TransactionAlls { get; set; }
        public DbSet<TransactionHeader> TransactionHeaders { get; set; }
        public DbSet<TransactionPaymentItem> TransactionPaymentItems { get; set; }
        public DbSet<TransactionPayment> TransactionPayments { get; set; }
        public DbSet<Typeof_Call_Preference_Number> Typeof_Call_Preference_Number { get; set; }
        public DbSet<Typeof_Contact_Preference_Type> Typeof_Contact_Preference_Type { get; set; }
        public DbSet<TypeofCostSensitivity> TypeofCostSensitivities { get; set; }
        public DbSet<TypeofDecision> TypeofDecisions { get; set; }
        public DbSet<TypeofHealth> TypeofHealths { get; set; }
        public DbSet<TypeofNote> TypeofNotes { get; set; }
        public DbSet<TypeofProviderPreference> TypeofProviderPreferences { get; set; }
        public DbSet<TypeofRisk> TypeofRisks { get; set; }
        public DbSet<TypeofWealth> TypeofWealths { get; set; }
        public DbSet<Underwriting_Class> Underwriting_Class { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<User_Online_Status_History> User_Online_Status_History { get; set; }
        public DbSet<User_Password> User_Password { get; set; }
        public DbSet<User_Security_Level> User_Security_Level { get; set; }
        public DbSet<Web_Lead_Record_History> Web_Lead_Record_History { get; set; }
        public DbSet<Case_Manager> Case_Manager { get; set; }
        public DbSet<vw_AEP_2013_Uncontacted_Plan_Transactions> vw_AEP_2013_Uncontacted_Plan_Transactions { get; set; }
        public DbSet<vw_Agent_Call_Statistics> vw_Agent_Call_Statistics { get; set; }
        public DbSet<vw_Agent_Call_Statistics_Thirty_Min_Totals_In> vw_Agent_Call_Statistics_Thirty_Min_Totals_In { get; set; }
        public DbSet<vw_Agent_Call_Statistics_Thirty_Min_Totals_Out> vw_Agent_Call_Statistics_Thirty_Min_Totals_Out { get; set; }
        public DbSet<vw_App_Received_2009_Temp> vw_App_Received_2009_Temp { get; set; }
        public DbSet<vw_App_Received_2010_Temp> vw_App_Received_2010_Temp { get; set; }
        public DbSet<vw_Application_Declined_Date> vw_Application_Declined_Date { get; set; }
        public DbSet<vw_Application_Withdrawn> vw_Application_Withdrawn { get; set; }
        public DbSet<vw_Applications_Placed> vw_Applications_Placed { get; set; }
        public DbSet<vw_Applications_Received> vw_Applications_Received { get; set; }
        public DbSet<vw_Applications_Requested> vw_Applications_Requested { get; set; }
        public DbSet<vw_Applications_Sent> vw_Applications_Sent { get; set; }
        public DbSet<vw_Applications_Submitted> vw_Applications_Submitted { get; set; }
        public DbSet<vw_aspnet_Applications> vw_aspnet_Applications { get; set; }
        public DbSet<vw_aspnet_MembershipUsers> vw_aspnet_MembershipUsers { get; set; }
        public DbSet<vw_aspnet_Profiles> vw_aspnet_Profiles { get; set; }
        public DbSet<vw_aspnet_Roles> vw_aspnet_Roles { get; set; }
        public DbSet<vw_aspnet_Users> vw_aspnet_Users { get; set; }
        public DbSet<vw_aspnet_UsersInRoles> vw_aspnet_UsersInRoles { get; set; }
        public DbSet<vw_aspnet_WebPartState_Paths> vw_aspnet_WebPartState_Paths { get; set; }
        public DbSet<vw_aspnet_WebPartState_Shared> vw_aspnet_WebPartState_Shared { get; set; }
        public DbSet<vw_aspnet_WebPartState_User> vw_aspnet_WebPartState_User { get; set; }
        public DbSet<vw_Completed_Record_Activities_Priority> vw_Completed_Record_Activities_Priority { get; set; }
        public DbSet<vw_Daily_Ext_Time_Period_Counts> vw_Daily_Ext_Time_Period_Counts { get; set; }
        public DbSet<vw_Daily_Records_Assigned> vw_Daily_Records_Assigned { get; set; }
        public DbSet<vw_Follow_Up_Live_Transfer_Attempts> vw_Follow_Up_Live_Transfer_Attempts { get; set; }
        public DbSet<vw_Get_Placed_Policies> vw_Get_Placed_Policies { get; set; }
        public DbSet<vw_Last_Completed_Activity> vw_Last_Completed_Activity { get; set; }
        public DbSet<vw_LastCompletedRecordActivitybyPlanTransaction> vw_LastCompletedRecordActivitybyPlanTransaction { get; set; }
        public DbSet<vw_LMS_Open_Activities> vw_LMS_Open_Activities { get; set; }
        public DbSet<vw_Master_List_AEP2012> vw_Master_List_AEP2012 { get; set; }
        public DbSet<vw_MasterFile_v1> vw_MasterFile_v1 { get; set; }
        public DbSet<vw_MasterFile_v2> vw_MasterFile_v2 { get; set; }
        public DbSet<vw_NextFollowUpbyPlanTransaction> vw_NextFollowUpbyPlanTransaction { get; set; }
        public DbSet<vw_Plan_Transaction_Dates> vw_Plan_Transaction_Dates { get; set; }
        public DbSet<vw_Policies_Deceased> vw_Policies_Deceased { get; set; }
        public DbSet<vw_Policies_Inforce_NonCommissionable> vw_Policies_Inforce_NonCommissionable { get; set; }
        public DbSet<vw_Policies_Lapsed_Terminated> vw_Policies_Lapsed_Terminated { get; set; }
        public DbSet<vw_Policies_Placed> vw_Policies_Placed { get; set; }
        public DbSet<vw_Policy_NTO> vw_Policy_NTO { get; set; }
        public DbSet<vw_RA_Initial_Attempt_All> vw_RA_Initial_Attempt_All { get; set; }
        public DbSet<vw_Record_Activity_Initial_Attempt> vw_Record_Activity_Initial_Attempt { get; set; }
        public DbSet<vw_Record_Activity_Latest_FU> vw_Record_Activity_Latest_FU { get; set; }
        public DbSet<vw_Record_Activity_Phone_Counts> vw_Record_Activity_Phone_Counts { get; set; }
        public DbSet<vw_Record_RA_NextFU> vw_Record_RA_NextFU { get; set; }
        public DbSet<vw_Screener_Dash_Contact_Set_Follow_Up_Screener> vw_Screener_Dash_Contact_Set_Follow_Up_Screener { get; set; }
        public DbSet<vw_Screener_Dash_Contacts> vw_Screener_Dash_Contacts { get; set; }
        public DbSet<vw_Screener_Dash_Initial_Attemps> vw_Screener_Dash_Initial_Attemps { get; set; }
        public DbSet<vw_Screener_Dash_Live_Transfer_Attempt> vw_Screener_Dash_Live_Transfer_Attempt { get; set; }
        public DbSet<vw_Screener_Dash_Live_Transfers> vw_Screener_Dash_Live_Transfers { get; set; }
        public DbSet<vw_Screener_Dash_NC_Eligible> vw_Screener_Dash_NC_Eligible { get; set; }
        public DbSet<vw_Screener_Dash_NC_NotEligible> vw_Screener_Dash_NC_NotEligible { get; set; }
        public DbSet<vw_Screener_Dash_NC_Totals> vw_Screener_Dash_NC_Totals { get; set; }
        public DbSet<vw_Screener_Dash_No_Contact> vw_Screener_Dash_No_Contact { get; set; }
        public DbSet<vw_Screener_Dash_Record_Type_Stats> vw_Screener_Dash_Record_Type_Stats { get; set; }
        public DbSet<vw_Screener_Dash_Responses> vw_Screener_Dash_Responses { get; set; }
        public DbSet<vw_Screener_Dash_Totals> vw_Screener_Dash_Totals { get; set; }
        public DbSet<vw_Screener_Dash_Uncontacted_Data> vw_Screener_Dash_Uncontacted_Data { get; set; }
        public DbSet<vw_Screener_Initial_Counts> vw_Screener_Initial_Counts { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new Activity_TypeMap());
            modelBuilder.Configurations.Add(new AdminDocumentationRatingMap());
            modelBuilder.Configurations.Add(new Affinity_PartnerMap());
            modelBuilder.Configurations.Add(new AgentMap());
            modelBuilder.Configurations.Add(new Agent_Type_LookupMap());
            modelBuilder.Configurations.Add(new Alternate_HelperMap());
            modelBuilder.Configurations.Add(new AppointmentMap());
            modelBuilder.Configurations.Add(new Appointment_StateMap());
            modelBuilder.Configurations.Add(new aspnet_ApplicationsMap());
            modelBuilder.Configurations.Add(new aspnet_MembershipMap());
            modelBuilder.Configurations.Add(new aspnet_PathsMap());
            modelBuilder.Configurations.Add(new aspnet_PersonalizationAllUsersMap());
            modelBuilder.Configurations.Add(new aspnet_PersonalizationPerUserMap());
            modelBuilder.Configurations.Add(new aspnet_ProfileMap());
            modelBuilder.Configurations.Add(new aspnet_RolesMap());
            modelBuilder.Configurations.Add(new aspnet_SchemaVersionsMap());
            modelBuilder.Configurations.Add(new aspnet_UsersMap());
            modelBuilder.Configurations.Add(new Assignment_TransactionMap());
            modelBuilder.Configurations.Add(new Auto_EmailMap());
            modelBuilder.Configurations.Add(new Auto_Email_HistoryMap());
            modelBuilder.Configurations.Add(new BB_Plan_TransactionMap());
            modelBuilder.Configurations.Add(new BB_RecordMap());
            modelBuilder.Configurations.Add(new BrokerMap());
            modelBuilder.Configurations.Add(new Buying_PeriodMap());
            modelBuilder.Configurations.Add(new Buying_Period_SubMap());
            modelBuilder.Configurations.Add(new CallRatingMap());
            modelBuilder.Configurations.Add(new Campaign_CodeMap());
            modelBuilder.Configurations.Add(new Campaign_HistoryMap());
            modelBuilder.Configurations.Add(new CarrierMap());
            modelBuilder.Configurations.Add(new Carrier_Phone_NumberMap());
            modelBuilder.Configurations.Add(new Carrier_RatingMap());
            modelBuilder.Configurations.Add(new Carrier_State_AuthorizedMap());
            modelBuilder.Configurations.Add(new Category_SituationMap());
            modelBuilder.Configurations.Add(new CategoryDriverMap());
            modelBuilder.Configurations.Add(new CertificationMap());
            modelBuilder.Configurations.Add(new Certification_Status_LookupMap());
            modelBuilder.Configurations.Add(new Certification_Type_LookupMap());
            modelBuilder.Configurations.Add(new Co_Branded_SiteMap());
            modelBuilder.Configurations.Add(new CompanyMap());
            modelBuilder.Configurations.Add(new ComplaintMap());
            modelBuilder.Configurations.Add(new Complaint_ActivityMap());
            modelBuilder.Configurations.Add(new Correspondance_TransactionMap());
            modelBuilder.Configurations.Add(new CriticalTopicCoverageMap());
            modelBuilder.Configurations.Add(new Cross_SellMap());
            modelBuilder.Configurations.Add(new Current_Insured_TypeMap());
            modelBuilder.Configurations.Add(new CustomerProfileMap());
            modelBuilder.Configurations.Add(new Daily_Open_CaseMap());
            modelBuilder.Configurations.Add(new DailyAgentCallDetailMap());
            modelBuilder.Configurations.Add(new DailyAgentCallStatisticMap());
            modelBuilder.Configurations.Add(new DailyDNDStatisticMap());
            modelBuilder.Configurations.Add(new DailyPhoneStatisticMap());
            modelBuilder.Configurations.Add(new DailyPhoneStatisticsDetailMap());
            modelBuilder.Configurations.Add(new DestinationRX_DataMap());
            modelBuilder.Configurations.Add(new DestinationRx_PlanMap());
            modelBuilder.Configurations.Add(new DocumentMap());
            modelBuilder.Configurations.Add(new Document_FieldMap());
            modelBuilder.Configurations.Add(new Document_FormMap());
            modelBuilder.Configurations.Add(new Email_Agent_CodeMap());
            modelBuilder.Configurations.Add(new Email_BoilerplateMap());
            modelBuilder.Configurations.Add(new Email_CodeMap());
            modelBuilder.Configurations.Add(new Email_HistoryMap());
            modelBuilder.Configurations.Add(new EmployeeMap());
            modelBuilder.Configurations.Add(new Employment_StatusMap());
            modelBuilder.Configurations.Add(new Exam_ServiceMap());
            modelBuilder.Configurations.Add(new Feature_CategoryMap());
            modelBuilder.Configurations.Add(new GlossaryMap());
            modelBuilder.Configurations.Add(new GroupMap());
            modelBuilder.Configurations.Add(new Group_Transaction_Date_TypeMap());
            modelBuilder.Configurations.Add(new IndustryMap());
            modelBuilder.Configurations.Add(new Insurance_Line_LookupMap());
            modelBuilder.Configurations.Add(new la_Industry_News_ArticleMap());
            modelBuilder.Configurations.Add(new la_Industry_SectionMap());
            modelBuilder.Configurations.Add(new la_Menu_ItemMap());
            modelBuilder.Configurations.Add(new la_News_ArticleMap());
            modelBuilder.Configurations.Add(new la_News_SectionMap());
            modelBuilder.Configurations.Add(new la_News_Section_LinkMap());
            modelBuilder.Configurations.Add(new la_Press_ReleaseMap());
            modelBuilder.Configurations.Add(new LetterMap());
            modelBuilder.Configurations.Add(new LicenseMap());
            modelBuilder.Configurations.Add(new Longevity_Manual_MasterFile_DataSetMap());
            modelBuilder.Configurations.Add(new LongevityMasterDataFileMap());
            modelBuilder.Configurations.Add(new LTCQ_Client_To_RecordMap());
            modelBuilder.Configurations.Add(new Marital_StatusMap());
            modelBuilder.Configurations.Add(new Marketing_GroupMap());
            modelBuilder.Configurations.Add(new Media_TypeMap());
            modelBuilder.Configurations.Add(new ModalMap());
            modelBuilder.Configurations.Add(new Non_CarrierMap());
            modelBuilder.Configurations.Add(new NoteMap());
            modelBuilder.Configurations.Add(new Note_HelperMap());
            modelBuilder.Configurations.Add(new OccupationMap());
            modelBuilder.Configurations.Add(new Online_StatusMap());
            modelBuilder.Configurations.Add(new OutsourceMap());
            modelBuilder.Configurations.Add(new Payment_MethodMap());
            modelBuilder.Configurations.Add(new Payment_ModeMap());
            modelBuilder.Configurations.Add(new PDFMap());
            modelBuilder.Configurations.Add(new PDFFieldMap());
            modelBuilder.Configurations.Add(new Phone_Number_TypeMap());
            modelBuilder.Configurations.Add(new PlanMap());
            modelBuilder.Configurations.Add(new Plan_Benefit_LengthMap());
            modelBuilder.Configurations.Add(new Plan_Daily_BenefitMap());
            modelBuilder.Configurations.Add(new Plan_DocumentMap());
            modelBuilder.Configurations.Add(new Plan_Elimination_PeriodMap());
            modelBuilder.Configurations.Add(new Plan_Inflation_ProtectionMap());
            modelBuilder.Configurations.Add(new Plan_oldMap());
            modelBuilder.Configurations.Add(new Plan_StateMap());
            modelBuilder.Configurations.Add(new Plan_TransactionMap());
            modelBuilder.Configurations.Add(new Plan_Transaction_DateMap());
            modelBuilder.Configurations.Add(new Plan_Transaction_HistoryMap());
            modelBuilder.Configurations.Add(new Plan_Transaction_RequestMap());
            modelBuilder.Configurations.Add(new Plan_Transaction_StatusMap());
            modelBuilder.Configurations.Add(new Plan_TypeMap());
            modelBuilder.Configurations.Add(new PodMap());
            modelBuilder.Configurations.Add(new PolicyMap());
            modelBuilder.Configurations.Add(new Policy_FeeMap());
            modelBuilder.Configurations.Add(new ProcessMap());
            modelBuilder.Configurations.Add(new Process_TypeMap());
            modelBuilder.Configurations.Add(new Products_Of_InterestMap());
            modelBuilder.Configurations.Add(new QuoteMap());
            modelBuilder.Configurations.Add(new Quote_Benefit_LengthMap());
            modelBuilder.Configurations.Add(new Quote_Elimination_PeriodMap());
            modelBuilder.Configurations.Add(new Rating_CompanyMap());
            modelBuilder.Configurations.Add(new ReconciliationMap());
            modelBuilder.Configurations.Add(new Reconciliation_TypeMap());
            modelBuilder.Configurations.Add(new RecordMap());
            modelBuilder.Configurations.Add(new Record_ActivityMap());
            modelBuilder.Configurations.Add(new Record_Activity_NoteMap());
            modelBuilder.Configurations.Add(new Record_Activity_StatusMap());
            modelBuilder.Configurations.Add(new Record_AddressMap());
            modelBuilder.Configurations.Add(new Record_AssignedMap());
            modelBuilder.Configurations.Add(new Record_Current_SituationMap());
            modelBuilder.Configurations.Add(new Record_DistributionMap());
            modelBuilder.Configurations.Add(new Record_Distribution_HistoryMap());
            modelBuilder.Configurations.Add(new Record_Distribution_TypeMap());
            modelBuilder.Configurations.Add(new Record_EligibilityMap());
            modelBuilder.Configurations.Add(new Record_Eligibility_HistoryMap());
            modelBuilder.Configurations.Add(new Record_NoteMap());
            modelBuilder.Configurations.Add(new Record_OutsourceMap());
            modelBuilder.Configurations.Add(new Record_SourceMap());
            modelBuilder.Configurations.Add(new Record_TypeMap());
            modelBuilder.Configurations.Add(new Referral_TypeMap());
            modelBuilder.Configurations.Add(new Sales_GroupMap());
            modelBuilder.Configurations.Add(new SalesRatingMap());
            modelBuilder.Configurations.Add(new SalutationMap());
            modelBuilder.Configurations.Add(new Security_LevelMap());
            modelBuilder.Configurations.Add(new Shipping_MethodMap());
            modelBuilder.Configurations.Add(new Source_CodeMap());
            modelBuilder.Configurations.Add(new StateMap());
            modelBuilder.Configurations.Add(new StatusofComplaintMap());
            modelBuilder.Configurations.Add(new StuffMap());
            modelBuilder.Configurations.Add(new SubsidyMap());
            modelBuilder.Configurations.Add(new sysdiagramMap());
            modelBuilder.Configurations.Add(new System_LockMap());
            modelBuilder.Configurations.Add(new Time_ZoneMap());
            modelBuilder.Configurations.Add(new TobaccoMap());
            modelBuilder.Configurations.Add(new Tobacco_UseMap());
            modelBuilder.Configurations.Add(new Transaction_Date_TypeMap());
            modelBuilder.Configurations.Add(new TransactionAllMap());
            modelBuilder.Configurations.Add(new TransactionHeaderMap());
            modelBuilder.Configurations.Add(new TransactionPaymentItemMap());
            modelBuilder.Configurations.Add(new TransactionPaymentMap());
            modelBuilder.Configurations.Add(new Typeof_Call_Preference_NumberMap());
            modelBuilder.Configurations.Add(new Typeof_Contact_Preference_TypeMap());
            modelBuilder.Configurations.Add(new TypeofCostSensitivityMap());
            modelBuilder.Configurations.Add(new TypeofDecisionMap());
            modelBuilder.Configurations.Add(new TypeofHealthMap());
            modelBuilder.Configurations.Add(new TypeofNoteMap());
            modelBuilder.Configurations.Add(new TypeofProviderPreferenceMap());
            modelBuilder.Configurations.Add(new TypeofRiskMap());
            modelBuilder.Configurations.Add(new TypeofWealthMap());
            modelBuilder.Configurations.Add(new Underwriting_ClassMap());
            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new User_Online_Status_HistoryMap());
            modelBuilder.Configurations.Add(new User_PasswordMap());
            modelBuilder.Configurations.Add(new User_Security_LevelMap());
            modelBuilder.Configurations.Add(new Web_Lead_Record_HistoryMap());
            modelBuilder.Configurations.Add(new Case_ManagerMap());
            modelBuilder.Configurations.Add(new vw_AEP_2013_Uncontacted_Plan_TransactionsMap());
            modelBuilder.Configurations.Add(new vw_Agent_Call_StatisticsMap());
            modelBuilder.Configurations.Add(new vw_Agent_Call_Statistics_Thirty_Min_Totals_InMap());
            modelBuilder.Configurations.Add(new vw_Agent_Call_Statistics_Thirty_Min_Totals_OutMap());
            modelBuilder.Configurations.Add(new vw_App_Received_2009_TempMap());
            modelBuilder.Configurations.Add(new vw_App_Received_2010_TempMap());
            modelBuilder.Configurations.Add(new vw_Application_Declined_DateMap());
            modelBuilder.Configurations.Add(new vw_Application_WithdrawnMap());
            modelBuilder.Configurations.Add(new vw_Applications_PlacedMap());
            modelBuilder.Configurations.Add(new vw_Applications_ReceivedMap());
            modelBuilder.Configurations.Add(new vw_Applications_RequestedMap());
            modelBuilder.Configurations.Add(new vw_Applications_SentMap());
            modelBuilder.Configurations.Add(new vw_Applications_SubmittedMap());
            modelBuilder.Configurations.Add(new vw_aspnet_ApplicationsMap());
            modelBuilder.Configurations.Add(new vw_aspnet_MembershipUsersMap());
            modelBuilder.Configurations.Add(new vw_aspnet_ProfilesMap());
            modelBuilder.Configurations.Add(new vw_aspnet_RolesMap());
            modelBuilder.Configurations.Add(new vw_aspnet_UsersMap());
            modelBuilder.Configurations.Add(new vw_aspnet_UsersInRolesMap());
            modelBuilder.Configurations.Add(new vw_aspnet_WebPartState_PathsMap());
            modelBuilder.Configurations.Add(new vw_aspnet_WebPartState_SharedMap());
            modelBuilder.Configurations.Add(new vw_aspnet_WebPartState_UserMap());
            modelBuilder.Configurations.Add(new vw_Completed_Record_Activities_PriorityMap());
            modelBuilder.Configurations.Add(new vw_Daily_Ext_Time_Period_CountsMap());
            modelBuilder.Configurations.Add(new vw_Daily_Records_AssignedMap());
            modelBuilder.Configurations.Add(new vw_Follow_Up_Live_Transfer_AttemptsMap());
            modelBuilder.Configurations.Add(new vw_Get_Placed_PoliciesMap());
            modelBuilder.Configurations.Add(new vw_Last_Completed_ActivityMap());
            modelBuilder.Configurations.Add(new vw_LastCompletedRecordActivitybyPlanTransactionMap());
            modelBuilder.Configurations.Add(new vw_LMS_Open_ActivitiesMap());
            modelBuilder.Configurations.Add(new vw_Master_List_AEP2012Map());
            modelBuilder.Configurations.Add(new vw_MasterFile_v1Map());
            modelBuilder.Configurations.Add(new vw_MasterFile_v2Map());
            modelBuilder.Configurations.Add(new vw_NextFollowUpbyPlanTransactionMap());
            modelBuilder.Configurations.Add(new vw_Plan_Transaction_DatesMap());
            modelBuilder.Configurations.Add(new vw_Policies_DeceasedMap());
            modelBuilder.Configurations.Add(new vw_Policies_Inforce_NonCommissionableMap());
            modelBuilder.Configurations.Add(new vw_Policies_Lapsed_TerminatedMap());
            modelBuilder.Configurations.Add(new vw_Policies_PlacedMap());
            modelBuilder.Configurations.Add(new vw_Policy_NTOMap());
            modelBuilder.Configurations.Add(new vw_RA_Initial_Attempt_AllMap());
            modelBuilder.Configurations.Add(new vw_Record_Activity_Initial_AttemptMap());
            modelBuilder.Configurations.Add(new vw_Record_Activity_Latest_FUMap());
            modelBuilder.Configurations.Add(new vw_Record_Activity_Phone_CountsMap());
            modelBuilder.Configurations.Add(new vw_Record_RA_NextFUMap());
            modelBuilder.Configurations.Add(new vw_Screener_Dash_Contact_Set_Follow_Up_ScreenerMap());
            modelBuilder.Configurations.Add(new vw_Screener_Dash_ContactsMap());
            modelBuilder.Configurations.Add(new vw_Screener_Dash_Initial_AttempsMap());
            modelBuilder.Configurations.Add(new vw_Screener_Dash_Live_Transfer_AttemptMap());
            modelBuilder.Configurations.Add(new vw_Screener_Dash_Live_TransfersMap());
            modelBuilder.Configurations.Add(new vw_Screener_Dash_NC_EligibleMap());
            modelBuilder.Configurations.Add(new vw_Screener_Dash_NC_NotEligibleMap());
            modelBuilder.Configurations.Add(new vw_Screener_Dash_NC_TotalsMap());
            modelBuilder.Configurations.Add(new vw_Screener_Dash_No_ContactMap());
            modelBuilder.Configurations.Add(new vw_Screener_Dash_Record_Type_StatsMap());
            modelBuilder.Configurations.Add(new vw_Screener_Dash_ResponsesMap());
            modelBuilder.Configurations.Add(new vw_Screener_Dash_TotalsMap());
            modelBuilder.Configurations.Add(new vw_Screener_Dash_Uncontacted_DataMap());
            modelBuilder.Configurations.Add(new vw_Screener_Initial_CountsMap());
        }
    }
}
