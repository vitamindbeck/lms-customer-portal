using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class la_News_Section_Link
    {
        public int News_Section_Link_ID { get; set; }
        public int News_Section_ID { get; set; }
        public string Link_URL { get; set; }
        public string Link_Text { get; set; }
        public int Display_Order { get; set; }
        public bool Active { get; set; }
    }
}
