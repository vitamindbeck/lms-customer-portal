using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Plan_Transaction_Status
    {
        public int Plan_Transaction_Status_ID { get; set; }
        public string Description { get; set; }
        public int Display_Order { get; set; }
        public Nullable<int> Transaction_Date_Type_ID { get; set; }
        public Nullable<int> GBS_ID { get; set; }
    }
}
