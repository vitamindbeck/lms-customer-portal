using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class System_Lock
    {
        public string Table_Name { get; set; }
        public int Key { get; set; }
        public System.DateTime Expires_On { get; set; }
        public System.DateTime Created_On { get; set; }
        public int User_ID { get; set; }
    }
}
