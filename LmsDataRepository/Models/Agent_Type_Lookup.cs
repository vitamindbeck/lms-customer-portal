using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Agent_Type_Lookup
    {
        public int Agent_Type_ID { get; set; }
        public string Description { get; set; }
    }
}
