using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class DestinationRX_Data
    {
        public int DestinationRx_Data_ID { get; set; }
        public string ConfirmationNumber { get; set; }
        public Nullable<System.DateTime> SubmitDate { get; set; }
        public string ContractID { get; set; }
        public string PlanID { get; set; }
        public string SegmentID { get; set; }
        public string ApplicantTitle { get; set; }
        public string ApplicantFirstName { get; set; }
        public string ApplicantMiddleInitial { get; set; }
        public string ApplicantLastName { get; set; }
        public Nullable<System.DateTime> ApplicantBirthDate { get; set; }
        public string ApplicantGender { get; set; }
        public string ApplicantAddress1 { get; set; }
        public string ApplicantAddress2 { get; set; }
        public string ApplicantAddress3 { get; set; }
        public string ApplicantCity { get; set; }
        public string ApplicantState { get; set; }
        public string ApplicantZip { get; set; }
        public string ApplicantPhone { get; set; }
        public string ApplicantEmailAddress { get; set; }
        public string ApplicantHICN { get; set; }
        public string ApplicantSSN { get; set; }
        public string MailingAddress1 { get; set; }
        public string MailingAddress2 { get; set; }
        public string MailingAddress3 { get; set; }
        public string MailingCity { get; set; }
        public string MailingState { get; set; }
        public string MailingZip { get; set; }
        public Nullable<System.DateTime> MedicarePartA { get; set; }
        public Nullable<System.DateTime> MedicarePartB { get; set; }
        public string SubmitTime { get; set; }
        public Nullable<int> Record_ID { get; set; }
        public Nullable<System.DateTime> DateTimeFileDownloaded { get; set; }
        public Nullable<bool> IsDownloadSucess { get; set; }
        public Nullable<bool> IsPartialUpload { get; set; }
    }
}
