using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Outsource
    {
        public int Outsource_ID { get; set; }
        public string Outsource_Name { get; set; }
        public System.DateTime Created_On { get; set; }
        public bool Active { get; set; }
    }
}
