using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Group_Transaction_Date_Type
    {
        public int Group_Transaction_Date_Type_ID { get; set; }
        public int Group_ID { get; set; }
        public int Transaction_Date_Type_ID { get; set; }
        public int Display_Order { get; set; }
        public bool Active { get; set; }
    }
}
