using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Plan_Elimination_Period
    {
        public int Plan_Elimination_Period_ID { get; set; }
        public string Description { get; set; }
        public int Low { get; set; }
        public int High { get; set; }
    }
}
