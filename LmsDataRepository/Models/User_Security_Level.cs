using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class User_Security_Level
    {
        public int User_ID { get; set; }
        public int Security_Level_ID { get; set; }
        public System.DateTime Created_On { get; set; }
        public virtual User User { get; set; }
    }
}
