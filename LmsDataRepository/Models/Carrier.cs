using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Carrier
    {
        public Carrier()
        {
            this.Plans = new List<Plan>();
        }

        public int Carrier_ID { get; set; }
        public string Name { get; set; }
        public string Address_1 { get; set; }
        public string Address_2 { get; set; }
        public string City { get; set; }
        public int State_ID { get; set; }
        public string Zip { get; set; }
        public bool Active { get; set; }
        public string About_Carrier { get; set; }
        public bool Need_Upload { get; set; }
        public Nullable<decimal> Service_Level { get; set; }
        public string Service_Level_Description { get; set; }
        public Nullable<decimal> Assets { get; set; }
        public string Code { get; set; }
        public string CompuComp { get; set; }
        public Nullable<int> GBS_Admin_ID { get; set; }
        public string Address_3 { get; set; }
        public Nullable<int> Group_ID { get; set; }
        public string Insurer_Number { get; set; }
        public string Contact_Name { get; set; }
        public bool LTC { get; set; }
        public virtual ICollection<Plan> Plans { get; set; }
    }
}
