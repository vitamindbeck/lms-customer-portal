using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Rating_Company
    {
        public int Rating_Company_ID { get; set; }
        public string Name { get; set; }
    }
}
