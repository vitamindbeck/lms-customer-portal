using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Co_Branded_Site
    {
        public int Co_Branded_Site_ID { get; set; }
        public string Name { get; set; }
        public string Logo_URL { get; set; }
        public string Logo_Alt_Text { get; set; }
        public string Title { get; set; }
        public string Header_Text { get; set; }
        public string Footer_Text { get; set; }
        public string Copyright_Info { get; set; }
        public string Phone_Number { get; set; }
        public string Email_Address { get; set; }
        public string Email_Text { get; set; }
        public string CSS_URL { get; set; }
        public string Admin_Phone_Number { get; set; }
        public string Admin_Email_Address { get; set; }
        public string P_CSS_URL { get; set; }
        public string URL { get; set; }
        public string Splash_Image_URL { get; set; }
        public string Optional_Logo_URL { get; set; }
        public string Optional_Splash_Image_URL { get; set; }
        public string Meta_Description { get; set; }
        public string Meta_Keywords { get; set; }
        public string Contact_Phone_Number { get; set; }
        public string Contact_Fax { get; set; }
        public string Contact_Mail { get; set; }
        public string Contact_In_Person { get; set; }
        public string What_Is { get; set; }
        public string Slogan { get; set; }
        public string Splash_Text { get; set; }
        public string Splash_Image_Text { get; set; }
        public int Source_Code_ID { get; set; }
        public string Folder_Name { get; set; }
        public bool Viewable { get; set; }
        public string Site_Header { get; set; }
        public bool Is_Partnership { get; set; }
        public string Splash_Bottom_Text { get; set; }
        public string Splash_Mid_Text { get; set; }
        public string Disclaimer { get; set; }
        public string Site_URL { get; set; }
        public string Source_Number { get; set; }
    }
}
