using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Complaint_Activity
    {
        public int Complaint_Activity_ID { get; set; }
        public int Complaint_ID { get; set; }
        public int Record_Activity_ID { get; set; }
        public int Record_Activity_Status_ID { get; set; }
        public virtual Complaint Complaint { get; set; }
        public virtual Record_Activity Record_Activity { get; set; }
    }
}
