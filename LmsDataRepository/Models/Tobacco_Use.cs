using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Tobacco_Use
    {
        public int Tobacco_Use_ID { get; set; }
        public string Description { get; set; }
        public int Display_Order { get; set; }
        public bool Active { get; set; }
    }
}
