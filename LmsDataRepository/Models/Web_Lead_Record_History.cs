using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Web_Lead_Record_History
    {
        public int Web_Lead_Record_History_ID { get; set; }
        public Nullable<int> Record_ID { get; set; }
        public Nullable<int> Web_Lead_ID { get; set; }
        public Nullable<System.DateTime> Date_Of_Import { get; set; }
    }
}
