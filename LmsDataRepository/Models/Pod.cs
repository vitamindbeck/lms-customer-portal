using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Pod
    {
        public int Pod_ID { get; set; }
        public string Pod_Name { get; set; }
    }
}
