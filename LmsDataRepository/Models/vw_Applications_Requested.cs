using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class vw_Applications_Requested
    {
        public int Plan_Transaction_ID { get; set; }
        public int Transaction_Date_Type_ID { get; set; }
        public Nullable<System.DateTime> Date_Value { get; set; }
    }
}
