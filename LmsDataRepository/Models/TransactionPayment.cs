using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class TransactionPayment
    {
        public Nullable<int> AgentID { get; set; }
        public string BillingPeriod { get; set; }
        public Nullable<System.DateTime> BillingPeriodDate { get; set; }
        public string Comment { get; set; }
        public Nullable<int> CommissionType { get; set; }
        public Nullable<System.DateTime> DateTimeLastModified { get; set; }
        public Nullable<byte> IsUserChanged { get; set; }
        public Nullable<int> NumberOfLives { get; set; }
        public Nullable<decimal> PaymentAmount { get; set; }
        public Nullable<byte> PaymentType { get; set; }
        public Nullable<int> PolicyID { get; set; }
        public Nullable<decimal> PremiumAmount { get; set; }
        public Nullable<int> SplitID { get; set; }
        public Nullable<int> TransactionID { get; set; }
        public int TransactionPaymentID { get; set; }
        public Nullable<int> UserID { get; set; }
        public string Comment2 { get; set; }
    }
}
