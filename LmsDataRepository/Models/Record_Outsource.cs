using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Record_Outsource
    {
        public int Record_Outsource_ID { get; set; }
        public int Record_ID { get; set; }
        public int Outsource_ID { get; set; }
        public string Campaign_ID { get; set; }
        public bool Outsourced { get; set; }
        public Nullable<System.DateTime> Outsource_Date { get; set; }
        public System.DateTime Record_Outsource_Created_On { get; set; }
    }
}
