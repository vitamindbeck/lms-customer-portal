using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class vw_Get_Placed_Policies
    {
        public Nullable<int> LMS_Agent_ID { get; set; }
        public string UserDefinedString41 { get; set; }
        public int PolicyID { get; set; }
        public Nullable<System.DateTime> Placed_Date { get; set; }
        public Nullable<System.DateTime> Closed_Date { get; set; }
        public Nullable<int> Status { get; set; }
    }
}
