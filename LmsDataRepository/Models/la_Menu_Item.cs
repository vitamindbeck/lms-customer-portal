using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class la_Menu_Item
    {
        public int Menu_Item_ID { get; set; }
        public string Page_Name { get; set; }
        public string Link_URL { get; set; }
        public string Link_Text { get; set; }
        public int Display_Order { get; set; }
        public bool Active { get; set; }
    }
}
