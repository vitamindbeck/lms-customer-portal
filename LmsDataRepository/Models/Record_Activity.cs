using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Record_Activity
    {
        public Record_Activity()
        {
            this.CallRatings = new List<CallRating>();
            this.Complaint_Activity = new List<Complaint_Activity>();
            this.Reconciliations = new List<Reconciliation>();
        }

        public int Record_Activity_ID { get; set; }
        public int Record_ID { get; set; }
        public int Activity_Type_ID { get; set; }
        public Nullable<System.DateTime> Due_Date { get; set; }
        public Nullable<System.DateTime> Actual_Date { get; set; }
        public System.DateTime Created_On { get; set; }
        public int Created_By { get; set; }
        public int User_ID { get; set; }
        public string Notes { get; set; }
        public bool Made_Contact { get; set; }
        public int Record_Activity_Status_ID { get; set; }
        public int Plan_Transaction_ID { get; set; }
        public int Priority_Level { get; set; }
        public Nullable<int> Auto_Email_ID { get; set; }
        public Nullable<int> Record_Activity_Note_ID { get; set; }
        public Nullable<int> GBS_Notes_ID { get; set; }
        public int CTCID { get; set; }
        public int SEEID { get; set; }
        public int AdminDocumentationID { get; set; }
        public int Campaign_Code_ID { get; set; }
        public virtual Auto_Email Auto_Email { get; set; }
        public virtual ICollection<CallRating> CallRatings { get; set; }
        public virtual Campaign_Code Campaign_Code { get; set; }
        public virtual ICollection<Complaint_Activity> Complaint_Activity { get; set; }
        public virtual Plan_Transaction Plan_Transaction { get; set; }
        public virtual ICollection<Reconciliation> Reconciliations { get; set; }
        public virtual Record Record { get; set; }
        public virtual Record_Activity_Note Record_Activity_Note { get; set; }
        public virtual Record_Activity_Status Record_Activity_Status { get; set; }
        public virtual User User { get; set; }
    }
}
