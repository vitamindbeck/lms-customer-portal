using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Insurance_Line_Lookup
    {
        public int Line_Of_Insurance_ID { get; set; }
        public string Description { get; set; }
    }
}
