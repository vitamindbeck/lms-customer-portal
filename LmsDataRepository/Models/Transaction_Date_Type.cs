using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Transaction_Date_Type
    {
        public int Transaction_Date_Type_ID { get; set; }
        public string Description { get; set; }
        public int Display_Order { get; set; }
        public bool Active { get; set; }
    }
}
