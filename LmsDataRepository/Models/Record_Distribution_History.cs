using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Record_Distribution_History
    {
        public int Record_Distribution_History_ID { get; set; }
        public int Record_ID { get; set; }
        public int Record_Distribution_Type_ID { get; set; }
        public Nullable<System.DateTime> Added_On { get; set; }
        public Nullable<System.DateTime> Removed_On { get; set; }
    }
}
