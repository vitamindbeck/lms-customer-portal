using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class vw_Screener_Dash_Contact_Set_Follow_Up_Screener
    {
        public Nullable<System.DateTime> MinGroupTime { get; set; }
        public Nullable<System.DateTime> MaxGroupTime { get; set; }
        public string TimePeriod { get; set; }
        public Nullable<int> Under_5 { get; set; }
        public Nullable<int> FivetoFifteen { get; set; }
        public Nullable<int> Over_15 { get; set; }
        public int Agent_ID { get; set; }
    }
}
