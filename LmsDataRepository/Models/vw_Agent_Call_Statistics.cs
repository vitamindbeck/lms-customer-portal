using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class vw_Agent_Call_Statistics
    {
        public string Answer_ext { get; set; }
        public System.DateTime Start_Time { get; set; }
        public Nullable<System.DateTime> EndTime { get; set; }
        public Nullable<System.DateTime> FirstMinGroupTime { get; set; }
        public Nullable<System.DateTime> FirstMaxGroupTime { get; set; }
        public Nullable<int> Call_Time_Second { get; set; }
        public Nullable<int> Elapsed { get; set; }
        public Nullable<int> FirstThirty { get; set; }
        public Nullable<int> LastThirty { get; set; }
        public Nullable<System.DateTime> LastMinGroupTime { get; set; }
        public Nullable<System.DateTime> LastMaxGroupTime { get; set; }
        public string Direction { get; set; }
    }
}
