using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Plan
    {
        public Plan()
        {
            this.Plan_Transaction = new List<Plan_Transaction>();
        }

        public int Plan_ID { get; set; }
        public string Name { get; set; }
        public int Carrier_ID { get; set; }
        public bool Active { get; set; }
        public string About { get; set; }
        public Nullable<int> GBS_Plan_ID { get; set; }
        public string Plan_Code { get; set; }
        public int Plan_Type_ID { get; set; }
        public virtual Carrier Carrier { get; set; }
        public virtual ICollection<Plan_Transaction> Plan_Transaction { get; set; }
    }
}
