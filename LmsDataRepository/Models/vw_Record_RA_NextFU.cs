using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class vw_Record_RA_NextFU
    {
        public int Record_ID { get; set; }
        public Nullable<System.DateTime> Record_RA_Due_Date { get; set; }
    }
}
