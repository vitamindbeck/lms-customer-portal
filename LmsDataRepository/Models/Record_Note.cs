using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Record_Note
    {
        public int Record_Note_ID { get; set; }
        public int Record_ID { get; set; }
        public Nullable<int> Display_Order { get; set; }
        public string Notes { get; set; }
        public Nullable<System.DateTime> Created_On { get; set; }
        public int Created_By { get; set; }
    }
}
