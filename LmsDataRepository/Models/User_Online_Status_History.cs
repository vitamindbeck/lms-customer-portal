using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class User_Online_Status_History
    {
        public int User_Online_Status_History_ID { get; set; }
        public int User_ID { get; set; }
        public short User_Online_Status_Old { get; set; }
        public short User_Online_Status_New { get; set; }
        public System.DateTime Created_On { get; set; }
    }
}
