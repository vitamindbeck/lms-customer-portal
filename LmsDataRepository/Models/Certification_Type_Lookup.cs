using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Certification_Type_Lookup
    {
        public int Certification_Type_ID { get; set; }
        public string Certification_Type { get; set; }
    }
}
