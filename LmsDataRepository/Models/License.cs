using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class License
    {
        public int License_ID { get; set; }
        public int Agent_ID { get; set; }
        public Nullable<System.DateTime> Effective_Date { get; set; }
        public Nullable<int> Line_Of_Insurance_ID { get; set; }
        public string License_Number { get; set; }
        public Nullable<System.DateTime> Renewal_Date { get; set; }
        public string States { get; set; }
        public Nullable<int> GBS_License_ID { get; set; }
        public Nullable<System.DateTime> Last_Updated_Date { get; set; }
        public Nullable<int> Updated_By { get; set; }
        public Nullable<bool> Active { get; set; }
    }
}
