using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class vw_Daily_Ext_Time_Period_Counts
    {
        public string Call_Date { get; set; }
        public string Agent_Ext { get; set; }
        public int Agent_ID { get; set; }
        public Nullable<int> Inbound_under_2min { get; set; }
        public Nullable<int> Inbound_2_to_5min { get; set; }
        public Nullable<int> Inbound_6_to_15min { get; set; }
        public Nullable<int> Inbound_16_to_30min { get; set; }
        public Nullable<int> Inbound_Over_30min { get; set; }
        public Nullable<int> Outbound_under_2min { get; set; }
        public Nullable<int> Outbound_2_to_5min { get; set; }
        public Nullable<int> Outbound_6_to_15min { get; set; }
        public Nullable<int> Outbound_16_to_30min { get; set; }
        public Nullable<int> Outbound_Over_30min { get; set; }
    }
}
