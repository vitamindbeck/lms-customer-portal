using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Reconciliation_Type
    {
        public Reconciliation_Type()
        {
            this.Reconciliations = new List<Reconciliation>();
        }

        public int Reconciliation_Type_ID { get; set; }
        public string Description { get; set; }
        public int Display_Order { get; set; }
        public bool Active { get; set; }
        public Nullable<int> Plan_Transaction_Status_ID { get; set; }
        public Nullable<int> Record_Type_ID { get; set; }
        public Nullable<int> Resolved_Record_Type_ID { get; set; }
        public Nullable<int> GBSTypeId { get; set; }
        public virtual ICollection<Reconciliation> Reconciliations { get; set; }
    }
}
