using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class PDFField
    {
        public int PDFFieldID { get; set; }
        public int PDFID { get; set; }
        public string FieldName { get; set; }
        public string PropertyName { get; set; }
        public bool IsActive { get; set; }
    }
}
