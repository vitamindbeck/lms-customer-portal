using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Record_Assigned
    {
        public int Record_Assigned_ID { get; set; }
        public int Record_ID { get; set; }
        public int User_ID { get; set; }
        public Nullable<System.DateTime> Date_Value { get; set; }
    }
}
