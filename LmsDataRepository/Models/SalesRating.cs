using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class SalesRating
    {
        public int SEEID { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }
    }
}
