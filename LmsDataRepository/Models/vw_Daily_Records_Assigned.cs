using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class vw_Daily_Records_Assigned
    {
        public string Assigned_Date { get; set; }
        public string First_Name { get; set; }
        public string Last_Name { get; set; }
        public Nullable<int> Records_Assigned { get; set; }
        public int User_ID { get; set; }
    }
}
