using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Glossary
    {
        public int Glossary_ID { get; set; }
        public string Term { get; set; }
        public string Definition { get; set; }
        public bool Show_On_Master { get; set; }
        public string Special_Flag { get; set; }
    }
}
