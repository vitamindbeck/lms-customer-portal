using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Record_Distribution
    {
        public int Record_Distribution_Type_ID { get; set; }
        public int Record_ID { get; set; }
    }
}
