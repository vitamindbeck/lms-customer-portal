using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class vw_LMS_Open_Activities
    {
        public int Record_ID { get; set; }
        public int Plan_Transaction_ID { get; set; }
        public string GBS_ID { get; set; }
        public int Record_Activity_ID { get; set; }
        public string First_Name { get; set; }
        public string Last_Name { get; set; }
        public string State { get; set; }
        public string Phone { get; set; }
        public string Email_Address { get; set; }
        public string PlanDesc { get; set; }
        public Nullable<int> Priority_Level { get; set; }
        public Nullable<System.DateTime> Due_Date { get; set; }
        public string SourceName { get; set; }
        public string EligibilityCategory { get; set; }
        public string BuyingPeriod { get; set; }
        public string BuyingPeriodSub { get; set; }
        public Nullable<System.DateTime> Buying_Period_Effective_Date { get; set; }
        public int Activity_Type_ID { get; set; }
        public int Record_Activity_Status_ID { get; set; }
        public Nullable<System.DateTime> Actual_Date { get; set; }
        public int User_ID { get; set; }
        public Nullable<int> Key { get; set; }
    }
}
