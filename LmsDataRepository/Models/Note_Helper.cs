using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Note_Helper
    {
        public int Note_Helper_ID { get; set; }
        public string Note { get; set; }
        public int User_ID { get; set; }
        public int Note_Type { get; set; }
    }
}
