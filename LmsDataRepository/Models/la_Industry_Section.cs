using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class la_Industry_Section
    {
        public int la_Industry_Section_ID { get; set; }
        public string Title { get; set; }
        public int Display_Order { get; set; }
        public bool Active { get; set; }
    }
}
