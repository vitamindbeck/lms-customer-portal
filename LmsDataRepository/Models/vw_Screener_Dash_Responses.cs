using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class vw_Screener_Dash_Responses
    {
        public Nullable<System.DateTime> MinGroupTime { get; set; }
        public Nullable<System.DateTime> MaxGroupTime { get; set; }
        public string TimePeriod { get; set; }
        public Nullable<int> Responses { get; set; }
        public int Agent_ID { get; set; }
    }
}
