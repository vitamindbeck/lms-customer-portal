using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Document
    {
        public int Document_ID { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
    }
}
