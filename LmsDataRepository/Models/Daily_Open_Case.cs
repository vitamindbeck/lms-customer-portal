using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Daily_Open_Case
    {
        public int Open_Case_ID { get; set; }
        public int Record_Activity_ID { get; set; }
        public Nullable<int> Record_ID { get; set; }
        public int Agent_ID { get; set; }
        public System.DateTime Created_On { get; set; }
    }
}
