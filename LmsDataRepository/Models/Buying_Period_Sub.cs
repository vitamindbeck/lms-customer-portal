using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Buying_Period_Sub
    {
        public int Buying_Period_Sub_ID { get; set; }
        public int Buying_Period_ID { get; set; }
        public string Description { get; set; }
    }
}
