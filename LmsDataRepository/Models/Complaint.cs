using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Complaint
    {
        public Complaint()
        {
            this.Complaint_Activity = new List<Complaint_Activity>();
        }

        public int Complaint_ID { get; set; }
        public int Record_ID { get; set; }
        public int Current_Record_Activity_Status_ID { get; set; }
        public bool Resolved { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public virtual ICollection<Complaint_Activity> Complaint_Activity { get; set; }
    }
}
