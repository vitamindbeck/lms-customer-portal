using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Email_Agent_Code
    {
        public int Email_Agent_Code_ID { get; set; }
        public string Code { get; set; }
        public int User_ID { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public Nullable<bool> HTML { get; set; }
    }
}
