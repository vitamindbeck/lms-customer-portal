using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class la_Industry_News_Article
    {
        public int la_Industry_News_Article_ID { get; set; }
        public int la_Industry_Section_ID { get; set; }
        public string Title { get; set; }
        public string Link_URL { get; set; }
        public string Image_URL { get; set; }
        public string Image_Alt { get; set; }
        public Nullable<System.DateTime> Article_Date { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }
    }
}
