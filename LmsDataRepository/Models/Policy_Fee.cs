using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Policy_Fee
    {
        public int Policy_Fee_ID { get; set; }
        public int Plan_ID { get; set; }
        public int Age_Lowest { get; set; }
        public int Age_Highest { get; set; }
        public decimal Coverage_Low { get; set; }
        public decimal Coverage_High { get; set; }
        public Nullable<decimal> Fee { get; set; }
        public bool Commisionable { get; set; }
    }
}
