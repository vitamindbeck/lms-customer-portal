using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Document_Field
    {
        public int Document_Field_ID { get; set; }
        public int Document_ID { get; set; }
        public int Display_Order { get; set; }
        public string Form_Name { get; set; }
        public string DB_Name { get; set; }
    }
}
