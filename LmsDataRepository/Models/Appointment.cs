using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Appointment
    {
        public int Appointment_ID { get; set; }
        public int Agent_ID { get; set; }
        public int Carrier_ID { get; set; }
        public byte[] Agent_Number { get; set; }
        public Nullable<System.DateTime> Last_Updated_Date { get; set; }
        public Nullable<System.DateTime> Appointment_Date { get; set; }
        public Nullable<System.DateTime> Terminate_Date { get; set; }
        public Nullable<int> Line_Of_Insurance_ID { get; set; }
        public Nullable<int> Updated_By { get; set; }
        public Nullable<int> GBS_Appointment_ID { get; set; }
    }
}
