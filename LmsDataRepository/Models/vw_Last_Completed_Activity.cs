using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class vw_Last_Completed_Activity
    {
        public int Record_ID { get; set; }
        public Nullable<int> Record_Activity_ID { get; set; }
    }
}
