using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Plan_Transaction_Request
    {
        public int Plan_Transaction_Request_ID { get; set; }
        public int Plan_Transaction_ID { get; set; }
        public int To_Employee_ID { get; set; }
        public int From_Employee_ID { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public Nullable<System.DateTime> Requested_On { get; set; }
        public Nullable<System.DateTime> Answered_On { get; set; }
        public Nullable<int> Answered_By { get; set; }
    }
}
