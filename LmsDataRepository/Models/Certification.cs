using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Certification
    {
        public int Certification_ID { get; set; }
        public int Agent_ID { get; set; }
        public Nullable<int> Carrier_ID { get; set; }
        public Nullable<int> Non_Carrier_ID { get; set; }
        public Nullable<bool> Notified { get; set; }
        public Nullable<System.DateTime> Registered_Date { get; set; }
        public Nullable<System.DateTime> Complete_Date { get; set; }
        public Nullable<int> Certification_Status_ID { get; set; }
        public string Certification_Link { get; set; }
        public byte[] Certification_User_Name { get; set; }
        public byte[] Certification_Password { get; set; }
        public string Special_Instructions { get; set; }
        public Nullable<System.DateTime> Due_Date { get; set; }
        public Nullable<int> Certification_Type_ID { get; set; }
        public Nullable<System.DateTime> Last_Updated_Date { get; set; }
        public Nullable<int> Updated_By { get; set; }
    }
}
