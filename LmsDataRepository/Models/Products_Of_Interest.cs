using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Products_Of_Interest
    {
        public int Products_Of_Interest_ID { get; set; }
        public string Products_Of_Interest_Desc { get; set; }
        public bool Active { get; set; }
        public short Order { get; set; }
    }
}
