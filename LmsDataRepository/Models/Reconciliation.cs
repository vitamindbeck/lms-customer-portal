using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Reconciliation
    {
        public int Reconciliation_ID { get; set; }
        public int Record_ID { get; set; }
        public int Reconciliation_Type_ID { get; set; }
        public Nullable<System.DateTime> Placed_Date { get; set; }
        public Nullable<int> Placed_By { get; set; }
        public Nullable<System.DateTime> Resolved_Date { get; set; }
        public Nullable<int> Resolved_By { get; set; }
        public string Notes { get; set; }
        public Nullable<int> Record_Activity_ID { get; set; }
        public virtual Reconciliation_Type Reconciliation_Type { get; set; }
        public virtual Record_Activity Record_Activity { get; set; }
    }
}
