using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Record_Type
    {
        public Record_Type()
        {
            this.Records = new List<Record>();
        }

        public int Record_Type_ID { get; set; }
        public string Description { get; set; }
        public int Display_Order { get; set; }
        public Nullable<int> GBS_ID { get; set; }
        public virtual ICollection<Record> Records { get; set; }
    }
}
