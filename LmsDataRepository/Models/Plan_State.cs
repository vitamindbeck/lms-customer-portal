using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Plan_State
    {
        public int Plan_State_ID { get; set; }
        public int Plan_ID { get; set; }
        public int State_ID { get; set; }
    }
}
