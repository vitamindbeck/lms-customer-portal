using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class DestinationRx_Plan
    {
        public string DestinationRx_Plan_Code { get; set; }
        public string GBS_Plan_ID { get; set; }
        public string Destination_Plan_Name { get; set; }
        public string GBS_Product_Name { get; set; }
        public string Client_Name { get; set; }
        public string Plan_Type { get; set; }
        public string State { get; set; }
    }
}
