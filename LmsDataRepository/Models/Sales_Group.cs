using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Sales_Group
    {
        public int Sales_Group_ID { get; set; }
        public string Description { get; set; }
        public int Manager_User_ID { get; set; }
        public int Admin_User_ID { get; set; }
    }
}
