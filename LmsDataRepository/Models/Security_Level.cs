using System;
using System.Collections.Generic;

namespace LmsDataRepository.Models
{
    public partial class Security_Level
    {
        public int Security_Level_ID { get; set; }
        public string Description { get; set; }
        public Nullable<bool> In_AD { get; set; }
    }
}
