﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LMS.Data;
using LMS.Data.Repository;

namespace LMS.Controllers
{
    public class ProspectController : Controller
    {
        private IRecordRepository _repository;
        private IUserRepository _userRepository;
        private IDropDownRepository _dropRepository;
        private IPlanTransactionRepository _planTransactionRepository;
        private IActivityRepository _recordActivtyRepository;



        public ProspectController()
            : this(new RecordRepository(), new UserRepository(), new DropDownRepository(), new PlanTransactionRepository(), new ActivityRepository())
        {
        }

        public ProspectController(IRecordRepository repository, IUserRepository userrepository, IDropDownRepository droprepository, IPlanTransactionRepository planTranRepo, IActivityRepository activityrepository)
        {
            _repository = repository;
            _userRepository = userrepository;
            _dropRepository = droprepository;
            _planTransactionRepository = planTranRepo;
            _recordActivtyRepository = activityrepository;
        }




        public ActionResult New()
        {
            LMS.Data.User user = UserRepository.GetUser(User.Identity.Name);


            //Data Context
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

            Record record = new Record();
            record.Agent_ID = user.User_ID;
            record.Assistant_Agent_ID = 49;
            record.Created_On = DateTime.Now;
            record.Imported = true;
            record.Record_Type_ID = 3;

            //var agents = _dropRepository.GetAgents(true);
            //ViewData["Agent_ID"] = new SelectList(agents, "User_ID", "FullName", record.Agent_ID);
            //ViewData["Assistant_Agent_ID"] = new SelectList(agents, "User_ID", "FullName", record.Assistant_Agent_ID);


            ViewData["State_ID"] = new SelectList(_dropRepository.GetStates(false), "State_ID", "Abbreviation", 52);
            ViewData["Marital_Status_ID"] = new SelectList(_dropRepository.GetMaritalStatuses(true), "Marital_Status_ID", "Description", -1);
            ViewData["Record_Current_Situation_ID"] = new SelectList(_dropRepository.GetRecordCurrentSituations(true), "Record_Current_Situation_ID", "Record_Current_Situation_Desc", record.Record_Current_Situation_ID);
            ViewData["Current_Insured_Type_ID"] = new SelectList(_dropRepository.GetCurrentInsuredTypes(true), "Current_Insured_Type_ID", "Current_Insured", record.Current_Insured_Type_ID);
            ViewData["best_time_to_call"] = new SelectList(_dropRepository.GetTimes());

            ViewData["change_plans"] = new SelectList(_dropRepository.GetUserAccess(false), "Key", "Value"); //ADD MAYBE to DDL
            ViewData["Drug_Coverage"] = new SelectList(_dropRepository.GetUserAccess(false), "Key", "Value");
            ViewData["Indemnity"] = new SelectList(_dropRepository.GetUserAccess(false), "Key", "Value");
            ViewData["Long_Term_Care"] = new SelectList(_dropRepository.GetUserAccess(false), "Key", "Value");
            ViewData["Critical_Illness"] = new SelectList(_dropRepository.GetUserAccess(false), "Key", "Value");

            //(Subsidy_ID)
            ViewData["Eligible_Medicaid"] = new SelectList(_dropRepository.GetUserAccess(false), "Key", "Value");
            ViewData["Eligible_LIS"] = new SelectList(_dropRepository.GetUserAccess(false), "Key", "Value");
            ViewData["Eligible_PACE"] = new SelectList(_dropRepository.GetUserAccess(false), "Key", "Value");
            return View(record);
        }

        [HttpPost]
        public ActionResult New(LMS.Data.Record record)
        {
            record.Record_Type_ID = 4; //Prospect
            //testing Data
            var First_Name = record.First_Name;
            var Middle_Initial = record.Middle_Initial;
            var Last_Name = record.Last_Name;
            var RequestedStartDate = record.RequestedStartDate;
            var Address_1 = record.Address_1;
            var Address_2 = record.Address_2;
            var City = record.City;
            var State_ID = record.State_ID;
            var Medical_Carrier = record.Medical_Carrier;
            var Medicare_Number = record.Medicare_Number;
            var Email_Address = record.Email_Address;
            var Current_Insured_Type_ID = record.Current_Insured_Type_ID;
            var Cell_Phone = record.Cell_Phone;
            var Work_Phone = record.Work_Phone;
            var Home_Phone = record.Home_Phone;
            var DOB = record.DOB;
            var Sex = record.Sex;
            var Notes = record.Notes;
            var Marital_Status_ID = record.Marital_Status_ID;
            //_repository.Insert(record);
            //_repository.Save();

            return View(record);
        }

    }
}
