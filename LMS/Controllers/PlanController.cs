using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using LMS.Data;
using LMS.Data.Repository;

namespace LMS.Controllers
{
    public class PlanController : Controller
    {
        //
        // GET: /Plan/
        LMSDataContext _DB = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
        DropDownRepository dropDowns = new DropDownRepository();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Detail(int id)
        {
            return View(_DB.Plans.Where(p => p.Plan_ID == id).FirstOrDefault());
        }

        public ActionResult Documents(int id)
        {
            Plan plan = _DB.Plans.Where(p=>p.Plan_ID==id).FirstOrDefault();

            ViewData["Title"] = plan.Name;

            if ( plan != null)
            {
                return View(plan.Plan_Documents);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        public ActionResult Create()
        {
            ViewData["State_ID"] = new SelectList(dropDowns.GetStates(false), "State_ID", "Name", 52);
            return View(new Plan_Document());
        }

        public ActionResult Edit(int id)
        {
            Plan plan = _DB.Plans.Where(p => id == p.Plan_ID).FirstOrDefault();

            return View(plan);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(int id, FormCollection collection)
        {
            Plan plan = _DB.Plans.Where(p => id == p.Plan_ID).FirstOrDefault();

            UpdateModel(plan);

            _DB.SubmitChanges();

            return RedirectToAction("Detail", new { id = plan.Plan_ID });
        }

        public ActionResult List(int id)
        {
            return View(_DB.Plans.Where(p => p.Carrier_ID == id));
        }
    }
}