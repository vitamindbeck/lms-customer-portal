﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LMS.Data.Repository;


namespace LMS.Controllers
{
    [HandleError]
    [Authorize]
    public class HomeController : Controller
    {
        UserRepository _userRepository = new UserRepository();

        public ActionResult Index()
        {
            LMS.Data.User user = _userRepository.GetUserFromADName(User.Identity.Name.Replace(@"LONGEVITY\", ""));

            if (user == null)
                throw new HttpException(401, "Invalid User");

            switch (user.Security_Level_ID)
            {
                case 4:
                    return RedirectToAction("Admin","Record");
                case 5:
                    return RedirectToAction("Support", "Record");
                case 2:
                    return RedirectToAction("NewLeads", "Record");
                case 8:
                    return RedirectToAction("AppointmentSetter", "Record");
                case 12:
                    return RedirectToAction("FollowUp", "Record");
                default:
                    if (user.Sales_Group_ID == 2)
                    {
                        return RedirectToAction("Search","Record");
                    }
                    else {
                        return RedirectToAction("Sales", "Record");
                    }
                    
            }
        }

        public ActionResult About()
        {
            return View();
        }

        public string KeepSessionAlive()
        {

            return "success";
        }
    }
}
