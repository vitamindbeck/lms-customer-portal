using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Data;
using LMS.Data.Repository;
using LMS.Data;
using LMS.Helpers;


namespace LMS.Controllers
{
    [Authorize]
    [HandleError]
    [SessionExpireFilterAttribute]
    public class AgentController : Controller
    {
       
        private IAgentRepository _agentRepository;
        private IDropDownRepository _dropdownRepository;
        private IUserRepository _userRepository;
       

              public AgentController()
            : this(new AgentRepository(),new DropDownRepository(),new UserRepository())
        {
        }

        public AgentController(IAgentRepository agentRepository, IDropDownRepository dropdownRepository, IUserRepository userRepository)
        {
            _dropdownRepository = dropdownRepository;
            _agentRepository = agentRepository;
            _userRepository = userRepository;
           
        }


        public ActionResult New()
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            Agent agent = new Agent();
           
            ViewData["Agent_Name"] = new SelectList(_dropdownRepository.GetAgentListFromUser(), "User_ID", "Full_Name");
            ViewData["State"] = new SelectList(_dropdownRepository.GetStates(true), "State_ID", "Abbreviation", "");
            ViewData["Agency_State_ID"] = new SelectList(_dropdownRepository.GetStates(true), "State_ID", "Abbreviation", "");
            ViewData["Associated_Agency"] = new SelectList(_agentRepository.GetAgencyList(true), "Agent_ID", "Agency_Name",-1);
           
            return View(agent);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult New(Agent agent, FormCollection collection)
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
          
            //If agent check box is clicked
            if(collection["chkAgent"] != "false")
            {
                //Get agent's userID
                int agentID = Int32.Parse(collection["Agent_Name"]);


               //Agency Name
                if (collection["Associated_Agency_ID"] == "0")
                {
                    ModelState.AddModelError("Associated_Agency_ID", "Associated Agency Name can not be blank");


                }
                else
                {
                    agent.Associated_Agency_ID = Int32.Parse(collection["Associated_Agency_ID"]);
                }
        

                //User details
                var userDetails = _userRepository.GetUser(agentID);

                    agent.User_ID = Convert.ToInt32(agentID);
                    agent.Agent_Type_ID = 1;
                    agent.Email = userDetails.Email_Address;
            }
            //If agency Check box is clicked
            else if(collection["chkAgency"] != "false")
            {
                //Agency Name
                if (collection["Agency_Name"] == "")
                {
                    ModelState.AddModelError("Agency_Name", "Agency Name can not be blank");
                    

                }
                else
                {
                    agent.Agency_Name = collection["Agency_Name"];
                }
                agent.Agent_Type_ID = 2;
                agent.User_ID = 0;
               
            }
            if (ModelState.IsValid)
            {
                try
                {
                    if (agent.Agent_Type_ID == 1)
                    {
                        agent.NPN = collection["NPN"];

                        agent.SSN = StringHelper.EncryptString(Convert.ToString(collection["Agent_SSN"]));
                   
                        if (collection["Agent_Start_Date"].Equals(""))
                        {
                            agent.Agent_Start_Date = null;
                        }
                        else
                        {
                            agent.Agent_Start_Date = DateTime.Parse(collection["Agent_Start_Date"]);
                        }
                        if (collection["DOB"].Equals(""))
                        {
                            agent.DOB = null;
                        }
                        else
                        {
                            agent.DOB = DateTime.Parse(collection["DOB"]);
                        }
                        agent.Active = true;
                        agent.Agency_Name = null;
                        agent.Tax_ID = null;
                        agent.Address_1 = collection["Address_1"];
                        agent.Address_2 = collection["Address_2"];
                        agent.City = collection["City"];

                        if (agent.State_ID != null && agent.State_ID > 0)
                        {

                            agent.State_ID = Int32.Parse(collection["State_ID"]);

                        }
                        else
                        {
                            agent.State_ID = null;
                        }
                      
                        agent.County = collection["County"];
                        agent.Zip_Code = collection["Zip_Code"];
                        agent.Email = collection["Email"];
                        agent.Phone_Number = collection["Phone_Number"];
                        agent.Last_Updated_Date = DateTime.Now;
                        agent.Updated_By = _userRepository.GetUserByUserName(User.Identity.Name).User_ID;

                       
                    }
                    else
                    {


                        agent.NPN = collection["Agency_NPN"];
                        agent.Tax_ID = collection["Tax_ID"];
                      
                        if (collection["Agency_Start_Date"].Equals(""))
                        {
                            agent.Agent_Start_Date = null;
                        }
                        else
                        {
                            agent.Agent_Start_Date = DateTime.Parse(collection["Agency_Start_Date"]);
                        }
                        agent.Active = true;
                        agent.SSN = null;
                        agent.Address_1 = collection["Agency_Address_1"];
                        agent.Address_2 = collection["Agency_Address_2"];
                        agent.City = collection["Agency_City"];

                        if (agent.State_ID != null && agent.State_ID > 0)
                        {

                            agent.State_ID = Int32.Parse(collection["Ag_State_ID"]);

                        }
                        else
                        {
                            agent.State_ID = null;
                        }
                        
                        agent.County = collection["Agency_County"];
                        agent.Zip_Code = collection["Agency_Zip_Code"];
                        agent.Phone_Number = collection["Agency_Phone_Number"];
                        agent.Last_Updated_Date = DateTime.Now;
                        agent.Updated_By = _userRepository.GetUserByUserName(User.Identity.Name).User_ID;
                       
                    }

                    db.Agents.InsertOnSubmit(agent);
                    db.SubmitChanges();

                    return RedirectToAction("Details", new { id = agent.Agent_ID});
                  
                }
                catch (Exception e)
                {
                    LMS.ErrorHandler.HandleError.EmailError(e);
                }

            }

            
                ViewData["Agent_Name"] = new SelectList(_dropdownRepository.GetAgentListFromUser(), "User_ID", "Full_Name", agent.Agent_ID);
                ViewData["State_ID"] = new SelectList(_dropdownRepository.GetStates(true), "State_ID", "Abbreviation", collection["State_ID"]);
                ViewData["Agency_State_ID"] = new SelectList(_dropdownRepository.GetStates(true), "State_ID", "Abbreviation", collection["Agency_State_ID"]);
         
            ViewData["Associated_Agency"] = new SelectList(_agentRepository.GetAgencyList(true), "Agent_ID", "Agency_Name", Int32.Parse(collection["Associated_Agency_ID"]));
            return View(agent);
        }

        public ActionResult Edit(int id)
        {

            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            Agent agent = _agentRepository.GetAgentByID((int)id);
            var agents = db.sp_Get_AgentsAgency(id).FirstOrDefault();
            ViewData["SSN"] = db.sp_Decrypt_Data(agent.SSN);
            ViewData["Agent_Name"] = agents.Full_Name;
            ViewData["Agency_Active"] = agents.Active;
            ViewData["Active"] = agents.Active;
            ViewData["State_ID"] = new SelectList(_dropdownRepository.GetStates(false), "State_ID", "Abbreviation", agent.State_ID.ToString());
            ViewData["Agency_State_ID"] = new SelectList(_dropdownRepository.GetStates(false), "State_ID", "Abbreviation", agent.State_ID.ToString());
            ViewData["Associated_Agency"] = new SelectList(_agentRepository.GetAgencyList(false),"Agent_ID", "Agency_Name", agent.Associated_Agency_ID);

            return View(agent);
            
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(int id, FormCollection collection)
        {

            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            Agent agent = db.Agents.Where(a => a.Agent_ID == id).FirstOrDefault();


            if (ModelState.IsValid)
            {
                try
                {
                    if (agent.Agent_Type_ID == 1)
                    {

                        agent.NPN = collection["NPN"];
                        agent.SSN = StringHelper.EncryptString(Convert.ToString(collection["Ag_SSN"]));
                        agent.Phone_Number = collection["Phone_Number"];
                        agent.Zip_Code = collection["Zip_Code"];
                        agent.County = collection["County"];
                        agent.City = collection["City"];

                        if (Int32.Parse(collection["State_ID"]).Equals(""))
                        {
                            agent.State_ID = null;
                        }
                        else
                        {
                            agent.State_ID = Int32.Parse(collection["State_ID"]);
                            ViewData["State_ID"] = new SelectList(_dropdownRepository.GetStates(false), "State_ID", "Abbreviation", agent.State_ID.ToString());
                        }

                        agent.Address_1 = collection["Address_1"];
                        agent.Address_2 = collection["Address_2"];
                        agent.Email = collection["Email"];
                        
                      
                        
                        agent.Associated_Agency_ID = Int32.Parse(collection["Associated_Agency_ID"]);
                        ViewData["Associated_Agency"] = new SelectList(_agentRepository.GetAgencyList(false), "Agent_ID", "Agency_Name", agent.Associated_Agency_ID);

                        if (collection["DOB"].Equals(""))
                        {
                            agent.DOB = null;
                        }
                        else
                        {
                            agent.DOB = DateTime.Parse(collection["DOB"]);
                        }

                        //Active check box
                        if (collection["Active"] != "false")
                        {
                            agent.Active = true;
                        }
                        else
                        {
                            agent.Active = false;
                        }

                        if (collection["Agent_Start_Date"].Equals(""))
                        {
                            agent.Agent_Start_Date = null;
                        }
                        else
                        {
                            agent.Agent_Start_Date = DateTime.Parse(collection["Agent_Start_Date"]);
                        }
                        
                       
                        if (collection["Agent_End_Date"].Equals(""))
                        {
                            agent.Agent_End_Date = null;
                        }
                        else
                        {
                            agent.Agent_End_Date = DateTime.Parse(collection["Agent_End_Date"]);
                        }
                    }
                    else
                    {
                      
                        agent.Agency_Name = collection["Agency_Name"];
                        agent.NPN = collection["Agency_NPN"];
                        agent.Tax_ID = collection["Tax_ID"];
                        agent.Phone_Number = collection["Agency_Phone_Number"];
                        agent.Zip_Code = collection["Agency_Zip_Code"];
                        agent.County = collection["Agency_County"];
                        agent.City = collection["Agency_City"];
                        agent.Address_1 = collection["Agency_Address_1"];
                        agent.Address_2 = collection["Agency_Address_2"];

                        //Active check box
                        if (collection["Agency_Active"] != "false")
                        {
                            agent.Active = true;
                        }
                        else
                        {
                            agent.Active = false;
                        }

                        if (collection["Agency_Start_Date"].Equals(""))
                        {
                            agent.Agent_Start_Date = null;
                        }
                        else
                        {
                            agent.Agent_Start_Date = DateTime.Parse(collection["Agency_Start_Date"]);
                        }
                        if (Int32.Parse(collection["Agency_State_ID"]).Equals(""))
                        {
                            agent.State_ID = null;
                        }
                        else
                        {
                            agent.State_ID = Int32.Parse(collection["Agency_State_ID"]);
                            ViewData["Agency_State_ID"] = new SelectList(_dropdownRepository.GetStates(false), "State_ID", "Abbreviation", agent.State_ID.ToString());
                        }
                    }

                         agent.Last_Updated_Date = DateTime.Now;
                         agent.Updated_By = _userRepository.GetUserByUserName(User.Identity.Name).User_ID;
                        
                        db.SubmitChanges();
                        return RedirectToAction("Details", new { id = agent.Agent_ID });
                }
                catch (Exception e)
                {
                  LMS.ErrorHandler.HandleError.EmailError(e);
                }

            }
          

            return View(agent);


        }

        public ActionResult Details(int? id)
        {
            Agent agent = _agentRepository.GetAgentByID((int)id);
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

            if (agent.Agent_Type_ID == 1)
            {
                var agents = db.sp_Get_AgentsAgency(id).FirstOrDefault();
                ViewData["Agent_Name"] = agents.Full_Name;
             }
            else
            {
                ViewData["AgentName"] = agent.Agency_Name;
            }

            //Assign SSN and TAX ID
            if (agent.Agent_Type_ID == 1)
            {
                ViewData["SSN"] = agent.SSN;
            }
            else
            {
                ViewData["Tax_ID"] = agent.Tax_ID;
            }
         

            if (agent.Updated_By != null)
            {
                ViewData["UserName"] = _userRepository.GetUser((int)agent.Updated_By).First_Name + " " + _userRepository.GetUser((int)agent.Updated_By).Last_Name;
            }
            else
            {
                ViewData["UserName"] = "";
            }
            ViewData["Active"] = Convert.ToBoolean(agent.Active);

            if (agent.Associated_Agency_ID != null && agent.Associated_Agency_ID > 0)
            {

                var agents = _agentRepository.GetAgencyByID((int) agent.Associated_Agency_ID);
                ViewData["Associated_Agency"] = agents.Agency_Name;
               
            }
            else
            {
                ViewData["Associated_Agency"] = "";
            }

            if (agent.State_ID != null && agent.State_ID > 0)
            {

                var agents = db.States.Where(c => c.State_ID == agent.State_ID).FirstOrDefault();
                ViewData["State"] = agents.Abbreviation;

            }
            else
            {
                ViewData["State"] = "";
            }

            
           
           return View(agent);
       
        }
        //Export to Excel
        public ActionResult Export()
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            DataTable table;
            DataColumn column;
            DataRow row;
            DataSet dataset;
            table = new DataTable("LicenseAgentList");

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Agent";
            column.ReadOnly = true;
            table.Columns.Add(column);


            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "NPN";
            column.ReadOnly = true;
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "SSN";
            column.ReadOnly = true;
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Address 1";
            column.ReadOnly = true;
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "City";
            column.ReadOnly = true;
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "State";
            column.ReadOnly = true;
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Zip Code";
            column.ReadOnly = true;
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Email";
            column.ReadOnly = true;
            table.Columns.Add(column);

           

                
                dataset = new DataSet();
                dataset.Tables.Add(table);
                List<sp_Agent_ListResult> rs = db.sp_Agent_List().ToList();
                for (int i = 0; i < rs.Count; i++)
                {
                    row = table.NewRow();

                    row["Agent"] = rs[i].Name;

                    row["NPN"] = rs[i].NPN;

                    row["SSN"] = rs[i].SSN;

                    row["Address 1"] = rs[i].Address_1;

                    row["City"] = rs[i].City;

                    row["State"] = rs[i].Abbreviation;

                    row["Zip Code"] = rs[i].Zip_Code;

                    row["Email"] = rs[i].Email;

                     
                    table.Rows.Add(row);
                }
                RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
                objExport.ExportDetails(table, RKLib.ExportData.Export.ExportFormat.Excel, "AgentList.xls");

                return View();

            }
           

        
        



        public ActionResult List()
        {
            //Get Agent Lists
            ViewData["LicenseAgentList"] = _agentRepository.GetLicenseAgentList();
          
            return View();
        }
        public ActionResult Select()
        {
            return View();
        }
    }
}
