using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using LMS.Data;
using LMS.Data.Repository;
using LMS.ErrorHandler;
using LMS.Helpers;


namespace LMS.Controllers
{
    public class ActivityController : Controller
    {

        //
        // GET: /Activity/
        IRecordRepository _recordRepository;
        IDropDownRepository _dropRepository;
        IActivityRepository _actRepository;
        IPlanTransactionRepository _tranRepository;
        ICallRatingRepository _callRepository;

        public ActivityController()
            : this(new RecordRepository(), new DropDownRepository(), new ActivityRepository(), new PlanTransactionRepository(), new CallRatingRepository())
        {
        }

        public ActivityController(IRecordRepository repository, IDropDownRepository droprepository, IActivityRepository actrepository, IPlanTransactionRepository tranRepository, ICallRatingRepository callRepository)
        {
            _recordRepository = repository;
            _dropRepository = droprepository;
            _actRepository = actrepository;
            _tranRepository = tranRepository;
            _callRepository = callRepository;
        }

        public ActionResult Index()
        {
            return View();
        }

        //public ActionResult View()
        //{
        //    return View();
        //}

        public ActionResult AddFollowUp(int id)
        {

            Plan_Transaction transaction = _tranRepository.Get(id);

            if (transaction == null)
                throw new HttpException(400, "Rad Request");

            LMS.Data.User user = UserRepository.GetUser(User.Identity.Name);

            Record_Activity activity = new Record_Activity();
            Record record = _recordRepository.GetRecord(transaction.Record_ID);

            ViewData["Record_Id"] = transaction.Record_ID;
            ViewData["Plan_Transaction_ID"] = new SelectList(_dropRepository.GetOpportunities(record.Record_ID), "Plan_Transaction_ID", "DropDownName", activity.Plan_Transaction_ID);
            ViewData["Follow_Up_Time"] = new SelectList(_dropRepository.GetTimes());
            ViewData["Follow_Activity_Type_ID"] = new SelectList(_dropRepository.GetFollowUpType(true), "Activity_Type_ID", "Description", -1);
            ViewData["Follow_User_ID"] = new SelectList(_dropRepository.GetAgents(false).Where(a => a.Active == true), "User_ID", "FullName", record.Agent_ID);
            ViewData["Follow_Priority_Level"] = new SelectList(_dropRepository.GetPriorityLevels(false), "Key", "Value", 3);
            ViewData["RecordNotes"] = new SelectList(_dropRepository.GetActivityNotes(user.User_ID, 0), "Record_Activity_Note_ID", "Description");
            ViewData["FollowRecordNotes"] = new SelectList(_dropRepository.GetActivityNotes(user.User_ID, 1), "Description", "Description");
            return View();
        }

        public ActionResult New(int id)
        {

            Plan_Transaction transaction = _tranRepository.Get(id);
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

            if (transaction == null)
                throw new HttpException(400, "Rad Request");

            LMS.Data.User user = UserRepository.GetUser(User.Identity.Name);

            Record_Activity activity = new Record_Activity();
            Record record = _recordRepository.GetRecord(transaction.Record_ID);

            ViewData["DisplayRecordActivitiesList"] = _actRepository.GetRecordActivitiesDisplay(record.Record_ID);

            ViewData["Record_Type_ID"] = record.Record_Type_ID;

            var opportunity = transaction.Plan_ID;
            var modalPremium = transaction.Modal_Premium;
            var subplan = transaction.Sub_Plan;
            var effective_Date = transaction.Buying_Period_Effective_Date;

            var group = transaction.Group_ID;
            ViewData["Group"] = group;


            if (modalPremium != 0 && opportunity != -1 && subplan != null && effective_Date != null)
            {
                ViewData["Modal_Premium"] = modalPremium;
                ViewData["Opportunity"] = opportunity;
                ViewData["SubPlan"] = subplan;
                ViewData["Eff_Date"] = effective_Date;
            }
            else
            {
                ViewData["Modal_Premium"] = "";
                ViewData["Opportunity"] = "";
                ViewData["SubPlan"] = "";
                ViewData["Eff_Date"] = "";
                ViewData["PaymentMode"] = "";
            }

            /*
            if (opportunity != -1)
            {
                ViewData["Opportunity"] = "";
            }
            else
            {
                ViewData["Opportunity"] = opportunity;
            }
 
            var modalPremium = transaction.Modal_Premium;
            if (modalPremium == 0)
            {
                ViewData["Modal_Premium"] = "";
            }
            else
            {
                ViewData["Modal_Premium"] = modalPremium;
            }                            
            var subplan = transaction.Sub_Plan;
            if (subplan == null)
            {
                ViewData["SubPlan"] = "";

            }
            else
            {
                ViewData["SubPlan"] = subplan;
            }
            
            var effective_Date = transaction.Buying_Period_Effective_Date;
            if (effective_Date != null)
            {
                ViewData["Eff_Date"] = effective_Date;
            }
            else
            {
                ViewData["Eff_Date"] = "";
            }
            */
           
            var modal = transaction.Modal;
            if (modal != null)
            {
                var paymentMode = modal.Description;
                if (paymentMode != null)
                {
                    ViewData["PaymentMode"] = paymentMode;
                }
                else
                {
                    ViewData["PaymentMode"] = "";
                }
            }              
            else
            {
                ViewData["PaymentMode"] = "";
            }

            var ptcarrier = transaction.Plan;
            if (ptcarrier == null)
            {
                ViewData["Carrier"] = "";                
            }
            else
            {
                var carrierName = ptcarrier.Carrier;
                if(carrierName != null)
                {
                    ViewData["Carrier"] = carrierName.Name;
                }
                else
                {
                    ViewData["Carrier"] = "";
                }   
            }           
  

            Record_Activity futureAct = record.Record_Activities.Where(ra => ra.Actual_Date.HasValue == false && ra.Record_ID == record.Record_ID && ra.Record_Activity_ID != activity.Record_Activity_ID).OrderByDescending(ra => ra.Actual_Date).FirstOrDefault();

            if (futureAct == null)
            {
                ViewData["Next_Activity_Date"] = "";
            }
            else
            {
                ViewData["Next_Activity_Date"] = futureAct.Due_Date;
            }

            var apptra =
                db.Record_Activities.Where(
                    a =>
                    a.Plan_Transaction_ID == activity.Plan_Transaction_ID && a.Activity_Type_ID == 22
                    && a.Actual_Date == null).ToList();


            var FutFU = apptra.FirstOrDefault();
            if (apptra.Count == 1)
            {
                if (activity.Activity_Type_ID == 22)
                {
                    ViewData["Future_Appointment_Date"] = "";
                }
                else
                {
                    ViewData["Future_Appointment_Date"] = FutFU.Due_Date.ToString();
                }

            }
            else if (apptra.Count < 1)
            {
                ViewData["Future_Appointment_Date"] = "";
            }
            else
            {

                ViewData["Future_Appointment_Date"] = FutFU.Due_Date.ToString();

            }

            /*
             * Enums.ComplaintType? complaintCurrentStep = null;
            var complaint = _recordRepository.GetComplaint(record.Record_ID);
            if (complaint == null)
            {
                complaintCurrentStep = Enums.ComplaintType.Open;

            }
            else
            {
                complaintCurrentStep = Enums.ComplaintType.InProgress;
                if (UserRepository.HasSecurityLevel(user, 13))
                {
                    complaintCurrentStep = Enums.ComplaintType.Resoloved;
                }
            }
             */
            
            //bool com
            // if complaint for activity exists under current plan transaction, send back ddl with complaint in process
            // if (record.plantrasaction = current )


            var ddlDisposition = new List<Reconciliation_Type>();
            Enums.ComplaintType? complaintCurrentStep = null;
            var complaint = _recordRepository.GetComplaint(record.Record_ID);
            if (complaint != null)
            {
                var complaintId = db.Complaint_Activities.FirstOrDefault(x => x.Complaint_ID == complaint.Complaint_ID);
                if (complaintId != null)
                {
                    // record activity with complaint
                    var recordActivity =
                        db.Record_Activities.FirstOrDefault(x => x.Record_Activity_ID == complaintId.Record_Activity_ID);
                    var complaintTransaction =
                        db.Record_Activities.FirstOrDefault(
                            x => x.Plan_Transaction_ID == recordActivity.Plan_Transaction_ID);

                    // current record activity
                    if (transaction.Plan_Transaction_ID == complaintTransaction.Plan_Transaction_ID)
                    {
                        
                    complaintCurrentStep = Enums.ComplaintType.InProgress;
                            ddlDisposition = _dropRepository.GetReconciliationTypes(true, complaintCurrentStep);
                            ddlDisposition =
                                ddlDisposition.Where(
                                    i => i.Reconciliation_Type_ID == 40 || i.Reconciliation_Type_ID == 64).ToList();

                    }
                        // complaint transaction and current transaction do not match
                    else
                    {
                        ddlDisposition = _dropRepository.GetReconciliationTypes(true, complaintCurrentStep);
                    }
                }
            }
            if (complaint == null)
            {
                complaintCurrentStep = Enums.ComplaintType.Open;
                ddlDisposition = _dropRepository.GetReconciliationTypes(true, complaintCurrentStep);
            }



            //var ddlDisposition = _dropRepository.GetReconciliationTypes(true, complaintCurrentStep);
            //if (activity.Record_Activity_Status_ID == 33 || activity.Record_Activity_Status_ID == 35)
            //{
            //    ddlDisposition = ddlDisposition.Where(i => i.Reconciliation_Type_ID == 40 || i.Reconciliation_Type_ID == 64).ToList();
            //}

            //Set Initials
            activity.Created_By = user.User_ID;
            activity.Created_On = DateTime.Now;
            activity.User_ID = user.User_ID;
            //activity.Activity_Type_ID = 2;
            activity.Priority_Level = record.Priority_Level;
            activity.Plan_Transaction_ID = id;
            activity.Record_ID = record.Record_ID;

            ViewData["Record"] = record;
            ViewData["RecordIDValue"] = record.Record_ID;
            ViewData["Record_Activities"] = record.Record_Activities.OrderByDescending(c => c.Actual_Date);
            ViewData["Activity_Type_ID"] = new SelectList(_dropRepository.GetActivityTypes(true), "Activity_Type_ID", "Description", -1);

            ViewData["Record_Activity_Status_ID"] = new SelectList(_dropRepository.GetActivityStatus(true, null), "Record_Activity_Status_ID", "Description", -1);
            //ViewData["Record_Activity_Status_ID_FollowUp"] = new SelectList(_dropRepository.GetActivityStatus(true, complaintFollowUpStep), "Record_Activity_Status_ID", "Description", -1);


            ViewData["Plan_Transaction_ID"] = new SelectList(_dropRepository.GetOpportunities(record.Record_ID), "Plan_Transaction_ID", "DropDownName", activity.Plan_Transaction_ID);

            ViewData["Follow_Activity_Type_ID"] = new SelectList(_dropRepository.GetFollowUpType(true), "Activity_Type_ID", "Description", -1);
            ViewData["Follow_User_ID"] = new SelectList(_dropRepository.GetAgents(false).Where(a => a.Active == true), "User_ID", "FullName", record.Agent_ID);
            ViewData["Follow_Up_Time"] = new SelectList(_dropRepository.GetTimes());

            //ViewData["Priority_Level"] = new SelectList(_dropRepository.GetPriorityLevels(false), "Key", "Value", activity.Priority_Level);
            //ViewData["Follow_Priority_Level"] = new SelectList(_dropRepository.GetPriorityLevels(false), "Key", "Value", 3);
            ViewData["CampaignDDL"] = new SelectList(_dropRepository.GetCampaignCodes(true), "Campaign_Code_ID", "Campaign_Description");

            ViewData["RecordNotes"] = new SelectList(_dropRepository.GetActivityNotes(user.User_ID, 0), "Record_Activity_Note_ID", "Description");
            ViewData["FollowRecordNotes"] = new SelectList(_dropRepository.GetActivityNotes(user.User_ID, 1), "Description", "Description");
            ViewData["Letter_ID"] = new SelectList(_dropRepository.GetLetters(true), "Letter_ID", "Description");
            ViewData["Reconciliation_Type_ID"] = new SelectList(ddlDisposition, "Reconciliation_Type_ID", "Description", -1);
           //ViewData["Reconciliation_Type_ID"] = new SelectList(_dropRepository.GetReconciliationTypes(true, complaintCurrentStep), "Reconciliation_Type_ID", "Description", -1);
            ViewData["Agent_Rating"] = new SelectList(_dropRepository.GetCallRatings(true), "Key", "Value", 0);
            ViewData["Manager_Rating"] = new SelectList(_dropRepository.GetCallRatings(true), "Key", "Value", 0);
            ViewData["Auto_Email"] = new SelectList(_dropRepository.GetAutoEmail(true), "Auto_Email_ID", "Notes", -1);
            ViewData["RecordEmail"] = record.Email_Address;
            ViewData["OptOutEmail"] = record.Opt_Out_Email;
            ViewData["OptOutNewsletter"] = record.Opt_Out_Newsletter;
            ViewData["CriticalTopicCoverageDDL"] = new SelectList(_dropRepository.GetCriticalTopicCoverage(true), "CTCID", "Description");
            ViewData["SalesRatingDDL"] = new SelectList(_dropRepository.GetSalesRating(true), "SEEID", "Description");
            ViewData["AdminDocumentationRatingDDL"] = new SelectList(_dropRepository.GetAdminDocumentationRating(true), "AdminDocumentationID", "Description");

            //LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            Record_Distribution AEPReminder = new Record_Distribution();
            AEPReminder = db.Record_Distributions.Where(rd => rd.Record_ID == transaction.Record_ID && rd.Record_Distribution_Type_ID == 1).FirstOrDefault();
            if (AEPReminder == null)
            {
                ViewData["AEP_Reminder"] = false;
            }
            else
            {
                ViewData["AEP_Reminder"] = true;
            }


            return View(activity);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult New(int id, FormCollection collection)
        {
            LMSDataContext db2 = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            Plan_Transaction planTransaction = _tranRepository.Get(id);
            Record_Distribution AEPReminder = new Record_Distribution();
            AEPReminder = db2.Record_Distributions.Where(rd => rd.Record_ID == planTransaction.Record_ID && rd.Record_Distribution_Type_ID == 1).FirstOrDefault();

            LMS.Data.User user = UserRepository.GetUser(User.Identity.Name);
            if (user == null)
                throw new HttpException(400, "Invalid User");
            if (planTransaction != null)
            {
                Record record = _recordRepository.GetRecord(planTransaction.Record_ID);
                var eMailActivityRecordID = 0;
                int complaintRecord_Activity_ID = 0;
                try
                {
                    if (record == null)
                        throw new HttpException(400, "Invalid Record ID");
                    List<sp_Has_ActivityResult> rs = db2.sp_Has_Activity(Convert.ToInt32(collection["Follow_User_ID"]), record.Record_ID).ToList();
                    bool? Has_Activity = rs[0].hasOCA;
                    //if (Has_Activity.HasValue && Has_Activity == true && !collection["SendApp"].Contains("true") && !collection["SendApp2"].Contains("true") && !collection["PreFill"].Contains("true"))
                    //{
                    //    ModelState.AddModelError("Follow_User_ID", "*");
                    //    ModelState.SetModelValue("Follow_User_ID", ValueProvider.GetValue("Follow_User_ID"));
                    //}

                    int current_Record_Activity_Status_ID = Convert.ToInt32(collection["Record_Activity_Status_ID"]);
                    int current_Reconciliation_Type_ID = Convert.ToInt32(collection["Reconciliation_Type_ID"]);

                    if (current_Reconciliation_Type_ID == 39)
                        current_Record_Activity_Status_ID = 33;
                    if (current_Reconciliation_Type_ID == 40)
                        current_Record_Activity_Status_ID = 35;
                    if (current_Reconciliation_Type_ID == 64)
                        current_Record_Activity_Status_ID = 34;


                    if (ModelState.IsValid)
                    {
                        string activityNotes = collection["Notes"];

                        using (var db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString))
                        {
                            var campaignCode = db.Campaign_Codes.FirstOrDefault(i => i.Campaign_Code_ID == Convert.ToInt32(collection["Campaign_Code"]));
                            if (campaignCode != null)
                            {
                                activityNotes = campaignCode.Campaign_Number + " - " + campaignCode.Campaign_Description + ". " + activityNotes;
                            }
                        }

                        Record_Activity activity = new Record_Activity();
                        //Set Initials
                        activity.Plan_Transaction_ID = Convert.ToInt32(collection["Plan_Transaction_ID"]);
                        activity.Created_By = user.User_ID;
                        activity.Created_On = DateTime.Now;
                        activity.User_ID = user.User_ID;
                        activity.Record_ID = record.Record_ID;
                        activity.Due_Date = DateTime.Now;
                        activity.Actual_Date = DateTime.Now;
                        activity.Activity_Type_ID = Convert.ToInt32(collection["Activity_Type_ID"]);
                        activity.Notes = StringHelper.EncryptString(activityNotes);
                        activity.Priority_Level = Convert.ToInt32(collection["Priority_Level"]);
                        activity.Record_Activity_Status_ID = Convert.ToInt32(collection["Record_Activity_Status_ID"]);
                        activity.Record_Activity_Note_ID = Convert.ToInt32(collection["RecordNotes"]);
                        activity.CTCID = Convert.ToInt32(collection["ctc_DDL"]);
                        activity.SEEID = Convert.ToInt32(collection["SEEID_DDL"]);
                        activity.AdminDocumentationID = Convert.ToInt32(collection["AdminDocumentationID_DDL"]);
                        activity.Campaign_Code_ID = Convert.ToInt32(collection["Campaign_Code"]);


                        _actRepository.Update(activity);
                        _actRepository.Save();

                        //int Record_Activity_Status_ID = Convert.ToInt32(collection["Record_Activity_Status_ID"]);


                        if (current_Record_Activity_Status_ID == 33 || current_Record_Activity_Status_ID == 34)
                        {
                            using (var db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString))
                            {
                                var complaint = new Complaint();

                                if (current_Record_Activity_Status_ID == 33)//CREATE OPEN COMPLAINT
                                {
                                    complaint = new Complaint()
                                    {
                                        Current_Record_Activity_Status_ID = current_Record_Activity_Status_ID,
                                        Record_ID = record.Record_ID,
                                        Resolved = false
                                    };

                                    db.Complaints.InsertOnSubmit(complaint);
                                    db.SubmitChanges(ConflictMode.ContinueOnConflict);
                                }
                                else
                                {
                                    //complaint = _recordRepository.GetComplaint(record.Record_ID);
                                    complaint = db.Complaints.Where(x => x.Record_ID == record.Record_ID && x.Resolved == false).FirstOrDefault();
                                    complaint.Current_Record_Activity_Status_ID = current_Record_Activity_Status_ID;
                                    db.SubmitChanges(ConflictMode.ContinueOnConflict);
                                }

                                if (current_Record_Activity_Status_ID == 33)
                                {
                                    //create followUP for Complaint
                                    var complaintFollowUp = new Record_Activity()
                                    {
                                        Plan_Transaction_ID = Convert.ToInt32(collection["Plan_Transaction_ID"]),
                                        Created_By = user.User_ID,
                                        Created_On = DateTime.Now,
                                        User_ID = 82,
                                        Record_ID = record.Record_ID,
                                        Due_Date = DateTime.Now,
                                        Actual_Date = null,
                                        Activity_Type_ID = Convert.ToInt32(collection["Activity_Type_ID"]),
                                        Notes = StringHelper.EncryptString(activityNotes),
                                        Priority_Level = Convert.ToInt32(collection["Priority_Level"]),
                                        Record_Activity_Status_ID = current_Record_Activity_Status_ID,
                                        Record_Activity_Note_ID = Convert.ToInt32(collection["RecordNotes"]),
                                        CTCID = Convert.ToInt32(collection["ctc_DDL"]),
                                        SEEID = Convert.ToInt32(collection["SEEID_DDL"]),
                                        AdminDocumentationID = Convert.ToInt32(collection["AdminDocumentationID_DDL"]),
                                        Campaign_Code_ID = Convert.ToInt32(collection["Campaign_Code"])
                                    };
                                    _actRepository.Update(complaintFollowUp);
                                    _actRepository.Save();
                                    complaintRecord_Activity_ID = complaintFollowUp.Record_Activity_ID;
                                }
                                else
                                {
                                    complaintRecord_Activity_ID = activity.Record_Activity_ID;
                                }

                                var complaintActivity = new Complaint_Activity()
                                {
                                    Complaint_ID = complaint.Complaint_ID,
                                    Record_Activity_ID = complaintRecord_Activity_ID,
                                    Record_Activity_Status_ID = current_Record_Activity_Status_ID,
                                };

                                db.Complaint_Activities.InsertOnSubmit(complaintActivity);
                                db.SubmitChanges(ConflictMode.ContinueOnConflict);
                            }
                        }

                        CallRating callrating = new CallRating();
                        //Set Values
                        callrating.Record_Activity_ID = activity.Record_Activity_ID;
                        callrating.Agent_Rating = Convert.ToInt32(collection["Agent_Rating"]);
                        callrating.Manager_Rating = Convert.ToInt32(collection["Manager_Rating"]);

                        if (callrating.Agent_Rating != -1)//check to see if rating needs to be saved
                        {
                            if (String.IsNullOrEmpty(collection["Call_of_Week"]) || collection["Call_of_Week"] == "false")
                            {
                                callrating.Call_of_Week = false;

                            }
                            else
                            {
                                callrating.Call_of_Week = true;
                            }
                            LMSDataContext _db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
                            _db.CallRatings.InsertOnSubmit(callrating);
                            _db.SubmitChanges(ConflictMode.ContinueOnConflict);
                        }


                        //if auto email check box checked create new record activity
                        if (!String.IsNullOrEmpty(collection["Include_Email"]))
                        {
                            if (collection["Include_Email"].StartsWith("true"))
                            {
                                Record_Activity tempAct = new Record_Activity();
                                Auto_Email aeNotes = db2.Auto_Emails.Where(ae => ae.Auto_Email_ID == Convert.ToInt32(collection["Auto_Email"])).FirstOrDefault();

                                tempAct.Created_By = user.User_ID;
                                tempAct.Created_On = DateTime.Now;
                                tempAct.User_ID = user.User_ID;
                                tempAct.Record_ID = record.Record_ID;
                                tempAct.Due_Date = DateTime.Now;
                                tempAct.Actual_Date = DateTime.Now;
                                tempAct.Activity_Type_ID = 15;
                                tempAct.Plan_Transaction_ID = activity.Plan_Transaction_ID;
                                tempAct.Priority_Level = activity.Priority_Level;
                                tempAct.Record_Activity_Status_ID = 13;
                                tempAct.Record_Activity_Note_ID = Convert.ToInt32(collection["RecordNotes"]);
                                tempAct.CTCID = Convert.ToInt32(collection["ctc_DDL"]);
                                tempAct.SEEID = Convert.ToInt32(collection["SEEID_DDL"]);
                                tempAct.AdminDocumentationID = Convert.ToInt32(collection["AdminDocumentationID_DDL"]);
                                tempAct.Campaign_Code_ID = Convert.ToInt32(collection["Campaign_Code"]);

                                if (aeNotes != null)
                                {
                                    tempAct.Notes = StringHelper.EncryptString(aeNotes.Notes);
                                    tempAct.Auto_Email_ID = aeNotes.Auto_Email_ID;
                                }
                                else
                                {
                                    tempAct.Notes = StringHelper.EncryptString("Email Sent");
                                }

                                LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

                                db.Record_Activities.InsertOnSubmit(tempAct);
                                db.SubmitChanges(ConflictMode.ContinueOnConflict);
                                eMailActivityRecordID = tempAct.Record_Activity_ID;

                                //If email selected from auto email dropdown box
                                if (aeNotes != null)
                                { 
                                    Plan plan = db.Plans.Where(p => p.Plan_ID == (db.Plan_Transactions.Where(at => at.Plan_Transaction_ID == activity.Plan_Transaction_ID)
                                        .FirstOrDefault()).Plan_ID).FirstOrDefault();
                                    Carrier carrier = null;
                                    if (plan != null)
                                    {
                                        carrier = db.Carriers.Where(c => c.Carrier_ID == plan.Carrier_ID).FirstOrDefault();
                                    }

                                    var planTypeDescription = string.Empty;
                                    if (plan != null)
                                    {
                                        var planTypes = db.Plan_Types.Where(y => y.Plan_Type_ID == plan.Plan_Type_ID).ToList();
                                        planTypeDescription = planTypes.Any()
                                            ? planTypes.First().Description
                                            : string.Empty;
                                    } 
                                    var date = DateTime.Today;
                                    var agent = string.Format("{0} {1}", user.First_Name, user.Last_Name);
                                    var carrierName = string.Empty;

                                    if (carrier != null)
                                    {
                                        if (!string.IsNullOrWhiteSpace(carrier.Name))
                                        {
                                            carrierName = carrier.Name;                                           
                                        }

                                    }

                                    var paymentMode = new object();
                                    var modalPremium = planTransaction.Modal_Premium;
                                    var modal = planTransaction.Modal;
                                   
                                    if (modal != null) {
                                        paymentMode = modal.Description;
                                    }
                                    if (paymentMode == null)
                                    {
                                        ViewData["PaymentMode"] = "";
                                    }
                                    else
                                    {
                                        ViewData["PaymentMode"] = paymentMode;
                                    }
                                    var subplan = planTransaction.Sub_Plan;
                                    if(subplan == null)
                                    {
                                        ViewData["SubPlan"] = "";
                                    }
                                    else
                                    {
                                        ViewData["SubPlan"] = subplan;
                                    }
                                                                        
                                    var effectiveDate = (planTransaction != null && planTransaction.Buying_Period_Effective_Date.HasValue)
                                    ? planTransaction.Buying_Period_Effective_Date.Value.ToString("MM/dd/yyyy")
                                    : DateTime.MinValue.ToString("MM/dd/yyyy");
                                    
                                    // When the email to be sent is 'Your Medicare Supplement Appt.' or 'Reminder:  Your Medicare Appt.'
                                    // change agent and date
                                    // TODO: this is silly! REFACTOR!
                                    var appointmentsSubjects = new List<string> { "Your Medicare Supplement Appt.", "Reminder:  Your Medicare Appt." };
                                    if (appointmentsSubjects.Contains(aeNotes.Subject))
                                    {
                                        var userId = Convert.ToInt32(collection["Follow_User_ID"]);
                                        var agentData = db.Users.Where(t => t.User_ID == userId).FirstOrDefault();
                                        agent = string.Format("{0} {1}", agentData.First_Name, agentData.Last_Name);
                                        date = Convert.ToDateTime(collection["Follow_Up_Date"] + " " + (collection["Follow_Up_Time"] == @"N\A" ? "12:00AM" : collection["Follow_Up_Time"]));
                                    }                                    

                                    string emailbody = aeNotes.Body.Replace("{RECORD_NAME}", string.Format("{0} {1}", record.First_Name, record.Last_Name));
                                    emailbody = emailbody.Replace("{agent first name last name}", agent);
                                    emailbody = emailbody.Replace("{date}", date.ToString("MM/dd/yyyy"));
                                    emailbody = emailbody.Replace("{YEAR}", date.Year.ToString());
                                    emailbody = emailbody.Replace("{carrier name}", carrierName);
                                    emailbody = emailbody.Replace("{type of plan}", planTypeDescription);
                                    emailbody = emailbody.Replace("{effective year}", effectiveDate);
                                    emailbody = emailbody.Replace("{name of company}", carrierName); //need to modify template parameter to {carrier name}
                                    emailbody = emailbody.Replace("{time}", date.ToString("hh:mm tt"));
                                    emailbody = emailbody.Replace("{day}", date.ToString("dd"));
                                    emailbody = emailbody.Replace("{month}", date.ToString("MM"));
                                    emailbody = emailbody.Replace("{modal premium}", string.Format("{0:0.00}", modalPremium));
                                    emailbody = emailbody.Replace("{mode}", paymentMode.ToString());
                                    emailbody = emailbody.Replace("{Sub Plan}", subplan.ToString());                                  
                                   

                                    var signature = "{0} {1}<br />Longevity Alliance, Inc.<br />5530 W. Chandler Blvd.<br />Chandler, AZ 85226<br />{2}<br />1-800-713-6610 {3}";
                                    emailbody = emailbody.Replace("{SC_SIGNATURE}", string.Format(signature, user.First_Name, user.Last_Name, user.Email_Address, user.Extension));


                                    emailbody = emailbody.Replace("ScheduleACall.aspx", "ScheduleACall.aspx?id=" + record.Customer_ID + "&email=" + record.Email_Address);


                                    if (!string.IsNullOrEmpty(record.Email_Address))
                                    {
                                        string emailTo = record.Email_Address;
                                        string emailFrom = user.Email_Address;
                                        string subject = aeNotes.Subject;
                                        string body = emailbody;
                                        LMS.ErrorHandler.HandleError.Email(emailTo, emailFrom, subject, body);
                                    }
                                }
                                
                            }

                        }

                        //Handle Flag for Reconciliation
                        if (collection["Reconciliation_Type_ID"] != "-1")
                        {
                            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

                            Reconciliation rec = new Reconciliation();
                            Reconciliation_Type type = db.Reconciliation_Types.Where(rt => rt.Reconciliation_Type_ID == Convert.ToInt32(collection["Reconciliation_Type_ID"])).FirstOrDefault();

                            if (type != null)
                            {
                                if (eMailActivityRecordID > 0)
                                {
                                    rec.Placed_By = user.User_ID;
                                    rec.Placed_Date = DateTime.Now;
                                    rec.Reconciliation_Type_ID = type.Reconciliation_Type_ID;
                                    rec.Record_ID = record.Record_ID;
                                    rec.Notes = "";
                                    rec.Record_Activity_ID = eMailActivityRecordID;

                                    db.Reconciliations.InsertOnSubmit(rec);
                                    db.SubmitChanges(ConflictMode.ContinueOnConflict);

                                    rec = new Reconciliation();
                                }

                                rec.Placed_By = user.User_ID;
                                rec.Placed_Date = DateTime.Now;
                                rec.Reconciliation_Type_ID = type.Reconciliation_Type_ID;
                                rec.Record_ID = record.Record_ID;
                                rec.Notes = "";
                                rec.Record_Activity_ID = activity.Record_Activity_ID;

                                db.Reconciliations.InsertOnSubmit(rec);
                                db.SubmitChanges(ConflictMode.ContinueOnConflict);


                                Plan_Transaction plan = db.Plan_Transactions.Where(at => at.Plan_Transaction_ID == activity.Plan_Transaction_ID).FirstOrDefault();
                                
                                List<Plan_Transaction> Plan_Transactions = db.Plan_Transactions.Where(pt => pt.Record_ID == record.Record_ID).ToList();
                                foreach (Plan_Transaction pt in Plan_Transactions)
                                {
                                    if (plan.Plan_Transaction_ID == pt.Plan_Transaction_ID && type.Plan_Transaction_Status_ID > 0)
                                    {
                                        pt.Plan_Transaction_Status_ID = type.Plan_Transaction_Status_ID;
                                        Plan_Transaction_Date ptd1 = db.Plan_Transaction_Dates.Where(d => d.Plan_Transaction_ID == plan.Plan_Transaction_ID && d.Transaction_Date_Type_ID == 2).FirstOrDefault();
                                        Plan_Transaction_Date ptd2 = db.Plan_Transaction_Dates.Where(d => d.Plan_Transaction_ID == plan.Plan_Transaction_ID && d.Transaction_Date_Type_ID == 8).FirstOrDefault();
                                        if (ptd1.Date_Value.HasValue && !ptd2.Date_Value.HasValue)
                                        {
                                            ptd2.Date_Value = DateTime.Now;
                                        }
                                    }
                                    db.SubmitChanges(ConflictMode.ContinueOnConflict);
                                }
                                if (type.Record_Type_ID.HasValue)
                                {
                                    if (type.Record_Type_ID > 0)
                                    {
                                        db.Set_Record_Type(rec.Record_ID, type.Record_Type_ID.Value);
                                    }
                                }

                                if (type.Reconciliation_Type_ID == 36) //Not eligible until AEP
                                {
                                    //set start date
                                    DateTime startdate;
                                    if (DateTime.Today != DateTime.Parse("1/1/" + DateTime.Today.Year.ToString()))
                                    {
                                        startdate = new DateTime(DateTime.Today.Year + 1, 1, 1);
                                    }
                                    else
                                    {
                                        startdate = new DateTime(DateTime.Today.Year, 1, 1);
                                    }
                                    db.sp_Set_Record_Start_Date(rec.Record_ID, startdate);
                                    db.sp_Set_Record_Situation(rec.Record_ID, 7);//set situation to AEP

                                    //set plan buying period to AEP
                                    plan.Buying_Period_ID = 2;
                                    db.SubmitChanges(ConflictMode.ContinueOnConflict);

                                    if (AEPReminder == null && Boolean.Parse(Request.Form.GetValues("AEP_Reminder")[0]) == true)
                                    {
                                        Record_Distribution Record_Distribution = new Record_Distribution();
                                        Record_Distribution.Record_ID = planTransaction.Record_ID;
                                        Record_Distribution.Record_Distribution_Type_ID = 1;
                                        db.Record_Distributions.InsertOnSubmit(Record_Distribution);
                                        db.SubmitChanges(ConflictMode.ContinueOnConflict);
                                    }
                                    else if (AEPReminder != null && Boolean.Parse(Request.Form.GetValues("AEP_Reminder")[0]) == false)
                                    {
                                        Record_Distribution Record_Distribution = new Record_Distribution();
                                        Record_Distribution = db.Record_Distributions.Where(rd => rd.Record_Distribution_Type_ID == 1 && rd.Record_ID == planTransaction.Record_ID).FirstOrDefault();
                                        db.Record_Distributions.DeleteOnSubmit(Record_Distribution);
                                        db.SubmitChanges(ConflictMode.ContinueOnConflict);
                                    }
                                    if (Boolean.Parse(Request.Form.GetValues("AEP_Reminder")[0]) == true)
                                    {
                                        //set a follow up
                                        Record_Activity Record_Activity = new Record_Activity();
                                        Record_Activity.Record_ID = planTransaction.Record_ID;
                                        Record_Activity.Activity_Type_ID = 2; //outbound call
                                        Record_Activity.Created_By = 36; //admin agent
                                        Record_Activity.Created_On = DateTime.Now;
                                        Record_Activity.User_ID = user.User_ID;
                                        Record_Activity.Made_Contact = false;
                                        Record_Activity.Priority_Level = 3;
                                        Record_Activity.Plan_Transaction_ID = planTransaction.Plan_Transaction_ID;
                                        Record_Activity.Notes = StringHelper.EncryptString("AEP Reminder permission to contact");
                                        Record_Activity.Record_Activity_Note_ID = Convert.ToInt32(collection["RecordNotes"]);
                                        Record_Activity.Campaign_Code_ID = Convert.ToInt32(collection["Campaign_Code"]);

                                        Random random = new Random();
                                        int followdate = random.Next(1, 14);
                                        Record_Activity.Due_Date = new DateTime(DateTime.Today.Year, 10, followdate);
                                        db.Record_Activities.InsertOnSubmit(Record_Activity);
                                        db.SubmitChanges(ConflictMode.ContinueOnConflict);

                                    }
                                }
                            }
                        }

                        //Handle Follow Up Date
                        if (!String.IsNullOrEmpty(collection["Follow_Up_Date"]) && collection["Follow_Up_Date"].Length > 0 && !collection["SendApp2"].StartsWith("true") && !collection["SendApp"].StartsWith("true"))
                        {
                            using (LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString))
                            {
                                Record_Activity act = new Record_Activity();
                                Activity_Type acttype = db.Activity_Types.Where(at => at.Activity_Type_ID == Convert.ToInt32(collection["Follow_Activity_Type_ID"])).FirstOrDefault();
                                var Record_Activity_Status_ID_FollowUp = Convert.ToInt32(collection["Record_Activity_Status_ID_FollowUp"]);

                                string activityNotesFollowUp = collection["Follow_Up_Notes"];
                                var campaignCode = db.Campaign_Codes.FirstOrDefault(i => i.Campaign_Code_ID == Convert.ToInt32(collection["Follow_Campaign_Code"]));
                                if (campaignCode != null)
                                {
                                    activityNotesFollowUp = campaignCode.Campaign_Number + " - " + campaignCode.Campaign_Description + ". " + activityNotesFollowUp;
                                }

                                act.Record_ID = record.Record_ID;
                                act.Created_On = DateTime.Now;
                                act.Created_By = user.User_ID;
                                act.Due_Date = Convert.ToDateTime(collection["Follow_Up_Date"] + " " + (collection["Follow_Up_Time"] == @"N\A" ? "12:00AM" : collection["Follow_Up_Time"]));
                                act.User_ID = Convert.ToInt32(collection["Follow_User_ID"]);
                                act.Priority_Level = Convert.ToInt32(collection["Follow_Priority_Level"]);
                                act.Record_Activity_Status_ID = 1;
                                act.Plan_Transaction_ID = Convert.ToInt32(collection["Plan_Transaction_ID"]);
                                act.Priority_Level = Convert.ToInt32(collection["Follow_Priority_Level"]);
                                act.Notes = StringHelper.EncryptString(activityNotesFollowUp);
                                act.Record_Activity_Note_ID = Convert.ToInt32(collection["RecordNotes"]);
                                act.Activity_Type_ID = Convert.ToInt32(collection["Follow_Activity_Type_ID"]);
                                act.Record_Activity_Status_ID = Record_Activity_Status_ID_FollowUp;
                                act.Campaign_Code_ID = Convert.ToInt32(collection["Follow_Campaign_Code"]);

                                _actRepository.Update(act);
                                _actRepository.Save();

                            };

                        }

                        if (collection["SendApp2"].StartsWith("true") || collection["SendApp"].StartsWith("true") || collection["SendInfo"].StartsWith("true") || collection["CancelApp"].StartsWith("true") || collection["PreFill"].StartsWith("true"))
                        {

                            //Handle Dates
                            if (collection["SendApp"].StartsWith("true"))
                            {
                                _tranRepository.UpdateSendAppDate(activity.Plan_Transaction_ID);
                                //Create record activity on partner if partner exists and plan status is "New".  Plan transaction will not let save a plan if no record activities on account.
                                if (record.Partner_ID > 0)
                                {
                                    LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
                                    Record_Activity tempAct = new Record_Activity();
                                    Record temprec = db.Records.Where(r => r.Record_ID == record.Partner_ID).FirstOrDefault();
                                    Plan_Transaction temppt = db.Plan_Transactions.Where(tpt => tpt.Record_ID == temprec.Record_ID && tpt.Plan_Transaction_Status_ID == 17).FirstOrDefault();

                                    if (temppt != null)
                                    {
                                        tempAct.Created_By = user.User_ID;
                                        tempAct.Created_On = DateTime.Now;
                                        tempAct.User_ID = user.User_ID;
                                        tempAct.Record_ID = temprec.Record_ID;
                                        tempAct.Due_Date = DateTime.Now;
                                        tempAct.Actual_Date = DateTime.Now;
                                        tempAct.Activity_Type_ID = 10;
                                        tempAct.Plan_Transaction_ID = temppt.Plan_Transaction_ID;
                                        tempAct.Priority_Level = 3;
                                        tempAct.Record_Activity_Status_ID = 1;
                                        tempAct.Notes = StringHelper.EncryptString("Please see partner record for notes.");
                                        tempAct.Record_Activity_Note_ID = Convert.ToInt32(collection["RecordNotes"]);
                                        tempAct.Campaign_Code_ID = Convert.ToInt32(collection["Campaign_Code"]);

                                        db.Record_Activities.InsertOnSubmit(tempAct);

                                        db.SubmitChanges(ConflictMode.ContinueOnConflict);
                                    }
                                }
                            }
                            if (collection["SendApp2"].StartsWith("true"))
                            {
                                _tranRepository.UpdateAppSentDateAndAppRequestDate(activity.Plan_Transaction_ID);

                            }
                            if (collection["PreFill"].StartsWith("true"))
                            {
                                _tranRepository.UpdatePreFillDate(activity.Plan_Transaction_ID);

                            }
                            if (collection["SendInfo"].StartsWith("true"))
                                _tranRepository.UpdateSendInfoDate(activity.Plan_Transaction_ID);
                            if (collection["CancelApp"].StartsWith("true"))
                            {
                                _tranRepository.UpdateAppCancelledDate(activity.Plan_Transaction_ID);
                                return RedirectToAction("Details", "Record", new { id = record.Record_ID });
                            }

                            if (activity.Plan_Transaction.Group.Group_ID == 1)
                            {
                                return RedirectToAction("Health", "Transaction", new { id = activity.Plan_Transaction_ID });
                            }
                            if (activity.Plan_Transaction.Group.Group_ID == 4)
                            {
                                return RedirectToAction("Annuity", "Transaction", new { id = activity.Plan_Transaction_ID });
                            }
                            else
                            {
                                return RedirectToAction("Life", "Transaction", new { id = activity.Plan_Transaction_ID });
                            }
                        }
                        else
                        {
                            return RedirectToAction("Details", "Record", new { id = planTransaction.Record_ID });
                        }
                    }
                    else
                    {
                        Record_Activity activity = new Record_Activity();

                        ViewData["DisplayRecordActivitiesList"] = _actRepository.GetRecordActivitiesDisplay(record.Record_ID);

                        ViewData["Record_Type_ID"] = record.Record_Type_ID;

                        Record_Activity futureAct = record.Record_Activities.Where(ra => ra.Actual_Date.HasValue == false && ra.Record_ID == record.Record_ID && ra.Record_Activity_ID != activity.Record_Activity_ID).OrderByDescending(ra => ra.Actual_Date).FirstOrDefault();

                        if (futureAct == null)
                        {
                            ViewData["Next_Activity_Date"] = "";
                        }
                        else
                        {
                            ViewData["Next_Activity_Date"] = futureAct.Due_Date;
                        }

                        //Set Initials
                        activity.Created_By = user.User_ID;
                        activity.Created_On = DateTime.Now;
                        activity.User_ID = user.User_ID;
                        //activity.Activity_Type_ID = 2;
                        activity.Priority_Level = record.Priority_Level;
                        activity.Plan_Transaction_ID = id;
                        activity.Record_ID = record.Record_ID;

                        ViewData["Record"] = record;
                        ViewData["RecordIDValue"] = record.Record_ID;
                        ViewData["Record_Activities"] = record.Record_Activities.OrderByDescending(c => c.Actual_Date);
                        ViewData["Activity_Type_ID"] = new SelectList(_dropRepository.GetActivityTypes(true), "Activity_Type_ID", "Description", -1);
                        ViewData["Record_Activity_Status_ID"] = new SelectList(_dropRepository.GetActivityStatus(true, null), "Record_Activity_Status_ID", "Description", -1);
                        //ViewData["Priority_Level"] = new SelectList(_dropRepository.GetPriorityLevels(false), "Key", "Value", activity.Priority_Level);

                        ViewData["Plan_Transaction_ID"] = new SelectList(_dropRepository.GetOpportunities(record.Record_ID), "Plan_Transaction_ID", "DropDownName", activity.Plan_Transaction_ID);
                        ViewData["Follow_Activity_Type_ID"] = new SelectList(_dropRepository.GetFollowUpType(true), "Activity_Type_ID", "Description", -1);
                        ViewData["Follow_User_ID"] = new SelectList(_dropRepository.GetAgents(false).Where(a => a.Active == true), "User_ID", "FullName", record.Agent_ID);
                        ViewData["Follow_Up_Time"] = new SelectList(_dropRepository.GetTimes());
                        //ViewData["Follow_Priority_Level"] = new SelectList(_dropRepository.GetPriorityLevels(false), "Key", "Value", 3);
                        ViewData["CampaignDDL"] = new SelectList(_dropRepository.GetCampaignCodes(true), "Campaign_Code_ID", "Campaign_Description");
                        ViewData["RecordNotes"] = new SelectList(_dropRepository.GetActivityNotes(user.User_ID, 0), "Record_Activity_Note_ID", "Description");
                        ViewData["FollowRecordNotes"] = new SelectList(_dropRepository.GetActivityNotes(user.User_ID, 1), "Description", "Description");
                        ViewData["Letter_ID"] = new SelectList(_dropRepository.GetLetters(true), "Letter_ID", "Description");
                        ViewData["Reconciliation_Type_ID"] = new SelectList(_dropRepository.GetReconciliationTypes(true, null), "Reconciliation_Type_ID", "Description", -1);
                        ViewData["Agent_Rating"] = new SelectList(_dropRepository.GetCallRatings(true), "Key", "Value", 0);
                        ViewData["Manager_Rating"] = new SelectList(_dropRepository.GetCallRatings(true), "Key", "Value", 0);
                        ViewData["Auto_Email"] = new SelectList(_dropRepository.GetAutoEmail(true), "Auto_Email_ID", "Notes", -1);
                        ViewData["RecordEmail"] = record.Email_Address;
                        ViewData["OptOutEmail"] = record.Opt_Out_Email;
                        ViewData["OptOutNewsletter"] = record.Opt_Out_Newsletter;

                        if (AEPReminder == null)
                        {
                            ViewData["AEP_Reminder"] = false;
                        }
                        else
                        {
                            ViewData["AEP_Reminder"] = true;
                        }
                        return View(activity);
                    }
                }
                catch (System.Exception ex)
                {
                    LMS.ErrorHandler.HandleError.EmailError(ex);
                    return View("New", new { id = planTransaction.Record_ID });
                }
            }
            else
            {
                return View("New", new { id = id });
            }

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Complete(int id, FormCollection collection)
        {
            LMSDataContext _db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            Plan_Transaction transaction = _tranRepository.Get(id);
            LMS.Data.User user = UserRepository.GetUser(User.Identity.Name);
            Record_Activity dbActivity = _db.Record_Activities.Where(ac => ac.Record_Activity_ID == id).FirstOrDefault();
            Record record = _recordRepository.GetRecord(dbActivity.Record_ID);
            CallRating callrating = _callRepository.GetCallRatingByActivityID(id);
            var complaint = new Complaint();
            Record_Distribution AEPReminder = new Record_Distribution();
            AEPReminder = _db.Record_Distributions.Where(rd => rd.Record_ID == dbActivity.Record_ID && rd.Record_Distribution_Type_ID == 1).FirstOrDefault();

            int complaintRecord_Activity_ID = 0;
            int complaintFUActivty_ID = 0;
            int current_Record_Activity_Status_ID = Convert.ToInt32(collection["Record_Activity_Status_ID"]);
            int current_Reconciliation_Type_ID = Convert.ToInt32(collection["Reconciliation_Type_ID"]);

            if (current_Reconciliation_Type_ID == 39)
                current_Record_Activity_Status_ID = 33;
            if (current_Reconciliation_Type_ID == 40)
                current_Record_Activity_Status_ID = 35;
            if (current_Reconciliation_Type_ID == 64)
                current_Record_Activity_Status_ID = 34;

            if (dbActivity.Plan_Transaction_ID > 0)
            {
                var eMailActivityRecordID = 0;
                try
                {
                    string activityNotes = collection["Notes"];
                    if (dbActivity.Campaign_Code_ID != Convert.ToInt32(collection["Campaign_Code"]))
                    {
                        using (var db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString))
                        {
                            var campaignCode = db.Campaign_Codes.FirstOrDefault(i => i.Campaign_Code_ID == Convert.ToInt32(collection["Campaign_Code"]));
                            if (campaignCode != null)
                            {
                                activityNotes = campaignCode.Campaign_Number + " - " + campaignCode.Campaign_Description + ". " + activityNotes;
                            }
                        }
                    }

                    try
                    {
                        dbActivity.Record_Activity_Status_ID = Convert.ToInt32(collection["Record_Activity_Status_ID"]);
                        dbActivity.Notes = StringHelper.EncryptString(activityNotes);
                        dbActivity.Actual_Date = DateTime.Now;
                        dbActivity.Priority_Level = Convert.ToInt32(collection["Priority_Level"]);
                        dbActivity.Activity_Type_ID = Int32.Parse(collection["Activity_Type_ID"]);
                        dbActivity.Plan_Transaction_ID = Int32.Parse(collection["Plan_Transaction_ID"]);
                        if (dbActivity.GBS_Notes_ID == null)
                        {
                            dbActivity.GBS_Notes_ID = null;
                        }

                        dbActivity.Record_Activity_Note_ID = Convert.ToInt32(collection["RecordNotes"]);
                        dbActivity.CTCID = Convert.ToInt32(collection["ctc_DDL"]);
                        dbActivity.SEEID = Convert.ToInt32(collection["SEEID_DDL"]);
                        dbActivity.AdminDocumentationID = Convert.ToInt32(collection["AdminDocumentationID_DDL"]);
                        dbActivity.Campaign_Code_ID = Convert.ToInt32(collection["Campaign_Code"]);

                        _db.SubmitChanges(ConflictMode.ContinueOnConflict);
                    }
                    catch (ChangeConflictException ex)
                    {
                        _db.ChangeConflicts.ResolveAll(RefreshMode.KeepCurrentValues);
                        _db.SubmitChanges(ConflictMode.ContinueOnConflict);
                    }

                    if (current_Record_Activity_Status_ID == 33 || current_Record_Activity_Status_ID == 34 || current_Record_Activity_Status_ID == 35)
                    {
                        using (var dbcomplaint = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString))
                        {
                            DateTime? actualDate = null;


                            if (current_Record_Activity_Status_ID == 33)//CREATE OPEN COMPLAINT
                            {
                                complaint = new Complaint()
                                {
                                    Current_Record_Activity_Status_ID = current_Record_Activity_Status_ID,
                                    Record_ID = record.Record_ID,
                                    Resolved = false
                                };

                                dbcomplaint.Complaints.InsertOnSubmit(complaint);
                                dbcomplaint.SubmitChanges(ConflictMode.ContinueOnConflict);
                            }
                            else
                            {
                                complaint = dbcomplaint.Complaints.Where(x => x.Record_ID == record.Record_ID && x.Resolved == false).FirstOrDefault();
                                if (current_Record_Activity_Status_ID == 35)
                                {
                                    complaint.Resolved = true;
                                    actualDate = DateTime.Now;
                                }

                                complaint.Current_Record_Activity_Status_ID = current_Record_Activity_Status_ID;
                                dbcomplaint.SubmitChanges(ConflictMode.ContinueOnConflict);
                            }

                            if (current_Record_Activity_Status_ID == 33)
                            {
                                //create followUP for Complaint
                                var complaintFollowUp = new Record_Activity()
                                {
                                    Plan_Transaction_ID = Convert.ToInt32(collection["Plan_Transaction_ID"]),
                                    Created_By = user.User_ID,
                                    Created_On = DateTime.Now,
                                    User_ID = 82,
                                    Record_ID = record.Record_ID,
                                    Due_Date = DateTime.Now,
                                    Actual_Date = actualDate,
                                    Activity_Type_ID = Convert.ToInt32(collection["Activity_Type_ID"]),
                                    Notes = StringHelper.EncryptString(activityNotes),
                                    Priority_Level = Convert.ToInt32(collection["Priority_Level"]),
                                    Record_Activity_Status_ID = current_Record_Activity_Status_ID,
                                    Record_Activity_Note_ID = Convert.ToInt32(collection["RecordNotes"]),
                                    CTCID = Convert.ToInt32(collection["ctc_DDL"]),
                                    SEEID = Convert.ToInt32(collection["SEEID_DDL"]),
                                    AdminDocumentationID = Convert.ToInt32(collection["AdminDocumentationID_DDL"]),
                                    Campaign_Code_ID = Convert.ToInt32(collection["Campaign_Code"])
                                };
                                _actRepository.Update(complaintFollowUp);
                                _actRepository.Save();
                                complaintRecord_Activity_ID = complaintFollowUp.Record_Activity_ID;
                            }
                            else
                            {
                                complaintRecord_Activity_ID = dbActivity.Record_Activity_ID;
                            }

                            var complaintActivity = new Complaint_Activity()
                            {
                                Complaint_ID = complaint.Complaint_ID,
                                Record_Activity_ID = complaintRecord_Activity_ID,
                                Record_Activity_Status_ID = current_Record_Activity_Status_ID,
                            };

                            dbcomplaint.Complaint_Activities.InsertOnSubmit(complaintActivity);
                            dbcomplaint.SubmitChanges(ConflictMode.ContinueOnConflict);

                        };
                    }

                    if (!String.IsNullOrEmpty(collection["Include_Email"]))
                    {
                        if (collection["Include_Email"].StartsWith("true"))
                        {
                            var tempAct = new Record_Activity();
                            var aeNotes = _db.Auto_Emails.FirstOrDefault(ae => ae.Auto_Email_ID == Convert.ToInt32(collection["Auto_Email"]));

                            tempAct.Created_By = user.User_ID;
                            tempAct.Created_On = DateTime.Now;
                            tempAct.User_ID = user.User_ID;
                            tempAct.Record_ID = record.Record_ID;
                            tempAct.Due_Date = DateTime.Now;
                            tempAct.Actual_Date = DateTime.Now;
                            tempAct.Activity_Type_ID = 15;
                            tempAct.Plan_Transaction_ID = dbActivity.Plan_Transaction_ID;
                            tempAct.Priority_Level = dbActivity.Priority_Level;
                            tempAct.Record_Activity_Status_ID = 13;
                            tempAct.CTCID = Convert.ToInt32(collection["ctc_DDL"]);
                            tempAct.SEEID = Convert.ToInt32(collection["SEEID_DDL"]);
                            tempAct.AdminDocumentationID = Convert.ToInt32(collection["AdminDocumentationID_DDL"]);
                            tempAct.Campaign_Code_ID = Convert.ToInt32(collection["Campaign_Code"]);

                            if (aeNotes != null)
                            {
                                tempAct.Notes = StringHelper.EncryptString(aeNotes.Notes);
                                tempAct.Auto_Email_ID = aeNotes.Auto_Email_ID;
                            }
                            else
                            {
                                tempAct.Notes = StringHelper.EncryptString("Email Sent");
                            }
                            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
                            db.Record_Activities.InsertOnSubmit(tempAct);
                            db.SubmitChanges(ConflictMode.ContinueOnConflict);
                            eMailActivityRecordID = tempAct.Record_Activity_ID;


                            if (aeNotes != null)
                            {
                                Plan_Transaction planTransaction = db.Plan_Transactions.FirstOrDefault(pt => pt.Plan_Transaction_ID == dbActivity.Plan_Transaction_ID);

                                Plan plan = db.Plans.Where(p => p.Plan_ID == (
                                    db.Plan_Transactions.Where(pt => pt.Plan_Transaction_ID == dbActivity.Plan_Transaction_ID).FirstOrDefault())
                                    .Plan_ID).FirstOrDefault();
                                string planTypeDescription = string.Empty;
                                Carrier carrier = null;
                                if (plan != null)
                                {
                                    carrier = db.Carriers.Where(c => c.Carrier_ID == plan.Carrier_ID).FirstOrDefault();
                                    planTypeDescription = db.Plan_Types.Any(y => y.Plan_Type_ID == plan.Plan_Type_ID)
                                    ? db.Plan_Types.Where(y => y.Plan_Type_ID == plan.Plan_Type_ID).First().Description
                                    : string.Empty;
                                }

                                

                                var date = DateTime.Today;
                                var agent = string.Format("{0} {1}", user.First_Name, user.Last_Name);
                                //Buying_Period_Effective_date all values in table are NULL
                                //var effectiveDate = transaction != null
                                //    ? transaction.Buying_Period_Effective_Date.Value.ToString("MM/dd/yyyy")
                                //    : DateTime.MinValue.ToString("MM/dd/yyyy");

                                var effectiveDate = string.Empty;
                                if (planTransaction.Buying_Period_Effective_Date != null)
                                {
                                    effectiveDate = planTransaction != null
                                        ? planTransaction.Buying_Period_Effective_Date.Value.ToString("MM/dd/yyyy")
                                        : DateTime.MinValue.ToString("MM/dd/yyyy");
                                }

                                /*
                                var effectiveDate = (planTransaction != null && planTransaction.Buying_Period_Effective_Date.HasValue)
                                    ? planTransaction.Buying_Period_Effective_Date.Value.ToString("MM/dd/yyyy")
                                    : DateTime.MinValue.ToString("MM/dd/yyyy");
                                */
                                var carrierName = string.Empty;
                                if (carrier != null)
                                {
                                    if (!string.IsNullOrWhiteSpace(carrier.Name))
                                    {
                                        carrierName = carrier.Name;
                                    }
                                }
                                else
                                {
                                    ViewData["Carrier"] = "";
                                }

                                // When the email to be sent is 'Your Medicare Supplement Appt.' or 'Reminder:  Your Medicare Appt.'
                                // change agent and date
                                // TODO: this is silly! REFACTOR!

                                //Medicare Supplment appt
                                var medSuppAppt = "Your Medicare Supplement Appt.";
                                if (aeNotes.Subject.Contains(medSuppAppt))
                                {
                                    var userId = Convert.ToInt32(collection["Follow_User_ID"]);
                                    var agentData = db.Users.Where(t => t.User_ID == userId).FirstOrDefault();
                                    agent = string.Format("{0} {1}", agentData.First_Name, agentData.Last_Name);

                                    var dateString = collection["Follow_Up_Date"] + " " + (collection["Follow_Up_Time"] == @"N\A" ? "12:00AM" : collection["Follow_Up_Time"]);
                                    if(!DateTime.TryParse(dateString, out date))
                                    {
                                        date = DateTime.MinValue;
                                    }
                                    
                                }


                                var modalPremium = planTransaction != null ? planTransaction.Modal_Premium : 0.00m;
                                var subplan = planTransaction != null ? planTransaction.Sub_Plan : string.Empty;
                                var paymentMode = string.Empty;
                                if (planTransaction != null)
                                {
                                     paymentMode = planTransaction.Modal != null ? planTransaction.Modal.Description : string.Empty;
                                }
                                
                                /*if (appointmentsSubjects.Contains(aeNotes.Subject))
                                    {
                                        var userId = Convert.ToInt32(collection["Follow_User_ID"]);
                                        var agentData = db.Users.Where(t => t.User_ID == userId).FirstOrDefault();
                                        agent = string.Format("{0} {1}", agentData.First_Name, agentData.Last_Name);
                                        date = Convert.ToDateTime(collection["Follow_Up_Date"] + " " + (collection["Follow_Up_Time"] == @"N\A" ? "12:00AM" : collection["Follow_Up_Time"]));
                                    }  */

                                //Medicare appt Reminder
                                var medSuppApptReminder = "Reminder:  Your Medicare Appt.";
                                if (aeNotes.Subject.Contains(medSuppApptReminder))
                                {
                                    Record_Activity apptra = db.Record_Activities.Where(a => a.Plan_Transaction_ID == dbActivity.Plan_Transaction_ID && a.Activity_Type_ID == 22 && a.Actual_Date ==null).FirstOrDefault();
                                    if (apptra.Due_Date != null)
                                    {
                                        var userId = Convert.ToInt32(collection["Follow_User_ID"]);
                                        var agentData = db.Users.Where(t => t.User_ID == userId).FirstOrDefault();
                                        agent = string.Format("{0} {1}", agentData.First_Name, agentData.Last_Name);
                                        date = apptra.Due_Date.HasValue ? apptra.Due_Date.Value : DateTime.MinValue;
                                    }
                                   
                                                                      
                                }  

                                string emailbody = aeNotes.Body.Replace("{RECORD_NAME}", string.Format("{0} {1}", record.First_Name, record.Last_Name));
                                emailbody = emailbody.Replace("{agent first name last name}", agent);
                                emailbody = emailbody.Replace("{date}", date.ToString("MM/dd/yyyy"));
                                emailbody = emailbody.Replace("{YEAR}", date.Year.ToString());
                                emailbody = emailbody.Replace("{carrier name}", carrierName);
                                emailbody = emailbody.Replace("{type of plan}", planTypeDescription);
                                emailbody = emailbody.Replace("{effective year}", effectiveDate);
                                emailbody = emailbody.Replace("{name of company}", carrierName); //need to modify template parameter to {carrier name}                                
                                emailbody = emailbody.Replace("{time}", date.ToString("hh:mm tt"));
                                emailbody = emailbody.Replace("{day}", date.ToString("dd"));
                                emailbody = emailbody.Replace("{month}", date.ToString("MM"));
                                emailbody = emailbody.Replace("{modal premium}", string.Format("{0:0.00}", modalPremium));
                                emailbody = emailbody.Replace("{mode}", paymentMode.ToString());
                                emailbody = emailbody.Replace("{Sub Plan}", subplan.ToString()); 

                                var signature = "{0} {1}<br />Longevity Alliance, Inc.<br />5530 W. Chandler Blvd.<br />Chandler, AZ 85226<br />{2}<br />1-800-713-6610 {3}";
                                emailbody = emailbody.Replace("{SC_SIGNATURE}", string.Format(signature, user.First_Name, user.Last_Name, user.Email_Address, user.Extension));


                                emailbody = emailbody.Replace("ScheduleACall.aspx", "ScheduleACall.aspx?id=" + record.Customer_ID + "&email=" + record.Email_Address);


                                if (!string.IsNullOrEmpty(record.Email_Address))
                                {
                                    string emailTo = record.Email_Address;
                                    string emailFrom = user.Email_Address;
                                    string subject = aeNotes.Subject;
                                    string body = emailbody;
                                    LMS.ErrorHandler.HandleError.Email(emailTo, emailFrom, subject, body);
                                }
                            }
                        }
                    }

                    if (!String.IsNullOrEmpty(collection["Follow_Up_Date"]) && collection["Follow_Up_Date"].Length > 0 && !collection["SendApp2"].StartsWith("true") && !collection["SendApp"].StartsWith("true"))
                    {
                        using (var db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString))
                        {

                            Record_Activity act = new Record_Activity();

                            var activityNotesFollowup = collection["Follow_Up_Notes"];

                            var campaignCode = db.Campaign_Codes.FirstOrDefault(i => i.Campaign_Code_ID == Convert.ToInt32(collection["Follow_Campaign_Code"]));
                            if (campaignCode != null)
                            {
                                activityNotesFollowup = campaignCode.Campaign_Number + " - " + campaignCode.Campaign_Description + ". " + activityNotesFollowup;
                            }

                            if (collection["Reconciliation_Type_ID"] != "64")
                            {
                                // Activity_Type acttype = db.Activity_Types.Where(at => at.Activity_Type_ID == Convert.ToInt32(collection["Follow_Activity_Type_ID"])).FirstOrDefault();

                                act.Created_On = DateTime.Now;
                                act.Record_ID = record.Record_ID;

                                act.Created_By = user.User_ID;
                                act.Due_Date = Convert.ToDateTime(collection["Follow_Up_Date"] + " " + (collection["Follow_Up_Time"] == @"N\A" ? "12:00AM" : collection["Follow_Up_Time"]));
                                act.User_ID = Convert.ToInt32(collection["Follow_User_ID"]);
                                act.Priority_Level = Convert.ToInt32(collection["Follow_Priority_Level"]);
                                act.Record_Activity_Status_ID = 0;
                                act.Plan_Transaction_ID = Convert.ToInt32(collection["Plan_Transaction_ID"]);
                                act.Activity_Type_ID = Convert.ToInt32(collection["Follow_Activity_Type_ID"]);
                                act.Record_Activity_Status_ID = Convert.ToInt32(collection["Record_Activity_Status_ID_FollowUp"]);
                                act.Campaign_Code_ID = Convert.ToInt32(collection["Follow_Campaign_Code"]);

                                //act.CTCID = Convert.ToInt32(collection["ctc_DDL"]);
                                //act.SEEID = Convert.ToInt32(collection["SEEID_DDL"]);
                                //act.AdminDocumentationID = Convert.ToInt32(collection["AdminDocumentationID_DDL"]);

                                act.Notes = StringHelper.EncryptString(activityNotesFollowup);
                                _actRepository.Update(act);
                                _actRepository.Save();
                            }
                            else
                            {
                                act.Created_On = DateTime.Now;
                                act.Record_ID = record.Record_ID;

                                act.Created_By = user.User_ID;
                                act.Due_Date = Convert.ToDateTime(collection["Follow_Up_Date"] + " " + (collection["Follow_Up_Time"] == @"N\A" ? "12:00AM" : collection["Follow_Up_Time"]));
                                //act.User_ID = 82;//Nancy for now hard coded .. need to update with dynamic solution
                                act.User_ID = Convert.ToInt32(collection["Follow_User_ID"]);
                                act.Priority_Level = Convert.ToInt32(collection["Follow_Priority_Level"]);
                                act.Record_Activity_Status_ID = 34;
                                act.Plan_Transaction_ID = Convert.ToInt32(collection["Plan_Transaction_ID"]);
                                act.Activity_Type_ID = Convert.ToInt32(collection["Follow_Activity_Type_ID"]);
                                act.Campaign_Code_ID = Convert.ToInt32(collection["Follow_Campaign_Code"]);
                                act.Notes = StringHelper.EncryptString(activityNotesFollowup);
                                _actRepository.Update(act);
                                _actRepository.Save();
                                complaintFUActivty_ID = act.Record_Activity_ID;


                                var complaintActivity = new Complaint_Activity()
                                {
                                    Complaint_ID = complaint.Complaint_ID,
                                    Record_Activity_ID = complaintFUActivty_ID,
                                    Record_Activity_Status_ID = act.Record_Activity_Status_ID,
                                };

                                db.Complaint_Activities.InsertOnSubmit(complaintActivity);
                                db.SubmitChanges(ConflictMode.ContinueOnConflict);


                            }
                        };
                    }

                    if (callrating == null)//Rating does not exist need to create new one
                    {
                        callrating = new CallRating();
                        //Set Values
                        callrating.Record_Activity_ID = dbActivity.Record_Activity_ID;
                        callrating.Agent_Rating = Convert.ToInt32(collection["Agent_Rating"]);
                        callrating.Manager_Rating = Convert.ToInt32(collection["Manager_Rating"]);

                        if (callrating.Agent_Rating != -1)//check to see if rating needs to be saved
                        {
                            if (String.IsNullOrEmpty(collection["Call_of_Week"]) || collection["Call_of_Week"] == "false")
                            {
                                callrating.Call_of_Week = false;

                            }
                            else
                            {

                                callrating.Call_of_Week = true;
                            }
                            _db.CallRatings.InsertOnSubmit(callrating);
                            _db.SubmitChanges(ConflictMode.ContinueOnConflict);
                        }
                    }
                    else//rating exists just need to update
                    {
                        UpdateModel(callrating);
                        callrating.Manager_Rating = Convert.ToInt32(collection["Manager_Rating"]);
                        callrating.Agent_Rating = Convert.ToInt32(collection["Agent_Rating"]);

                        if (String.IsNullOrEmpty(collection["Call_of_Week"]) || collection["Call_of_Week"] == "false")
                        {
                            callrating.Call_of_Week = false;
                        }
                        else
                        {

                            callrating.Call_of_Week = true;
                        }
                        _callRepository.Save();
                    }


                    //Handle Flag for Reconciliation
                    if (collection["Reconciliation_Type_ID"] != "-1")
                    {
                        LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
                        Reconciliation rec = new Reconciliation();
                        Reconciliation_Type type = db.Reconciliation_Types.Where(rt => rt.Reconciliation_Type_ID == Convert.ToInt32(collection["Reconciliation_Type_ID"])).FirstOrDefault();

                        if (type != null)
                        {
                            if (eMailActivityRecordID > 0)
                            {
                                rec.Placed_By = user.User_ID;
                                rec.Placed_Date = DateTime.Now;
                                rec.Reconciliation_Type_ID = type.Reconciliation_Type_ID;
                                rec.Record_ID = record.Record_ID;
                                rec.Notes = "";
                                rec.Record_Activity_ID = eMailActivityRecordID;

                                db.Reconciliations.InsertOnSubmit(rec);
                                db.SubmitChanges(ConflictMode.ContinueOnConflict);

                                rec = new Reconciliation();
                            }

                            DateTime? resolvedDate = null;
                            if (current_Record_Activity_Status_ID == 35)
                                resolvedDate = DateTime.Now;

                            rec.Placed_By = user.User_ID;
                            rec.Placed_Date = DateTime.Now;
                            rec.Reconciliation_Type_ID = type.Reconciliation_Type_ID;
                            rec.Record_ID = record.Record_ID;
                            rec.Notes = "";
                            rec.Record_Activity_ID = dbActivity.Record_Activity_ID;

                            db.Reconciliations.InsertOnSubmit(rec);
                            db.SubmitChanges(ConflictMode.ContinueOnConflict);
                            Plan_Transaction plan = db.Plan_Transactions.Where(at => at.Plan_Transaction_ID == dbActivity.Plan_Transaction_ID).FirstOrDefault();
                            List<Plan_Transaction> Plan_Transactions = db.Plan_Transactions.Where(pt => pt.Record_ID == record.Record_ID).ToList();
                            foreach (Plan_Transaction pt in Plan_Transactions)
                            {
                                if (plan.Plan_Transaction_ID == pt.Plan_Transaction_ID && type.Plan_Transaction_Status_ID > 0)
                                {
                                    pt.Plan_Transaction_Status_ID = type.Plan_Transaction_Status_ID;
                                    Plan_Transaction_Date ptd1 = db.Plan_Transaction_Dates.Where(d => d.Plan_Transaction_ID == plan.Plan_Transaction_ID && d.Transaction_Date_Type_ID == 2).FirstOrDefault();
                                    Plan_Transaction_Date ptd2 = db.Plan_Transaction_Dates.Where(d => d.Plan_Transaction_ID == plan.Plan_Transaction_ID && d.Transaction_Date_Type_ID == 8).FirstOrDefault();
                                    if (ptd1.Date_Value.HasValue && !ptd2.Date_Value.HasValue)
                                    {
                                        ptd2.Date_Value = DateTime.Now;
                                    }
                                }
                                //if (pt.Plan_Transaction_Status_ID == 1)//app sent
                                //{
                                //    pt.Plan_Transaction_Status_ID = 18; //app sent cancelled
                                //    Plan_Transaction_Date ptd = db.Plan_Transaction_Dates.Where(d => d.Plan_Transaction_ID == pt.Plan_Transaction_ID && d.Transaction_Date_Type_ID == 8).FirstOrDefault();
                                //    ptd.Date_Value = DateTime.Now;
                                //}
                                db.SubmitChanges(ConflictMode.ContinueOnConflict);
                            }

                            if (type.Record_Type_ID.HasValue)
                            {
                                if (type.Record_Type_ID > 0)
                                {
                                    db.Set_Record_Type(rec.Record_ID, type.Record_Type_ID.Value);
                                }
                            }
                            if (type.Reconciliation_Type_ID == 36) //Not eligible until AEP
                            {
                                //set start date
                                DateTime startdate;
                                if (DateTime.Today != DateTime.Parse("1/1/" + DateTime.Today.Year.ToString()))
                                {
                                    startdate = new DateTime(DateTime.Today.Year + 1, 1, 1);
                                }
                                else
                                {
                                    startdate = new DateTime(DateTime.Today.Year, 1, 1);
                                }
                                db.sp_Set_Record_Start_Date(rec.Record_ID, startdate);
                                db.sp_Set_Record_Situation(rec.Record_ID, 7);//set situation to AEP
                                //set plan buying period to AEP
                                plan.Buying_Period_ID = 2;
                                db.SubmitChanges(ConflictMode.ContinueOnConflict);
                                if (AEPReminder == null && Boolean.Parse(Request.Form.GetValues("AEP_Reminder")[0]) == true)
                                {
                                    Record_Distribution Record_Distribution = new Record_Distribution();
                                    Record_Distribution.Record_ID = dbActivity.Record_ID;
                                    Record_Distribution.Record_Distribution_Type_ID = 1;
                                    db.Record_Distributions.InsertOnSubmit(Record_Distribution);
                                    db.SubmitChanges(ConflictMode.ContinueOnConflict);
                                }
                                else if (AEPReminder != null && Boolean.Parse(Request.Form.GetValues("AEP_Reminder")[0]) == false)
                                {
                                    Record_Distribution Record_Distribution = new Record_Distribution();
                                    Record_Distribution = db.Record_Distributions.Where(rd => rd.Record_Distribution_Type_ID == 1 && rd.Record_ID == dbActivity.Record_ID).FirstOrDefault();
                                    db.Record_Distributions.DeleteOnSubmit(Record_Distribution);
                                    db.SubmitChanges(ConflictMode.ContinueOnConflict);
                                }
                                if (Boolean.Parse(Request.Form.GetValues("AEP_Reminder")[0]) == true)
                                {
                                    //set a follow up
                                    Record_Activity Record_Activity = new Record_Activity();
                                    Record_Activity.Record_ID = dbActivity.Record_ID;
                                    Record_Activity.Activity_Type_ID = 2; //outbound call
                                    Record_Activity.Created_By = 36; //admin agent
                                    Record_Activity.Created_On = DateTime.Now;
                                    Record_Activity.User_ID = user.User_ID;
                                    Record_Activity.Made_Contact = false;
                                    Record_Activity.Priority_Level = 3;
                                    Record_Activity.Plan_Transaction_ID = dbActivity.Plan_Transaction_ID;
                                    Record_Activity.Notes = StringHelper.EncryptString("AEP Reminder permission to contact");
                                    Record_Activity.Campaign_Code_ID = Convert.ToInt32(collection["Campaign_Code"]);
                                    //Record_Activity.CTCID = Convert.ToInt32(collection["ctc_DDL"]);
                                    //Record_Activity.SEEID = Convert.ToInt32(collection["SEEID_DDL"]);
                                    //Record_Activity.AdminDocumentationID = Convert.ToInt32(collection["AdminDocumentationID_DDL"]);

                                    Random random = new Random();
                                    int followdate = random.Next(1, 14);
                                    Record_Activity.Due_Date = new DateTime(DateTime.Today.Year, 10, followdate);
                                    db.Record_Activities.InsertOnSubmit(Record_Activity);
                                    db.SubmitChanges(ConflictMode.ContinueOnConflict);

                                }
                            }

                        }



                    }


                    if (collection["SendApp2"].StartsWith("true") || collection["SendApp"].StartsWith("true") || collection["SendInfo"].StartsWith("true") || collection["CancelApp"].StartsWith("true") || collection["PreFill"].StartsWith("true"))
                    {

                        //Handle Dates
                        if (collection["SendApp"].StartsWith("true"))
                        {
                            _tranRepository.UpdateSendAppDate(dbActivity.Plan_Transaction_ID);
                            //Create record activity on partner if partner exists and plan status is "New".  Plan transaction will not let save a plan if no record activities on account.
                            if (record.Partner_ID > 0)
                            {
                                LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
                                Record_Activity tempAct = new Record_Activity();
                                Record temprec = db.Records.Where(r => r.Record_ID == record.Partner_ID).FirstOrDefault();
                                Plan_Transaction temppt = db.Plan_Transactions.Where(tpt => tpt.Record_ID == temprec.Record_ID && tpt.Plan_Transaction_Status_ID == 17).FirstOrDefault();

                                if (temppt != null)
                                {
                                    tempAct.Created_By = user.User_ID;
                                    tempAct.Created_On = DateTime.Now;
                                    tempAct.User_ID = user.User_ID;
                                    tempAct.Record_ID = temprec.Record_ID;
                                    tempAct.Due_Date = DateTime.Now;
                                    tempAct.Actual_Date = DateTime.Now;
                                    tempAct.Activity_Type_ID = 10;
                                    tempAct.Plan_Transaction_ID = temppt.Plan_Transaction_ID;
                                    tempAct.Priority_Level = 3;
                                    tempAct.Record_Activity_Status_ID = 1;
                                    tempAct.Notes = StringHelper.EncryptString("Please see partner record for notes.");
                                    tempAct.Campaign_Code_ID = Convert.ToInt32(collection["Campaign_Code"]);
                                    //tempAct.CTCID = Convert.ToInt32(collection["ctc_DDL"]);
                                    //tempAct.SEEID = Convert.ToInt32(collection["SEEID_DDL"]);
                                    //tempAct.AdminDocumentationID = Convert.ToInt32(collection["AdminDocumentationID_DDL"]);
                                    db.Record_Activities.InsertOnSubmit(tempAct);

                                    db.SubmitChanges(ConflictMode.ContinueOnConflict);
                                }
                            }
                        }
                        if (collection["SendApp2"].StartsWith("true"))
                        {
                            _tranRepository.UpdateAppSentDateAndAppRequestDate(dbActivity.Plan_Transaction_ID);

                        }
                        if (collection["PreFill"].StartsWith("true"))
                        {
                            _tranRepository.UpdatePreFillDate(dbActivity.Plan_Transaction_ID);

                        }
                        if (collection["SendInfo"].StartsWith("true"))
                            _tranRepository.UpdateSendInfoDate(dbActivity.Plan_Transaction_ID);
                        if (collection["CancelApp"].StartsWith("true"))
                        {
                            _tranRepository.UpdateAppCancelledDate(dbActivity.Plan_Transaction_ID);
                            return RedirectToAction("Details", "Record", new { id = record.Record_ID });
                        }

                        if (dbActivity.Plan_Transaction.Group.Group_ID == 1)
                        {
                            return RedirectToAction("Health", "Transaction", new { id = dbActivity.Plan_Transaction_ID });
                        }
                        if (dbActivity.Plan_Transaction.Group.Group_ID == 4)
                        {
                            return RedirectToAction("Annuity", "Transaction", new { id = dbActivity.Plan_Transaction_ID });
                        }
                        else
                        {
                            return RedirectToAction("Life", "Transaction", new { id = dbActivity.Plan_Transaction_ID });
                        }
                    }
                    else
                    {
                        return RedirectToAction("Details", "Record", new { id = record.Record_ID });
                    }
                }
                catch (ChangeConflictException)
                {
                    _db.ChangeConflicts.ResolveAll(RefreshMode.KeepChanges);
                    _db.SubmitChanges(ConflictMode.ContinueOnConflict);
                    return View("Complete", new { id = id });
                }
                catch (System.Exception ex)
                {
                    LMS.ErrorHandler.HandleError.EmailError(ex, id);
                    return View("Complete", new { id = id });
                }
            }
            else
            {
                ModelState.AddModelError("Plan_Transaction_ID", "Create Plan Before Completing Activity");
                return RedirectToAction("Details", "Record", dbActivity.Record_ID);
            }
        }

        public ActionResult Complete(int ID)
        {
            LMS.Data.User user = UserRepository.GetUser(HttpContext.User.Identity.Name);
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            if (user == null)
                throw new HttpException(500, HttpContext.User.Identity.IsAuthenticated.ToString());

            Record_Activity recordActivity = _actRepository.GetActivity(ID);
            Record record = _recordRepository.GetRecord(recordActivity.Record_ID);
            CallRating callrating = _callRepository.GetCallRatingByActivityID(ID);

            ViewData["DisplayRecordActivitiesList"] = _actRepository.GetRecordActivitiesDisplay(recordActivity.Record_ID);
            ViewData["Record_Type_ID"] = record.Record_Type_ID;

            var plan_transaction = recordActivity.Plan_Transaction;
            var opportunity = plan_transaction.Plan_ID;
            var modalPremium = plan_transaction.Modal_Premium;
            var subplan = plan_transaction.Sub_Plan;
            var effective_Date = plan_transaction.Buying_Period_Effective_Date;

            if (plan_transaction != null)
            {
                if ((effective_Date != null || (!string.IsNullOrEmpty(effective_Date.ToString()))) && opportunity != -1 && modalPremium != 0 && subplan != null)
                {
                    ViewData["Eff_Date"] = effective_Date;
                    ViewData["Modal_Premium"] = modalPremium.ToString();
                    ViewData["Opportunity"] = opportunity;
                    ViewData["SubPlan"] = subplan.ToString();

                }
                else
                {
                    ViewData["Eff_Date"] = "";
                    ViewData["Modal_Premium"] = "";
                    ViewData["Opportunity"] = "";
                    ViewData["SubPlan"] = "";
                }
            }
            else
            {

            }
                /*
                if (plan_transaction != null)
                {
                    var opportunity = plan_transaction.Plan_ID;
                    if (opportunity == -1)
                    {
                        ViewData["Opportunity"] = "";
                    }
                    else
                    {
                        ViewData["Opportunity"] = opportunity;
                    }
                }
                else
                {
                    ViewData["Opportunity"] = "";
                }

                //var modalPremium = plan_transaction.Modal_Premium;
                if (modalPremium == 0)
                {
                    ViewData["Modal_Premium"] = "";
                }
                else
                {
                    ViewData["Modal_Premium"] = modalPremium.ToString();
                }

                //var modal = plan_transaction.Modal;
                if (modal != null)
                {
                    //var paymentMode = modal.Description;
                    if (paymentMode == null)
                    {
                        ViewData["PaymentMode"] = "";
                    }
                    else
                    {
                        ViewData["PaymentMode"] = paymentMode.ToString();
                    }
                }
                else
                {
                    ViewData["PaymentMode"] = "";
                }

                //var subplan = plan_transaction.Sub_Plan;
                if (subplan == null)
                {
                    ViewData["SubPlan"] = "";
                }
                else
                {
                    ViewData["SubPlan"] = subplan.ToString();
                }
                */

                var modal = plan_transaction.Modal;
                if (modal != null)
                {
                    var paymentMode = modal.Description;
                    if (paymentMode == null)
                    {
                        ViewData["PaymentMode"] = "";
                    }
                    else
                    {
                        ViewData["PaymentMode"] = paymentMode.ToString();
                    }
                }
                else
                {
                    ViewData["PaymentMode"] = "";
                }

                var plan_ID = plan_transaction.Plan_ID;
                if (plan_ID != null)
                {
                    var plan = db.Plans.Where(t => t.Plan_ID == plan_ID).FirstOrDefault();
                    if (plan != null)
                    {
                        var carrierID = db.Carriers.Where(r => r.Carrier_ID == plan.Carrier_ID).FirstOrDefault();
                        var carrierName = carrierID.Name;
                        if (carrierName == null)
                        {
                            ViewData["Carrier"] = "";
                        }
                        else
                        {
                            ViewData["Carrier"] = carrierName;
                        }
                    }
                    else
                    {
                        
                    }
                }
                else
                {
                    
                }
            
  

            Record_Activity futureAct = record.Record_Activities.Where(ra => ra.Actual_Date.HasValue == false && ra.Record_ID == record.Record_ID && ra.Record_Activity_ID != recordActivity.Record_Activity_ID).OrderByDescending(ra => ra.Actual_Date).FirstOrDefault();
            if (futureAct == null)
            {
                ViewData["Next_Activity_Date"] = "";
            }
            else
            {
                ViewData["Next_Activity_Date"] = futureAct.Due_Date;
            }


            var apptra =
                db.Record_Activities.Where(
                    a =>
                    a.Plan_Transaction_ID == recordActivity.Plan_Transaction_ID && a.Activity_Type_ID == 22
                    && a.Actual_Date == null).ToList();

            var FutFU = apptra.FirstOrDefault();
            if (apptra.Count == 1)
            {
                if (recordActivity.Activity_Type_ID == 22)
                {
                    ViewData["Future_Appointment_Date"] = "";
                }
                else
                {
                    ViewData["Future_Appointment_Date"] = FutFU.Due_Date.ToString();
                }

            }
            else if (apptra.Count < 1)
            {
                ViewData["Future_Appointment_Date"] = "";
            }
            else
            {

                ViewData["Future_Appointment_Date"] = FutFU.Due_Date.ToString();

            }

            var ddlDisposition = new List<Reconciliation_Type>();
            Enums.ComplaintType? complaintCurrentStep = null;
            var complaint = _recordRepository.GetComplaint(record.Record_ID);
            if (complaint != null)
            {
                var complaintId = db.Complaint_Activities.FirstOrDefault(x => x.Complaint_ID == complaint.Complaint_ID);
                if (complaintId != null)
                {
                    // record activity with complaint
                    var activity =
                        db.Record_Activities.FirstOrDefault(x => x.Record_Activity_ID == complaintId.Record_Activity_ID);
                    var complaintTransaction =
                        db.Record_Activities.FirstOrDefault(
                            x => x.Plan_Transaction_ID == activity.Plan_Transaction_ID);

                    // current record activity
                    if (plan_transaction.Plan_Transaction_ID == complaintTransaction.Plan_Transaction_ID)
                    {

                        complaintCurrentStep = Enums.ComplaintType.InProgress;
                        if (UserRepository.HasSecurityLevel(user, 13))
                        {
                            complaintCurrentStep = Enums.ComplaintType.Resoloved;
                        }
                        ddlDisposition = _dropRepository.GetReconciliationTypes(true, complaintCurrentStep);
                        ddlDisposition =
                            ddlDisposition.Where(
                                i => i.Reconciliation_Type_ID == 40 || i.Reconciliation_Type_ID == 64).ToList();

                    }
                    // complaint transaction and current transaction do not match
                    else
                    {
                        ddlDisposition = _dropRepository.GetReconciliationTypes(true, complaintCurrentStep);
                    }
                }
            }
            if (complaint == null)
            {
                complaintCurrentStep = Enums.ComplaintType.Open;
                ddlDisposition = _dropRepository.GetReconciliationTypes(true, complaintCurrentStep);
            }

            //var ddlDisposition = new List<Reconciliation_Type>();
            //Enums.ComplaintType? complaintCurrentStep = null;
            //var complaint = _recordRepository.GetComplaint(record.Record_ID);
            //if (complaint == null)
            //{
            //    complaintCurrentStep = Enums.ComplaintType.Open;
            //    ddlDisposition = _dropRepository.GetReconciliationTypes(true, complaintCurrentStep);
            //}
            //else
            //{
            //    complaintCurrentStep = Enums.ComplaintType.InProgress;                
            //    if (UserRepository.HasSecurityLevel(user, 13))
            //    {
            //        complaintCurrentStep = Enums.ComplaintType.Resoloved;
            //    }
            //    ddlDisposition = _dropRepository.GetReconciliationTypes(true, complaintCurrentStep);
            //    ddlDisposition = ddlDisposition.Where(i => i.Reconciliation_Type_ID == 40 || i.Reconciliation_Type_ID == 64).ToList();
            //}



            //var ddlDisposition = _dropRepository.GetReconciliationTypes(true, complaintCurrentStep);
            //if (recordActivity.Record_Activity_Status_ID == 33 || recordActivity.Record_Activity_Status_ID == 35)
            //{
            //    ddlDisposition = ddlDisposition.Where(i => i.Reconciliation_Type_ID == 40 || i.Reconciliation_Type_ID == 64).ToList();
            //}
            
            ViewData["Record"] = record;
            ViewData["RecordIDValue"] = record.Record_ID;
            ViewData["Record_Activities"] = record.Record_Activities.OrderByDescending(c => c.Actual_Date);
            ViewData["Activity_Type_ID"] = new SelectList(_dropRepository.GetActivityTypes(false), "Activity_Type_ID", "Description", recordActivity.Activity_Type_ID);
            ViewData["Record_Activity_Status_ID"] = new SelectList(_dropRepository.GetActivityStatus(true, null), "Record_Activity_Status_ID", "Description", -1);
            ViewData["CampaignDDL"] = new SelectList(_dropRepository.GetCampaignCodes(true), "Campaign_Code_ID", "Campaign_Description");
            //ViewData["Priority_Level"] = new SelectList(_dropRepository.GetPriorityLevels(false), "Key", "Value", Activity.Priority_Level);
            ViewData["Plan_Transaction_ID"] = new SelectList(_dropRepository.GetOpportunities(record.Record_ID), "Plan_Transaction_ID", "DropDownName", recordActivity.Plan_Transaction_ID);
            ViewData["Follow_Activity_Type_ID"] = new SelectList(_dropRepository.GetFollowUpType(true), "Activity_Type_ID", "Description", -1);
            ViewData["Follow_User_ID"] = new SelectList(_dropRepository.GetAgents(false), "User_ID", "FullName", user.User_ID);
            ViewData["Follow_Up_Time"] = new SelectList(_dropRepository.GetTimes());
            //ViewData["Follow_Priority_Level"] = new SelectList(_dropRepository.GetPriorityLevels(false), "Key", "Value", 3);
            ViewData["RecordNotes"] = new SelectList(_dropRepository.GetActivityNotes(user.User_ID, 0), "Record_Activity_Note_ID", "Description");
            ViewData["FollowRecordNotes"] = new SelectList(_dropRepository.GetActivityNotes(user.User_ID, 1), "Description", "Description");
            ViewData["Letter_ID"] = new SelectList(_dropRepository.GetLetters(true), "Letter_ID", "Description");
            ViewData["Reconciliation_Type_ID"] = new SelectList(ddlDisposition, "Reconciliation_Type_ID", "Description", -1);
            ViewData["Auto_Email"] = new SelectList(_dropRepository.GetAutoEmail(true), "Auto_Email_ID", "Notes", -1);
            ViewData["RecordEmail"] = record.Email_Address;
            ViewData["OptOutEmail"] = record.Opt_Out_Email;
            ViewData["CriticalTopicCoverageDDL"] = new SelectList(_dropRepository.GetCriticalTopicCoverage(true), "CTCID", "Description");
            ViewData["SalesRatingDDL"] = new SelectList(_dropRepository.GetSalesRating(true), "SEEID", "Description");
            ViewData["AdminDocumentationRatingDDL"] = new SelectList(_dropRepository.GetAdminDocumentationRating(true), "AdminDocumentationID", "Description");


            if (callrating == null)
            {
                ViewData["Agent_Rating"] = new SelectList(_dropRepository.GetCallRatings(true), "Key", "Value", 0);
                ViewData["Manager_Rating"] = new SelectList(_dropRepository.GetCallRatings(true), "Key", "Value", 0);
            }
            else
            {
                ViewData["Agent_Rating"] = new SelectList(_dropRepository.GetCallRatings(false), "Key", "Value", callrating.Agent_Rating);
                ViewData["Manager_Rating"] = new SelectList(_dropRepository.GetCallRatings(false), "Key", "Value", callrating.Manager_Rating);
            }

            Record_Distribution AEPReminder = new Record_Distribution();
            AEPReminder = db.Record_Distributions.Where(rd => rd.Record_ID == recordActivity.Record_ID && rd.Record_Distribution_Type_ID == 1).FirstOrDefault();
            if (AEPReminder == null)
            {
                ViewData["AEP_Reminder"] = false;
            }
            else
            {
                ViewData["AEP_Reminder"] = true;
            }

            return View(recordActivity);
        }

        public ActionResult PrintLetter(int id, int recid)
        {

            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            Record record = db.Records.First(r => r.Record_ID == recid);
            User AgentLetter = db.Users.First(u => u.User_ID == record.Agent_ID);
            Letter letter = db.Letters.First(l => l.Letter_ID == id);

            ViewData["Record"] = record;
            ViewData["Agent"] = AgentLetter;

            return View(letter);
        }

        public string PrintLetterHide(int id, int recid)
        {

            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            Record record = db.Records.First(r => r.Record_ID == recid);
            User AgentLetter = db.Users.First(u => u.User_ID == record.Agent_ID);
            Letter letter = db.Letters.First(l => l.Letter_ID == id);

            return "Done";
        }

        public ActionResult Edit(int id)
        {
            try
            {
                LMS.Data.User user = UserRepository.GetUser(User.Identity.Name);
                Record_Activity Activity = _actRepository.GetActivity(id);
                if (Activity.Record_Activity_Status_ID == 0 && Activity.Actual_Date == null)
                    Activity.Record_Activity_Status_ID = 30;


                Enums.ComplaintType? complaintCurrentStep = null;
                var complaint = _recordRepository.GetComplaint(Activity.Record_ID);
                if (complaint != null)
                {
                    complaintCurrentStep = Enums.ComplaintType.InProgress;
                }

                

                var userLevel = user.Security_Level_ID;
                if (userLevel != null)
                {
                    ViewData["UserSecuritylevel"] = userLevel;
                }

                if (user.Security_Level_ID == 1 || user.Security_Level_ID == 4)
                {
                    ViewData["Follow_User_ID"] = new SelectList(_dropRepository.GetAgents(false).Where(x => x.User_ID == Activity.User_ID || x.User_ID == user.User_ID), "User_ID", "FullName", Activity.User_ID);
                }
                else
                {
                    ViewData["Follow_User_ID"] = new SelectList(_dropRepository.GetAgents(false), "User_ID", "FullName", Activity.User_ID);
                }


                Record record = _recordRepository.GetRecord(Activity.Record_ID);
                CallRating callrating = _callRepository.GetCallRatingByActivityID(id);
                ViewData["DisplayRecordActivitiesList"] = _actRepository.GetRecordActivitiesDisplay(Activity.Record_ID);
                ViewData["Record"] = record;
                ViewData["RecordIDValue"] = record.Record_ID;
                //ViewData["Follow_User_ID"] = new SelectList(_dropRepository.GetAgents(false), "User_ID", "FullName", Activity.User_ID);
                ViewData["Due_Time"] = new SelectList(_dropRepository.GetTimes());
                ViewData["Record_Activities"] = record.Record_Activities.OrderByDescending(c => c.Actual_Date);
                ViewData["Activity_Type_ID"] = new SelectList(_dropRepository.GetEditActivityTypes(false), "Activity_Type_ID", "Description", Activity.Activity_Type_ID);
                ViewData["Record_Activity_Status_ID"] = new SelectList(_dropRepository.GetActivityStatus(false, complaintCurrentStep), "Record_Activity_Status_ID", "Description", Activity.Record_Activity_Status_ID);
                ViewData["CampaignDDL"] = new SelectList(_dropRepository.GetCampaignCodes(true), "Campaign_Code_ID", "Campaign_Description", Activity.Campaign_Code_ID);
                //ViewData["Priority_Level"] = new SelectList(_dropRepository.GetPriorityLevels(false), "Key", "Value", Activity.Priority_Level);
                ViewData["Plan_Transaction_ID"] = new SelectList(_dropRepository.GetOpportunities(record.Record_ID), "Plan_Transaction_ID", "DropDownName", Activity.Plan_Transaction_ID);
                ViewData["RecordNotes"] = new SelectList(_dropRepository.GetActivityNotes(user.User_ID, 0), "Record_Activity_Note_ID", "Description", Activity.Record_Activity_Note_ID);
                ViewData[""] = new SelectList(_dropRepository.GetAutoEmail(false), "Auto_Email_ID", "Notes", Activity.Auto_Email_ID);
                ViewData["CriticalTopicCoverageDDL"] = new SelectList(_dropRepository.GetCriticalTopicCoverage(true), "CTCID", "Description", Activity.CTCID);
                ViewData["SalesRatingDDL"] = new SelectList(_dropRepository.GetSalesRating(true), "SEEID", "Description", Activity.SEEID);
                ViewData["AdminDocumentationRatingDDL"] = new SelectList(_dropRepository.GetAdminDocumentationRating(true), "AdminDocumentationID", "Description", Activity.AdminDocumentationID);
                LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

                //7/23/12 Mike - OrderByDescending to get the most recent record if there is more than one 
                Reconciliation rec = db.Reconciliations.Where(r => r.Record_Activity_ID == Activity.Record_Activity_ID).OrderByDescending(r => r.Reconciliation_ID).FirstOrDefault();
                if (rec != null)
                {
                    Reconciliation_Type type = db.Reconciliation_Types.Where(rt => rt.Reconciliation_Type_ID == rec.Reconciliation_Type_ID).FirstOrDefault();
                    if (type != null)
                    {
                        var selectedItem = type.Reconciliation_Type_ID;
                        if (!type.Active)
                        {
                            selectedItem = 0;
                        }

                        var reconciliationTypesDDL = new SelectList(_dropRepository.GetReconciliationTypes(false, complaintCurrentStep), "Reconciliation_Type_ID", "Description", selectedItem).ToList();
                        if (!type.Active)
                        {
                            reconciliationTypesDDL.Insert(0, (new SelectListItem { Text = type.Description, Value = type.Reconciliation_Type_ID.ToString() }));
                        }

                        ViewData["Reconciliation_Type_ID"] = reconciliationTypesDDL;


                    }
                }
                else
                {
                    ViewData["Reconciliation_Type_ID"] = new SelectList(_dropRepository.GetReconciliationTypes(true, complaintCurrentStep), "Reconciliation_Type_ID", "Description", -1);

                }
                Record_Distribution AEPReminder = new Record_Distribution();
                AEPReminder = db.Record_Distributions.Where(rd => rd.Record_ID == Activity.Record_ID && rd.Record_Distribution_Type_ID == 1).FirstOrDefault();
                if (AEPReminder == null)
                {
                    ViewData["AEP_Reminder"] = false;
                }
                else
                {
                    ViewData["AEP_Reminder"] = true;
                }

                if (callrating == null)
                {
                    ViewData["Agent_Rating"] = new SelectList(_dropRepository.GetCallRatings(true), "Key", "Value", -1);
                    ViewData["Manager_Rating"] = new SelectList(_dropRepository.GetCallRatings(true), "Key", "Value", -1);
                    ViewData["Call_of_Week"] = "false";
                }
                else
                {
                    ViewData["Agent_Rating"] = new SelectList(_dropRepository.GetCallRatings(false), "Key", "Value", callrating.Agent_Rating);
                    ViewData["Call_of_Week"] = callrating.Call_of_Week;
                    if (callrating.Manager_Rating == -1)
                    {
                        ViewData["Manager_Rating"] = new SelectList(_dropRepository.GetCallRatings(true), "Key", "Value", -1);
                    }
                    else
                    {
                        ViewData["Manager_Rating"] = new SelectList(_dropRepository.GetCallRatings(false), "Key", "Value", callrating.Manager_Rating);
                    }
                }
                return View(Activity);
            }
            catch (System.Exception ex)
            {
                LMS.ErrorHandler.HandleError.EmailError(ex);
                return View("Complete", new { id = id });
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(int id, Record_Activity act, FormCollection collection)
        {
            try
            {
                LMSDataContext _db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
                LMS.Data.User user = UserRepository.GetUser(User.Identity.Name);
                Record_Activity dbActivity = _db.Record_Activities.Where(ac => ac.Record_Activity_ID == id).FirstOrDefault();
                Record record = _recordRepository.GetRecord(dbActivity.Record_ID);
                CallRating callrating = _callRepository.GetCallRatingByActivityID(id);
                int FollowUser = 0;

                if (Convert.ToInt32(collection["Follow_User_ID"]) > 0)
                {
                    FollowUser = Convert.ToInt32(collection["Follow_User_ID"]);
                }
                else
                {
                    FollowUser = dbActivity.User_ID;
                }

                if (Convert.ToInt32(collection["Follow_User_ID"]) > 0)
                {
                    dbActivity.User_ID = Convert.ToInt32(collection["Follow_User_ID"]);
                }

                var userLevel = _db.Users.FirstOrDefault(x => x.Security_Level_ID == user.Security_Level_ID);
                if (userLevel != null)
                {
                    ViewData["UserSecuritylevel"] = userLevel;
                }

                string activityNotes = collection["txtNotes"];
                if (dbActivity.Campaign_Code_ID != Convert.ToInt32(collection["Campaign_Code"]))
                {
                    using (var db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString))
                    {

                        var campaignCode = db.Campaign_Codes.FirstOrDefault(i => i.Campaign_Code_ID == Convert.ToInt32(collection["Campaign_Code"]));
                        if (campaignCode != null)
                        {
                            activityNotes = campaignCode.Campaign_Number + " - " + campaignCode.Campaign_Description + ". " + activityNotes;
                        }
                    }
                }

                var test = collection["Record_Activity_Status_ID"];
                int Record_Activity_Status_ID = Convert.ToInt32(collection["Record_Activity_Status_ID"]);

                string newDescription = _db.Activity_Types.Where(acttype => acttype.Activity_Type_ID == Convert.ToInt32(collection["Activity_Type_ID"])).FirstOrDefault().Description;
                //update activity from ui
                dbActivity.Plan_Transaction_ID = Int32.Parse(collection["Plan_Transaction_ID"]);
                dbActivity.User_ID = FollowUser;
                dbActivity.Activity_Type_ID = Int32.Parse(collection["Activity_Type_ID"]);
                dbActivity.Record_Activity_Status_ID = Int32.Parse(collection["Record_Activity_Status_ID"]);
                dbActivity.Priority_Level = Convert.ToInt32(collection["Priority_Level"]);
                dbActivity.Notes = StringHelper.EncryptString(activityNotes);
                dbActivity.Due_Date = Convert.ToDateTime(DateTime.Parse(collection["Due_Date"]).ToShortDateString() + " " + (collection["Due_Time"] == @"N\A" ? "12:00AM" : collection["Due_Time"]));
                dbActivity.Record_Activity_Note_ID = Convert.ToInt32(collection["RecordNotes"]);
                dbActivity.CTCID = Convert.ToInt32(collection["ctc_DDL"]);
                dbActivity.SEEID = Convert.ToInt32(collection["SEEID_DDL"]);
                dbActivity.AdminDocumentationID = Convert.ToInt32(collection["AdminDocumentationID_DDL"]);
                dbActivity.Campaign_Code_ID = Convert.ToInt32(collection["Campaign_Code"]);

                _db.SubmitChanges(ConflictMode.ContinueOnConflict);
                _actRepository.Save();

                if (callrating == null)//check to see if rating exists yet
                {
                    callrating = new CallRating();
                    //Set Values
                    callrating.Record_Activity_ID = dbActivity.Record_Activity_ID;
                    callrating.Agent_Rating = Convert.ToInt32(collection["Agent_Rating"]);
                    callrating.Manager_Rating = Convert.ToInt32(collection["Manager_Rating"]);

                    if (callrating.Agent_Rating != -1)//check to see if rating needs to be saved
                    {
                        if (String.IsNullOrEmpty(collection["Call_of_Week"]) || collection["Call_of_Week"] == "false")
                        {
                            callrating.Call_of_Week = false;

                        }
                        else
                        {
                            callrating.Call_of_Week = true;
                        }
                        _db.CallRatings.InsertOnSubmit(callrating);
                        _db.SubmitChanges(ConflictMode.ContinueOnConflict);
                    }
                }
                else//rating exists just need to update
                {
                    //UpdateModel(callrating);
                    callrating.Manager_Rating = Convert.ToInt32(collection["Manager_Rating"]);
                    callrating.Agent_Rating = Convert.ToInt32(collection["Agent_Rating"]);

                    if (String.IsNullOrEmpty(collection["Call_of_Week"]) || collection["Call_of_Week"] == "false")
                    {
                        callrating.Call_of_Week = false;
                    }
                    else
                    {

                        callrating.Call_of_Week = true;
                    }
                    _callRepository.Save();
                }

                //Handle Flag for Reconciliation
                if (collection["Reconciliation_Type_ID"] != "-1")
                {
                    LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
                    Reconciliation_Type type = db.Reconciliation_Types.Where(rt => rt.Reconciliation_Type_ID == Convert.ToInt32(collection["Reconciliation_Type_ID"])).FirstOrDefault();
                    Reconciliation rec = db.Reconciliations.Where(re => re.Record_Activity_ID == id).FirstOrDefault();
                    Record_Distribution AEPReminder = new Record_Distribution();
                    AEPReminder = db.Record_Distributions.Where(rd => rd.Record_ID == record.Record_ID && rd.Record_Distribution_Type_ID == 1).FirstOrDefault();

                    if (Convert.ToInt32(collection["Reconciliation_Type_ID"]) == 36) //Not eligible until AEP
                    {
                        //set start date
                        DateTime startdate;
                        if (DateTime.Today != DateTime.Parse("1/1/" + DateTime.Today.Year.ToString()))
                        {
                            startdate = new DateTime(DateTime.Today.Year + 1, 1, 1);
                        }
                        else
                        {
                            startdate = new DateTime(DateTime.Today.Year, 1, 1);
                        }
                        //change record requested start date
                        db.sp_Set_Record_Start_Date(rec.Record_ID, startdate);
                        db.sp_Set_Record_Situation(rec.Record_ID, 7);//set situation to AEP
                        if (AEPReminder == null && Boolean.Parse(Request.Form.GetValues("AEP_Reminder")[0]) == true)
                        {
                            Record_Distribution Record_Distribution = new Record_Distribution();
                            Record_Distribution.Record_ID = record.Record_ID;
                            Record_Distribution.Record_Distribution_Type_ID = 1;
                            db.Record_Distributions.InsertOnSubmit(Record_Distribution);
                            db.SubmitChanges(ConflictMode.ContinueOnConflict);
                        }
                        else if (AEPReminder != null && Boolean.Parse(Request.Form.GetValues("AEP_Reminder")[0]) == false)
                        {
                            Record_Distribution Record_Distribution = new Record_Distribution();
                            Record_Distribution = db.Record_Distributions.Where(rd => rd.Record_Distribution_Type_ID == 1 && rd.Record_ID == dbActivity.Record_ID).FirstOrDefault();
                            db.Record_Distributions.DeleteOnSubmit(Record_Distribution);
                            db.SubmitChanges(ConflictMode.ContinueOnConflict);
                        }
                        //set a follow up if AEP reminder is selected
                        if (Boolean.Parse(Request.Form.GetValues("AEP_Reminder")[0]) == true && AEPReminder == null)
                        {
                            //set a follow up
                            Record_Activity Record_Activity = new Record_Activity();
                            Record_Activity.Record_ID = record.Record_ID;
                            Record_Activity.Activity_Type_ID = 2; //outbound call
                            Record_Activity.Created_By = 36; //admin agent
                            Record_Activity.Created_On = DateTime.Now;
                            Record_Activity.User_ID = user.User_ID;
                            Record_Activity.Made_Contact = false;
                            Record_Activity.Priority_Level = 3;
                            Record_Activity.Plan_Transaction_ID = dbActivity.Plan_Transaction_ID;
                            Record_Activity.Notes = StringHelper.EncryptString("AEP Reminder permission to contact");
                            Record_Activity.Campaign_Code_ID = Convert.ToInt32(collection["Campaign_Code"]);

                            //Record_Activity.CTCID = Convert.ToInt32(collection["ctc_DDL"]);
                            //Record_Activity.SEEID = Convert.ToInt32(collection["SEEID_DDL"]);
                            //Record_Activity.AdminDocumentationID = Convert.ToInt32(collection["AdminDocumentationID_DDL"]);

                            Random random = new Random();
                            int followdate = random.Next(1, 14);
                            Record_Activity.Due_Date = new DateTime(DateTime.Today.Year, 10, followdate);
                            db.Record_Activities.InsertOnSubmit(Record_Activity);
                            db.SubmitChanges(ConflictMode.ContinueOnConflict);
                        }

                    }

                    if (rec == null)
                    {
                        rec = new Reconciliation();
                        if (type != null)
                        {
                            rec.Placed_By = user.User_ID;
                            rec.Placed_Date = DateTime.Now;
                            rec.Reconciliation_Type_ID = type.Reconciliation_Type_ID;
                            rec.Record_ID = record.Record_ID;
                            rec.Notes = "";
                            rec.Record_Activity_ID = dbActivity.Record_Activity_ID;
                            db.Reconciliations.InsertOnSubmit(rec);
                            db.SubmitChanges(ConflictMode.ContinueOnConflict);
                        }
                    }
                    else
                    {
                        int currentDispostion = rec.Reconciliation_Type_ID;
                        if (type != null)
                        {
                            int newDispostion = type.Reconciliation_Type_ID; // Intermittent nullref error here

                            if (currentDispostion != newDispostion)
                                rec = new Reconciliation();

                            rec.Placed_By = user.User_ID;
                            rec.Placed_Date = DateTime.Now;
                            rec.Reconciliation_Type_ID = type.Reconciliation_Type_ID;
                            rec.Record_ID = record.Record_ID;
                            rec.Notes = "";
                            rec.Record_Activity_ID = dbActivity.Record_Activity_ID;

                            if (currentDispostion != newDispostion) //Create new Reconciliation record if the dispostion has changed
                                db.Reconciliations.InsertOnSubmit(rec);

                            db.SubmitChanges(ConflictMode.ContinueOnConflict);
                        }
                    }

                    if (type != null)
                    {
                        if (type.Plan_Transaction_Status_ID > 0)
                        {

                            Plan_Transaction plan = db.Plan_Transactions.Where(at => at.Plan_Transaction_ID == dbActivity.Plan_Transaction_ID).FirstOrDefault();
                            plan.Plan_Transaction_Status_ID = type.Plan_Transaction_Status_ID;
                            db.SubmitChanges(ConflictMode.ContinueOnConflict);

                        }

                        if (type.Record_Type_ID.HasValue)
                            if (type.Record_Type_ID > 0)
                            {
                                db.Set_Record_Type(rec.Record_ID, type.Record_Type_ID.Value);
                            }
                    }
                }


                return RedirectToAction("Details", "Record", new { id = dbActivity.Record_ID });
            }
            catch (System.Exception ex)
            {
                LMS.ErrorHandler.HandleError.EmailError(ex);
                return View("Complete", new { id = id });
            }


        }

        public ActionResult Delete(int id)
        {
            var user = UserRepository.GetUser(User.Identity.Name);
            Record_Activity dbActivity = _actRepository.GetActivity(id);
            int recid = dbActivity.Record_ID;

            try
            {

                if (dbActivity.Record_Activity_Status_ID != null && dbActivity.Actual_Date == null)
                {
                    if (dbActivity.Record_Activity_Status_ID == 33 || dbActivity.Record_Activity_Status_ID == 34 || dbActivity.Record_Activity_Status_ID == 35)
                    {
                        if (!LMS.Data.Repository.UserRepository.HasSecurityLevel(user, 13) && user.User_ID != dbActivity.User_ID)
                        {
                            return RedirectToAction("Details", "Record", new { id = recid });
                        }
                    }
                }


                _actRepository.Delete(dbActivity);

                _actRepository.Save();



            }
            catch (System.Exception ex)
            {
                LMS.ErrorHandler.HandleError.EmailError(ex);
            }

            return RedirectToAction("Details", "Record", new { id = recid });
        }
    }
}

