using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using LMS.Data.Repository;
using LMS.Data;
using LMS.Helpers;

namespace LMS.Controllers
{
    [Authorize]
    [HandleError]
    [SessionExpireFilterAttribute]
    public class RecordController : Controller
    {
        private IRecordRepository _repository;
        private IUserRepository _userRepository;
        private IDropDownRepository _dropRepository;
        private IPlanTransactionRepository _planTransactionRepository;
        private IActivityRepository _recordActivtyRepository;


        public RecordController()
            : this(new RecordRepository(), new UserRepository(), new DropDownRepository(), new PlanTransactionRepository(), new ActivityRepository())
        {
        }

        public RecordController(IRecordRepository repository, IUserRepository userrepository, IDropDownRepository droprepository, IPlanTransactionRepository planTranRepo, IActivityRepository activityrepository)
        {
            _repository = repository;
            _userRepository = userrepository;
            _dropRepository = droprepository;
            _planTransactionRepository = planTranRepo;
            _recordActivtyRepository = activityrepository;
        }

        public ActionResult Index()
        {
            return View();
        }

        [OutputCache(CacheProfile = "Heartbeat")]
        public JsonResult HomeTabHeartbeat()
        {
            //var username = UserRepository.GetUserName(User.Identity.Name);
            bool? returnColor = null; // false = green, truen = red, null = no color
            LMS.Data.User hUser = UserRepository.GetUser(User.Identity.Name);
            var username = hUser.User_Name;
            int userSecurity = hUser.Security_Level_ID;

            if (userSecurity != 1)
            {
                // Checks for new leads assigned to user
                if (_repository.GetMyUnassinedLeads(username).Count > 0)
                {
                    returnColor = true;
                }
                // Checks for new leads not assigned to user
                else if (_repository.GetOtherUnassinedLeads(username).Count > 0)
                {
                    returnColor = false;
                }

                return Json(new { color = returnColor });
            }
            else
            {
                return Json(new { });
            }
        }

        public ActionResult AgentsByPods(int podId, int agentsecurityID)
        {
            int agSecurityId = agentsecurityID;

            //For New Leads
            ViewData["NewLeadsByPod"] = _repository.GetNewLeadsByPods(podId, agSecurityId);

            //Get My Follow Ups
            ViewData["FollowUpsByPod"] = _repository.GetAgentFollowUpsByPods(podId);

            //Agent Appointment Follow Ups
            ViewData["AppointmentFollowUpsByPod"] = _repository.GetAgentAppointmentFollowUpsByPods(podId);

            //Get Applications Sent with open activities
            ViewData["AppSentOpenActivitiesByPod"] = _repository.GetAppSentOpenActivitiesByPods(podId);


            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Sales(FormCollection collection)
        {
            int AgentID = UserRepository.GetUser(User.Identity.Name).User_ID;

            int SelectedAgentID = Int32.Parse(collection["User_ID"]);
            int AgentSecurityID = _userRepository.GetUser(Int32.Parse(collection["User_ID"])).Security_Level_ID;

            int podId = Int32.Parse(collection["Pd_ID"]);

            if (podId == -1)
            {

                //Get Values for Select a user dropdown based on their pod.
                ViewData["Pod_ID"] = new SelectList(_dropRepository.GetPodLIst(AgentID, false), "User_ID", "User_Name", collection["User_ID"]);

                //Get Values for Select a Pod dropdown
                ViewData["Pd_ID"] = new SelectList(_dropRepository.GetPods(true), "Pod_ID", "Pod_Name", -1);


                //Get New Leads
                ViewData["NewLeads"] = _repository.GetNewLeads(SelectedAgentID, AgentSecurityID);

                //Get My Follow Ups
                ViewData["FollowUps"] = _repository.GetAgentFollowUps(SelectedAgentID);

                //Agent Appointment Follow Ups
                ViewData["AppointFollowUps"] = _repository.GetAgentAppointmentFollowUps(SelectedAgentID);

                //Get Applications Sent with open activities
                ViewData["AppSentOpenActivities"] = _repository.GetAppSentOpenActivities(SelectedAgentID);
            }

            else
            {

                return RedirectToAction("AgentsByPods", new { podId = podId, agentsecurityID = AgentSecurityID });

            }


            return View();
        }

        public ActionResult FollowUp()
        {

            ViewData["InitialApplicationSentFollowUp"] = _repository.GetInitialApplicationSentFollowUps();

            ViewData["ApplicationSentFollowUpAfterInitial"] = _repository.GetApplicationSentFollowUpsAfterInitial();

            return View();
        }

        public ActionResult Sales()
        {
            //todo: need to add try catch block here

            int AgentID = UserRepository.GetUser(User.Identity.Name).User_ID;
            int AgentSecurityID = UserRepository.GetUser(User.Identity.Name).Security_Level_ID;

            //Get users based on pod 
            ViewData["Pod_ID"] = new SelectList(_dropRepository.GetPodLIst(AgentID, false), "User_ID", "User_Name", AgentID);

            //Get Pods from pod table
            ViewData["Pd_ID"] = new SelectList(_dropRepository.GetPods(true), "Pod_ID", "Pod_Name");

            //Get New Leads
            ViewData["NewLeads"] = _repository.GetNewLeads(AgentID, AgentSecurityID);

            //Get My Follow Ups
            ViewData["FollowUps"] = _repository.GetAgentFollowUps(AgentID);

            //Agent Appointment Follow Ups
            ViewData["AppointFollowUps"] = _repository.GetAgentAppointmentFollowUps(AgentID);

            //Get Applications Sent with open activities
            ViewData["AppSentOpenActivities"] = _repository.GetAppSentOpenActivities(AgentID);




            return View();
        }

        [CompressFilter]
        public ActionResult Support()
        {
            int Agent_ID = UserRepository.GetUser(User.Identity.Name).User_ID;

            //Get New Leads
            ViewData["Fulfillment"] = _repository.GetSupportLeads(Agent_ID);

            //Get My Follow Ups
            ViewData["SupportActivities"] = _repository.GetFulfillmentActivities(Agent_ID);

            //Get Recon Leads
            ViewData["ReconLeadData"] = _repository.GetAllOpenComplaints();
            //ViewData["ReconLeadData"] = _repository.GetReconciliations();

            return View();
        }

        [CompressFilter]
        public ActionResult Admin()
        {
            var agents = _userRepository.GetActiveAgents();

            ViewData["Agent_ID"] = new SelectList(agents, "User_ID", "FullName");

            ViewData["MyUnassignedLeadsList"] = _repository.GetMyUnassinedLeads(UserRepository.GetUserName(User.Identity.Name));
            ViewData["OtherUnassignedLeadsList"] = _repository.GetOtherUnassinedLeads(UserRepository.GetUserName(User.Identity.Name));
            return View(ViewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Admin(FormCollection collection)
        {
            // Action String
            string action = "";

            // Check for Assign
            if (collection.AllKeys.Contains("Assign"))
                action = "Assign";

            // Check for Bad Leads
            if (collection.AllKeys.Contains("BadLeads"))
                action = "Bad";

            // Check for Assign
            if (collection.AllKeys.Contains("Duplicate"))
                action = "Duplicate";

            // Check for Repeat
            if (collection.AllKeys.Contains("Repeat"))
                action = "Repeat";

            // Check for Repeat
            if (collection.AllKeys.Contains("Setter"))
                action = "Setter";

            // Check for Assign
            if (collection.AllKeys.Contains("Delete"))
                action = "Delete";

            if (collection.AllKeys.Contains("Getlead"))
                action = "Getlead";

            foreach (string key in collection.AllKeys)
            {
                if (key.StartsWith("Record"))
                {
                    if (collection[key].StartsWith("true"))
                    {
                        int Record_ID = Convert.ToInt32(key.Substring(6));

                        switch (action)
                        {
                            case "Assign":
                                _repository.AssignLead(Record_ID, Convert.ToInt32(collection["Agent_ID"]));
                                break;
                            case "Bad":
                                _repository.BadLead(Record_ID);
                                break;
                            case "Duplicate":
                                _repository.DuplicateLead(Record_ID);
                                break;
                            case "Repeat":
                                _repository.RepeatLead(Record_ID);
                                break;
                            case "Setter":
                                _repository.AssignAppointmentSetter(Record_ID, Convert.ToInt32(collection["Agent_ID"]));
                                break;
                            case "Delete":
                                _repository.Delete(Record_ID);
                                break;
                            case "Getlead":
                                _repository.AssignSalesCoordinator(Record_ID, UserRepository.GetUserName(User.Identity.Name));
                                break;
                            default:
                                break;
                        }

                    }
                }
            }

            ViewData["Agent_ID"] = new SelectList(_dropRepository.GetAgents(false), "User_ID", "FullName");
            ViewData["MyUnassignedLeadsList"] = _repository.GetMyUnassinedLeads(UserRepository.GetUserName(User.Identity.Name));
            ViewData["OtherUnassignedLeadsList"] = _repository.GetOtherUnassinedLeads(UserRepository.GetUserName(User.Identity.Name));
            return View(ViewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult BadLeads(FormCollection collection)
        {
            ViewData["Agent_ID"] = new SelectList(_dropRepository.GetAgents(false), "User_ID", "FullName");
            return View(_repository.GetUnassignedLeads());
        }

        public ActionResult Details(int? id)
        {
            LMSDataContext DB = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            Record record = _repository.GetRecord(id.Value);
            LMS.Data.User user = UserRepository.GetUser(User.Identity.Name);

            if (record.Mailing_State_ID == null)
            {
                record.Mailing_State_ID = 52;
            }
            // Calculate Age
            if (record.DOB != null)
                if (record.DOB.HasValue)
                {
                    // get the difference in years
                    int years = DateTime.Now.Year - record.DOB.Value.Year;
                    // subtract another year if we're before the
                    // birth day in the current year
                    if (DateTime.Now.Month < record.DOB.Value.Month || (DateTime.Now.Month == record.DOB.Value.Month && DateTime.Now.Day < record.DOB.Value.Day))
                        years--;

                    record.Age = years;
                }

            ViewData["GroupID"] = new SelectList(_dropRepository.GetGroups(false), "Group_ID", "Description", record.Source_Code.Group_ID);

            // Partner Fields
            ViewData["PartnerSalutationID"] = new SelectList(_dropRepository.GetSalutations(), "Salutation_ID", "Description");
            ViewData["Plan_Transaction_ID"] = new SelectList(_dropRepository.GetOpportunities(record.Record_ID), "Plan_Transaction_ID", "DropDownName");
            ViewData["Follow_Activity_Type_ID"] = new SelectList(_dropRepository.GetFollowUpType(true), "Activity_Type_ID", "Description", -1);
            ViewData["Follow_User_ID"] = new SelectList(_dropRepository.GetAgents(false).Where(a => a.Active == true), "User_ID", "FullName", record.Agent_ID);
            ViewData["Follow_Up_Time"] = new SelectList(_dropRepository.GetTimes());
            //ViewData["Follow_Priority_Level"] = new SelectList(_dropRepository.GetPriorityLevels(false), "Key", "Value", 3);
            ViewData["CampaignDDL"] = new SelectList(_dropRepository.GetCampaignCodes(true), "Campaign_Code_ID", "Campaign_Description");
            ViewData["FollowRecordNotes"] = new SelectList(_dropRepository.GetActivityNotes(user.User_ID, 1), "Description", "Description");
            ViewData["PlanStatus"] = _repository.GetPlanTransactionDescStatusDate(id.Value);
            ViewData["PlanRequests"] = DB.GetPlanTransactionStatus(record.Record_ID).ToList();
            ViewData["GBSNotes"] = DB.sp_Get_GBS_Notes(record.Record_ID).ToList();
            ViewData["DistributionList"] = _repository.GetRecordDistributionList(id.Value);
            //Partner Activities

            ViewData["DisplayRecordActivitiesList"] = _repository.GetRecordActivitiesDisplay(record.Record_ID);

            ViewData["DisplayPartnerRecordActivitiesList"] = _repository.GetRecordActivitiesDisplay(record.Partner_ID);

            return View(record);
        }

        public ActionResult CheckCreate()
        {
            ViewData["Message"] = "Please do a quick search before creating a new lead.";
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CreatePartner(int id, FormCollection collection)
        {
            Record record = _repository.GetPartner(id);

            try
            {
                record.Salutation_ID = Int32.Parse(collection["Salutation_ID"]);
                record.Suffix = collection["Suffix"];
                record.First_Name = collection["First_Name"];
                record.Last_Name = collection["Last_Name"];
                record.Middle_Initial = collection["Middle_Initial"];
                record.Address_1 = collection["Address_1"];
                record.Address_2 = collection["Address_2"];
                record.City = collection["City"];
                record.State_ID = Int32.Parse(collection["State_ID"]);
                record.Zipcode = collection["Zipcode"];
                record.County = collection["County"];
                record.Alt_First_Name = collection["Alt_First_Name"];
                record.Alt_Last_Name = collection["Alt_Last_Name"];
                record.Alt_Address_1 = collection["Alt_Address_1"];
                record.Alt_Address_2 = collection["Alt_Address_2"];
                record.Alt_City = collection["Alt_City"];
                int astateid;
                if (Int32.TryParse(collection["Alt_State_ID"], out astateid))
                {
                    record.Alt_State_ID = Int32.Parse(collection["Alt_State_ID"]);
                }
                record.Alt_Zipcode = collection["Alt_Zipcode"];
                DateTime aafd;
                if (DateTime.TryParse(collection["Alt_Address_From_Date"], out aafd))
                {
                    record.Alt_Address_From_Date = DateTime.Parse(collection["Alt_Address_From_Date"]);
                }
                else
                {
                    record.Alt_Address_From_Date = null;
                }
                DateTime aatd;
                if (DateTime.TryParse(collection["Alt_Address_To_Date"], out aatd))
                {
                    record.Alt_Address_To_Date = DateTime.Parse(collection["Alt_Address_To_Date"]);
                }
                else
                {
                    record.Alt_Address_To_Date = null;
                }
                DateTime bd;
                if (DateTime.TryParse(collection["DOB"], out bd))
                {
                    record.DOB = bd;
                }
                else
                {
                    record.DOB = null;
                }
                int age;
                if (Int32.TryParse(collection["Age"], out age))
                {
                    record.Age = age;
                }
                else
                {
                    record.Age = null;
                }
                record.Home_Phone = collection["Home_Phone"];
                record.Work_Phone = collection["Work_Phone"];
                record.Work_Phone_Ext = collection["Work_Phone_Ext"];
                record.Cell_Phone = collection["Cell_Phone"];
                record.Fax = collection["Fax"];
                record.Email_Address = collection["Email_Address"];
                record.Alt_Email_Address = collection["Alt_Email_Address"];
                record.Alt_Phone_Number = collection["Alt_Phone_Number"];
                record.Email_Address_2 = collection["Email_Address_2"];
                record.Momentum = collection["Momentum"].Contains("true");
                record.Do_Not_Contact = collection["Do_Not_Contact"].Contains("true");
                record.Opt_Out_Email = collection["Opt_Out_Email"].Contains("true");
                record.B2E = collection["B2E"].Contains("true");
                record.Source_Code_ID = Int32.Parse(collection["Source_Code_ID"]);
                record.Record_Type_ID = Int32.Parse(collection["Record_Type_ID"]);
                record.Agent_ID = Int32.Parse(collection["Agent_ID"]);
                record.Assistant_Agent_ID = Int32.Parse(collection["Assistant_Agent_ID"]);
                record.Reffered_By = collection["Reffered_By"];
                record.Referral_Type_ID = Int32.Parse(collection["Referral_Type_ID"]);
                record.Broker_ID = Int32.Parse(collection["Broker_ID"]);
                record.Products_Of_Interest_ID = Int32.Parse(collection["Products_Of_Interest_ID"]);
                record.Record_Current_Situation_ID = Int32.Parse(collection["Record_Current_Situation_ID"]);
                record.Opt_Out_Newsletter = collection["Opt_Out_Newsletter"].Contains("true");
                DateTime rsd;
                if (DateTime.TryParse(collection["RequestedStartDate"], out rsd))
                {
                    record.RequestedStartDate = rsd;
                }
                else
                {
                    record.RequestedStartDate = null;
                }
                record.Notes = StringHelper.EncryptString(collection["Notes"]);
                record.Medicare_FirstName = collection["Medicare_FirstName"];
                record.Medicare_InitialName = collection["Medicare_InitialName"];
                record.Medicare_LastName = collection["Medicare_LastName"];
                //remove ssn dashes
                if (collection["SSN"].Trim() != "")
                {
                    string tempssn = collection["SSN"].Remove(collection["SSN"].IndexOf("-"), 1);
                    record.SSN = StringHelper.EncryptString(tempssn.Remove(tempssn.IndexOf("-"), 1));
                }
                record.Medicare_Number = StringHelper.EncryptString(collection["Medicare_Number"]);
                record.Part_A_Effective = collection["Part_A_Effective"].Contains("true");
                record.Part_B_Effective = collection["Part_B_Effective"].Contains("true");
                DateTime paed;
                if (DateTime.TryParse(collection["Part_A_Effective_Date"], out paed))
                {
                    record.Part_A_Effective_Date = paed;
                }
                else
                {
                    record.Part_A_Effective_Date = null;
                }
                DateTime pbed;
                if (DateTime.TryParse(collection["Part_B_Effective_Date"], out pbed))
                {
                    record.Part_B_Effective_Date = pbed;
                }
                else
                {
                    record.Part_B_Effective_Date = null;
                }
                record.Drug_List_ID = StringHelper.EncryptString(collection["Drug_List_ID"]);
                decimal ai;
                if (Decimal.TryParse(collection["Annual_Income"], out ai))
                {
                    record.Annual_Income = ai;
                }
                else
                {
                    record.Annual_Income = 0.00m;
                }
                record.Employment_Status_ID = Int32.Parse(collection["ddl_Employment_Status_ID"]);
                //record.Occupation_ID = Int32.Parse(collection["Occupation_ID"]);
                record.Occupation = collection["Occupation"];
                record.Sex = collection["Sex"];
                if (record.Sex != "M" && record.Sex != "F")
                    record.Sex = "U";
                record.Marital_Status_ID = Int32.Parse(collection["Marital_Status_ID"]);

                //Get mailing address information if exists
                bool isChecked = (collection["IsMailing_Address"] != "false");
                record.IsMailing_Address = isChecked;
                if (record.IsMailing_Address == false)
                {

                    record.Mailing_Address_1 = collection["Mailing_Address_1"];
                    record.Mailing_Address_2 = collection["txtMailing_Address_2"];
                    record.Mailing_City = collection["txtMailing_City"];
                    record.Mailing_State_ID = Convert.ToInt32(collection["dlMailing_State_ID"]);
                    record.Mailing_ZipCode = collection["txtMailing_ZipCode"];
                }
                // Calculate Height and Weight
                int Feet;
                int Inches;
                if (Int32.TryParse(collection["Height_Feet"], out Feet) && Int32.TryParse(collection["Height_Inches"], out Inches))
                {
                    record.Height = Feet * 12 + Inches;
                }
                int weight;
                if (Int32.TryParse(collection["Weight"], out weight))
                {
                    record.Weight = weight;
                }
                else
                {
                    record.Weight = 0;
                }
                record.Subsidy_ID = Int32.Parse(collection["Subsidy_ID"]);
                record.Currently_Insured = collection["Currently_Insured"].Contains("true");
                record.Medical_Carrier = collection["Medical_Carrier"];
                record.Drug_Plan_Co = collection["Drug_Plan_Co"];
                record.Current_Tobacco_Use_Status_ID = Int32.Parse(collection["ddl_Current_Tobacco_Use_Status_ID"]);
                record.Current_Tobacco_ID = Int32.Parse(collection["ddl_Current_Tobacco_ID"]);
                record.Past_Tobacco_Use_Status_Id = Int32.Parse(collection["ddl_Past_Tobacco_Use_Status_Id"]);
                record.Past_Tobacco_ID = Int32.Parse(collection["ddl_Past_Tobacco_ID"]);
                record.Quit_Date = collection["Quit_Date"];
                record.Current_Insured_Type_ID = Int32.Parse(collection["ddl_Current_Insured_Type_ID"]);
                DateTime osd;
                if (DateTime.TryParse(collection["Original_Start_Date"], out osd))
                {
                    record.Original_Start_Date = osd;
                }
                else
                {
                    record.Original_Start_Date = null;
                }
                DateTime dposd;
                if (DateTime.TryParse(collection["Drug_Plan_Original_Start_Date"], out dposd))
                {
                    record.Drug_Plan_Original_Start_Date = dposd;
                }
                else
                {
                    record.Drug_Plan_Original_Start_Date = null;
                }

                // Set Last Update Date
                record.Last_Updated_Date = DateTime.Now;


            }
            catch
            {
            }

            if (record.Last_Name.Trim().Length < 1)
            {
                ModelState.AddModelError("Last_Name", "Last Name Can Not Be Blank");
                ModelState.SetModelValue("Last_Name", new ValueProviderResult(ValueProvider.GetValue("Last_Name").AttemptedValue, collection["Last_Name"], System.Globalization.CultureInfo.CurrentCulture));
            }

            if (record.First_Name.Trim().Length < 1)
            {
                ModelState.AddModelError("First_Name", "First Name Can Not Be Blank");
                ModelState.SetModelValue("First_Name", new ValueProviderResult(ValueProvider.GetValue("First_Name").AttemptedValue, collection["First_Name"], System.Globalization.CultureInfo.CurrentCulture));
            }

            LMS.Data.User user = _userRepository.GetUserFromADName(User.Identity.Name.Replace(@"LONGEVITY\", ""));
            if ((record.Record_Type_ID == 2 || record.Record_Type_ID == 3) && (user.Group_ID == 1 || user.Group_ID == 5 && user.Security_Level_ID != 5))
            {
                if (record.Products_Of_Interest_ID.Value < 1)
                {
                    ModelState.AddModelError("Products_Of_Interest_ID", "Products of Interest Cannot Be Blank");
                    ModelState.SetModelValue("Products_Of_Interest_ID", new ValueProviderResult(ValueProvider.GetValue("Products_Of_Interest_ID").AttemptedValue, collection["Products_Of_Interest_ID"], System.Globalization.CultureInfo.CurrentCulture));
                }

                if (record.Record_Current_Situation_ID.Value < 1)
                {
                    ModelState.AddModelError("Record_Current_Situation_ID", "Current Situation Cannot Be Blank");
                    ModelState.SetModelValue("Record_Current_Situation_ID", new ValueProviderResult(ValueProvider.GetValue("Record_Current_Situation_ID").AttemptedValue, collection["Record_Current_Situation_ID"], System.Globalization.CultureInfo.CurrentCulture));

                }

                if (record.RequestedStartDate.ToString() == "")
                {
                    ModelState.AddModelError("RequestedStartDate", "Requested Start Date Cannot Be Blank");
                    ModelState.SetModelValue("RequestedStartDate", new ValueProviderResult(ValueProvider.GetValue("RequestedStartDate").AttemptedValue, collection["RequestedStartDate"], System.Globalization.CultureInfo.CurrentCulture));

                }
                if (record.DOB.ToString() == "")
                {
                    ModelState.AddModelError("DOB", "Date of Birth Cannot Be Blank");
                    ModelState.SetModelValue("DOB", new ValueProviderResult(ValueProvider.GetValue("DOB").AttemptedValue, collection["DOB"], System.Globalization.CultureInfo.CurrentCulture));

                }
            }

            if (record.IsMailing_Address == false && record.Mailing_Address_1.Trim().Length < 1)
            {
                ModelState.AddModelError("Mailing_Address_1", "Please enter address.");
                ModelState.SetModelValue("Mailing_Address_1", new ValueProviderResult(ValueProvider.GetValue("Mailing_Address_1").AttemptedValue, collection["Mailing_Address_1"], System.Globalization.CultureInfo.CurrentCulture));
            }

            if (ModelState.IsValid)
            {
                //Insert Record
                _repository.Insert(record);

                // Save Partner ID
                Record orig = _repository.GetRecord(id);
                orig.Partner_ID = record.Record_ID;
                _repository.Save();

                int Record_ID = record.Record_ID;
                // Save record distribution list
                bool[] subscribed = new bool[_repository.GetDistributionList(Record_ID).Count()];
                Record_Distribution Record_Distribution = null;
                LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
                int distribution_type_id;
                int? record_id;

                for (int i = 0; i < subscribed.Count(); i++)
                {
                    distribution_type_id = _repository.GetDistributionList(Record_ID)[i].Record_Distribution_Type_ID;
                    record_id = _repository.GetDistributionList(Record_ID)[i].Record_ID;
                    subscribed[i] = Boolean.Parse(Request.Form.GetValues(distribution_type_id + "_" + _repository.GetDistributionList(Record_ID)[i].Record_Distribution_Type_Description.Replace(' ', '_'))[0]);
                    if (subscribed[i])
                    {
                        Record_Distribution = new Record_Distribution();
                        Record_Distribution.Record_ID = Record_ID;
                        Record_Distribution.Record_Distribution_Type_ID = distribution_type_id;
                        db.Record_Distributions.InsertOnSubmit(Record_Distribution);
                        db.SubmitChanges();
                    }

                }
                // Go Back to Original Record
                return RedirectToAction("Details", new { id = record.Record_ID });
            }
            else
            {
                //Data Context
                LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

                //Get Agents
                var agents = _userRepository.GetActiveAgents();
                //_dropRepository.GetAgents(false).Where(a => a.Active == true);
                ViewData["Agent_ID"] = new SelectList(agents, "User_ID", "FullName", record.Agent_ID);
                ViewData["Assistant_Agent_ID"] = new SelectList(agents, "User_ID", "FullName", record.Assistant_Agent_ID);

                //Get States
                var states = db.States;
                ViewData["State_ID"] = new SelectList(states, "State_ID", "Abbreviation", record.State_ID);
                ViewData["Alt_State_ID"] = new SelectList(states, "State_ID", "Abbreviation", record.Alt_State_ID);
                ViewData["Mailing_State_ID"] = new SelectList(states, "State_ID", "Abbreviation", record.Mailing_State_ID);


                //Get Referral Types
                var refTypes = db.Referral_Types.Where(r => r.Active == true);
                ViewData["Referral_Type_ID"] = new SelectList(refTypes, "Referral_Type_ID", "Referral_Description", record.Referral_Type_ID);


                //Get Referral Types
                var empStatus = db.Employment_Status;
                ViewData["Employment_Status_ID"] = new SelectList(empStatus, "Employment_Status_ID", "Description", record.Employment_Status_ID);


                //Get Marital Statuses
                ViewData["Marital_Status_ID"] = new SelectList(_dropRepository.GetMaritalStatuses(true), "Marital_Status_ID", "Description", record.Marital_Status_ID);

                ////Get Referral Types
                //var occupations = db.Occupations.Where(r => r.Active == true);
                //ViewData["Occupation_ID"] = new SelectList(occupations, "Occupation_ID", "Description", record.Occupation_ID);

                //Get Tobacco Types
                ViewData["Current_Tobacco_ID"] = new SelectList(_dropRepository.GetTobaccoTypes(true), "Tobacco_ID", "Description", record.Current_Tobacco_ID);
                ViewData["Past_Tobacco_ID"] = new SelectList(_dropRepository.GetTobaccoTypes(true), "Tobacco_ID", "Description", record.Past_Tobacco_ID);


                //Get Tobacco Uses Status
                ViewData["Current_Tobacco_Use_Status_ID"] = new SelectList(_dropRepository.GetTobaccoStatuses(true), "Tobacco_Use_ID", "Description", -1);
                ViewData["Past_Tobacco_Use_Status_Id"] = new SelectList(_dropRepository.GetTobaccoStatuses(true), "Tobacco_USe_ID", "Description", -1);

                //Get Industries
                var industries = db.Industries.Where(s => s.Active == true);
                int indid = (record.Company == null ? 1 : record.Company.Industry_ID);
                ViewData["Company.Industry_ID"] = new SelectList(industries, "Industry_ID", "Description", indid);

                //Get States
                int csid = (record.Company == null ? 52 : record.Company.State_ID);
                ViewData["Company.State_ID"] = new SelectList(states, "State_ID", "Abbreviation", csid);

                //Get Record Types
                var recTypes = db.Record_Types;
                ViewData["Record_Type_ID"] = new SelectList(recTypes, "Record_Type_ID", "Description", record.Record_Type_ID);

                //Get Source Codes
                var sourceCodes = db.Source_Codes.Where(s => s.Active == true);
                ViewData["Source_Code_ID"] = new SelectList(sourceCodes, "Source_Code_ID", "DropDownName", record.Source_Code_ID);

                //Get Gender

                ViewData["Sex"] = new SelectList(_dropRepository.GetGender(true), "Key", "Value", "");



                ViewData["Salutation_ID"] = new SelectList(_dropRepository.GetSalutations(), "Salutation_ID", "Description", 1);

                ViewData["Suffix"] = new SelectList(_dropRepository.GetSuffix(), "");


                ViewData["Broker_ID"] = new SelectList(_dropRepository.GetBrokers(false), "Broker_ID", "DropDownName", record.Broker_ID);

                //Current Situation
                ViewData["Record_Current_Situation_ID"] = new SelectList(_dropRepository.GetRecordCurrentSituations(true), "Record_Current_Situation_ID", "Record_Current_Situation_Desc", record.Record_Current_Situation_ID);

                //Product of Interest
                ViewData["Products_Of_Interest_ID"] = new SelectList(_dropRepository.GetProductsOfInterest(true), "Products_Of_Interest_ID", "Products_Of_Interest_Desc", record.Products_Of_Interest_ID);

                //Subsidy
                ViewData["Subsidy_ID"] = new SelectList(_dropRepository.GetSubsidies(true), "Subsidy_ID", "Subsidy_Desc", record.Subsidy_ID);

                //Current Insured Type
                ViewData["Current_Insured_Type_ID"] = new SelectList(_dropRepository.GetCurrentInsuredTypes(true), "Current_Insured_Type_ID", "Current_Insured", record.Current_Insured_Type_ID);

                //Get Distribution List
                ViewData["DistributionList"] = _repository.GetDistributionList(0);

                bool[] sub = new bool[_repository.GetDistributionList(0).Count()];
                int distributiontypeid;
                int? recordid;
                for (int i = 0; i < sub.Count(); i++)
                {
                    distributiontypeid = _repository.GetDistributionList(0)[i].Record_Distribution_Type_ID;
                    recordid = _repository.GetDistributionList(0)[i].Record_ID;
                    sub[i] = Boolean.Parse(Request.Form.GetValues(distributiontypeid + "_" + _repository.GetDistributionList(0)[i].Record_Distribution_Type_Description.Replace(' ', '_'))[0]);
                    if (sub[i])
                    {
                        ViewData[distributiontypeid + "_" + _repository.GetDistributionList(0)[i].Record_Distribution_Type_Description] = "true";
                    }
                }
                return View(record);

            }
        }

        public ActionResult CreatePartner(int id)
        {

            Record record = _repository.GetPartner(id);
            //record.Occupation_ID = 73;


            //Data Context
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

            //Get Agents
            var agents = _userRepository.GetActiveAgents();
            //_dropRepository.GetAgents(false).Where(a => a.Active == true);
            ViewData["Agent_ID"] = new SelectList(agents, "User_ID", "FullName", record.Agent_ID);
            ViewData["Assistant_Agent_ID"] = new SelectList(agents, "User_ID", "FullName", record.Assistant_Agent_ID);

            //Get States
            var states = db.States;
            ViewData["State_ID"] = new SelectList(states, "State_ID", "Abbreviation", record.State_ID);
            ViewData["Alt_State_ID"] = new SelectList(states, "State_ID", "Abbreviation", record.Alt_State_ID);
            ViewData["Mailing_State_ID"] = new SelectList(states, "State_ID", "Abbreviation", record.Mailing_State_ID);

            //Get Referral Types
            var refTypes = db.Referral_Types.Where(r => r.Active == true);
            ViewData["Referral_Type_ID"] = new SelectList(refTypes, "Referral_Type_ID", "Referral_Description", record.Referral_Type_ID);

            //Get Referral Types
            var empStatus = db.Employment_Status;
            ViewData["Employment_Status_ID"] = new SelectList(empStatus, "Employment_Status_ID", "Description", record.Employment_Status_ID);

            //Get Marital Statuses
            ViewData["Marital_Status_ID"] = new SelectList(_dropRepository.GetMaritalStatuses(true), "Marital_Status_ID", "Description", record.Marital_Status_ID);

            ////Get Referral Types
            //var occupations = db.Occupations.Where(r => r.Active == true);
            //ViewData["occupation_id"] = new SelectList(occupations, "Occupation_ID", "Description", record.Occupation_ID);


            //Get Tobacco Types
            ViewData["Current_Tobacco_ID"] = new SelectList(_dropRepository.GetTobaccoTypes(true), "Tobacco_ID", "Description", record.Current_Tobacco_ID);
            ViewData["Past_Tobacco_ID"] = new SelectList(_dropRepository.GetTobaccoTypes(true), "Tobacco_ID", "Description", record.Past_Tobacco_ID);


            //Get Tobacco Uses Status
            ViewData["Current_Tobacco_Use_Status_ID"] = new SelectList(_dropRepository.GetTobaccoStatuses(true), "Tobacco_Use_ID", "Description", -1);
            ViewData["Past_Tobacco_Use_Status_Id"] = new SelectList(_dropRepository.GetTobaccoStatuses(true), "Tobacco_USe_ID", "Description", -1);


            //Get Industries
            var industries = db.Industries.Where(s => s.Active == true);
            int indid = (record.Company == null ? 1 : record.Company.Industry_ID);
            ViewData["Company.Industry_ID"] = new SelectList(industries, "Industry_ID", "Description", indid);

            //Get States
            int csid = (record.Company == null ? 52 : record.Company.State_ID);
            ViewData["Company.State_ID"] = new SelectList(states, "State_ID", "Abbreviation", csid);

            //Get Record Types
            var recTypes = db.Record_Types;
            ViewData["Record_Type_ID"] = new SelectList(recTypes, "Record_Type_ID", "Description", record.Record_Type_ID);

            //Get Source Codes
            var sourceCodes = db.Source_Codes.Where(s => s.Active == true);
            ViewData["Source_Code_ID"] = new SelectList(sourceCodes, "Source_Code_ID", "DropDownName", record.Source_Code_ID);

            //Get Gender

            ViewData["Sex"] = new SelectList(_dropRepository.GetGender(true), "Key", "Value", "");

            ViewData["Salutation_ID"] = new SelectList(_dropRepository.GetSalutations(), "Salutation_ID", "Description", 1);

            ViewData["Suffix"] = new SelectList(_dropRepository.GetSuffix(), "");


            ViewData["Broker_ID"] = new SelectList(_dropRepository.GetBrokers(false), "Broker_ID", "DropDownName", record.Broker_ID);

            //Current Situation
            ViewData["Record_Current_Situation_ID"] = new SelectList(_dropRepository.GetRecordCurrentSituations(true), "Record_Current_Situation_ID", "Record_Current_Situation_Desc", record.Record_Current_Situation_ID);

            //Product of Interest
            ViewData["Products_Of_Interest_ID"] = new SelectList(_dropRepository.GetProductsOfInterest(true), "Products_Of_Interest_ID", "Products_Of_Interest_Desc", record.Products_Of_Interest_ID);

            //Subsidy
            ViewData["Subsidy_ID"] = new SelectList(_dropRepository.GetSubsidies(true), "Subsidy_ID", "Subsidy_Desc", record.Subsidy_ID);

            //Current Insured Type
            ViewData["Current_Insured_Type_ID"] = new SelectList(_dropRepository.GetCurrentInsuredTypes(true), "Current_Insured_Type_ID", "Current_Insured", record.Current_Insured_Type_ID);

            //Get Distribution List
            ViewData["DistributionList"] = _repository.GetDistributionList(0);
            return View(record);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CreateNewPlan(int id, FormCollection collection)
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            Record record = _repository.GetRecord(id);
            Plan_Transaction plan = new Plan_Transaction();

            // Set Plan transaction variables
            plan.Active = false;
            plan.Agent_ID = record.Agent_ID;
            plan.Annual_Premium = 0;
            plan.Assistant_Agent_ID = 49;
            plan.Best_Time_For_Exam = "";
            plan.Cash_With_Application = false;
            plan.Cash_With_Application_Amount = 0;
            plan.Cash_With_Application_Check = false;
            plan.Comments = "";
            plan.Coverage_Amount = 0;
            plan.Created_On = DateTime.Now;
            plan.Deposit_Amount = 0;
            plan.eApp = false;
            plan.PreFillApp = false;
            plan.Exam_Details = "";
            plan.Exam_Ref_Number = "";
            plan.Exam_Service_ID = -1;
            plan.GBS_ID = "";
            plan.Group_ID = Convert.ToInt32(collection["GroupID"]);
            plan.Guaranteed_Issue = false;
            plan.Inbound_Shipping_Method_ID = -1;
            plan.Initial_Enrollment = false;
            plan.Modal_ID = -1;
            plan.Modal_Premium = 0;
            plan.Mode_ID = -1;
            plan.Non_Qualified_1035_Exchange = false;
            plan.Outbound_Shipping_Method_ID = -1;
            plan.Parent_Plan_ID = -1;
            plan.Payment_Mode_ID = -1;
            plan.Plan_ID = -1;
            plan.Plan_Transaction_Status_ID = 17;
            plan.Qualified_Transfer = false;
            plan.Record_ID = record.Record_ID;
            plan.Replacement = false;
            plan.Replacement_Details = "";
            plan.Source_Code_ID = record.Source_Code_ID;
            plan.Special_Instructions = "";
            plan.Tracking_Number_Inbound = "";
            plan.Tracking_Number_Outbound = "";
            plan.Transaction_Number = -1;
            plan.Underwriting_Class_ID = -1;
            plan.Buying_Period_ID = record.Buying_Period_ID;
            plan.Buying_Period_Sub_ID = record.Buying_Period_Sub_ID;

            _planTransactionRepository.Insert(plan);

            if (plan.Group_ID == 1)
                return RedirectToAction("Health", "Transaction", new { id = plan.Plan_Transaction_ID });
            if (plan.Group_ID == 4)
                return RedirectToAction("Annuity", "Transaction", new { id = plan.Plan_Transaction_ID });
            if (plan.Group_ID == 8)
                return RedirectToAction("Financial", "Transaction", new { id = plan.Plan_Transaction_ID });

            return RedirectToAction("Life", "Transaction", new { id = plan.Plan_Transaction_ID });
        }

        public ActionResult Edit(int id)
        {
            //Get Record
            Record record = _repository.GetRecord(id);

            // Calculate Age
            if (record.DOB.HasValue)
            {
                // get the difference in years
                int years = DateTime.Now.Year - record.DOB.Value.Year;
                // subtract another year if we're before the
                // birth day in the current year
                if (DateTime.Now.Month < record.DOB.Value.Month || (DateTime.Now.Month == record.DOB.Value.Month && DateTime.Now.Day < record.DOB.Value.Day))
                    years--;

                record.Age = years;
            }

            //Data Context
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            //Get Agents
            var agents = _userRepository.GetActiveAgents();
            //_dropRepository.GetAgents(false).Where(a => a.Active == true);

            ViewData["Agent_ID"] = new SelectList(agents, "User_ID", "FullName", record.Agent_ID.ToString());



            if (!record.Original_Agent_ID.HasValue)
            {
                record.Original_Agent_ID = 49;
            }

            ViewData["Original_Agent_ID"] = new SelectList(agents, "User_ID", "FullName", record.Original_Agent_ID);

            ViewData["Assistant_Agent_ID"] = new SelectList(_dropRepository.GetAppointmentSetters(false), "User_ID", "FullName", record.Assistant_Agent_ID);

            //Get States
            ViewData["State_ID"] = new SelectList(_dropRepository.GetStates(false), "State_ID", "Abbreviation", record.State_ID.ToString());
            ViewData["Alt_State_ID"] = new SelectList(_dropRepository.GetStates(false), "State_ID", "Abbreviation", record.Alt_State_ID);
            ViewData["Mailing_State_ID"] = new SelectList(_dropRepository.GetStates(false), "State_ID", "Abbreviation", record.Mailing_State_ID);

            //Get Referral Types
            var refTypes = db.Referral_Types.Where(r => r.Active == true);
            ViewData["Referral_Type_ID"] = new SelectList(refTypes, "Referral_Type_ID", "Referral_Description", record.Referral_Type_ID);

            //Get Buying Period
            var buyingPeriods = _dropRepository.GetBuyingPeriods(false);
            ViewData["Buying_Period_ID"] = new SelectList(buyingPeriods, "Buying_Period_ID", "Description", record.Buying_Period_ID);

            //Get Buying Period
            var buyingPeriodSubs = _dropRepository.GetBuyingPeriodSubs(false);
            ViewData["Buying_Period_Sub_ID"] = new SelectList(buyingPeriodSubs, "Buying_Period_Sub_ID", "Description", record.Buying_Period_Sub_ID);

            //Record Eligibilities
            var recordEligibilities = _dropRepository.GetRecordEligibility(true);
            ViewData["Record_Eligibility"] = new SelectList(recordEligibilities, "Record_Eligibility_ID", "CategoryDescription", record.Record_Eligibility_ID);

            // Priority Levels
            var priorityLevels = _dropRepository.GetPriorityLevels(false);
            ViewData["Priority_Level"] = new SelectList(priorityLevels, "Key", "Value", record.Priority_Level);

            //Get Referral Types
            var empStatus = db.Employment_Status;
            ViewData["Employment_Status_ID"] = new SelectList(empStatus, "Employment_Status_ID", "Description", record.Employment_Status_ID);

            //Get Marital Statuses
            ViewData["Marital_Status_ID"] = new SelectList(_dropRepository.GetMaritalStatuses(true), "Marital_Status_ID", "Description", record.Marital_Status_ID);


            ////Get Referral Types
            //var occupations = db.Occupations.Where(r => r.Active == true);
            //ViewData["Occupation_ID"] = new SelectList(occupations, "Occupation_ID", "Description", record.Occupation_ID);

            //Get Tobacco Types
            ViewData["Current_Tobacco_ID"] = new SelectList(_dropRepository.GetTobaccoTypes(true), "Tobacco_ID", "Description", record.Current_Tobacco_ID);
            ViewData["Past_Tobacco_ID"] = new SelectList(_dropRepository.GetTobaccoTypes(true), "Tobacco_ID", "Description", record.Past_Tobacco_ID);

            //Get Industries
            var industries = db.Industries.Where(s => s.Active == true);
            int indid = (record.Company == null ? 1 : record.Company.Industry_ID);
            ViewData["Company.Industry_ID"] = new SelectList(industries, "Industry_ID", "Description", indid);

            ViewData["Source_Code_ID"] = new SelectList(_dropRepository.GetSourceCodes(true), "Source_Code_ID", "DropDownName", record.Source_Code_ID);

            //Get States
            int csid = (record.Company == null ? 52 : record.Company.State_ID);
            ViewData["Company.State_ID"] = new SelectList(_dropRepository.GetStates(false), "State_ID", "Abbreviation", csid);

            ViewData["Salutation_ID"] = new SelectList(_dropRepository.GetSalutations(), "Salutation_ID", "Description", (record.Salutation_ID) < 1 ? 1 : record.Salutation_ID);

            ViewData["Suffix"] = new SelectList(_dropRepository.GetSuffix(), (record.Suffix == null ? "" : record.Suffix));

            ViewData["Broker_ID"] = new SelectList(_dropRepository.GetBrokers(false), "Broker_ID", "DropDownName", record.Broker_ID);

            //Get Gender

            var genderDetails = _dropRepository.GetGender(false);
            ViewData["Sex"] = new SelectList(genderDetails, "Key", "Value", record.Sex);


            // Get Record Type ID
            ViewData["Record_Type_ID"] = new SelectList(_dropRepository.GetRecordType(false), "Record_Type_ID", "Description", record.Record_Type_ID);

            //Uses_Tobacco
            //ViewData["Uses_Tobacco"] = new SelectList(_dropRepository.GetUsesTobacco(), "Key", "Value", record.Uses_Tobacco);
            ViewData["Current_Tobacco_Use_Status_ID"] = new SelectList(_dropRepository.GetTobaccoStatuses(true), "Tobacco_Use_ID", "Description", record.Current_Tobacco_Use_Status_ID);
            ViewData["Past_Tobacco_Use_Status_Id"] = new SelectList(_dropRepository.GetTobaccoStatuses(true), "Tobacco_Use_ID", "Description", record.Past_Tobacco_Use_Status_Id);


            //Current Situation
            ViewData["Record_Current_Situation_ID"] = new SelectList(_dropRepository.GetRecordCurrentSituations(true), "Record_Current_Situation_ID", "Record_Current_Situation_Desc", record.Record_Current_Situation_ID);

            //Product of Interest
            ViewData["Products_Of_Interest_ID"] = new SelectList(_dropRepository.GetProductsOfInterest(true), "Products_Of_Interest_ID", "Products_Of_Interest_Desc", record.Products_Of_Interest_ID);

            //Subsidy
            ViewData["Subsidy_ID"] = new SelectList(_dropRepository.GetSubsidies(true), "Subsidy_ID", "Subsidy_Desc", record.Subsidy_ID);

            //Current Insured Type
            ViewData["Current_Insured_Type_ID"] = new SelectList(_dropRepository.GetCurrentInsuredTypes(true), "Current_Insured_Type_ID", "Current_Insured", record.Current_Insured_Type_ID);

            //Get Edit Owener
            ViewData["InEditBy"] = TableLockHelper.CheckLock("Record", id, User.Identity.Name.Replace(@"LONGEVITY\", "")).ToLower();

            //Get Distribution List
            ViewData["DistributionList"] = _repository.GetDistributionList(id);
            return View(record);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(int id, FormCollection collection)
        {
            //Data Context
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

            //Get Record
            Record record = db.Records.Where(r => r.Record_ID == id).FirstOrDefault();

            // Update Record From Web Page Data
            record.Salutation_ID = Int32.Parse(collection["Salutation_ID"]);
            record.Suffix = collection["Suffix"];
            record.First_Name = collection["First_Name"];
            record.Last_Name = collection["Last_Name"];
            record.Middle_Initial = collection["Middle_Initial"];
            record.Address_1 = collection["Address_1"];
            record.Address_2 = collection["Address_2"];
            record.City = collection["City"];
            record.State_ID = Int32.Parse(collection["State_ID"]);
            record.Zipcode = collection["Zipcode"];
            record.County = collection["County"];
            record.Alt_First_Name = collection["Alt_First_Name"];
            record.Alt_Last_Name = collection["Alt_Last_Name"];
            record.Alt_Address_1 = collection["Alt_Address_1"];
            record.Alt_Address_2 = collection["Alt_Address_2"];
            record.Alt_City = collection["Alt_City"];
            int astateid;
            if (Int32.TryParse(collection["Alt_State_ID"], out astateid))
            {
                record.Alt_State_ID = Int32.Parse(collection["Alt_State_ID"]);
            }
            record.Alt_Zipcode = collection["Alt_Zipcode"];
            DateTime aafd;
            if (DateTime.TryParse(collection["Alt_Address_From_Date"], out aafd))
            {
                record.Alt_Address_From_Date = DateTime.Parse(collection["Alt_Address_From_Date"]);
            }
            else
            {
                record.Alt_Address_From_Date = null;
            }
            DateTime aatd;
            if (DateTime.TryParse(collection["Alt_Address_To_Date"], out aatd))
            {
                record.Alt_Address_To_Date = DateTime.Parse(collection["Alt_Address_To_Date"]);
            }
            else
            {
                record.Alt_Address_To_Date = null;
            }
            DateTime bd;
            if (DateTime.TryParse(collection["DOB"], out bd))
            {
                record.DOB = bd;
            }
            else
            {
                record.DOB = null;
            }
            int age;
            if (Int32.TryParse(collection["Age"], out age))
            {
                record.Age = age;
            }
            else
            {
                record.Age = null;
            }
            record.Home_Phone = collection["Home_Phone"];
            record.Work_Phone = collection["Work_Phone"];
            record.Work_Phone_Ext = collection["Work_Phone_Ext"];
            record.Cell_Phone = collection["Cell_Phone"];
            record.Fax = collection["Fax"];
            record.Email_Address = collection["Email_Address"];
            record.Alt_Email_Address = collection["Alt_Email_Address"];
            record.Alt_Phone_Number = collection["Alt_Phone_Number"];
            record.Email_Address_2 = collection["Email_Address_2"];
            record.Momentum = collection["Momentum"].Contains("true");
            record.Do_Not_Contact = collection["Do_Not_Contact"].Contains("true");
            record.Opt_Out_Email = collection["Opt_Out_Email"].Contains("true");
            record.B2E = collection["B2E"].Contains("true");
            if (collection["Source_Code_ID"] != null)
            {
                record.Source_Code_ID = Int32.Parse(collection["Source_Code_ID"]);
            }
            if (collection["Record_Type_ID"] != null)
            {
                record.Record_Type_ID = Int32.Parse(collection["Record_Type_ID"]);
            }
            if (collection["Agent_ID"] != null)
            {
                record.Agent_ID = Int32.Parse(collection["Agent_ID"]);
            }
            if (collection["Original_Agent_ID"] != null)
            {
                record.Original_Agent_ID = Int32.Parse(collection["Original_Agent_ID"]);
            }
            record.Assistant_Agent_ID = Int32.Parse(collection["Assistant_Agent_ID"]);
            record.Reffered_By = collection["Reffered_By"];
            record.Referral_Type_ID = Int32.Parse(collection["Referral_Type_ID"]);
            record.Broker_ID = Int32.Parse(collection["Broker_ID"]);
            record.Products_Of_Interest_ID = Int32.Parse(collection["Products_Of_Interest_ID"]);
            record.Record_Current_Situation_ID = Int32.Parse(collection["Record_Current_Situation_ID"]);
            record.Opt_Out_Newsletter = collection["Opt_Out_Newsletter"].Contains("true");
            DateTime rsd;
            if (DateTime.TryParse(collection["RequestedStartDate"], out rsd))
            {
                record.RequestedStartDate = rsd;
            }
            else
            {
                record.RequestedStartDate = null;
            }
            record.Notes = StringHelper.EncryptString(collection["Notes"]);
            record.Medicare_FirstName = collection["Medicare_FirstName"];
            record.Medicare_InitialName = collection["Medicare_InitialName"];
            record.Medicare_LastName = collection["Medicare_LastName"];
            //remove ssn dashes
            if (collection["SSN"].Trim() != "")
            {
                string tempssn = collection["SSN"].Remove(collection["SSN"].IndexOf("-"), 1);
                record.SSN = StringHelper.EncryptString(tempssn.Remove(tempssn.IndexOf("-"), 1));
            }
            record.Medicare_Number = StringHelper.EncryptString(collection["Medicare_Number"]);
            record.Part_A_Effective = collection["Part_A_Effective"].Contains("true");
            record.Part_B_Effective = collection["Part_B_Effective"].Contains("true");
            DateTime paed;
            if (DateTime.TryParse(collection["Part_A_Effective_Date"], out paed))
            {
                record.Part_A_Effective_Date = paed;
            }
            else
            {
                record.Part_A_Effective_Date = null;
            }
            DateTime pbed;
            if (DateTime.TryParse(collection["Part_B_Effective_Date"], out pbed))
            {
                record.Part_B_Effective_Date = pbed;
            }
            else
            {
                record.Part_B_Effective_Date = null;
            }
            record.Drug_List_ID = StringHelper.EncryptString(collection["Drug_List_ID"]);
            decimal ai;
            if (Decimal.TryParse(collection["Annual_Income"], out ai))
            {
                record.Annual_Income = ai;
            }
            else
            {
                record.Annual_Income = 0.00m;
            }
            record.Employment_Status_ID = Int32.Parse(collection["Employment_Status_ID"]);
            //record.Occupation_ID = Int32.Parse(collection["Occupation_ID"]);
            record.Occupation = collection["Occupation"];
            record.Sex = collection["Sex"];
            if (record.Sex != "M" && record.Sex != "F")
                record.Sex = "U";
            record.Marital_Status_ID = Int32.Parse(collection["Marital_Status_ID"]);

            bool isChecked = (collection["IsMailing_Address"] != "false");
            record.IsMailing_Address = isChecked;
            if (record.IsMailing_Address == false)
            {

                record.Mailing_Address_1 = collection["Mailing_Address_1"];
                record.Mailing_Address_2 = collection["txtMailing_Address_2"];
                record.Mailing_City = collection["txtMailing_City"];
                record.Mailing_State_ID = Convert.ToInt32(collection["dlMailing_State_ID"]);
                record.Mailing_ZipCode = collection["txtMailing_ZipCode"];
            }
            // Calculate Height and Weight
            int Feet;
            int Inches;
            if (Int32.TryParse(collection["Height_Feet"], out Feet) && Int32.TryParse(collection["Height_Inches"], out Inches))
            {
                record.Height = Feet * 12 + Inches;
            }
            int weight;
            if (Int32.TryParse(collection["Weight"], out weight))
            {
                record.Weight = weight;
            }
            else
            {
                record.Weight = 0;
            }
            record.Subsidy_ID = Int32.Parse(collection["Subsidy_ID"]);
            record.Currently_Insured = collection["Currently_Insured"].Contains("true");
            record.Medical_Carrier = collection["Medical_Carrier"];
            record.Drug_Plan_Co = collection["Drug_Plan_Co"];
            record.Current_Tobacco_Use_Status_ID = Int32.Parse(collection["Current_Tobacco_Use_Status_ID"]);
            record.Current_Tobacco_ID = Int32.Parse(collection["Current_Tobacco_ID"]);
            record.Past_Tobacco_Use_Status_Id = Int32.Parse(collection["Past_Tobacco_Use_Status_Id"]);
            record.Past_Tobacco_ID = Int32.Parse(collection["Past_Tobacco_ID"]);
            record.Quit_Date = collection["Quit_Date"];
            record.Current_Insured_Type_ID = Int32.Parse(collection["Current_Insured_Type_ID"]);
            DateTime osd;
            if (DateTime.TryParse(collection["Original_Start_Date"], out osd))
            {
                record.Original_Start_Date = osd;
            }
            else
            {
                record.Original_Start_Date = null;
            }
            DateTime dposd;
            if (DateTime.TryParse(collection["Drug_Plan_Original_Start_Date"], out dposd))
            {
                record.Drug_Plan_Original_Start_Date = dposd;
            }
            else
            {
                record.Drug_Plan_Original_Start_Date = null;
            }

            // Set Last Update Date
            record.Last_Updated_Date = DateTime.Now;
            record.Alt_Address_1 = collection["txtAlt_Address_1"];
            record.Alt_Address_2 = collection["txtAlt_Address_2"];
            record.Alt_City = collection["txtAlt_City"];
            record.Alt_State_ID = Int32.Parse(collection["dlAlt_State_ID"]);
            record.Alt_Zipcode = collection["Alt_Zipcode"];


            // Validate Record
            if (record.First_Name.Trim().Length < 1)
                ModelState.AddModelError("First_Name", "First Name Cannot Be Blank");

            if (record.Last_Name.Trim().Length < 1)
                ModelState.AddModelError("Last_Name", "Last Name Can Not Be Blank");

            LMS.Data.User user = _userRepository.GetUserFromADName(User.Identity.Name.Replace(@"LONGEVITY\", ""));


            if ((user.Security_Level_ID == 4) && (user.Security_Level_ID != 1 || user.Security_Level_ID != 2 || user.Security_Level_ID != 3 || user.Security_Level_ID != 5 || user.Security_Level_ID != 6 || user.Security_Level_ID != 7 || user.Security_Level_ID != 8))
            {
                if (collection["DOB"] == "")
                {
                    ModelState.AddModelError("DOB", "Date of Birth Cannot Be Blank");
                    ModelState.SetModelValue("DOB", new ValueProviderResult(ValueProvider.GetValue("DOB").AttemptedValue, collection["DOB"], System.Globalization.CultureInfo.CurrentCulture));
                }

            }
            else
            {
                if ((record.Record_Type_ID == 2 || record.Record_Type_ID == 3) && (user.Group_ID == 1 || user.Group_ID == 5 && user.Security_Level_ID != 5))
                {
                    if (record.Products_Of_Interest_ID.Value < 1)
                    {
                        ModelState.AddModelError("Products_Of_Interest_ID", "Products of Interest Cannot Be Blank");
                        ModelState.SetModelValue("Products_Of_Interest_ID", new ValueProviderResult(ValueProvider.GetValue("Products_Of_Interest_ID").AttemptedValue, collection["Products_Of_Interest_ID"], System.Globalization.CultureInfo.CurrentCulture));
                    }
                    if (record.Record_Current_Situation_ID.Value < 1)
                    {
                        ModelState.AddModelError("Record_Current_Situation_ID", "Current Situation Cannot Be Blank");
                        ModelState.SetModelValue("Record_Current_Situation_ID", new ValueProviderResult(ValueProvider.GetValue("Record_Current_Situation_ID").AttemptedValue, collection["Record_Current_Situation_ID"], System.Globalization.CultureInfo.CurrentCulture));
                    }
                    if (collection["RequestedStartDate"] == "")
                    {
                        ModelState.AddModelError("RequestedStartDate", "Requested Start Date Cannot Be Blank");
                        ModelState.SetModelValue("RequestedStartDate", new ValueProviderResult(ValueProvider.GetValue("RequestedStartDate").AttemptedValue, collection["RequestedStartDate"], System.Globalization.CultureInfo.CurrentCulture));
                    }
                    if (collection["DOB"] == "")
                    {
                        ModelState.AddModelError("DOB", "Date of Birth Cannot Be Blank");
                        ModelState.SetModelValue("DOB", new ValueProviderResult(ValueProvider.GetValue("DOB").AttemptedValue, collection["DOB"], System.Globalization.CultureInfo.CurrentCulture));
                    }
                }
            }

            if (record.IsMailing_Address == false && record.Mailing_Address_1.Trim().Length < 1)
            {
                ModelState.AddModelError("Mailing_Address_1", "Please enter address.");
                ModelState.SetModelValue("Mailing_Address_1", new ValueProviderResult(ValueProvider.GetValue("Mailing_Address_1").AttemptedValue, collection["Mailing_Address_1"], System.Globalization.CultureInfo.CurrentCulture));
            }

            if (ModelState.IsValid)
            {
                // Save Data
                db.SubmitChanges();

                if (record.Individual_Client_ID.HasValue)
                    db.sp_Update_GBS_from_LMS_Client(record.Individual_Client_ID.Value);

                // Save record distribution list
                bool[] subscribed = new bool[_repository.GetDistributionList(id).Count()];
                Record_Distribution Record_Distribution = null;
                int distribution_type_id;
                int? record_id;

                for (int i = 0; i < subscribed.Count(); i++)
                {
                    distribution_type_id = _repository.GetDistributionList(id)[i].Record_Distribution_Type_ID;
                    record_id = _repository.GetDistributionList(id)[i].Record_ID;
                    subscribed[i] = Boolean.Parse(Request.Form.GetValues(distribution_type_id + "_" + _repository.GetDistributionList(id)[i].Record_Distribution_Type_Description.Replace(' ', '_'))[0]);
                    if (subscribed[i] && record_id == null)
                    {
                        Record_Distribution = new Record_Distribution();
                        Record_Distribution.Record_ID = id;
                        Record_Distribution.Record_Distribution_Type_ID = distribution_type_id;
                        db.Record_Distributions.InsertOnSubmit(Record_Distribution);
                        db.SubmitChanges();
                    }
                    else if (!subscribed[i] && record_id != null)
                    {
                        Record_Distribution = new Record_Distribution();
                        Record_Distribution = db.Record_Distributions.Where(rd => rd.Record_ID == (int)record_id && rd.Record_Distribution_Type_ID == distribution_type_id).FirstOrDefault();
                        db.Record_Distributions.DeleteOnSubmit(Record_Distribution);
                        db.SubmitChanges();
                    }
                }
                return RedirectToAction("Details", new { id = id });
            }
            else
            {

                //Get Agents
                var agents = _userRepository.GetActiveAgents();
                ViewData["Agent_ID"] = new SelectList(agents, "User_ID", "FullName", record.Agent_ID);
                ViewData["Original_Agent_ID"] = new SelectList(agents, "User_ID", "FullName", record.Original_Agent_ID);
                ViewData["Assistant_Agent_ID"] = new SelectList(agents, "User_ID", "FullName", record.Assistant_Agent_ID);

                //Get States
                ViewData["State_ID"] = new SelectList(_dropRepository.GetStates(false), "State_ID", "Abbreviation", record.State_ID);
                ViewData["Alt_State_ID"] = new SelectList(_dropRepository.GetStates(false), "State_ID", "Abbreviation", record.Alt_State_ID);
                ViewData["Mailing_State_ID"] = new SelectList(_dropRepository.GetStates(false), "State_ID", "Abbreviation", record.Mailing_State_ID);

                //Get Referral Types
                var refTypes = db.Referral_Types.Where(r => r.Active == true);
                ViewData["Referral_Type_ID"] = new SelectList(refTypes, "Referral_Type_ID", "Referral_Description", record.Referral_Type_ID);

                //Get Buying Period
                var buyingPeriods = _dropRepository.GetBuyingPeriods(false);
                ViewData["Buying_Period_ID"] = new SelectList(buyingPeriods, "Buying_Period_ID", "Description", record.Buying_Period_ID);

                //Get Buying Period
                var buyingPeriodSubs = db.Buying_Period_Subs;
                ViewData["Buying_Period_Sub_ID"] = new SelectList(buyingPeriodSubs, "Buying_Period_Sub_ID", "Description", record.Buying_Period_Sub_ID);


                // Priority Levels
                var priorityLevels = _dropRepository.GetPriorityLevels(false);
                ViewData["Priority_Level"] = new SelectList(priorityLevels, "Key", "Value", record.Priority_Level);

                //Record Eligibilities
                var recordEligibilities = _dropRepository.GetRecordEligibility(true);
                ViewData["Record_Eligibility"] = new SelectList(recordEligibilities, "Record_Eligibility_ID", "CategoryDescription", record.Record_Eligibility_ID);

                //Get Referral Types
                var empStatus = db.Employment_Status;
                ViewData["Employment_Status_ID"] = new SelectList(empStatus, "Employment_Status_ID", "Description", record.Employment_Status_ID);

                //Get Marital Statuses
                var maritalStatuses = _dropRepository.GetMaritalStatuses(true);
                ViewData["Marital_Status_ID"] = new SelectList(maritalStatuses, "Marital_Status_ID", "Description", record.Marital_Status_ID);

                ////Get Referral Types
                //var occupations = db.Occupations.Where(r => r.Active == true);
                //ViewData["Occupation_ID"] = new SelectList(occupations, "Occupation_ID", "Description", record.Occupation_ID);

                //Get Tobacco Types
                var tobaccoTypes = _dropRepository.GetTobaccoTypes(true);
                ViewData["Current_Tobacco_ID"] = new SelectList(tobaccoTypes, "Tobacco_ID", "Description", record.Current_Tobacco_ID);
                ViewData["Past_Tobacco_ID"] = new SelectList(tobaccoTypes, "Tobacco_ID", "Description", record.Past_Tobacco_ID);


                //Uses_Tobacco
                //ViewData["Uses_Tobacco"] = new SelectList(_dropRepository.GetUsesTobacco(), "Key", "Value", record.Uses_Tobacco);
                var tobaccoUseStatus = _dropRepository.GetTobaccoStatuses(true);
                ViewData["Current_Tobacco_Use_Status_ID"] = new SelectList(tobaccoUseStatus, "Tobacco_Use_ID", "Description", record.Current_Tobacco_Use_Status_ID);
                ViewData["Past_Tobacco_Use_Status_Id"] = new SelectList(tobaccoUseStatus, "Tobacco_Use_ID", "Description", record.Past_Tobacco_Use_Status_Id);


                //Get Record Types
                var recTypes = db.Record_Types;
                ViewData["Record_Type_ID"] = new SelectList(recTypes, "Record_Type_ID", "Description", record.Record_Type_ID);

                //Get Source Codes
                var sourceCodes = db.Source_Codes.Where(s => s.Active == true);
                ViewData["Source_Code_ID"] = new SelectList(sourceCodes, "Source_Code_ID", "Description", record.Source_Code_ID);

                //Get Industries
                var industries = db.Industries.Where(s => s.Active == true);
                int indusid = record.Company == null ? 1 : record.Company.Industry_ID;
                ViewData["Company.Industry_ID"] = new SelectList(industries, "Industry_ID", "Description", indusid);

                //Get States
                int csid = (record.Company == null ? 52 : record.Company.State_ID);
                ViewData["Company.State_ID"] = new SelectList(_dropRepository.GetStates(false), "State_ID", "Abbreviation", csid);

                ViewData["Broker_ID"] = new SelectList(_dropRepository.GetBrokers(false), "Broker_ID", "DropDownName", record.Broker_ID);

                ViewData["Salutation_ID"] = new SelectList(_dropRepository.GetSalutations(), "Salutation_ID", "Description", (record.Salutation_ID) < 1 ? 1 : record.Salutation_ID);

                ViewData["Suffix"] = new SelectList(_dropRepository.GetSuffix(), (record.Suffix == null ? "" : record.Suffix));

                //Get Gender

                var genderDetails = _dropRepository.GetGender(false);
                ViewData["Sex"] = new SelectList(genderDetails, "Key", "Value", record.Sex);


                // Get Record Type ID
                ViewData["Record_Type_ID"] = new SelectList(_dropRepository.GetRecordType(false), "Record_Type_ID", "Description", record.Record_Type_ID);

                //Current Situation
                ViewData["Record_Current_Situation_ID"] = new SelectList(_dropRepository.GetRecordCurrentSituations(true), "Record_Current_Situation_ID", "Record_Current_Situation_Desc", record.Record_Current_Situation_ID);

                //Product of Interest
                ViewData["Products_Of_Interest_ID"] = new SelectList(_dropRepository.GetProductsOfInterest(true), "Products_Of_Interest_ID", "Products_Of_Interest_Desc", record.Products_Of_Interest_ID);

                //Subsidy
                ViewData["Subsidy_ID"] = new SelectList(_dropRepository.GetSubsidies(true), "Subsidy_ID", "Subsidy_Desc", record.Subsidy_ID);

                //Current Insured Type
                ViewData["Current_Insured_Type_ID"] = new SelectList(_dropRepository.GetCurrentInsuredTypes(true), "Current_Insured_Type_ID", "Current_Insured", record.Current_Insured_Type_ID);

                ViewData["InEditBy"] = TableLockHelper.CheckLock("Record", id, User.Identity.Name.Replace(@"LONGEVITY\", "")).ToLower();

                //Get Distribution List
                ViewData["DistributionList"] = _repository.GetDistributionList(id);

                bool[] subscribed = new bool[_repository.GetDistributionList(id).Count()];
                int distribution_type_id;
                int? record_id;
                for (int i = 0; i < subscribed.Count(); i++)
                {
                    distribution_type_id = _repository.GetDistributionList(id)[i].Record_Distribution_Type_ID;
                    record_id = _repository.GetDistributionList(id)[i].Record_ID;
                    subscribed[i] = Boolean.Parse(Request.Form.GetValues(distribution_type_id + "_" + _repository.GetDistributionList(id)[i].Record_Distribution_Type_Description.Replace(' ', '_'))[0]);
                    if (subscribed[i])
                    {
                        ViewData[distribution_type_id + "_" + _repository.GetDistributionList(id)[i].Record_Distribution_Type_Description] = "true";
                    }
                }
                return View(record);
            }


        }

        public ActionResult Create()
        {
            LMS.Data.User user = UserRepository.GetUser(User.Identity.Name);

            //Data Context
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

            Record record = new Record();
            record.Agent_ID = user.User_ID;
            record.Assistant_Agent_ID = 49;
            record.Created_On = DateTime.Now;
            record.Imported = true;
            record.Record_Type_ID = 3;

            Company company = new Company();


            //Get Agents
            var agents = _dropRepository.GetAgents(true);
            ViewData["Agent_ID"] = new SelectList(agents, "User_ID", "FullName", record.Agent_ID);
            ViewData["Assistant_Agent_ID"] = new SelectList(agents, "User_ID", "FullName", record.Assistant_Agent_ID);

            //Get States
            ViewData["State_ID"] = new SelectList(_dropRepository.GetStates(false), "State_ID", "Abbreviation", 52);
            ViewData["Alt_State_ID"] = new SelectList(_dropRepository.GetStates(false), "State_ID", "Abbreviation", 52);
            ViewData["Mailing_State_ID"] = new SelectList(_dropRepository.GetStates(false), "State_ID", "Abbreviation", 52);

            //Get Referral Types
            var refTypes = db.Referral_Types.Where(r => r.Active == true);
            ViewData["Referral_Type_ID"] = new SelectList(refTypes, "Referral_Type_ID", "Referral_Description", record.Referral_Type_ID);

            //Get Buying Period

            ViewData["Buying_Period_ID"] = new SelectList(_dropRepository.GetBuyingPeriods(true), "Buying_Period_ID", "Description", -1);

            //Get Buying Period

            ViewData["Buying_Period_Sub_ID"] = new SelectList(_dropRepository.GetBuyingPeriodSubs(true), "Buying_Period_Sub_ID", "Description", -1);

            //Get Marital Statuses
            ViewData["Marital_Status_ID"] = new SelectList(_dropRepository.GetMaritalStatuses(true), "Marital_Status_ID", "Description", -1);


            // Priority Levels

            ViewData["Priority_Level"] = new SelectList(_dropRepository.GetPriorityLevels(true), "Key", "Value", 1);

            //Record Eligibilities
            var recordEligibilities = _dropRepository.GetRecordEligibility(true);
            ViewData["Record_Eligibility"] = new SelectList(recordEligibilities, "Record_Eligibility_ID", "CategoryDescription", record.Record_Eligibility_ID);

            //Get Employment Types
            var empStatus = db.Employment_Status.OrderBy(m => m.Display_Order);
            ViewData["Employment_Status_ID"] = new SelectList(empStatus, "Employment_Status_ID", "Description", record.Employment_Status_ID);

            ////Get Occupation Types
            //var occupations = db.Occupations.Where(r => r.Active == true).OrderBy(r => r.Display_Order);
            //ViewData["Occupation_ID"] = new SelectList(occupations, "Occupation_ID", "Description", record.Occupation_ID);

            //Get Tobacco Types
            ViewData["Current_Tobacco_ID"] = new SelectList(_dropRepository.GetTobaccoTypes(true), "Tobacco_ID", "Description", -1);
            ViewData["Past_Tobacco_ID"] = new SelectList(_dropRepository.GetTobaccoTypes(true), "Tobacco_ID", "Description", -1);

            //Get Tobacco Uses Status
            ViewData["Current_Tobacco_Use_Status_ID"] = new SelectList(_dropRepository.GetTobaccoStatuses(true), "Tobacco_Use_ID", "Description", -1);
            ViewData["Past_Tobacco_Use_Status_Id"] = new SelectList(_dropRepository.GetTobaccoStatuses(true), "Tobacco_USe_ID", "Description", -1);


            //Get Record Types

            ViewData["Record_Type_ID"] = new SelectList(_dropRepository.GetRecordType(true), "Record_Type_ID", "Description", 1);

            //Get Source Codes

            ViewData["Source_Code_ID"] = new SelectList(_dropRepository.GetSourceCodes(true), "Source_Code_ID", "DropDownName", -1);

            //Get Industries
            var industries = db.Industries.Where(s => s.Active == true);
            ViewData["Company.Industry_ID"] = new SelectList(industries, "Industry_ID", "Description", company.Industry_ID);

            //Get States
            ViewData["Company.State_ID"] = new SelectList(_dropRepository.GetStates(false), "State_ID", "Abbreviation", company.State_ID);

            ViewData["Company"] = company;

            //Get Gender


            ViewData["Sex"] = new SelectList(_dropRepository.GetGender(true), "Key", "Value", "");

            ViewData["Salutation_ID"] = new SelectList(_dropRepository.GetSalutations(), "Salutation_ID", "Description", 1);

            ViewData["Suffix"] = new SelectList(_dropRepository.GetSuffix(), "");


            ViewData["Broker_ID"] = new SelectList(_dropRepository.GetBrokers(false), "Broker_ID", "DropDownName", record.Broker_ID);

            //Current Situation
            ViewData["Record_Current_Situation_ID"] = new SelectList(_dropRepository.GetRecordCurrentSituations(true), "Record_Current_Situation_ID", "Record_Current_Situation_Desc", record.Record_Current_Situation_ID);

            //Product of Interest
            ViewData["Products_Of_Interest_ID"] = new SelectList(_dropRepository.GetProductsOfInterest(true), "Products_Of_Interest_ID", "Products_Of_Interest_Desc", record.Products_Of_Interest_ID);

            //Subsidy
            ViewData["Subsidy_ID"] = new SelectList(_dropRepository.GetSubsidies(true), "Subsidy_ID", "Subsidy_Desc", record.Subsidy_ID);

            //Current Insured Type
            ViewData["Current_Insured_Type_ID"] = new SelectList(_dropRepository.GetCurrentInsuredTypes(true), "Current_Insured_Type_ID", "Current_Insured", record.Current_Insured_Type_ID);

            //Get Distribution List
            ViewData["DistributionList"] = _repository.GetDistributionList(0);

            return View(record);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create(FormCollection collection)
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            Record record = new Record();
            record.Salutation_ID = Int32.Parse(collection["Salutation_ID"]);
            record.Suffix = collection["Suffix"];
            record.First_Name = collection["First_Name"];
            record.Last_Name = collection["Last_Name"];
            record.Middle_Initial = collection["Middle_Initial"];
            record.Address_1 = collection["Address_1"];
            record.Address_2 = collection["Address_2"];
            record.City = collection["City"];
            record.State_ID = Int32.Parse(collection["State_ID"]);
            record.Zipcode = collection["Zipcode"];
            record.County = collection["County"];
            record.Alt_First_Name = collection["Alt_First_Name"];
            record.Alt_Last_Name = collection["Alt_Last_Name"];
            record.Alt_Address_1 = collection["Alt_Address_1"];
            record.Alt_Address_2 = collection["Alt_Address_2"];
            record.Alt_City = collection["Alt_City"];
            int astateid;
            if (Int32.TryParse(collection["Alt_State_ID"], out astateid))
            {
                record.Alt_State_ID = Int32.Parse(collection["Alt_State_ID"]);
            }
            record.Alt_Zipcode = collection["Alt_Zipcode"];
            DateTime aafd;
            if (DateTime.TryParse(collection["Alt_Address_From_Date"], out aafd))
            {
                record.Alt_Address_From_Date = DateTime.Parse(collection["Alt_Address_From_Date"]);
            }
            else
            {
                record.Alt_Address_From_Date = null;
            }
            DateTime aatd;
            if (DateTime.TryParse(collection["Alt_Address_To_Date"], out aatd))
            {
                record.Alt_Address_To_Date = DateTime.Parse(collection["Alt_Address_To_Date"]);
            }
            else
            {
                record.Alt_Address_To_Date = null;
            }
            DateTime bd;
            if (DateTime.TryParse(collection["DOB"], out bd))
            {
                record.DOB = bd;
            }
            else
            {
                record.DOB = null;
            }
            int age;
            if (Int32.TryParse(collection["Age"], out age))
            {
                record.Age = age;
            }
            else
            {
                record.Age = null;
            }
            record.Home_Phone = collection["Home_Phone"];
            record.Work_Phone = collection["Work_Phone"];
            record.Work_Phone_Ext = collection["Work_Phone_Ext"];
            record.Cell_Phone = collection["Cell_Phone"];
            record.Fax = collection["Fax"];
            record.Email_Address = collection["Email_Address"];
            record.Alt_Email_Address = collection["Alt_Email_Address"];
            record.Alt_Phone_Number = collection["Alt_Phone_Number"];
            record.Email_Address_2 = collection["Email_Address_2"];
            record.Momentum = collection["Momentum"].Contains("true");
            record.Do_Not_Contact = collection["Do_Not_Contact"].Contains("true");
            record.Opt_Out_Email = collection["Opt_Out_Email"].Contains("true");
            record.B2E = collection["B2E"].Contains("true");
            record.Source_Code_ID = Int32.Parse(collection["Source_Code_ID"]);
            record.Record_Type_ID = Int32.Parse(collection["Record_Type_ID"]);
            record.Agent_ID = Int32.Parse(collection["Agent_ID"]);
            record.Assistant_Agent_ID = Int32.Parse(collection["Assistant_Agent_ID"]);
            record.Reffered_By = collection["Reffered_By"];
            record.Referral_Type_ID = Int32.Parse(collection["Referral_Type_ID"]);
            record.Broker_ID = Int32.Parse(collection["Broker_ID"]);
            record.Products_Of_Interest_ID = Int32.Parse(collection["Products_Of_Interest_ID"]);
            record.Record_Current_Situation_ID = Int32.Parse(collection["Record_Current_Situation_ID"]);
            record.Opt_Out_Newsletter = collection["Opt_Out_Newsletter"].Contains("true");
            DateTime rsd;
            if (DateTime.TryParse(collection["RequestedStartDate"], out rsd))
            {
                record.RequestedStartDate = rsd;
            }
            else
            {
                record.RequestedStartDate = null;
            }
            record.Notes = StringHelper.EncryptString(collection["Notes"]);
            record.Medicare_FirstName = collection["Medicare_FirstName"];
            record.Medicare_InitialName = collection["Medicare_InitialName"];
            record.Medicare_LastName = collection["Medicare_LastName"];
            //remove ssn dashes
            if (collection["SSN"].Trim() != "")
            {
                string tempssn = collection["SSN"].Remove(collection["SSN"].IndexOf("-"), 1);
                record.SSN = StringHelper.EncryptString(tempssn.Remove(tempssn.IndexOf("-"), 1));
            }
            record.Medicare_Number = StringHelper.EncryptString(collection["Medicare_Number"]);
            record.Part_A_Effective = collection["Part_A_Effective"].Contains("true");
            record.Part_B_Effective = collection["Part_B_Effective"].Contains("true");
            DateTime paed;
            if (DateTime.TryParse(collection["Part_A_Effective_Date"], out paed))
            {
                record.Part_A_Effective_Date = paed;
            }
            else
            {
                record.Part_A_Effective_Date = null;
            }
            DateTime pbed;
            if (DateTime.TryParse(collection["Part_B_Effective_Date"], out pbed))
            {
                record.Part_B_Effective_Date = pbed;
            }
            else
            {
                record.Part_B_Effective_Date = null;
            }
            record.Drug_List_ID = StringHelper.EncryptString(collection["Drug_List_ID"]);
            decimal ai;
            if (Decimal.TryParse(collection["Annual_Income"], out ai))
            {
                record.Annual_Income = ai;
            }
            else
            {
                record.Annual_Income = 0.00m;
            }
            record.Employment_Status_ID = Int32.Parse(collection["Employment_Status_ID"]);
            //record.Occupation_ID = Int32.Parse(collection["Occupation_ID"]);
            record.Occupation = collection["Occupation"];
            record.Sex = collection["Sex"];
            if (record.Sex != "M" && record.Sex != "F")
                record.Sex = "U";
            record.Marital_Status_ID = Int32.Parse(collection["Marital_Status_ID"]);            //Get mailing address information if exists
            bool isChecked = (collection["IsMailing_Address"] != "false");
            record.IsMailing_Address = isChecked;
            if (record.IsMailing_Address == false)
            {

                record.Mailing_Address_1 = collection["Mailing_Address_1"];
                record.Mailing_Address_2 = collection["txtMailing_Address_2"];
                record.Mailing_City = collection["txtMailing_City"];
                record.Mailing_State_ID = Convert.ToInt32(collection["dlMailing_State_ID"]);
                record.Mailing_ZipCode = collection["txtMailing_ZipCode"];

            }

            // Calculate Height and Weight
            int Feet;
            int Inches;
            if (Int32.TryParse(collection["Height_Feet"], out Feet) && Int32.TryParse(collection["Height_Inches"], out Inches))
            {
                record.Height = Feet * 12 + Inches;
            }
            int weight;
            if (Int32.TryParse(collection["Weight"], out weight))
            {
                record.Weight = weight;
            }
            else
            {
                record.Weight = 0;
            }
            record.Subsidy_ID = Int32.Parse(collection["Subsidy_ID"]);
            record.Currently_Insured = collection["Currently_Insured"].Contains("true");
            record.Medical_Carrier = collection["Medical_Carrier"];
            record.Drug_Plan_Co = collection["Drug_Plan_Co"];
            record.Current_Tobacco_Use_Status_ID = Int32.Parse(collection["Current_Tobacco_Use_Status_ID"]);
            record.Current_Tobacco_ID = Int32.Parse(collection["Current_Tobacco_ID"]);
            record.Past_Tobacco_Use_Status_Id = Int32.Parse(collection["Past_Tobacco_Use_Status_Id"]);
            record.Past_Tobacco_ID = Int32.Parse(collection["Past_Tobacco_ID"]);
            record.Quit_Date = collection["Quit_Date"];
            record.Current_Insured_Type_ID = Int32.Parse(collection["Current_Insured_Type_ID"]);
            DateTime osd;
            if (DateTime.TryParse(collection["Original_Start_Date"], out osd))
            {
                record.Original_Start_Date = osd;
            }
            else
            {
                record.Original_Start_Date = null;
            }
            DateTime dposd;
            if (DateTime.TryParse(collection["Drug_Plan_Original_Start_Date"], out dposd))
            {
                record.Drug_Plan_Original_Start_Date = dposd;
            }
            else
            {
                record.Drug_Plan_Original_Start_Date = null;
            }

            // Set Last Update Date
            record.Last_Updated_Date = DateTime.Now;
            record.Alt_Address_1 = collection["Alt_Address_1"];
            record.Alt_Address_2 = collection["Alt_Address_2"];
            record.Alt_City = collection["Alt_City"];
            record.Alt_State_ID = Int32.Parse(collection["Alt_State_ID"]);
            record.Alt_Zipcode = collection["Alt_Zipcode"];


            if (record.Last_Name.Trim().Length < 1)
            {
                ModelState.AddModelError("Last_Name", "Last Name Can Not Be Blank");
                ModelState.SetModelValue("Last_Name", new ValueProviderResult(ValueProvider.GetValue("Last_Name").AttemptedValue, collection["Last_Name"], System.Globalization.CultureInfo.CurrentCulture));
            }

            if (record.First_Name.Trim().Length < 1)
            {
                ModelState.AddModelError("First_Name", "First Name Can Not Be Blank");
                ModelState.SetModelValue("First_Name", new ValueProviderResult(ValueProvider.GetValue("First_Name").AttemptedValue, collection["First_Name"], System.Globalization.CultureInfo.CurrentCulture));
            }

            if (record.Source_Code_ID < 1)
            {
                ModelState.AddModelError("Source_Code_ID", "Source Code Can Not Be Blank");
                ModelState.SetModelValue("Source_Code_ID", new ValueProviderResult(ValueProvider.GetValue("Source_Code_ID").AttemptedValue, collection["Source_Code_ID"], System.Globalization.CultureInfo.CurrentCulture));
            }

            LMS.Data.User user = _userRepository.GetUserFromADName(User.Identity.Name.Replace(@"LONGEVITY\", ""));
            if ((record.Record_Type_ID == 2 || record.Record_Type_ID == 3) && (user.Group_ID == 1 || user.Group_ID == 5 && user.Security_Level_ID != 5
                ))
            {
                if (record.Products_Of_Interest_ID.Value < 1)
                {
                    ModelState.AddModelError("Products_Of_Interest_ID", "Products of Interest Cannot Be Blank");
                    ModelState.SetModelValue("Products_Of_Interest_ID", new ValueProviderResult(ValueProvider.GetValue("Products_Of_Interest_ID").AttemptedValue, collection["Products_Of_Interest_ID"], System.Globalization.CultureInfo.CurrentCulture));
                }

                if (record.Record_Current_Situation_ID.Value < 1)
                {
                    ModelState.AddModelError("Record_Current_Situation_ID", "Current Situation Cannot Be Blank");
                    ModelState.SetModelValue("Record_Current_Situation_ID", new ValueProviderResult(ValueProvider.GetValue("Record_Current_Situation_ID").AttemptedValue, collection["Record_Current_Situation_ID"], System.Globalization.CultureInfo.CurrentCulture));
                }

                if (record.RequestedStartDate.ToString() == "")
                {
                    ModelState.AddModelError("RequestedStartDate", "Requested Start Date Cannot Be Blank");
                    ModelState.SetModelValue("RequestedStartDate", new ValueProviderResult(ValueProvider.GetValue("RequestedStartDate").AttemptedValue, collection["RequestedStartDate"], System.Globalization.CultureInfo.CurrentCulture));
                }
                if (record.DOB.ToString() == "")
                {
                    ModelState.AddModelError("DOB", "Date of Birth Cannot Be Blank");
                    ModelState.SetModelValue("DOB", new ValueProviderResult(ValueProvider.GetValue("DOB").AttemptedValue, collection["DOB"], System.Globalization.CultureInfo.CurrentCulture));
                }
            }
            else
            {

            }

            if (record.IsMailing_Address == false && record.Mailing_Address_1.Trim().Length < 1)
            {
                ModelState.AddModelError("Mailing_Address_1", "Please enter address.");
                ModelState.SetModelValue("Mailing_Address_1", new ValueProviderResult(ValueProvider.GetValue("Mailing_Address_1").AttemptedValue, collection["Mailing_Address_1"], System.Globalization.CultureInfo.CurrentCulture));
            }


            // If Valid Save
            if (ModelState.IsValid)
            {
                try
                {
                    // IF B2E save company and assign ID
                    if (record.B2E)
                    {
                        Company Company = new Company();

                        Company.Address_1 = collection["Company.Name"];
                        Company.Address_2 = collection["Company.Address_2"];
                        Company.City = collection["Company.City"];
                        Company.Company_Phone = collection["Company.Company_Phone"];
                        Company.Company_President = collection["Company.Company_President"];
                        Company.Company_Size = collection["Company.Company_Size"];
                        Company.Email_Address = collection["Company.Email_Address"];
                        Company.Industry_ID = Convert.ToInt32(collection["Company.Industry_ID"]);
                        Company.Name = collection["Company.Name"];
                        Company.Primary_Contact = collection["Company.Primary_Contact"];
                        Company.Primary_Email = collection["Company.Primary_Email"];
                        Company.Primary_Phone = collection["Company.Primary_Phone"];
                        Company.State_ID = Convert.ToInt32(collection["Company.State_ID"]);
                        Company.Title = collection["Company.Title"];
                        Company.Web_Address = collection["Company.Web_Address"];
                        Company.Zipcode = collection["Company.Zipcode"];

                        _repository.UpdateCompany(Company);
                        _repository.Save();

                        record.Company_ID = Company.Company_ID;

                    }

                    _repository.Insert(record);

                    _repository.Save();
                    int Record_ID = record.Record_ID;
                    // Save record distribution list
                    bool[] subscribed = new bool[_repository.GetDistributionList(Record_ID).Count()];
                    Record_Distribution Record_Distribution = null;
                    int distribution_type_id;
                    int? record_id;

                    for (int i = 0; i < subscribed.Count(); i++)
                    {
                        distribution_type_id = _repository.GetDistributionList(Record_ID)[i].Record_Distribution_Type_ID;
                        record_id = _repository.GetDistributionList(Record_ID)[i].Record_ID;
                        subscribed[i] = Boolean.Parse(Request.Form.GetValues(distribution_type_id + "_" + _repository.GetDistributionList(Record_ID)[i].Record_Distribution_Type_Description.Replace(' ', '_'))[0]);
                        if (subscribed[i])
                        {
                            Record_Distribution = new Record_Distribution();
                            Record_Distribution.Record_ID = Record_ID;
                            Record_Distribution.Record_Distribution_Type_ID = distribution_type_id;
                            db.Record_Distributions.InsertOnSubmit(Record_Distribution);
                            db.SubmitChanges();
                        }

                    }

                    return RedirectToAction("Details", new { id = record.Record_ID });
                }
                catch (Exception ex)
                {
                    LMS.ErrorHandler.HandleError.EmailError(ex);
                }


            }

            //LMS.Data.User user = UserRepository.GetUser(User.Identity.Name);

            //Data Context

            Company company = new Company();

            //Get Agents
            var agents = _dropRepository.GetAgents(true);
            ViewData["Agent_ID"] = new SelectList(agents, "User_ID", "FullName", record.Agent_ID);
            ViewData["Assistant_Agent_ID"] = new SelectList(agents, "User_ID", "FullName", record.Assistant_Agent_ID);

            //Get States
            ViewData["State_ID"] = new SelectList(_dropRepository.GetStates(false), "State_ID", "Abbreviation", record.State_ID);
            ViewData["Alt_State_ID"] = new SelectList(_dropRepository.GetStates(false), "State_ID", "Abbreviation", record.Alt_State_ID);
            ViewData["Mailing_State_ID"] = new SelectList(_dropRepository.GetStates(false), "State_ID", "Abbreviation", record.Mailing_State_ID);

            //Get Referral Types
            var refTypes = db.Referral_Types.Where(r => r.Active == true);
            ViewData["Referral_Type_ID"] = new SelectList(refTypes, "Referral_Type_ID", "Referral_Description", record.Referral_Type_ID);

            //Get Buying Period

            ViewData["Buying_Period_ID"] = new SelectList(_dropRepository.GetBuyingPeriods(true), "Buying_Period_ID", "Description", -1);

            //Get Buying Period

            ViewData["Buying_Period_Sub_ID"] = new SelectList(_dropRepository.GetBuyingPeriodSubs(true), "Buying_Period_Sub_ID", "Description", -1);

            //Get Marital Statuses
            ViewData["Marital_Status_ID"] = new SelectList(_dropRepository.GetMaritalStatuses(true), "Marital_Status_ID", "Description", -1);

            // Priority Levels

            ViewData["Priority_Level"] = new SelectList(_dropRepository.GetPriorityLevels(true), "Key", "Value", 1);

            //Record Eligibilities
            var recordEligibilities = _dropRepository.GetRecordEligibility(true);
            ViewData["Record_Eligibility"] = new SelectList(recordEligibilities, "Record_Eligibility_ID", "CategoryDescription", record.Record_Eligibility_ID);

            //Get Referral Types
            var empStatus = db.Employment_Status;
            ViewData["Employment_Status_ID"] = new SelectList(empStatus, "Employment_Status_ID", "Description", record.Employment_Status_ID);



            ////Get Referral Types
            //var occupations = db.Occupations.Where(r => r.Active == true);
            //ViewData["Occupation_ID"] = new SelectList(occupations, "Occupation_ID", "Description", record.Occupation_ID);

            //Get Referral Types
            ViewData["Current_Tobacco_ID"] = new SelectList(_dropRepository.GetTobaccoTypes(true), "Tobacco_ID", "Description", -1);
            ViewData["Past_Tobacco_ID"] = new SelectList(_dropRepository.GetTobaccoTypes(true), "Tobacco_ID", "Description", -1);

            //Get Tobacco Uses Status
            ViewData["Current_Tobacco_Use_Status_ID"] = new SelectList(_dropRepository.GetTobaccoStatuses(true), "Tobacco_Use_ID", "Description", -1);
            ViewData["Past_Tobacco_Use_Status_Id"] = new SelectList(_dropRepository.GetTobaccoStatuses(true), "Tobacco_USe_ID", "Description", -1);



            //Get Record Types

            ViewData["Record_Type_ID"] = new SelectList(_dropRepository.GetRecordType(true), "Record_Type_ID", "Description", 1);

            //Get Source Codes

            ViewData["Source_Code_ID"] = new SelectList(_dropRepository.GetSourceCodes(true), "Source_Code_ID", "DropDownName", -1);

            //Get Industries
            var industries = db.Industries.Where(s => s.Active == true);
            ViewData["Company.Industry_ID"] = new SelectList(industries, "Industry_ID", "Description", company.Industry_ID);

            //Get States
            ViewData["Company.State_ID"] = new SelectList(_dropRepository.GetStates(false), "State_ID", "Abbreviation", company.State_ID);

            ViewData["Company"] = company;

            //Get Gender

            ViewData["Sex"] = new SelectList(_dropRepository.GetGender(true), "Key", "Value", "");


            ViewData["Salutation_ID"] = new SelectList(_dropRepository.GetSalutations(), "Salutation_ID", "Description", 1);

            ViewData["Suffix"] = new SelectList(_dropRepository.GetSuffix(), "");

            ViewData["Broker_ID"] = new SelectList(_dropRepository.GetBrokers(false), "Broker_ID", "DropDownName", record.Broker_ID);

            //Current Situation
            ViewData["Record_Current_Situation_ID"] = new SelectList(_dropRepository.GetRecordCurrentSituations(true), "Record_Current_Situation_ID", "Record_Current_Situation_Desc", record.Record_Current_Situation_ID);

            //Product of Interest
            ViewData["Products_Of_Interest_ID"] = new SelectList(_dropRepository.GetProductsOfInterest(true), "Products_Of_Interest_ID", "Products_Of_Interest_Desc", record.Products_Of_Interest_ID);

            //Subsidy
            ViewData["Subsidy_ID"] = new SelectList(_dropRepository.GetSubsidies(true), "Subsidy_ID", "Subsidy_Desc", record.Subsidy_ID);

            //Current Insured Type
            ViewData["Current_Insured_Type_ID"] = new SelectList(_dropRepository.GetCurrentInsuredTypes(true), "Current_Insured_Type_ID", "Current_Insured", record.Current_Insured_Type_ID);

            //Get Distribution List
            ViewData["DistributionList"] = _repository.GetDistributionList(0);

            bool[] sub = new bool[_repository.GetDistributionList(0).Count()];
            int distributiontypeid;
            int? recordid;
            for (int i = 0; i < sub.Count(); i++)
            {
                distributiontypeid = _repository.GetDistributionList(0)[i].Record_Distribution_Type_ID;
                recordid = _repository.GetDistributionList(0)[i].Record_ID;
                sub[i] = Boolean.Parse(Request.Form.GetValues(distributiontypeid + "_" + _repository.GetDistributionList(0)[i].Record_Distribution_Type_Description.Replace(' ', '_'))[0]);
                if (sub[i])
                {
                    ViewData[distributiontypeid + "_" + _repository.GetDistributionList(0)[i].Record_Distribution_Type_Description] = "true";
                }
            }
            return View(record);


        }


        public ActionResult Search()
        {

            LMS.Data.User user = UserRepository.GetUser(User.Identity.Name);

            ViewData["SourceCodeID"] = new SelectList(_dropRepository.GetSourceCodes(true), "Source_Code_ID", "DropDownName");
            ViewData["PlanSourceCodeID"] = new SelectList(_dropRepository.GetSourceCodes(true), "Source_Code_ID", "DropDownName");

            ViewData["RecordTypeID"] = new SelectList(_dropRepository.GetRecordStatus(true), "Record_Type_ID", "Description");

            if (user.Security_Level_ID == 1)
            {
                ViewData["AgentID"] = new SelectList(_dropRepository.GetAgents(true), "User_ID", "FullName", user.User_ID);
            }
            else
            {
                ViewData["AgentID"] = new SelectList(_dropRepository.GetAgents(true), "User_ID", "FullName");
            }

            ViewData["FollowUpAgent"] = new SelectList(_dropRepository.GetAgents(true), "User_ID", "FullName");
            ViewData["AssistantAgentID"] = new SelectList(_dropRepository.GetAgents(true), "User_ID", "FullName");
            ViewData["StateID"] = new SelectList(_dropRepository.GetStates(true), "State_ID", "Abbreviation");
            ViewData["QStateID"] = new SelectList(_dropRepository.GetStates(true), "State_ID", "Abbreviation");
            ViewData["PriorityLevel"] = new SelectList(_dropRepository.GetPriorityLevels(true), "Key", "Value");
            ViewData["BuyingPeriodID"] = new SelectList(_dropRepository.GetBuyingPeriods(true), "Buying_Period_ID", "Description");
            ViewData["BuyingPeriodSubID"] = new SelectList(_dropRepository.GetBuyingPeriodSubs(true), "Buying_Period_Sub_ID", "Description");
            ViewData["PlanStatus"] = new SelectList(_dropRepository.GetTransactionStatus(true), "Plan_Transaction_Status_ID", "Description");
            ViewData["ReconcilliationType"] = new SelectList(_dropRepository.GetReconciliationTypes(true, null), "Reconciliation_Type_ID", "Description");
            ViewData["TransactionDateType"] = new SelectList(_dropRepository.GetTransactionDates(true), "Transaction_Date_Type_ID", "Description");
            ViewData["RecordActivityType"] = new SelectList(_dropRepository.GetActivityTypes(true), "Activity_Type_ID", "Description");
            ViewData["RecordEligibilityType"] = new SelectList(_dropRepository.GetRecordEligibility(true), "Record_Eligibility_ID", "Description");
            ViewData["RecordActivityStatus"] = new SelectList(_dropRepository.GetActivityStatus(true, null), "Record_Activity_Status_ID", "Description");
            ViewData["Record_Current_Situation_ID"] = new SelectList(_dropRepository.GetRecordCurrentSituations(true), "Record_Current_Situation_ID", "Record_Current_Situation_Desc");
            //Get Distribution List
            ViewData["DistributionList"] = _repository.GetDistributionList(0);
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Search(string btnSearch, string FirstName, string QFirstName, string QMiddleName, string QLastName, int QStateID, string QEmailAddress, string LastName, int SourceCodeID, int AgentID,
            int AssistantAgentID, string PhoneNumber, int RecordTypeID, int StateID,
            string Zipcode, string CreatedOnFrom, string CreatedOnTo, int BuyingPeriodID, string FollowUpFrom,
            string FollowUpTo, int BuyingPeriodSubID, string EmailAddress, int FollowUpAgent, string ContactName,
            string ContactEmail, string ContactPhone, int PlanStatus, string Customer_ID, bool ViewBadData, int PlanSourceCodeID, int ReconcilliationType, int TransactionDateType, bool ViewOpenActivities, string TransactionDateFromValue, string TransactionDateToValue, string QDOB,
            int RecordActivityType, int RecordEligibilityType, int RecordActivityStatus, int Record_Current_Situation_ID, int PriorityLevel, FormCollection collection)
        {
            bool[] sub = new bool[_repository.GetDistributionList(0).Count()];
            string distributiontypeids = "";
            int distributiontypeid;
            for (int i = 0; i < sub.Count(); i++)
            {
                distributiontypeid = _repository.GetDistributionList(0)[i].Record_Distribution_Type_ID;
                sub[i] = Boolean.Parse(Request.Form.GetValues(distributiontypeid + "_" + _repository.GetDistributionList(0)[i].Record_Distribution_Type_Description.Replace(' ', '_'))[0]);
                if (sub[i])
                {
                    distributiontypeids = distributiontypeids + "," + _repository.GetDistributionList(0)[i].Record_Distribution_Type_ID.ToString();
                }
                if (distributiontypeids.Length > 0)
                {
                    distributiontypeids = distributiontypeids.Substring(1);
                }
            }
            if (btnSearch == "s")
            {
                var list = _repository.Search(FirstName.Replace("'", "''"), LastName.Replace("'", "''"), SourceCodeID, AgentID, AssistantAgentID, PhoneNumber,
                    RecordTypeID, StateID, Zipcode, CreatedOnFrom, CreatedOnTo, BuyingPeriodID, FollowUpFrom,
                    FollowUpTo, BuyingPeriodSubID, EmailAddress, FollowUpAgent, ContactName, ContactEmail, ContactPhone, PlanStatus, Customer_ID, ViewBadData, PlanSourceCodeID,
                    ReconcilliationType, TransactionDateType, ViewOpenActivities, TransactionDateFromValue, TransactionDateToValue, RecordActivityType,
                    RecordEligibilityType, RecordActivityStatus, Record_Current_Situation_ID, PriorityLevel, distributiontypeids);

                ViewData["SearchData"] = list;

            }
            else
            {
                //Required field validations for First name, Last name, DOB and Email address.
                if (QFirstName.Equals(""))
                {
                    ModelState.AddModelError("", "First Name can not be empty");
                }
                if (QLastName.Equals(""))
                {
                    ModelState.AddModelError("", "Last Name Can not be empty");
                }
                if (ModelState.IsValid)
                {
                    try
                    {
                        var quickList = _repository.QuickSearch(QFirstName.Replace("'", "''"), QMiddleName.Replace("'", "''"), QLastName.Replace("'", "''"), QStateID, QDOB, QEmailAddress);

                        ViewData["QuickSearchData"] = quickList;



                    }
                    catch (Exception ex)
                    {
                        LMS.ErrorHandler.HandleError.EmailError(ex);
                    }


                }



            }


            ViewData["SourceCodeID"] = new SelectList(_dropRepository.GetSourceCodes(true), "Source_Code_ID", "DropDownName", SourceCodeID);
            ViewData["PlanSourceCodeID"] = new SelectList(_dropRepository.GetSourceCodes(true), "Source_Code_ID", "DropDownName", PlanSourceCodeID);
            ViewData["RecordTypeID"] = new SelectList(_dropRepository.GetRecordStatus(true), "Record_Type_ID", "Description", RecordTypeID);
            ViewData["AgentID"] = new SelectList(_dropRepository.GetAgents(true), "User_ID", "FullName", AgentID);
            ViewData["AssistantAgentID"] = new SelectList(_dropRepository.GetAgents(true), "User_ID", "FullName", AssistantAgentID);
            ViewData["StateID"] = new SelectList(_dropRepository.GetStates(true), "State_ID", "Abbreviation", StateID);
            ViewData["QStateID"] = new SelectList(_dropRepository.GetStates(true), "State_ID", "Abbreviation", QStateID);
            ViewData["PriorityLevel"] = new SelectList(_dropRepository.GetPriorityLevels(true), "Key", "Value", PriorityLevel);
            ViewData["BuyingPeriodID"] = new SelectList(_dropRepository.GetBuyingPeriods(true), "Buying_Period_ID", "Description", BuyingPeriodID);
            ViewData["BuyingPeriodSubID"] = new SelectList(_dropRepository.GetBuyingPeriodSubs(true), "Buying_Period_Sub_ID", "Description", BuyingPeriodSubID);
            ViewData["FollowUpAgent"] = new SelectList(_dropRepository.GetAgents(true), "User_ID", "FullName", FollowUpAgent);
            ViewData["PlanStatus"] = new SelectList(_dropRepository.GetTransactionStatus(true), "Plan_Transaction_Status_ID", "Description", PlanStatus);
            ViewData["ReconcilliationType"] = new SelectList(_dropRepository.GetReconciliationTypes(true, null), "Reconciliation_Type_ID", "Description", ReconcilliationType);
            ViewData["TransactionDateType"] = new SelectList(_dropRepository.GetTransactionDates(true), "Transaction_Date_Type_ID", "Description", TransactionDateType);
            ViewData["RecordActivityType"] = new SelectList(_dropRepository.GetActivityTypes(true), "Activity_Type_ID", "Description", RecordActivityType);
            ViewData["RecordEligibilityType"] = new SelectList(_dropRepository.GetRecordEligibility(true), "Record_Eligibility_ID", "Description", RecordEligibilityType);
            ViewData["RecordActivityStatus"] = new SelectList(_dropRepository.GetActivityStatus(true, null), "Record_Activity_Status_ID", "Description", RecordActivityStatus);
            ViewData["Record_Current_Situation_ID"] = new SelectList(_dropRepository.GetRecordCurrentSituations(true), "Record_Current_Situation_ID", "Record_Current_Situation_Desc", Record_Current_Situation_ID);
            //Get Distribution List
            ViewData["DistributionList"] = _repository.GetDistributionList(0);
            return View();
        }

        public ActionResult Prospect()
        {
            int AgentID = UserRepository.GetUser(User.Identity.Name).User_ID;
            ViewData["Prospects"] = _repository.GetProspects(AgentID);

            return View();
        }

        public ActionResult Leads()
        {
            int AgentID = UserRepository.GetUser(User.Identity.Name).User_ID;

            return View(_repository.GetLeads(AgentID));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CreateFollowUp(int id, FormCollection formData)
        {
            ActivityRepository _actRepository = new ActivityRepository();
            LMS.Data.User user = UserRepository.GetUser(User.Identity.Name);
            Record record = _repository.GetRecord(id);
            Record_Activity act = new Record_Activity();

            //List<sp_Has_ActivityResult> rs = db.sp_Has_Activity(Convert.ToInt32(formData["Follow_User_ID"]), id).ToList();
            //bool? Has_Activity = rs[0].hasOCA;
            //if (Has_Activity.HasValue && Has_Activity == true)
            //{
            //    ModelState.AddModelError("Follow_User_ID", "*");
            //}
            if (ModelState.IsValid)
            {
                string activityNotes = formData["Follow_Up_Notes"];
                using (var db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString))
                {
                    var campaignCode = db.Campaign_Codes.FirstOrDefault(i => i.Campaign_Code_ID == Convert.ToInt32(formData["Modal_Follow_Campaign_Code"]));
                    if (campaignCode != null)
                    {
                        activityNotes = campaignCode.Campaign_Number + " - " + campaignCode.Campaign_Description + ". " + activityNotes;
                    }

                    var Record_Activity_Status_ID_FollowUp = Convert.ToInt32(formData["Record_Activity_Status_ID_FollowUp"]);
                    act.Created_On = DateTime.Now;
                    act.Record_ID = id;
                    act.Activity_Type_ID = Convert.ToInt32(formData["Follow_Activity_Type_ID"]);
                    act.Created_By = user.User_ID;
                    act.Due_Date = Convert.ToDateTime(formData["Follow_Up_Date"] + " " + (formData["Follow_Up_Time"] == @"N\A" ? "12:00AM" : formData["Follow_Up_Time"]));
                    act.User_ID = Convert.ToInt32(formData["Follow_User_ID"]);
                    act.Priority_Level = Convert.ToInt32(formData["Follow_Priority_Level"]);
                    //act.Record_Activity_Status_ID = 1;
                    act.Plan_Transaction_ID = Convert.ToInt32(formData["Plan_Transaction_ID"]);
                    act.Notes = StringHelper.EncryptString(activityNotes);
                    act.Record_Activity_Status_ID = Record_Activity_Status_ID_FollowUp;
                    act.Campaign_Code_ID = Convert.ToInt32(formData["Modal_Follow_Campaign_Code"]);

                    _actRepository.Update(act);
                    _actRepository.Save();

                    if (Record_Activity_Status_ID_FollowUp == 33 || Record_Activity_Status_ID_FollowUp == 34)
                    {
                        var complaint = new Complaint();

                        if (Record_Activity_Status_ID_FollowUp == 33)//CREATE OPEN COMPLAINT
                        {
                            complaint = new Complaint()
                            {
                                Current_Record_Activity_Status_ID = Record_Activity_Status_ID_FollowUp,
                                Record_ID = record.Record_ID,
                                Resolved = false
                            };

                            db.Complaints.InsertOnSubmit(complaint);
                            db.SubmitChanges();
                        }
                        else
                        {
                            complaint = db.Complaints.Where(x => x.Record_ID == record.Record_ID && x.Resolved == false).FirstOrDefault();
                            complaint.Current_Record_Activity_Status_ID = Record_Activity_Status_ID_FollowUp;
                            db.SubmitChanges();
                        }

                        var complaintActivity = new Complaint_Activity()
                        {
                            Complaint_ID = complaint.Complaint_ID,
                            Record_Activity_ID = act.Record_Activity_ID,
                            Record_Activity_Status_ID = Record_Activity_Status_ID_FollowUp,
                        };

                        db.Complaint_Activities.InsertOnSubmit(complaintActivity);
                        db.SubmitChanges();
                    }
                };
            }
            return RedirectToAction("Details", "Record", new { id = id });

        }

        public ActionResult Partner()
        {
            ViewData["LeftList"] = new List<LMS.Data.Record>();
            ViewData["RightList"] = new List<LMS.Data.Record>();
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Partner(FormCollection collection)
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

            if (collection["LastName"] != "" || collection["FirstName"] != "" || collection["CustomerID"] != "")
            {
                var origin = from r in db.Records
                             where r.Last_Name.Contains(collection["LastName"]) && r.First_Name.Contains(collection["FirstName"]) && r.Customer_ID.Contains(collection["CustomerID"])
                             select r;

                ViewData["LeftList"] = origin;
            }
            else
            {
                ViewData["LeftList"] = new List<LMS.Data.Record>();
            }

            if (collection["DestLastName"] != "" || collection["DestFirstName"] != "" || collection["DestCustomerID"] != "")
            {
                var dest = from r in db.Records
                           where r.Last_Name.Contains(collection["DestLastName"]) && r.First_Name.Contains(collection["DestFirstName"]) && r.Customer_ID.Contains(collection["DestCustomerID"])
                           select r;

                ViewData["RightList"] = dest;
            }
            else
            {
                ViewData["RightList"] = new List<LMS.Data.Record>();
            }

            return View();
        }

        public ActionResult GetPartnerList(FormCollection collection)
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

            var origin = from r in db.Records
                         where r.Last_Name == collection["LastName"] && r.First_Name == collection["FirstName"]
                         select r;

            if (Request.IsAjaxRequest())
            {
                var partialViewResult = new PartialViewResult();
                partialViewResult.ViewName = "PartnerSearchList";
                partialViewResult.ViewData.Model = origin;

                return partialViewResult;
            }
            else
            {
                return RedirectToAction("Partner");
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Link(FormCollection collection)
        {
            int recordID_1 = Convert.ToInt32(collection["Left_Record_ID"]);
            int recordID_2 = Convert.ToInt32(collection["Right_Record_ID"]);

            Record r1 = _repository.GetRecord(recordID_1);
            Record r2 = _repository.GetRecord(recordID_2);

            r2.Partner_ID = r1.Record_ID;
            r1.Partner_ID = r2.Record_ID;

            _repository.Save();

            return RedirectToAction("Search");
        }

        public ActionResult Merge()
        {
            ViewData["LeftList"] = new List<LMS.Data.Record>();
            ViewData["RightList"] = new List<LMS.Data.Record>();
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Merge(FormCollection collection)
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

            if (collection["LastName"] != "" || collection["FirstName"] != "" || collection["CustomerID"] != "")
            {
                var origin = from r in db.Records
                             where r.Last_Name.Contains(collection["LastName"]) && r.First_Name.Contains(collection["FirstName"]) && r.Customer_ID.Contains(collection["CustomerID"])
                             select r;

                ViewData["LeftList"] = origin;
            }
            else
            {
                ViewData["LeftList"] = new List<LMS.Data.Record>();
            }

            if (collection["DestLastName"] != "" || collection["DestFirstName"] != "" || collection["DestCustomerID"] != "")
            {
                var dest = from r in db.Records
                           where r.Last_Name.Contains(collection["DestLastName"]) && r.First_Name.Contains(collection["DestFirstName"]) && r.Customer_ID.Contains(collection["DestCustomerID"])
                           select r;

                ViewData["RightList"] = dest;
            }
            else
            {
                ViewData["RightList"] = new List<LMS.Data.Record>();
            }

            return View();
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MergeItems(FormCollection collection)
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

            int recordID_1 = Convert.ToInt32(collection["Left_Record_ID"]);
            int recordID_2 = Convert.ToInt32(collection["Right_Record_ID"]);
            string lms2lms = Convert.ToString(collection["LMS2LMS"]);

            _recordActivtyRepository.GetRecordActivitiesDisplay(recordID_1);

            Record_Activity ra1 = db.Record_Activities.Where(ra => ra.Record_ID == recordID_1).FirstOrDefault();

            var recordactivity = from ra in db.Record_Activities
                                 where ra.Record_ID == recordID_1
                                 select ra;





            //Determine what kind of merge type is necessary.

            if (collection["LMS2LMS"] != "false")
            {
                if (ra1 != null)
                {
                    ra1.Record_ID = recordID_2;
                    _recordActivtyRepository.Update(ra1);
                    _recordActivtyRepository.Save();
                }
            }
            if (collection["LMS2GBS"] != "false")
            {

            }







            return RedirectToAction("Search");
        }

        public ActionResult Delete(int id)
        {
            try
            {
                Record r = _repository.GetRecord(id);

                _repository.Delete(r);

                _repository.Save();
            }
            catch (Exception ex)
            {
                LMS.ErrorHandler.HandleError.EmailError(ex);
            }
            return RedirectToAction("Search");
        }

        /// <summary>
        /// Verify Reconcilliation record
        /// </summary>
        /// <param name="id">RecordID</param>
        /// <returns>redirect back to support page</returns>
        public ActionResult Verify(int id)
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            db.sp_Verify_Reconcilliation(id, UserRepository.GetUser(User.Identity.Name).User_ID);
            return RedirectToAction("Support");
        }

        public ActionResult AppointmentSetter()
        {
            LMS.Data.User user = UserRepository.GetUser(User.Identity.Name);
            var myLeads = _repository.GetAppointmentLeads(user.User_ID);

            return View(myLeads);
        }

        public ActionResult Refresh(int statusid)
        {
            if (statusid > 0)
            {
                _userRepository.UpdateUserOnlineStatus(User.Identity.Name, statusid);

            }
            return View();
        }

        public ActionResult RefreshEdit(int id)
        {
            ViewData["InEditBy"] = TableLockHelper.CheckLock("Record", id, User.Identity.Name.Replace(@"LONGEVITY\", "")).ToLower();
            return View();
        }

        //public ActionResult HasActivity(int recordid, int userid)
        //{
        //    LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
        //    List<sp_Has_ActivityResult> rs = db.sp_Has_Activity(userid, recordid).ToList();
        //    return View(rs[0]);
        //}

        public ActionResult NewLeads()
        {
            var agents = _userRepository.GetActiveAgents();

            ViewData["Agent_ID"] = new SelectList(agents, "User_ID", "FullName");

            return View(_repository.GetOtherUnassinedLeads(""));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewLeads(FormCollection collection)
        {
            // Action String
            string action = "";

            // Check for Assign
            if (collection.AllKeys.Contains("Assign"))
                action = "Assign";

            // Check for Bad Leads
            if (collection.AllKeys.Contains("BadLeads"))
                action = "Bad";

            // Check for Assign
            if (collection.AllKeys.Contains("Duplicate"))
                action = "Duplicate";

            // Check for Repeat
            if (collection.AllKeys.Contains("Repeat"))
                action = "Repeat";

            // Check for Repeat
            if (collection.AllKeys.Contains("Setter"))
                action = "Setter";

            // Check for Assign
            if (collection.AllKeys.Contains("Delete"))
                action = "Delete";

            foreach (string key in collection.AllKeys)
            {
                if (key.StartsWith("Record"))
                {
                    if (collection[key].StartsWith("true"))
                    {
                        int Record_ID = Convert.ToInt32(key.Substring(6));

                        switch (action)
                        {
                            case "Assign":
                                _repository.AssignLead(Record_ID, Convert.ToInt32(collection["Agent_ID"]));
                                break;
                            case "Bad":
                                _repository.BadLead(Record_ID);
                                break;
                            case "Duplicate":
                                _repository.DuplicateLead(Record_ID);
                                break;
                            case "Repeat":
                                _repository.RepeatLead(Record_ID);
                                break;
                            case "Setter":
                                _repository.AssignAppointmentSetter(Record_ID, Convert.ToInt32(collection["Agent_ID"]));
                                break;
                            case "Delete":
                                _repository.Delete(Record_ID);
                                break;
                            default:
                                break;
                        }

                    }
                }
            }

            ViewData["Agent_ID"] = new SelectList(_dropRepository.GetAgents(false), "User_ID", "FullName");
            return View(_repository.GetOtherUnassinedLeads(UserRepository.GetUserName(User.Identity.Name)));


        }

    }

}
