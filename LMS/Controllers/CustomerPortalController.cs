﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerPortalController.cs" company="Longevity Allaince">
//   Longevity Allaince 2014(c), All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace LMS.Controllers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using LMS.Models;

    using LmsDataRepository.Models;

    /// <summary>
    /// The customer portal controller.
    /// </summary>
    public class CustomerPortalController : Controller
    {
        #region Public Methods and Operators

        /// <summary>
        /// The index.
        /// </summary>
        /// <param name="recordId">
        /// The record Id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Index(int id)
        {
            var viewModel = new LMS.Models.CustomerPortalViewModel();
            var record = new Record();
            using (var context = new LMSContext())
            {
                record = context.Records.FirstOrDefault(t => t.Record_ID == id);
            }

            if (record == null)
            {
                viewModel.RecordId = 0;
                return this.View(viewModel);
            }

            viewModel.CustomerId = record.Customer_ID;
            viewModel.FirstName = record.First_Name;
            viewModel.LastName = record.Last_Name;
            viewModel.MiddleInitial = record.Middle_Initial;
            viewModel.RecordId = record.Record_ID;

            return this.View(viewModel);
        }

        public ActionResult CustomerInformation(int id)
        {
            var viewModel = CustomerInformationViewModel.GetViewModel(id);
            return this.View(viewModel);
        }

        public ActionResult CoverageProfile(int id)
        {
            var viewModel = new CoverageProfileViewModel();
            return this.View(viewModel);
        }

        public ActionResult CustomerProfile(int id)
        {
            var viewModel = new LMS.Models.CustomerProfileViewModel();
            return this.View(viewModel);
        }

        public ActionResult B2EProfile(int id)
        {
            var viewModel = new LMS.Models.B2EProfileViewModel();
            return this.View(viewModel);
        }

        public ActionResult Policies(int id)
        {
            var viewModel = new LMS.Models.PoliciesViewModel();
            return this.View(viewModel);
        }

        public ActionResult Policy(int id)
        {
            var viewModel = new LMS.Models.PolicyViewModel();
            return this.View(viewModel);
        }

        //public ActionResult RecordActivities(int id)
        //{
        //    var viewModel = new LMS.Models.RecordActivityViewModel();
        //    return this.View(viewModel);
        //}

        //public ActionResult RecordActivity(int id)
        //{
        //    var viewModel = new LMS.Models.RecordActivityViewModel();
        //    return this.View(viewModel);
        //}

        //[HttpPost]
        //public ActionResult RecordActivity(RecordActivityViewModel recordActivityViewModel)
        //{


        //    var id = recordActivity();
        //    return this.RecordActivities(id);
        //}

        #endregion
    }
}