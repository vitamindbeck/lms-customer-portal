using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using LMS.Data;
using LMS.Data.Repository;
using LMS.CustomActionResults;
using System.Collections;

namespace LMS.Controllers
{
    public class AdminController : Controller
    {
        private IDropDownRepository _dropRepository;
        private IReportRepository _reportRepository;
        private IRecordRepository _recordRepository;
        private Dictionary<string, Section> _ReportData;

        public AdminController()
            : this(new DropDownRepository(), new ReportRepository(), new RecordRepository())
        {
        }

        public AdminController(IDropDownRepository droprepository, IReportRepository reportrepository, IRecordRepository recrepository)
        {
            _dropRepository = droprepository;
            _reportRepository = reportrepository;
            _recordRepository = recrepository;
        }


        public ActionResult Index()
        {
            int leadcount = 0;
            ViewData["AppReceived"] = new List<LMS.Data.Agent_App_Received_ReportResult>();
            ViewData["AppSent"] = new List<LMS.Data.Agent_App_Sent_ReportResult>();
            ViewData["AppPlaced"] = new List<LMS.Data.Agent_App_Placed_ReportResult>();
            ViewData["AgentLeads"] = new List<LMS.Data.Agent_Leads_By_Source_ReportResult>();
            ViewData["COW"] = new List<LMS.Data.sp_Agent_Call_of_Week_ReportResult>();

            LMS.Data.User user = UserRepository.GetUser(User.Identity.Name);
            if (user.Security_Level_ID == 1)
            {
                Dictionary<int, string> userdata = new Dictionary<int, string>();
                userdata.Add(user.User_ID, user.Last_Name + ", " + user.First_Name);

                ViewData["User_ID"] = new SelectList(userdata,"key","value");
            }

            else
            {
                ViewData["User_ID"] = new SelectList(_dropRepository.GetAgents(false), "User_ID", "FullName");
            }
            ViewData["LeadCount"] = leadcount;
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Index(int User_ID, DateTime? FromDate, DateTime? ToDate, FormCollection collection)
        {
            int leadcount = 0;
            if (!FromDate.HasValue)
            {
                ModelState.AddModelError("FromDate", "You must Pick a From Date");
            }
            if (!ToDate.HasValue)
            {
                ModelState.AddModelError("ToDate", "You must Pick a To Date");
            }

            if (ModelState.IsValid)
            {
                
                
                ViewData["AppReceived"] = _reportRepository.GetAgentAppReceivedReport(FromDate.Value,ToDate.Value,User_ID);
                ViewData["AppSent"] = _reportRepository.GetAgentAppSentReport(FromDate.Value, ToDate.Value, User_ID);
                ViewData["AppPlaced"] = _reportRepository.GetAgentAppPlacedReport(FromDate.Value, ToDate.Value, User_ID);

                Boolean cow;

                if (String.IsNullOrEmpty(collection["Call_of_Week"]) || collection["Call_of_Week"] == "false")
                {
                    cow = false;
                }
                else 
                {
                    cow = true;
                }
                ViewData["COW"] = _reportRepository.GetAgentCOWs(FromDate.Value, ToDate.Value, User_ID, cow);
                ViewData["Call_of_Week"] = cow.ToString();
                
                
                var leads = _reportRepository.GetAgentLeadBySourceReport(FromDate.Value, ToDate.Value, User_ID);
                foreach(Agent_Leads_By_Source_ReportResult r in leads)
                {
                    if ( r.Count.HasValue)
                        leadcount += r.Count.Value;
                }
                ViewData["AgentLeads"] = leads;
            }
            else
            {
                ViewData["AppReceived"] = new List<LMS.Data.Agent_App_Received_ReportResult>();
                ViewData["AppSent"] = new List<LMS.Data.Agent_App_Sent_ReportResult>();
                ViewData["AgentLeads"] = new List<LMS.Data.Agent_Leads_By_Source_ReportResult>();
                ViewData["AppPlaced"] = new List<LMS.Data.Agent_App_Placed_ReportResult>();
                ViewData["COW"] = new List<LMS.Data.sp_Agent_Call_of_Week_ReportResult>();
            }
            LMS.Data.User user = UserRepository.GetUser(User.Identity.Name);
            if (user.Security_Level_ID == 1)
            {
                Dictionary<int, string> userdata = new Dictionary<int, string>();
                userdata.Add(user.User_ID, user.Last_Name + ", " + user.First_Name);

                ViewData["User_ID"] = new SelectList(userdata, "key", "value");
            }

            else
            {
                ViewData["User_ID"] = new SelectList(_dropRepository.GetAgents(false), "User_ID", "FullName");
            }

            ViewData["LeadCount"] = leadcount;
            return View();
        }

        public ActionResult SourceCodes()
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

            return View(db.Source_Codes);
        }

        public ActionResult SourceCode(int id)
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

            Source_Code code = db.Source_Codes.Single(sc => sc.Source_Code_ID==id);

            ViewData["Group_ID"] = new SelectList(_dropRepository.GetGroups(false),"Group_ID","Description",code.Group_ID);
            ViewData["Affinity_Partner_ID"] = new SelectList(_dropRepository.GetAffinityPartners(false), "Affinity_Partner_ID", "Name", code.Group_ID);
            ViewData["Marketing_Group_ID"] = new SelectList(_dropRepository.GetMarketingGroups(false), "Marketing_Group_ID", "Description", code.Marketing_Group_ID);
            ViewData["Media_Type_ID"] = new SelectList(_dropRepository.GetMediaTypes(false), "Media_Type_ID", "Media_Type_Description", code.Media_Type_ID);

            return View(code);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SourceCode(int id, Source_Code modelCode)
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            Source_Code code = db.Source_Codes.Single(sc => sc.Source_Code_ID == id);
            UpdateModel(code);
            db.SubmitChanges();

            return RedirectToAction("SourceCodes");
        }

        public ActionResult Duplicate(int id)
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            Record record = _recordRepository.GetRecord(id);
            ViewData["Record"] = record;

            var dups = from r in db.Records
                       where ((r.First_Name.Equals(record.First_Name) && r.Last_Name.Equals(record.Last_Name))
                        || ( r.Last_Name.Equals(record.Last_Name) && r.DOB.Equals(record.DOB) )
                        || (r.Last_Name.Equals(record.Last_Name) && (r.Home_Phone.Equals(record.Home_Phone) || r.Work_Phone.Equals(record.Home_Phone) || r.Cell_Phone.Equals(record.Home_Phone) ))
                        || (r.Last_Name.Equals(record.Last_Name) && (r.Email_Address.Equals(record.Email_Address) || r.Alt_Email_Address.Equals(record.Email_Address) || r.Email_Address_2.Equals(record.Email_Address)))
                        )&& r.Customer_ID!= record.Customer_ID
                       select r;

            return View(dups.ToList());
        }

        public ActionResult LeadDecay()
        {
            //_reportRepository.RunReport(new DateTime(2009, 6, 15), new DateTime(2009, 6, 23), 0, 0, "", "", "");
           
            ViewData["Source_Code_ID"] = new SelectList(_dropRepository.GetSourceCodes(false),"Source_Code_ID","DropDownName");
            ViewData["Group_ID"] = new SelectList(_dropRepository.GetGroups(true),"Group_ID","Description");
            ViewData["Agent_ID"] = new SelectList(_dropRepository.GetAgents(true),"User_ID","FullName");
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LeadDecay(FormCollection collection)
        {
            Dictionary<string, LeadDecayResult> report = new Dictionary<string, LeadDecayResult>();
            int Total = 0;
            report.Add("5min", new LeadDecayResult());
            report.Add("15min", new LeadDecayResult());
            report.Add("30min", new LeadDecayResult());
            report.Add("1hr", new LeadDecayResult());
            report.Add("3hr", new LeadDecayResult());
            report.Add("6hr", new LeadDecayResult());
            report.Add("12hr", new LeadDecayResult());
            report.Add("24hr", new LeadDecayResult());
            report.Add("48hr", new LeadDecayResult());
            report.Add("96hr", new LeadDecayResult());
            report.Add("1wk", new LeadDecayResult());
            report.Add("1wk+", new LeadDecayResult());
            
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

            string query = string.Format("exec Report_Lead_Decay '{0}','{1}','{2}',{3},{4}", Convert.ToDateTime(collection["StartDate"]), Convert.ToDateTime(collection["EndDate"]), collection["Source_Code_ID"], Convert.ToInt32(collection["Group_ID"]), Convert.ToInt32(collection["Agent_ID"]));

            List<LeadDecay> ReportData = db.ExecuteQuery<LeadDecay>(query).ToList();

            query = string.Format("exec Report_Lead_Decay_Count '{0}','{1}','{2}',{3},{4}", Convert.ToDateTime(collection["StartDate"]), Convert.ToDateTime(collection["EndDate"]), collection["Source_Code_ID"], Convert.ToInt32(collection["Group_ID"]), Convert.ToInt32(collection["Agent_ID"]));

            int LeadCount = db.ExecuteQuery<int>(query).First();

            //var Report = db.Report_Lead_Decay(Convert.ToDateTime(collection["StartDate"]), Convert.ToDateTime(collection["EndDate"]), collection["Source_Code_ID"], Convert.ToInt32(collection["Group_ID"]), Convert.ToInt32(collection["Agent_ID"]));

            ViewData["Source_Code_ID"] = new SelectList(_dropRepository.GetSourceCodes(false), "Source_Code_ID", "DropDownName");
            ViewData["Group_ID"] = new SelectList(_dropRepository.GetGroups(true), "Group_ID", "Description");
            ViewData["Agent_ID"] = new SelectList(_dropRepository.GetAgents(true), "User_ID", "FullName");

            Total = LeadCount;

            foreach (LeadDecay ld in ReportData)
            {
                if (ld.MinutesToContact <= 5)
                {
                    report["5min"].Number++;
                    report["5min"].AppsSent += ld.Apps_Sent;
                    report["5min"].AppsRec += ld.Apps_Received;
                    report["5min"].AppsPlaced += ld.Apps_Submitted;

                }
                else if(ld.MinutesToContact<=15)
                {
                    report["15min"].Number++;
                    report["15min"].AppsSent += ld.Apps_Sent;
                    report["15min"].AppsRec += ld.Apps_Received;
                    report["15min"].AppsPlaced += ld.Apps_Submitted;
                }
                else if (ld.MinutesToContact <= 30)
                {
                    report["30min"].Number++;
                    report["30min"].AppsSent += ld.Apps_Sent;
                    report["30min"].AppsRec += ld.Apps_Received;
                    report["30min"].AppsPlaced += ld.Apps_Submitted;
                }
                else if (ld.MinutesToContact <= 60)
                {
                    report["1hr"].Number++;
                    report["1hr"].AppsSent += ld.Apps_Sent;
                    report["1hr"].AppsRec += ld.Apps_Received;
                    report["1hr"].AppsPlaced += ld.Apps_Submitted;
                }
                else if (ld.MinutesToContact <= 180)
                {
                    report["3hr"].Number++;
                    report["3hr"].AppsSent += ld.Apps_Sent;
                    report["3hr"].AppsRec += ld.Apps_Received;
                    report["3hr"].AppsPlaced += ld.Apps_Submitted;
                }
                else if (ld.MinutesToContact <= 360)
                {
                    report["6hr"].Number++;
                    report["6hr"].AppsSent += ld.Apps_Sent;
                    report["6hr"].AppsRec += ld.Apps_Received;
                    report["6hr"].AppsPlaced += ld.Apps_Submitted;
                }
                else if (ld.MinutesToContact <= 720)
                {
                    report["12hr"].Number++;
                    report["12hr"].AppsSent += ld.Apps_Sent;
                    report["12hr"].AppsRec += ld.Apps_Received;
                    report["12hr"].AppsPlaced += ld.Apps_Submitted;
                }
                else if (ld.MinutesToContact <= 1440)
                {
                    report["24hr"].Number++;
                    report["24hr"].AppsSent += ld.Apps_Sent;
                    report["24hr"].AppsRec += ld.Apps_Received;
                    report["24hr"].AppsPlaced += ld.Apps_Submitted;
                }
                else if (ld.MinutesToContact <= 2880)
                {
                    report["48hr"].Number++;
                    report["48hr"].AppsSent += ld.Apps_Sent;
                    report["48hr"].AppsRec += ld.Apps_Received;
                    report["48hr"].AppsPlaced += ld.Apps_Submitted;
                }
                else if (ld.MinutesToContact <= 5760)
                {
                    report["96hr"].Number++;
                    report["96hr"].AppsSent += ld.Apps_Sent;
                    report["96hr"].AppsRec += ld.Apps_Received;
                    report["96hr"].AppsPlaced += ld.Apps_Submitted;
                }
                else if (ld.MinutesToContact <= 10080)
                {
                    report["1wk"].Number++;
                    report["1wk"].AppsSent += ld.Apps_Sent;
                    report["1wk"].AppsRec += ld.Apps_Received;
                    report["1wk"].AppsPlaced += ld.Apps_Submitted;
                }
                else 
                {
                    report["1wk+"].Number++;
                    report["1wk+"].AppsSent += ld.Apps_Sent;
                    report["1wk+"].AppsRec += ld.Apps_Received;
                    report["1wk+"].AppsPlaced += ld.Apps_Submitted;
                }
            }

            ViewData["NumberOfLeads"] = LeadCount;
            ViewData["Report"] = report;

            if (collection["Export"].StartsWith("true"))
            {
                //return this.LeadDecayActionResult(report, LeadCount, "LeadDeacy.xls");

                query = string.Format("exec Report_Lead_Decay_Long '{0}','{1}','{2}',{3},{4}", Convert.ToDateTime(collection["StartDate"]), Convert.ToDateTime(collection["EndDate"]), collection["Source_Code_ID"], Convert.ToInt32(collection["Group_ID"]), Convert.ToInt32(collection["Agent_ID"]));

                IEnumerable<LeadDecayDetail> RData = db.ExecuteQuery<LeadDecayDetail>(query);

                Hashtable table = new Hashtable();

                table.Add("StartDate", Convert.ToDateTime(collection["StartDate"]));
                table.Add("EndDate", Convert.ToDateTime(collection["EndDate"]));
                table.Add("Source_Code_ID", collection["Source_Code_ID"]);
                table.Add("Group_ID", Convert.ToInt32(collection["Group_ID"]));
                table.Add("Agent_ID", Convert.ToInt32(collection["Agent_ID"]));

                return this.LeadDecayActionResult(RData, table, "LeadDecayReport.xls");
                //return View();
            }
            else
            {
                return View();
            }
        }

        ///// <summary>
        ///// Generates Excel document using headers grabbed from property names
        ///// </summary>
        //public ActionResult GenerateExcel()
        //{
        //    LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
        //    return this.LeadDecayActionResult(report, "data.xls");
        //}

        public ActionResult AdvancedLeadDecay()
        {
            _ReportData = new Dictionary<string, Section>();

            _ReportData.Add("5min", new Section());
            _ReportData.Add("15min", new Section());
            _ReportData.Add("30min", new Section());
            _ReportData.Add("1hr", new Section());
            _ReportData.Add("3hrs", new Section());
            _ReportData.Add("6hrs", new Section());
            _ReportData.Add("12hrs", new Section());
            _ReportData.Add("24hrs", new Section());
            _ReportData.Add("48hrs", new Section());
            _ReportData.Add("96hrs", new Section());
            _ReportData.Add("1wk", new Section());
            _ReportData.Add("1wk+", new Section()); 
            
            LMSDataContext _db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            List<Report_Advanced_Lead_DecayResult> _Data = _db.Report_Advanced_Lead_Decay(new DateTime(2009, 6, 15), new DateTime(2009, 7, 1)).ToList();

            int TotalCount = _Data.Count();
            _ReportData["5min"].LeadTotal = TotalCount;
            _ReportData["15min"].LeadTotal = TotalCount;
            _ReportData["30min"].LeadTotal = TotalCount;
            _ReportData["1hr"].LeadTotal = TotalCount;
            _ReportData["3hrs"].LeadTotal = TotalCount;
            _ReportData["6hrs"].LeadTotal = TotalCount;
            _ReportData["12hrs"].LeadTotal = TotalCount;
            _ReportData["24hrs"].LeadTotal = TotalCount;
            _ReportData["48hrs"].LeadTotal = TotalCount;
            _ReportData["96hrs"].LeadTotal = TotalCount;
            _ReportData["1wk"].LeadTotal = TotalCount;
            _ReportData["1wk+"].LeadTotal = TotalCount;


            foreach (Report_Advanced_Lead_DecayResult result in _Data)
            {
                int MinutesToContact = 0;
                int MinutesToAttempt = 0;


                if (result.InititalContact.HasValue)
                {
                    TimeSpan span = result.InititalContact.Value.Subtract(result.Assigned_Date.Value);
                    MinutesToContact = Convert.ToInt32(span.TotalMinutes);
                }

                if (result.InititalAttempt.HasValue)
                {
                    TimeSpan span = result.InititalAttempt.Value.Subtract(result.Assigned_Date.Value);
                    MinutesToAttempt = Convert.ToInt32(span.TotalMinutes);
                }

                AssignAttemptValue(MinutesToAttempt);
                AssignContactValue(MinutesToContact, result);
            }

            return this.AdvancedLeadDeacyResult(_ReportData,"LeadDecay.csv");
        }

        public ActionResult LeadTimeReport()
        {
            List<string> times = new List<string>();
            times.Add("12:00 AM");
            times.Add("1:00 AM");
            times.Add("2:00 AM");
            times.Add("3:00 AM");
            times.Add("4:00 AM");
            times.Add("5:00 AM");
            times.Add("6:00 AM");
            times.Add("7:00 AM");
            times.Add("8:00 AM");
            times.Add("9:00 AM");
            times.Add("10:00 AM");
            times.Add("11:00 AM");
            times.Add("12:00 PM");
            times.Add("1:00 PM");
            times.Add("2:00 PM");
            times.Add("3:00 PM");
            times.Add("4:00 PM");
            times.Add("5:00 PM");
            times.Add("6:00 PM");
            times.Add("7:00 PM");
            times.Add("8:00 PM");
            times.Add("9:00 PM");
            times.Add("10:00 PM");
            times.Add("11:00 PM");

            ViewData["Times"] = new SelectList(times);
            ViewData["Source_Code_ID"] = new SelectList(_dropRepository.GetSourceCodes(false), "Source_Code_ID", "DropDownName");
            ViewData["Group_ID"] = new SelectList(_dropRepository.GetGroups(true), "Group_ID", "Description");
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LeadTimeReport(FormCollection collection)
        {
            DateTime startdate = new DateTime();
            DateTime enddate = new DateTime();

            //Get Report Data
            if (DateTime.TryParse(collection["StartDate"], out startdate) && DateTime.TryParse(collection["EndDate"], out enddate))
            {
                LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
                IEnumerable<Report_Lead_Time_ReportResult> report = db.Report_Lead_Time_Report(startdate, enddate).ToList();

                var query = report;

                if (collection["Source_Code_ID"].Length > 0)
                {
                    List<int> ids = new List<int>();
                    foreach (string s in collection["Source_Code_ID"].Split(','))
                    {
                        int id = Convert.ToInt32(s);
                        ids.Add(id);
                    }

                    var myReport = from p in report
                                     where ids.Contains(p.Source_Code_ID)
                                     select p;

                    report = myReport.ToList();
                }

                ViewData["ReportData"] = report;
            }

            //Set UI Components
            List<string> times = new List<string>();
            times.Add("12:00 AM");
            times.Add("1:00 AM");
            times.Add("2:00 AM");
            times.Add("3:00 AM");
            times.Add("4:00 AM");
            times.Add("5:00 AM");
            times.Add("6:00 AM");
            times.Add("7:00 AM");
            times.Add("8:00 AM");
            times.Add("9:00 AM");
            times.Add("10:00 AM");
            times.Add("11:00 AM");
            times.Add("12:00 PM");
            times.Add("1:00 PM");
            times.Add("2:00 PM");
            times.Add("3:00 PM");
            times.Add("4:00 PM");
            times.Add("5:00 PM");
            times.Add("6:00 PM");
            times.Add("7:00 PM");
            times.Add("8:00 PM");
            times.Add("9:00 PM");
            times.Add("10:00 PM");
            times.Add("11:00 PM");

            ViewData["Times"] = new SelectList(times);
            ViewData["Source_Code_ID"] = new SelectList(_dropRepository.GetSourceCodes(false), "Source_Code_ID", "DropDownName");
            ViewData["Group_ID"] = new SelectList(_dropRepository.GetGroups(true), "Group_ID", "Description");

            //Return View
            return View();
        }
        private void AssignAttemptValue(int MinutesToAttempt)
        {
            if (MinutesToAttempt < 5)
            {
                _ReportData["5min"].NumberAttempted++;
            }
            else if (MinutesToAttempt < 15)
            {
                _ReportData["15min"].NumberAttempted++;
            }
            else if (MinutesToAttempt < 30)
            {
                _ReportData["30min"].NumberAttempted++;
            }
            else if (MinutesToAttempt < 60)
            {
                _ReportData["1hr"].NumberAttempted++;
            }
            else if (MinutesToAttempt < 180)
            {
                _ReportData["3hrs"].NumberAttempted++;
            }
            else if (MinutesToAttempt < 360)
            {
                _ReportData["6hrs"].NumberAttempted++;
            }
            else if (MinutesToAttempt < 720)
            {
                _ReportData["12hrs"].NumberAttempted++;
            }
            else if (MinutesToAttempt < 1440)
            {
                _ReportData["24hrs"].NumberAttempted++;
            }
            else if (MinutesToAttempt < 2880)
            {
                _ReportData["48hrs"].NumberAttempted++;
            }
            else if (MinutesToAttempt < 5760)
            {
                _ReportData["96hrs"].NumberAttempted++;
            }
            else if (MinutesToAttempt < 10080)
            {
                _ReportData["1wk"].NumberAttempted++;
            }
            else if (MinutesToAttempt >= 10080)
            {
                _ReportData["1wk+"].NumberAttempted++;
            }
        }

        private void AssignContactValue(int MinutesToContact, Report_Advanced_Lead_DecayResult result)
        {
            if (MinutesToContact < 5)
            {
                _ReportData["5min"].NumberContact++;
                _ReportData["5min"].AppsSent += result.Apps_Sent.Value;
                _ReportData["5min"].AppsRecieved += result.Apps_Received.Value;
                _ReportData["5min"].AppsPlaced += result.Apps_Placed.Value;

            }
            else if (MinutesToContact < 15)
            {
                _ReportData["15min"].NumberContact++;
                _ReportData["15min"].AppsSent += result.Apps_Sent.Value;
                _ReportData["15min"].AppsRecieved += result.Apps_Received.Value;
                _ReportData["15min"].AppsPlaced += result.Apps_Placed.Value;
            }
            else if (MinutesToContact < 30)
            {
                _ReportData["30min"].NumberContact++;
                _ReportData["30min"].AppsSent += result.Apps_Sent.Value;
                _ReportData["30min"].AppsRecieved += result.Apps_Received.Value;
                _ReportData["30min"].AppsPlaced += result.Apps_Placed.Value;
            }
            else if (MinutesToContact < 60)
            {
                _ReportData["1hr"].NumberContact++;
                _ReportData["1hr"].AppsSent += result.Apps_Sent.Value;
                _ReportData["1hr"].AppsRecieved += result.Apps_Received.Value;
                _ReportData["1hr"].AppsPlaced += result.Apps_Placed.Value;
            }
            else if (MinutesToContact < 180)
            {
                _ReportData["3hrs"].NumberContact++;
                _ReportData["3hrs"].AppsSent += result.Apps_Sent.Value;
                _ReportData["3hrs"].AppsRecieved += result.Apps_Received.Value;
                _ReportData["3hrs"].AppsPlaced += result.Apps_Placed.Value;
            }
            else if (MinutesToContact < 360)
            {
                _ReportData["6hrs"].NumberContact++;
                _ReportData["6hrs"].AppsSent += result.Apps_Sent.Value;
                _ReportData["6hrs"].AppsRecieved += result.Apps_Received.Value;
                _ReportData["6hrs"].AppsPlaced += result.Apps_Placed.Value;
            }
            else if (MinutesToContact < 720)
            {
                _ReportData["12hrs"].NumberContact++;
                _ReportData["12hrs"].AppsSent += result.Apps_Sent.Value;
                _ReportData["12hrs"].AppsRecieved += result.Apps_Received.Value;
                _ReportData["12hrs"].AppsPlaced += result.Apps_Placed.Value;
            }
            else if (MinutesToContact < 1440)
            {
                _ReportData["24hrs"].NumberContact++;
                _ReportData["24hrs"].AppsSent += result.Apps_Sent.Value;
                _ReportData["24hrs"].AppsRecieved += result.Apps_Received.Value;
                _ReportData["24hrs"].AppsPlaced += result.Apps_Placed.Value;
            }
            else if (MinutesToContact < 2880)
            {
                _ReportData["48hrs"].NumberContact++;
                _ReportData["48hrs"].AppsSent += result.Apps_Sent.Value;
                _ReportData["48hrs"].AppsRecieved += result.Apps_Received.Value;
                _ReportData["48hrs"].AppsPlaced += result.Apps_Placed.Value;
            }
            else if (MinutesToContact < 5760)
            {
                _ReportData["96hrs"].NumberContact++;
                _ReportData["96hrs"].AppsSent += result.Apps_Sent.Value;
                _ReportData["96hrs"].AppsRecieved += result.Apps_Received.Value;
                _ReportData["96hrs"].AppsPlaced += result.Apps_Placed.Value;
            }
            else if (MinutesToContact < 10080)
            {
                _ReportData["1wk"].NumberContact++;
                _ReportData["1wk"].AppsSent += result.Apps_Sent.Value;
                _ReportData["1wk"].AppsRecieved += result.Apps_Received.Value;
                _ReportData["1wk"].AppsPlaced += result.Apps_Placed.Value;
            }
            else if (MinutesToContact >= 10080)
            {
                _ReportData["1wk+"].NumberContact++;
                _ReportData["1wk+"].AppsSent += result.Apps_Sent.Value;
                _ReportData["1wk+"].AppsRecieved += result.Apps_Received.Value;
                _ReportData["1wk+"].AppsPlaced += result.Apps_Placed.Value;
            }
        }


        
    }
}