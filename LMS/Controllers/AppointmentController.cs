using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Data;
using System.Net.Mail;
using System.Net;
using LMS.Data.Repository;
using LMS.Data;
using LMS.Helpers;
using System.Text;
using LMS.Models;
using System.Web.Mvc.Html;

namespace LMS.Controllers
{
    [Authorize]
    [HandleError]
    [SessionExpireFilterAttribute]

    public class AppointmentController : Controller
    {

        private IDropDownRepository _dropRepository;
        private IUserRepository _userRepository;
        private IAgentRepository _agentRepository;
        private IAppointmentRepository _appointmentRepository;

        public AppointmentController()
            : this(new DropDownRepository(), new UserRepository(), new AgentRepository(), new AppointmentRepository())
        {

        }

        public AppointmentController(IDropDownRepository dropRepository, IUserRepository userRepository, IAgentRepository agentRepository, IAppointmentRepository appointmentRepository)
        {
            _dropRepository = dropRepository;
            _userRepository = userRepository;
            _agentRepository = agentRepository;
            _appointmentRepository = appointmentRepository;
        }
        //        
        // GET: /Appointment/


        public ActionResult List(int? id)
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            //int agentid = db.sp_Agent_List().ToList()[1].Agent_ID;

            //MikeMc added 8/23/2013 - allow users to view there own appointments and not edit
            LMS.Data.User user = LMS.Data.Repository.UserRepository.GetUser(User.Identity.Name);
            if (LMS.Data.Repository.UserRepository.HasSecurityLevel(user, 1))//AGENT
            {
                Agent agent = _agentRepository.GetAgentByUserID(user.User_ID);
                id = agent.Agent_ID;
                //id = agent.Agent_ID = 32; //TESTING VALUE
            }
            if (id == null)
            {

                ViewData["Agent_List"] = new SelectList(_dropRepository.GetAgentAgencyAndAll(false), "Agent_ID", "Name", 18);
                ViewData["AppointmentListByAgent"] = db.sp_Appointment_List_By_Agent(18).ToList();
            }
            else
            {
                ViewData["Agent_List"] = new SelectList(_dropRepository.GetAgentAgencyAndAll(false), "Agent_ID", "Name", id);
                ViewData["AppointmentListByAgent"] = db.sp_Appointment_List_By_Agent(id).ToList();
            }

            ViewData["State_List"] = new SelectList(_dropRepository.GetStatesAndAll(false), "State_ID", "Abbreviation", 3);
            ViewData["Carrier_List"] = new SelectList(_dropRepository.GetStateAuthorizedCarriersAndAll(false), "Carrier_ID", "Name", 271);
            ViewData["AppointmentListByStateCarrier"] = db.sp_Appointment_List_By_State_Carrier(3, 271).ToList();
            return View(user);
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult List(FormCollection collection)
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            ViewData["State_List"] = new SelectList(_dropRepository.GetStatesAndAll(false), "State_ID", "Abbreviation", Int32.Parse(collection["State"]));
            ViewData["Agent_List"] = new SelectList(_dropRepository.GetAgentAgencyAndAll(false), "Agent_ID", "Name", Int32.Parse(collection["Agent"]));
            ViewData["AppointmentListByAgent"] = db.sp_Appointment_List_By_Agent(Int32.Parse(collection["Agent"])).ToList();
            ViewData["Carrier_List"] = new SelectList(_dropRepository.GetStateAuthorizedCarriersAndAll(false), "Carrier_ID", "Name", Int32.Parse(collection["Carrier"]));
            ViewData["AppointmentListByStateCarrier"] = db.sp_Appointment_List_By_State_Carrier(Int32.Parse(collection["State"]), Int32.Parse(collection["Carrier"])).ToList();
            return View();
        }

        public ActionResult New(int? id)
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            Appointment appointment = new Appointment();
            if (id != null)
            {
                ViewData["Agent_List"] = new SelectList(db.sp_Get_Active_Agents().ToList(), "Agent_ID", "Full_Name", (int)id);
                appointment.Agent_ID = (int)id;
            }
            else
            {
                ViewData["Agent_List"] = new SelectList(db.sp_Get_Active_Agents().ToList(), "Agent_ID", "Full_Name");
            }

            ViewData["Line_Of_Insurance_List"] = new SelectList(_dropRepository.GetInsuranceLine(true), "Line_Of_Insurance_ID", "Description", -1);
            ViewData["Carrier_List"] = new SelectList(_dropRepository.GetStateAuthorizedCarriers(true), "Carrier_ID", "Name", -1);
            return View(appointment);

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult New(FormCollection collection)
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            Appointment appointment = new Appointment();
            DateTime dt;
            if (!String.IsNullOrEmpty(collection["btnAdd"]))
            {
                if (collection["Carrier"] == "-1")
                {
                    ModelState.AddModelError("Carrier", "Carrier Cannot Be Blank");
                    ModelState.SetModelValue("Carrier", new ValueProviderResult(ValueProvider.GetValue("Carrier").AttemptedValue, collection["Carrier"], System.Globalization.CultureInfo.CurrentCulture));
                }
                if (collection["Appointment_Date"] != "" && !DateTime.TryParse(collection["Appointment_Date"], out dt))
                {
                    ModelState.AddModelError("Appointment_Date", "Appointment date not in good format.");
                    ModelState.SetModelValue("Appointment_Date", new ValueProviderResult(ValueProvider.GetValue("Appointment_Date").AttemptedValue, collection["Appointment_Date"], System.Globalization.CultureInfo.CurrentCulture));
                }
                if (collection["Terminate_Date"] != "" && !DateTime.TryParse(collection["Terminate_Date"], out dt))
                {
                    ModelState.AddModelError("Terminate_Date", "Terminated date not in good format.");
                    ModelState.SetModelValue("Terminate_Date", new ValueProviderResult(ValueProvider.GetValue("Terminate_Date").AttemptedValue, collection["Terminate_Date"], System.Globalization.CultureInfo.CurrentCulture));
                }
                if (collection["Line_Of_Insurance"] == "-1")
                {
                    ModelState.AddModelError("Line_Of_Insurance", "Product Cannot Be Blank");
                    ModelState.SetModelValue("Line_Of_Insurance", new ValueProviderResult(ValueProvider.GetValue("Line_Of_Insurance").AttemptedValue, collection["Line_Of_Insurance"], System.Globalization.CultureInfo.CurrentCulture));
                }
                if (collection["Agent_Number"] == "")
                {
                    ModelState.AddModelError("Agent_Number", "Agent Number Cannot Be Blank");
                    ModelState.SetModelValue("Agent_Number", new ValueProviderResult(ValueProvider.GetValue("Agent_Number").AttemptedValue, collection["Agent_Number"], System.Globalization.CultureInfo.CurrentCulture));
                }
            }
            if (ModelState.IsValid && !String.IsNullOrEmpty(collection["btnAdd"]))
            {
                try
                {
                    //insert appointment
                    appointment.Agent_Number = StringHelper.EncryptString(Convert.ToString(collection["Agent_Number"]));
                    DateTime ad;
                    if (DateTime.TryParse(collection["Appointment_Date"], out ad))
                    {
                        appointment.Appointment_Date = ad;
                    }
                    else
                    {
                        appointment.Appointment_Date = null;
                    }
                    DateTime td;
                    if (DateTime.TryParse(collection["Terminate_Date"], out td))
                    {
                        appointment.Terminate_Date = td;
                    }
                    else
                    {
                        appointment.Terminate_Date = null;
                    }
                    appointment.Carrier_ID = Int32.Parse(collection["Carrier"]);
                    appointment.Agent_ID = Int32.Parse(collection["Agent"]);
                    appointment.Line_Of_Insurance_ID = Int32.Parse(collection["Line_Of_Insurance"]);
                    appointment.Updated_By = _userRepository.GetUserByUserName(User.Identity.Name).User_ID;
                    appointment.Last_Updated_Date = DateTime.Now;
                    db.Appointments.InsertOnSubmit(appointment);
                    db.SubmitChanges();
                    int appointmentid = appointment.Appointment_ID;
                    //insert app states
                    List<LMS.Data.sp_Get_Authorized_States_By_Carrier_IDResult> rs = db.sp_Get_Authorized_States_By_Carrier_ID(Int32.Parse(collection["Carrier"])).ToList();
                    Appointment_State appointment_state;
                    foreach (sp_Get_Authorized_States_By_Carrier_IDResult item in rs)
                    {
                        if (collection["ck" + item.State_ID].Contains("true"))
                        {
                            appointment_state = new Appointment_State();
                            appointment_state.State_ID = item.State_ID;
                            appointment_state.Appointment_ID = appointmentid;
                            appointment_state.Created_On = DateTime.Now;
                            db.Appointment_States.InsertOnSubmit(appointment_state);
                            db.SubmitChanges();
                        }
                    }
                    return RedirectToAction("Details", new { id = appointmentid });
                }
                catch (Exception e)
                {
                    LMS.ErrorHandler.HandleError.EmailError(e);
                }
            }

            if (DateTime.TryParse(collection["Appointment_Date"], out dt))
            {
                appointment.Appointment_Date = DateTime.Parse(collection["Appointment_Date"]);
            }
            if (DateTime.TryParse(collection["Terminate_Date"], out dt))
            {
                appointment.Terminate_Date = DateTime.Parse(collection["Terminate_Date"]);
            }
            appointment.Agent_Number = StringHelper.EncryptString(Convert.ToString(collection["Agent_Number"]));
            ViewData["Agent_List"] = new SelectList(db.sp_Get_Active_Agents().ToList(), "Agent_ID", "Full_Name", Int32.Parse(collection["Agent"]));
            ViewData["Line_Of_Insurance_List"] = new SelectList(_dropRepository.GetInsuranceLine(true), "Line_Of_Insurance_ID", "Description", Int32.Parse(collection["Line_Of_Insurance"]));
            ViewData["Carrier_List"] = new SelectList(_dropRepository.GetStateAuthorizedCarriers(true), "Carrier_ID", "Name", Int32.Parse(collection["Carrier"]));


            return View(appointment);
        }

        public ActionResult Edit(int id)
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            Appointment appointment = _appointmentRepository.GetAppointmentByID(id);
            List<sp_Agent_ListResult> agents = db.sp_Agent_List().ToList();
            ViewData["Agent_List"] = new SelectList(agents, "Agent_ID", "Name", appointment.Agent_ID);
            ViewData["Line_Of_Insurance_List"] = new SelectList(_dropRepository.GetInsuranceLine(false), "Line_Of_Insurance_ID", "Description", appointment.Line_Of_Insurance_ID);
            ViewData["Carrier_List"] = new SelectList(_dropRepository.GetStateAuthorizedCarriers(false), "Carrier_ID", "Name", appointment.Carrier_ID);
            return View(appointment);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(int id, FormCollection collection)
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            Appointment appointment = db.Appointments.Where(app => app.Appointment_ID == id).FirstOrDefault();
            DateTime dt;
            if (!String.IsNullOrEmpty(collection["btnSave"]))
            {

                if (collection["Appointment_Date"] != "" && !DateTime.TryParse(collection["Appointment_Date"], out dt))
                {
                    ModelState.AddModelError("Appointment_Date", "Appointment date not in good format.");
                    ModelState.SetModelValue("Appointment_Date", new ValueProviderResult(ValueProvider.GetValue("Appointment_Date").AttemptedValue, collection["Appointment_Date"], System.Globalization.CultureInfo.CurrentCulture));
                }
                if (collection["Terminate_Date"] != "" && !DateTime.TryParse(collection["Terminate_Date"], out dt))
                {
                    ModelState.AddModelError("Terminate_Date", "Terminated date not in good format.");
                    ModelState.SetModelValue("Terminate_Date", new ValueProviderResult(ValueProvider.GetValue("Terminate_Date").AttemptedValue, collection["Terminate_Date"], System.Globalization.CultureInfo.CurrentCulture));
                }
                if (collection["Agent_Number"] == "")
                {
                    ModelState.AddModelError("Agent_Number", "Agent Number Cannot Be Blank");
                    ModelState.SetModelValue("Agent_Number", new ValueProviderResult(ValueProvider.GetValue("Agent_Number").AttemptedValue, collection["Agent_Number"], System.Globalization.CultureInfo.CurrentCulture));
                }
            }
            if (ModelState.IsValid && !String.IsNullOrEmpty(collection["btnSave"]))
            {
                try
                {
                    //edit appointment
                    appointment.Agent_Number = StringHelper.EncryptString(Convert.ToString(collection["Agent_Number"]));
                    DateTime ad;
                    if (DateTime.TryParse(collection["Appointment_Date"], out ad))
                    {
                        appointment.Appointment_Date = ad;
                    }
                    else
                    {
                        appointment.Appointment_Date = null;
                    }
                    DateTime td;
                    if (DateTime.TryParse(collection["Terminate_Date"], out td))
                    {
                        appointment.Terminate_Date = td;
                    }
                    else
                    {
                        appointment.Terminate_Date = null;
                    }
                    appointment.Carrier_ID = Int32.Parse(collection["Carrier1"]);
                    appointment.Agent_ID = Int32.Parse(collection["Agent1"]);
                    appointment.Line_Of_Insurance_ID = Int32.Parse(collection["Line_Of_Insurance"]);
                    appointment.Updated_By = _userRepository.GetUserByUserName(User.Identity.Name).User_ID;
                    appointment.Last_Updated_Date = DateTime.Now;
                    db.SubmitChanges();
                    //edit app states
                    List<LMS.Data.State> rs = db.States.ToList();
                    Appointment_State appointment_state;
                    foreach (State item in rs)
                    {
                        appointment_state = db.Appointment_States.Where(appstate => appstate.State_ID == item.State_ID && appstate.Appointment_ID == id).FirstOrDefault();
                        if (!String.IsNullOrEmpty(collection["ck" + item.State_ID]) && collection["ck" + item.State_ID].Contains("true"))
                        {
                            if (appointment_state == null)
                            {
                                appointment_state = new Appointment_State();
                                appointment_state.Appointment_ID = id;
                                appointment_state.State_ID = item.State_ID;
                                appointment_state.Created_On = DateTime.Now;
                                db.Appointment_States.InsertOnSubmit(appointment_state);
                                db.SubmitChanges();
                            }
                        }
                        else
                        {
                            if (appointment_state != null)
                            {
                                db.Appointment_States.DeleteOnSubmit(appointment_state);
                                db.SubmitChanges();
                            }
                        }
                    }
                    return RedirectToAction("Details", new { id = id });
                }
                catch (Exception e)
                {
                    LMS.ErrorHandler.HandleError.EmailError(e);
                }
            }

            if (DateTime.TryParse(collection["Appointment_Date"], out dt))
            {
                appointment.Appointment_Date = DateTime.Parse(collection["Appointment_Date"]);
            }
            else
            {
                appointment.Appointment_Date = null;
            }
            if (DateTime.TryParse(collection["Terminate_Date"], out dt))
            {
                appointment.Terminate_Date = DateTime.Parse(collection["Terminate_Date"]);
            }
            else
            {
                appointment.Terminate_Date = null;
            }
            appointment.Agent_Number = StringHelper.EncryptString(Convert.ToString(collection["Agent_Number"]));
            ViewData["Agent_List"] = new SelectList(db.sp_Get_Active_Agents().ToList(), "Agent_ID", "Full_Name", Int32.Parse(collection["Agent1"]));
            ViewData["Line_Of_Insurance_List"] = new SelectList(_dropRepository.GetInsuranceLine(false), "Line_Of_Insurance_ID", "Description", Int32.Parse(collection["Line_Of_Insurance"]));
            ViewData["Carrier_List"] = new SelectList(_dropRepository.GetStateAuthorizedCarriers(false), "Carrier_ID", "Name", Int32.Parse(collection["Carrier1"]));


            return View(appointment);
        }

        public ActionResult Details(int? id)
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            Appointment appointment = _appointmentRepository.GetAppointmentByID((int)id);
            Agent agent = _agentRepository.GetAgentByID(appointment.Agent_ID);
            if (agent.Agent_Type_ID == 1)
            {
                ViewData["AgentName"] = _userRepository.GetUser(agent.User_ID).First_Name + " " + _userRepository.GetUser(agent.User_ID).Last_Name;
            }
            else
            {
                ViewData["AgentName"] = agent.Agency_Name;
            }
            if (appointment.Updated_By != null && appointment.Updated_By != -1)
            {
                ViewData["UserName"] = _userRepository.GetUser((int)appointment.Updated_By).First_Name + " " + _userRepository.GetUser((int)appointment.Updated_By).Last_Name;
            }
            else
            {
                ViewData["UserName"] = "";
            }
            ViewData["StateList"] = db.sp_Get_Appointment_States_By_Appointment_ID(appointment.Appointment_ID).ToList()[0].State_List;

            ViewData["RequestStateList"] = new SelectList(db.sp_Get_Appointment_Request_State_List((int)id), "Abbreviation", "Abbreviation");
            return View(appointment);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Details(int id, FormCollection collection)
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            Appointment appointment = _appointmentRepository.GetAppointmentByID((int)id);
            Agent agent = _agentRepository.GetAgentByID(appointment.Agent_ID);
            string agent_name;
            if (agent.Agent_Type_ID == 1)
            {
                ViewData["AgentName"] = _userRepository.GetUser(agent.User_ID).First_Name + " " + _userRepository.GetUser(agent.User_ID).Last_Name;
                agent_name = _userRepository.GetUser(agent.User_ID).First_Name + " " + _userRepository.GetUser(agent.User_ID).Last_Name;
            }
            else
            {
                ViewData["AgentName"] = agent.Agency_Name;
                agent_name = agent.Agency_Name;
            }
            if (appointment.Updated_By != null && appointment.Updated_By != -1)
            {
                ViewData["UserName"] = _userRepository.GetUser((int)appointment.Updated_By).First_Name + " " + _userRepository.GetUser((int)appointment.Updated_By).Last_Name;
            }
            else
            {
                ViewData["UserName"] = "";
            }
            ViewData["StateList"] = db.sp_Get_Appointment_States_By_Appointment_ID(appointment.Appointment_ID).ToList()[0].State_List;

            ViewData["RequestStateList"] = new SelectList(db.sp_Get_Appointment_Request_State_List((int)id), "Abbreviation", "Abbreviation");
            if (!String.IsNullOrEmpty(collection["RequestState"]))
            {
                string carrier = appointment.Carrier.Name;
                string useremail = UserRepository.GetUser(User.Identity.Name).Email_Address;
                string url;
                if (db.Connection.DataSource.ToLower() != "lasql2")
                {
                    url = "http://test.lms.com";
                }
                else
                {
                    url = "http://lms.com";
                }
                MailMessage msg = new MailMessage();
                msg.To.Add("licensing@longevityalliance.com");
                //msg.To.Add("jmei@longevityalliance.com");
                msg.CC.Add(useremail);
                msg.From = new MailAddress(useremail);
                msg.Subject = "Request Appointment";
                string body = "Please appoint agent " + agent_name + " with " + carrier + " in the state of " + collection["RequestState"] + ".<br />";
                msg.Body = body + url + "/Appointment/Edit/" + appointment.Appointment_ID.ToString();
                msg.IsBodyHtml = true;
                SmtpClient client = new SmtpClient();
                client.Send(msg);
            }
            return View(appointment);
        }

        public ActionResult State()
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            return View(db.States);
        }

        public ActionResult SSN()
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            List<sp_Get_Active_AgentsResult> rs = db.sp_Get_Active_Agents().ToList();
            return View(rs);
        }

        public ActionResult Export(int AgentID, string Chk, int StateID, int CarrierID)
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            DataTable table;
            DataColumn column;
            DataRow row;
            DataSet dataset;
            table = new DataTable("AppointmentByAgent");
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Name";
            column.ReadOnly = true;
            table.Columns.Add(column);
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Carrier";
            column.ReadOnly = true;
            table.Columns.Add(column);
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "States";
            column.ReadOnly = true;
            table.Columns.Add(column);
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Agent#";
            column.ReadOnly = true;
            table.Columns.Add(column);
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "AppointmentDate";
            column.ReadOnly = true;
            table.Columns.Add(column);
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Product";
            column.ReadOnly = true;
            table.Columns.Add(column);

            if (String.IsNullOrEmpty(Chk) || (!String.IsNullOrEmpty(Chk) && !Chk.Contains("true")))
            {
                dataset = new DataSet();
                dataset.Tables.Add(table);
                List<sp_Appointment_List_By_AgentResult> rs = db.sp_Appointment_List_By_Agent(AgentID).ToList();
                for (int i = 0; i < rs.Count; i++)
                {
                    row = table.NewRow();
                    row["Name"] = rs[i].Name;
                    row["Carrier"] = rs[i].Carrier;
                    row["States"] = rs[i].State_List;
                    row["Agent#"] = rs[i].Agent_Number;
                    if (rs[i].Appointment_Date != null)
                    {
                        row["AppointmentDate"] = rs[i].Appointment_Date.Value.ToShortDateString();
                    }
                    else
                    {
                        row["AppointmentDate"] = "";
                    }
                    row["Product"] = rs[i].Product;
                    table.Rows.Add(row);
                }
                if (rs.Count > 0)
                {
                    RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
                    objExport.ExportDetails(table, RKLib.ExportData.Export.ExportFormat.Excel, "AppointmentByAgent.xls");
                }
                else
                {
                    return RedirectToAction("RecordOutput", "Certification");
                }

            }
            else
            {
                dataset = new DataSet();
                dataset.Tables.Add(table);
                List<sp_Appointment_List_By_State_CarrierResult> rs = db.sp_Appointment_List_By_State_Carrier(StateID, CarrierID).ToList();
                for (int i = 0; i < rs.Count; i++)
                {
                    row = table.NewRow();
                    row["Name"] = rs[i].Name;
                    row["Carrier"] = rs[i].Carrier;
                    row["States"] = rs[i].State_List;
                    row["Agent#"] = rs[i].Agent_Number;
                    if (rs[i].Appointment_Date != null)
                    {
                        row["AppointmentDate"] = rs[i].Appointment_Date.Value.ToShortDateString();
                    }
                    else
                    {
                        row["AppointmentDate"] = "";
                    }
                    row["Product"] = rs[i].Product;
                    table.Rows.Add(row);
                }
                if (rs.Count > 0)
                {
                    RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
                    objExport.ExportDetails(table, RKLib.ExportData.Export.ExportFormat.Excel, "AppointmentByStateCarrier.xls");
                }
                else
                {
                    return RedirectToAction("RecordOutput", "Certification");
                }
            }

            return View();
        }
    }
}
