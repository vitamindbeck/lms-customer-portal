using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Data;
using LMS.Data.Repository;
using LMS.Data;
using LMS.Helpers;

namespace LMS.Controllers
{
    [Authorize]
    [HandleError]
    [SessionExpireFilterAttribute]
    public class LicenseController : Controller
    {

        private IDropDownRepository _dropRepository;
        private IUserRepository _userRepository;
        private IAgentRepository _agentRepository;
        private ILicenseRepository _licenseRepository;

        public LicenseController()
            : this(new DropDownRepository(), new UserRepository(), new AgentRepository(), new LicenseRepository())
        {

        }

        public LicenseController(IDropDownRepository dropRepository, IUserRepository userRepository, IAgentRepository agentRepository, ILicenseRepository licenseRepository)
        {
            _dropRepository = dropRepository;
            _userRepository = userRepository;
            _agentRepository = agentRepository;
            _licenseRepository = licenseRepository;
        }

        public ActionResult List(int? id)
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
           // int agentid = db.sp_Agent_List().ToList()[1].Agent_ID;
            if (id == null)
            {
                ViewData["Agent_List"] = new SelectList(_dropRepository.GetAgentAgencyAndAll(false), "Agent_ID", "Name", 18);
                ViewData["LicenseListByAgent"] = db.sp_License_List_By_Agent(18).ToList();
            }
            else
            {
                ViewData["Agent_List"] = new SelectList(_dropRepository.GetAgentAgencyAndAll(false), "Agent_ID", "Name", id);
                ViewData["LicenseListByAgent"] = db.sp_License_List_By_Agent(id).ToList();
            }
            ViewData["State_List"] = new SelectList(_dropRepository.GetStates(false), "Abbreviation", "Abbreviation", "AZ");
            ViewData["LicenseListByState"] = db.sp_License_List_By_State("AZ").ToList();
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult List(FormCollection collection)
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            if (!Boolean.Parse(Request.Form.GetValues("chkStateWise")[0]))
            {
                ViewData["Agent_List"] = new SelectList(_dropRepository.GetAgentAgencyAndAll(false), "Agent_ID", "Name", Int32.Parse(collection["Agent"]));
                ViewData["LicenseListByAgent"] = db.sp_License_List_By_Agent(Int32.Parse(collection["Agent"])).ToList();
                ViewData["State_List"] = new SelectList(_dropRepository.GetStates(false), "Abbreviation", "Abbreviation", collection["State"]);
                ViewData["LicenseListByState"] = db.sp_License_List_By_State(collection["State"]).ToList();
            }
            else
            {
                ViewData["State_List"] = new SelectList(_dropRepository.GetStates(false), "Abbreviation", "Abbreviation", collection["State"]);
                ViewData["Agent_List"] = new SelectList(_dropRepository.GetAgentAgencyAndAll(false), "Agent_ID", "Name", Int32.Parse(collection["Agent"]));
                ViewData["LicenseListByAgent"] = db.sp_License_List_By_Agent(Int32.Parse(collection["Agent"])).ToList();
                ViewData["LicenseListByState"] = db.sp_License_List_By_State(collection["State"]).ToList();

            }
            
            return View();
        }

        public ActionResult Details(int? id)
        {
            License license = _licenseRepository.GetLicenseByID((int)id);
            Agent agent = _agentRepository.GetAgentByID(license.Agent_ID);
            if (agent.Agent_Type_ID == 1)
            {
                ViewData["AgentName"] = _userRepository.GetUser(agent.User_ID).First_Name + " " + _userRepository.GetUser(agent.User_ID).Last_Name;
            }
            else
            {
                ViewData["AgentName"] = agent.Agency_Name;
            }
            if (license.Updated_By != null)
            {
                ViewData["UserName"] = _userRepository.GetUser((int)license.Updated_By).First_Name + " " + _userRepository.GetUser((int)license.Updated_By).Last_Name;
            }
            else
            {
                ViewData["UserName"] = "";
            }
            return View(license);
        }

        public ActionResult New(int? id)
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            License license = new License();
            if (id != null)
            {
                ViewData["Agent_List"] = new SelectList(db.sp_Get_Active_Agents().ToList(), "Agent_ID", "Full_Name", (int)id);
            }
            else
            {
                ViewData["Agent_List"] = new SelectList(db.sp_Get_Active_Agents().ToList(), "Agent_ID", "Full_Name");
            }
            ViewData["Line_Of_Insurance"] = new SelectList(_dropRepository.GetInsuranceLine(true), "Line_Of_Insurance_ID", "Description", -1);
            ViewData["State"] = new SelectList(_dropRepository.GetStates(true), "Abbreviation", "Abbreviation", "");
            return View(license);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult New(FormCollection collection)
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            License license = new License();
            if (collection["States"] == "")
            {
                ModelState.AddModelError("States", "State Cannot Be Blank");
                ModelState.SetModelValue("States", new ValueProviderResult(ValueProvider.GetValue("States").AttemptedValue, collection["States"], System.Globalization.CultureInfo.CurrentCulture));

            }
            if (collection["Effective_Date"] == "")
            {
                ModelState.AddModelError("Effective_Date", "Effective Date Cannot Be Blank");
                ModelState.SetModelValue("Effective_Date", new ValueProviderResult(ValueProvider.GetValue("Effective_Date").AttemptedValue, collection["Effective_Date"], System.Globalization.CultureInfo.CurrentCulture));

            }
            if (collection["License_Number"] == "")
            {
                ModelState.AddModelError("License_Number", "License Number Cannot Be Blank");
                ModelState.SetModelValue("License_Number", new ValueProviderResult(ValueProvider.GetValue("License_Number").AttemptedValue, collection["License_Number"], System.Globalization.CultureInfo.CurrentCulture));

            }
            if (collection["Line_Of_Insurance_ID"] == "-1")
            {
                ModelState.AddModelError("Line_Of_Insurance_ID", "Line Of Insurance Cannot Be Blank");
                ModelState.SetModelValue("Line_Of_Insurance_ID", new ValueProviderResult(ValueProvider.GetValue("Line_Of_Insurance_ID").AttemptedValue, collection["Line_Of_Insurance_ID"], System.Globalization.CultureInfo.CurrentCulture));

            }
            if (ModelState.IsValid)
            {
                try
                {
                    license.Agent_ID = Int32.Parse(collection["Agent"]);
                    license.States = collection["States"];
                    license.Effective_Date = DateTime.Parse(collection["Effective_Date"]);
                    license.License_Number = collection["License_Number"];
                    license.Line_Of_Insurance_ID = Int32.Parse(collection["Line_Of_Insurance_ID"]);
                    license.Active = Boolean.Parse(Request.Form.GetValues("Active")[0]);
                    if (collection["Renewal_Date"].Trim() != "")
                    {
                        license.Renewal_Date = DateTime.Parse(collection["Renewal_Date"]);
                    }
                    else
                    {
                        license.Renewal_Date = null;
                    }
                    license.Last_Updated_Date = DateTime.Now;
                    license.Updated_By = _userRepository.GetUserByUserName(User.Identity.Name).User_ID;
                    db.Licenses.InsertOnSubmit(license);
                    db.SubmitChanges();
                    return RedirectToAction("Details", new { id = license.License_ID });
                }
                catch (Exception e)
                {
                    LMS.ErrorHandler.HandleError.EmailError(e);
                }

            }
            DateTime odate;
            if (DateTime.TryParse(collection["Effective_Date"], out odate))
            {
                license.Effective_Date = DateTime.Parse(collection["Effective_Date"]);
            }
            license.License_Number = collection["License_Number"];
            license.Active = Boolean.Parse(Request.Form.GetValues("Active")[0]);
            List<sp_Get_Active_AgentsResult> agents = db.sp_Get_Active_Agents().ToList();
            ViewData["Agent_List"] = new SelectList(agents, "Agent_ID", "Full_Name", Int32.Parse(collection["Agent"]));
            ViewData["Line_Of_Insurance"] = new SelectList(_dropRepository.GetInsuranceLine(true), "Line_Of_Insurance_ID", "Description", Int32.Parse(collection["Line_Of_Insurance_ID"]));
            ViewData["State"] = new SelectList(_dropRepository.GetStates(true), "Abbreviation", "Abbreviation", collection["States"]);
            return View(license);

        }

        public ActionResult Edit(int id)
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            License license = _licenseRepository.GetLicenseByID(id);
            List<sp_Agent_ListResult> agents = db.sp_Agent_List().ToList();
            ViewData["Agent_List"] = new SelectList(agents, "Agent_ID", "Name",license.Agent_ID);
            ViewData["Line_Of_Insurance"] = new SelectList(_dropRepository.GetInsuranceLine(false), "Line_Of_Insurance_ID", "Description",license.Line_Of_Insurance_ID );
            ViewData["State"] = new SelectList(_dropRepository.GetStates(false), "Abbreviation", "Abbreviation", license.States);
            return View(license);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(int id, FormCollection collection)
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            License license = db.Licenses.Where(l => l.License_ID == id).FirstOrDefault();
            if (collection["Effective_Date"] == "")
            {
                ModelState.AddModelError("Effective_Date", "Effective Date Cannot Be Blank");
                ModelState.SetModelValue("Effective_Date", new ValueProviderResult(ValueProvider.GetValue("Effective_Date").AttemptedValue, collection["Effective_Date"], System.Globalization.CultureInfo.CurrentCulture));
            }
            if (collection["License_Number"] == "")
            {
                ModelState.AddModelError("License_Number", "License Number Cannot Be Blank");
                ModelState.SetModelValue("License_Number", new ValueProviderResult(ValueProvider.GetValue("License_Number").AttemptedValue, collection["License_Number"], System.Globalization.CultureInfo.CurrentCulture));
            }
            if (ModelState.IsValid)
            {
                try
                {
                    license.Agent_ID = Int32.Parse(collection["Agent_ID"]);
                    license.States = collection["States"];
                    license.Effective_Date = DateTime.Parse(collection["Effective_Date"]);
                    license.License_Number = collection["License_Number"];
                    license.Line_Of_Insurance_ID = Int32.Parse(collection["Line_Of_Insurance_ID"]);
                    license.Active = Boolean.Parse(Request.Form.GetValues("Active")[0]);
                    if (collection["Renewal_Date"].Trim() != "")
                    {
                        license.Renewal_Date = DateTime.Parse(collection["Renewal_Date"]);
                    }
                    else
                    {
                        license.Renewal_Date = null;
                    }
                    license.Last_Updated_Date = DateTime.Now;
                    license.Updated_By = _userRepository.GetUserByUserName(User.Identity.Name).User_ID;
                    db.SubmitChanges();
                    return RedirectToAction("Details", new { id = license.License_ID });
                }
                catch (Exception e)
                {
                    LMS.ErrorHandler.HandleError.EmailError(e);
                }

            }
            DateTime odate;
            if (DateTime.TryParse(collection["Effective_Date"], out odate))
            {
                license.Effective_Date = DateTime.Parse(collection["Effective_Date"]);
            }
            license.License_Number = collection["License_Number"];
            license.Active = Boolean.Parse(Request.Form.GetValues("Active")[0]);
            List<sp_Agent_ListResult> agents = db.sp_Agent_List().ToList();
            ViewData["Agent_List"] = new SelectList(agents, "Agent_ID", "Name", Int32.Parse(collection["Agent_ID"]));
            ViewData["Line_Of_Insurance"] = new SelectList(_dropRepository.GetInsuranceLine(false), "Line_Of_Insurance_ID", "Description", Int32.Parse(collection["Line_Of_Insurance_ID"]));
            ViewData["State"] = new SelectList(_dropRepository.GetStates(false), "Abbreviation", "Abbreviation", collection["States"]);
            return View(license);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CreateLineOfInsurance(FormCollection collection)
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            if (String.IsNullOrEmpty(collection["Description"]))
            {
                ModelState.AddModelError("Description", "*");
            }
            if (ModelState.IsValid)
            {
                Insurance_Line_Lookup product = new Insurance_Line_Lookup();
                product.Description = collection["Description"];
                db.Insurance_Line_Lookups.InsertOnSubmit(product);
                db.SubmitChanges();
            }
            return RedirectToAction("New", "License");

        }

        public ActionResult NPN()
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            ViewData["NPNList"] = db.sp_NPN_List().ToList();
            return View();
        }

        public ActionResult Export(int AgentID, string Chk, string State)
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            DataTable table;
            DataColumn column;
            DataRow row;
            DataSet dataset;
            table = new DataTable("LicenseByAgent");
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Name";
            column.ReadOnly = true;
            table.Columns.Add(column);
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "State";
            column.ReadOnly = true;
            table.Columns.Add(column);
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "License#";
            column.ReadOnly = true;
            table.Columns.Add(column);
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "EffectiveDate";
            column.ReadOnly = true;
            table.Columns.Add(column);
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "RenewalDate";
            column.ReadOnly = true;
            table.Columns.Add(column);
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "LineOfAuthority";
            column.ReadOnly = true;
            table.Columns.Add(column);
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Active";
            column.ReadOnly = true;
            table.Columns.Add(column);
            if (String.IsNullOrEmpty(Chk) ||(!String.IsNullOrEmpty(Chk) && !Chk.Contains("true")))
            {
                dataset = new DataSet();
                dataset.Tables.Add(table);
                List<sp_License_List_By_AgentResult> rs = db.sp_License_List_By_Agent(AgentID).ToList();
                for (int i = 0; i < rs.Count; i++)
                {
                    row = table.NewRow();
                    if (rs[i].Agent_Type_ID == 1)
                    {
                        row["Name"] = rs[i].First_Name + " " + rs[i].Last_Name;
                    }
                    else
                    {
                        row["Name"] = rs[i].Agency_Name;
                    }
                    row["State"] = rs[i].States;
                    row["License#"] = rs[i].License_Number;
                    if (rs[i].Effective_Date != null)
                    {
                        row["EffectiveDate"] = rs[i].Effective_Date.Value.ToShortDateString();
                    }
                    else
                    {
                        row["EffectiveDate"] = "";
                    }
                    if (rs[i].Renewal_Date != null)
                    {
                        row["RenewalDate"] = rs[i].Renewal_Date.Value.ToShortDateString();
                    }
                    else
                    {
                        row["RenewalDate"] = "";
                    }
                    row["LineOfAuthority"] = rs[i].Line_Of_Insurance;
                    if (rs[i].Active != null && rs[i].Active == true)
                    {
                        row["Active"] = "Yes";
                    }
                    else
                    {
                        row["Active"] = "No";
                    }
                    table.Rows.Add(row);
                }
                if (rs.Count > 0)
                {
                    RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
                    objExport.ExportDetails(table, RKLib.ExportData.Export.ExportFormat.Excel, "LicenseByAgent.xls");
                }
                else
                {
                    return RedirectToAction("RecordOutput", "Certification");
                }
            }
            else
            {
                dataset = new DataSet();
                dataset.Tables.Add(table);
                List<sp_License_List_By_StateResult> rs = db.sp_License_List_By_State(State).ToList();
                for (int i = 0; i < rs.Count; i++)
                {
                    row = table.NewRow();
                    if (rs[i].Agent_Type_ID == 1)
                    {
                        row["Name"] = rs[i].First_Name + " " + rs[i].Last_Name;
                    }
                    else
                    {
                        row["Name"] = rs[i].Agency_Name;
                    }
                    row["State"] = rs[i].States;
                    row["License#"] = rs[i].License_Number;
                    if (rs[i].Effective_Date != null)
                    {
                        row["EffectiveDate"] = rs[i].Effective_Date.Value.ToShortDateString();
                    }
                    else
                    {
                        row["EffectiveDate"] = "";
                    }
                    if (rs[i].Renewal_Date != null)
                    {
                        row["RenewalDate"] = rs[i].Renewal_Date.Value.ToShortDateString();
                    }
                    else
                    {
                        row["RenewalDate"] = "";
                    }
                    row["LineOfAuthority"] = rs[i].Line_Of_Insurance;
                    if (rs[i].Active != null && rs[i].Active == true)
                    {
                        row["Active"] = "Yes";
                    }
                    else
                    {
                        row["Active"] = "No";
                    }
                    table.Rows.Add(row);
                }
                if (rs.Count > 0)
                {
                    RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
                    objExport.ExportDetails(table, RKLib.ExportData.Export.ExportFormat.Excel, "LicenseByState.xls");
                }
                else
                {
                    return RedirectToAction("RecordOutput", "Certification");
                }
            }

            return View();
        }
    }
}
