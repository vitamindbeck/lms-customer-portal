using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using LMS.Data;
using LMS.Data.Repository;
using System.Net.Mail;

namespace LMS.Controllers
{
    public class RequestController : Controller
    {
        private static LMSDataContext _DB = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
        private static DropDownRepository _DropDown = new DropDownRepository();

        public ActionResult New(int id)
        {
            LMS.Data.User user = UserRepository.GetUser(User.Identity.Name);
            if ((user.Sales_Group_ID == 2) || (user.Sales_Group_ID == 1))
            {

                Sales_Group group = _DB.Sales_Groups.Where(sg => sg.Sales_Group_ID.Equals(user.Sales_Group_ID)).FirstOrDefault();
                Plan_Transaction_Request request = new Plan_Transaction_Request();

                request.Plan_Transaction_ID = id;

                ViewData["To_Employee_ID"] = new SelectList(_DB.Users, "User_ID", "FullName", group.Admin_User_ID);

                return View(request);
            }
            else
            {
                return RedirectToAction("Permission", "Request");
            }
        }

        public ActionResult Permission()
        {
            return View();
        }

        public ActionResult List(int id)
        {
            var data = _DB.Plan_Transaction_Requests.Where(pts => pts.From_Employee_ID.Equals(id) && pts.Answered_On.HasValue.Equals(false));

            var tempdata = from d in data
                           join pt in _DB.Plan_Transactions on d.Plan_Transaction_ID equals pt.Plan_Transaction_ID
                           join r in _DB.Records on pt.Record_ID equals r.Record_ID
                           select new RequestDisplay
                           {
                               Record_ID = r.Record_ID,
                               Requested_On = d.Requested_On,
                               DropDownName = pt.DropDownName,
                               Name = r.FullName,
                               Question = d.Question,
                               To_Employee_ID = d.To_Employee_ID
                           };


            return View(tempdata);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult New(int id, Plan_Transaction_Request request)
        {
            LMS.Data.User user = UserRepository.GetUser(User.Identity.Name);

            request.From_Employee_ID = user.User_ID;
            request.Requested_On = DateTime.Now;
            request.Plan_Transaction_ID = id;

            _DB.Plan_Transaction_Requests.InsertOnSubmit(request);
            _DB.SubmitChanges();

            // Email The Question
            SendRequestEmail(request);


            Plan_Transaction transaction = _DB.Plan_Transactions.Where(t => t.Plan_Transaction_ID.Equals(request.Plan_Transaction_ID)).FirstOrDefault();

            return RedirectToAction("Details", "Record", new { id = transaction.Record_ID });
        }

        public ActionResult Answer(int id)
        {
            Plan_Transaction_Request request = _DB.Plan_Transaction_Requests.Where(r => r.Plan_Transaction_Request_ID.Equals(id)).FirstOrDefault();

            return View(request);

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Answer(int id, FormCollection collection)
        {
            Plan_Transaction_Request request = _DB.Plan_Transaction_Requests.Where(r => r.Plan_Transaction_Request_ID.Equals(id)).FirstOrDefault();
            LMS.Data.User user = UserRepository.GetUser(User.Identity.Name);

            request.Answered_By = user.User_ID;
            request.Answered_On = DateTime.Now;
            request.Answer = collection["Answer"];

            _DB.SubmitChanges();

            _DB.sp_Insert_Note_GBS(request.Plan_Transaction_ID, string.Format("Question: {0} ANSWER: {1}", request.Question, request.Answer));

            SendAnswerEmail(request);

            return RedirectToAction("Index", "Home");
        }

        private void SendRequestEmail(Plan_Transaction_Request request)
        {
            LMS.Data.User user = UserRepository.GetUser(User.Identity.Name);
            LMS.Data.User toUser = _DB.Users.Where(u => u.User_ID.Equals(request.To_Employee_ID)).FirstOrDefault();
            Plan_Transaction transaction = _DB.Plan_Transactions.Where(pt => pt.Plan_Transaction_ID.Equals(request.Plan_Transaction_ID)).FirstOrDefault();
            Record rec = _DB.Records.Where(r => r.Record_ID.Equals(transaction.Record_ID)).FirstOrDefault();
            Sales_Group group = _DB.Sales_Groups.Where(sg => sg.Sales_Group_ID.Equals(user.Sales_Group_ID)).FirstOrDefault();

            MailMessage message = new MailMessage();

            message.From = new MailAddress(user.Email_Address);
            message.To.Add(new MailAddress(toUser.Email_Address));
            message.To.Add(new MailAddress("vhassell@longevityalliance.com"));
            //message.To.Add(new MailAddress("tjones@longevityalliance.com"));
            message.Subject = "Plan Transaction Status Request";

            message.Body = string.Format("Question Regarding Plan: {0}<br />" + System.Environment.NewLine, transaction.DropDownName);
            message.Body += string.Format("Record Name: {0}, {1}<br />" + System.Environment.NewLine, rec.Last_Name, rec.First_Name);
            message.Body += string.Format("Address: {0}<br />", rec.Address_1);
            message.Body += string.Format("City: {0}<br />", rec.City);
            message.Body += System.Environment.NewLine;
            message.Body += request.Question;
            message.Body += System.Environment.NewLine;
            message.Body += "<br /><br />http://lms.com/Request/Answer/" + request.Plan_Transaction_Request_ID.ToString();
            message.IsBodyHtml = true;
            SmtpClient client = new SmtpClient("192.168.1.202");
            client.Send(message);

        }

        private void SendAnswerEmail(Plan_Transaction_Request request)
        {
            LMS.Data.User user = UserRepository.GetUser(User.Identity.Name);
            LMS.Data.User toUser = _DB.Users.Where(u => u.User_ID.Equals(request.From_Employee_ID)).FirstOrDefault();
            Plan_Transaction transaction = _DB.Plan_Transactions.Where(pt => pt.Plan_Transaction_ID.Equals(request.Plan_Transaction_ID)).FirstOrDefault();
            Record rec = _DB.Records.Where(r => r.Record_ID.Equals(transaction.Record_ID)).FirstOrDefault();

            MailMessage message = new MailMessage();

            message.From = new MailAddress(user.Email_Address);
            message.To.Add(new MailAddress(toUser.Email_Address));
            message.To.Add(new MailAddress("vhassell@longevityalliance.com"));
            message.Subject = "Plan Transaction Status Request";

            message.Body = string.Format("Question Regarding Plan: {0}<br />" + System.Environment.NewLine, transaction.DropDownName);
            message.Body += string.Format("Record Name: {0}, {1}<br />" + System.Environment.NewLine, rec.Last_Name, rec.First_Name);
            message.Body += string.Format("Address: {0}<br />", rec.Address_1);
            message.Body += string.Format("City: {0}<br />", rec.City);
            message.Body += "<br />Question:";
            message.Body += "<br />" + request.Question;
            message.Body += "<br />Answer:" + System.Environment.NewLine;
            message.Body += "<br />" + request.Answer;
            message.Body += System.Environment.NewLine;
            message.IsBodyHtml = true;
            SmtpClient client = new SmtpClient("192.168.1.202");
            client.Send(message);

        }


    }
}
