using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Data;
using LMS.Data.Repository;
using LMS.Data;
using LMS.Helpers;
using System.Web.UI.WebControls;

namespace LMS.Controllers
{
    [Authorize]
    [HandleError]
    [SessionExpireFilterAttribute]
    public class LoginInfoController : Controller
    {
        private IDropDownRepository _dropRepository;
        private IUserRepository _userRepository;
        private IAgentRepository _agentRepository;

        public LoginInfoController()
            : this(new DropDownRepository(), new UserRepository(), new AgentRepository())
        {

        }

        public LoginInfoController(IDropDownRepository dropRepository, IUserRepository userRepository, IAgentRepository agentRepository)
        {
            _dropRepository = dropRepository;
            _userRepository = userRepository;
            _agentRepository = agentRepository;
        }

        public ActionResult List(int? id)
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            //int agentid = db.sp_Agent_List().ToList()[1].Agent_ID;
            //get current user
            User cUser = LMS.Data.Repository.UserRepository.GetUser(User.Identity.Name);
            int userID;
            Agent currAgent;
            int default_agent_id = 49;

            if (cUser != null)
            {
                //if current user exists find agent id
               
                    userID = cUser.User_ID;
                
                    currAgent = _agentRepository.GetAgentByUserID(userID);

                   
                    if (currAgent != null)
                    {
                        var securityLevels =(from u in db.User_Security_Levels where currAgent.User_ID == u.User_ID select u).ToList();

                        if (LMS.Data.Repository.UserRepository.HasSecurityLevel(cUser,1))  // (securityLevels.Count > 0)                      
                        {
                            default_agent_id = currAgent.Agent_ID;


                            //foreach(User_Security_Level props in securityLevels)
                            //{


                                //int SecurityLevelID = props.Security_Level_ID;

                                //if (SecurityLevelID == 1)
                                //{
                                //    default_agent_id = currAgent.Agent_ID;
                                //}
                                //else if (SecurityLevelID == 10)
                                //{
                                //    default_agent_id = -2;
                                    
                                //}                                 
                            //}
                        }

                        if (LMS.Data.Repository.UserRepository.HasSecurityLevel(cUser, 10))
                        {
                            default_agent_id = -2;
                        }
                        
                        
                        
                        //if (id == null && currAgent.User_Security_Level.Security_Level_ID.ToString() != "1")
                        //{
                        //    default_agent_id = 18;
                        //}
                        //else if (currAgent.User_Security_Level.Security_Level_ID.ToString() == "1")
                        //{

                        //    default_agent_id = currAgent.Agent_ID;
                        //}
                        
                    }
                    
                    
            }
            
            else
            {
                default_agent_id = -2;
            }

            ViewData["Agent_List"] = new SelectList(_dropRepository.GetAgentAgencyAndAll(false), "Agent_ID", "Name",default_agent_id);
            ViewData["Carrier_List"] = new SelectList(_dropRepository.GetGBSCarriersAndAll(false), "Carrier_ID", "Name", -2);
            ViewData["NonCarrier_List"] = new SelectList(_dropRepository.GetNonCarriersAndAll(false), "Non_Carrier_ID", "Non_Carrier_Name", -2);
            ViewData["State_List"] = new SelectList(_dropRepository.GetStatesAndAll(false), "State_ID", "Abbreviation", -2);

            ViewData["LoginInfoList"] = db.sp_LoginInfo_List(default_agent_id, -2, -2, -2).ToList();
            return View();
         
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult List(FormCollection collection)
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            ViewData["Agent_List"] = new SelectList(_dropRepository.GetAgentAgencyAndAll(false), "Agent_ID", "Name", Int32.Parse(collection["Agent"]));
            ViewData["Carrier_List"] = new SelectList(_dropRepository.GetGBSCarriersAndAll(false), "Carrier_ID", "Name", Int32.Parse(collection["Carrier"]));
            ViewData["NonCarrier_List"] = new SelectList(_dropRepository.GetNonCarriersAndAll(false), "Non_Carrier_ID", "Non_Carrier_Name", Int32.Parse(collection["NonCarrier"]));
            ViewData["State_List"] = new SelectList(_dropRepository.GetStatesAndAll(false), "State_ID", "Abbreviation", Int32.Parse(collection["State"]));
            ViewData["LoginInfoList"] = db.sp_LoginInfo_List(Int32.Parse(collection["Agent"]), Int32.Parse(collection["Carrier"]), Int32.Parse(collection["NonCarrier"]), Int32.Parse(collection["State"])).ToList();

            return View();
        }
        public ActionResult New(int? id)
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            User_Password userPassword = new User_Password();
            
            ViewData["Agent_Name"] = new SelectList(_dropRepository.GetActiveAgentAgency(true), "Agent_ID", "Full_Name", id);
            //Carrier
            ViewData["Carrier"] = new SelectList(_dropRepository.GetGBSCarriers(true), "Carrier_ID", "Name", -1);
            ViewData["State"] = new SelectList(_dropRepository.GetStates(true), "State_ID", "Abbreviation", -1);
            ViewData["NonCarrier"] = new SelectList(_dropRepository.GetNonCarriers(true), "Non_Carrier_ID", "Non_Carrier_Name", -1);
             return View(userPassword);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult New(User_Password userPassword, FormCollection collection)
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

            //Required field Validation

            if (collection["ckOther"].Contains("true") && collection["Non_Carrier_ID"] == "-1")
            {
                ModelState.AddModelError("Non_Carrier_ID", "Non-Carrier Cannot Be Blank");
                ModelState.SetModelValue("Non_Carrier_ID", new ValueProviderResult(ValueProvider.GetValue("Non_Carrier_ID").AttemptedValue, collection["Non_Carrier_ID"], System.Globalization.CultureInfo.CurrentCulture));
            }
           
            int agentID = Int32.Parse(collection["Agent_ID"]);

            if (ModelState.IsValid)
            {
                try
                {
                    userPassword.Agent_ID = agentID;
                    if (collection["ckOther"].Contains("true"))
                    {
                        userPassword.Carrier_ID = -1;
                        userPassword.Non_Carrier_ID = Int32.Parse(collection["Non_Carrier_ID"]);
                    }
                    else
                    {
                        userPassword.Carrier_ID = Int32.Parse(collection["Carrier_ID"]);
                        userPassword.Non_Carrier_ID = -1;
                    }
                    userPassword.State_ID = Int32.Parse(collection["State_ID"]);
                    userPassword.Carrier_Site_Link = collection["Carrier_Site_Link"];
                    userPassword.User_Name = StringHelper.EncryptString(Convert.ToString(collection["Login_User_Name"]));
                    userPassword.Password = StringHelper.EncryptString(Convert.ToString(collection["Login_Password"]));
                   
                    userPassword.Last_Updated_Date = DateTime.Now;
                    userPassword.Updated_By = _userRepository.GetUserByUserName(User.Identity.Name).User_ID;

                    db.User_Passwords.InsertOnSubmit(userPassword);
                    db.SubmitChanges();

                    return RedirectToAction("Details", new { id = userPassword.User_Password_ID });

                }

                catch (Exception e)
                {
                    LMS.ErrorHandler.HandleError.EmailError(e);
                }
            }
            ViewData["Agent_Name"] = new SelectList(_dropRepository.GetActiveAgentAgency(true), "Agent_ID", "Full_Name", Int32.Parse(collection["Agent_ID"]));
            ViewData["Carrier"] = new SelectList(_dropRepository.GetGBSCarriers(true), "Carrier_ID", "Name", Int32.Parse(collection["Carrier_ID"]));
            ViewData["State"] = new SelectList(_dropRepository.GetStates(true), "State_ID", "Abbreviation", Int32.Parse(collection["State_ID"]));
            ViewData["NonCarrier"] = new SelectList(_dropRepository.GetNonCarriers(true), "Non_Carrier_ID", "Non_Carrier_Name", Int32.Parse(collection["Non_Carrier_ID"]));
            return View(userPassword);
        }


        //Certification Details Page
        public ActionResult Details(int? id)
        {

            
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            User_Password userPassword = db.User_Passwords.Where(u => u.User_Password_ID == id).FirstOrDefault();


            //Get Agent Name
            var agents = db.sp_Get_AgentsAgency(userPassword.Agent_ID).FirstOrDefault();
            if (agents != null)
                ViewData["Agent_Name"] = agents.Full_Name;
            else
                ViewData["Agent_Name"] = "";

            //Get Other Details
            Carrier carrierDetails = db.Carriers.Where(c=>c.Carrier_ID == userPassword.Carrier_ID).FirstOrDefault();

            if (carrierDetails != null)
                ViewData["Carrier"] = carrierDetails.Name;
            else
                ViewData["Carrier"] = "";

            if (userPassword.State_ID > 0)
                ViewData["State"] = db.States.Where(s => s.State_ID == userPassword.State_ID).FirstOrDefault().Abbreviation;
            else
                ViewData["State"] = "";

            if (userPassword.Non_Carrier_ID > 0)
                ViewData["NonCarrier"] = db.Non_Carriers.Where(n => n.Non_Carrier_ID == userPassword.Non_Carrier_ID).FirstOrDefault().Non_Carrier_Name;
            else
                ViewData["NonCarrier"] = "";

            ViewData["User_Name"] = userPassword.User_Name;
            
            ViewData["Password"] = userPassword.Password;

            ViewData["Carrier_Site_Link"] = userPassword.Carrier_Site_Link;


            //Username
            if (userPassword.Updated_By != null)
            {
                ViewData["UserName"] = _userRepository.GetUser((int)userPassword.Updated_By).First_Name + " " + _userRepository.GetUser((int)userPassword.Updated_By).Last_Name;
            }
            else
            {
                ViewData["UserName"] = "";
            }


            return View(userPassword);

        }

        //Edit
        public ActionResult Edit(int id)
        {

            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            User_Password userPassword = db.User_Passwords.Where(u => u.User_Password_ID == id).FirstOrDefault();
            ViewData["Agent_Name"] = new SelectList(_dropRepository.GetActiveAgentAgency(true), "Agent_ID", "Full_Name", userPassword.Agent_ID);
           

            ViewData["Carrier"] = new SelectList(_dropRepository.GetGBSCarriers(true), "Carrier_ID", "Name", userPassword.Carrier_ID.ToString());
            ViewData["NonCarrier"] = new SelectList(_dropRepository.GetNonCarriers(true), "Non_Carrier_ID", "Non_Carrier_Name", userPassword.Non_Carrier_ID);
            ViewData["State"] = new SelectList(_dropRepository.GetStates(true), "State_ID", "Abbreviation", userPassword.State_ID);

            return View(userPassword);
        }

        //Edit Post
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(int id, FormCollection collection)
        {

            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            User_Password userPassword = db.User_Passwords.Where(u => u.User_Password_ID == id).FirstOrDefault();

            if (collection["ckOther"].Contains("true") && collection["Non_Carrier_ID"] == "-1")
            {
                ModelState.AddModelError("Non_Carrier_ID", "Non-Carrier Cannot Be Blank");
                ModelState.SetModelValue("Non_Carrier_ID", new ValueProviderResult(ValueProvider.GetValue("Non_Carrier_ID").AttemptedValue, collection["Non_Carrier_ID"], System.Globalization.CultureInfo.CurrentCulture));
            }
            int agentID = Int32.Parse(collection["Agent_ID"]);
            if (ModelState.IsValid)
            {
                try
                {

                    userPassword.Agent_ID = agentID;
                    if (collection["ckOther"].Contains("true"))
                    {
                        userPassword.Carrier_ID = -1;
                        userPassword.Non_Carrier_ID = Int32.Parse(collection["Non_Carrier_ID"]);
                    }
                    else
                    {
                        userPassword.Carrier_ID = Int32.Parse(collection["Carrier_ID"]);
                        userPassword.Non_Carrier_ID = -1;
                    }
                    userPassword.State_ID = Int32.Parse(collection["State_ID"]);
                    userPassword.Carrier_Site_Link = collection["Carrier_Site_Link"];
                    userPassword.User_Name = StringHelper.EncryptString(Convert.ToString(collection["User_Name"]));
                    userPassword.Password = StringHelper.EncryptString(Convert.ToString(collection["Password"]));
                    userPassword.Last_Updated_Date = DateTime.Now;
                    userPassword.Updated_By = _userRepository.GetUserByUserName(User.Identity.Name).User_ID;

                    db.SubmitChanges();
                    return RedirectToAction("Details", new { id = userPassword.User_Password_ID });
                }
                catch (Exception e)
                {
                   LMS.ErrorHandler.HandleError.EmailError(e);
                }

            }
            ViewData["NonCarrier"] = new SelectList(_dropRepository.GetNonCarriers(true), "Non_Carrier_ID", "Non_Carrier_Name", Int32.Parse(collection["Non_Carrier_ID"]));
            ViewData["State"] = new SelectList(_dropRepository.GetStates(true), "State_ID", "Abbreviation", Int32.Parse(collection["State_ID"]));
            ViewData["Agent_Name"] = new SelectList(_dropRepository.GetActiveAgentAgency(true), "Agent_ID", "Full_Name", userPassword.Agent_ID);
            ViewData["Carrier"] = new SelectList(_dropRepository.GetGBSCarriers(true), "Carrier_ID", "Name", Int32.Parse(collection["Carrier_ID"]));
           
            return View(userPassword);
        }


        //Export to Excel
        public ActionResult Export(int AgentID, int CarrierID, int StateID, int NoncarrierID)
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            DataTable table;
            DataColumn column;
            DataRow row;
            DataSet dataset;
            table = new DataTable("LoginInfoList");

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Agent/Agency";
            column.ReadOnly = true;
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Carrier/Non-carrier/State";
            column.ReadOnly = true;
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Carrier Site Link";
            column.ReadOnly = true;
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "User Name";
            column.ReadOnly = true;
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Password";
            column.ReadOnly = true;
            table.Columns.Add(column);

            dataset = new DataSet();
            dataset.Tables.Add(table);
            List<sp_LoginInfo_ListResult> rs = db.sp_LoginInfo_List(AgentID,CarrierID,NoncarrierID,StateID).ToList();
            for (int i = 0; i < rs.Count; i++)
            {
                row = table.NewRow();
                if (!String.IsNullOrEmpty(rs[i].First_Name))
                {
                    row["Agent/Agency"] = rs[i].First_Name + " " + rs[i].Last_Name;
                }
                else
                {
                    row["Agent/Agency"] = rs[i].Agency_Name;
                }
                row["Carrier/Non-carrier/State"] = rs[i].Name + rs[i].Abbreviation;
                row["Carrier Site Link"] = rs[i].Carrier_Site_Link;
                row["User Name"] = rs[i].User_Name;
                row["Password"] = rs[i].Password;

                table.Rows.Add(row);
            }
            if (rs.Count > 0)
            {
                RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
                objExport.ExportDetails(table, RKLib.ExportData.Export.ExportFormat.Excel, "LoginInfoByAgent.xls");
            }
            else
            {
                return RedirectToAction("RecordOutput", "Certification");
            }

            return View();
        }


    }
}
