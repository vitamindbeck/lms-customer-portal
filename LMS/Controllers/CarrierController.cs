using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using LMS.Data.Repository;
using LMS.Data;

namespace LMS.Controllers
{
    public class CarrierController : Controller
    {
        private ICarrierRepository _repository;
        private IDropDownRepository _dropRepository;

        public CarrierController()
            : this(new CarrierRepository(), new DropDownRepository())
        {
        }

        public CarrierController(ICarrierRepository repository, IDropDownRepository droprepository)
        {
            _repository = repository;
            _dropRepository = droprepository;
        }


        public ActionResult Index()
        {

            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            return View(_repository.GetAll());
        }



        public ActionResult Edit(int id)
        {
            Carrier carrier = _repository.Get(id);
            ViewData["State_ID"] = new SelectList(_dropRepository.GetStates(false), "State_ID", "Name", carrier.State_ID);
            ViewData["Group_ID"] = new SelectList(_dropRepository.GetGroups(false), "Group_ID", "Description", carrier.Group_ID);
            return View(carrier);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(int id, Carrier pageCarrier)
        {
            Carrier carrier = _repository.Get(id);
            UpdateModel(carrier);
            _repository.Save();

            return RedirectToAction("List");
        }




        public ActionResult New()
        {
            Carrier carrier = new Carrier();
            ViewData["State_ID"] = new SelectList(_dropRepository.GetStates(false), "State_ID", "Name", carrier.State_ID);

            return View(carrier);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult New(Carrier carrier)
        {
           
            _repository.Insert(carrier);
            _repository.Save();

            return RedirectToAction("Index");
        }


        public ActionResult Details(int id)
        {
            //return View(_repository.Get(id));
            return PartialView("CarrierView", _repository.Get(id));
        }

     
        public ActionResult Search(string q, int limit)
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            var list = db.Carriers.Where(c => c.Name.Contains(q)).OrderBy(c => c.Name).Take(limit).ToList();
            var data = from s in list select new { s.Name, s.City, s.Carrier_ID };
            return Json(data);
        }

    }
}
