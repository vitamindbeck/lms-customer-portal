﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq;
using System.Web;
using System.Web.Mvc;
using LMS.Data;
using LMS.Data.Repository;
using Lib.Web.Mvc.JQuery.JqGrid;
using System.Text;
using System.IO;
using LMS.Models;
using System.Runtime.Serialization;
using LMS.Helpers;

namespace LMS.Controllers
{
    public class OperationsAdminController : Controller
    {
        #region Fields
        private IMasterFileRepository _masterFileRepository;
        private IDropDownRepository _dropRepository;
        #endregion

        #region Constructors
        public OperationsAdminController()
            : this(new MasterFileRepository(), new DropDownRepository())
        {
        }

        public OperationsAdminController(IMasterFileRepository masterFileRepository, IDropDownRepository droprepository)
        {
            _masterFileRepository = masterFileRepository;
            _dropRepository = droprepository;
        }
        #endregion

        // GET: /OperationsAdmin/Details/5

        public ActionResult Retrieve()
        {

            string stateString = string.Empty;
            var viewModel = new CustomSearchViewModel();
            if (viewModel.Populated)
            {
                Session["viewModel"] = viewModel;
            }
            else
            {
                viewModel = (CustomSearchViewModel)Session["viewModel"];
                if (viewModel != null)
                {
                    if (viewModel.States != null)
                    {
                        stateString = string.Join(",", viewModel.States);
                    }

                }
            }
            if (viewModel == null)
            { viewModel = new CustomSearchViewModel { Plan_Transaction_Status_ID = -1 }; }

            //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            ViewData["Catagory_DriverDDL"] = new SelectList(_dropRepository.GetCatagoryDriver(true), "CategoryDriverID", "CategoryDriver1");
            ViewData["Catagory_Driver_searchDDL"] = new SelectList(_dropRepository.GetCatagoryDriver(true), "CategoryDriver1", "CategoryDriver1");
            ViewData["CategorySituationSearchDDL"] = new SelectList(_dropRepository.GetCategorySituation(true), "CategorySituationID", "CategorySituationDesc");
            ViewData["CategorySituationDDL"] = new SelectList(_dropRepository.GetCategorySituation(true), "CategorySituationID", "CategorySituationDesc");

            ViewData["RecordTypeSearchDDL"] = new SelectList(_dropRepository.GetRecordStatus(true), "Record_Type_ID", "Description");

            ViewData["DOB_MonthSearchDDL"] = new SelectList(_dropRepository.GetAllMonths(true), "Key", "Value");
            ViewData["Effective_MonthSearchDDL"] = new SelectList(_dropRepository.GetAllMonths(true), "Key", "Value");

            //Category_Situation
            ViewData["PriorityDDL"] = new SelectList(_dropRepository.GetPriorityOpAdmin(true), "Key", "Value", 3);
            ViewData["AgentGroupDDL"] = new SelectList(_dropRepository.GetAgentGroup(true), "Description", "Description");
            ViewData["AgentTypeDDL"] = new SelectList(_dropRepository.GetAgentType(true), "Key", "Value");
            ViewData["AgentFinal_searchDDL"] = new SelectList(_dropRepository.GetAgentFinal(true), "User_ID", "User_Name");
            ViewData["AgentFinalDDL"] = new SelectList(_dropRepository.GetAgentFinal(true), "User_ID", "User_Name");

            ViewData["AgentStatusDDL"] = new SelectList(_dropRepository.GetAgentStatus(true), "Key", "Value");
            ViewData["State"] = new SelectList(_dropRepository.GetStatesAndAll(false), "Abbreviation", "Abbreviation", stateString);

            ViewData["FollowUpTypeDDL"] = new SelectList(_dropRepository.GetActivityTypes(true), "Activity_Type_ID", "Description");


            //ViewData["FollowUpUser"] = new SelectList(_dropRepository.GetAgents(true).Where(a => a.Active == true), "User_ID", "FullName",null);//user table lms 
            ViewData["RecordStatusDDL"] = new SelectList(_dropRepository.GetCustomRecordStatus(true), "Key", "Value");
            ViewData["CurrentDispostionDDL"] = new SelectList(_dropRepository.GetReconciliationTypes(true, null), "Description", "Description");
            ViewData["SourceNumberDDL"] = new SelectList(_dropRepository.GetSourceCodes(true), "Source_Code_ID", "Source_Number");//source code table lms - source number and description
            //ViewData["CreateFollowUp_User"] = new SelectList(_dropRepository.GetAgents(true).Where(a => a.Active == true), "User_ID", "FullName", null);//user table lms 
            ViewData["PartnerDDL"] = new SelectList(_dropRepository.GetYesNo(true), "Key", "Value");//built in view .. "Y" or "N"

            ViewData["CarrierDDL"] = new SelectList(_dropRepository.GetGBSCarriers(true), "Carrier_ID", "Name");//Administrators table in GBS
            ViewData["PlanNameDDL"] = new SelectList(_dropRepository.GetGBSProducts(true), "ProductName", "ProductName");//Products table GBS
            ViewData["ProductTypeDDL"] = new SelectList(_dropRepository.GetGBSProductTypeLookup(true), "ProductType", "ProductTypeDescription");//May need to get these from some one hold off

            ViewData["PlanTransactionStatusDDL"] = new SelectList(_dropRepository.GetTransactionStatus(true).OrderBy(x => x.Plan_Transaction_Status_ID), "Plan_Transaction_Status_ID", "Description");
            ViewData["AgentDeparted_searchDDL"] = new SelectList(_dropRepository.GetAgentsDeparted(true).OrderBy(x => x.First_Name), "User_ID", "User_Name");
            //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

            return View(viewModel);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Retrieve(JqGridRequest request, CustomSearchViewModel viewModel)
        {
            string filterExpression = String.Empty;
            JqGridResponse response = new JqGridResponse();
            try
            {
                if (viewModel.Populated)
                {
                    Session["viewModel"] = viewModel;
                }
                else
                {
                    viewModel = (CustomSearchViewModel)Session["viewModel"];
                }

                var stateList = new List<string>();
                var agentList = new List<int>();
                if (viewModel != null)
                {
                    filterExpression = GetFilterExpression(viewModel);

                    if (viewModel.States != null)
                    {
                        stateList = viewModel.States.ToList();
                    }

                    if (viewModel.Agent_Departed != null)
                    {
                        agentList = viewModel.Agent_Departed.ToList();
                    }
                }

                var masterItems = new List<MasterListLocal>();
                if ((List<MasterListLocal>)Session["sessionMasterList"] == null)
                {
                    List<vw_MasterFile_v2> masterList = _masterFileRepository.GetFilteredData(filterExpression.ToString(), stateList, agentList);

                    masterItems = GetJqueryGridList(masterList);
                    if (masterItems.Count > 0)
                        Session["sessionMasterList"] = masterItems;
                }
                else
                {
                    masterItems = (List<MasterListLocal>)Session["sessionMasterList"];
                }

                int totalRecords = masterItems.Count();
                //Fix for grouping, because it adds column name instead of index to SortingName
                string sortingName = request.SortingName.Replace("AgentFinalUserID", "Agent_Final").Replace("CategoryDriverID", "Category_Driver").Replace("Lead_Name", "LastName").Replace("CategorySituationID", "CategorySituationDesc");

                //Total pages count
                response.TotalPagesCount = (int)Math.Ceiling((float)totalRecords / (float)request.RecordsCount);
                //Page number
                response.PageIndex = request.PageIndex;
                //Total records count
                response.TotalRecordsCount = totalRecords;

                string sortExpression = sortingName;
                string sortDirection = request.SortingOrder.ToString();
                int pageIndex = request.PageIndex;
                int pageSize = request.RecordsCount;
                int pagesCount = request.PagesCount.HasValue ? request.PagesCount.Value : 1;

                try
                {
                    ////Table with rows data
                    var x = 1;
                    if (totalRecords > 0)
                    {
                        foreach (MasterListLocal item in masterItems.AsQueryable().OrderBy(sortExpression + " " + sortDirection).Skip(pageIndex * pageSize).Take(pageSize))
                        {
                            response.Records.Add(new JqGridRecord(Convert.ToString(item.MasterFileID), new List<object>()
                        {
                            string.Empty,
                            x,
                            item.MasterFileID,
                            item.GBSPolicyID,
                            item.LMSID,
                            item.LastName.Trim() + ", " + item.FirstName.Trim(),
                            item.NextFollowUp.ToString(),
                            item.Customer_State,
                            item.Category_Driver,
                            item.Priority,
                            item.Product_Type,
                            item.Plan_Type,
                            item.SourceNumber,
                            item.Agent_Final,
                            item.CategorySituationDesc,
                            item.Record_ID,
                            item.Plan_Transaction_ID,
                            item.Plan_Transaction_Status_Description,
                            Convert.ToDateTime(item.EffectiveDateOrPurchaseDate).ToShortDateString(), 
                            Convert.ToDateTime(item.DOB).ToShortDateString()
                        }));
                            x++;
                        }
                    }
                }
                catch (Exception)
                {

                    throw;
                }
            }
            catch (Exception)
            {

                throw;
            }

            //Return data as json
            return new JqGridJsonResult() { Data = response };
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Search(CustomSearchViewModel searchModel)
        {
            var requestJqgrid = new JqGridRequest
               {
                   RecordsCount = 25,
                   PageIndex = 0,
                   SortingName = "NextFollowUp",
                   SortingOrder = JqGridSortingOrders.Asc
               };

            searchModel.Populated = true;

            TempData["2"] = searchModel;
            Session["viewModel"] = searchModel;
            Session["sessionMasterList"] = null;

            return RedirectToAction("Retrieve", new { viewModel = searchModel });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateMaster(List<MasterTable> viewModel)
        {
            var results = new UpdateResults();
            int totalRecords = viewModel.Count();
            int updateCount = 0;
            string resultMessage = string.Empty;

            try
            {
                using (var _db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString))
                {
                    var masterItems = (List<MasterListLocal>)Session["sessionMasterList"];
                    foreach (MasterTable masterItem in viewModel)
                    {
                        var masterFile = new LongevityMasterDataFile();
                        masterFile = _db.LongevityMasterDataFiles.Where(x => x.MasterFileID == masterItem.MasterFileID).FirstOrDefault();

                        if (masterItem.CategoryDriverID > 0)
                            masterFile.CategoryDriverID = masterItem.CategoryDriverID;

                        //if (masterItem.Priority > 0)
                        //    masterFile.Priority = masterItem.Priority;

                        if (masterItem.AgentFinalUserID > 0)
                            masterFile.AgentFinalUserID = masterItem.AgentFinalUserID;

                        if (masterItem.CategorySituationID > 0)
                            masterFile.CategorySituationID = masterItem.CategorySituationID;

                        _db.SubmitChanges();

                        //UpdateSessionValues
                        if (masterItems != null)
                        {
                            foreach (MasterListLocal item in masterItems)
                            {
                                if (item.MasterFileID == masterItem.MasterFileID)
                                {
                                    if (!string.IsNullOrEmpty(masterItem.CategoryDriverText) && masterItem.CategoryDriverID > 0)
                                        item.Category_Driver = masterItem.CategoryDriverText;

                                    //if (masterItem.Priority > 0)
                                    //    item.Priority = masterItem.Priority;

                                    if (!string.IsNullOrEmpty(masterItem.AgentFinalUserText) && masterItem.AgentFinalUserID > 0)
                                        item.Agent_Final = masterItem.AgentFinalUserText;

                                    if (!string.IsNullOrEmpty(masterItem.CategorySituationText) && masterItem.CategorySituationID > 0)
                                        item.CategorySituationDesc = masterItem.CategorySituationText;
                                }
                            }
                        }
                        updateCount++;
                    }
                    Session["sessionMasterList"] = masterItems;
                }

                resultMessage = string.Format("{0} of {1} updates completed successfuly", updateCount, totalRecords);
                results.Success = true;
                results.Message = resultMessage;
            }
            catch (Exception e)
            {
                resultMessage = string.Format("{0} of {1} records updated before there was a problem. Exception Message: {2}.", updateCount, totalRecords, e.Message);
                results.Success = false;
                results.Message = resultMessage;
            }

            return Json(results);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CreateFollowUp(List<CreateFollowUp> createFollowUp)
        {
            var results = new UpdateResults();
            int totalRecords = createFollowUp.Count();
            int updateCount = 0;
            int notUpdatedCount = 0;
            string resultMessage = string.Empty;

            using (var _DB = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString))
            {
                try
                {
                    foreach (CreateFollowUp item in createFollowUp)
                    {
                        if (item.Record_ID > 0)
                        {
                            var CreateFollowUp = new Record_Activity();
                            var catdrive = new CategoryDriver();

                            catdrive = _DB.CategoryDrivers.Where(cd => cd.CategoryDriver1 == item.CategoryDriverID).FirstOrDefault();

                            if (catdrive != null)
                            {
                                CreateFollowUp.Notes = StringHelper.EncryptString(catdrive.CategoryDriver1 + " " + catdrive.CategoryDriverDetails + item.FollowUpNotes);
                            }
                            else
                            { CreateFollowUp.Notes = StringHelper.EncryptString(string.Empty); }

                            CreateFollowUp.Created_By = 35; //admin agent
                            CreateFollowUp.Created_On = DateTime.Now;
                            CreateFollowUp.User_ID = item.FollowUpFor;
                            CreateFollowUp.Record_ID = item.Record_ID;
                            CreateFollowUp.Due_Date = Convert.ToDateTime(item.FollowUpDate);
                            //CreateFollowUp.Actual_Date = DateTime.Now;
                            CreateFollowUp.Activity_Type_ID = item.FollowUpType;
                            CreateFollowUp.Plan_Transaction_ID = item.Plan_Transaction_ID;
                            CreateFollowUp.Priority_Level = item.FollowUpPriority;
                            CreateFollowUp.Record_Activity_Status_ID = 1;
                            //CreateFollowUp.Record_Activity_Note_ID = Convert.ToInt32(collection["RecordNotes"]);
                            CreateFollowUp.Notes = StringHelper.EncryptString(item.FollowUpNotes);

                            _DB.Record_Activities.InsertOnSubmit(CreateFollowUp);
                            _DB.SubmitChanges(ConflictMode.ContinueOnConflict);

                            updateCount++;
                        }
                    }
                    notUpdatedCount = totalRecords - updateCount;

                    string notUpdatedMessage = string.Empty;
                    if (notUpdatedCount > 0)
                        notUpdatedMessage = string.Format("{0} records are missing Record_ID's and no updates were made.", notUpdatedCount);

                    resultMessage = string.Format("{0} of {1} updates completed successfuly. {2}", updateCount, totalRecords, notUpdatedMessage);
                    results.Success = true;
                    results.Message = resultMessage;
                }
                catch (Exception e)
                {
                    string result = string.Format("{0} of {1} records updated before there was a problem. Exception Message: {2}.", updateCount, totalRecords, e.Message);
                    results.Success = false;
                    results.Message = result;
                }
            };

            return Json(results);
        }

        #region jqGridDDLS

        [HttpGet]
        public ActionResult Category_Driver()
        {
            //Get Schools
            var qry = _dropRepository.GetCatagoryDriver(true);

            //Convert to Dictionary
            var ls = qry.ToDictionary(q => q.CategoryDriverID, q => q.CategoryDriver1);

            //Return Partial View
            return PartialView("Select", ls);
        }

        [HttpGet]
        public ActionResult Category_Situation_DDL()
        {
            //Get Schools
            var qry = _dropRepository.GetCategorySituation(true);

            //Convert to Dictionary
            var ls = qry.ToDictionary(q => q.CategorySituationID, q => q.CategorySituationDesc);

            //Return Partial View
            return PartialView("Select", ls);
        }

        [HttpGet]
        public ActionResult Priority()
        {
            //Get Schools
            var qry = _dropRepository.GetPriorityOpAdmin(true);

            //Convert to Dictionary
            var ls = qry.ToDictionary(q => q.Key, q => q.Value);

            //Return Partial View
            return PartialView("Select", ls);
        }

        [HttpGet]
        public ActionResult Agent_Final()
        {
            //Get Schools
            var qry = _dropRepository.GetAgentFinal(true);

            //Convert to Dictionary
            var ls = qry.ToDictionary(q => q.User_ID, q => q.User_Name);

            //Return Partial View
            return PartialView("Select", ls);
        }
        #endregion

        #region Methods

        public string GetFilterExpression(CustomSearchViewModel SearchViewModel)
        {
            var viewModel = cleanObject(SearchViewModel);
            var filterExpressionBuilder = new StringBuilder();

            if (!String.IsNullOrEmpty(viewModel.PolicyNumber))
                filterExpressionBuilder.Append(String.Format("PolicyNumber = \"{0}\" AND ", viewModel.PolicyNumber.Trim()));

            if (!String.IsNullOrEmpty(viewModel.Agent_Status))
                filterExpressionBuilder.Append(String.Format("Agent_Status = \"{0}\" AND ", viewModel.Agent_Status.Trim()));

            if (!String.IsNullOrEmpty(viewModel.Category_Driver))
                filterExpressionBuilder.Append(String.Format("Category_Driver = \"{0}\" AND ", viewModel.Category_Driver.Trim()));

            if (!String.IsNullOrEmpty(viewModel.Agent_Group))
                filterExpressionBuilder.Append(String.Format("AgentGroup = \"{0}\" AND ", viewModel.Agent_Group.Trim()));

            if (!String.IsNullOrEmpty(viewModel.Agent_Type))
                filterExpressionBuilder.Append(String.Format("Agent_Type = \"{0}\" AND ", viewModel.Agent_Type.Trim()));

            if (!String.IsNullOrEmpty(viewModel.FirstName))
                filterExpressionBuilder.Append(String.Format("FirstName.ToUpper() = \"{0}\" AND ", viewModel.FirstName.ToUpper().Trim()));

            if (!String.IsNullOrEmpty(viewModel.LastName))
                filterExpressionBuilder.Append(String.Format("LastName.ToUpper() = \"{0}\" AND ", viewModel.LastName.ToUpper().Trim()));

            if (viewModel.EffectiveDateOrPurchaseDateMonth > 0)
                filterExpressionBuilder.Append(String.Format("EffectiveDateOrPurchaseDateMonth = {0} AND ", viewModel.EffectiveDateOrPurchaseDateMonth));

            if (viewModel.Plan_Transaction_Status_ID > 0)
                filterExpressionBuilder.Append(String.Format("Plan_Transaction_Status_ID = {0} AND ", viewModel.Plan_Transaction_Status_ID));

            if (viewModel.Agent_Final > 0)
                filterExpressionBuilder.Append(String.Format("AgentFinalUserID = {0} AND ", viewModel.Agent_Final));

            if (viewModel.CategorySituationID > 0)
                filterExpressionBuilder.Append(String.Format("CategorySituationID = {0} AND ", viewModel.CategorySituationID));

            if (viewModel.Record_Type_ID > 0)
                filterExpressionBuilder.Append(String.Format("Record_Type_ID = {0} AND ", viewModel.Record_Type_ID));

            if (viewModel.DOB_Month > 0)
                filterExpressionBuilder.Append(String.Format("DOB_Month = {0} AND ", viewModel.DOB_Month));

            if (viewModel.Carrier > 0)
                filterExpressionBuilder.Append(String.Format("CarrierID = {0} AND ", viewModel.Carrier));

            if (!String.IsNullOrEmpty(viewModel.Plan_Name))
                filterExpressionBuilder.Append(String.Format("Plan_Name = \"{0}\" AND ", viewModel.Plan_Name.Trim()));

            if (!String.IsNullOrEmpty(viewModel.Plan_Selected))
                filterExpressionBuilder.Append(String.Format("Plan_Selected = \"{0}\" AND ", viewModel.Plan_Selected.Trim()));

            if (!String.IsNullOrEmpty(viewModel.Product_Type))
                filterExpressionBuilder.Append(String.Format("ProductTypeID = {0} AND ", viewModel.Product_Type.Trim()));

            if (!String.IsNullOrEmpty(viewModel.PostalCode))
                filterExpressionBuilder.Append(String.Format("Customer_Zip = \"{0}\" AND ", viewModel.PostalCode.Trim()));

            if (!String.IsNullOrEmpty(viewModel.Country))
                filterExpressionBuilder.Append(String.Format("Customer_County = \"{0}\" AND ", viewModel.Country.Trim()));

            if (viewModel.Priority != 0)
            {
                if (viewModel.Priority == -1)
                    viewModel.Priority = 0;

                filterExpressionBuilder.Append(String.Format("Priority = {0} AND ", viewModel.Priority));
            }

            if (!String.IsNullOrEmpty(viewModel.LMSID))
                filterExpressionBuilder.Append(String.Format("LMSID = \"{0}\" AND ", viewModel.LMSID.Trim()));

            //------------Dates formating------------//
            if (!String.IsNullOrEmpty(viewModel.DateofBirthStart))
                filterExpressionBuilder.Append(String.Format("DOB >= \"{0}\" AND ", viewModel.DateofBirthStart.Trim()));

            if (!String.IsNullOrEmpty(viewModel.DateofBirthEnd))
                filterExpressionBuilder.Append(String.Format("DOB <= \"{0}\" AND ", viewModel.DateofBirthEnd.Trim()));

            if (!String.IsNullOrEmpty(viewModel.NextFollowUpStart))
                filterExpressionBuilder.Append(String.Format("NextFollowUp >= \"{0}\" AND ", viewModel.NextFollowUpStart.Trim()));
            if (!String.IsNullOrEmpty(viewModel.NextFollowUpEnd))
                filterExpressionBuilder.Append(String.Format("NextFollowUp <= \"{0}\" AND ", viewModel.NextFollowUpEnd.Trim()));

            if (!String.IsNullOrEmpty(viewModel.PolicyEffectiveStart))
                filterExpressionBuilder.Append(String.Format("EffectiveDateOrPurchaseDate >= \"{0}\" AND ", viewModel.PolicyEffectiveStart.Trim()));

            if (!String.IsNullOrEmpty(viewModel.PolicyEffectiveEnd))
                filterExpressionBuilder.Append(String.Format("EffectiveDateOrPurchaseDate <= \"{0}\" AND ", viewModel.PolicyEffectiveEnd.Trim()));

            if (!String.IsNullOrEmpty(viewModel.NoFollowUpsBefore))
                filterExpressionBuilder.Append(String.Format("NextFollowUp >= \"{0}\" AND ", viewModel.NoFollowUpsBefore.Trim()));

            //------------Dates formating------------//

            if (!String.IsNullOrEmpty(viewModel.FollowUpType))
                if (viewModel.FollowUpType != "-1")
                    filterExpressionBuilder.Append(String.Format("NextFollowUpType = \"{0}\" AND ", viewModel.FollowUpType.Trim()));

            if (!String.IsNullOrEmpty(viewModel.FollowUpUser))
                if (viewModel.FollowUpUser != "-1")
                    filterExpressionBuilder.Append(String.Format("NextFollowUpFor = {0} AND ", viewModel.FollowUpUser.Trim()));

            if (!String.IsNullOrEmpty(viewModel.RecordStatus))
                filterExpressionBuilder.Append(String.Format("LMS_Current_Record_Status = \"{0}\" AND ", viewModel.RecordStatus.Trim()));

            if (!String.IsNullOrEmpty(viewModel.CurrentDispostion))
                filterExpressionBuilder.Append(String.Format("LMS_Current_Disposition_Status = \"{0}\" AND ", viewModel.CurrentDispostion.Trim()));

            if (viewModel.SourceNumber != 0 && viewModel.SourceNumber != -1)
                filterExpressionBuilder.Append(String.Format("Source_Code_ID = {0} AND ", viewModel.SourceNumber));

            if (!String.IsNullOrEmpty(viewModel.Partner))
                filterExpressionBuilder.Append(String.Format("Partners = \"{0}\" AND ", viewModel.Partner.Trim()));

            if (filterExpressionBuilder.Length > 0)
                filterExpressionBuilder.Remove(filterExpressionBuilder.Length - 5, 5);

            return filterExpressionBuilder.ToString();
        }

        public CustomSearchViewModel cleanObject(CustomSearchViewModel viewModel)
        {
            try
            {
                if (!string.IsNullOrEmpty(viewModel.Agent_Status))
                    viewModel.Agent_Status = viewModel.Agent_Status.Replace("\"", string.Empty).Replace("Choose", string.Empty);

                if (!string.IsNullOrEmpty(viewModel.Category_Driver))
                    viewModel.Category_Driver = viewModel.Category_Driver.Replace("\"", string.Empty).Replace("Choose", string.Empty);

                if (!string.IsNullOrEmpty(viewModel.Agent_Group))
                    viewModel.Agent_Group = viewModel.Agent_Group.Replace("\"", string.Empty).Replace("Choose", string.Empty);

                if (!string.IsNullOrEmpty(viewModel.Agent_Type))
                    viewModel.Agent_Type = viewModel.Agent_Type.Replace("\"", string.Empty).Replace("Choose", string.Empty);

                if (!string.IsNullOrEmpty(viewModel.Plan_Name))
                    viewModel.Plan_Name = viewModel.Plan_Name.Replace("\"", string.Empty).Replace("-- Choose User --", string.Empty);

                if (!string.IsNullOrEmpty(viewModel.Plan_Selected))
                    viewModel.Plan_Selected = viewModel.Plan_Selected.Replace("\"", string.Empty).Replace("-- Choose User --", string.Empty);

                if (!string.IsNullOrEmpty(viewModel.Product_Type))
                    viewModel.Product_Type = viewModel.Product_Type.Replace("\"", string.Empty).Replace("0", string.Empty);

                if (viewModel.States != null)
                {
                    List<string> newList = new List<string>();
                    foreach (string item in viewModel.States.ToList())
                    {
                        if (item != "--Choose State--" && item != "All States")
                            newList.Add(item);
                    }
                    viewModel.States = newList.ToArray();
                }

                if (viewModel.Agent_Departed != null)
                {
                    var agentDepartedList = new List<int>();
                    foreach (int item in viewModel.Agent_Departed.ToList())
                    {
                        if (item > 0)
                            agentDepartedList.Add(item);
                    }
                    viewModel.Agent_Departed = agentDepartedList.ToArray();
                }

                if (!string.IsNullOrEmpty(viewModel.PostalCode))
                    viewModel.PostalCode = viewModel.PostalCode.Replace("\"", string.Empty).Replace("Choose", string.Empty);

                if (!string.IsNullOrEmpty(viewModel.Country))
                    viewModel.Country = viewModel.Country.Replace("\"", string.Empty).Replace("Choose", string.Empty);

                //------------Dates formating------------//
                if (!string.IsNullOrEmpty(viewModel.DateofBirthStart))
                    viewModel.DateofBirthStart = viewModel.DateofBirthStart.Replace("\"", string.Empty).Replace("Choose", string.Empty);

                if (!string.IsNullOrEmpty(viewModel.DateofBirthEnd))
                    viewModel.DateofBirthEnd = viewModel.DateofBirthEnd.Replace("\"", string.Empty).Replace("Choose", string.Empty);

                if (!string.IsNullOrEmpty(viewModel.NextFollowUpStart))
                    viewModel.NextFollowUpStart = viewModel.NextFollowUpStart.Replace("\"", string.Empty).Replace("Choose", string.Empty);

                if (!string.IsNullOrEmpty(viewModel.NextFollowUpEnd))
                    viewModel.NextFollowUpEnd = viewModel.NextFollowUpEnd.Replace("\"", string.Empty).Replace("Choose", string.Empty);

                if (!string.IsNullOrEmpty(viewModel.PolicyEffectiveStart))
                    viewModel.PolicyEffectiveStart = viewModel.PolicyEffectiveStart.Replace("\"", string.Empty).Replace("Choose", string.Empty);

                if (!string.IsNullOrEmpty(viewModel.PolicyEffectiveEnd))
                    viewModel.PolicyEffectiveEnd = viewModel.PolicyEffectiveEnd.Replace("\"", string.Empty).Replace("Choose", string.Empty);

                if (!string.IsNullOrEmpty(viewModel.NoFollowUpsBefore))
                    viewModel.NoFollowUpsBefore = viewModel.NoFollowUpsBefore.Replace("\"", string.Empty).Replace("Choose", string.Empty);

                //------------Dates formating------------//
                if (!string.IsNullOrEmpty(viewModel.FollowUpType))
                    viewModel.FollowUpType = viewModel.FollowUpType.Replace("\"", string.Empty).Replace("-- Choose Activity Type --", string.Empty);

                if (!string.IsNullOrEmpty(viewModel.FollowUpUser))
                    viewModel.FollowUpUser = viewModel.FollowUpUser.Replace("\"", string.Empty).Replace("-- Choose User --", string.Empty);

                if (!string.IsNullOrEmpty(viewModel.RecordStatus))
                    viewModel.RecordStatus = viewModel.RecordStatus.Replace("\"", string.Empty).Replace("-- Choose Status --", string.Empty);

                if (!String.IsNullOrEmpty(viewModel.CurrentDispostion))
                    viewModel.CurrentDispostion = viewModel.CurrentDispostion.Replace("\"", string.Empty).Replace("-- Choose Type --", string.Empty);

                if (!string.IsNullOrEmpty(viewModel.Partner))
                    viewModel.Partner = viewModel.Partner.Replace("\"", string.Empty).Replace("Choose", string.Empty);
            }
            catch { }


            return viewModel;
        }

        public List<MasterListLocal> GetJqueryGridList(List<vw_MasterFile_v2> masterList)
        {
            var masterItems = new List<MasterListLocal>();
            try
            {
                var masterItem = new MasterListLocal();

                foreach (LMS.Data.vw_MasterFile_v2 item in masterList)
                {
                    masterItem = new MasterListLocal
                    {
                        MasterFileID = item.MasterFileID,
                        GBSPolicyID = item.GBSPolicyID,
                        LMSID = item.LMSID,
                        AgentGroup = item.AgentGroup,
                        Original_Agent = item.Original_Agent,
                        Current_Record_Agent = item.Current_Record_Agent,
                        Plan_Agent = item.Plan_Agent,
                        Agent_Final = item.Agent_Final,
                        AgentFinalUserID = item.AgentFinalUserID,
                        Agent_Status = item.Agent_Status,
                        Agent_Type = item.Agent_Type,
                        Product_Type = item.Product_Type,
                        PolicyNumber = item.PolicyNumber,
                        SourceNumber = item.SourceNumber,
                        Source_Code_ID = item.Source_Code_ID,
                        FirstName = item.FirstName,
                        LastName = item.LastName,
                        Partners = item.Partners,
                        EffectiveDateOrPurchaseDate = item.EffectiveDateOrPurchaseDate,
                        Plan_Type = item.Plan_Type,
                        Carrier = item.Carrier,
                        CarrierID = item.CarrierID,
                        ProductTypeID = item.ProductTypeID,
                        Plan_Name = item.Plan_Name,
                        Plan_Selected = item.Plan_Selected,
                        DOB = item.DOB,
                        Age = item.Age,
                        Customer_Address = item.Customer_Address,
                        Customer_City = item.Customer_City,
                        Customer_State = item.Customer_State,
                        Customer_Zip = item.Customer_Zip,
                        Customer_County = item.Customer_County,
                        Customer_Email = item.Customer_Email,
                        Category_Driver = item.Category_Driver,
                        Category_Driver_Desc = item.Category_Driver_Desc,
                        Priority = item.Priority,
                        LMS_Current_Disposition_Status = item.LMS_Current_Disposition_Status,
                        NextFollowUp = item.NextFollowUp,
                        NextFollowUpType = item.NextFollowUpType,
                        NextFollowUpFor = item.NextFollowUpFor,
                        LatestActivityType = item.LatestActivityType,
                        LatestActivityAssignedTo = item.LatestActivityAssignedTo,
                        LatestActivityDate = item.LatestActivityDate,
                        LMS_Current_Record_Status = item.LMS_Current_Record_Status,
                        CategorySituation = item.CategorySituation,
                        CategorySituationDesc = item.CategorySituationDesc,
                        CategorySituationID = Convert.ToInt32(item.CategorySituationID),
                        Record_ID = item.Record_ID,
                        Plan_Transaction_ID = item.Plan_Transaction_ID,
                        Plan_Transaction_Status_ID = item.Plan_Transaction_Status_ID,
                        Plan_Transaction_Status_Description = item.Plan_Transaction_Status_Description,
                        EffectiveDateOrPurchaseDateMonth  = item.EffectiveDateOrPurchaseDateMonth
                    };
                    masterItems.Add(masterItem);
                }
            }
            catch { }

            return masterItems;
        }

        #endregion
    }
}
