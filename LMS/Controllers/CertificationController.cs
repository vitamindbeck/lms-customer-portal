using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Data;
using LMS.Data.Repository;
using LMS.Data;
using LMS.Helpers;


namespace LMS.Controllers
{
    [Authorize]
    [HandleError]
    [SessionExpireFilterAttribute]
    public class CertificationController : Controller
    {

        private IDropDownRepository _dropRepository;
        private IUserRepository _userRepository;
        private ICertificationRepository _certificationRepository;
        private ILicenseRepository _licenseRepository;
        private ICarrierRepository _carrierRepository;

        public CertificationController()
            : this(new DropDownRepository(), new UserRepository(), new CertificationRepository(), new LicenseRepository(), new CarrierRepository())
        {

        }

        public CertificationController(IDropDownRepository dropRepository, IUserRepository userRepository, ICertificationRepository certificationRepository, ILicenseRepository licenseRepository, ICarrierRepository carrierRepository)
        {
            _dropRepository = dropRepository;
            _userRepository = userRepository;
            _certificationRepository =certificationRepository ;
            _licenseRepository = licenseRepository;
            _carrierRepository = carrierRepository;
        }

        public ActionResult List(int? id)
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
           // int agentid = db.sp_Agent_List().ToList()[1].Agent_ID;
            if (id == null)
            {
                ViewData["Agent_List"] = new SelectList(_dropRepository.GetAgentAndAll(false), "Agent_ID", "Full_Name", 18);
                ViewData["CertificationListByAgent"] = db.sp_Certification_List_By_Agent(18).ToList();
            }
            else
            {
                ViewData["Agent_List"] = new SelectList(_dropRepository.GetAgentAndAll(false), "Agent_ID", "Full_Name", id);
                ViewData["CertificationListByAgent"] = db.sp_Certification_List_By_Agent(id).ToList();
            }

             ViewData["Carrier_List"] = new SelectList(_dropRepository.GetGBSCarriersAndAll(false), "Carrier_ID", "Name", 271);
             ViewData["CertificationListByCarrier"] = db.sp_Certification_List_By_Carrier(271).ToList();
       
          return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult List(FormCollection collection)
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

           if (!Boolean.Parse(Request.Form.GetValues("chkCarrierWise")[0]))
            {
                ViewData["Agent_List"] = new SelectList(_dropRepository.GetAgentAndAll(false), "Agent_ID", "Full_Name", Int32.Parse(collection["Agent"]));
                ViewData["CertificationListByAgent"] = db.sp_Certification_List_By_Agent(Int32.Parse(collection["Agent"])).ToList();
                ViewData["Carrier_List"] = new SelectList(_dropRepository.GetGBSCarriersAndAll(false), "Carrier_ID", "Name", Int32.Parse(collection["Carrier"]));
                ViewData["CertificationListByCarrier"] = db.sp_Certification_List_By_Carrier(Int32.Parse(collection["Carrier"])).ToList();
         
            }
           else
           {


               ViewData["Carrier_List"] = new SelectList(_dropRepository.GetGBSCarriersAndAll(false),"Carrier_ID", "Name", Int32.Parse(collection["Carrier"]));
               ViewData["CertificationListByCarrier"] = db.sp_Certification_List_By_Carrier(Int32.Parse(collection["Carrier"])).ToList();
               ViewData["Agent_List"] = new SelectList(_dropRepository.GetAgentAndAll(false), "Agent_ID", "Full_Name", Int32.Parse(collection["Agent"]));
               ViewData["CertificationListByAgent"] = db.sp_Certification_List_By_Agent(Int32.Parse(collection["Agent"])).ToList();
         

           }

            return View();
        }

        public ActionResult New(int? id)
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            Certification certification = new Certification();


           
           //Agent
           //ViewData["Agent_Name"] = new SelectList(_dropRepository.GetCertAgentOnly(true), "Agent_ID", "Full_Name", -1);

           if (id != null)
           {
               ViewData["Agent_Name"] = new SelectList(_dropRepository.GetCertAgentOnly(false).ToList(), "Agent_ID", "Full_Name", (int)id);
           }
           else
           {
               ViewData["Agent_Name"] = new SelectList(_dropRepository.GetCertAgentOnly(true).ToList(), "Agent_ID", "Full_Name");
           }


           //Carrier
           ViewData["Carrier"] = new SelectList(_dropRepository.GetGBSCarriers(true), "Carrier_ID", "Name", -1);
           ViewData["NonCarrier"] = new SelectList(_dropRepository.GetNonCarriers(true), "Non_Carrier_ID", "Non_Carrier_Name", -1);
           ViewData["Certification_Status"] = new SelectList(_dropRepository.GetCertificationStatus(true), "Certification_Status_ID", "Certification_Status", -1);
            ViewData["Certification_Type"] = new SelectList(_dropRepository.GetCertificationType(true), "Certification_Type_ID", "Certification_Type", -1);

            return View(certification);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult New(Certification certification, FormCollection collection)
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
           
            //Required field Validation
             if (!collection["ckOther"].Contains("true") && collection["Carrier_ID"] == "-1")
            {
                ModelState.AddModelError("Carrier_ID", "Carrier Cannot Be Blank");
                ModelState.SetModelValue("Carrier_ID", new ValueProviderResult(ValueProvider.GetValue("Carrier_ID").AttemptedValue, collection["Carrier_ID"], System.Globalization.CultureInfo.CurrentCulture));

            }
             else if (collection["ckOther"].Contains("true") && collection["Non_Carrier_ID"] == "-1")
             {
                 ModelState.AddModelError("Non_Carrier_ID", "Non-Carrier Cannot Be Blank");
                 ModelState.SetModelValue("Non_Carrier_ID", new ValueProviderResult(ValueProvider.GetValue("Non_Carrier_ID").AttemptedValue, collection["Non_Carrier_ID"], System.Globalization.CultureInfo.CurrentCulture));
             }
             if (collection["Registered_Date"] == "")
             {
                 ModelState.AddModelError("Registered_Date", "Registered Date Cannot Be Blank");
                 ModelState.SetModelValue("Registered_Date", new ValueProviderResult(ValueProvider.GetValue("Registered_Date").AttemptedValue, collection["Registered_Date"], System.Globalization.CultureInfo.CurrentCulture));
             }
            int agentID = Int32.Parse(collection["Agent_ID"]);
            if (agentID == 0)
            {
                ModelState.AddModelError("Agent_ID", "Agent Cannot Be Blank");
                ModelState.SetModelValue("Agent_ID", new ValueProviderResult(ValueProvider.GetValue("Agent_ID").AttemptedValue, collection["Agent_ID"], System.Globalization.CultureInfo.CurrentCulture));
            }

            //Get user details
            var userDetails = _userRepository.GetUser(agentID);
            certification.Agent_ID = Convert.ToInt32(agentID);
            if (ModelState.IsValid)
            {
                try
                {
                    certification.Agent_ID = agentID;
                    if (collection["Registered_Date"].Equals(""))
                    {
                        certification.Registered_Date = null;
                    }
                    else
                    {
                        certification.Registered_Date = DateTime.Parse(collection["Registered_Date"]);
                    }
                    if (collection["Complete_Date"].Equals(""))
                    {
                        certification.Complete_Date = null;
                    }
                    else
                    {
                        certification.Complete_Date = DateTime.Parse(collection["Complete_Date"]);
                    }
                    if (collection["Due_Date"].Equals(""))
                    {
                        certification.Due_Date = null;
                    }
                    else
                    {
                        certification.Due_Date = DateTime.Parse(collection["Due_Date"]);
                    }

                    if (collection["ckOther"].Contains("true"))
                    {
                        certification.Carrier_ID = -1;
                        certification.Non_Carrier_ID = Int32.Parse(collection["Non_Carrier_ID"]);
                    }
                    else
                    {
                        certification.Non_Carrier_ID = -1;
                        certification.Carrier_ID = Int32.Parse(collection["Carrier_ID"]);
                    }

                    certification.Certification_Link = collection["Certification_Link"];
                    certification.Certification_User_Name = StringHelper.EncryptString(Convert.ToString(collection["Cert_User_Name"]));
                    certification.Certification_Password = StringHelper.EncryptString(Convert.ToString(collection["Cert_Password"]));

                    //Certification Status

                    if ((collection["Certification_Status_ID"] != null) && (Int32.Parse(collection["Certification_Status_ID"]) > 0))
                    {
                        certification.Certification_Status_ID = Int32.Parse(collection["Certification_Status_ID"]);
                    }
                    else
                    {
                        certification.Certification_Status_ID = null;
                    }

                    //Certification Type

                    if ((collection["Certification_Type_ID"] != null) && (Int32.Parse(collection["Certification_Type_ID"]) > 0))
                    {
                        certification.Certification_Type_ID = Int32.Parse(collection["Certification_Type_ID"]);
                    }
                    else
                    {
                        certification.Certification_Type_ID = null;
                    }

                    
                   //Check if status is complete, put notified as null. Otherwise set notified as false for any other statuses.
                    if (certification.Certification_Status_ID == 1)
                    {
                        certification.Notified = null;
                    }
                    else
                    {
                        certification.Notified = false;
                    }

                    certification.Last_Updated_Date = DateTime.Now;
                    certification.Updated_By = _userRepository.GetUserByUserName(User.Identity.Name).User_ID;

                    db.Certifications.InsertOnSubmit(certification);
                    db.SubmitChanges();

                    return RedirectToAction("Details", new { id = certification.Certification_ID });

                }

                catch (Exception e)
                {
                    LMS.ErrorHandler.HandleError.EmailError(e);
                }
            }
            ViewData["Agent_Name"] = new SelectList(_dropRepository.GetCertAgentOnly(true), "Agent_ID", "Full_Name", certification.Agent_ID);
            ViewData["Carrier"] = new SelectList(_dropRepository.GetGBSCarriers(true), "Carrier_ID", "Name", certification.Carrier_ID);
            ViewData["NonCarrier"] = new SelectList(_dropRepository.GetNonCarriers(true), "Non_Carrier_ID", "Non_Carrier_Name", certification.Non_Carrier_ID);
            ViewData["Certification_Status"] = new SelectList(_dropRepository.GetCertificationStatus(true), "Certification_Status_ID", "Certification_Status",certification.Certification_Status_ID);
            ViewData["Certification_Type"] = new SelectList(_dropRepository.GetCertificationType(true), "Certification_Type_ID", "Certification_Type",certification.Certification_Type_ID);
            return View(certification);
        }
       
        //Edit
        public ActionResult Edit(int id)
        {

            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            Certification certification = _certificationRepository.GetCertificationByID((int)id);

            var agents = db.sp_Get_AgentsAgency(certification.Agent_ID).FirstOrDefault();
            ViewData["Agent_Name"] = new SelectList(_dropRepository.GetCertAgentOnly(true), "Agent_ID", "Full_Name", agents.Full_Name);         
            ViewData["Carrier"] = new SelectList(_dropRepository.GetCarriers(false), "Carrier_ID", "Name", certification.Carrier_ID.ToString());
            ViewData["Certification_Status"] = new SelectList(_dropRepository.GetCertificationStatus(false), "Certification_Status_ID", "Certification_Status", certification.Certification_Status_ID.ToString());
            ViewData["NonCarrier"] = new SelectList(_dropRepository.GetNonCarriers(true), "Non_Carrier_ID", "Non_Carrier_Name", certification.Non_Carrier_ID);
            ViewData["Certification_Type"] = new SelectList(_dropRepository.GetCertificationType(false), "Certification_Type_ID", "Certification_Type", certification.Certification_Type_ID.ToString());

            return View(certification);
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(int id, FormCollection collection)
        {

            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            Certification certification = db.Certifications.Where(a => a.Certification_ID == id).FirstOrDefault();


            if (!collection["ckOther"].Contains("true") && collection["Carrier_ID"] == "-1")
            {
                ModelState.AddModelError("Carrier_ID", "Carrier Cannot Be Blank");
                ModelState.SetModelValue("Carrier_ID", new ValueProviderResult(ValueProvider.GetValue("Carrier_ID").AttemptedValue, collection["Carrier_ID"], System.Globalization.CultureInfo.CurrentCulture));

            }
            else if (collection["ckOther"].Contains("true") && collection["Non_Carrier_ID"] == "-1")
            {
                ModelState.AddModelError("Non_Carrier_ID", "Non-Carrier Cannot Be Blank");
                ModelState.SetModelValue("Non_Carrier_ID", new ValueProviderResult(ValueProvider.GetValue("Non_Carrier_ID").AttemptedValue, collection["Non_Carrier_ID"], System.Globalization.CultureInfo.CurrentCulture));
            }
             if (collection["Registered_Date"] == "")
             {
                 ModelState.AddModelError("Registered_Date", "Registered Date Cannot Be Blank");
                 ModelState.SetModelValue("Registered_Date", new ValueProviderResult(ValueProvider.GetValue("Registered_Date").AttemptedValue, collection["Registered_Date"], System.Globalization.CultureInfo.CurrentCulture));
             }
            int agentID = Int32.Parse(collection["Agent_ID"]);

            if (agentID == 0)
            {
                ModelState.AddModelError("Agent_ID", "Agent Cannot Be Blank");
                ModelState.SetModelValue("Agent_ID", new ValueProviderResult(ValueProvider.GetValue("Agent_ID").AttemptedValue, collection["Agent_ID"], System.Globalization.CultureInfo.CurrentCulture));
            }
           if (ModelState.IsValid)
            {
                try
                {
                    certification.Agent_ID = agentID;
                    if (collection["Registered_Date"].Equals(""))
                    {
                        certification.Registered_Date = null;
                    }
                    else
                    {
                        certification.Registered_Date = DateTime.Parse(collection["Registered_Date"]);
                    }
                    if (collection["Complete_Date"].Equals(""))
                    {
                        certification.Complete_Date = null;
                    }
                    else
                    {
                        certification.Complete_Date = DateTime.Parse(collection["Complete_Date"]);
                    }
                    if (collection["Due_Date"].Equals(""))
                    {
                        certification.Due_Date = null;
                    }
                    else
                    {
                        certification.Due_Date = DateTime.Parse(collection["Due_Date"]);
                    }
                    if (collection["ckOther"].Contains("true"))
                    {
                        certification.Carrier_ID = -1;
                        certification.Non_Carrier_ID = Int32.Parse(collection["Non_Carrier_ID"]);
                    }
                    else
                    {
                        certification.Non_Carrier_ID = -1;
                        certification.Carrier_ID = Int32.Parse(collection["Carrier_ID"]);
                    }
                    certification.Certification_Link = collection["Certification_Link"];

                    certification.Certification_User_Name = StringHelper.EncryptString(Convert.ToString(collection["Cert_User_Name"]));
                    certification.Certification_Password = StringHelper.EncryptString(Convert.ToString(collection["Cert_Password"])); 
                    certification.Special_Instructions = collection["Special_Instructions"];

                    
                    if((collection["Certification_Status_ID"] != null) && (Int32.Parse(collection["Certification_Status_ID"])> 0))
                    {
                        certification.Certification_Status_ID = Int32.Parse(collection["Certification_Status_ID"]);
                    }
                    else
                    {
                        certification.Certification_Status_ID = null;
                    }

                    if ((collection["Certification_Type_ID"] != null) && (Int32.Parse(collection["Certification_Type_ID"]) > 0))
                    {
                        certification.Certification_Type_ID = Int32.Parse(collection["Certification_Type_ID"]);
                    }
                    else
                    {
                        certification.Certification_Type_ID = null;
                    }


                   //Check if status is complete, put notified as null. Otherwise set notified as false for any other statuses.
                    if (certification.Certification_Status_ID == 1)
                    {
                        certification.Notified = null;
                    }
                    else
                    {
                        certification.Notified = false;
                    }

                    certification.Last_Updated_Date = DateTime.Now;
                    certification.Updated_By = _userRepository.GetUserByUserName(User.Identity.Name).User_ID;

                    db.SubmitChanges();
                    return RedirectToAction("Details", new { id = certification.Certification_ID});
                }
                catch (Exception e)
                {
                   LMS.ErrorHandler.HandleError.EmailError(e);
                }

            }

            ViewData["Agent_Name"] = new SelectList(_dropRepository.GetCertAgentOnly(true), "Agent_ID", "Full_Name", certification.Agent_ID);
            ViewData["Carrier"] = new SelectList(_dropRepository.GetGBSCarriers(true), "Carrier_ID", "Name", certification.Carrier_ID);
            ViewData["Certification_Status"] = new SelectList(_dropRepository.GetCertificationStatus(true), "Certification_Status_ID", "Certification_Status", certification.Certification_Status_ID);
            ViewData["Certification_Type"] = new SelectList(_dropRepository.GetCertificationType(true), "Certification_Type_ID", "Certification_Type", certification.Certification_Type_ID);
            ViewData["NonCarrier"] = new SelectList(_dropRepository.GetNonCarriers(true), "Non_Carrier_ID", "Non_Carrier_Name", certification.Non_Carrier_ID);

            return View(certification);
       }


        //Certification Details Page
        public ActionResult Details(int? id)
        {

            Certification certification = _certificationRepository.GetCertificationByID((int)id);
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
           //Get Agent Name
            var agents = db.sp_Get_AgentsAgency(certification.Agent_ID).FirstOrDefault();
            ViewData["Agent_Name"] = agents.Full_Name;

            //Get Other Details
            var carrierDetails = db.sp_Get_Certification_Details(certification.Certification_ID).FirstOrDefault();

            //Carrier
            ViewData["Carrier"] = carrierDetails.Name;
            ViewData["NonCarrier"] = carrierDetails.Non_Carrier_Name;
            //Status 
            ViewData["Certification_Status"] = carrierDetails.Certification_Status;

            //Type
            ViewData["Certification_Type"] = carrierDetails.Certification_Type;

          
            //Username
            if (certification.Updated_By != null)
            {
                ViewData["UserName"] = _userRepository.GetUser((int)certification.Updated_By).First_Name + " " + _userRepository.GetUser((int)certification.Updated_By).Last_Name;
            }
            else
            {
                ViewData["UserName"] = "";
            }
         

            return View(certification);

        }
        //Export to Excel
        public ActionResult Export(int AgentID, string Chk, int CarrierID)
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            DataTable table;
            DataColumn column;
            DataRow row;
            DataSet dataset;
            table = new DataTable("CertificationListByAgent");
            
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Agent";
            column.ReadOnly = true;
            table.Columns.Add(column);
            
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Carrier/Non-carrier";
            column.ReadOnly = true;
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "User Name";
            column.ReadOnly = true;
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Password";
            column.ReadOnly = true;
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Web Site Link";
            column.ReadOnly = true;
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Registered Date";
            column.ReadOnly = true;
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Date Completed";
            column.ReadOnly = true;
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Due Date";
            column.ReadOnly = true;
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Certification Status";
            column.ReadOnly = true;
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Special Instructions";
            column.ReadOnly = true;
            table.Columns.Add(column);
           
            if (String.IsNullOrEmpty(Chk) || (!String.IsNullOrEmpty(Chk) && !Chk.Contains("true")))
            {
                dataset = new DataSet();
                dataset.Tables.Add(table);
                List<sp_Certification_List_By_AgentResult> rs = db.sp_Certification_List_By_Agent(AgentID).ToList();
                for (int i = 0; i < rs.Count; i++)
                {
                    row = table.NewRow();

                    row["Agent"] = rs[i].First_Name + " " + rs[i].Last_Name;

                    row["Carrier/Non-carrier"] = rs[i].Name;

                    row["User Name"] = rs[i].Certification_User_Name;

                    row["Password"] = rs[i].Certification_Password;

                    row["Web Site Link"] = rs[i].Certification_Link;

                  
                    if (rs[i].Registered_Date != null)
                    {
                        row["Registered Date"] = rs[i].Registered_Date.Value.ToShortDateString();
                    }
                    else
                    {
                        row["Registered Date"] = "";
                    }
                    if (rs[i].Complete_Date  != null)
                    {
                        row["Date Completed"] = rs[i].Complete_Date.Value.ToShortDateString();
                    }
                    else
                    {
                        row["Date Completed"] = "";
                    }
                    if (rs[i].Due_Date != null)
                    {
                        row["Due Date"] = rs[i].Due_Date.Value.ToShortDateString();
                    }
                    else
                    {
                        row["Due Date"] = "";
                    }

                    row["Certification status"] = rs[i].Certification_Status;
                    row["Special Instructions"] = rs[i].Special_Instructions;
                    table.Rows.Add(row);
                }
                if (rs.Count > 0)
                {
                    RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
                    objExport.ExportDetails(table, RKLib.ExportData.Export.ExportFormat.Excel, "CertificationByAgent.xls");
                }
                else
                {
                    return RedirectToAction("RecordOutput", "Certification");
                }

            }
            else
            {

                dataset = new DataSet();
                dataset.Tables.Add(table);
                List<sp_Certification_List_By_CarrierResult> rs = db.sp_Certification_List_By_Carrier(CarrierID).ToList();
                for (int i = 0; i < rs.Count; i++)
                {
                    row = table.NewRow();
                    row["Agent"] = rs[i].First_Name + " " + rs[i].Last_Name;

                    row["Carrier/Non-carrier"] = rs[i].Name;

                    row["User Name"] = rs[i].Certification_User_Name;

                    row["Password"] = rs[i].Certification_Password;

                    row["Web Site Link"] = rs[i].Certification_Link;

                    if (rs[i].Registered_Date != null)
                    {
                        row["Registered Date"] = rs[i].Registered_Date.Value.ToShortDateString();
                    }
                    else
                    {
                        row["Registered Date"] = "";
                    }
                    if (rs[i].Complete_Date != null)
                    {
                        row["Date Completed"] = rs[i].Complete_Date.Value.ToShortDateString();
                    }
                    else
                    {
                        row["Date Completed"] = "";
                    }
                    if (rs[i].Due_Date != null)
                    {
                        row["Due Date"] = rs[i].Due_Date.Value.ToShortDateString();
                    }
                    else
                    {
                        row["Due Date"] = "";
                    }

                    row["Certification status"] = rs[i].Certification_Status;
                    row["Special Instructions"] = rs[i].Special_Instructions;
                    table.Rows.Add(row);
                }
                if (rs.Count > 0)
                {
                    RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
                    objExport.ExportDetails(table, RKLib.ExportData.Export.ExportFormat.Excel, "CertificationByCarrier.xls");
                }
                else
                {
                    return RedirectToAction("RecordOutput", "Certification");
                }
            }

            return View();
        }

        //RecordOutput
        public ActionResult RecordOutput()
        {
            return View();
        }

    }
}
