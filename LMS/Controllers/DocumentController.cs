using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using LMS.Data;
using LMS.Data.Repository;
using System.IO;
using System.Net.Mail;
using System.Text;
using LMS.Helpers;

namespace LMS.Controllers
{
    public class DocumentController : Controller
    {
        //
        // GET: /Document/
        LMSDataContext _DB = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
        DropDownRepository dropRepository = new DropDownRepository();

        public ActionResult List(int id)
        {
            var docs = _DB.Plan_Documents.Where(p => p.Plan_ID == id).OrderBy(p => p.Title);
            return View(docs);
        }

        public ActionResult Edit(int id)
        {
            Plan_Document doc = _DB.Plan_Documents.Where(pd => pd.Plan_Document_ID == id).FirstOrDefault();

            ViewData["State_ID"] = new SelectList(dropRepository.GetStates(false), "State_ID", "Name", doc.State_ID);

            return View(doc);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(int id, FormCollection collection)
        {
            Plan_Document doc = _DB.Plan_Documents.Where(pd => pd.Plan_Document_ID == id).FirstOrDefault();

            UpdateModel(doc);

            _DB.SubmitChanges();

            return RedirectToAction("List", new { id = doc.Plan_ID });
        }

        public ActionResult Create(int id)
        {
            Plan_Document doc = new Plan_Document();

            doc.Plan_ID = id;

            ViewData["Plan_ID"] = id;

            var states = _DB.States.Where(s => s.Abbreviation.Length > 0);
            ViewData["State_ID"] = new SelectList(states, "State_ID", "Name");

            return View(new Plan_Document());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create(FormCollection collection)
        {
            int planID = Convert.ToInt32(collection["Plan_ID"]);
            int DocumentTitle = 0;
            int Count = 0;

            if (collection["State_ID"].Length < 1)
                ModelState.AddModelError("State_ID", "Must Select State");

            if (ModelState.IsValid)
            {
                var stateList = collection["State_ID"].Split(',');

                foreach (string state in stateList)
                {
                    Plan_Document doc = new Plan_Document();

                    int displayorder = _DB.Plan_Documents.Where(pd => pd.Plan_ID == doc.Plan_ID).Count();

                    doc.Plan_ID = Convert.ToInt32(collection["Plan_ID"]);
                    doc.Display_Order = displayorder;

                    doc.Active = collection["Active"].StartsWith("true");
                    doc.State_ID = Convert.ToInt32(state);
                    doc.Title = collection["Title"];
                    doc.Location = "";
                    doc.User_ID = UserRepository.GetUser(User.Identity.Name).User_ID;
                    _DB.Plan_Documents.InsertOnSubmit(doc);

                    _DB.SubmitChanges();

                    

                    Count++;

                    if (Count < 2)
                    {
                        DocumentTitle = doc.Plan_Document_ID;
                    }

                    doc.Location = DocumentTitle + ".pdf";

                    _DB.SubmitChanges();
                }

                try
                {
                    var r = new List<ViewDataUploadFilesResult>();

                    foreach (string file in Request.Files)
                    {
                        HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                        if (hpf.ContentLength == 0)
                            continue;

                        string savedFileName = Path.Combine(
                          AppDomain.CurrentDomain.BaseDirectory + @"documents",
                          DocumentTitle.ToString() + ".pdf");

                        hpf.SaveAs(savedFileName);

                    }
                }
                catch (Exception)
                { }


                return RedirectToAction("List", new { id = Convert.ToInt32(collection["Plan_ID"]) });
            }
            else
            {
                return RedirectToAction("Create", planID);
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Delete(int id)
        {
            Plan_Document doc = _DB.Plan_Documents.Where(pd => pd.Plan_Document_ID == id).FirstOrDefault();
            int planid = doc.Plan_ID;

            _DB.Plan_Documents.DeleteOnSubmit(doc);
            _DB.SubmitChanges();

            return RedirectToAction("List", new { id = planid });

        }

        public class ViewDataUploadFilesResult
        {
            public string Name { get; set; }
            public int Length { get; set; }
        }

        public ActionResult GetAll()
        {
            var docs = from d in _DB.Plan_Documents
                       select new { d.Plan_ID, d.Plan_Document_ID, d.Title };

            return Json(docs);

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Email(FormCollection collection)
        {
            bool rValue = true;
            LMSDataContext DB = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            List<string> filenames = new List<string>();

            Plan_Transaction transaction = DB.Plan_Transactions.Where(pt => pt.Plan_Transaction_ID == Convert.ToInt32(collection["Plan_Transaction_ID"])).FirstOrDefault();
            
            if (transaction != null )
            {
                Record record = DB.Records.Where(r => r.Record_ID == transaction.Record_ID).FirstOrDefault();
                LMS.Data.User user = UserRepository.GetUser(User.Identity.Name);
                try
                {
                    
                    SmtpClient client = new SmtpClient("192.168.1.202");
                    MailMessage msg = new MailMessage();
                    msg.To.Add(collection["ToAddress"]);
                    msg.Bcc.Add(new MailAddress(user.Email_Address));
                    msg.From = new MailAddress(collection["FromAddress"]);
                    msg.Subject = collection["Subject"];
                    msg.Body = collection["Body"].Replace("\r\n","<br />");
                    msg.IsBodyHtml = true;

                    try
                    {
                        foreach (string id in collection["Plan_Document_ID"].Split(','))
                        {
                            Plan_Document document = DB.Plan_Documents.Where(pd => pd.Plan_Document_ID == Convert.ToInt32(id)).FirstOrDefault();

                            if (document != null)
                            {
                                string oldFileName = Server.MapPath("/") + string.Format(@"/Documents/{0}", document.Location);
                                string fileName = Server.MapPath("/") + string.Format(@"Documents\TEMP\{0}.pdf", document.Title + "_" + document.Plan_Document_ID.ToString());
                                
                                FileInfo f = new FileInfo(oldFileName);

                                f.CopyTo(fileName, true);
                                f = null;
                                Attachment attachment = new Attachment(fileName);
                                msg.Attachments.Add(attachment);
                                filenames.Add(fileName);
                            }
                        }
                    }
                    catch (Exception ex) 
                    {
                        LMS.ErrorHandler.HandleError.EmailError(ex);
                    }

                    if (msg.Attachments.Count > 0)
                    {
                        client.Send(msg);

                        Record_Activity tempAct = new Record_Activity();

                        tempAct.Created_By = user.User_ID;
                        tempAct.Created_On = DateTime.Now;
                        tempAct.User_ID = user.User_ID;
                        tempAct.Record_ID = record.Record_ID;
                        tempAct.Due_Date = DateTime.Now;
                        tempAct.Actual_Date = DateTime.Now;
                        tempAct.Activity_Type_ID = 3;
                        tempAct.Notes = StringHelper.EncryptString("Sent Email From Plan Transaction");
                        tempAct.Plan_Transaction_ID = transaction.Plan_Transaction_ID;
                        tempAct.Priority_Level = record.Priority_Level;
                        tempAct.Record_Activity_Status_ID = 13;

                        LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

                        db.Record_Activities.InsertOnSubmit(tempAct);

                        db.SubmitChanges();
                    }

                }
                catch (Exception ex)
                {
                    LMS.ErrorHandler.HandleError.EmailError(ex);
                }
            }

            return RedirectToAction(Request.UrlReferrer.PathAndQuery.Split('/')[2], Request.UrlReferrer.PathAndQuery.Split('/')[1], new { id = transaction.Plan_Transaction_ID });

        }


        public ActionResult AjaxGet(int id, int state)
        {
            var docs = from d in _DB.Plan_Documents
                       where d.Plan_ID == id && d.State_ID == state
                       select new { d.Plan_ID, d.Plan_Document_ID, d.Title };

            return Json(docs);
        }

        public ActionResult EmailView(int RecordID, int PlanTransactionID, int PlanID, int StateID)
        {
            Record record = _DB.Records.Where(r=> r.Record_ID == RecordID).FirstOrDefault();
            User user = UserRepository.GetUser(User.Identity.Name);

            var docs = from d in _DB.Plan_Documents
                       where d.Plan_ID == PlanID && d.State_ID == StateID
                       select d;

            LMS.Models.EmailDocumentModel emailModel = new LMS.Models.EmailDocumentModel();
            emailModel.Body = GetEmailBody(record,user);
            emailModel.FromAddress = user.Email_Address;
            emailModel.ToAddress = record.Email_Address;
            emailModel.Subject = "Plan Documents";
            emailModel.Plan_Transaction_ID = PlanTransactionID;
            emailModel.Documents = docs;

            return PartialView("DocumentEmail", emailModel);
        }

        public ActionResult DocumentList(int id, int state)
        {
            var docs = from d in _DB.Plan_Documents
                       where d.Plan_ID == id && d.State_ID == state
                       select d;

            ViewData["plandocs"] = docs.ToList();

            return PartialView("documentsdropdown");
        }

        private string GetEmailBody(Record record, User user)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(string.Format("Dear {0} {1},", record.First_Name, record.Last_Name));
            sb.Append(System.Environment.NewLine);
            sb.Append(System.Environment.NewLine);
            sb.Append(string.Format("Attached are the documents discussed for your review.  Applications, once completed and signed, can either be faxed to my attention at 480-897-9599 or emailed to me at {0}.  Of course, I�m here to answer any questions you may have.", user.Email_Address));
            sb.Append(System.Environment.NewLine);
            sb.Append(System.Environment.NewLine);
            sb.Append("Thank you for the opportunity to be of service.");
            sb.Append(System.Environment.NewLine);
            sb.Append(System.Environment.NewLine);
            sb.Append("Sincerely,");
            sb.Append(System.Environment.NewLine);
            sb.Append(System.Environment.NewLine);
            sb.Append(user.First_Name + " " + user.Last_Name);
            sb.Append(System.Environment.NewLine);
            sb.Append("Longevity Alliance, Inc.");
            sb.Append(System.Environment.NewLine);
            sb.Append("5530 W. Chandler Blvd.");
            sb.Append(System.Environment.NewLine);
            sb.Append("Chandler, AZ 85226");
            sb.Append(System.Environment.NewLine);
            sb.Append(user.Email_Address);
            sb.Append(System.Environment.NewLine);
            sb.Append(string.Format("1-800-713-6610 {0}", user.Extension));
            return sb.ToString();
        }
    }
}
