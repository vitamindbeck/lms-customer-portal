using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using LMS.Data;

namespace LMS.Controllers
{
    public class ProcessController : Controller
    {
        //
        // GET: /Process/

        public bool AddressLabel(int id)
        {
            var Success = true;
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            
            Dymo_User dymouser = db.Dymo_Users.Where(du => du.User_Name == LMS.Data.Repository.UserRepository.GetUserName(User.Identity.Name)).FirstOrDefault();
            if (dymouser != null)
            {
                try
                {
                    Record client = db.Records.Where(r => r.Record_ID == id).FirstOrDefault();
                    string address = string.Format("{0} {1}\r\n", client.First_Name.ToUpper(), client.Last_Name.ToUpper());
                    address += string.Format("{0}\r\n", client.Address_1);
                    address += string.Format("{0}\r\n", client.Address_2);
                    address += string.Format("{0}, {1} {2}\r\n", client.City, client.State.Abbreviation, client.Zipcode);
                    //LMS.DymoLabelPrinter.Printer.PrintAddress(address);
                }
                catch (Exception ex)
                {
                    string env = "Prod";
                    if(db.Connection.ConnectionString.ToLower().Contains("ladev01"))
                    {
                        env = "Dev";
                    }
                    else if(db.Connection.ConnectionString.ToLower().Contains("ladev03"))
                    {
                        env = "Test";
                    }
                    ErrorHandler.HandleError.Email("IT@longevityalliance.com", "errors@longevityalliance.com", "LMS Label Printer Error (" + env + ")", ex.Message + Environment.NewLine + ex.StackTrace);
                }

            }
            else
            {
                try
                {
                    LMSDataContext DB = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

                    Process process = new Process();

                    process.Process_Type_ID = 4;
                    process.Value = id;

                    DB.Processes.InsertOnSubmit(process);
                    DB.SubmitChanges();

                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            return Success;
        }

    }
}
