using System;
using System.Collections.Generic;
using System.Linq;

using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Text.RegularExpressions;
using LMS.Data.Repository;
using LMS.Data;
using LMS.Helpers;
using System.Net.Mail;
using System.Data.Linq;

namespace LMS.Controllers
{

    [Authorize]
    [HandleError]
    [SessionExpireFilterAttribute]
    public class SuperAdminController : Controller
    {
        private IRecordRepository _repository;
        private IUserRepository _userRepository;
        private IDropDownRepository _dropRepository;
        private IPlanTransactionRepository _planTransactionRepository;
        private ISourceCodeRepository _sourceRepository;
        private static Regex isGuid = new Regex(@"^(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}$", RegexOptions.Compiled);
        
        //
        // GET: /SuperAdmin/
        public SuperAdminController()
            : this(new RecordRepository(), new UserRepository(), new DropDownRepository(), new PlanTransactionRepository(), new SourceCodeRepository())
        {

        }
        public SuperAdminController(IRecordRepository repository, IUserRepository userrepository, IDropDownRepository droprepository, IPlanTransactionRepository planTranRepo, ISourceCodeRepository sourceRepository)
        {
            _repository = repository;
            _userRepository = userrepository;
            _dropRepository = droprepository;
            _planTransactionRepository = planTranRepo;
            _sourceRepository = sourceRepository;
        }


        public ActionResult Index()
        {
            return View();
        }

        //************************************
        [CompressFilter]

        //Main Page with Add, modify and delete link for admin to navigate
        public ActionResult LMSUser()
        {

            return View();

        }
 //Page where user account can be updated.
        public ActionResult UpdateUser(int id)
        {
            User usrDetails = _userRepository.GetUser(id);


            ViewData["First_Name"] = usrDetails.First_Name;
            ViewData["Last_Name"] = usrDetails.Last_Name;
            ViewData["User_Name"] = usrDetails.User_Name;

            //Get Security_Level
            var secLevel = _dropRepository.GetSecurityLevels(false);
            ViewData["Security_Level_ID"] = new SelectList(secLevel, "Security_Level_ID", "Description", usrDetails.Security_Level_ID);

            return View(usrDetails);
        }


     
        //Create New User account
        public ActionResult CreateUser()
        {
            LMS.Data.User user = new User();

            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

            //Get Group
            var grp = _dropRepository.GetGroups(true);
            ViewData["Group_ID"] = new SelectList(grp, "Group_ID", "Description", user.Group_ID);

            //Get Security
            var secutrityLevel = _dropRepository.GetSecurityLevels(true);
            ViewData["Security_Level_ID"] = new SelectList(secutrityLevel, "Security_Level_ID", "Description", user.Security_Level_ID);

            //Get Pod Id
            var podId = _dropRepository.GetPods(true);
            ViewData["Pod_ID"] = new SelectList(podId, "Pod_ID", "Pod_Name", user.Pod_ID);


            //Get Request Page Accessibility
            var pageAccess = _dropRepository.GetUserAccess(true);
            ViewData["Sales_Group_ID"] = new SelectList(pageAccess, "Key", "Value", user.Sales_Group_ID); 

           return View(user);

        }

        //Saves new user details when saved
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CreateUser(User user, FormCollection collection)
        {
           
            //Check for Last name
            if (user.Last_Name.Trim().Length < 1)
                ModelState.AddModelError("Last_Name", "Last Name can not be blank");

            //Check for First name
            if (user.First_Name.Trim().Length < 1)
                ModelState.AddModelError("First_Name", "First Name can not be blank");


            //Check for User name
            if (user.User_Name.Trim().Length < 1)
                ModelState.AddModelError("User_Name", "User Name can not be blank");

            //Check for Group 
            if (user.Group_ID < 1)
                ModelState.AddModelError("Group_ID", "Group can not be left unselected.");

            //Check for Security Level
            if (user.Security_Level_ID < 1)
                ModelState.AddModelError("Security_Level_ID", "Security Level can not be left unselected.");

            // If Valid Save
            if (ModelState.IsValid)
            {

                try
                {
                    bool isAssignHealth = user.Auto_Assignment;
                    bool isEmpAccount = user.In_AD;
                    bool isAssignLife = user.Auto_Assignment_Life;


                    User recUser = new User();
                    recUser.First_Name = user.First_Name;
                    recUser.Last_Name = user.Last_Name;
                    recUser.User_Name = user.User_Name;
                    recUser.Password = "123456";
                    recUser.Group_ID = user.Group_ID;
                    recUser.Security_Level_ID = user.Security_Level_ID;
                    recUser.Phone = user.Phone;
                    recUser.Extension = user.Extension;
                    recUser.Email_Address = user.Email_Address;
                    recUser.Active = true;
                    recUser.Recommend_Line = "";
                    recUser.Auto_Assignment_Life_Pointer = false;
                    recUser.Auto_Assignment_Pointer = false;
                    recUser.Assignment_Indicator = false;
                    user.Assign_To = 35;
                    user.Auto_Territory_Indicator = true;
                    if (isAssignHealth == true)
                    {
                        recUser.Auto_Assignment = true;
                    }
                    else
                    {
                        recUser.Auto_Assignment = false;
                    }
                    if (isAssignLife == true && recUser.Security_Level_ID == 3)
                    {
                        recUser.Auto_Assignment_Life = true;
                    }
                    else
                    {
                        recUser.Auto_Assignment_Life = false;
                    }

                    if (isEmpAccount == true)
                    {
                        recUser.In_AD = true;
                    }
                    else
                    {
                        recUser.In_AD = false;
                    }

                    if (user.Sales_Group_ID < 0)
                    {
                        recUser.Sales_Group_ID = null;
                    }
                    else if (user.Sales_Group_ID == 1)
                    {
                        recUser.Sales_Group_ID = 1;
                    }
                    else
                    {
                        recUser.Sales_Group_ID = 0;
                    }



                    recUser.Lead_Cap = 99999;
                    recUser.Online_Status_ID = 2;
                    recUser.SessionId = null;
                    recUser.Pod_ID = user.Pod_ID;


                    _userRepository.Update(recUser);

                    _userRepository.Save();

                    //Send Email to Admin
                    SendEmail(recUser);

                    //Confirmation Message 
                    ViewData["Message"] = "New user profile is created. Please check your email for details.";

                }
                catch (Exception ex)
                {
                    LMS.ErrorHandler.HandleError.EmailError(ex);
                }
            }

           
            //Group ID
            ViewData["Group_ID"] = new SelectList(_dropRepository.GetGroups(true), "Group_ID", "Description", user.Group_ID);

            //Security Level ID
            ViewData["Security_Level_ID"] = new SelectList(_dropRepository.GetSecurityLevels(true), "Security_Level_ID", "Description", user.Security_Level_ID);

            //Access to Request App Page
            ViewData["Sales_Group_ID"] = new SelectList(_dropRepository.GetUserAccess(true), "Key", "Value", user.Sales_Group_ID);
            
            //Pod Id
            ViewData["Pod_ID"] = new SelectList(_dropRepository.GetPods(true), "Pod_ID", "Pod_Name", user.Pod_ID);


            return View(user);
        }

        //Send email to notify user that account has been created.
        private void SendEmail(User usr)
        {

            //GetLooge in user credentials
            LMS.Data.User loguser = _userRepository.GetUserFromADName(User.Identity.Name.Replace(@"LONGEVITY\", ""));

            string loggedInUserEmail = loguser.Email_Address;

            //create mail object
            MailMessage msg = new MailMessage();
            msg.Subject = "New User Information";


            string body = "New User Account has been created.\n\n";
            body += "First Name: " + usr.First_Name + "\n";
            body += "Last Name: " + usr.Last_Name + "\n";
            body += "User Name: " + usr.User_Name + "\n";
            body += "Email: " + usr.Email_Address + "\n\n"; 
            
            body += "Thanks,\n";

            body += "IT";

            msg.Body = body;


            msg.To.Add(new MailAddress(loggedInUserEmail));

            msg.From = new MailAddress("it@longevityalliance.com");

            SmtpClient smt = new SmtpClient("192.168.1.202");

            smt.Send(msg);
        }

       
         //Page where user account can be deleted or edited.
        public ActionResult ManageUser()
        {
             LMS.Data.User user = new User();

            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

           
            //Get User names
            var usrNames = _dropRepository.GetUserNames(true);
            ViewData["User_ID"] = new SelectList(usrNames, "User_ID", "User_Name", user.User_ID);

           
          
            return View();
        }


        //Search for user based on username and return the details on the page.
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManageUser(int User_ID)
        {
           //Get Record
            User user = _userRepository.GetUser(User_ID);

            if (User_ID < 1 )
            {
                ModelState.AddModelError("User_ID", "Please select user name before searching");
            }
           
           if (ModelState.IsValid)
            {
                try
                {
                    var usrList = _userRepository.UserSearch(User_ID);
                    ViewData["UserSearchData"] = usrList;
                      
                }
                catch (Exception ex)
                {
                    LMS.ErrorHandler.HandleError.EmailError(ex);
                }
            }
           ViewData["User_ID"] = new SelectList(_dropRepository.GetUserNames(true), "User_ID", "User_Name", -1);
            return View();
        }

        
        //Deletes User when Delete button is clicked on ManageUser page.
        public ActionResult Delete(int id)
        {
            try
            {

                User usr = _userRepository.GetUser(id);
                
                //Do not delete but set the active field to 0
                //_userRepository.Delete(usr);

                usr.Active = false;

                UpdateModel(usr);
               
                _userRepository.Save();

                //GetLooge in user credentials
                LMS.Data.User loguser = _userRepository.GetUserFromADName(User.Identity.Name.Replace(@"LONGEVITY\", ""));
                string loggedInUserEmail = loguser.Email_Address;

                _userRepository.SendEmailDelete(usr, loggedInUserEmail);

                if (usr.Active == false)
                {

                    ViewData["UserSearchData"] = null;

                }
               
            }
            catch (Exception ex)
            {
                LMS.ErrorHandler.HandleError.EmailError(ex);
            }

            return RedirectToAction("ManageUser","SuperAdmin");
        }

        //Update User information 
        public ActionResult EditUser(int id)
        {

            User usrDetails = _userRepository.GetUser(id);


            ViewData["First_Name"] = usrDetails.First_Name;
            ViewData["Last_Name"] = usrDetails.Last_Name;
            ViewData["User_Name"] = usrDetails.User_Name;

            //Get Security_Level
            var secLevel = _dropRepository.GetSecurityLevels(false);
            ViewData["Security_Level_ID"] = new SelectList(secLevel, "Security_Level_ID", "Description", usrDetails.Security_Level_ID);


            //Get Group
            var grp = _dropRepository.GetGroups(false);
            ViewData["Group_ID"] = new SelectList(grp, "Group_ID", "Description", usrDetails.Group_ID);

            //Get Pod
            var pod = _dropRepository.GetPods(false);
            ViewData["Pod_ID"] = new SelectList(pod, "Pod_ID", "Pod_Name", usrDetails.Pod_ID);

            ViewData["Phone"] = usrDetails.Phone;
            ViewData["Extension"] = usrDetails.Extension;
            ViewData["Email_Address"] = usrDetails.Email_Address;

            var pageAccess = _dropRepository.GetUserAccess(true);
            ViewData["Sales_Group_ID"] = new SelectList(pageAccess, "Key", "Value", usrDetails.Sales_Group_ID); 
            
            ViewData["In_AD"] = usrDetails.In_AD;
            ViewData["Auto_Assignment"] = usrDetails.Auto_Assignment;
            ViewData["Auto_Assignment_Life"] = usrDetails.Auto_Assignment_Life;

            return View(usrDetails);
          
        }

        //Save update information for the selected user
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditUser(int id, FormCollection collection)
        {
            //Data Context
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

            //Get Record
            User user = _userRepository.GetUser(id);

            // Update Record From Web Page Data
            UpdateModel(user);

            //Check for Last name
            if (user.Last_Name.Trim().Length < 1)
                ModelState.AddModelError("Last_Name", "Last Name can not be blank");

            //Check for First name
            if (user.First_Name.Trim().Length < 1)
                ModelState.AddModelError("First_Name", "First Name can not be blank");


            //Check for User name
            if (user.User_Name.Trim().Length < 1)
                ModelState.AddModelError("User_Name", "User Name can not be blank");

            //Check for Group 
            if (user.Group_ID < 1)
                ModelState.AddModelError("Group_ID", "Group can not be left unselected.");

            //Check for Security Level
            if (user.Security_Level_ID < 1)
                ModelState.AddModelError("Security_Level_ID", "Security Level can not be left unselected.");


            if (ModelState.IsValid)
            {
                try
                {
                    UpdateModel(user);
                    _userRepository.Save();


                    //GetLooge in user credentials
                    LMS.Data.User loguser = _userRepository.GetUserFromADName(User.Identity.Name.Replace(@"LONGEVITY\", ""));

                    string loggedInUserEmail = loguser.Email_Address;

                    //Send Email to Admin
                    _userRepository.SendEmailUpdate(user, loggedInUserEmail);


                    //Confirmation Message 
                    ViewData["Message"] = "User profile has been updated. Please check your email for details.";

                }
                catch (Exception ex)
                {
                    LMS.ErrorHandler.HandleError.EmailError(ex);
                }

            }

            //Group ID
            ViewData["Group_ID"] = new SelectList(_dropRepository.GetGroups(true), "Group_ID", "Description", user.Group_ID);

            //Security Level ID
            ViewData["Security_Level_ID"] = new SelectList(_dropRepository.GetSecurityLevels(true), "Security_Level_ID", "Description", user.Security_Level_ID);

            //Access to Request App Page
            ViewData["Sales_Group_ID"] = new SelectList(_dropRepository.GetUserAccess(true), "Key", "Value", user.Sales_Group_ID);

            //Pod Id
            ViewData["Pod_ID"] = new SelectList(_dropRepository.GetPods(true), "Pod_ID", "Pod_Name", user.Pod_ID);


            return View(user);
        }


        public ActionResult SourceCodeEdit(int id)
        {
            Source_Code source_code = _sourceRepository.GetSourceCode(id);
            ViewData["Group_ID"] = new SelectList(_dropRepository.GetActiveGroups(false), "Group_ID", "Description", source_code.Source_Code_ID.ToString());
            ViewData["Affinity_Partner_ID"] = new SelectList(_dropRepository.GetAffinityPartners(false), "Affinity_Partner_ID", "Name", source_code.Affinity_Partner_ID.ToString());
            ViewData["Web"] = _sourceRepository.InWeb(id);
            Partner partner = _sourceRepository.GetPartner(source_code.Source_Number, !source_code.LTC);
            if (partner != null)
            {
                ViewData["WebName"] = partner.WebName;
                ViewData["Code"] = partner.Code;
            }
            else
            {
                ViewData["WebName"] = "";
                ViewData["Code"] = "";
            }
            return View(source_code);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SourceCodeEdit(int id, FormCollection collection)
        {
            Source_Code source_code = _sourceRepository.GetSourceCode(id);
            UpdateModel(source_code);
            Partner partner = _sourceRepository.GetPartner(source_code.Source_Number, !source_code.LTC);
            if (source_code.Description.Trim().Length < 1)
            {
                ModelState.AddModelError("Description", "Source Name Cannot Be Blank");
            }
            if (source_code.Source_Number.Trim().Length < 1)
            {
                ModelState.AddModelError("Source_Number", "Source Number Cannot Be Blank");
            }
            //if (collection["Source_Number"].Trim().Length > 0 && !_sourceRepository.UniqueSourceNumber(collection["Source_Number"].Trim()))
            //{
            //    ModelState.AddModelError("Source_Number", "Duplicated Source Number");
            //    ModelState.SetModelValue("Source_Number", new ValueProviderResult(ValueProvider["Source_Number"].AttemptedValue, collection["Source_Number"], System.Globalization.CultureInfo.CurrentCulture));
            //}
            bool isweb = false;
            if (Boolean.TryParse(Request.Form.GetValues("WEB")[0], out isweb) && Boolean.Parse(Request.Form.GetValues("WEB")[0]) && String.IsNullOrEmpty(collection["Code"]))
            {
                ModelState.AddModelError("Code", "Code Cannot Be Blank");
            }
            if (Boolean.TryParse(Request.Form.GetValues("WEB")[0], out isweb) && Boolean.Parse(Request.Form.GetValues("WEB")[0]) && String.IsNullOrEmpty(collection["WebName"]))
            {
                ModelState.AddModelError("WebName", "Web Name Cannot Be Blank");
            }
            if (!String.IsNullOrEmpty(collection["WebName"]) && !isGuid.IsMatch(collection["Code"]))
            {
                ModelState.AddModelError("Code", "Code is not a valid GUID");
            }
            //if (!String.IsNullOrEmpty(collection["WebName"]) && !_sourceRepository.UniqueWebName(collection["WebName"]))
            //{
            //    ModelState.AddModelError("WebName", "Duplicated Web Name");
            //    ModelState.SetModelValue("WebName", new ValueProviderResult(ValueProvider["WebName"].AttemptedValue, collection["WebName"], System.Globalization.CultureInfo.CurrentCulture));

            //}
            if (ModelState.IsValid)
            {
                source_code.Source_Number = collection["Source_Number"];
                source_code.Description = collection["Description"];
                source_code.Group_ID = Int32.Parse(collection["Group_ID"]);
                source_code.Affinity_Partner_ID=Int32.Parse(collection["Affinity_Partner_ID"]);
                if (!String.IsNullOrEmpty(collection["Launch_Date"]))
                {
                    source_code.Launch_Date = DateTime.Parse(collection["Launch_Date"]);
                }
                else
                {
                    source_code.Launch_Date = null;
                }
                source_code.LTC = Boolean.Parse(Request.Form.GetValues("LTC")[0]);
                source_code.Web_Select = true;
                source_code.Active = Boolean.Parse(Request.Form.GetValues("Active")[0]);
                _sourceRepository.Save();
                int source_code_id = source_code.Source_Code_ID;
                try
                {
                    if (Boolean.Parse(Request.Form.GetValues("WEB")[0]) == true)
                    {
                        if (partner != null)
                        {
                            partner.WebName = collection["WebName"];
                            partner.Code = new Guid(collection["Code"]);
                            _sourceRepository.SavePartner();
                            ViewData["WebName"] = partner.WebName;
                            ViewData["Code"] = partner.Code;
                        }
                        else
                        {
                            Partner part = new Partner();
                            part.Active = source_code.Active;
                            part.Code = new Guid(collection["Code"]);
                            part.WebName = collection["WebName"];
                            part.Description = source_code.Description;
                            part.Source_Number = source_code.Source_Number;
                            part.LMS = !source_code.LTC;
                            part.Prefix = "WEB";
                            _sourceRepository.InsertPartner(part);
                            ViewData["WebName"] = collection["WebName"];
                            ViewData["Code"] = collection["Code"];
                        }
                    }
                }
                catch (Exception ex)
                {
                }
                ViewData["Group_ID"] = new SelectList(_dropRepository.GetActiveGroups(false), "Group_ID", "Description", source_code.Source_Code_ID.ToString());
                ViewData["Affinity_Partner_ID"] = new SelectList(_dropRepository.GetAffinityPartners(false), "Affinity_Partner_ID", "Name", source_code.Affinity_Partner_ID.ToString());
                ViewData["Web"] = _sourceRepository.InWeb(id);
                return RedirectToAction("SourceCodeDetails", new { id = source_code_id });
            }
            else
            {
                ViewData["Group_ID"] = new SelectList(_dropRepository.GetActiveGroups(false), "Group_ID", "Description", source_code.Source_Code_ID.ToString());
                ViewData["Affinity_Partner_ID"] = new SelectList(_dropRepository.GetAffinityPartners(false), "Affinity_Partner_ID", "Name", source_code.Affinity_Partner_ID.ToString());
                ViewData["Web"] = Request.Form.GetValues("WEB")[0];
                if (partner != null)
                {
                    ViewData["WebName"] = partner.WebName;
                    ViewData["Code"] = partner.Code;
                }
                else
                {
                    ViewData["WebName"] = "";
                    ViewData["Code"] = "";
                }
                return View(source_code);
            }

        }

        public ActionResult SourceCodeCreate()
        {
            Source_Code sourcecode = new Source_Code();
            ViewData["Group_ID"] = new SelectList(_dropRepository.GetActiveGroups(true), "Group_ID", "Description");
            ViewData["Affinity_Partner_ID"] = new SelectList(_dropRepository.GetAffinityPartners(true), "Affinity_Partner_ID", "Name");
            ViewData["Web"] = false;
            return View(sourcecode);

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SourceCodeCreate(Source_Code sourcecode, FormCollection collection)
        {
            ViewData["Group_ID"] = new SelectList(_dropRepository.GetActiveGroups(true), "Group_ID", "Description");
            ViewData["Affinity_Partner_ID"] = new SelectList(_dropRepository.GetAffinityPartners(true), "Affinity_Partner_ID", "Name");
            ViewData["WebName"] = collection["WebName"];
            ViewData["Web"] = Request.Form.GetValues("WEB")[0];
            ViewData["Code"] = collection["Code"];
            if (collection["Description"].Trim().Length < 1)
            {
                ModelState.AddModelError("Description", "Source Name Cannot Be Blank");
            }
            if (collection["Source_Number"].Trim().Length < 1)
            {
                ModelState.AddModelError("Source_Number", "Source Number Cannot Be Blank");
            }
            if (sourcecode.Affinity_Partner_ID < 0)
            {
                ModelState.AddModelError("Affinity_Partner_ID", "Affinity Partner Cannot Be Blank");
            }
            if (sourcecode.Group_ID < 0)
            {
                ModelState.AddModelError("Group_ID", "Group Cannot Be Blank");
            }
            bool isweb = false;
            if (collection["Source_Number"].Trim().Length > 0 && !_sourceRepository.UniqueSourceNumber(collection["Source_Number"].Trim()))
            {
                ModelState.AddModelError("Source_Number", "Duplicated Source Number");
                ModelState.SetModelValue("Source_Number", new ValueProviderResult(ValueProvider.GetValue("Source_Number").AttemptedValue, collection["Source_Number"], System.Globalization.CultureInfo.CurrentCulture));

            }
            if (Boolean.TryParse(Request.Form.GetValues("WEB")[0], out isweb) && Boolean.Parse(Request.Form.GetValues("WEB")[0]) && String.IsNullOrEmpty(collection["Code"]))
            {
                ModelState.AddModelError("Code", "Code Cannot Be Blank");
            }
            if (Boolean.TryParse(Request.Form.GetValues("WEB")[0], out isweb) && Boolean.Parse(Request.Form.GetValues("WEB")[0]) && String.IsNullOrEmpty(collection["WebName"]))
            {
                ModelState.AddModelError("WebName", "Web Name Cannot Be Blank");
            }
            if (!String.IsNullOrEmpty(collection["WebName"]) && !isGuid.IsMatch(collection["Code"]))
            {
                ModelState.AddModelError("Code", "Code is not a valid GUID");
            }
            if (!String.IsNullOrEmpty(collection["WebName"]) && !_sourceRepository.UniqueWebName(collection["WebName"]))
            {
                ModelState.AddModelError("WebName", "Duplicated Web Name");
                ModelState.SetModelValue("WebName", new ValueProviderResult(ValueProvider.GetValue("WebName").AttemptedValue, collection["WebName"], System.Globalization.CultureInfo.CurrentCulture));
            }
            if (ModelState.IsValid)
            {
                sourcecode.Paid = false;
                sourcecode.Cost = 0;
                sourcecode.Marketing_Group_ID = -1;
                sourcecode.Media_Type_ID = -1;
                sourcecode.Allow_Mailing = false;
                sourcecode.Web_Select = true;
                _sourceRepository.Insert(sourcecode);
                if (Boolean.Parse(Request.Form.GetValues("WEB")[0]) == true)
                {
                    Partner partner = new Partner();
                    partner.Prefix = "WEB";
                    partner.LMS = !sourcecode.LTC;
                    partner.Description = sourcecode.Description;
                    partner.WebName = collection["WebName"];
                    partner.Source_Number = sourcecode.Source_Number;
                    partner.Code = new Guid(collection["Code"]);
                    partner.Active = sourcecode.Active;
                    _sourceRepository.InsertPartner(partner);

                }

                return RedirectToAction("SourceCodeDetails", new { id = sourcecode.Source_Code_ID });

            }
            else
            {
                return View(sourcecode);
            }

        }

        public ActionResult SourceCodeDetails(int? id)
        {
            Source_Code sourcecode = _sourceRepository.GetSourceCode(id.Value);
            Partner partner = _sourceRepository.GetPartner(sourcecode.Source_Number, !sourcecode.LTC);
            ViewData["Web"] = _sourceRepository.InWeb(id.Value);
            if (partner != null)
            {
                ViewData["WebName"] = partner.WebName;
                ViewData["Code"] = partner.Code;
            }
            else
            {
                ViewData["WebName"] = "";
                ViewData["Code"] = "";
            }
            return View(sourcecode);

        }

        public ActionResult SourceCode()
        {

            ViewData["SourceCodeList"] = _sourceRepository.GetSourceCodeList();
            return View();
        }
    }
}
