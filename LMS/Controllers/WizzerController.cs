﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LMS.Controllers
{
    using LMS.Models;
    using LMS.Services;

    using log4net;

    using StructureMap;

    using WizzerLib.Implementations;
    using WizzerLib.Interfaces;
    using WizzerLib.Models;

    public class WizzerController : Controller
    {
        private readonly ILog logger = LogManager.GetLogger(typeof(WizzerService).FullName);

         /// <summary>
        ///     Initializes a new instance of the <see cref="WizzerService" /> class.
        /// </summary>
        public WizzerController()
            : this(
                ObjectFactory.GetAllInstances<IRecordActivityRules>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="WizzerService"/> class.
        /// </summary>
        /// <param name="recordActivityRules">
        /// The record Activity Rules.
        /// </param>
        public WizzerController(
            IEnumerable<IRecordActivityRules> recordActivityRules)
        {
            this.RecordActivityRules = recordActivityRules;
        }

        /// <summary>
        ///     Gets or sets the activity rules.
        /// </summary>
        internal IEnumerable<IRecordActivityRules> RecordActivityRules { get; set; }

        /// <summary>
        /// The get rules result.
        /// </summary>
        /// <param name="request">
        /// The request.
        /// </param>
        /// <returns>
        /// The <see cref="JsonResult"/>.
        /// </returns>
        [HttpGet]
        public JsonResult GetRulesResult(int activityId, int statusId, int leadDispositionId)
        {
            var response = new Response();
            try
            {
                var selectedItems = new SelectedItems
                                        {
                                            Activity = activityId,
                                            Status = statusId,
                                            LeadDisposition = leadDispositionId
                                        };

                var rules = new RecordActivityRules();
                var allowedItems = rules.GetRules(selectedItems);
                response.Activity = allowedItems.Activity.Distinct().ToList();
                response.LeadDisposition = allowedItems.LeadDisposition.Distinct().ToList();
                response.Status = allowedItems.Status.Distinct().ToList();
                //response.healthOnly = allowedItems.healthOnly;
                //response.ltcOnly = allowedItems.ltcOnly;
                response.Success = true;

            }
            catch (Exception ex)
            {
                this.logger.Fatal(ex);
                response.Success = false;
                response.Error = ex.Message;
            }

            return this.Json(response, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public JsonResult CheckForFollowUp(int activityId, int statusId, int leadDispositionId)
        {
            var response = new Response();
            try
            {
                var selectedItems = new SelectedItems
                {
                    Activity = activityId,
                    Status = statusId,
                    LeadDisposition = leadDispositionId
                };

                var recordActivityRules = new RecordActivityRules();
                var getfollowUp = recordActivityRules.GetFollowUp(selectedItems);
                response.FollowUp = getfollowUp.FollowUp;
                response.FollowUpType = getfollowUp.FollowUpType;


            }
            catch (Exception ex)
            {
                this.logger.Fatal(ex);
                response.Success = false;
                response.Error = ex.Message;
            }
            return this.Json(response, JsonRequestBehavior.AllowGet);
        }

    }
}
