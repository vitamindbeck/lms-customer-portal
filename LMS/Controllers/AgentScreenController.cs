using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Net.Mail;
using System.Net;
using LMS.Data.Repository;
using LMS.Data;
using LMS.Helpers;

namespace LMS.Controllers
{
    public class AgentScreenController : Controller
    {
        //
        // GET: /AgentScreen/
        private IUserRepository _userRepository;
        private ICertificationRepository _certificationRepository;
        private IAgentRepository _agentRepository;

        public AgentScreenController()
            : this(new UserRepository(), new CertificationRepository(), new AgentRepository())
        {

        }

        public AgentScreenController(IUserRepository userRepository, ICertificationRepository certificationRepository, IAgentRepository agentRepository)
        {
            _userRepository = userRepository;
            _certificationRepository = certificationRepository;
            _agentRepository = agentRepository;

        }

        public ActionResult Certification()
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            ViewData["CertificationListByUser"] = db.sp_Certification_List_By_User(UserRepository.GetUser(User.Identity.Name).User_ID).ToList();
            return View();
        }

        public ActionResult License()
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

            ViewData["LicenseListByUser"] = db.sp_License_List_By_User(UserRepository.GetUser(User.Identity.Name).User_ID).ToList();
            return View();
        }

        public ActionResult CertificationDetails(int id)
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            Certification certification = _certificationRepository.GetCertificationByID(id);
            //Get Agent Name
            var agents = db.sp_Get_AgentsAgency(certification.Agent_ID).FirstOrDefault();
            ViewData["Agent_Name"] = agents.Full_Name;

            //Get Other Details
            var carrierDetails = db.sp_Get_Certification_Details(certification.Certification_ID).FirstOrDefault();

            //Carrier
            ViewData["Carrier"] = carrierDetails.Name;

            //Status 
            ViewData["Certification_Status_ID"] = carrierDetails.Certification_Status;

            //Type
            ViewData["Certification_Type_ID"] = carrierDetails.Certification_Type;


            //Username
            if (certification.Updated_By != null)
            {
                ViewData["UserName"] = _userRepository.GetUser((int)certification.Updated_By).First_Name + " " + _userRepository.GetUser((int)certification.Updated_By).Last_Name;
            }
            else
            {
                ViewData["UserName"] = "";
            }
            return View(certification);
        }

        public ActionResult Notify(int id)
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            string useremail = UserRepository.GetUser(User.Identity.Name).Email_Address;
            string agentname = UserRepository.GetUser(User.Identity.Name).First_Name + " " + UserRepository.GetUser(User.Identity.Name).Last_Name;
            int agentid = _agentRepository.GetAgentByUserID(UserRepository.GetUser(User.Identity.Name).User_ID).Agent_ID;
            string url;
            Certification certification = db.Certifications.Where(c => c.Certification_ID == id).FirstOrDefault();
            certification.Notified = true;
            db.SubmitChanges();

            if(db.Connection.DataSource.ToLower() != "lasql2")
            {
                url = "http://test.lms.com";
            }
            else
            {
                url = "http://lms.com";
            }
            MailMessage msg = new MailMessage();
            msg.To.Add("licensing@longevityalliance.com");
            //msg.To.Add("jmei@longevityalliance.com");
            msg.From = new MailAddress(useremail);
            msg.Subject = "Certification Status Notice";
            string body = agentname + " has completed a certification. Please use the link below to update status. <br />";
            msg.Body = body + url + "/Certification/Edit/" + certification.Certification_ID.ToString();
            msg.IsBodyHtml = true;
            SmtpClient client = new SmtpClient();
            client.Send(msg);
            
            return RedirectToAction("Certification");
        }

    }
}
