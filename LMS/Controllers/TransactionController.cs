using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using LMS.Data.Repository;
using LMS.Data;
using Helpers;
using LMS.Helpers;
using System.IO;
using LMS.Models;
using System.Text;
using System.Collections.Specialized;
using System.Data.Linq;

namespace LMS.Controllers
{
    [Authorize]

    public class TransactionController : Controller
    {
        IPlanTransactionRepository _transactionRepository;
        IActivityRepository _activityRepository;
        IDropDownRepository _dropRepository;
        LMSDataContext _DB = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

        public TransactionController()
            : this(new PlanTransactionRepository(), new DropDownRepository(), new ActivityRepository())
        {
        }

        public TransactionController(IPlanTransactionRepository tranRepo, IDropDownRepository dropRepo, IActivityRepository actRepo)
        {
            _transactionRepository = tranRepo;
            _dropRepository = dropRepo;
            _activityRepository = actRepo;
        }

        public ActionResult Details(int id)
        {
            Plan_Transaction transaction = _transactionRepository.Get(id);

            // Get Carrier Drop Down List
            if (transaction.Plan_ID > 0)
            {
                ViewData["Carrier_ID"] = new SelectList(_dropRepository.GetCarriers(true), "Carrier_ID", "Name", transaction.Plan.Carrier_ID);
            }
            else
            {
                ViewData["Carrier_ID"] = new SelectList(_dropRepository.GetCarriers(true), "Carrier_ID", "Name", -1);
            }

            //  Get Plans Drop Down List
            ViewData["Plan_ID"] = new CascadingSelectList(_dropRepository.GetPlans(true), "Carrier_ID", "Plan_ID", "Name", transaction.Plan_ID.ToString());

            //  Get Transaction Dates
            ViewData["TransactionDates"] = _transactionRepository.GetDateTypes(transaction.Group_ID);

            return View(transaction);
        }

        public ActionResult View(int id)
        {
            Plan_Transaction transaction = _transactionRepository.Get(id);

            if (transaction.Group_ID == 1)
            {
                return RedirectToAction("Health", new { id = id });
            }

            if (transaction.Group_ID == 2)
            {
                return RedirectToAction("LTCQ", new { id = id });
            }

            if (transaction.Group_ID == 3)
            {
                return RedirectToAction("Life", new { id = id });
            }

            if (transaction.Group_ID == 4)
            {
                return RedirectToAction("Annuity", new { id = id });
            }

            return RedirectToAction("Annuity", new { id = id });

        }

        public ActionResult Plans(int id)
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

            var plans = from p in db.Plans
                        where p.Carrier_ID == id
                        select p;

            return Json(plans.ToList());
        }

        public ActionResult New(int id)
        {
            if (id == 1)
            {
                return View("Life");
            }
            else
            {
                return View("Life");
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Delete(int id)
        {
            LMSDataContext _db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

            var planToDelete = (from m in _db.Plan_Transactions
                                where m.Plan_Transaction_ID == id
                                select m).FirstOrDefault();

            _db.Plan_Transactions.DeleteOnSubmit(planToDelete);
            _db.SubmitChanges();

            return RedirectToAction("Index");
        }

        public ActionResult Health(int id)
        {
            DocumentRepository docRepository = new DocumentRepository();
            Plan_Transaction transaction = _transactionRepository.Get(id);
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            LMS.Data.User user = UserRepository.GetUser(User.Identity.Name);


            if (user == null)
                throw new HttpException(404, "User IS NULL");

            Record record = db.Records.SingleOrDefault(rec => rec.Record_ID == transaction.Record_ID);
            var transactionDateList = transaction.Plan_Dates.OrderBy(pd => pd.Transaction_Date_Type.Display_Order);

            ViewData["Record"] = record;
            ViewData["Plan_Transaction_ID"] = id;
            //Check Status and forward to View Only if Status in (New,Request Application, or Request Info/Quote)
            if (transaction.Plan_Transaction_Status_ID.HasValue)
                if (transaction.Plan_Transaction_Status_ID.Value != 17 && transaction.Plan_Transaction_Status_ID.Value != 1 && transaction.Plan_Transaction_Status_ID.Value != 18 && transaction.Plan_Transaction_Status_ID.Value != 19 && transaction.Plan_Transaction_Status_ID.Value != 20 && transaction.Plan_Transaction_Status_ID.Value != 21 && transaction.Plan_Transaction_Status_ID != 5)
                    if (user.Security_Level_ID == 1)
                        return RedirectToAction("HealthView", new { id = id });

            ////In-Force Mike Mc - Removed from v2.2.4 Deployment
            //var transactionDate = db.Plan_Transaction_Dates.FirstOrDefault(x => x.Plan_Transaction_ID == transaction.Plan_Transaction_ID && x.Transaction_Date_Type_ID == 8);
            //if (transactionDate != null)
            //    if (transactionDate.Date_Value != null)
            //        if (user.User_ID != 47 && user.User_ID != 117 && user.User_ID != 265)
            //            return RedirectToAction("HealthView", new { id = id });


            // Get Carrier Drop Down List
            if (transaction.Plan_ID > 0)
            {
                ViewData["Carrier_ID"] = new SelectList(_dropRepository.GetCarriers(transaction.Group_ID, true), "Carrier_ID", "Name", transaction.Plan.Carrier_ID);
            }
            else
            {
                ViewData["Carrier_ID"] = new SelectList(_dropRepository.GetCarriers(transaction.Group_ID, true), "Carrier_ID", "Name", -1);
            }

            ViewData["Plan_Document_ID"] = new SelectList(db.Plan_Documents.Where(pd => pd.Plan_ID == transaction.Plan_ID && pd.State_ID == record.State_ID && pd.Active.Equals(true)), "Plan_Document_ID", "Title");

            //  Get Plans Drop Down List
            ViewData["Plan_ID"] = new CascadingSelectList(_dropRepository.GetPlans(true), "Carrier_ID", "Plan_ID", "Name", transaction.Plan_ID.ToString());

            //Get Case Manager 
            ViewData["Case_Manag_ID"] = new SelectList(_dropRepository.GetCaseManagers(true), "Case_Manager_ID", "Name", transaction.Case_Manager_ID);


            //  Get Transaction Dates
            ViewData["TransactionDates"] = _transactionRepository.GetDateTypes(transaction.Group_ID);

            ViewData["Inbound_Shipping_Method_List"] = new SelectList(_dropRepository.GetShippingMethods(true), "Shipping_Method_ID", "Description", transaction.Inbound_Shipping_Method_ID);
            ViewData["Outbound_Shipping_Method_List"] = new SelectList(_dropRepository.GetShippingMethods(true), "Shipping_Method_ID", "Description", transaction.Outbound_Shipping_Method_ID);

            //Plan Status
            ViewData["Plan_Transaction_Status_ID"] = new SelectList(_dropRepository.GetTransactionStatus(false), "Plan_Transaction_Status_ID", "Description", transaction.Plan_Transaction_Status_ID);

            // Get Modals
            ViewData["Modal_ID"] = new SelectList(_dropRepository.GetModals(true), "Modal_ID", "Description", transaction.Modal_ID);

            // Note Helper
            ViewData["NoteHelpers"] = new SelectList(_dropRepository.GetUserNotes(UserRepository.GetUser(User.Identity.Name).User_ID), "Note", "Note");

            ViewData["DateTypes"] = _transactionRepository.GetDateTypes(transaction.Group_ID);

            ViewData["Agent_List"] = new SelectList(_dropRepository.GetAgents(true), "User_ID", "FullName", transaction.Agent_ID);

            ViewData["Source_Code_ID"] = new SelectList(_dropRepository.GetSourceCodes(true), "Source_Code_ID", "DropDownName", transaction.Source_Code_ID);

            ViewData["Payment_Mode_ID"] = new SelectList(_dropRepository.GetPaymentModes(true), "Payment_Mode_ID", "Description", transaction.Payment_Mode_ID);

            ViewData["Underwriting_Class_List"] = new SelectList(_dropRepository.GetUnderwritingClass(true), "Underwriting_ID", "Description", transaction.Underwriting_Class_ID);

            //Get Buying Period
            var buyingPeriods = _dropRepository.GetBuyingPeriods(false);
            ViewData["Buying_Period_List"] = new SelectList(buyingPeriods, "Buying_Period_ID", "Description", transaction.Buying_Period_ID);

            //Get Buying Period
            var buyingPeriodSubs = _dropRepository.GetBuyingPeriodSubs(true);
            ViewData["Buying_Period_Sub_List"] = new SelectList(buyingPeriodSubs, "Buying_Period_Sub_ID", "Description", transaction.Buying_Period_Sub_ID);

            Dictionary<int, string> parts = new Dictionary<int, string>();
            parts.Add(record.Record_ID, record.FullName);
            if (record.Partner_ID > 0)
            {
                Record partner = db.Records.FirstOrDefault(r => r.Record_ID == record.Partner_ID);
                if (partner != null)
                {
                    parts.Add(partner.Record_ID, partner.FullName);
                }
            }

            ViewData["Who"] = new SelectList(parts, "Key", "Value");


            EmailDocumentModel emailModel = new EmailDocumentModel();
            emailModel.Body = GetEmailBody(record, user);
            emailModel.FromAddress = user.Email_Address;
            emailModel.ToAddress = record.Email_Address;
            emailModel.Subject = "Plan Documents";
            emailModel.Plan_Transaction_ID = transaction.Plan_Transaction_ID;
            emailModel.Documents = docRepository.GetAllByPlan(transaction.Plan_ID, record.State_ID);
            ViewData["AllPlanDocuments"] = emailModel;

            return View(transaction);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Health(int id, FormCollection collection)
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            Plan_Transaction transaction = db.Plan_Transactions.Where(pt => pt.Plan_Transaction_ID == id).FirstOrDefault();
            if (collection["Created_On"] != null)
            {
                transaction.Created_On = DateTime.Parse(collection["Created_On"]);
            }
            transaction.Agent_ID = Int32.Parse(collection["Agent_ID"]);
            if (collection["Case_Manager_ID"] != null)
            {
                transaction.Case_Manager_ID = Int32.Parse(collection["Case_Manager_ID"]);
            }
            else
            {
                transaction.Case_Manager_ID = -1;
            }
            transaction.Source_Code_ID = Int32.Parse(collection["Source_Code_ID"]);
            transaction.Plan_Transaction_Status_ID = Int32.Parse(collection["Plan_Transaction_Status_ID"]);
            transaction.PreFillApp = collection["PreFillApp"].Contains("true");
            transaction.eApp = collection["eApp"].Contains("true");
            transaction.Initial_Enrollment = collection["Initial_Enrollment"].Contains("true");
            transaction.Guaranteed_Issue = collection["Guaranteed_Issue"].Contains("true");
            transaction.Plan_ID = Int32.Parse(collection["Plan_ID"]);
            transaction.Sub_Plan = collection["Sub_Plan"];
            transaction.Buying_Period_ID = Int32.Parse(collection["Buying_Period_ID"]);
            transaction.Buying_Period_Sub_ID = Int32.Parse(collection["Buying_Period_Sub_ID"]);

            DateTime bped;
            if (DateTime.TryParse(collection["Buying_Period_Effective_Date"], out bped))
            {
                transaction.Buying_Period_Effective_Date = DateTime.Parse(collection["Buying_Period_Effective_Date"]);
            }
            else
            {
                transaction.Buying_Period_Effective_Date = null;
            }

            if (DateTime.TryParse(collection["Status_Date"], out bped))
            {
                transaction.Status_Date = DateTime.Parse(collection["Status_Date"]);
            }
            else
            {
                transaction.Status_Date = null;
            }


            transaction.Underwriting_Class_ID = Int32.Parse(collection["Underwriting_Class_ID"]);
            decimal mp;
            if (Decimal.TryParse(collection["Modal_Premium"], out mp))
            {
                transaction.Modal_Premium = mp;
            }
            else
            {
                transaction.Modal_Premium = 0.00m;
            }
            transaction.Modal_ID = Int32.Parse(collection["Modal_ID"]);
            decimal ap;
            if (Decimal.TryParse(collection["Annual_Premium"], out ap))
            {
                transaction.Annual_Premium = ap;
            }
            else
            {
                transaction.Annual_Premium = 0.00m;
            }
            transaction.Cash_With_Application = collection["Cash_With_Application"].Contains("true");
            transaction.Payment_Mode_ID = Int32.Parse(collection["Payment_Mode_ID"]);
            transaction.Outbound_Shipping_Method_ID = Int32.Parse(collection["Outbound_Shipping_Method_ID"]);
            transaction.Tracking_Number_Outbound = collection["Tracking_Number_Outbound"];
            transaction.Inbound_Shipping_Method_ID = Int32.Parse(collection["Inbound_Shipping_Method_ID"]);
            transaction.Tracking_Number_Inbound = collection["Tracking_Number_Inbound"];
            transaction.Policy_Number = StringHelper.EncryptString(collection["Policy_Number"]);
            transaction.Special_Instructions = collection["Special_Instructions"];

            Plan_Transaction_Date plan_trans_date = new Plan_Transaction_Date();

            if (transaction.Agent_ID < 0 || transaction.Agent_ID == 49)
            {
                ModelState.AddModelError("Agent_ID", "Choose Agent");
                ModelState.SetModelValue("Agent_ID", new ValueProviderResult(ValueProvider.GetValue("Agent_ID").AttemptedValue, collection["Agent_ID"], System.Globalization.CultureInfo.CurrentCulture));
            }
            if (transaction.Source_Code_ID < 1)
            {
                ModelState.AddModelError("Source_Code_ID", "Choose Source Code");
                ModelState.SetModelValue("Source_Code_ID", new ValueProviderResult(ValueProvider.GetValue("Source_Code_ID").AttemptedValue, collection["Source_Code_ID"], System.Globalization.CultureInfo.CurrentCulture));
            }
            if (transaction.Buying_Period_ID == 5)
            {
                ModelState.AddModelError("Buying_Period_ID", "Buying Period Cannot be None");
                ModelState.SetModelValue("Buying_Period_ID", new ValueProviderResult(ValueProvider.GetValue("Buying_Period_ID").AttemptedValue, collection["Buying_Period_ID"], System.Globalization.CultureInfo.CurrentCulture));
            }
            if ((transaction.Plan_Transaction_Status_ID == 1 || transaction.Plan_Transaction_Status_ID == 20) && transaction.Buying_Period_Sub_ID < 1)
            {
                ModelState.AddModelError("Buying_Period_Sub_ID", "*");
                ModelState.SetModelValue("Buying_Period_Sub_ID", new ValueProviderResult(ValueProvider.GetValue("Buying_Period_Sub_ID").AttemptedValue, collection["Buying_Period_Sub_ID"], System.Globalization.CultureInfo.CurrentCulture));
            }
            if ((transaction.Plan_Transaction_Status_ID == 1 || transaction.Plan_Transaction_Status_ID == 20) && String.IsNullOrEmpty(transaction.Buying_Period_Effective_Date.ToString()))
            {
                ModelState.AddModelError("Buying_Period_Effective_Date", "*");
                ModelState.SetModelValue("Buying_Period_Effective_Date", new ValueProviderResult(ValueProvider.GetValue("Buying_Period_Effective_Date").AttemptedValue, collection["Buying_Period_Effective_Date"], System.Globalization.CultureInfo.CurrentCulture));
            }
            if ((transaction.Plan_Transaction_Status_ID == 1 || transaction.Plan_Transaction_Status_ID == 20) && transaction.Outbound_Shipping_Method_ID < 1)
            {
                ModelState.AddModelError("Outbound_Shipping_Method_ID", "*");
                ModelState.SetModelValue("Outbound_Shipping_Method_ID", new ValueProviderResult(ValueProvider.GetValue("Outbound_Shipping_Method_ID").AttemptedValue, collection["Outbound_Shipping_Method_ID"], System.Globalization.CultureInfo.CurrentCulture));
            }



            foreach (string key in collection.AllKeys)
            {
                if (key.StartsWith("DateType"))
                {
                    int PlanTransactionDateID = Convert.ToInt32(key.Replace("DateType", ""));
                    Plan_Transaction_Date date = db.Plan_Transaction_Dates.First(d => d.Plan_Transaction_Date_ID == PlanTransactionDateID);
                    Session[key] = collection[key]; //register session
                    if (transaction.Plan_Transaction_Status_ID == 1)
                    {
                        if (date.Transaction_Date_Type_ID == 2 && String.IsNullOrEmpty(collection[key]))
                        {

                            ModelState.AddModelError("DateType" + PlanTransactionDateID.ToString(), "*");
                            ModelState.SetModelValue("DateType" + PlanTransactionDateID.ToString(), ValueProvider.GetValue("DateType" + PlanTransactionDateID.ToString()));

                        }
                        else if (date.Transaction_Date_Type_ID == 18 && String.IsNullOrEmpty(collection[key]))
                        {
                            ModelState.AddModelError("DateType" + PlanTransactionDateID.ToString(), "*");
                            ModelState.SetModelValue("DateType" + PlanTransactionDateID.ToString(), ValueProvider.GetValue("DateType" + PlanTransactionDateID.ToString()));
                        }

                    }
                    else if (transaction.Plan_Transaction_Status_ID == 20)
                    {

                        if (date.Transaction_Date_Type_ID == 18 && String.IsNullOrEmpty(collection[key]))
                        {
                            ModelState.AddModelError("DateType" + PlanTransactionDateID.ToString(), "*");
                            ModelState.SetModelValue("DateType" + PlanTransactionDateID.ToString(), ValueProvider.GetValue("DateType" + PlanTransactionDateID.ToString()));
                        }
                    }

                }
            }

            if (ModelState.IsValid)
            {
                try
                {
                    try
                    {
                        db.SubmitChanges();
                        //Add Plan Transaction History
                        var recDetails = db.Plan_Transactions.Where(p => p.Plan_Transaction_ID == transaction.Plan_Transaction_ID && p.Case_Manager_ID != null).FirstOrDefault();

                        var recfromhistory = db.Plan_Transaction_Histories.Where(p => p.Plan_Transaction_ID == recDetails.Plan_Transaction_ID && p.Case_Manager_ID == recDetails.Case_Manager_ID).FirstOrDefault();
                        //check if plan transaction ID

                        //Check if case manager id already exists in plan_transaction_History table for that plan_transaction ID
                        if (recfromhistory == null)
                        {
                            //Insert row in Pla_Transaction_History table with Plan_transaction_ID, Case_Manager Id and rec created on date.
                            Plan_Transaction_History planTransHistory = new Plan_Transaction_History();

                            planTransHistory.Created_On = DateTime.Now;
                            planTransHistory.Plan_Transaction_ID = recDetails.Plan_Transaction_ID;
                            planTransHistory.Case_Manager_ID = recDetails.Case_Manager_ID;

                            // Save webLeadHistory
                            db.Plan_Transaction_Histories.InsertOnSubmit(planTransHistory);
                            db.SubmitChanges();
                        }
                        if (!String.IsNullOrEmpty(recDetails.GBS_ID))
                        {
                            int gbsId = Convert.ToInt32(recDetails.GBS_ID);

                            db.sp_Update_GBS_Policies_from_LMS_Plan_Page(gbsId);
                        }
                    }
                    catch (Exception ex)
                    {
                        if (collection["Annual_Premium"] != null)
                            if (collection["Annual_Premium"] == "")
                            {
                                transaction.Annual_Premium = 0;
                            }
                            else
                            {
                                transaction.Annual_Premium = Convert.ToDecimal(collection["Annual_Premium"].Replace("$", "").Replace(",", ""));
                            }
                        if (collection["Modal_Premium"] != null)
                            if (collection["Modal_Premium"] == "")
                            {
                                transaction.Modal_Premium = 0;
                            }
                            else
                            {
                                transaction.Modal_Premium = Convert.ToDecimal(collection["Modal_Premium"].Replace("$", "").Replace(",", ""));
                            }
                    }
                    try
                    {
                        if (transaction.Buying_Period_ID != 5)
                        {
                            db.SubmitChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        LMS.ErrorHandler.HandleError.EmailError(ex);
                        return RedirectToAction("Health", new { id = id });
                    }
                    // if status is application sent, create follow up activity depending on shipping method
                    if ((transaction.Plan_Transaction_Status_ID == 1 || transaction.Plan_Transaction_Status_ID == 20) && !_activityRepository.HasOpenActivities(transaction.Record_ID))
                    {
                        Record_Activity activity = new Record_Activity();
                        activity.Activity_Type_ID = 2;//outbound call
                        activity.Actual_Date = null;
                        activity.Created_On = DateTime.Now;
                        activity.Created_By = LMS.Data.Repository.UserRepository.GetUser(User.Identity.Name).User_ID;
                        activity.Record_ID = transaction.Record_ID;
                        activity.User_ID = (int)transaction.Agent_ID;
                        activity.Made_Contact = false;
                        activity.Plan_Transaction_ID = transaction.Plan_Transaction_ID;
                        activity.Priority_Level = 3;
                        activity.Record_Activity_Status_ID = 1;
                        if (transaction.Outbound_Shipping_Method_ID == 1 || transaction.Outbound_Shipping_Method_ID == 4)
                        {
                            activity.Due_Date = DateTime.Today.AddDays(1);
                        }
                        else if (transaction.Outbound_Shipping_Method_ID == 2)
                        {
                            activity.Due_Date = DateTime.Today.AddDays(5);
                        }
                        else
                        {
                            activity.Due_Date = DateTime.Today.AddDays(3);
                        }
                        db.Record_Activities.InsertOnSubmit(activity);
                        db.SubmitChanges();
                    }

                    foreach (string key in collection.AllKeys)
                    {
                        if (key.StartsWith("DateType"))
                        {
                            int PlanTransactionDateID = Convert.ToInt32(key.Replace("DateType", ""));
                            Plan_Transaction_Date date = db.Plan_Transaction_Dates.First(d => d.Plan_Transaction_Date_ID == PlanTransactionDateID);

                            if (collection[key] != "")
                            {
                                date.Date_Value = new DateTime?(Convert.ToDateTime(collection[key]));
                                db.SubmitChanges();
                            }
                            else
                            {
                                date.Date_Value = null;
                                db.SubmitChanges();
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    LMS.ErrorHandler.HandleError.EmailError(ex);
                }

                // Check for Assign
                if (collection.AllKeys.Contains("New.x"))
                {

                    // Create New Plan
                    Plan_Transaction NewPlan = new Plan_Transaction();

                    NewPlan.Agent_ID = transaction.Agent_ID;
                    NewPlan.Special_Instructions = transaction.Special_Instructions;
                    NewPlan.Created_On = DateTime.Now;
                    NewPlan.Group_ID = transaction.Group_ID;
                    NewPlan.Record_ID = transaction.Record_ID;
                    NewPlan.Source_Code_ID = transaction.Source_Code_ID;
                    NewPlan.Plan_Transaction_Status_ID = 17;
                    NewPlan.Outbound_Shipping_Method_ID = transaction.Outbound_Shipping_Method_ID;
                    NewPlan.Inbound_Shipping_Method_ID = transaction.Inbound_Shipping_Method_ID;
                    NewPlan.Active = false;
                    NewPlan.Cash_With_Application = false;
                    NewPlan.Cash_With_Application_Check = false;
                    db.Plan_Transactions.InsertOnSubmit(NewPlan);
                    db.SubmitChanges();

                    if (NewPlan.Plan_Transaction_ID > 0)
                    {
                        db.CreatePlanTransactionDates(NewPlan.Plan_Transaction_ID);

                        return RedirectToAction("Health", "Transaction", new { id = NewPlan.Plan_Transaction_ID });
                    }
                }
                //here will be the condition added to check if prefilled check box is checked, redirect it to   
                if ((transaction.PreFillApp == true) && (transaction.Plan_Transaction_Status_ID == 17))
                {
                    return RedirectToAction("New", "Request", new { id = transaction.Plan_Transaction_ID });
                }
                else
                {
                    return RedirectToAction("Details", "Record", new { id = transaction.Record_ID });
                }
            }
            else
            {
                DocumentRepository docRepository = new DocumentRepository();
                LMS.Data.User user = UserRepository.GetUser(User.Identity.Name);

                if (user == null)
                    throw new HttpException(404, "User IS NULL");
                Record record = db.Records.SingleOrDefault(rec => rec.Record_ID == transaction.Record_ID);
                ViewData["Record"] = record;
                ViewData["Plan_Transaction_ID"] = id;
                //Check Status and forward to View Only if Status in (New,Request Application, or Request Info/Quote)
                if (transaction.Plan_Transaction_Status_ID.HasValue)
                    if (transaction.Plan_Transaction_Status_ID.Value != 17 && transaction.Plan_Transaction_Status_ID.Value != 20 && transaction.Plan_Transaction_Status_ID.Value != 21)
                        if (user.Security_Level_ID == 1)
                            return RedirectToAction("HealthView", new { id = id });


                // Get Carrier Drop Down List
                if (transaction.Plan_ID > 0)
                {
                    ViewData["Carrier_ID"] = new SelectList(_dropRepository.GetCarriers(transaction.Group_ID, true), "Carrier_ID", "Name", transaction.Plan.Carrier_ID);


                }
                else
                {
                    ViewData["Carrier_ID"] = new SelectList(_dropRepository.GetCarriers(transaction.Group_ID, true), "Carrier_ID", "Name", -1);
                }

                ViewData["Plan_Document_ID"] = new SelectList(db.Plan_Documents.Where(pd => pd.Plan_ID == transaction.Plan_ID && pd.State_ID == record.State_ID && pd.Active.Equals(true)), "Plan_Document_ID", "Title");

                //  Get Plans Drop Down List
                ViewData["Plan_ID"] = new CascadingSelectList(_dropRepository.GetPlans(true), "Carrier_ID", "Plan_ID", "Name", transaction.Plan_ID.ToString());

                //  Get Transaction Dates
                ViewData["TransactionDates"] = _transactionRepository.GetDateTypes(transaction.Group_ID);

                ViewData["Inbound_Shipping_Method_List"] = new SelectList(_dropRepository.GetShippingMethods(true), "Shipping_Method_ID", "Description", transaction.Inbound_Shipping_Method_ID);
                ViewData["Outbound_Shipping_Method_List"] = new SelectList(_dropRepository.GetShippingMethods(true), "Shipping_Method_ID", "Description", transaction.Outbound_Shipping_Method_ID);

                //Plan Status
                ViewData["Plan_Transaction_Status_ID"] = new SelectList(_dropRepository.GetTransactionStatus(false), "Plan_Transaction_Status_ID", "Description", transaction.Plan_Transaction_Status_ID);

                // Get Modals
                ViewData["Modal_ID"] = new SelectList(_dropRepository.GetModals(true), "Modal_ID", "Description", transaction.Modal_ID);

                // Note Helper
                ViewData["NoteHelpers"] = new SelectList(_dropRepository.GetUserNotes(UserRepository.GetUser(User.Identity.Name).User_ID), "Note", "Note");

                ViewData["DateTypes"] = _transactionRepository.GetDateTypes(transaction.Group_ID);

                ViewData["Agent_List"] = new SelectList(_dropRepository.GetAgents(true), "User_ID", "FullName", transaction.Agent_ID);



                //Get Case Manager 
                ViewData["Case_Manag_ID"] = new SelectList(_dropRepository.GetCaseManagers(true), "Case_Manager_ID", "Name", transaction.Case_Manager_ID);


                ViewData["Source_Code_ID"] = new SelectList(_dropRepository.GetSourceCodes(true), "Source_Code_ID", "DropDownName", transaction.Source_Code_ID);

                ViewData["Payment_Mode_ID"] = new SelectList(_dropRepository.GetPaymentModes(true), "Payment_Mode_ID", "Description", transaction.Payment_Mode_ID);

                ViewData["Underwriting_Class_List"] = new SelectList(_dropRepository.GetUnderwritingClass(true), "Underwriting_ID", "Description", transaction.Underwriting_Class_ID);

                //Get Buying Period
                var buyingPeriods = _dropRepository.GetBuyingPeriods(false);
                ViewData["Buying_Period_List"] = new SelectList(buyingPeriods, "Buying_Period_ID", "Description", transaction.Buying_Period_ID);

                //Get Buying Period
                var buyingPeriodSubs = _dropRepository.GetBuyingPeriodSubs(true);
                ViewData["Buying_Period_Sub_List"] = new SelectList(buyingPeriodSubs, "Buying_Period_Sub_ID", "Description", transaction.Buying_Period_Sub_ID);

                foreach (string key in collection.AllKeys)
                {
                    if (key.StartsWith("DateType"))
                    {
                        int PlanTransactionDateID = Convert.ToInt32(key.Replace("DateType", ""));
                        Plan_Transaction_Date date = db.Plan_Transaction_Dates.First(d => d.Plan_Transaction_Date_ID == PlanTransactionDateID);
                        //ViewData["DateType" + PlanTransactionDateID.ToString()] = 

                    }
                }

                Dictionary<int, string> parts = new Dictionary<int, string>();
                parts.Add(record.Record_ID, record.FullName);
                if (record.Partner_ID > 0)
                {
                    Record partner = db.Records.FirstOrDefault(r => r.Record_ID == record.Partner_ID);
                    if (partner != null)
                    {
                        parts.Add(partner.Record_ID, partner.FullName);
                    }
                }

                ViewData["Who"] = new SelectList(parts, "Key", "Value");


                EmailDocumentModel emailModel = new EmailDocumentModel();
                emailModel.Body = GetEmailBody(record, user);
                emailModel.FromAddress = user.Email_Address;
                emailModel.ToAddress = record.Email_Address;
                emailModel.Subject = "Plan Documents";
                emailModel.Plan_Transaction_ID = transaction.Plan_Transaction_ID;
                emailModel.Documents = docRepository.GetAllByPlan(transaction.Plan_ID, record.State_ID);
                ViewData["AllPlanDocuments"] = emailModel;

                return View(transaction);
            }

        }

        public ActionResult Annuity(int id)
        {
            DocumentRepository docRepository = new DocumentRepository();
            Plan_Transaction transaction = _transactionRepository.Get(id);
            LMS.Data.User user = UserRepository.GetUser(User.Identity.Name);

            //Check Status and forward to View Only if Status in (New,Request Application, or Request Info/Quote)
            if (transaction.Plan_Transaction_Status_ID.HasValue)
                if (transaction.Plan_Transaction_Status_ID.Value != 17 && transaction.Plan_Transaction_Status_ID.Value != 20 && transaction.Plan_Transaction_Status_ID.Value != 21 && transaction.Plan_Transaction_Status_ID.Value != 1 && transaction.Plan_Transaction_Status_ID.Value != 19)
                    if (user.Security_Level_ID == 1)
                        return RedirectToAction("AnnuityView", new { id = id });


            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

            Record record = db.Records.SingleOrDefault(rec => rec.Record_ID == transaction.Record_ID);

            ViewData["Record"] = record;

            // Get Carrier Drop Down List
            if (transaction.Plan_ID > 0)
            {
                ViewData["Carrier_ID"] = new SelectList(_dropRepository.GetCarriers(transaction.Group_ID, true), "Carrier_ID", "Name", transaction.Plan.Carrier_ID);
                ViewData["Plan_Document_ID"] = new SelectList(db.Plan_Documents.Where(pd => pd.Plan_ID == transaction.Plan_ID && pd.State_ID == record.State_ID), "Plan_Document_ID", "Title");
            }
            else
            {
                ViewData["Carrier_ID"] = new SelectList(_dropRepository.GetCarriers(transaction.Group_ID, true), "Carrier_ID", "Name", -1);
            }

            //  Get Plans Drop Down List
            ViewData["Plan_ID"] = new CascadingSelectList(_dropRepository.GetPlans(true), "Carrier_ID", "Plan_ID", "Name", transaction.Plan_ID.ToString());

            //  Get Transaction Dates
            ViewData["TransactionDates"] = _transactionRepository.GetDateTypes(transaction.Group_ID);

            ViewData["Inbound_Shipping_Method_ID"] = new SelectList(_dropRepository.GetShippingMethods(true), "Shipping_Method_ID", "Description", transaction.Inbound_Shipping_Method_ID);
            ViewData["Outbound_Shipping_Method_ID"] = new SelectList(_dropRepository.GetShippingMethods(true), "Shipping_Method_ID", "Description", transaction.Outbound_Shipping_Method_ID);

            //Plan Status
            ViewData["Plan_Transaction_Status_ID"] = new SelectList(_dropRepository.GetTransactionStatus(false), "Plan_Transaction_Status_ID", "Description", transaction.Plan_Transaction_Status_ID);

            //// Get Modals
            ViewData["Modal_ID"] = new SelectList(_dropRepository.GetModals(true), "Modal_ID", "Description", transaction.Modal_ID);

            //ViewData["DateTypes"] = _transactionRepository.GetDateTypes(transaction.Group_ID);
            ViewData["NoteHelpers"] = new SelectList(_dropRepository.GetUserNotes(UserRepository.GetUser(User.Identity.Name).User_ID), "Note", "Note");

            ViewData["Agent_ID"] = new SelectList(_dropRepository.GetAgents(true), "User_ID", "FullName", transaction.Agent_ID);

            ViewData["Source_Code_ID"] = new SelectList(_dropRepository.GetSourceCodes(true), "Source_Code_ID", "DropDownName", transaction.Source_Code_ID);

            ViewData["Payment_Mode_ID"] = new SelectList(_dropRepository.GetPaymentModes(true), "Payment_Mode_ID", "Description", transaction.Payment_Mode_ID);

            ViewData["Underwriting_Class_ID"] = new SelectList(_dropRepository.GetUnderwritingClass(true), "Underwriting_ID", "Description", transaction.Underwriting_Class_ID);

            EmailDocumentModel emailModel = new EmailDocumentModel();
            emailModel.Body = GetEmailBody(record, user);
            emailModel.FromAddress = user.Email_Address;
            emailModel.ToAddress = record.Email_Address;
            emailModel.Subject = "Plan Documents";
            emailModel.Plan_Transaction_ID = transaction.Plan_Transaction_ID;
            emailModel.Documents = docRepository.GetAllByPlan(transaction.Plan_ID, record.State_ID);
            ViewData["AllPlanDocuments"] = emailModel;

            return View(transaction);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Annuity(int id, FormCollection collection)
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            Plan_Transaction transaction = db.Plan_Transactions.Where(pt => pt.Plan_Transaction_ID == id).FirstOrDefault();
            try
            {
                if (collection["Created_On"] != null)
                {
                    transaction.Created_On = DateTime.Parse(collection["Created_On"]);
                }
                transaction.Agent_ID = Int32.Parse(collection["Agent_ID"]);
                transaction.Source_Code_ID = Int32.Parse(collection["Source_Code_ID"]);
                transaction.Plan_Transaction_Status_ID = Int32.Parse(collection["Plan_Transaction_Status_ID"]);
                transaction.eApp = collection["eApp"].Contains("true");
                transaction.Plan_ID = Int32.Parse(collection["Plan_ID"]);
                decimal da;
                if (Decimal.TryParse(collection["Deposit_Amount"], out da))
                {
                    transaction.Deposit_Amount = da;
                }
                else
                {
                    transaction.Deposit_Amount = 0.00m;
                }
                transaction.Underwriting_Class_ID = Int32.Parse(collection["Underwriting_Class_ID"]);
                decimal mp;
                if (Decimal.TryParse(collection["Modal_Premium"], out mp))
                {
                    transaction.Modal_Premium = mp;
                }
                else
                {
                    transaction.Modal_Premium = 0.00m;
                }
                transaction.Modal_ID = Int32.Parse(collection["Modal_ID"]);
                decimal ap;
                if (Decimal.TryParse(collection["Annual_Premium"], out ap))
                {
                    transaction.Annual_Premium = ap;
                }
                else
                {
                    transaction.Annual_Premium = 0.00m;
                }
                DateTime bped;
                if (DateTime.TryParse(collection["Status_Date"], out bped))
                {
                    transaction.Status_Date = DateTime.Parse(collection["Status_Date"]);
                }
                else
                {
                    transaction.Status_Date = null;
                }
                transaction.Cash_With_Application = collection["Cash_With_Application"].Contains("true");
                transaction.Payment_Mode_ID = Int32.Parse(collection["Payment_Mode_ID"]);
                transaction.Qualified_Transfer = collection["Qualified_Transfer"].Contains("true");
                transaction.Non_Qualified_1035_Exchange = collection["Non_Qualified_1035_Exchange"].Contains("true");
                transaction.Replacement = collection["Replacement"].Contains("true");
                transaction.Replacement_Details = collection["Replacement_Details"];
                transaction.Outbound_Shipping_Method_ID = Int32.Parse(collection["Outbound_Shipping_Method_ID"]);
                transaction.Tracking_Number_Outbound = collection["Tracking_Number_Outbound"];
                transaction.Inbound_Shipping_Method_ID = Int32.Parse(collection["Inbound_Shipping_Method_ID"]);
                transaction.Tracking_Number_Inbound = collection["Tracking_Number_Inbound"];
                transaction.Policy_Number = StringHelper.EncryptString(collection["Policy_Number"]);
                transaction.Special_Instructions = collection["Special_Instructions"];
            }
            catch (Exception ex)
            {
                if (collection["Annual_Premium"] != null)
                    if (collection["Annual_Premium"] == "")
                    {
                        transaction.Annual_Premium = 0;
                    }
                    else
                    {
                        transaction.Annual_Premium = Convert.ToDecimal(collection["Annual_Premium"].Replace("$", "").Replace(",", ""));
                    }

                if (collection["Modal_Premium"] != null)
                    if (collection["Modal_Premium"] == "")
                    {
                        transaction.Modal_Premium = 0;
                    }
                    else
                    {
                        transaction.Modal_Premium = Convert.ToDecimal(collection["Modal_Premium"].Replace("$", "").Replace(",", ""));
                    }
            }

            try
            {
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                LMS.ErrorHandler.HandleError.EmailError(ex);
                return RedirectToAction("Annuity", new { id = id });
            }

            foreach (string key in collection.AllKeys)
            {
                if (key.StartsWith("DateType"))
                {

                    int PlanTransactionDateID = Convert.ToInt32(key.Replace("DateType", ""));
                    Plan_Transaction_Date date = db.Plan_Transaction_Dates.First(d => d.Plan_Transaction_Date_ID == PlanTransactionDateID);

                    if (collection[key] != "")
                    {
                        date.Date_Value = new DateTime?(Convert.ToDateTime(collection[key]));
                    }
                    else
                    {
                        date.Date_Value = null;
                    }
                    db.SubmitChanges();
                }
            }

            // Check for Assign
            if (collection.AllKeys.Contains("New.x"))
            {


                // Create New Plan
                Plan_Transaction NewPlan = new Plan_Transaction();

                NewPlan.Agent_ID = transaction.Agent_ID;
                NewPlan.Special_Instructions = transaction.Special_Instructions;
                NewPlan.Plan_Transaction_Status_ID = 17;
                NewPlan.Created_On = DateTime.Now;
                NewPlan.Group_ID = transaction.Group_ID;
                NewPlan.Record_ID = transaction.Record_ID;
                NewPlan.Source_Code_ID = transaction.Source_Code_ID;
                NewPlan.Outbound_Shipping_Method_ID = transaction.Outbound_Shipping_Method_ID;
                NewPlan.Inbound_Shipping_Method_ID = transaction.Inbound_Shipping_Method_ID;
                NewPlan.Active = false;
                NewPlan.Cash_With_Application = false;
                NewPlan.Cash_With_Application_Check = false;

                db.Plan_Transactions.InsertOnSubmit(NewPlan);

                db.SubmitChanges();

                if (NewPlan.Plan_Transaction_ID > 0)
                {
                    db.CreatePlanTransactionDates(NewPlan.Plan_Transaction_ID);

                    //db.Copy_Date_Values(transaction.Plan_Transaction_ID, NewPlan.Plan_Transaction_ID);

                    return RedirectToAction("Annuity", "Transaction", new { id = NewPlan.Plan_Transaction_ID });
                }

            }

            return RedirectToAction("Details", "Record", new { id = transaction.Record_ID });
        }

        public ActionResult Life(int id)
        {
            DocumentRepository docRepository = new DocumentRepository();
            Plan_Transaction transaction = _transactionRepository.Get(id);
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            LMS.Data.User user = UserRepository.GetUser(User.Identity.Name);
            Record record = db.Records.SingleOrDefault(rec => rec.Record_ID == transaction.Record_ID);

            ViewData["Record"] = record;

            //Check Status and forward to View Only if Status in (New,Request Application, or Request Info/Quote)
            if (transaction.Plan_Transaction_Status_ID.HasValue)
                if (transaction.Plan_Transaction_Status_ID.Value != 17 && transaction.Plan_Transaction_Status_ID.Value != 20 && transaction.Plan_Transaction_Status_ID.Value != 21)
                    if (user.Security_Level_ID == 1)
                        return RedirectToAction("LifeView", new { id = id });


            // Get Carrier Drop Down List
            if (transaction.Plan_ID > 0)
            {
                ViewData["Carrier_ID"] = new SelectList(_dropRepository.GetCarriers(transaction.Group_ID, true), "Carrier_ID", "Name", transaction.Plan.Carrier_ID);
                ViewData["Plan_Document_ID"] = new SelectList(db.Plan_Documents.Where(pd => pd.Plan_ID == transaction.Plan_ID && pd.State_ID == record.State_ID), "Plan_Document_ID", "Title");
            }
            else
            {
                ViewData["Carrier_ID"] = new SelectList(_dropRepository.GetCarriers(transaction.Group_ID, true), "Carrier_ID", "Name", -1);
            }

            //  Get Plans Drop Down List
            ViewData["Plan_ID"] = new CascadingSelectList(_dropRepository.GetPlans(true), "Carrier_ID", "Plan_ID", "Name", transaction.Plan_ID.ToString());

            //  Get Transaction Dates
            ViewData["TransactionDates"] = _transactionRepository.GetDateTypes(transaction.Group_ID);

            ViewData["Inbound_Shipping_Method_ID"] = new SelectList(_dropRepository.GetShippingMethods(true), "Shipping_Method_ID", "Description", transaction.Inbound_Shipping_Method_ID);


            ViewData["Outbound_Shipping_Method_ID"] = new SelectList(_dropRepository.GetShippingMethods(true), "Shipping_Method_ID", "Description", transaction.Outbound_Shipping_Method_ID);

            //Plan Status
            ViewData["Plan_Transaction_Status_ID"] = new SelectList(_dropRepository.GetTransactionStatus(false), "Plan_Transaction_Status_ID", "Description", transaction.Plan_Transaction_Status_ID);

            // Get Modals
            ViewData["Modal_ID"] = new SelectList(_dropRepository.GetModals(true), "Modal_ID", "Description", transaction.Modal_ID);

            ViewData["DateTypes"] = _transactionRepository.GetDateTypes(transaction.Group_ID);

            ViewData["Underwriting_Class_ID"] = new SelectList(_dropRepository.GetUnderwritingClass(true), "Underwriting_ID", "Description", transaction.Underwriting_Class_ID);

            ViewData["Exam_Service_ID"] = new SelectList(_dropRepository.GetExamService(true), "Exam_Service_ID", "Description", transaction.Exam_Service_ID);

            ViewData["NoteHelpers"] = new SelectList(_dropRepository.GetUserNotes(UserRepository.GetUser(User.Identity.Name).User_ID), "Note", "Note");

            ViewData["Agent_ID"] = new SelectList(_dropRepository.GetAgents(true), "User_ID", "FullName", transaction.Agent_ID);

            ViewData["Payment_Mode_ID"] = new SelectList(_dropRepository.GetPaymentModes(true), "Payment_Mode_ID", "Description", transaction.Payment_Mode_ID);

            ViewData["Source_Code_ID"] = new SelectList(_dropRepository.GetSourceCodes(true), "Source_Code_ID", "DropDownName", transaction.Source_Code_ID);

            EmailDocumentModel emailModel = new EmailDocumentModel();
            emailModel.Body = GetEmailBody(record, user);
            emailModel.FromAddress = user.Email_Address;
            emailModel.ToAddress = record.Email_Address;
            emailModel.Subject = "Plan Documents";
            emailModel.Plan_Transaction_ID = transaction.Plan_Transaction_ID;
            emailModel.Documents = docRepository.GetAllByPlan(transaction.Plan_ID, record.State_ID);
            ViewData["AllPlanDocuments"] = emailModel;

            return View(transaction);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Life(int id, FormCollection collection)
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            Plan_Transaction transaction = db.Plan_Transactions.Where(pt => pt.Plan_Transaction_ID == id).FirstOrDefault();

            try
            {
                if (collection["Created_On"] != null)
                {
                    transaction.Created_On = DateTime.Parse(collection["Created_On"]);
                }
                transaction.Agent_ID = Int32.Parse(collection["Agent_ID"]);
                transaction.Source_Code_ID = Int32.Parse(collection["Source_Code_ID"]);
                transaction.Plan_Transaction_Status_ID = Int32.Parse(collection["Plan_Transaction_Status_ID"]);
                transaction.eApp = collection["eApp"].Contains("true");
                transaction.Plan_ID = Int32.Parse(collection["Plan_ID"]);
                decimal da;
                if (Decimal.TryParse(collection["Coverage_Amount"], out da))
                {
                    transaction.Coverage_Amount = da;
                }
                else
                {
                    transaction.Coverage_Amount = 0.00m;
                }
                transaction.Underwriting_Class_ID = Int32.Parse(collection["Underwriting_Class_ID"]);
                decimal mp;
                if (Decimal.TryParse(collection["Modal_Premium"], out mp))
                {
                    transaction.Modal_Premium = mp;
                }
                else
                {
                    transaction.Modal_Premium = 0.00m;
                }
                transaction.Modal_ID = Int32.Parse(collection["Modal_ID"]);
                decimal ap;
                if (Decimal.TryParse(collection["Annual_Premium"], out ap))
                {
                    transaction.Annual_Premium = ap;
                }
                else
                {
                    transaction.Annual_Premium = 0.00m;
                }
                DateTime bped;
                if (DateTime.TryParse(collection["Status_Date"], out bped))
                {
                    transaction.Status_Date = DateTime.Parse(collection["Status_Date"]);
                }
                else
                {
                    transaction.Status_Date = null;
                }
                transaction.Cash_With_Application = collection["Cash_With_Application"].Contains("true");
                transaction.Payment_Mode_ID = Int32.Parse(collection["Payment_Mode_ID"]);
                transaction.Replacement = collection["Replacement"].Contains("true");
                transaction.Replacement_Details = collection["Replacement_Details"];
                transaction.Outbound_Shipping_Method_ID = Int32.Parse(collection["Outbound_Shipping_Method_ID"]);
                transaction.Tracking_Number_Outbound = collection["Tracking_Number_Outbound"];
                transaction.Inbound_Shipping_Method_ID = Int32.Parse(collection["Inbound_Shipping_Method_ID"]);
                transaction.Tracking_Number_Inbound = collection["Tracking_Number_Inbound"];
                transaction.Exam_Service_ID = Int32.Parse(collection["Exam_Service_ID"]);
                transaction.Exam_Ref_Number = collection["Exam_Ref_Number"];
                transaction.Policy_Number = StringHelper.EncryptString(collection["Policy_Number"]);
                transaction.Special_Instructions = collection["Special_Instructions"];
                transaction.Exam_Details = collection["Exam_Details"];

            }
            catch (Exception ex)
            {
                if (collection["Annual_Premium"] != null)
                    if (collection["Annual_Premium"] == "")
                    {
                        transaction.Annual_Premium = 0;
                    }
                    else
                    {
                        transaction.Annual_Premium = Convert.ToDecimal(collection["Annual_Premium"].Replace("$", "").Replace(",", ""));
                    }

                if (collection["Modal_Premium"] != null)
                    if (collection["Modal_Premium"] == "")
                    {
                        transaction.Modal_Premium = 0;
                    }
                    else
                    {
                        transaction.Modal_Premium = Convert.ToDecimal(collection["Modal_Premium"].Replace("$", "").Replace(",", ""));
                    }

                if (collection["Coverage_Amount"] != null)
                    if (collection["Coverage_Amount"] == "")
                    {
                        transaction.Coverage_Amount = 0;
                    }
                    else
                    {
                        transaction.Coverage_Amount = Convert.ToDecimal(collection["Coverage_Amount"].Replace("$", "").Replace(",", ""));
                    }

            }

            try
            {
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                LMS.ErrorHandler.HandleError.EmailError(ex);
                return RedirectToAction("Life", new { id = id });
            }

            foreach (string key in collection.AllKeys)
            {
                if (key.StartsWith("DateType"))
                {

                    int PlanTransactionDateID = Convert.ToInt32(key.Replace("DateType", ""));
                    Plan_Transaction_Date date = db.Plan_Transaction_Dates.First(d => d.Plan_Transaction_Date_ID == PlanTransactionDateID);

                    if (collection[key] != "")
                    {
                        date.Date_Value = new DateTime?(Convert.ToDateTime(collection[key]));

                    }
                    else
                    {
                        date.Date_Value = null;
                    }
                    db.SubmitChanges();
                }
            }

            // Check for Assign
            if (collection.AllKeys.Contains("New.x"))
            {


                // Create New Plan
                Plan_Transaction NewPlan = new Plan_Transaction();

                NewPlan.Agent_ID = transaction.Agent_ID;
                NewPlan.Special_Instructions = transaction.Special_Instructions;
                NewPlan.Plan_Transaction_Status_ID = 17;
                NewPlan.Created_On = DateTime.Now;
                NewPlan.Group_ID = transaction.Group_ID;
                NewPlan.Record_ID = transaction.Record_ID;
                NewPlan.Source_Code_ID = transaction.Source_Code_ID;
                NewPlan.Outbound_Shipping_Method_ID = transaction.Outbound_Shipping_Method_ID;
                NewPlan.Inbound_Shipping_Method_ID = transaction.Inbound_Shipping_Method_ID;
                NewPlan.Active = false;
                NewPlan.Cash_With_Application = false;
                NewPlan.Cash_With_Application_Check = false;

                db.Plan_Transactions.InsertOnSubmit(NewPlan);

                db.SubmitChanges();

                if (NewPlan.Plan_Transaction_ID > 0)
                {
                    db.CreatePlanTransactionDates(NewPlan.Plan_Transaction_ID);

                    //db.Copy_Date_Values(transaction.Plan_Transaction_ID, NewPlan.Plan_Transaction_ID);

                    return RedirectToAction("Life", "Transaction", new { id = NewPlan.Plan_Transaction_ID });
                }

            }

            return RedirectToAction("Details", "Record", new { id = transaction.Record_ID });
        }

        public ActionResult AnnuityView(int id)
        {
            Plan_Transaction transaction = _transactionRepository.Get(id);
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

            Record record = db.Records.SingleOrDefault(rec => rec.Record_ID == transaction.Record_ID);

            ViewData["Record"] = record;

            return View(transaction);
        }

        public ActionResult HealthView(int id)
        {
            Plan_Transaction transaction = _transactionRepository.Get(id);
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

            Record record = db.Records.SingleOrDefault(rec => rec.Record_ID == transaction.Record_ID);

            ViewData["Record"] = record;

            return View(transaction);
        }

        public ActionResult LifeView(int id)
        {
            Plan_Transaction transaction = _transactionRepository.Get(id);
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

            Record record = db.Records.SingleOrDefault(rec => rec.Record_ID == transaction.Record_ID);

            ViewData["Record"] = record;

            return View(transaction);
        }

        public ActionResult QuickAdd(int id)
        {
            var plans = _transactionRepository.GetRecordPlans(id);

            return View(plans);
        }

        //public ActionResult GetPDF(int id)
        //{
        //    LMSDataContext DB = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

        //    Plan_Document doc = DB.Plan_Documents.Where(pd => pd.Plan_Document_ID == id).FirstOrDefault();

        //    string filename = "Documents/" + id.ToString() + ".pdf";

        //    string savedFileName = Path.Combine(
        //             AppDomain.CurrentDomain.BaseDirectory + @"documents",
        //             id.ToString() + ".pdf");

        //    return File(savedFileName, "application/pdf", Server.HtmlEncode(doc.Title + ".pdf"));
        //}

        public ActionResult GetPDF(int id)
        {
            LMSDataContext DB = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

            Plan_Document doc = DB.Plan_Documents.Where(pd => pd.Plan_Document_ID == id).FirstOrDefault();

            string filename = "Documents/" + doc.Location;

            string savedFileName = Path.Combine(
                     AppDomain.CurrentDomain.BaseDirectory + @"documents",
                     id.ToString() + ".pdf");

            return File(savedFileName, "application/pdf", Server.HtmlEncode(doc.Location));
        }

        public bool IsActive(int id)
        {
            LMSDataContext DB = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            Plan_Transaction transaction = _transactionRepository.Get(id);
            Record r = DB.Records.Where(rc => rc.Record_ID.Equals(transaction.Record_ID)).FirstOrDefault();


            if (r != null)
            {
                if (r.Record_Type_ID > 4)
                    return false;
            }

            if (transaction != null)
            {
                if (transaction.Plan_Transaction_Status_ID == 17 ||
                    transaction.Plan_Transaction_Status_ID == 19 ||
                    transaction.Plan_Transaction_Status_ID == 20 ||
                    transaction.Plan_Transaction_Status_ID == 21
                    )
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }

        private string GetEmailBody(Record record, User user)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(string.Format("Dear {0} {1},", record.First_Name, record.Last_Name));
            sb.Append(System.Environment.NewLine);
            sb.Append(System.Environment.NewLine);
            sb.Append(string.Format("Attached are the documents discussed for your review.  Applications, once completed and signed, can either be faxed to my attention at 480-897-9599 or emailed to me at {0}.  Of course, I�m here to answer any questions you may have.", user.Email_Address));
            sb.Append(System.Environment.NewLine);
            sb.Append(System.Environment.NewLine);
            sb.Append("Thank you for the opportunity to be of service.");
            sb.Append(System.Environment.NewLine);
            sb.Append(System.Environment.NewLine);
            sb.Append("Sincerely,");
            sb.Append(System.Environment.NewLine);
            sb.Append(System.Environment.NewLine);
            sb.Append(user.First_Name + " " + user.Last_Name);
            sb.Append(System.Environment.NewLine);
            sb.Append("Longevity Alliance, Inc.");
            sb.Append(System.Environment.NewLine);
            sb.Append("5530 W. Chandler Blvd.");
            sb.Append(System.Environment.NewLine);
            sb.Append(user.Email_Address);
            sb.Append(System.Environment.NewLine);
            sb.Append(string.Format("1-800-713-6610 {0}", user.Extension));
            return sb.ToString();
        }




        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Request(Plan_Transaction_Request request)
        {

            return Json("Success");
        }
    }
}
