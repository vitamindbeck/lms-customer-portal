﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Data.Linq;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Net.Mail;
using LMS.Data;


namespace LMS
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        LMSDataContext _db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "AjaxEmailDocuments", // Route name
                "Document/EmailView/{RecordID}/{PlanTransactionID}/{PlanID}/{StateID}",  // URL with parameters        
                new { controller = "Document", action = "EmailView" }  // Parameter defaults    
            );
            routes.MapRoute(
                "AjaxGetDocuments", // Route name
                "Document/AjaxGet/{id}/{state}",  // URL with parameters        
                new { controller = "Document", action = "AjaxGet" }  // Parameter defaults    
            );
            routes.MapRoute(
                "AjaxGetListDocuments", // Route name
                "Document/DocumentList/{id}/{state}",  // URL with parameters        
                new { controller = "Document", action = "DocumentList" }  // Parameter defaults    
            );
            routes.MapRoute(
                "LinkPartner", // Route name
                "Record/Link/Partner",  // URL with parameters        
                new { controller = "Record", action = "Partner" }  // Parameter defaults    
            );

            routes.MapRoute(
                "PrintLetterRoute", // Route name
                "Activity/PrintLetter/{id}/{recid}",  // URL with parameters        
                new { controller = "Activity", action = "PrintLetter" }  // Parameter defaults    
            );

            routes.MapRoute(
                "NewActivity", // Route name
                "Activity/new/{id}/{Plan_Transaction_ID}",  // URL with parameters        
                new { controller = "Activity", action = "New" }  // Parameter defaults    
            );

            routes.MapRoute(
                "DocumentListByPlanState", // Route name
                "Document/List/{state}/{planid}",  // URL with parameters        
                new { controller = "Document", action = "List", state ="", planid = "" }  // Parameter defaults    
            );

            routes.MapRoute(
                "PrintLetterHideRoute", // Route name
                "Activity/PrintLetterHide/{id}/{recid}",  // URL with parameters        
                new { controller = "Activity", action = "PrintLetterHide" }  // Parameter defaults    
            );
         
            routes.MapRoute(
                "UserOnlineStatus", // Route name
                "Record/Refresh/{statusid}",  // URL with parameters        
                new { controller = "Record", action = "Refresh", statusid = 1 }  // Parameter defaults    
            );

            routes.MapRoute(
                "HasActivity", // Route name
                "Record/HasActivity/{recordid}/{userid}",  // URL with parameters        
                new { controller = "Record", action = "HasActivity" }  // Parameter defaults    
            );

            routes.MapRoute(
               "GetRulesResult",
               "Wizzer/GetRulesResult/{activityId}/{statusId}/{leadDispositionId}",
               new { controller = "Wizzer", action = "GetRulesResult" }
             );

            routes.MapRoute(
               "CheckForFollowUp",
               "Wizzer/CheckForFollowUp/{activityId}/{statusId}/{leadDispositionId}",
               new { controller = "Wizzer", action = "CheckForFollowUp" }
             );

            routes.MapRoute(
               "Default",
               "{controller}/{action}/{id}",
               new { action = "Index", id = "" }
             );

            

            routes.MapRoute(
              "Root",
              "",
              new { controller = "Home", action = "Index", id = "" }
            );

        }

        public override string GetVaryByCustomString(HttpContext context, string arg)
        {
            if (arg == "User")
            {
                // depends on your authentication mechanism
                return "User=" + context.User.Identity.Name;
                //?return "User=" + context.Session.SessionID;
            }

            return base.GetVaryByCustomString(context, arg);
        }

        protected void Application_Start()
        {
            RegisterRoutes(RouteTable.Routes);
            Bootstrapper.Bootstrap();

        }

        protected void Session_Start(object sender, EventArgs e)
        {
            Session.RemoveAll();
            if (User != null)
            {
                User u = _db.Users.Where(user => user.User_Name == User.Identity.Name.Replace(@"LONGEVITY\", "")).FirstOrDefault();
                if (u != null)
                {
                    u.Online_Status_ID = 1; //online
                    u.SessionId = Session.SessionID;//update session id
                    _db.SubmitChanges(ConflictMode.ContinueOnConflict);
                }
           }
        }

        protected void Session_End(object sender, EventArgs e)
        {
            if (User != null)
            {
                User u = _db.Users.Where(user => user.User_Name == User.Identity.Name.Replace(@"LONGEVITY\", "")).FirstOrDefault();
                if (u != null)
                {
                    u.Online_Status_ID = 2; //offline
                    _db.SubmitChanges();
                }
            }
        }

        protected void Application_EndRequest(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}