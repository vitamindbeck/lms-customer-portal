<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<LMS.Data.Letter>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
        <%= Model.Description %></title>
    <style media="print">
        .hide
        {
            display: none;
        }
    </style>
</head>
<body>
    <div style="font-family: Century Gothic; font-size: 14px;">
        <img src="../../../../Content/icons/printer.png" id="PrintButton" alt="Print" class="hide"
            onclick="javascript:window.print();" />
        <br />
        <% LMS.Data.Record record = (LMS.Data.Record)ViewData["Record"]; %>
        <% LMS.Data.User agent = (LMS.Data.User)ViewData["Agent"]; %>
        <br />
        <br />
        <%= Html.Encode(DateTime.Now.ToShortDateString()) %>
        <br />
        <br />
        <%= Html.Encode(record.First_Name + " " + record.Last_Name) %>
        <br />
        <%= Html.Encode(record.Address_1) %>
        <br />
        <% if (record.Address_2.Length > 0)
           {%>
        <%= Html.Encode(record.Address_2) %>
        <br />
        <%} %>
        <%= Html.Encode(record.City + ", " + record.State.Abbreviation + " " + record.Zipcode) %>
        <br />
        <br />
        Dear
        <%= Html.Encode(record.First_Name + " " + record.Last_Name) %>,
        <br />
        <br />
        <%= Model.Body %>
        <br />
        <br />
        <div style="font-family: Blackadder ITC; font-size: 20px;">
            <%= Html.Encode(agent.First_Name + " " + agent.Last_Name) %>
        </div>
        <%= Html.Encode(agent.First_Name + " " + agent.Last_Name) %>
        <br />
        <%= Html.Encode(agent.Phone) %>
        <% if (agent.Extension.Length > 0)
           { %>
        ext.
        <%= Html.Encode(agent.Extension) %>
        <%} %>
        <br />
        <%= Html.Encode(agent.Email_Address) %>
    </div>
</body>
</html>
