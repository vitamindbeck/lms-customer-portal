<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Licensing.Master" Inherits="System.Web.Mvc.ViewPage<LMS.Data.User_Password>" %>
<%@ Import Namespace="LMS.Helpers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Edit Login Information
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<script type="text/javascript">
    $(function() {
        $('#EditLogininfo').load()
        {
            if ($("#ckOther").is(":checked")) {
                $("#other").show();
                $("#noncarrier1").show();
                $("#noncarrier2").show();
                $("#carrier1").hide();
                $("#carrier2").hide();
                $("#state1").hide();
                $("#state2").hide();
                $("#State_ID option[value='52']").attr('selected', 'selected');
                $("#Carrier_ID option[value='-1']").attr('selected', 'selected');
            }
            else {
                if ($("#State_ID").val() == "52") {
                    $("#Non_Carrier_ID option[value='-1']").attr('selected', 'selected');
                    $("#carrier1").show();
                    $("#carrier2").show();
                    $("#other").show();
                    $("#noncarrier1").hide();
                    $("#noncarrier2").hide();
                    if ($("#Carrier_ID").val() == "-1") {
                        $("#state1").show();
                        $("#state2").show();
                    } else {
                        $("#state1").hide();
                        $("#state2").hide();
                    }
                } else {
                    $("#other").hide();
                    $("#state1").show();
                    $("#state2").show();
                    $("#noncarrier1").hide();
                    $("#noncarrier2").hide();
                    $("#carrier1").hide();
                    $("#carrier2").hide();
                    $("#Carrier_ID option[value='-1']").attr('selected', 'selected');
                    $("#Non_Carrier_ID option[value='-1']").attr('selected', 'selected');
                }
            }
        };

        $("#ckOther").click(function() {
            if ($("#ckOther").is(":checked")) {
                $("#noncarrier1").show();
                $("#noncarrier2").show();
                $("#carrier1").hide();
                $("#carrier2").hide();
                $("#state1").hide();
                $("#state2").hide();
                $("#State_ID option[value='52']").attr('selected', 'selected');
                $("#Non_Carrier_ID option[value='-1']").attr('selected', 'selected');
                $("#Carrier_ID option[value='-1']").attr('selected', 'selected');
            }
            else {
                $("#carrier1").show();
                $("#carrier2").show();
                $("#noncarrier1").hide();
                $("#noncarrier2").hide();
                $("#state1").show();
                $("#state2").show();
                $("#State_ID option[value='52']").attr('selected', 'selected');
                $("#Non_Carrier_ID option[value='-1']").attr('selected', 'selected');
                $("#Carrier_ID option[value='-1']").attr('selected', 'selected');
            }
        });

        $("#State_ID").change(function() {
            if ($("#State_ID").val() != "52") {
                $("#noncarrier1").hide();
                $("#noncarrier2").hide();
                $("#carrier1").hide();
                $("#carrier2").hide();
                $("#other").hide();
                $("#Non_Carrier_ID option[value='-1']").attr('selected', 'selected');
                $("#Carrier_ID option[value='-1']").attr('selected', 'selected');
            }
            else {
                $("#carrier1").show();
                $("#carrier2").show();
                $("#other").show();
                $("#noncarrier1").hide();
                $("#noncarrier2").hide();
                $("#ckOther").removeAttr("checked");
                $("#Non_Carrier_ID option[value='-1']").attr('selected', 'selected');
                $("#Carrier_ID option[value='-1']").attr('selected', 'selected');
            }
        });

        $("#Carrier_ID").change(function() {
            if ($("#Carrier_ID").val() != "-1") {
                $("#state1").hide();
                $("#state2").hide();
            }
            else {
                $("#state1").show();
                $("#state2").show();
                $("#State_ID option[value='52']").attr('selected', 'selected');
            }
        });
    });

</script>

<%= Html.ValidationSummary("Unable to Create Record.") %>
<div class="client-outer" style="background-color: #e0e4ee;">
 <% using (Html.BeginForm("Edit", "LoginInfo", FormMethod.Post, new { id = "EditLogininfo" }))
      
    {%>
    <div class="section-title">
        Update Login Information
    </div>
    <div class="section-inner">
        <table class="section" cellpadding="0" cellspacing="0">
          <tr>
            <td class="label">
                Agent:
            </td>
            <td>
              <%= Html.DropDownList("Agent_ID", (SelectList)ViewData["Agent_Name"], new { style = "width: 150px;", tabindex = "1" })%>
              
            </td>
            <td class="label">
                <span id="carrier1">Carrier:</span>
            </td>
            <td>
               <span id="carrier2">
                 <%= Html.DropDownList("Carrier_ID", (SelectList)ViewData["Carrier"], new { style = "width: 280px;", tabindex = "2" })%>
               </span>
               <span id="other">
               <%if (!String.IsNullOrEmpty(Request.Form["ckOther"]))
                   { %>
                        Other? <%=Html.CheckBox("ckOther", (Request.Form["ckOther"] ?? string.Empty).Contains("true"))%>
                 <%}
                   else
                   {%>
                        Other? <%=Html.CheckBox("ckOther", Model.Carrier_ID < 1 && Model.State_ID == 52)%>
                 <%} %>
                 </span>
            </td>
          </tr>
          <tr>
            <td class="label">
                <span id="state1">
                State:
                </span>
            </td>
            <td>
                <span id="state2">
                <%= Html.DropDownList("State_ID", (SelectList)ViewData["State"], new { tabindex = "2" })%>
                </span>
                
            </td>
            <td class="label">
              <span id="noncarrier1">
                Non-Carrier:
              </span>
            </td>
            <td>
              <span id="noncarrier2">
                <%= Html.DropDownList("Non_Carrier_ID", (SelectList)ViewData["NonCarrier"])%>
              </span>
            </td>
          </tr>
          <tr>
            <td class="label">
                User Name:
            </td>
            <td>
            
                <%= Html.TextBox("User_Name", StringHelper.DecryptData(Model.User_Name), new { style = "width: 150px;", tabindex = "3" })%>
                
           </td>
            <td class="label">
               Password:
            </td>
            <td>
               
                <%= Html.TextBox("Password", StringHelper.DecryptData(Model.Password), new { style = "width: 150px;", tabindex = "4" })%>
            </td>
          </tr>
          <tr>
            <td class="label">
               Carrier Site Link:
            </td>
            <td>
                <%= Html.TextBox("Carrier_Site_Link", Model.Carrier_Site_Link, new { style = "width: 150px;", tabindex = "5" })%>
           </td>
         
          </tr>
         
          
        </table>
    </div>
    <div style="text-align: center;">
        <input type="image" src="../../Content/icons/check.png" title="Save" alt="Save" id="saveBtn" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="../../loginInfo/List" alt="Cancel"><img src="../../content/icons/close.png" alt="Cancel" title="Cancel" style="border: 0px;
                vertical-align: top;" />
        </a>
    </div>
<%} %>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>


