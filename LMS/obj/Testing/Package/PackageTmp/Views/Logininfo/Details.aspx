<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Licensing.Master" Inherits="System.Web.Mvc.ViewPage<LMS.Data.User_Password>" %>
<%@ Import Namespace="LMS.Helpers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Login Information Detail
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
 
  

<div style="text-align: left;">
<% LMS.Data.User user = LMS.Data.Repository.UserRepository.GetUser(Page.User.Identity.Name); %>
<%if (LMS.Data.Repository.UserRepository.HasSecurityLevel(user, 10))
  {%>       
        <a href="../../LoginInfo/Edit/<%= Model.User_Password_ID %>" style="text-decoration: none;">
            <img src="../../content/icons/document_edit.png" title="Edit" alt="Edit" style="border: 0px;
                vertical-align: middle;" />
        </a>
<%} %>     
</div>
    <br/>
<div id ="LoginInfoDetails" class="client-outer" style="background-color: #e0e4ee;">
    <div class="section-title">
       Login Information Details
    </div>
    <div class="section-inner">
        <table class="section" cellpadding="0" cellspacing="0">
          <tr>
            <td class="label">
                Agent:
            </td>
            <td>
                <%= ViewData["Agent_Name"]%>
            </td>
             <td class="label">
                Carrier:
            </td>
            <td>
                <%= ViewData["Carrier"]%>
            </td>
           
          </tr>
          <tr>
            <td class="label">
                State:
            </td>
            <td>
                <%=ViewData["State"]%>
            </td>
            <td class="label">
                Non-Carrier:
            </td>
            <td>
                <%=ViewData["NonCarrier"]%>
            </td>
          </tr>
          <tr>
           <td class="label">
               User Name:
            </td>
            <td>
                <%= Html.Encode(StringHelper.DecryptData(Model.User_Name)) %>
                
            </td>
            <td class="label">
              Password:
            </td>
            <td>
             <%= Html.Encode(StringHelper.DecryptData(Model.Password)) %>
            </td>
           
          </tr>
          <tr>
           <td class="label">
                Carrier Site Link:
            </td>
            <td>
               <a href="<%= Html.Encode(Model.Carrier_Site_Link) %>" target=_blank"><%= Html.Encode(Model.Carrier_Site_Link) %></a>
            </td>
             <td class="label">
               Last Updated:
            </td>
            <td>
                <%= Html.Encode(Model.Last_Updated_Date) %>
                &nbsp;
                <%if (!String.IsNullOrEmpty(ViewData["UserName"].ToString()))
                  { %>
                <strong>By:</strong>&nbsp;
                <%} %>
                <%= ViewData["UserName"] %>
            </td>
          </tr>
        </table>
    </div>
</div>



</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptContent" runat="server">
</asp:Content>
