﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/LogOnMaster.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="loginTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Log On
</asp:Content>
<asp:Content ID="loginContent" ContentPlaceHolderID="MainContent" runat="server">
    <div style="padding: 5px ;">
        <%= Html.ValidationSummary("Login was unsuccessful. Please correct the errors and try again.") %>
        <% using (Html.BeginForm())
           { %>
        <div>
            <p>
                <label for="username">
                    Username:</label>
                <%= Html.TextBox("username", "", new { style = " width: 150px;" })%>
            </p>
            <p>
                <label for="password">
                    Password:</label>
                <%= Html.Password("password", "", new { style = "width: 150px;" })%>
            </p>
            <p>
                <input type="submit" value="Log On" />
            </p>
        </div>
        <% } %>
    </div>
</asp:Content>
