<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	TestGrid
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

      <script type="text/javascript">
        $(function() {
            jQuery("#list").jqGrid({
            url: '/Record/GridData/',
                datatype: 'json',
                mtype: 'GET',
                colNames: ['Id', 'Votes', 'Title'],
                colModel: [
          { name: 'Record_ID', index: 'Record_ID', width: 100, align: 'left' },
              { name: 'FullName', index: 'FullName', width: 400, align: 'left' },
              { name: 'Priority', index: 'Priority', width: 200, align: 'left'}],
                pager: jQuery('#pager'),
                rowNum: 30,
                rowList: [5, 10, 20, 50],
                sortname: 'Record_ID',
                sortorder: "desc",
                viewrecords: true,
                imgpath: '/scripts/themes/coffee/images',
                caption: 'My first grid'
            });
        }); 
    </script>
    
    <h2>
        My Grid Data</h2>
    <table id="list" class="scroll" cellpadding="0" cellspacing="0">
    </table>
    <div id="pager" class="scroll" style="text-align: center;">
    </div>

</asp:Content>
