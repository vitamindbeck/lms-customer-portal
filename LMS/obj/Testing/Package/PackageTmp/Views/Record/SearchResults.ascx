<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<LMS.Data.Record>>" %>
<table class="sortable" width="100%" cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th width="200px">
                Submit Date
            </th>
            <th>
            </th>
            <th>
                Name
            </th>
            <th>
                Customer ID
            </th>
            <th>
                State
            </th>
            <th>
                Source
            </th>
            <th>
                Follow Up Date
            </th>
            <th>
                Priority
            </th>
        </tr>
    </thead>
    <tbody>
        <% foreach (var item in Model)
           { %>
        <tr>
            <td>
                <%= Html.Encode(item.Created_On) %>
            </td>
            <td>
                <%= Html.ActionLink("Edit", "Edit", new { id=item.Record_ID }) %>
                |
                <%= Html.ActionLink("Details", "Details", new { id=item.Record_ID })%>
            </td>
            <td>
                <%= Html.Encode(item.FullName) %>
            </td>
            <td>
                <%= Html.Encode(item.Customer_ID) %>
            </td>
            <td>
                <%= Html.Encode(item.State.Abbreviation) %>
            </td>
            <td>
                <%= Html.Encode(item.Source_Code.Source_Number) %>
            </td>
            <td>
                <%= Html.Encode(item.Follow_Up_Date) %>
            </td>
            <td>
                <%= Html.Encode(item.Priority_Level) %>
            </td>
        </tr>
        <% } %>
    </tbody>
</table>
<p>
    <%= Html.ActionLink("Create New", "Create") %>
</p>
