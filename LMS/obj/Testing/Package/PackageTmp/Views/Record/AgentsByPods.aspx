<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>


<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
 <div>
    <table width="100%" border="0">
       <tr>
        <td>
            Lead Status Based On Selected Pod
        </td>
        <td align = "right">
            <a href="/Record/Sales">Back To Sales Page</a>
        </td>
       </tr>
     </table>
   </div>
  <div>
    <table width="100%" border="0">
       <tr>
        <td>
         <% Html.RenderPartial("NewLeadListPod", ViewData["NewLeadsByPod"]); %>
        </td>
      </tr>
      <tr>
        <td>
          <% Html.RenderPartial("AppointmentFollowUpListPod", ViewData["AppointmentFollowUpsByPod"]); %>
        </td>
      </tr>
        <tr>
        <td>
           <% Html.RenderPartial("AppSentOpenActivitiesPod", ViewData["AppSentOpenActivitiesByPod"]); %>
        </td>
      </tr>
         <tr>
        <td>
         <% Html.RenderPartial("FollowUpListPod", ViewData["FollowUpsByPod"]); %>
        </td>
      </tr>
    </table>    
  </div>


</asp:Content>

