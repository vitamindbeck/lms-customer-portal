<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<LMS.Data.RequestDisplay>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    List
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="client-outer">
        <div class="section-title">
            [
            <% if (Model.Count() > 0)
               { %>
            <%= Model.Count().ToString()%>
            <%}
               else
               { %>
            No
            <%} %>
            Unanswered Questions ]
        </div>
        <div class="client-inner">
            <table class="sortable" width="100%" cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th>
                            Requested On
                        </th>
                        <th>
                            Name
                        </th>
                        <th>
                            Plan
                        </th>
                        <th>
                            Question
                        </th>
                        <th>
                            Sent To
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <% foreach (var item in Model.OrderBy(r => r.Requested_On))
                       { %>
                    <tr>
                    <td style="width: 160px;">
                            <%= Html.Encode(String.Format("{0:g}", item.Requested_On)) %>
                        </td>
                        <td style="width: 200px;">
                            <%= Html.Encode(item.Name)%>
                        </td>
                        <td style="width: 200px;">
                            <%= Html.ActionLink(item.DropDownName, "Details", "Record", new { id = item.Record_ID },null)%>
                        </td>
                        <td>
                            <%= Html.Encode(item.Question) %>
                        </td>
                        <td style="width: 200px;">
                            <%= Html.Encode(LMS.Data.Repository.UserRepository.GetUserByID(item.To_Employee_ID).FullName) %>
                        </td>                        
                    </tr>
                    <% } %>
                </tbody>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
