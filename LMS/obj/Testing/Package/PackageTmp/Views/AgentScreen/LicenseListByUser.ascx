<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<LMS.Data.sp_License_List_By_UserResult>>" %>

<%@ Import Namespace="Helpers" %>
   <div class="client-outer">
    <div class="client-title">
         [ <%= Html.Encode(Model.Count()) %> Licenses ]
    </div>
    <div class="client-inner">
    <table class="tablesorter" width="100%" cellpadding="0" cellspacing="0" id="sortable1">
        <thead>
            <tr>
            <th>State</th>
            <th>License #</th>
            <th>Effective Date</th>
            <th>Renewal Date</th>
            <th>Product</th>
            </tr>
        </thead>
        <tbody>
        <% foreach (LMS.Data.sp_License_List_By_UserResult rs in Model)
           { %>
        <tr>
            <td>
                <%= Html.Encode(rs.States) %>
            </td>
            <td>
                <%= Html.Encode(rs.License_Number) %>
            </td>
            <td>
                <%if (rs.Effective_Date != null)
                   { %>
                <%= Html.Encode(rs.Effective_Date.Value.ToShortDateString()) %>
                 <% } %>
            </td>
            <td>
               <% if (rs.Renewal_Date != null && DateTime.Compare(DateTime.Parse(rs.Renewal_Date.ToString()), DateTime.Today) < 0)
                   {%>
                   <font color="red">
                   <%= Html.Encode(rs.Renewal_Date.Value.ToShortDateString()) %>
                   </font>
                <% }
                   else if (rs.Renewal_Date != null)
                   { %>
                    <%= Html.Encode(rs.Renewal_Date.Value.ToShortDateString()) %>
                <% } %>
            </td>
            <td>
                <%= Html.Encode(rs.Line_Of_Insurance) %>
            </td>
        </tr>
        <%} %>
        </tbody>
    </table>
    </div>
</div>