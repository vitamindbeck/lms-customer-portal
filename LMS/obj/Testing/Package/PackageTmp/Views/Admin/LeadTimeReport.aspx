<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Lead Time Report
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <script type="text/javascript">
        $(function() {
            $("#StartDate").datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: '2000:2020'
            });
            $("#EndDate").datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: '2000:2020'
            });
        });
    </script>

    <% using (Html.BeginForm())
       { %>
    <table>
        <tr>
            <td>
                Start Time:
            </td>
            <td>
                <%= Html.TextBox("StartDate", "", new { style = "width: 90px;" })%>&nbsp;<%= Html.DropDownList("StartTime", (SelectList)ViewData["Times"])%>
            </td>
        </tr>
        <tr>
            <td>
                End Time:
            </td>
            <td>
                <%= Html.TextBox("EndDate", "", new { style = "width: 90px;" })%>&nbsp;<%= Html.DropDownList("EndTime", (SelectList)ViewData["Times"])%>
            </td>
        </tr>
        <tr>
            <td>
                Source:
            </td>
            <td>
                <%= Html.ListBox("Source_Code_ID", (SelectList)ViewData["Source_Code_ID"], new { name = "Source_Code_ID", width = "300px", size = "10" })%>
            </td>
        </tr>
        <tr>
            <td>
                Group:
            </td>
            <td>
                <%= Html.DropDownList("Group_ID")%>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <input type="submit" value="Run Report" name="RunReport" />
            </td>
        </tr>
    </table>
    <%} %>
    <% if (ViewData["ReportData"] != null)
       {
           List<LMS.Data.Report_Lead_Time_ReportResult> report = (List<LMS.Data.Report_Lead_Time_ReportResult>)ViewData["ReportData"];%>
    <br />
    <br />
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td>
                Total Leads:&nbsp;
            </td>
            <td class="reportheader">
                <%= Html.Encode(report.Sum(c => c.Number)) %>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
            </td>
            <td class="reportheader">
                12 AM
            </td>
            <td class="reportheader">
                1 AM
            </td>
            <td class="reportheader">
                2 AM
            </td>
            <td class="reportheader">
                3 AM
            </td>
            <td class="reportheader">
                4 AM
            </td>
            <td class="reportheader">
                5 AM
            </td>
            <td class="reportheader">
                6 AM
            </td>
            <td class="reportheader">
                7 AM
            </td>
            <td class="reportheader">
                8 AM
            </td>
            <td class="reportheader">
                9 AM
            </td>
            <td class="reportheader">
                10 AM
            </td>
            <td class="reportheader">
                11 AM
            </td>
            <td class="reportheader">
                12 PM
            </td>
            <td class="reportheader">
                1 PM
            </td>
            <td class="reportheader">
                2 PM
            </td>
            <td class="reportheader">
                3 PM
            </td>
            <td class="reportheader">
                4 PM
            </td>
            <td class="reportheader">
                5 PM
            </td>
            <td class="reportheader">
                6 PM
            </td>
            <td class="reportheader">
                7 PM
            </td>
            <td class="reportheader">
                8 PM
            </td>
            <td class="reportheader">
                9 PM
            </td>
            <td class="reportheader">
                10 PM
            </td>
            <td class="reportheader">
                11 PM
            </td>
        </tr>
        <tr>
            <td>
                #
            </td>
            <td class="reportdata">
                <%= Html.Encode(report.Where(c => c.HourOfDay==0).Sum(c => c.Number )) %>
            </td>
            <td class="reportdata">
                <%= Html.Encode(report.Where(c => c.HourOfDay == 1).Sum(c => c.Number))%>
            </td>
            <td class="reportdata">
                <%= Html.Encode(report.Where(c => c.HourOfDay == 2).Sum(c => c.Number))%>
            </td>
            <td class="reportdata">
                <%= Html.Encode(report.Where(c => c.HourOfDay ==3).Sum(c => c.Number))%>
            </td>
            <td class="reportdata">
                <%= Html.Encode(report.Where(c => c.HourOfDay == 4).Sum(c => c.Number))%>
            </td>
            <td class="reportdata">
                <%= Html.Encode(report.Where(c => c.HourOfDay == 5).Sum(c => c.Number))%>
            </td>
            <td class="reportdata">
                <%= Html.Encode(report.Where(c => c.HourOfDay == 6).Sum(c => c.Number))%>
            </td>
            <td class="reportdata">
                <%= Html.Encode(report.Where(c => c.HourOfDay == 7).Sum(c => c.Number))%>
            </td>
            <td class="reportdata">
                <%= Html.Encode(report.Where(c => c.HourOfDay == 8).Sum(c => c.Number))%>
            </td>
            <td class="reportdata">
                <%= Html.Encode(report.Where(c => c.HourOfDay==9).Sum(c => c.Number )) %>
            </td>
            <td class="reportdata">
                <%= Html.Encode(report.Where(c => c.HourOfDay==10).Sum(c => c.Number )) %>
            </td>
            <td class="reportdata">
                <%= Html.Encode(report.Where(c => c.HourOfDay==11).Sum(c => c.Number )) %>
            </td>
            <td class="reportdata">
                <%= Html.Encode(report.Where(c => c.HourOfDay==12).Sum(c => c.Number )) %>
            </td>
            <td class="reportdata">
                <%= Html.Encode(report.Where(c => c.HourOfDay == 13).Sum(c => c.Number))%>
            </td>
            <td class="reportdata">
                <%= Html.Encode(report.Where(c => c.HourOfDay == 14).Sum(c => c.Number))%>
            </td>
            <td class="reportdata">
                <%= Html.Encode(report.Where(c => c.HourOfDay == 15).Sum(c => c.Number))%>
            </td>
            <td class="reportdata">
                <%= Html.Encode(report.Where(c => c.HourOfDay == 16).Sum(c => c.Number))%>
            </td>
            <td class="reportdata">
                <%= Html.Encode(report.Where(c => c.HourOfDay == 17).Sum(c => c.Number))%>
            </td>
            <td class="reportdata">
                <%= Html.Encode(report.Where(c => c.HourOfDay == 18).Sum(c => c.Number))%>
            </td>
            <td class="reportdata">
                <%= Html.Encode(report.Where(c => c.HourOfDay == 19).Sum(c => c.Number))%>
            </td>
            <td class="reportdata">
                <%= Html.Encode(report.Where(c => c.HourOfDay == 20).Sum(c => c.Number))%>
            </td>
            <td class="reportdata">
                <%= Html.Encode(report.Where(c => c.HourOfDay == 21).Sum(c => c.Number))%>
            </td>
            <td class="reportdata">
                <%= Html.Encode(report.Where(c => c.HourOfDay == 22).Sum(c => c.Number))%>
            </td>
            <td class="reportdata" style="border-right: solid 1px Black;">
                <%= Html.Encode(report.Where(c => c.HourOfDay == 23).Sum(c => c.Number))%>
            </td>
        </tr>
        <tr>
            <td>
                %
            </td>
            <td class="reportdatabottom">
                <%= Html.Encode(string.Format("{0:P}", (double)report.Where(c => c.HourOfDay == 0).Sum(c => c.Number) / (double)report.Sum(c => c.Number)))%>
            </td>
            <td class="reportdatabottom">
                <%= Html.Encode(string.Format("{0:P}", (double)report.Where(c => c.HourOfDay == 1).Sum(c => c.Number) / (double)report.Sum(c => c.Number)))%>
            </td>
            <td class="reportdatabottom">
                <%= Html.Encode(string.Format("{0:P}", (double)report.Where(c => c.HourOfDay == 2).Sum(c => c.Number) / (double)report.Sum(c => c.Number)))%>
            </td>
            <td class="reportdatabottom">
                <%= Html.Encode(string.Format("{0:P}", (double)report.Where(c => c.HourOfDay == 3).Sum(c => c.Number) / (double)report.Sum(c => c.Number)))%>
            </td>
            <td class="reportdatabottom">
                <%= Html.Encode(string.Format("{0:P}", (double)report.Where(c => c.HourOfDay == 4).Sum(c => c.Number) / (double)report.Sum(c => c.Number)))%>
            </td>
            <td class="reportdatabottom">
                <%= Html.Encode(string.Format("{0:P}", (double)report.Where(c => c.HourOfDay == 5).Sum(c => c.Number) / (double)report.Sum(c => c.Number)))%>
            </td>
            <td class="reportdatabottom">
                <%= Html.Encode(string.Format("{0:P}", (double)report.Where(c => c.HourOfDay == 6).Sum(c => c.Number) / (double)report.Sum(c => c.Number)))%>
            </td>
            <td class="reportdatabottom">
                <%= Html.Encode(string.Format("{0:P}", (double)report.Where(c => c.HourOfDay == 7).Sum(c => c.Number) / (double)report.Sum(c => c.Number)))%>
            </td>
            <td class="reportdatabottom">
                <%= Html.Encode(string.Format("{0:P}", (double)report.Where(c => c.HourOfDay == 8).Sum(c => c.Number) / (double)report.Sum(c => c.Number)))%>
            </td>
            <td class="reportdatabottom">
                <%= Html.Encode(string.Format("{0:P}", (double)report.Where(c => c.HourOfDay == 9).Sum(c => c.Number) / (double)report.Sum(c => c.Number)))%>
            </td>
            <td class="reportdatabottom">
                <%= Html.Encode(string.Format("{0:P}", (double)report.Where(c => c.HourOfDay == 10).Sum(c => c.Number) / (double)report.Sum(c => c.Number)))%>
            </td>
            <td class="reportdatabottom">
                <%= Html.Encode(string.Format("{0:P}", (double)report.Where(c => c.HourOfDay == 11).Sum(c => c.Number) / (double)report.Sum(c => c.Number)))%>
            </td>
            <td class="reportdatabottom">
                <%= Html.Encode(string.Format("{0:P}", (double)report.Where(c => c.HourOfDay == 12).Sum(c => c.Number) / (double)report.Sum(c => c.Number)))%>
            </td>
            <td class="reportdatabottom">
                <%= Html.Encode(string.Format("{0:P}", (double)report.Where(c => c.HourOfDay == 13).Sum(c => c.Number) / (double)report.Sum(c => c.Number)))%>
            </td>
            <td class="reportdatabottom">
                <%= Html.Encode(string.Format("{0:P}", (double)report.Where(c => c.HourOfDay == 14).Sum(c => c.Number) / (double)report.Sum(c => c.Number)))%>
            </td>
            <td class="reportdatabottom">
                <%= Html.Encode(string.Format("{0:P}", (double)report.Where(c => c.HourOfDay == 15).Sum(c => c.Number) / (double)report.Sum(c => c.Number)))%>
            </td>
            <td class="reportdatabottom">
                <%= Html.Encode(string.Format("{0:P}", (double)report.Where(c => c.HourOfDay == 16).Sum(c => c.Number) / (double)report.Sum(c => c.Number)))%>
            </td>
            <td class="reportdatabottom">
                <%= Html.Encode(string.Format("{0:P}", (double)report.Where(c => c.HourOfDay == 17).Sum(c => c.Number) / (double)report.Sum(c => c.Number)))%>
            </td>
            <td class="reportdatabottom">
                <%= Html.Encode(string.Format("{0:P}", (double)report.Where(c => c.HourOfDay == 18).Sum(c => c.Number) / (double)report.Sum(c => c.Number)))%>
            </td>
            <td class="reportdatabottom">
                <%= Html.Encode(string.Format("{0:P}", (double)report.Where(c => c.HourOfDay == 19).Sum(c => c.Number) / (double)report.Sum(c => c.Number)))%>
            </td>
            <td class="reportdatabottom">
                <%= Html.Encode(string.Format("{0:P}", (double)report.Where(c => c.HourOfDay == 20).Sum(c => c.Number) / (double)report.Sum(c => c.Number)))%>
            </td>
            <td class="reportdatabottom">
                <%= Html.Encode(string.Format("{0:P}", (double)report.Where(c => c.HourOfDay == 21).Sum(c => c.Number) / (double)report.Sum(c => c.Number)))%>
            </td>
            <td class="reportdatabottom">
                <%= Html.Encode(string.Format("{0:P}", (double)report.Where(c => c.HourOfDay == 22).Sum(c => c.Number) / (double)report.Sum(c => c.Number)))%>
            </td>
            <td class="reportdatabottom" style="border-right: solid 1px Black;">
                <%= Html.Encode(string.Format("{0:P}", (double)report.Where(c => c.HourOfDay == 23).Sum(c => c.Number) / (double)report.Sum(c => c.Number)))%>
            </td>
        </tr>
    </table>
    <%} %>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
    <style>
        .reportheader
        {
            background-color: Yellow;
            font-weight: bold;
            text-align: center;
        }
        .reportdata
        {
            border-top: solid 1px Black;
            border-left: solid 1px Black;
        }
        .reportdatabottom
        {
            border-top: solid 1px Black;
            border-left: solid 1px Black;
            border-bottom: solid 1px Black;
        }
    </style>
</asp:Content>
