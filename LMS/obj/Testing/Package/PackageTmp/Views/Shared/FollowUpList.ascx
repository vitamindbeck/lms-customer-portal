<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<LMS.Data.FollowUpsResult>>" %>
<%@ Import Namespace="LMS.Helpers" %>
<div class="client-outer">
    <div class="section-title">
        [
        <% if (Model.Count() > 0)
           { %>
        <%= Model.Count().ToString()%>
        <%}
           else
           { %>
        No
        <%} %>
        Open Activities ]
    </div>
    <div class="client-inner">
        <table class="sortable" width="100%" cellpadding="0" cellspacing="0">
            <thead>    
                <tr>
                    <th>
                        Lead Name
                    </th>
                    <th>
                        Follow Up Date
                    </th>
                    <th>
                        Time
                    </th>
                    <th style="text-align: center;">
                        State
                    </th>
                    <th>
                        Source
                    </th>
                    <th>
                        Status
                    </th>
                    <th style="text-align: left;">
                        Priority Level
                    </th>
                    <th style="text-align: center;">
                        Campaign
                    </th>
                </tr>
            </thead>
            <tbody>
                <% foreach (LMS.Data.FollowUpsResult record in Model)
                   {
                       var trClass = string.Empty;

                       if (record.Record_Activity_Status_ID != null)
                       {
                           if (record.Record_Activity_Status_ID == 33 || record.Record_Activity_Status_ID == 34 || record.Record_Activity_Status_ID == 35)
                           {
                               trClass = "orange";
                           }
                       }
                       
                %>
                <tr class="<%=trClass%>">
                    <td nowrap="nowrap">
                        <%if (record.Key != null)
                          { %>
                        <span style="background-color: Yellow;">
                            <%} %>
                            <%= Html.ActionLink(StringHelper.UppercaseFirst(record.Last_Name) + ", " + StringHelper.UppercaseFirst(record.First_Name), "Complete", "Activity", new { id = record.Record_Activity_ID }, new { target = "_blank", style = "target-new: tab ! important" })%>
                            <%if (record.Key != null)
                              { %>
                        </span>
                        <%} %>
                    </td>
                    <td nowrap="nowrap">
                        <%= Html.Encode(string.Format("{0:d}", record.Due_Date))%>
                    </td>
                    <td>
                        <% if (record.Due_Date.Value.TimeOfDay.Hours > 0)
                           { %>
                        <%= Html.Encode(record.Due_Date.Value.ToShortTimeString())%>
                        <%}
                           else
                           { %>
                        <%} %>
                    </td>
                    <td style="text-align: center;">
                        <%= Html.Encode(record.State) %>
                    </td>
                    <td nowrap="nowrap">
                        <%= Html.Encode(record.SourceName) %>
                    </td>
                    <td>
                        <%= Html.Encode(record.PlanDesc) %>
                    </td>
                    <td style="text-align: left;">
                        <%= Html.Encode(record.Priority_Level) %>
                    </td>
                    <td style="text-align: left;">
                        <%= Html.Encode(record.campaignDescription)%>
                    </td>
                </tr>
                <%} %>
            </tbody>
        </table>
    </div>
</div>
