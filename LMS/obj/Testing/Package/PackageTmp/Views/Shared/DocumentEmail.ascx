<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<LMS.Models.EmailDocumentModel>" %>
<script type="text/javascript">
    $(document).ready(function() {
        $("#SubmitEmail").click(function(e) {
            e.preventDefault();
            $("#emailform").submit();
        });
    });
</script>
  <form name="emailform" id="emailform" action="/Document/Email" method="post">
<%= Html.Hidden("Plan_Transaction_ID",Model.Plan_Transaction_ID) %>
<table>
    <tr>
        <td>
            To:
        </td>
        <td>
            <%= Html.TextBox("ToAddress", Model.ToAddress, new { style = "width: 500px;" }) %>
        </td>
    </tr>
    <tr>
        <td>
            From:
        </td>
        <td>
            <%= Html.TextBox("FromAddress", Model.FromAddress, new { style = "width: 500px;" }) %>
        </td>
    </tr>
    <tr>
        <td>
            Subject:
        </td>
        <td>
            <%= Html.TextBox("Subject", Model.Subject, new { style = "width: 500px;" }) %>
        </td>
    </tr>
    <tr>
        <td>
            Attchments:
        </td>
        <td>
            <% foreach (LMS.Data.Plan_Document document in Model.Documents)
               {%>
                <input type="checkbox" checked="checked" name="Plan_Document_ID" value="<%= Html.Encode(document.Plan_Document_ID) %>" />
                <%= Html.Encode(document.Title) %>
            <br />
            <%} %>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <%=Html.TextArea("Body", new { rows = "10", cols = "79" }) %>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="text-align: center;">
            <input type="button" value="Send" name="Send" id="SubmitEmail" />
            <input type="button" value="Close" name="Close" id="CloseWindow" onclick="$('.window').hide();$('#dmask').hide();" />
        </td>
    </tr>
</table>
</form>
