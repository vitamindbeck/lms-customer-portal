<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import Namespace="Helpers" %>
<% LMS.Data.User user = LMS.Data.Repository.UserRepository.GetUser(Page.User.Identity.Name); %>
<div align="right">

 <%if (LMS.Data.Repository.UserRepository.HasSecurityLevel(user, 10))
        {%>
            <a href="/Agent/List" title="Go to Licensing" style="color: #FFFFFF">Go to Licensing</a>
        <%}
       else if(LMS.Data.Repository.UserRepository.HasSecurityLevel(user, 2))
        {%>
            <a href="/License/List" title="Go to Licensing" style="color: #FFFFFF">Go to Licensing</a>
        <%}
        else if(LMS.Data.Repository.UserRepository.HasSecurityLevel(user, 1))
        {%>
               <a href="/AgentScreen/Certification" title="Go to Licensing" style="color: #FFFFFF">Go to Licensing</a>
        <%}%>
                
    
 
</div>
<div id="menucontainer" style="text-shadow: 1px 1px 1px #000;">
    <ul id="menu">
        <% switch (user.Security_Level_ID)
           {
               case 1: %>
                    <% if (Html.IsCurrentAction("Sales", "Record"))
                       { %>
                    <li class="active" id="homeTab">
                        <%= Html.ActionLink("Home", "Sales", "Record")%></li>
                    <% }
                       else
                       { %>
                    <li  id="homeTab">
                        <%= Html.ActionLink("Home", "Sales", "Record")%></li>
                    <% }%>
                    <% if (Html.IsCurrentAction("Search", "Record"))
                       { %>
                    <li class="active">
                        <%= Html.ActionLink("Search", "Search", "Record")%></li>
                    <% }
                       else
                       { %>
                    <li>
                        <%= Html.ActionLink("Search", "Search", "Record")%></li>
                    <% }%>
                    <% if (Html.IsCurrentAction("Leads", "Record"))
                       { %>
                    <li class="active">
                        <%= Html.ActionLink("Leads", "Leads", "Record")%></li>
                    <% }
                       else
                       { %>
                    <li>
                        <%= Html.ActionLink("Leads", "Leads", "Record")%></li>
                    <% }%>
                    <% if (Html.IsCurrentAction("Prospect", "Record"))
                       { %>
                    <li class="active">
                        <%= Html.ActionLink("Prospects", "Prospect", "Record")%></li>
                    <% }
                       else
                       { %>
                    <li>
                        <%= Html.ActionLink("Prospects", "Prospect", "Record")%></li>
                    <% }%>
                    <% if (Html.IsCurrentAction("Create", "Record"))
                       { %>
                    <li class="active">
                        <%= Html.ActionLink("Create New", "CheckCreate", "Record")%></li>
                    <% }
                       else
                       { %>
                    <li>
                        <%= Html.ActionLink("Create New", "CheckCreate", "Record")%></li>
                    <% }%>
                    <% if (Html.IsCurrentAction("Index", "Admin"))
                       { %>
                    <li class="active">
                        <%= Html.ActionLink("Admin", "Index", "Admin")%></li>
                    <% }
                       else
                       { %>
                    <li>
                        <%= Html.ActionLink("Admin", "Index", "Admin")%></li>
                    <% }%>
                    <% if (Html.IsCurrentAction("List", "Request"))
                       { %>
                    <li class="active">
                        <%= Html.ActionLink("Requests", "List", "Request",new { id = user.User_ID},null)%></li>
                    <% }
                       else
                       { %>
                    <li>
                        <%= Html.ActionLink("Requests", "List", "Request", new { id = user.User_ID }, null)%></li>
                    <% }%>
        <% break;
               case 2: %>
                  <% if (Html.IsCurrentAction("Admin", "Record"))
                       { %>
                    <li class="active" id="homeTab">
                        <%= Html.ActionLink("Home", "Admin", "Record")%></li>
                    <% }
                       else
                       { %>
                    <li id="homeTab">
                        <%= Html.ActionLink("Home", "Admin", "Record")%></li>
                    <% }%>
                    
                    
                     <% if (Html.IsCurrentAction("NewLeads", "Record"))
                       { %>
                    <li class="active">
                        <%= Html.ActionLink("New Leads", "NewLeads", "Record")%></li>
                    <% }
                       else
                       { %>
                    <li>
                        <%= Html.ActionLink("New Leads", "NewLeads", "Record")%></li>
                    <% }%>
                                
                     <% if (Html.IsCurrentAction("Sales", "Record"))
                       { %>
                    <li class="active">
                        <%= Html.ActionLink("Sales", "Sales", "Record")%></li>
                    <% }
                       else
                       { %>
                    <li>
                        <%= Html.ActionLink("Sales", "Sales", "Record")%></li>
                    <% }%>
                    <% //----------- PARTNER PAGE ------------------- %>
                    <% if (Html.IsCurrentAction("Partner", "Record"))
                       { %>
                    <li class="active">
                        <%= Html.ActionLink("Link Partners", "Partner", "Record")%></li>
                    <% }
                       else
                       { %>
                    <li>
                        <%= Html.ActionLink("Link Partners", "Partner", "Record")%></li>
                    <% }%>
                    <% if (Html.IsCurrentAction("Support", "Record"))
                       { %>
                    <li class="active">
                        <%= Html.ActionLink("Support", "Support", "Record")%></li>
                    <% }
                       else
                       { %>
                    <li>
                        <%= Html.ActionLink("Support", "Support", "Record")%></li>
                    <% }%>
                    <% if (Html.IsCurrentAction("Search", "Record"))
                       { %>
                    <li class="active">
                        <%= Html.ActionLink("Search", "Search", "Record")%></li>
                    <% }
                       else
                       { %>
                    <li>
                        <%= Html.ActionLink("Search", "Search", "Record")%></li>
                    <% }%>
                      <% if (Html.IsCurrentAction("Leads", "Record"))
                       { %>
                    <li class="active">
                        <%= Html.ActionLink("Leads", "Leads", "Record")%></li>
                    <% }
                       else
                       { %>
                    <li>
                        <%= Html.ActionLink("Leads", "Leads", "Record")%></li>
                    <% }%>
                    <% //----------- PROSPECTS ------------------- %>
                    <% if (Html.IsCurrentAction("Prospect", "Record"))
                       { %>
                    <li class="active">
                        <%= Html.ActionLink("Prospects", "Prospect", "Record")%></li>
                    <% }
                       else
                       { %>
                    <li>
                        <%= Html.ActionLink("Prospects", "Prospect", "Record")%></li>
                    <% }%>
                    <% if (Html.IsCurrentAction("Create", "Record"))
                       { %>
                    <li class="active">
                        <%= Html.ActionLink("Create New", "CheckCreate", "Record")%></li>
                    <% }
                       else
                       { %>
                    <li>
                        <%= Html.ActionLink("Create New", "CheckCreate", "Record")%></li>
                    <% }%>
                    <% if (Html.IsCurrentAction("Retrieve", "OperationsAdmin"))
                       { %>
                    <li class="active">
                        <%= Html.ActionLink("Op Admin", "Retrieve", "OperationsAdmin")%></li>
                    <% }
                       else
                       { %>
                    <li>
                        <%= Html.ActionLink("Op Admin", "Retrieve", "OperationsAdmin")%></li>
                    <% }%>
                    <% if (Html.IsCurrentAction("LeadDecay", "Admin"))
                       { %>
                    <li class="active">
                        <%= Html.ActionLink("Lead Decay", "LeadDecay", "Admin")%></li>
                    <% }
                       else
                       { %>
                    <li>
                        <%= Html.ActionLink("Lead Decay", "LeadDecay", "Admin")%></li>
                    <% }%>
                    <% if (Html.IsCurrentAction("List", "Carrier"))
                       { %>
                    <li class="active">
                        <%= Html.ActionLink("Carrier", "List", "Carrier")%></li>
                    <% }
                       else
                       { %>
                    <li>
                        <%= Html.ActionLink("Carrier", "List", "Carrier")%></li>
                    <% }%>
                    <% if (Html.IsCurrentAction("List", "Request"))
                       { %>
                    <li class="active">
                        <%= Html.ActionLink("Requests", "List", "Request",new { id = user.User_ID},null)%></li>
                    <% }
                       else
                       { %>
                    <li>
                        <%= Html.ActionLink("Requests", "List", "Request", new { id = user.User_ID }, null)%></li>
                    <% }%>
              <% break; 
               case 3:
               case 4:%>
                    <% if (Html.IsCurrentAction("Admin", "Record"))
                       { %>
                    <li class="active" id="homeTab">
                        <%= Html.ActionLink("Home", "Admin", "Record")%></li>
                    <% }
                       else
                       { %>
                    <li id="homeTab">
                        <%= Html.ActionLink("Home", "Admin", "Record")%></li>
                    <% }%>                             
                     <% if (Html.IsCurrentAction("Sales", "Record"))
                       { %>
                    <li class="active">
                        <%= Html.ActionLink("Sales", "Sales", "Record")%></li>
                    <% }
                       else
                       { %>
                    <li>
                        <%= Html.ActionLink("Sales", "Sales", "Record")%></li>
                    <% }%>
                    <% //----------- PARTNER PAGE ------------------- %>
                    <% if (Html.IsCurrentAction("Partner", "Record"))
                       { %>
                    <li class="active">
                        <%= Html.ActionLink("Link Partners", "Partner", "Record")%></li>
                    <% }
                       else
                       { %>
                    <li>
                        <%= Html.ActionLink("Link Partners", "Partner", "Record")%></li>
                    <% }%>
                    <% if (Html.IsCurrentAction("Support", "Record"))
                       { %>
                    <li class="active">
                        <%= Html.ActionLink("Support", "Support", "Record")%></li>
                    <% }
                       else
                       { %>
                    <li>
                        <%= Html.ActionLink("Support", "Support", "Record")%></li>
                    <% }%>
                    <% if (Html.IsCurrentAction("Search", "Record"))
                       { %>
                    <li class="active">
                        <%= Html.ActionLink("Search", "Search", "Record")%></li>
                    <% }
                       else
                       { %>
                    <li>
                        <%= Html.ActionLink("Search", "Search", "Record")%></li>
                    <% }%>
                      <% if (Html.IsCurrentAction("Leads", "Record"))
                       { %>
                    <li class="active">
                        <%= Html.ActionLink("Leads", "Leads", "Record")%></li>
                    <% }
                       else
                       { %>
                    <li>
                        <%= Html.ActionLink("Leads", "Leads", "Record")%></li>
                    <% }%>
                    <% //----------- PROSPECTS ------------------- %>
                    <% if (Html.IsCurrentAction("Prospect", "Record"))
                       { %>
                    <li class="active">
                        <%= Html.ActionLink("Prospects", "Prospect", "Record")%></li>
                    <% }
                       else
                       { %>
                    <li>
                        <%= Html.ActionLink("Prospects", "Prospect", "Record")%></li>
                    <% }%>
                    <% if (Html.IsCurrentAction("Create", "Record"))
                       { %>
                    <li class="active">
                        <%= Html.ActionLink("Create New", "CheckCreate", "Record")%></li>
                    <% }
                       else
                       { %>
                    <li>
                        <%= Html.ActionLink("Create New", "CheckCreate", "Record")%></li>
                    <% }%>
                    <% if (Html.IsCurrentAction("Index", "Admin"))
                       { %>
                    <li class="active">
                        <%= Html.ActionLink("Admin", "Index", "Admin")%></li>
                    <% }
                       else
                       { %>
                    <li>
                        <%= Html.ActionLink("Admin", "Index", "Admin")%></li>
                    <% }%>
                    <% if (Html.IsCurrentAction("LeadDecay", "Admin"))
                       { %>
                    <li class="active">
                        <%= Html.ActionLink("Lead Decay", "LeadDecay", "Admin")%></li>
                    <% }
                       else
                       { %>
                    <li>
                        <%= Html.ActionLink("Lead Decay", "LeadDecay", "Admin")%></li>
                    <% }%>
                   
                    <% if (Html.IsCurrentAction("List", "Request"))
                       { %>
                    <li class="active">
                        <%= Html.ActionLink("Requests", "List", "Request",new { id = user.User_ID},null)%></li>
                    <% }
                       else
                       { %>
                    <li>
                        <%= Html.ActionLink("Requests", "List", "Request", new { id = user.User_ID }, null)%></li>
                    <% }%>
        <% break;
               case 5:
                   if (Html.IsCurrentAction("Support", "Record"))
                       { %>
                    <li class="active" id="homeTab">
                        <%= Html.ActionLink("Home", "Support", "Record")%></li>
                    <% }
                       else
                       { %>
                    <li id="homeTab">
                        <%= Html.ActionLink("Home", "Support", "Record")%></li>
                    <% }%>
                    
                    <% if (Html.IsCurrentAction("Search", "Record"))
                       { %>
                    <li class="active">
                        <%= Html.ActionLink("Search", "Search", "Record")%></li>
                    <% }
                       else
                       { %>
                    <li>
                        <%= Html.ActionLink("Search", "Search", "Record")%></li>
                    <% }%>
                     
                     <% if (Html.IsCurrentAction("Create", "Record"))
                       { %>
                    <li class="active">
                        <%= Html.ActionLink("Create New", "CheckCreate", "Record")%></li>
                    <% }
                       else
                       { %>
                    <li>
                        <%= Html.ActionLink("Create New", "CheckCreate", "Record")%></li>
                    <% }%>
                    <% if (Html.IsCurrentAction("Index", "Admin"))
                       { %>
                    <li class="active">
                        <%= Html.ActionLink("Admin", "Index", "Admin")%></li>
                    <% }
                       else
                       { %>
                    <li>
                        <%= Html.ActionLink("Admin", "Index", "Admin")%></li>
                    <% }%>
                    <% if (Html.IsCurrentAction("List", "Carrier"))
                       { %>
                    <li class="active">
                        <%= Html.ActionLink("Carrier", "List", "Carrier")%></li>
                    <% }
                       else
                       { %>
                    <li>
                        <%= Html.ActionLink("Carrier", "List", "Carrier")%></li>
                    <% }%>
                    <% break;
               case 8:%>
                    <% if (Html.IsCurrentAction("AppointmentSetter", "Record"))
                       { %>
                    <li class="active" id="homeTab">
                        <%= Html.ActionLink("Home", "AppointmentSetter", "Record")%></li>
                    <% }
                       else
                       { %>
                    <li id="homeTab">
                        <%= Html.ActionLink("Home", "AppointmentSetter", "Record")%></li>
                    <% }%>
                    
                    <% if (Html.IsCurrentAction("Support", "Record"))
                       { %>
                    <li class="active">
                        <%= Html.ActionLink("Assign Leads", "Admin", "Record")%></li>
                    <% }
                       else
                       { %>
                    <li>
                        <%= Html.ActionLink("Assign Leads", "Admin", "Record")%></li>
                    <% }%>
                    
                    
                    <% if (Html.IsCurrentAction("Search", "Record"))
                       { %>
                    <li class="active">
                        <%= Html.ActionLink("Search", "Search", "Record")%></li>
                    <% }
                       else
                       { %>
                    <li>
                        <%= Html.ActionLink("Search", "Search", "Record")%></li>
                    <% }%>
                     
                     <% if (Html.IsCurrentAction("Create", "Record"))
                       { %>
                    <li class="active">
                        <%= Html.ActionLink("Create New", "CheckCreate", "Record")%></li>
                    <% }
                       else
                       { %>
                    <li>
                        <%= Html.ActionLink("Create New", "CheckCreate", "Record")%></li>
                    <% }%>
                    <% if (Html.IsCurrentAction("Index", "Admin"))
                       { %>
                    <li class="active">
                        <%= Html.ActionLink("Admin", "Index", "Admin")%></li>
                    <% }
                       else
                       { %>
                    <li>
                        <%= Html.ActionLink("Admin", "Index", "Admin")%></li>
                    <% }%>
                    <% if (Html.IsCurrentAction("List", "Carrier"))
                       { %>
                    <li class="active">
                        <%= Html.ActionLink("Carrier", "List", "Carrier")%></li>
                    <% }
                       else
                       { %>
                    <li>
                        <%= Html.ActionLink("Carrier", "List", "Carrier")%></li>
                    <% }%>
        <% break;
               case 7: %>
               <% if (Html.IsCurrentAction("Sales", "Record"))
                       { %>
                    <li class="active" id="homeTab">
                        <%= Html.ActionLink("Home", "Sales", "Record")%></li>
                    <% }
                       else
                       { %>
                    <li id="homeTab">
                        <%= Html.ActionLink("Home", "Sales", "Record")%></li>
                    <% }%>

                    <% if (Html.IsCurrentAction("Search", "Record"))
                       { %>
                    <li class="active">
                        <%= Html.ActionLink("Search", "Search", "Record")%></li>
                    <% }
                       else
                       { %>
                    <li>
                        <%= Html.ActionLink("Search", "Search", "Record")%></li>
                    <% }%>
                    <% if (Html.IsCurrentAction("Leads", "Record"))
                       { %>
                    <li class="active">
                        <%= Html.ActionLink("Leads", "Leads", "Record")%></li>
                    <% }
                       else
                       { %>
                    <li>
                        <%= Html.ActionLink("Leads", "Leads", "Record")%></li>
                    <% }%>
                    <% if (Html.IsCurrentAction("Prospect", "Record"))
                       { %>
                    <li class="active">
                        <%= Html.ActionLink("Prospects", "Prospect", "Record")%></li>
                    <% }
                       else
                       { %>
                    <li>
                        <%= Html.ActionLink("Prospects", "Prospect", "Record")%></li>
                    <% }%>
                    <% if (Html.IsCurrentAction("Create", "Record"))
                       { %>
                    <li class="active">
                        <%= Html.ActionLink("Create New", "CheckCreate", "Record")%></li>
                    <% }
                       else
                       { %>
                    <li>
                        <%= Html.ActionLink("Create New", "CheckCreate", "Record")%></li>
                    <% }%>
                    <% if (Html.IsCurrentAction("Index", "Admin"))
                       { %>
                    <li class="active">
                        <%= Html.ActionLink("Admin", "Index", "Admin")%></li>
                    <% }
                       else
                       { %>
                    <li>
                        <%= Html.ActionLink("Admin", "Index", "Admin")%></li>
                    <% }%>
                    <% if (Html.IsCurrentAction("NewLeads", "Record"))
                       { %>
                    <li class="active">
                        <%= Html.ActionLink("Assign", "NewLeads", "Record")%></li>
                    <% }
                       else
                       { %>
                    <li>
                        <%= Html.ActionLink("Assign", "NewLeads", "Record")%></li>
                    <% }
                 break;
                       case 12:%>
                    <% if (Html.IsCurrentAction("Admin", "Record"))
                       { %>
                    <li class="active" id="homeTab">
                        <%= Html.ActionLink("Home", "Admin", "Record")%></li>
                    <% }
                       else
                       { %>
                    <li id="homeTab">
                        <%= Html.ActionLink("Home", "Admin", "Record")%></li>
                    <% }%>                             
                     <% if (Html.IsCurrentAction("Sales", "Record"))
                       { %>
                    <li class="active">
                        <%= Html.ActionLink("Sales", "Sales", "Record")%></li>
                    <% }
                       else
                       { %>
                    <li>
                        <%= Html.ActionLink("Sales", "Sales", "Record")%></li>
                    <% }%>
                    <% if (Html.IsCurrentAction("FollowUp", "Record"))
                       { %>
                    <li class="active">
                        <%= Html.ActionLink("Follow Ups", "FollowUp", "Record")%></li>
                    <% }
                       else
                       { %>
                    <li>
                        <%= Html.ActionLink("Follow Ups", "FollowUp", "Record")%></li>
                    <% }%>
                    <% if (Html.IsCurrentAction("Support", "Record"))
                       { %>
                    <li class="active">
                        <%= Html.ActionLink("Support", "Support", "Record")%></li>
                    <% }
                       else
                       { %>
                    <li>
                        <%= Html.ActionLink("Support", "Support", "Record")%></li>
                    <% }%>
                    <% if (Html.IsCurrentAction("Search", "Record"))
                       { %>
                    <li class="active">
                        <%= Html.ActionLink("Search", "Search", "Record")%></li>
                    <% }
                       else
                       { %>
                    <li>
                        <%= Html.ActionLink("Search", "Search", "Record")%></li>
                    <% }%>
                     
                    <% if (Html.IsCurrentAction("Create", "Record"))
                       { %>
                    <li class="active">
                        <%= Html.ActionLink("Create New", "CheckCreate", "Record")%></li>
                    <% }
                       else
                       { %>
                    <li>
                        <%= Html.ActionLink("Create New", "CheckCreate", "Record")%></li>
                    <% }%>
                    <% if (Html.IsCurrentAction("Index", "Admin"))
                       { %>
                    <li class="active">
                        <%= Html.ActionLink("Admin", "Index", "Admin")%></li>
                    <% }
                       else
                       { %>
                    <li>
                        <%= Html.ActionLink("Admin", "Index", "Admin")%></li>
                    <% }%>
                                       
                    <% if (Html.IsCurrentAction("List", "Request"))
                       { %>
                    <li class="active">
                        <%= Html.ActionLink("Requests", "List", "Request",new { id = user.User_ID},null)%></li>
                    <% }
                       else
                       { %>
                    <li>
                        <%= Html.ActionLink("Requests", "List", "Request", new { id = user.User_ID }, null)%></li>
                    <% }%>
        <% break;
       default: break;
           } %>
           <%if (LMS.Data.Repository.UserRepository.HasSecurityLevel(user, 9))
             {%>
             <% if (Html.IsCurrentAction("Index", "SuperAdmin"))
               { %>
             <li class="sactive">
                <%= Html.ActionLink("Super Admin", "Index", "SuperAdmin")%></li>
            <% }
               else
               { %>
            <li class="sinactive">
                <%= Html.ActionLink("Super Admin", "Index", "SuperAdmin")%></li>
            <% }%>
           <%} %>
    </ul>
</div>
