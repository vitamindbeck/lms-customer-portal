<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<LMS.Data.Record>>" %>
<%@ Import Namespace="LMS.Helpers" %>
   <div class="client-outer">
    <div class="section-title">
        [
        
        Leads ]
    </div>
    <div class="client-inner">
        <table class="sortable" width="100%" cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th>
                        Lead Name
                    </th>
                    <th>
                        Follow Up Date
                    </th>
                    <th style="text-align: center;">
                        State
                    </th>
                    <th>
                        Phone Number
                    </th>
                    <th>
                    
                        Email Address
                    </th>
                    <th style="text-align: center;">
                        Priority
                    </th>
                </tr>
            </thead>
            <tbody>
                <% foreach (LMS.Data.Record record in Model)
                   { %>
                <tr>
                    <td width="30%">
                        <%= Html.ActionLink(StringHelper.UppercaseFirst(record.Last_Name) + ", " + StringHelper.UppercaseFirst(record.First_Name), "Details", new { id = record.Record_ID })%>
                    </td>
                    <td width="12%">
                        <%= Html.Encode(string.Format("{0:d}",(record.FollowUpDate()==new DateTime() ? "" : record.FollowUpDate().ToShortDateString()))) %>
                    </td>
                    <td width="5%" style="text-align: center;">
                        <%= Html.Encode(record.State.Abbreviation) %>
                    </td>
                    <td width="15%">
                        <%= Html.Encode(record.Home_Phone) %>
                    </td>
                    <td width="20%">
                        <%= Html.Encode(record.Email_Address) %>
                    </td>
                    <td width="8%" style="text-align: center;">
                        <%= Html.Encode(record.Priority_Level) %>
                    </td>
                </tr>
                <%} %>
            </tbody>
        </table>
    </div>
</div>


