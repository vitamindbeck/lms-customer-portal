<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<IEnumerable<LMS.Data.sp_Get_Active_AgentsResult>>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Agent SSN List</title>
    <link href="../../Content/Site.css" rel="stylesheet" type="text/css" />
</head>
<body>
   <div class="client-outer" style="width:400px">
    <div class="client-title">
         [ Agent SSN List ]
    </div>
    <div class="client-inner">
    <table class="sortable" width="100%" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
            <th>Agent</th>
            <th>SSN</th>
            </tr>
        </thead>
        <tbody style="color:Black;">
        <% foreach (LMS.Data.sp_Get_Active_AgentsResult rs in Model)
           {
               if (!String.IsNullOrEmpty(rs.First_Name))
               {%>
        <tr>
            <td>
                <%= Html.Encode(rs.Full_Name)%>
            </td>
            <td>
                <%= Html.Encode(rs.SSN)%>
            </td>
        </tr>
            <%}
          } %>
        </tbody>
    </table>
    </div>
</div>
</body>
</html>
