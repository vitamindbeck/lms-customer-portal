<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<LMS.Data.sp_Appointment_List_By_State_CarrierResult>>" %>
<%@ Import Namespace="LMS.Helpers" %>
   <div class="client-outer">
    <div class="client-title">
         [ <%= Html.Encode(Model.Count()) %> Appointments ]
    </div>
    <div class="client-inner">
    <table class="tablesorter" width="100%" cellpadding="0" cellspacing="0" id="sortable2">
        <thead>
            <tr>
            <th>Action</th>
            <th>Name</th>
            <th>Carrier</th>
            <th>Agent/Agency #</th>
            <th>States</th>  
            <th>Appointment Date</th>          
            <th>Product</th>
            </tr>
        </thead>
        <tbody>
        <% foreach (LMS.Data.sp_Appointment_List_By_State_CarrierResult rs in Model)
           { %>
        <tr>
            <td>
            <% LMS.Data.User user = LMS.Data.Repository.UserRepository.GetUser(Page.User.Identity.Name); %>
            <%if (LMS.Data.Repository.UserRepository.HasSecurityLevel(user, 10))
              {%> 
                <%= Html.ActionLink("Edit", "Edit", new { id = rs.Appointment_ID }, new { target = "_blank", style = "target-new: tab ! important" })%>&nbsp;
            <%} %>
                <%= Html.ActionLink("View", "Details", new { id = rs.Appointment_ID }, new { target = "_blank", style = "target-new: tab ! important"  })%>
            </td>
            <td>
                <%= Html.Encode(rs.Name) %>
            </td>
            <td>
                <%= Html.Encode(rs.Carrier) %>
            </td>
             <td>
                <% if (rs.Agent_Number != null)
                   { %>
                         <%= Html.Encode(rs.Agent_Number) %>
                    <% } %>
                    
            </td>
            <td>
                <%= Html.Encode(rs.State_List) %>
            </td>  
            <td>
                <%= Html.Encode(rs.Appointment_Date.HasValue ? rs.Appointment_Date.Value.ToShortDateString() : "") %>
            </td>        
            <td>
                <%= Html.Encode(rs.Product) %>
            </td>
        </tr>
        <%} %>
        </tbody>
    </table>
    </div>
</div>

