<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>
<%@ Import Namespace="Helpers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	index
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="ClientFactsDiv" class="client-outer" style="background-color: #e0e4ee;">
        <div class="section-title">
            Super Administration
        </div>
        <div class="section-inner">
 
  <table class="section">    
    <tr>
        <td class="label" width="100px">
            Users:
        </td>
        <td width="200px">
            <%= Html.ActionLink("Create New User", "CreateUser", "SuperAdmin")%>
        </td>
         <td>
            <%= Html.ActionLink("Manage Existing User", "ManageUser", "SuperAdmin")%>
        </td>
    </tr>
       <tr>
        <td class="label">
            Source Numbers:
        </td>
        <td>
            <%= Html.ActionLink("Create New Source Code", "SourceCodeCreate", "SuperAdmin")%>
        </td>
        <td>
           <%= Html.ActionLink("Manage Source Code", "SourceCode", "SuperAdmin")%>
        </td>
    </tr>
</table>
<div id="pager" class="scroll" style="text-align:center;"></div>

    </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
