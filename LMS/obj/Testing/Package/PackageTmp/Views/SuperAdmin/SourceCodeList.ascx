<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<LMS.Data.sp_Source_Code_ListResult>>" %>

   
   <div style="text-align:right"><a href="/SuperAdmin/SourceCodeCreate">Add a new Source Number</a></div>
   <div class="client-outer">
    <div class="client-title">
         [ Source Code List ]
    </div>
    <div class="client-inner">
    <table class="sortable" width="100%" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
            <th></th>
            <th>Source Number</th>
            <th>Source Name</th>
            <th>Group</th>
            <th>Affinity Partner</th>
            <th>Launch Date</th>
            <th>LTC</th>
            <th>Active</th>
            <th>Web Post</th>
            </tr>
        </thead>
        <tbody>
        <% foreach (LMS.Data.sp_Source_Code_ListResult rs in Model)
           { %>
        <tr>
            <td>
                <%= Html.ActionLink("Edit", "SourceCodeEdit", new { id = rs.Source_Code_ID }, new { target = "_blank", style = "target-new: tab ! important"  })%>
            </td>
            <td>
                <%=Html.Encode(rs.Source_Number) %>
            </td>
            <td>
                <%=Html.Encode(rs.Description) %>
            </td>
            <td>
                <%=Html.Encode(rs.Group) %>
            </td>
            <td>
                <%=Html.Encode(rs.Affinity) %>
            </td>
            <td>
            <%if (!String.IsNullOrEmpty(rs.Launch_Date.ToString())) %>
                <%=Html.Encode(String.Format("{0:d}",DateTime.Parse(rs.Launch_Date.ToString()))) %>
            </td>
            <td>
                <%= Html.CheckBox("LTC", Boolean.Parse(rs.LTC.ToString()), new { disabled = "disabled" })%>
            </td>
            <td>
                <%= Html.CheckBox("Active", Boolean.Parse(rs.Active.ToString()), new { disabled = "disabled" })%>
            </td>
            <td>
                <%= Html.Encode(rs.WEB.ToString() == "1" ? "Yes" : "No")%>
            </td>
        </tr>
        <%} %>
        </tbody>
    </table>
    </div>
</div>