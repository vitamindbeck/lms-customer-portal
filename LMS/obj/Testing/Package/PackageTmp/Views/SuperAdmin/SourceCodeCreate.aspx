<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<LMS.Data.Source_Code>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Create Source Code 
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<script type="text/javascript" src="../../Scripts/jquery.Guid.js"></script>
<script type="text/javascript">
    $(function() {
        $("#Launch_Date").datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: '1900:2020'
        });
        $("#WEB").click(function() {
            if ($("#WEB").is(":checked")) {
                $("#WebDiv").show();
            }
            else {
                $("#WebDiv").hide();

            }
        });
        if ($("#WEB").is(":checked")) {
            $("#WebDiv").show();
        }
        else {
            $("#WebDiv").hide();
        };
//        $("#Source_Number").keydown(function(event) {
//            // Allow only backspace and delete
//            if (event.keyCode == 46 || event.keyCode == 8) {
//                // let it happen, don't do anything
//            }
//            else {
//                // Ensure that it is a number and stop the keypress
//                if (event.keyCode < 48 || event.keyCode > 57) {
//                    event.preventDefault();
//                }
//            }
//        });
        
        $("#Source_Number").val("<%= Model.Source_Number %>");
        $("#Description").val("<%= Model.Description %>");
        $("#Group_ID").val("<%=Model.Group_ID %>");
        $("#Affinity_Partner_ID").val("<%=Model.Affinity_Partner_ID %>");
        $("#Launch_Date").val("<%=Model.Launch_Date %>");
        $("#tabs").tabs();
        $("#saveBtn").click(function() {
            if ($("#WEB").is(":checked")) {
                if ($("#WebName").val() == "" || $("#WebName").val() == null) {
                    alert("Please enter an unique web name.");
                    return false;
                }
                else if ($("#Code").val() == "" || $("#Code").val() == null) {
                    alert("Please enter a key.");
                    return false;
                }
                else {
                    return true;
                }
            }
            else {
                return true;
            }

        });
        $("#btnGUID").click(function() {
            var guid = jQuery.Guid.Value();
            $("#Code").val(guid);
        });


    });
</script>
    <% 
        using (Html.BeginForm())
       {%>
    <%if (ViewData["EditSuccess"] == "true")
      { %>
        <div id="highlighted" style="background-color:Yellow; width:250px;">Edit Successful</div>
    <%}
      else if (ViewData["EditSuccess"] == "false")
      { %>
        <div style="background-color:Yellow; width:350px;">Edit failed. Please try again.</div>
    <%} %>
<div id="tabs" style="font-size: 1em; font-family: Calibri;">
        <ul class="ui-tabs-nav">
            <li><a href="#tabs-1">Source Code Details</a>
                </li>
            <li><a href="#tabs-2">Web Post</a></li>
        </ul>
        <div id="tabs-1" style="background-color: #e0e4ee;">
            <table class="section" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="label">
                        Source Number:
                    </td>
                    <td>
                        <%= Html.TextBox("Source_Number",Model.Source_Number, new { style="width: 100px;", tabindex = "1" })%>
                        <%= Html.ValidationMessage("Source_Number", "*")%>
                    </td>
                    <td class="label">
                        Source Name:
                    </td>
                    <td>
                        <%= Html.TextBox("Description",Model.Description, new { style="width: 200px;", tabindex = "2" })%>
                        <%= Html.ValidationMessage("Description", "*")%>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Group:
                    </td>
                    <td>
                        <%= Html.DropDownList("Group_ID", (SelectList)ViewData["Group_ID"], new { tabindex = "3" })%>
                        <%= Html.ValidationMessage("Group_ID", "*")%>
                    </td>
                    <td class="label">
                        Affinity Partner:
                    </td>
                    <td>
                        <%= Html.DropDownList("Affinity_Partner_ID", (SelectList)ViewData["Affinity_Partner_ID"], new { tabindex = "4" })%>
                        <%= Html.ValidationMessage("Affinity_Partner_ID", "*")%>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Launch Date:
                    </td>
                    <td>
                        <%= Html.TextBox("Launch_Date", Model.Launch_Date.HasValue == true ? Model.Launch_Date.Value.ToShortDateString() : "", new { style = "width: 100px", tabindex = "5" })%>
                    </td>
                    <td class="label">
                        
                    </td>
                    <td>
                        
                    </td>
                </tr>
                <tr>
                    <td class="label">LTC:</td>
                    <td>
                        <%= Html.CheckBox("LTC", new { tabindex = "7" })%>
                    </td>
                    <td class="label"> Active:</td>
                    <td><%= Html.CheckBox("Active", new { tabindex = "8" })%></td>
                </tr>

            </table>
        </div>
        <div id="tabs-2" style="background-color: #e0e4ee;">
            <table class="section" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="label" width="200px">
                        This source code has a web post link:
                    </td>
                    <td>
                    <%if (Boolean.Parse(ViewData["Web"].ToString()) == true)
                      { %>
                        <% Response.Write(Html.CheckBox("WEB", true, new { tabindex = "9" }));
                      }%>
                    <%else
                      { %>
                        <% Response.Write(Html.CheckBox("WEB", new { tabindex = "9" }));
                      }%>

                    </td>
                </tr>
            </table>
            <div id="WebDiv">
            <table class="section" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="label">
                        Web Name (must be unique):
                    </td>
                    <td>
                    
                        https://leads.longevityalliance.com/API/Submit/
                        <%= Html.TextBox("WebName", ViewData["WebName"], new { style = "width: 200px;", tabindex = "10" })%>

                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Key: 
                    </td>
                    <td>
                        <%= Html.TextBox("Code", ViewData["Code"], new { style = "width: 400px;", tabindex = "11" })%>
                        <input type="button" title="Generate a key" id="btnGUID" value="Generate a key"  />
                    </td>
                </tr>
                
             </table>
            </div>
        </div>
    </div>
    <br />
    <div style="text-align: center;">
        
        
            <input type="image" src="../../Content/icons/check.png" title="Save" alt="Save" id="saveBtn"/>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="/SuperAdmin/Index" alt="Cancel">
        <img src="../../content/icons/close.png" alt="Cancel" title="Cancel" style="border: 0px; vertical-align: top;" />
        </a>
        
    </div>
    <%} %>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptContent" runat="server">
</asp:Content>

