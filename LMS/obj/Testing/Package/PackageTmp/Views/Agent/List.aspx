<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Licensing.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Agent/Agency List
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
 <form id="frmAgentList" method="post" action="/Agent/New">
   <% Html.RenderPartial("LicenseAgentList", ViewData["LicenseAgentList"]); %>
</form>
</asp:Content>


