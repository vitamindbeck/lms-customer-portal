﻿function bindDropDownList(e, targetDropDownList) {
    var key = this.value;
    var allOptions = targetDropDownList.allOptions;
    var option;
    var newOption;
    targetDropDownList.options.length = 0;

    for (var i = 0; i < allOptions.length; i++) {
        option = allOptions[i];
        if (option.key == key) {
            newOption = new Option(option.text, option.value);
            if (option.selected == "True") {
                newOption.selected = true;
            }
            targetDropDownList.options.add(newOption);
        }
    }
}

function SetInitialDropDownList(parentDDL, childDDL) {
    var key = parentDDL.value;
    var allOptions = childDDL.allOptions;

    var option;
    var newOption;
    childDDL.options.length = 0;

    for (var i = 0; i < allOptions.length; i++) {
        option = allOptions[i];
        if (option.key == key) {
            newOption = new Option(option.text, option.value);
            if (option.selected == "True") {
                newOption.selected = true;
            }
            childDDL.options.add(newOption);
        }
    }
}