<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title><%= Html.Encode(((LMS.Data.Plan) ViewData["Plan"]).Name) %> Documents</title>

    <script src="../../../../Scripts/jquery-1.3.2.min-vsdoc.js" type="text/javascript"></script>

    <script src="../../../../Scripts/jquery-1.3.2.js" type="text/javascript"></script>

    <style type="text/css">
        body
        {
            font-family: Trebuchet MS;
            font-size: 10pt;
            margin: 10px;
            min-width: 400px;
        }
        #documentlist
        {
            background-color: White;
        }
        th
        {
            text-align: left;
        }
        .odd
        {
            background-color: Yellow;
        }
        select
        {
            font-size: 10pt;
        }
        #DocTable
        {
            border: solid 1px #5171c4;
        }
        #DocTable thead
        {
            background-color: #5374c9;
            color: White;
        }
        #MailDiv
        {
            margin: 20px;
        }
    </style>

    <script type="text/javascript">
        $().ready(function() {
            stripeTable('DocTable');
            $("#DocTable tr:odd").css("background-color", "#e6edff");
        });

        function stripeTable(table) {
            $("tr:even", table).removeClass("odd");
            $("tr:odd", table).addClass("odd");
        }
        function ToggleColorOn(obj) {
            $(obj).css("color", "red");
        }
        function ToggleColorOff(obj) {
            $(obj).css("color", "black");
        }
        
    </script>

</head>
<body>
    <div id="documentlist">
        <table id="DocTable" cellspacing="0" cellpadding="0" width="100%">
            <thead>
                <tr>
                    <th>
                    </th>
                    <th>
                        Name
                    </th>
                    <th>
                        Descripiton
                    </th>
                    <th>
                    </th>
                </tr>
            </thead>
            <tbody>
                <% foreach (LMS.Data.Plan_Document pd in ((LMS.Data.Plan)ViewData["Plan"]).Plan_Documents)
                   { %>
                <tr onmouseover="ToggleColorOn(this);" onmouseout="ToggleColorOff(this);">
                    <td>
                        <%= Html.CheckBox("Document|"+pd.Plan_Document_ID.ToString(),false) %>
                    </td>
                    <td style="padding-right: 20px; overflow: hidden;">
                        <%= Html.Encode(pd.Title) %>
                    </td>
                    <td style="padding-right: 20px; overflow: hidden;">
                        <%= Html.Encode(pd.Location) %>
                    </td>
                    <td>
                        <a href="<%= Html.Encode(pd.Location) %>" style="text-decoration: none;" target="_blank">
                            <img src="../../../../Content/icons/pdf.png" title="Print" alt="Print" style="border: 0px;" />
                        </a>
                    </td>
                </tr>
                <%} %>
            </tbody>
        </table>
        <div id="MailDiv">
            <label for="EmailList">
                Send E-Mail:</label>
            <select id="EmailList">
                <option>Application Request</option>
                <option>Information Request</option>
            </select>
            <input type="image" src="../../../../Content/icons/mail.png" style="vertical-align: middle" />
        </div>
    </div>
</body>
</html>
