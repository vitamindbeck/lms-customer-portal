<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<LMS.Data.Plan_Document>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Create
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Create</h2>

    <%= Html.ValidationSummary("Create was unsuccessful. Please correct the errors and try again.") %>

    <% using (Html.BeginForm()) {%>

        <fieldset>
            <legend>Fields</legend>
            <p>
                <label for="Plan_Document_ID">Plan_Document_ID:</label>
                <%= Html.TextBox("Plan_Document_ID") %>
                <%= Html.ValidationMessage("Plan_Document_ID", "*") %>
            </p>
            <p>
                <label for="Plan_ID">Plan_ID:</label>
                <%= Html.TextBox("Plan_ID") %>
                <%= Html.ValidationMessage("Plan_ID", "*") %>
            </p>
            <p>
                <label for="State_ID">State_ID:</label>
                <%= Html.TextBox("State_ID") %>
                <%= Html.ValidationMessage("State_ID", "*") %>
            </p>
            <p>
                <label for="Title">Title:</label>
                <%= Html.TextBox("Title") %>
                <%= Html.ValidationMessage("Title", "*") %>
            </p>
            <p>
                <label for="Location">Location:</label>
                <%= Html.TextBox("Location") %>
                <%= Html.ValidationMessage("Location", "*") %>
            </p>
            <p>
                <label for="Active">Active:</label>
                <%= Html.TextBox("Active") %>
                <%= Html.ValidationMessage("Active", "*") %>
            </p>
            <p>
                <label for="Display_Order">Display_Order:</label>
                <%= Html.TextBox("Display_Order") %>
                <%= Html.ValidationMessage("Display_Order", "*") %>
            </p>
            <p>
                <label for="Parent_Plan_Document_ID">Parent_Plan_Document_ID:</label>
                <%= Html.TextBox("Parent_Plan_Document_ID") %>
                <%= Html.ValidationMessage("Parent_Plan_Document_ID", "*") %>
            </p>
            <p>
                <input type="submit" value="Create" />
            </p>
        </fieldset>

    <% } %>

    <div>
        <%=Html.ActionLink("Back to List", "Index") %>
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>

