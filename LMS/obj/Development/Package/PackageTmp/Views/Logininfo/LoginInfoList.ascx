<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<LMS.Data.sp_LoginInfo_ListResult>>" %>
<%@ Import Namespace="LMS.Helpers" %>
<%@ Import Namespace="LMS.Data" %>
   <div class="client-outer">
    <div class="client-title">
         [ <%= Html.Encode(Model.Count()) %> Login Informations ]
    </div>
    <div class="client-inner">
    <table class="sortable" width="100%" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th>Action</th>
                <th>Agent/Agency Name</th>
                <th>Carrier/Non-carrier/State</th>
                <th>Carrier Site Link</th>
                <th>User Name</th>
                <th>Password</th>
            </tr>
        </thead>
        <tbody>
        <% foreach ( LMS.Data.sp_LoginInfo_ListResult log in Model)
           { %>
            <tr>
        
            <td>
            <% LMS.Data.User user = LMS.Data.Repository.UserRepository.GetUser(Page.User.Identity.Name); %>
            <%if (LMS.Data.Repository.UserRepository.HasSecurityLevel(user, 10))
              {%> 
                <%= Html.ActionLink("Edit", "Edit", new { id = log.User_Password_ID }, new { target = "_blank", style = "target-new: tab ! important" })%>&nbsp;
            <%} %>
                <%= Html.ActionLink("View", "Details", new { id = log.User_Password_ID }, new { target = "_blank", style = "target-new: tab ! important" })%>
            </td>
            <td>
            <%if (!String.IsNullOrEmpty(log.First_Name))
              { %>
                <%= Html.Encode(log.First_Name + " " + log.Last_Name)%>
            <%}
              else
              { %>
                <%= Html.Encode(log.Agency_Name)%>
            <%} %>
            </td>
            <td>
                <%= Html.Encode(log.Name)%>

                <%= Html.Encode(log.Abbreviation) %>
            </td>
            <td>
               <%if (log.Carrier_Site_Link.Length > 40 && log.Carrier_Site_Link != null)
                        {%>
                         <a href="<%= Html.Encode(log.Carrier_Site_Link) %>" target=_blank"><%= Html.Encode(log.Carrier_Site_Link.Substring(0, 40))%>...</a>
                      <%} %>
                    <%else
                          { %>
                        <a href="<%= Html.Encode(log.Carrier_Site_Link) %>" target=_blank"><%= Html.Encode(log.Carrier_Site_Link)%></a>
                      <%} %>
             
            </td>
            <td>
                <%=Html.Encode(log.User_Name)%>
            </td>    
            <td>
                <%=Html.Encode(log.Password)%>
            </td>      
                
           <%} %>
          </tr>
        </tbody>
    </table>
    </div>
</div>
