﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<LMS.Data.sp_Certification_List_By_AgentResult>>" %>
<%@ Import Namespace="LMS.Helpers" %>
 <div class="client-outer">
    <div class="client-title">
         [ <%= Html.Encode(Model.Count()) %> Certifications ]
    </div>
      <div class="client-inner">
        <table class="tablesorter" width="100%" cellpadding="0" cellspacing="0" id="sortable1">
        <thead>
            <tr>
            <th>Action</th>
            <th>Name</th>
            <th>Carrier/Non-Carrier</th>
             <th>Web Site Link</th>
            <th>User Name</th>
            <th>Password</th>
            <th>Due Date</th>
            <th>Date Completed</th>
            <th>Status</th>
            <th>Special Instructions</th>
          </tr>
        </thead>
        <tbody>
        <% foreach (LMS.Data.sp_Certification_List_By_AgentResult cert in Model)
           { %>
        <tr>
         <%if (cert.Notified == true)
             { %>
           <td bgcolor="#dee2e7">
            <% LMS.Data.User user = LMS.Data.Repository.UserRepository.GetUser(Page.User.Identity.Name); %>
            <%if (LMS.Data.Repository.UserRepository.HasSecurityLevel(user, 10))
              {%> 
                <%= Html.ActionLink("Edit", "Edit", new { id = cert.Certification_ID}, new { target = "_blank", style = "target-new: tab ! important" })%>&nbsp;
            <%} %>
                <%= Html.ActionLink("View", "Details", new { id = cert.Certification_ID}, new { target = "_blank", style = "target-new: tab ! important"  })%>
            </td>
             <td bgcolor="#dee2e7">
                  <%= Html.Encode(cert.First_Name + " " + cert.Last_Name)%>
             </td>
                <td bgcolor="#dee2e7">
                    <%= Html.Encode(cert.Name)%>
                </td>
                <td bgcolor="#dee2e7">
                  <%if (cert.Certification_Link.Length > 40 && cert.Certification_Link != null)
                        {%>
                         <a href="<%= Html.Encode(cert.Certification_Link) %>" target=_blank"><%= Html.Encode(cert.Certification_Link.Substring(0,40))%>...</a>
                      <%} %>
                    <%else
                          { %>
                        <a href="<%= Html.Encode(cert.Certification_Link) %>" target=_blank"><%= Html.Encode(cert.Certification_Link)%></a>
                      <%} %>
                </td>
                 <td bgcolor="#dee2e7">
                    <%= Html.Encode(cert.Certification_User_Name)%>
                </td>
                 <td bgcolor="#dee2e7">
                    <%= Html.Encode(cert.Certification_Password)%>
                </td>
             
                <td bgcolor="#dee2e7">
                <% if (cert.Due_Date != null)
                   { %>
                        <%= Html.Encode(cert.Due_Date.Value.ToShortDateString())%>
                    <% } %>
                    
                </td>
                     <td>
                 <% if (cert.Complete_Date != null)
                   { %>
                        <%= Html.Encode(cert.Complete_Date.Value.ToShortDateString())%>
                    <% } %>
                    
                </td>
                 <td bgcolor="#dee2e7">
                    <%= Html.Encode(cert.Certification_Status)%>
                </td>
                 <td bgcolor="#dee2e7">
                <% if (cert.Special_Instructions != null && cert.Special_Instructions.Length > 30)
               {%>
                <%= Html.Encode(cert.Special_Instructions.Substring(0, 25))%>&nbsp;
                 <%= Html.ActionLink("(more)", "Details", new { id = cert.Certification_ID }, new { target = "_blank" })%>
            <% }
               else
               { %>
                <%= Html.Encode(cert.Special_Instructions)%>
            <% } %>
            </td>
                    
              
               <%} %>
            <%else
               { %>
               <td>
                <% LMS.Data.User user = LMS.Data.Repository.UserRepository.GetUser(Page.User.Identity.Name); %>
                <%if (LMS.Data.Repository.UserRepository.HasSecurityLevel(user, 10))
                  {%> 
                    <%= Html.ActionLink("Edit", "Edit", new { id = cert.Certification_ID}, new { target = "_blank", style = "target-new: tab ! important" })%>&nbsp;
                <%} %>
                    <%= Html.ActionLink("View", "Details", new { id = cert.Certification_ID}, new { target = "_blank", style = "target-new: tab ! important"  })%>
                </td>
                 <td>
                  <%= Html.Encode(cert.First_Name + " " + cert.Last_Name)%>
                 </td>
                 <td>
                    <%= Html.Encode(cert.Name)%>
                </td>
                <td>
                      <%if (cert.Certification_Link.Length > 40 && cert.Certification_Link != null)
                        {%>
                         <a href="<%= Html.Encode(cert.Certification_Link) %>" target=_blank"><%= Html.Encode(cert.Certification_Link.Substring(0,40))%>...</a>
                      <%} %>
                    <%else
                          { %>
                        <a href="<%= Html.Encode(cert.Certification_Link) %>" target=_blank"><%= Html.Encode(cert.Certification_Link)%></a>
                      <%} %>
                 
                </td>
                 <td>
                    <%= Html.Encode(cert.Certification_User_Name)%>
                </td>
                 <td>
                    <%= Html.Encode(cert.Certification_Password)%>
                </td>
                <td>
                <% if (cert.Due_Date != null)
                   { %>
                        <%= Html.Encode(cert.Due_Date.Value.ToShortDateString())%>
                    <% } %>
                    
                </td>
                <td>
                 <% if (cert.Complete_Date != null)
                   { %>
                        <%= Html.Encode(cert.Complete_Date.Value.ToShortDateString())%>
                    <% } %>
                    
                </td>
                <td>
                    <%= Html.Encode(cert.Certification_Status)%>
                </td>
                 <td>
                    <% if (cert.Special_Instructions != null && cert.Special_Instructions.Length > 30)
                    {%>
                        <%= Html.Encode(cert.Special_Instructions.Substring(0, 25))%>&nbsp;
                         <%= Html.ActionLink("(more)", "Details", new { id = cert.Certification_ID }, new { target = "_blank" })%>
                    <% }
                    else
                    { %>
                        <%= Html.Encode(cert.Special_Instructions)%>
                    <% } %>
                </td>
             <%} %>
        </tr>
        <%} %>
        </tbody>
    </table>
    </div>
</div>