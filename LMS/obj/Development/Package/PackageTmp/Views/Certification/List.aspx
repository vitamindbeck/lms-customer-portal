<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Licensing.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Certifications
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<script type="text/javascript">
    $(document).ready(function() {
        $("#sortable1").tablesorter();
        $("#sortable2").tablesorter();
    }
    ); 
    
    $(function() {
    $('#CertificationList').load()
    {
        if ($("#chkCarrierWise").is(":checked")) {
            $("#DivCarrier").show();
            $("#DivCarrierResult").show();
            $("#DivAgent").hide();
            $("#DivAgentResult").hide();
        }
        else {
            $("#DivAgent").show();
            $("#DivAgentResult").show();
            $("#DivCarrier").hide();
            $("#DivCarrierResult").hide();
        }
    };

    $('.statedetails').popupWindow({
        centerBrowser: 1,
        scrollbars: 1
    }); 

    $("#chkCarrierWise").click(function() {
        if ($("#chkCarrierWise").is(":checked")) {
            $("#DivCarrier").show();
            $("#DivCarrierResult").show();
            $("#DivAgent").hide();
            $("#DivAgentResult").hide();
        }
        else {
            $("#DivAgent").show();
            $("#DivAgentResult").show();
            $("#DivCarrier").hide();
            $("#DivCarrierResult").hide();
        }
    });

});


</script>

<% using (Html.BeginForm("List", "Certification", FormMethod.Post,new { id = "CertificationList" }))
   {%>
<table width="100%" border="0">
 <tr>
 <td width="500px">
  
    <div id="DivAgent">
    Select Agent: <%= Html.DropDownList("Agent", (SelectList)ViewData["Agent_List"], new { tabindex = "1" })%>
    </div>
    <div id="DivCarrier">
    Select Carrier: <%= Html.DropDownList("Carrier", (SelectList)ViewData["Carrier_List"], new { tabindex = "1" })%>
    </div>
    <% LMS.Data.User user = LMS.Data.Repository.UserRepository.GetUser(Page.User.Identity.Name); %>
    <%if (LMS.Data.Repository.UserRepository.HasSecurityLevel(user, 10))
      {%>
     
            <%=Html.ActionLink("Add New Certification", "New", new { id = Int32.Parse(Request.Form["Agent"] ?? "1") })%>
       <%} %>
  </td>
  <td valign="top">
    View Information Carrier Wise: <%=Html.CheckBox("chkCarrierWise", (Request.Form["chkCarrierWise"] ?? string.Empty).Contains("true"))%>
  </td>
  <td valign="top"> 
    <a href="/Appointment/SSN" title="Agent SSN" class="statedetails">Agent SSN</a>
  </td>
<td align="right" valign="top">
<% LMS.Data.LMSDataContext db = new LMS.Data.LMSDataContext();
     List<LMS.Data.sp_Agent_ListResult> rs = db.sp_Agent_List().ToList();
      string agentid = rs[0].Agent_ID.ToString(); %>
    <%=Html.ActionLink("Export to Excel", "Export", new { AgentID = Int32.Parse(Request.Form["Agent"] ?? agentid), Chk = Request.Form["chkCarrierWise"] ?? "", CarrierID = Int32.Parse(Request.Form["Carrier"] ?? "271") })%>
  </td>
 </tr>
</table>
<div id="DivAgentResult">
<% Html.RenderPartial("CertificationListByAgent", ViewData["CertificationListByAgent"]); %>
</div>
<div id="DivCarrierResult">
<% Html.RenderPartial("CertificationListByCarrier", ViewData["CertificationListByCarrier"]); %>
</div>


<% } %>
    
<script type="text/javascript">
    $("#Agent").change(function() {
    $("#CertificationList").submit();
    });
    $("#Carrier").change(function() {
    $("#CertificationList").submit();
    });
    $("#chkCarrierWise").click(function() {
    $("#CertificationList").submit();
    });
    
</script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptContent" runat="server">
</asp:Content>
