<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    KeepSessionAlive
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        KeepSessionAlive</h2>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
    <meta id="MetaRefresh" http-equiv="refresh" content="21600;url=Home/KeepSessionAlive"
        runat="server" />

    <script language="javascript">
        window.status = '<%= ViewData["SessionInfo"]%>;
    </script>

</asp:Content>
