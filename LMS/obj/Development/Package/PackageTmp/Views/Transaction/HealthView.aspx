<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<LMS.Data.Plan_Transaction>" %>
<%@ Import Namespace="LMS.Helpers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%= Html.Encode(Model.DropDownName) %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% LMS.Data.Record record = (LMS.Data.Record)ViewData["Record"]; %>
    <% Html.RenderPartial("RecordHeaderDetails", record); %>
    <br />
    <div id="ClientFactsDiv" class="client-outer" style="background-color: #e0e4ee;">
        <div class="section-title">
            Plan Details:
        </div>
        <div class="section-inner">
            <table style="border: none;" class="section">
                <tr>
                    <td valign="top">
                        <table cellpadding="0" cellspacing="0" style="border: none;">
                            <tr>
                                <td class="label">
                                    Agent:
                                </td>
                                <td width="200px">
                                     <% if (Model.Agent_ID > 0)
                                       {%>
                                    <%= Html.Encode(Model.Agent.FullName) %>
                                    <%} %>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Source:
                                </td>
                                <td width="200px">
                                     <%= Html.Encode(Model.Source_Code.DropDownName)%>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Current Status:
                                </td>
                                <td width="200px">
                                     <%= Html.Encode(Model.Plan_Transaction_Status.Description)%>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    PreFill Application:
                                </td>
                                <td width="200px">
                                   <% if ((bool)Model.PreFillApp)
                                       { %>Yes<%}
                                       else
                                       { %>No<%} %>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    eApplication:
                                </td>
                                <td width="200px">
                                    <% if (Model.eApp)
                                       { %>Yes<%}
                                       else
                                       { %>No<%} %>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Initial Enrollment:
                                </td>
                                <td width="200px">
                                 <% if (Model.Initial_Enrollment)
                                       { %>Yes<%}
                                       else
                                       { %>No<%} %>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Guaranteed Issue:
                                </td>
                                <td width="200px">
                                <% if (Model.Guaranteed_Issue)
                                       { %>Yes<%}
                                       else
                                       { %>No<%} %>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Carrier:
                                </td>
                                <td width="200px">
                                    <% if (Model.Plan != null)
                                       { %>
                                    <%= Html.Encode(Model.Plan.Carrier.Name)%>
                                    <%} %>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Plan:
                                </td>
                                <td width="200px">
                                    <% if (Model.Plan != null)
                                       { %>
                                    <%= Html.Encode(Model.Plan.Name)%>
                                    <%} %>
                                </td>
                            </tr>
                             <tr>
                                <td class="label">
                                    Sub Plan:
                                </td>
                                <td width="200px">
                                    <%= Html.Encode(Model.Sub_Plan)%>
                                </td>
                            </tr>                         
                             <tr>
                                <td class="label">
                                    Underwriting Class:
                                </td>
                                <td width="200px">
                                     <% if (Model.Underwriting_Class != null)
                                       { %>
                                    <%= Html.Encode(Model.Underwriting_Class.Description)%>
                                    <%} %>
                                </td>
                            </tr>
                             <tr>
                                <td class="label">
                                    Modal Premium:
                                </td>
                                <td width="200px">
                                    <%= Html.Encode(string.Format("{0:c}", Model.Modal_Premium))%>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Mode:
                                </td>
                                <td width="200px">
                                    <% if (Model.Modal != null)
                                       { %>
                                    <%= Html.Encode(Model.Modal.Description)%>
                                    <%} %>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Annual Premium:
                                </td>
                                <td width="200px">
                                    <%= Html.Encode(string.Format("{0:c}", Model.Annual_Premium))%>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Cash With App:
                                </td>
                                <td width="200px">
                                    <% if (Model.Cash_With_Application.Value)
                                       { %>Yes<%}
                                       else
                                       { %>No<%} %>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Payment Type:
                                </td>
                                <td width="200px">
                                     <% if (Model.Payment_Mode != null)
                                       { %>
                                    <%= Html.Encode(Model.Payment_Mode.Description)%>
                                    <%} %>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Shipping Method Outbound:
                                </td>
                                <td width="200px">                                     
                                      <% if (Model.Outbound_Shipping_Method != null)
                                       { %>
                                   <%= Html.Encode(Model.Outbound_Shipping_Method.Description)%>
                                    <%} %>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Tracking Number Outbound:
                                </td>
                                <td width="200px">
                                    <%= Html.Encode(Model.Tracking_Number_Outbound)%>
                                </td>
                            </tr>
                         <tr>
                                <td class="label">
                                    Shipping Method Inbound:
                                </td>
                                <td width="200px">                                    
                                    <% if (Model.Inbound_Shipping_Method != null)
                                       { %>
                                  <%= Html.Encode(Model.Inbound_Shipping_Method.Description)%>
                                    <%} %>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Tracking Number Inbound:
                                </td>
                                <td width="200px">
                                     <%= Html.Encode(Model.Tracking_Number_Inbound)%>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="label">
                                    Policy Number:
                                </td>
                                <td width="200px">
                                    <%= Html.Encode(StringHelper.DecryptData(Model.Policy_Number))%>
                                </td>
                            </tr>
                            <% foreach (LMS.Data.Plan_Transaction_Date date in Model.Plan_Dates)
                               { %>
                            <tr>
                                <td class="label" style="border: none;">
                                    <%= Html.Encode(date.Transaction_Date_Type.Description) %>:
                                </td>
                                <td width="200px" style="border: none;">
                                    <%= Html.Encode((date.Date_Value.HasValue ? date.Date_Value.Value.ToString("MM/dd/yyyy") : ""))%>
                                </td>
                            </tr>
                            <%} %>
                            <tr>
                                <td class="label" valign="top">
                                    Special Instructions:
                                </td>
                                <td width="200px">
                                    <%= Html.Encode(Model.Special_Instructions)%>
                                </td>
                            </tr>
                            <tr>
                                <td class="label" valign="top">
                                    Case Manager:
                                </td>
                                <td width="200px">
                                    <%= Html.Encode(Model.Case_Manager_ID)%>
                                </td>
                            </tr>
                             <tr>
                                <td class="label">
                                    Buying Period:
                                </td>
                               
                                <td class="style1"> 
                                 <% if (Model.Buying_Period != null)
                                       { %>                                   
                                    <%= Html.Encode(Model.Buying_Period.Description)%>
                                     <%} %>                                    
                                </td>
                               
                            </tr>
                            <tr>
                                <td class="label">
                                    Sub Buying Period:
                                </td>
                                
                                <td class="style1"> 
                                <% if (Model.Buying_Period_Sub != null)
                                       { %>                                   
                                    <%= Html.Encode(Model.Buying_Period_Sub.Description)%>
                                     <%} %>
                                </td>
                               
                            </tr>
                            <tr>
                                <td class="label">
                                    Requested Effective Date:
                                </td>
                                <td class="style1">
                                    <%=Html.Encode(Model.Buying_Period_Effective_Date)%>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                         <a href="../../Record/Details/<%= Model.Record_ID %>">
                            <img src="../../content/icons/back.png" alt="Cancel" title="Back To Details" style="border: 0px;
                                vertical-align: top;" />
                        </a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
