<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Licensing.Master" Inherits="System.Web.Mvc.ViewPage<LMS.Data.Agent>" %>
<%@ Import Namespace="LMS.Helpers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Agent/Agency Details
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
 <script type="text/javascript">
     $(function() {


         if ('<%= Model.Agent_Type_ID %>' == 1) {
             $("#AgentDetails").show();
             $("#AgencyDetails").hide();
             $("#Associated_Agency_ID").attr('disabled', true);
             

         }
         else {
             $("#AgentDetails").hide();
             $("#AgencyDetails").show();
         }

       });
</script>

<div style="text-align: left;">
<% LMS.Data.User user = LMS.Data.Repository.UserRepository.GetUser(Page.User.Identity.Name); %>
<%if (LMS.Data.Repository.UserRepository.HasSecurityLevel(user, 10))
  {%>  
        <table class="section" cellpadding="0" cellspacing="0">
          <tr>
            <td>
                 <a href="../../agent/Edit/<%= Model.Agent_ID %>" style="text-decoration: none;">
                 <img src="../../content/icons/document_edit.png" title="Edit" alt="Edit" style="border: 0px;
                vertical-align: middle;" />
                </a> &nbsp;
                <%= Html.ActionLink("View Licenses ", "List", "License", new { id = Model.Agent_ID}, new { target = "_blank", style = "target-new: tab ! important" })%>
              </td>  
           
           </tr>    
         </table>
<%} %>     
</div>
    <br />
<div id ="AgentDetails" class="client-outer" style="background-color: #e0e4ee;">
    <div class="section-title">
       Agent Details
    </div>
    <div class="section-inner">
        <table class="section" cellpadding="0" cellspacing="0">
          <tr>
            <td class="label" style="width: 172px">
                Agent Name:
            </td>
            <td class="style2">
                <%= ViewData["Agent_Name"]%>
            </td>
             <td class="label" style="width: 172px">
                Date Of Birth:
            </td>
            <td class="style2">
                <%= Html.Encode(Model.DOB.HasValue ? Model.DOB.Value.ToShortDateString() : "") %>
            </td>
            </tr>
            <tr>
             <td class="label" style="width: 168px">
                Start Date:
            </td>
            <td>
                 <%= Html.Encode(Model.Agent_Start_Date.HasValue ? Model.Agent_Start_Date.Value.ToShortDateString() : "") %>
            </td>
            <td class="label" style="width: 172px">
                End Date:
            </td>
            <td class="style2">
               <%= Html.Encode(Model.Agent_End_Date.HasValue ? Model.Agent_End_Date.Value.ToShortDateString() : "") %>
              
            </td>
            </tr>
            <tr>
            <td class="label" style="width: 168px">
               NPN:
            </td>
            <td>
                <%= Html.Encode(Model.NPN)%>
            </td>
            <td class="label" style="width: 172px">
               SSN:
            </td>
            <td class="style2">
                <%= Html.Encode(StringHelper.DecryptData(Model.SSN)) %>
              
            </td>
            </tr>
            <tr>
               
              <td  class="label" >
              Associated Agency:
                
             </td>
                <td class="style2">
                
                            <% if (Model.Associated_Agency_ID != null)
                               { %>
                            <%= ViewData["Associated_Agency"]%>
                            <%}
                               else
                               { %>
                                None
                            <%} %>
                        </td>
               
             
                <td class="label" style="height: 18px">
                 Active:
                </td>
                <td>
                  <%=(Model.Active? "Yes":"No")%>
               </td>
            </tr>
            <tr>
                <td class="label" style="width: 172px">
                   Address Line 1:
                </td>
                <td class="style2">
                   <%=(Model.Address_1)%>
                </td>
                <td class="label" style="height: 18px">
                  Address Line 2:
                </td>
                <td class="style2">
                   <%=(Model.Address_2)%>
                </td>
            </tr>
            <tr>
                <td class="label" style="width: 172px">
                   City:
                </td>
                <td class="style2">
                   <%=(Model.City)%>
                </td>
                <td class="label" style="height: 18px">
                 State:
                </td>
                <td>
                  <% if (Model.State_ID  != null && Model.State_ID > 0)
                            { %>
                          
                            <%=(Model.State.Abbreviation)%>
                            <%}
                               else
                               { %>
                               
                            <%} %>
                        </td>
                  
            </tr>
            <tr>
                <td class="label" style="width: 172px">
                   County:
                </td>
                <td class="style2">
                   <%=(Model.County)%>
                </td>
                <td class="label" style="height: 18px">
                 Zip Code:
                </td>
                <td class="style2">
                   <%=(Model.Zip_Code)%>
                </td>
            </tr>
            <tr>
                <td class="label" style="width: 172px">
                   Home Phone Number:
                </td>
                <td class="style2">
                   <%=(Model.Phone_Number)%>
                </td>
                <td class="label" style="height: 18px">
                 Email Address:
                </td>
                <td class="style2">
                   <%=(Model.Email)%>
                </td>
            </tr>
          <tr>
            <td class="label" style="width: 172px">
               Last Updated:
            </td>
            
            <td class="style2">
                <%= Html.Encode(Model.Last_Updated_Date) %>
              
            </td>
            <td >
                 <%if (!String.IsNullOrEmpty(ViewData["UserName"].ToString()))
                  { %>
                <strong>By:</strong>
                <%} %>
            </td>
            <td>    
                <%= ViewData["UserName"] %>
            </td>
            
          </tr>
        </table>
    </div>
</div>

<div id ="AgencyDetails" class="client-outer" style="background-color: #e0e4ee;">
    <div class="section-title">
       Agent Details
    </div>
    <div class="section-inner">
        <table class="section" cellpadding="0" cellspacing="0">
          <tr>
            <td class="label" style="width: 173px">
                Agency Name:
            </td>
            <td class="style2">
                <%= ViewData["AgentName"]%>
            </td>
             <td class="label" style="width: 168px">
               In corporated Date:
            </td>
            <td>
                   <%= Html.Encode(Model.Agent_Start_Date.HasValue ? Model.Agent_Start_Date.Value.ToShortDateString() : "") %>
            </td>
            </tr>
            <tr>
            <td class="label" style="width: 168px">
               NPN:
            </td>
            <td>
                <%= Html.Encode(Model.NPN)%>
            </td>
            <td class="label" style="width: 173px">
               Tax ID:
            </td>
            <td class="style2">
                <%= Html.Encode(Model.Tax_ID) %>
            </td>
          </tr>
          <tr>
             <td class="label" style="height: 18px">
             Active:
           </td>
           <td>
              <%=(Model.Active? "Yes":"No")%>
           </td>
            <td class="label" style="height: 18px">
             Phone:
           </td>
           <td>
              <%=(Model.Phone_Number)%>
           </td>
          </tr>
          <tr>
          <td class="label" style="height: 18px">
             Address Line 1:
           </td>
           <td>
              <%=(Model.Address_1)%>
           </td>
            <td class="label" style="height: 18px">
             Address Line 2:
           </td>
           <td>
              <%=(Model.Address_2)%>
           </td>
          </tr>
            <tr>
          <td class="label" style="height: 18px">
            City:
           </td>
           <td>
              <%=(Model.City)%>
           </td>
            <td class="label" style="height: 18px">
             State:
           </td>
             <td>
                  <% if (Model.State_ID  != null && Model.State_ID > 0)
                            { %>
                          
                            <%=(Model.State.Abbreviation)%>
                            <%}
                               else
                               { %>
                               
                            <%} %>
                        </td>
                  
          </tr>
               <tr>
          <td class="label" style="height: 18px">
            County:
           </td>
           <td>
              <%=(Model.County)%>
           </td>
            <td class="label" style="height: 18px">
             Zip Code:
           </td>
           <td>
              <%=(Model.Zip_Code)%>
           </td>
          </tr>
          
          <tr>
          
            <td class="label" style="width: 173px">
               Last Updated:
            </td>
            <td>
                <%= Html.Encode(Model.Last_Updated_Date) %>
            </td>
             <td>
                <%if (!String.IsNullOrEmpty(ViewData["UserName"].ToString()))
                  { %>
                <strong>By:</strong>
                <%} %>
             </td>
             <td>
                <%= ViewData["UserName"] %>
            </td>
          </tr>
          <tr>
           <td>
                <%if (!String.IsNullOrEmpty(ViewData["Associated_Agency"].ToString()))
                  { %>
                <strong>Associated Agency:</strong>
                <%} %>
             </td>
             <td class="style2">
                
                            <% if (Model.Associated_Agency_ID != null)
                               { %>
                            <%= ViewData["Associated_Agency"]%>
                            <%}
                               else
                               { %>
                                
                            <%} %>
                        </td>
      
             </tr>
          </table>
    </div>
</div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
    <style type="text/css">
        .style2
        {
            width: 220px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptContent" runat="server">
</asp:Content>
