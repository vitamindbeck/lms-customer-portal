﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<LMS.Models.CustomerPortalViewModel>" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <title>Index</title>
</head>
<body>
    <div>
        <h2>Customer Portal - <%= string.Format("{0} - {1} {2} {3}", Model.CustomerId, Model.FirstName, Model.MiddleInitial, Model.LastName)%></h2>
    </div>
    <div>
        <img alt="Add User" src="<%: Url.Content("~/content/icons/user_add.png") %>"/>
        <img alt="Add Policy" src="<%: Url.Content("~/content/icons/folder_add.png") %>" />
        <img alt="Print Name Label" src="<%: Url.Content("~/Content/icons/printer-orange-icon.png") %>" />
        <img alt="Print Address Label" src="<%: Url.Content("~/Content/icons/printer-green-icon.png") %>" />
    </div>
    <div>
        
        <div style="clear:both"></div>
    </div>
</body>
</html>
