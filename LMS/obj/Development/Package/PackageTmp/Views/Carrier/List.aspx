<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<LMS.Data.Carrier>>" %>

<%@ Import Namespace="LMS.Helpers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Carriers
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <script type="text/javascript">
        $(
 function() {
     $("#CarrierTable tr").hover(
   function() {
       $(this).addClass("highlight");
   },
   function() {
       $(this).removeClass("highlight");
   }
  )
 }
)
    </script>

    <table class="sortable" id="CarrierTable" cellpadding="5" cellspacing="0">
        <thead>
            <tr>
                <th>
                </th>
                <th>
                    Name
                </th>
                <th>
                    Address
                </th>
                <th>
                    City
                </th>
                <th>
                    State
                </th>
                <th>
                    Active
                </th>
                <th>
                    Plans
                </th>
            </tr>
        </thead>
        <tbody>
            <% foreach (var item in Model)
               { %>
            <tr>
                <td>
                    <%= Html.ActionLink("Edit", "Edit", new { id=item.Carrier_ID }) %>
                </td>
                <td>
                    <%= Html.Encode(item.Name) %>
                </td>
                <td>
                    <%= Html.Encode(item.Address_1) %>
                </td>
                <td>
                    <%= Html.Encode(item.City) %>
                </td>
                <td>
                <% if (item.State != null)
                   { %>
                    <%= Html.Encode(item.State.Abbreviation)%>
                   <% }%>
                </td>
                <td>
                    <%= Html.Encode(item.Active ? "Yes" : "No") %>
                </td>
                <td>
                    <%= Html.ImageLink("List", "Plan", new { id = item.Carrier_ID }, "../../Content/images/folder-open.png", "Plans", null, new { style = "border:0px;" })%>
                </td>
            </tr>
            <% } %>
        </tbody>
    </table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
