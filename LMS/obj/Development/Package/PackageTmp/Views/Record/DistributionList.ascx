<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<LMS.Data.sp_Get_Distribution_ListResult>>" %>

<% foreach( LMS.Data.sp_Get_Distribution_ListResult rs in Model)
  { 
     if(rs.Record_ID != null || (ViewData[rs.Record_Distribution_Type_ID + "_" + rs.Record_Distribution_Type_Description] != null && ViewData[rs.Record_Distribution_Type_ID + "_" + rs.Record_Distribution_Type_Description] == "true"))
     {%>
        
        <%=Html.CheckBox(rs.Record_Distribution_Type_ID + "_" + rs.Record_Distribution_Type_Description.Replace(' ','_'),true) %>
        <%=Html.Encode(rs.Record_Distribution_Type_Description) %>
        &nbsp;&nbsp;
   <%}
     else
     { %>
        
        <%=Html.CheckBox(rs.Record_Distribution_Type_ID + "_" + rs.Record_Distribution_Type_Description.Replace(' ','_'),false) %>
        <%=Html.Encode(rs.Record_Distribution_Type_Description) %>
        &nbsp;&nbsp;
   <%} %>
<%} %>   