<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>
<%@ Import Namespace="LMS.Data" %>
<%@ Import Namespace="LMS.Helpers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Search
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   
 
    <script type="text/javascript">


        function ClearQuickSearchList() {
            $("#QFirstName").val('');
            $("#QMiddleName").val('');
            $("#QLastName").val('');
            $('#QStateID')[0].selectedIndex = 0;
            $("#QDOB").val('');
            $("#QEmailAddress").val('');

        }

        function ClearList() {
            $("#FirstName").val('');
            $("#LastName").val('');
            $("#PhoneNumber").val('');
            $("#Zipcode").val('');
            $("#ContactName").val('');
            $("#ContactEmail").val('');
            $("#ContactPhone").val('');
            $('#StateID')[0].selectedIndex = 0;
            $('#AgentID')[0].selectedIndex = 0;
            $('#AssistantAgentID')[0].selectedIndex = 0;
            $('#SourceCodeID')[0].selectedIndex = 0;
            $('#RecordTypeID')[0].selectedIndex = 0;
            $('#PlanStatus')[0].selectedIndex = 0;
            $("#CreatedOnFrom").val('');
            $("#CreatedOnTo").val('');
            $("#FollowUpFrom").val('');
            $("#FollowUpTo").val('');
            $("#EmailAddress").val('');
            $('#BuyingPeriodID')[0].selectedIndex = 0;
            $('#BuyingPeriodSubID')[0].selectedIndex = 0;
            $("#Customer_ID").val('');
            $('#FollowUpAgent')[0].selectedIndex = 0;
            $('#TransactionDateType')[0].selectedIndex = 0;
            $('#TransactionDateFromValue').val('');
            $('#TransactionDateToValue').val('');
            $('#ViewBadData').attr('checked', false);
            $('#ViewOpenActivities').attr('checked', false);
            $('#ReconcilliationType')[0].selectedIndex = 0;
            $('#PlanSourceCodeID')[0].selectedIndex = 0;
            $('#RecordActivityType')[0].selectedIndex = 0;
            $('#RecordEligibilityType')[0].selectedIndex = 0;
            $('#RecordActivityStatus')[0].selectedIndex = 0;
            $('#PriorityLevel')[0].selectedIndex = 0;
        }
        function SetQuickSearchCookie() {
            if ($("#QFirstName").val().length > 0) {
                $.cookie('QFName', $("#QFirstName").val());
            }

            if ($("#QMiddleName").val().length > 0) {
                $.cookie('QMName', $("#QMiddleName").val());
            }
            
            if ($("#QLastName").val().length > 0) {
                $.cookie('QLName', $("#QLastName").val());
            }

           
          }
            function SetAdSearchCookie() {
            if ($("#FirstName").val().length > 0) {
                $.cookie('QFName', $("#FirstName").val());
            }
            else {
                $.cookie('QFName', null);
            }
          
            if ($("#LastName").val().length > 0) {
                $.cookie('QLName', $("#LastName").val());
            }
            else {
                $.cookie('QLName', null);
            }
            }

        $(function() {
            $(".section-title").click(function() {
                $(this).next(".section-inner").slideToggle(300)
                return false;
            });

            $("#QDOB").mask("99/99/9999");

            function RunLoading() {
                $("#resultDiv").slideUp();

            }

            function UnRunLoading() {
                $("#resultDiv").slideDown();
            }

            $(function() {
                $("#CreatedOnFrom").datepicker({
                    changeMonth: true,
                    changeYear: true,
                    yearRange: '1900:2020'
                });
                $("#CreatedOnTo").datepicker({
                    changeMonth: true,
                    changeYear: true,
                    yearRange: '1900:2020'
                });
                $("#FollowUpFrom").datepicker({
                    changeMonth: true,
                    changeYear: true,
                    yearRange: '1900:2020'
                });
                $("#FollowUpTo").datepicker({
                    changeMonth: true,
                    changeYear: true,
                    yearRange: '1900:2020'
                });
                $("#TransactionDateToValue").datepicker({
                    changeMonth: true,
                    changeYear: true,
                    yearRange: '1900:2020'
                });
                $("#TransactionDateFromValue").datepicker({
                    changeMonth: true,
                    changeYear: true,
                    yearRange: '1900:2020'
                });
            });

        });
        $(function() {
        $("#tabs").tabs(
            {
                cookie: {
                     //store cookie for a day, without, it would be a session cookie
                    expires: 1
                }
            }
            );
        });

        function deleteComplete() {
        }
       
</script>
<%= Html.ValidationSummary("Unable to Search.") %>
<% LMS.Data.User user = LMS.Data.Repository.UserRepository.GetUser(User.Identity.Name); %>
    <% using (Html.BeginForm())
       {%>
     
    <div id="tabs" style="font-size: 1em; font-family: Calibri;">
        <ul class="ui-tabs-nav">
            <li><a href="#tabs-1">Quick Search</a></li>
            <li><a href="#tabs-2">Advance Search</a></li>
        </ul>
        <div id="tabs-1" style="background-color: #e0e4ee;">
          <table class="section" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="label">
                        First Name:
                    </td>
                    <td>
                        <%=Html.TextBox("QFirstName") %>
                        <%= Html.ValidationMessage("QFirstName", "*")%>   
                    </td>
                    <td class="label">
                        Middle Initial:
                    </td>
                    <td>
                        <%=Html.TextBox("QMiddleName") %>
                        <%= Html.ValidationMessage("QMiddleName", "*")%>   
                    </td>
                     <td class="label">
                        Last Name:
                    </td>
                    <td>
                        <%=Html.TextBox("QLastName") %>
                        <%= Html.ValidationMessage("QLastName", "*")%>   
                    </td>
                   
                  </tr>
                  <tr>
                   <td class="label">
                        State:
                    </td>
                    <td>
                        <%=Html.DropDownList("QStateID") %>
                    </td>
                   <td class="label">
                        Birth Date:
                    </td>
                    <td>
                        <%= Html.TextBox("QDOB") %>
                        <%= Html.ValidationMessage("QDOB","*") %>   
                    </td>
                   <td class="label">
                       Email Address:
                    </td>
                    <td>
                        <%= Html.TextBox("QEmailAddress") %>
                        <%= Html.ValidationMessage("QEmailAddress", "*")%>   
                    </td>
                  </tr>
                  <tr>
                   <td class="label">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                   <td class="label">
                       &nbsp;</td>
                    <td>
                        &nbsp;</td>
                  </tr>
                  <tr>
                    <td colspan="4" align="center">
                     
                        <input type="submit" value="q" name="btnSearch" class="displaySearchImage" style="color: #e0e4ee;" onclick="SetQuickSearchCookie()"/>
                       
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <img id ="ClearQuickSearch" alt="Clear Quick Search" src="../../Content/icons/cancel.png" onclick="ClearQuickSearchList()" title="Clear Search" />
                    </td>
                  </tr>
                  <tr>
                    <td colspan="4" align="center">
                        &nbsp;</td>
                  </tr>
                  </table>
    
           <% if (ViewData["QuickSearchData"] != null) 
     { %>
     
      <script type="text/javascript">
          window.status = 'Number of Results: <%= Html.Encode(((IEnumerable<LMS.Data.Record>)ViewData["QuickSearchData"]).Count()) %>';
       </script>
        <table class="sortable" width="100%" cellpadding="0" cellspacing="0" 
                style="border: solid 1px #304f9f; background-color: #FFFFFF;">
        <thead>
            <tr>
                <% if (user.Security_Level_ID == 2)
                   { %>
                <th></th>
                <%} %>
                <th width="100px">
                    Submit Date
                </th>
                <th>
                    Name
                </th>
                <th>
                    Customer ID
                </th>
                <th>
                    DOB
                </th>
                 <th>
                    Email Address
                </th>
                <th>
                    State
                </th>
                 <th>
                   Has Partner
                </th>
                <th>
                    Source
                </th>
                <th>
                    Last Contact Date
                </th>
              
                <th>
                    Agent
                </th>
            </tr>
        </thead>
        <tbody>
            <% foreach (var item in (IEnumerable<LMS.Data.Record>)ViewData["QuickSearchData"])
               { %>
            <tr>
                <% if (user.Security_Level_ID == 2)
                   { %>
                <td>
                    <%=Ajax.ActionLink("Delete", "Delete", new { id = item.Record_ID}, new AjaxOptions { Confirm = "Are you sure you want to delete?", OnComplete = "deleteComplete", HttpMethod = "POST"})%>
                </td>
                <%} %>
                <td>
                    <% if (item.Created_On.HasValue)
                       { %>
                    <%= Html.Encode(item.Created_On.Value.ToShortDateString()) %>
                    <%}
                       else
                       { %>
                    None
                    <%} %>
                </td>
                <td nowrap="nowrap">
                    <%= Html.ActionLink(StringHelper.UppercaseFirst(item.Last_Name) + ", " + StringHelper.UppercaseFirst(item.First_Name), "Details", new { id = item.Record_ID }, new { target = "_blank", style = "target-new: tab ! important"  })%>
                </td>
                <td>
                    <%= Html.Encode(item.Customer_ID) %>
                </td>
                 <td>
                        <%= Html.Encode(item.DOB.HasValue==true ? item.DOB.Value.ToString("MM/dd/yyyy") : "")%>
                </td>
                 <td>
                    <%= Html.Encode(item.Email_Address) %>
                </td>
                <td>
                    <%= Html.Encode(item.State.Abbreviation) %>
                </td>
                <td>
                 <% if (item.Partner_ID > 0 && item.Partner_ID != null)
                  { %>
                    <%= Html.Encode("Yes")%>
                 <%}
                 else
                 { %>
                  <%= Html.Encode("No") %>
                    <%} %>
                 </td>
                <td>
                    <% if (item.Source_Code != null)
                       { %>
                    <%= Html.Encode(item.Source_Code.Source_Number)%>
                    <%}
                       else
                       { %>
                    None
                    <%} %>
                </td>
                <td>
                    <%= Html.Encode(item.Follow_Up_Date) %>
                </td>
                
                <td>
                    <% if (item.Agent != null)
                       { %>
                    <%= Html.Encode(item.Agent.FullName)%>
                    <%} %>
                </td>
            </tr>
            <% } %>
        </tbody>
    </table>
    <table>
        <tr>
        <td>
            <%= Html.Encode(((IEnumerable<LMS.Data.Record>)ViewData["QuickSearchData"]).Count()) %> Results from <b>Quick Search</b>. To create a new lead, please 
        </td>
         <td style="font-size: large">
            <%=Html.ActionLink("click here", "Create", "Record")%>
        </td>
       </tr>
    </table>
   
    <% }%> 
   </div>
        <div id="tabs-2" style="background-color: #e0e4ee;">
              <table class="section" cellpadding="0" cellspacing="0">  
               <tr>
                    <td class="label">
                        First Name:
                    </td>
                    <td>
                        <%=Html.TextBox("FirstName") %>
                    </td>
                    <td class="label">
                        Agent:
                    </td>
                    <td>
                        <%=Html.DropDownList("AgentID") %>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Last Name:
                    </td>
                    <td>
                        <%=Html.TextBox("LastName") %>
                    </td>
                    <td class="label">
                        Sales Coordinator:
                    </td>
                    <td>
                        <%=Html.DropDownList("AssistantAgentID") %>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Phone:
                    </td>
                    <td>
                        <%=Html.TextBox("PhoneNumber") %>
                    </td>
                    <td class="label">
                        Record Source Code:
                    </td>
                    <td>
                        <%=Html.DropDownList("SourceCodeID") %>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        State:
                    </td>
                    <td>
                        <%=Html.DropDownList("StateID") %>
                    </td>
                    <td class="label">
                        Record Type:
                    </td>
                    <td>
                        <%=Html.DropDownList("RecordTypeID") %>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Zip:
                    </td>
                    <td>
                        <%=Html.TextBox("Zipcode", "", new { style = "width: 80px;" })%>
                    </td>
                    <td class="label">
                        Plan Status:
                    </td>
                    <td>
                        <%=Html.DropDownList("PlanStatus") %>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Email Address:
                    </td>
                    <td>
                        <%=Html.TextBox("EmailAddress", "", new { style = "width: 150px;" })%>
                    </td>
                    <td class="label">
                        Buying Period:
                    </td>
                    <td>
                        <%=Html.DropDownList("BuyingPeriodID")%>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Created On:
                    </td>
                    <td>
                        <%=Html.TextBox("CreatedOnFrom", "", new { style = "width: 80px;" })%>
                        To
                        <%=Html.TextBox("CreatedOnTo", "", new { style = "width: 80px;" })%>
                    </td>
                    <td class="label">
                        Buying Period Sub:
                    </td>
                    <td>
                        <%=Html.DropDownList("BuyingPeriodSubID")%>
                    </td>
               </tr>
                <tr>
                    <td class="label">
                        Follow Up:
                    </td>
                    <td>
                        <%=Html.TextBox("FollowUpFrom", "", new { style = "width: 80px;" })%>
                        To
                        <%=Html.TextBox("FollowUpTo", "", new { style = "width: 80px;" })%>
                    </td>
                    <td class="label">
                        Customer ID:
                    </td>
                    <td>
                        <%= Html.TextBox("Customer_ID") %>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Follow Up For:
                    </td>
                    <td>
                        <%=Html.DropDownList("FollowUpAgent") %>
                    </td>
                    <td class="label">
                        Hide Bad Record Types:
                    </td>
                    <td>
                        <%=Html.CheckBox("ViewBadData") %>
                    </td>
                 </tr>
                 <tr>
                    <td class="label">
                        Open Activities Only:
                    </td>
                    <td>
                        <%=Html.CheckBox("ViewOpenActivities") %>
                    </td>
                     <td class="label">
                        Lead Disposition:
                    </td>
                    <td>
                        <%=Html.DropDownList("ReconcilliationType")%>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Contact Name:
                    </td>
                    <td>
                        <%=Html.TextBox("ContactName") %>
                    </td>
                    <td class="label">
                        Plan Source Code:
                    </td>
                    <td>
                        <%=Html.DropDownList("PlanSourceCodeID") %>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Contact Email:
                    </td>
                    <td>
                        <%=Html.TextBox("ContactEmail") %>
                    </td>
                    <td class="label">
                        Plan Date Value:
                    </td>
                    <td>
                        <%=Html.TextBox("TransactionDateFromValue", "", new { style = "width: 80px;" })%>
                        To
                        <%=Html.TextBox("TransactionDateToValue", "", new { style = "width: 80px;" })%>
                    </td>
               </tr>
                <tr>
                    <td class="label">
                        Contact Phone:
                    </td>
                    <td>
                        <%=Html.TextBox("ContactPhone") %>
                    </td>
                    <td class="label">
                        Plan Date Type:
                    </td>
                    <td>
                        <%=Html.DropDownList("TransactionDateType")%>
                    </td>
                    
                </tr>
                <tr>
                    <td class="label">
                        Record Eligibility:
                    </td>
                    <td width="200px">
                        <%=Html.DropDownList("RecordEligibilityType", (SelectList)ViewData["RecordEligibilityType"], new { style = "width: 200px;" })%>
                    </td>
                    <td class="label">
                        Record Activity Type:
                    </td>
                    <td>
                        <%=Html.DropDownList("RecordActivityType") %>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Current Situation:
                    </td>
                    <td width="200px">
                        <%=Html.DropDownList("Record_Current_Situation_ID")%>
                    </td>
                    <td class="label">
                        Record Activity Status:
                    </td>
                    <td>
                        <%=Html.DropDownList("RecordActivityStatus") %>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td class="label">
                        Activity Priority: </td>
                    <td>
                        <%=Html.DropDownList("PriorityLevel")%></td>
                </tr>
                <tr>
                    <td class="label">
                        Distribution List:
                    </td>
                    <td colspan="5">
                        <% Html.RenderPartial("DistributionList", ViewData["DistributionList"]); %>
                    </td>
                 </tr>
                <tr>
                    <td colspan="4" align="center">
                        <input type="submit" value="s" id="btnSearch" name="btnSearch" class="displaySearchImage" style="color: #e0e4ee;" onclick="SetAdSearchCookie()"/>
                       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                       <img id ="ClearSearch" alt="Clear Search" src="../../Content/icons/cancel.png" onclick="ClearList()" title="Clear Search" />
                   </td>
                </tr>
                <tr>
                
                    <td colspan="4" align="center">
                        &nbsp;</td>
                </tr>
           </table>
         <br />
    <% if (ViewData["SearchData"] != null)
       { %>
       
       <script type="text/javascript">
           window.status = 'Number of Results: <%= Html.Encode(((IEnumerable<LMS.Data.Record>)ViewData["SearchData"]).Count()) %>';
       </script>
       <table class="sortable" width="100%" cellpadding="0" cellspacing="0" 
                style="border: solid 1px #304f9f; background-color: #FFFFFF;">
        <thead>
            <tr>
                <% if (user.Security_Level_ID == 2)
                   { %>
                <th></th>
                <%} %>
                <th width="100px">
                    Submit Date
                </th>
                <th>
                    Name
                </th>
                <th>
                    Customer ID
                </th>
                 <th>
                    DOB
                </th>
                 <th>
                    Email Address
                </th>
                <th>
                   Has Partner
                </th>
                <th>
                    State
                </th>
                <th>
                    Source
                </th>
                <th>
                    Last Contact Date
                </th>
                <th>
                    Priority
                </th>
                <th>
                    Agent
                </th>
            </tr>
        </thead>
      <tbody>
            <% foreach (var item in (IEnumerable<LMS.Data.Record>)ViewData["SearchData"])
               { %>
            <tr>
                <% if (user.Security_Level_ID == 2)
                   { %>
                <td>
                    <%=Ajax.ActionLink("Delete", "Delete", new { id = item.Record_ID}, new AjaxOptions { Confirm = "Are you sure you want to delete?", OnComplete = "deleteComplete", HttpMethod = "POST"})%>
                </td>
                <%} %>
                <td>
                    <% if (item.Created_On.HasValue)
                       { %>
                    <%= Html.Encode(item.Created_On.Value.ToShortDateString()) %>
                    <%}
                       else
                       { %>
                    None
                    <%} %>
                </td>
                <td>
                    <%= Html.ActionLink(LMS.Helpers.StringHelper.UppercaseFirst(item.Last_Name) + ", " + LMS.Helpers.StringHelper.UppercaseFirst(item.First_Name), "Details", new { id = item.Record_ID }, new { target = "_blank", style = "target-new: tab ! important"  })%>
                </td>
                <td>
                    <%= Html.Encode(item.Customer_ID) %>
                </td>
                 <td>
                      <%= Html.Encode(item.DOB.HasValue==true ? item.DOB.Value.ToString("MM/dd/yyyy") : "")%>
                </td>
                 <td>
                    <%= Html.Encode(item.Email_Address) %>
                </td> 
                <td>
                 <% if (item.Partner_ID > 0 && item.Partner_ID != null)
                  { %>
                    <%= Html.Encode("Yes")%>
                 <%}
                 else
                 { %>
                  <%= Html.Encode("No") %>
                    <%} %>
                 </td>
                <td>
                    <%= Html.Encode(item.State.Abbreviation) %>
                </td>
                <td>
                    <% if (item.Source_Code != null)
                       { %>
                    <%= Html.Encode(item.Source_Code.Source_Number)%>
                    <%}
                       else
                       { %>
                    None
                    <%} %>
                </td>
                <td>
                    <%= Html.Encode(item.Follow_Up_Date) %>
                </td>
                <td>
                    <%= Html.Encode(item.Priority_Level) %>
                </td>
                <td>
                    <% if (item.Agent != null)
                       { %>
                    <%= Html.Encode(item.Agent.FullName)%>
                    <%} %>
                </td>
            </tr>
            <% } %>
        </tbody>
    </table>
       <table>
          <tr>
            <td>
                <%= Html.Encode(((IEnumerable<LMS.Data.Record>)ViewData["SearchData"]).Count()) %> Results from <b>Advance Search</b>. To create a new lead, please 
            </td>
            <td style="font-size: large">
                <%=Html.ActionLink("click here", "Create", "Record")%>
            </td>
           
          </tr>
        </table>
    <% }%>
 

 

        </div>
    </div>
    <% } %>
   <script type="text/javascript">

    $('#FirstName').keypress(function(e){
        if (e.keyCode == '13') {
            e.preventDefault();
            $('#btnSearch').click();
    }
    });
    $('#LastName').keypress(function(e) {
        if (e.keyCode == '13') {
            e.preventDefault();
            $('#btnSearch').click();
        }
    });
    $('#PhoneNumber').keypress(function(e) {
        if (e.keyCode == '13') {
            e.preventDefault();
            $('#btnSearch').click();
        }
    });
    $('#StateID').keypress(function(e) {
        if (e.keyCode == '13') {
            e.preventDefault();
            $('#btnSearch').click();
        }
    });
    $('#Zipcode').keypress(function(e) {
        if (e.keyCode == '13') {
            e.preventDefault();
            $('#btnSearch').click();
        }
    });
    $('#ContactName').keypress(function(e) {
        if (e.keyCode == '13') {
            e.preventDefault();
            $('#btnSearch').click();
        }
    });
    $('#ContactEmail').keypress(function(e) {
        if (e.keyCode == '13') {
            e.preventDefault();
            $('#btnSearch').click();
        }
    });
    $('#ContactPhone').keypress(function(e) {
        if (e.keyCode == '13') {
            e.preventDefault();
            $('#btnSearch').click();
        }
    });
    $('#AgentID').keypress(function(e) {
        if (e.keyCode == '13') {
            e.preventDefault();
            $('#btnSearch').click();
        }
    });
    $('#AssistantAgentID').keypress(function(e) {
        if (e.keyCode == '13') {
            e.preventDefault();
            $('#btnSearch').click();
        }
    });
    $('#SourceCodeID').keypress(function(e) {
        if (e.keyCode == '13') {
            e.preventDefault();
            $('#btnSearch').click();
        }
    });
    $('#RecordTypeID').keypress(function(e) {
        if (e.keyCode == '13') {
            e.preventDefault();
            $('#btnSearch').click();
        }
    });
    $('#PlanStatus').keypress(function(e) {
        if (e.keyCode == '13') {
            e.preventDefault();
            $('#btnSearch').click();
        }
    });
    $('#CreatedOnFrom').keypress(function(e) {
        if (e.keyCode == '13') {
            e.preventDefault();
            $('#btnSearch').click();
        }
    });
    $('#CreatedOnTo').keypress(function(e) {
        if (e.keyCode == '13') {
            e.preventDefault();
            $('#btnSearch').click();
        }
    });
    $('#FollowUpFrom').keypress(function(e) {
        if (e.keyCode == '13') {
            e.preventDefault();
            $('#btnSearch').click();
        }
    });
    $('#FollowUpTo').keypress(function(e) {
        if (e.keyCode == '13') {
            e.preventDefault();
            $('#btnSearch').click();
        }
    });
    $('#EmailAddress').keypress(function(e) {
        if (e.keyCode == '13') {
            e.preventDefault();
            $('#btnSearch').click();
        }
    });
    $('#BuyingPeriodID').keypress(function(e) {
        if (e.keyCode == '13') {
            e.preventDefault();
            $('#btnSearch').click();
        }
    });
    $('#BuyingPeriodSubID').keypress(function(e) {
        if (e.keyCode == '13') {
            e.preventDefault();
            $('#btnSearch').click();
        }
    });
    $('#Customer_ID').keypress(function(e) {
        if (e.keyCode == '13') {
            e.preventDefault();
            $('#btnSearch').click();
        }
    });
    $('#FollowUpAgent').keypress(function(e) {
        if (e.keyCode == '13') {
            e.preventDefault();
            $('#btnSearch').click();
        }
    });
    $('#TransactionDateType').keypress(function(e) {
        if (e.keyCode == '13') {
            e.preventDefault();
            $('#btnSearch').click();
        }
    });
    $('#TransactionDateFromValue').keypress(function(e) {
        if (e.keyCode == '13') {
            e.preventDefault();
            $('#btnSearch').click();
        }
    });
    $('#TransactionDateToValue').keypress(function(e) {
        if (e.keyCode == '13') {
            e.preventDefault();
            $('#btnSearch').click();
        }
    });
    $('#ViewBadData').keypress(function(e) {
        if (e.keyCode == '13') {
            e.preventDefault();
            $('#btnSearch').click();
        }
    });
    $('#ViewBadData').keypress(function(e) {
        if (e.keyCode == '13') {
            e.preventDefault();
            $('#btnSearch').click();
        }
    });
    $('#ViewOpenActivities').keypress(function(e) {
        if (e.keyCode == '13') {
            e.preventDefault();
            $('#btnSearch').click();
        }
    });
    $('#ReconcilliationType').keypress(function(e) {
        if (e.keyCode == '13') {
            e.preventDefault();
            $('#btnSearch').click();
        }
    });
    $('#PlanSourceCodeID').keypress(function(e) {
        if (e.keyCode == '13') {
            e.preventDefault();
            $('#btnSearch').click();
        }
    });
    $('#RecordActivityType').keypress(function(e) {
        if (e.keyCode == '13') {
            e.preventDefault();
            $('#btnSearch').click();
        }
    });
    $('#RecordEligibilityType').keypress(function(e) {
        if (e.keyCode == '13') {
            e.preventDefault();
            $('#btnSearch').click();
        }
    });
    $('#RecordActivityStatus').keypress(function(e) {
        if (e.keyCode == '13') {
            e.preventDefault();
            $('#btnSearch').click();
        }
    });
    $('#PriorityLevel').keypress(function(e) {
        if (e.keyCode == '13') {
            e.preventDefault();
            $('#btnSearch').click();
        }
    });
    
   </script>
</asp:Content>
