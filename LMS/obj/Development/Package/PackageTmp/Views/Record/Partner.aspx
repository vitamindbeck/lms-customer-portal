<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Partner
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <script type="text/javascript">
        $(function() {
            $(".person").draggable({ revert: 'invalid', opacity: 0.7, helper: 'clone' });
            $(".personright").draggable({ revert: 'invalid', opacity: 0.7, helper: 'clone' });
            //            $("#PartnerOne").droppable({
            //                drop: function(event, ui) {
            //                    $(this).addClass('ui-state-highlight').find('p').html('Dropped!');
            //                }
            //            });
            $("#PartnerOne").droppable({
                accept: ".person",
                activeClass: 'droppable-active',
                hoverClass: "hover",
                drop: function(ev, ui) {

                    document.getElementById("PartnerOne").innerHTML = '<table>' + $(ui.draggable).children("table").html() + '</table>';
                    //document.getElementById("PartnerOne").innerHTML = $(ui.draggable).children("p").text();

                    var lid = $(ui.draggable).children("table").children("tbody").children("tr").children("td").children("input").val();

                    var removeLink = document.createElement("a");
                    removeLink.innerHTML = "remove";
                    removeLink.href = "#";
                    removeLink.onclick = function() {
                        $("#PartnerOne").children().remove(".person");
                        $(this).remove();
                        document.getElementById("PartnerOne").innerHTML = '';
                    }

                    //$(this).append($(ui.draggable).clone().children("p").addClass("blocker"));
                    $(this).append(removeLink);
                    var currentElement = document.createElement("input");
                    currentElement.setAttribute("type", "hidden");
                    currentElement.setAttribute("name", "RecordLeft");
                    currentElement.setAttribute("id", "RecordLeft");
                    currentElement.setAttribute("value", lid);

                    $(this).append(currentElement);
                }
            });

            $("#PartnerTwo").droppable({
                accept: ".personright",
                activeClass: 'droppable-active',
                hoverClass: 'droppable-hover',
                drop: function(ev, ui) {

                    document.getElementById("PartnerTwo").innerHTML = '<table>' + $(ui.draggable).children("table").html() + '</table>';
                    //document.getElementById("PartnerOne").innerHTML = $(ui.draggable).children("p").text();

                    var lid = $(ui.draggable).children("table").children("tbody").children("tr").children("td").children("input").val();

                    var removeLink = document.createElement("a");
                    removeLink.innerHTML = "remove";
                    removeLink.href = "#";
                    removeLink.onclick = function() {
                        $("#PartnerTwo").children().remove(".person");
                        $(this).remove();
                        document.getElementById("PartnerTwo").innerHTML = '';
                    }

                    //$(this).append($(ui.draggable).clone().children("p").addClass("blocker"));
                    $(this).append(removeLink);
                    var currentElement = document.createElement("input");
                    currentElement.setAttribute("type", "hidden");
                    currentElement.setAttribute("name", "RecordRight");
                    currentElement.setAttribute("id", "RecordRight");
                    currentElement.setAttribute("value", lid);

                    $(this).append(currentElement);
                }
            });
        });
    </script>

    <% using (Html.BeginForm("Link","Record"))
       { %>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td width="50%">
                Partner 1:
                <br />
                <div id="PartnerOne">
                    <p>
                    </p>
                </div>
            </td>
            <td width="50%">
                Partner 2:
                <br />
                <div id="PartnerTwo">
                    <p>
                    </p>
                </div>
            </td>
        </tr>
    </table>
    <div style="text-align: left; width: 300px;">
        <input type="submit" title="Link Partners" value="Link Partners" />
    </div>
    <%} %>
    <br />
    <% using (Html.BeginForm())
       { %>
    <table width="100%">
        <tr>
            <td width="220px" valign="top">
                <table cellpadding="0" cellspacing="0" width="100%" style="border: 1px solid #304f9f;">
                    <tr>
                        <td style="background-color: #4b6bbe; color: White;">
                            Partner 2 Search:
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <label for="FirstName" style="width: 100px;">
                                            First Name:</label>
                                    </td>
                                    <td>
                                        <%= Html.TextBox("FirstName")%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="LastName" style="width: 100px;">
                                            Last Name:</label>
                                    </td>
                                    <td>
                                        <%= Html.TextBox("LastName")%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="CustomerID" style="width: 100px;">
                                            Customer ID:</label>
                                    </td>
                                    <td>
                                        <%= Html.TextBox("CustomerID")%>
                                    </td>
                                </tr>
                            </table>
                            <% foreach (LMS.Data.Record record in (IEnumerable<LMS.Data.Record>)ViewData["LeftList"])
                               { %>
                            <div class="person">
                                <table>
                                    <tr>
                                        <td width="200px">
                                            <%= Html.Encode(record.First_Name)%>
                                            ( <%= Html.Encode(record.Customer_ID)%> )
                                            <%= Html.Hidden("Left_Record_ID", record.Record_ID)%>
                                        </td>
                                        <td width="100px">
                                            <%= Html.Encode(record.Last_Name)%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="300px" colspan="2">
                                            <%= Html.Encode(record.Address_1)%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="300px" colspan="2">
                                            <%= Html.Encode(record.City + ", " + record.State.Abbreviation + " " + record.Zipcode)%>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <br />
                            <%} %>
                        </td>
                    </tr>
                </table>
            </td>
            <td width="220px" valign="top">
                <table cellpadding="0" cellspacing="0" width="100%" style="border: 1px solid #304f9f;">
                    <tr>
                        <td style="background-color: #4b6bbe; color: White;">
                            Partner 2 Search:
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <label for="DestFirstName" style="width: 100px;">
                                            First Name:</label>
                                    </td>
                                    <td>
                                        <%= Html.TextBox("DestFirstName")%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="DestLastName" style="width: 100px;">
                                            Last Name:</label>
                                    </td>
                                    <td>
                                        <%= Html.TextBox("DestLastName")%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="DestCustomerID" style="width: 100px;">
                                            Customer ID:</label>
                                    </td>
                                    <td>
                                        <%= Html.TextBox("DestCustomerID")%><input type="submit" value="Filter" />
                                    </td>
                                </tr>
                            </table>
                            <% foreach (LMS.Data.Record record in (IEnumerable<LMS.Data.Record>)ViewData["RightList"])
                               { %>
                            <div class="personright">
                                <table>
                                    <tr>
                                        <td width="200px">
                                            <%= Html.Encode(record.First_Name)%>
                                            ( <%= Html.Encode(record.Customer_ID)%> )
                                            <%= Html.Hidden("Right_Record_ID", record.Record_ID)%>
                                        </td>
                                        <td width="100px">
                                            <%= Html.Encode(record.Last_Name)%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="300px" colspan="2">
                                            <%= Html.Encode(record.Address_1)%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="300px" colspan="2">
                                            <%= Html.Encode(record.City + ", " + record.State.Abbreviation + " " + record.Zipcode)%>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <br />
                            <%} %>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <% } %>
    <br />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
    <style type="text/css">
        .draggable
        {
            width: 100px;
            height: 100px;
            padding: 0.5em;
            float: left;
            margin: 10px 10px 10px 0;
        }
        #droppable
        {
            width: 150px;
            height: 150px;
            padding: 0.5em;
            float: left;
            margin: 10px;
        }
        #PartnerOne
        {
            color: White;
            padding: 2px;
            text-align: left;
            width: 400px;
            height: 80px;
            background-color: #5488e9;
            border: solid 1px Black;
        }
        #PartnerOne a
        {
            color: Red;
        }
        #PartnerTwo
        {
            color: White;
            padding: 2px;
            text-align: left;
            width: 400px;
            height: 80px;
            background-color: #5488e9;
            border: solid 1px Black;
        }
        #PartnerTwo a
        {
            color: Red;
        }
        .hover
        {
           background-color: Green;
        }
    </style>
</asp:Content>
