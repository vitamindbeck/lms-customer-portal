<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">

    <style type="text/css">
        .style1
        {
            width: 724px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   <script type="text/javascript">
       $(function() {

           $('#frmSales').load()
           {
               $("#divPods").hide();

           };

           $("#chkPodWise").click(function() {
               if ($("#chkPodWise").is(":checked")) {

                   $("#divPods").show();
                   $("#divUsers").hide();


               }
               else {

                   $("#divUsers").show();
                   $("#divPods").hide();
               }

           });

       });
</script>
    <form id="frmSales" method="post" action="/Record/Sales">
    
    <table width="100%" border="0">
     <tr>
           <td align ="right" class="style1">
           View Information Pod wise:<%=Html.CheckBox("chkPodWise",false) %>
        </td>
       <td align ="right">
            <div id="divUsers" >
             Select a user's view: <%= Html.DropDownList("User_ID", (SelectList)ViewData["Pod_ID"], new { style = "font-size: 12px;" })%>
            </div>
             <div id="divPods" >
             Select a Pod: <%= Html.DropDownList("Pd_ID", (SelectList)ViewData["Pd_ID"], new { style = "font-size: 12px;" })%>
            </div>
        </td>
     </tr>
     </table>
 

    <% Html.RenderPartial("NewLeadList", ViewData["NewLeads"]); %>
    <br />
    <% Html.RenderPartial("AppointmentFollowUpList", ViewData["AppointFollowUps"]); %>
    <br />
    <% Html.RenderPartial("AppSentOpenActivities", ViewData["AppSentOpenActivities"]); %>
    <br />
    <% Html.RenderPartial("FollowUpList", ViewData["FollowUps"]); %>
    
    

    <script type="text/javascript">
        $("#User_ID").change(function() {
            $("#frmSales").submit();
        });

        $("#Pd_ID").change(function() {
            $("#frmSales").submit();
        });

       
    </script>
</form>
</asp:Content>
