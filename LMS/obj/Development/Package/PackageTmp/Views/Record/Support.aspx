<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Import Namespace="LMS.Helpers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Sales Support Home Page
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        $(function () {
            $(".sortable tr:last td").css('border', 'none');
        });
    </script>
    <%  var _userRepository = new LMS.Data.Repository.UserRepository();
        LMS.Data.User user = _userRepository.GetUserFromADName(User.Identity.Name.Replace(@"LONGEVITY\", ""));
    %>

    <% Html.RenderPartial("SupportLeads", ViewData["Fulfillment"]); %>
    <br />
    <% Html.RenderPartial("FulfillmentActivityList", ViewData["SupportActivities"]); %>
    <%if (LMS.Data.Repository.UserRepository.HasSecurityLevel(user, 13))
      {%>
    <br />
    <% Html.RenderPartial("ReconView", ViewData["ReconLeadData"]); %>
    <%}%>
</asp:Content>
