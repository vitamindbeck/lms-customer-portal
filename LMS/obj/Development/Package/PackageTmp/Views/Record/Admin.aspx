<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Admin - LMS
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <% Html.RenderPartial("MyUnassignedLeadsList",ViewData["MyUnassignedLeadsList"]); %>
    <% Html.RenderPartial("OtherUnassignedLeadsList",ViewData["OtherUnassignedLeadsList"]); %>

</asp:Content>

