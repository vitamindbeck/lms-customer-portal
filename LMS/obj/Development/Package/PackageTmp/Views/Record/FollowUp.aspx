﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<script type="text/javascript">
    $(document).ready(function () {
        $("#sortable1").tablesorter();
        $("#sortable2").tablesorter();
    }
    ); 
</script>
 <% Html.RenderPartial("InitialApplicationSentFollowUp", ViewData["InitialApplicationSentFollowUp"]); %>
 <br />
 <% Html.RenderPartial("ApplicationSentFollowUpAfterInitial", ViewData["ApplicationSentFollowUpAfterInitial"]); %>
 <br />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptContent" runat="server">
</asp:Content>
