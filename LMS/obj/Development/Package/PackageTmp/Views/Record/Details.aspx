<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<LMS.Data.Record>" %>

<%@ Import Namespace="LMS.Helpers" %>
<%@ Import Namespace="LMS.Data" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%= Html.Encode(Model.FullName) %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<script src = "http://labelwriter.com/software/dls/sdk/js/DYMO.Label.Framework.latest.js" type="text/javascript" charset="UTF-8"></script>
    <script type="text/javascript">
	    $(function () {
           $('#copyNoteModal').click(function () {
                var $NoteTextField = $('#Follow_Up_Notes'),
                    $NoteList = $('#FollowRecordNotes :selected');
                
                console.log($NoteTextField.val());
                console.log($NoteList.val());


                if ($NoteTextField.val() == "")
                    $NoteTextField.val($NoteList.text());
                else
                    $NoteTextField.val($NoteTextField.val() + ', ' + $NoteList.text());
            });

            $("#Image1").click(function(e){
                e.preventDefault();
            
                if ($("#Follow_Up_Date").val() == '') {
                    alert('You must select a follow up date');
                    return false;
                }

                if ($("#Follow_User_ID").val() == 49 && !$("#SendApp2").is(":checked") && !$("#SendApp").is(":checked") && !$("#PreFill").is(":checked")) {
                    alert('You must select a follow up user');
                    return false;
                }
                if ($("#Follow_Activity_Type_ID").val() < 1) {
                    alert('You must select a follow up activity');
                    return false;
                }
                if ($("#Modal_Follow_Campaign_Code").val() == 0) {
                    alert('Please select a follow up campaign code!');
                    return false;
                }
                
                $('#FollowModalContent form').submit();

            });




	        $("#datepicker").datepicker();
	        $(".section-title:gt(0)").click(function () {
	            $(this).next(".section-inner").slideToggle(300)
	            return false;
	        });
	        if ('<%= Model.B2E %>' == 'True') {
	            $("#AdditionalDiv").hide();
	            $("#CompanyDiv").show();
	        }
	        else {
	            $("#AdditionalDiv").show();
	            $("#CompanyDiv").hide();
	        }
	        $("#GroupID").val('<%= Model.Source_Code.Group_ID %>');
	        $("#tabs").tabs({ history: true });
	        $("#ptabs").tabs();

	        $('#Follow_Up_Date').mask("99/99/9999");

	        $('#AddPlan').click(function (e) {
	            e.preventDefault();
	            // example of calling the confirm function
	            // you must use a callback function to perform the "yes" action
	            confirmPlan("Continue", function () {
	                alert("OK");
	            });
	        });

	        $('#AddFollowUp').click(function (e) {
	            e.preventDefault();
	            // example of calling the confirm function
	            // you must use a callback function to perform the "yes" action
	            confirmFollow("Continue", function () {
	                alert("OK");
	            });
	        });

	        $('#PrintAddressLabel').click(function (e) {
	            e.preventDefault();
	            confirmAddress("Continue", function () {
	                alert("OK");
	            });
	        });

	        $('#PrintNameLabel').click(function (e) {
	            e.preventDefault();
                enumDymoPrinters();
	            confirmName("Continue", function () {
	                alert("OK");
	            });
	        });

	        $('#BtnPrintAdd').click(function (e)
            {
                 var dieCutLabelLayout = '<?xml version="1.0" encoding="utf-8"?>\
                    <DieCutLabel Version="8.0" Units="twips">\
                        <PaperOrientation>Landscape</PaperOrientation>\
                        <Id>Address</Id>\
                        <PaperName>30252 Address</PaperName>\
                        <DrawCommands/>\
                        <ObjectInfo>\
                            <TextObject>\
                                <Name>Text</Name>\
                                <ForeColor Alpha="255" Red="0" Green="0" Blue="0" />\
                                <BackColor Alpha="0" Red="255" Green="255" Blue="255" />\
                                <LinkedObjectName></LinkedObjectName>\
                                <Rotation>Rotation0</Rotation>\
                                <IsMirrored>False</IsMirrored>\
                                <IsVariable>True</IsVariable>\
                                <HorizontalAlignment>Left</HorizontalAlignment>\
                                <VerticalAlignment>Middle</VerticalAlignment>\
                                <TextFitMode>AlwaysFit</TextFitMode>\
                                <UseFullFontHeight>True</UseFullFontHeight>\
                                <Verticalized>False</Verticalized>\
                                <StyledText/>\
                            </TextObject>\
                            <Bounds X="332" Y="150" Width="4455" Height="1260" />\
                        </ObjectInfo>\
                    </DieCutLabel>';

                var labelXML = dieCutLabelLayout;
                var labelSetBuilder = new dymo.label.framework.LabelSetBuilder();
                var record = labelSetBuilder.addRecord();
                var address = '';

                if (('<%= Model.IsMailing_Address %>' == 'True') || ('<%= Model.IsMailing_Address %>' == 'null')) 
	               {
	                    address = '<% Response.Write(System.Web.HttpUtility.JavaScriptStringEncode(Model.First_Name.ToUpper() + " " + Model.Last_Name.ToUpper()));%>' + '\r\n' + '<% Response.Write(System.Web.HttpUtility.JavaScriptStringEncode(Model.Address_1)); %>' + '\r\n' + '<%=Model.Address_2 %>' + '\r\n' + '<%=Model.City + "," + Model.State.Abbreviation + " " + Model.Zipcode %>' + '\r\n';                       
	               }
	                                  
	            else
	              {
	                    address = '<% Response.Write(System.Web.HttpUtility.JavaScriptStringEncode(Model.First_Name.ToUpper() + " " + Model.Last_Name.ToUpper()));%>' + '\r\n' + '<% Response.Write(System.Web.HttpUtility.JavaScriptStringEncode(Model.Mailing_Address_1)); %>' + '\r\n' + '<%=Model.Mailing_Address_2 %>' + '\r\n' + '<%=Model.Mailing_City + "," + Model.Mailing_State.Abbreviation +" " + Model.Mailing_ZipCode %>' + '\r\n';                       
	              }


                
                //Get Printer
                var printers = dymo.label.framework.getPrinters();
                if (printers.length == 0)
                    throw "No DYMO printers are installed. Install DYMO printers.";

                var printerName = "";
                for (var i = 0; i < printers.length; ++i)
                {
                    var printer = printers[i];
                    if (printer.printerType == "LabelWriterPrinter")
                    {
                        printerName = printer.name;
                        break;
                    }
                }

               var params = dymo.label.framework.createLabelWriterPrintParamsXml({twinTurboRoll: dymo.label.framework.TwinTurboRoll.Right});

                record.setTextMarkup("Text", address);
                dymo.label.framework.printLabel(printerName, params, labelXML, labelSetBuilder);

            });

             $('#BtnPrintName').click(function (e)
            {
                 var dieCutLabelLayout = '<?xml version="1.0" encoding="utf-8"?>\
                    <DieCutLabel Version="8.0" Units="twips">\
                        <PaperOrientation>Landscape</PaperOrientation>\
                        <Id>Address</Id>\
                        <PaperName>30252 Address</PaperName>\
                        <DrawCommands/>\
                        <ObjectInfo>\
                            <TextObject>\
                                <Name>Text</Name>\
                                <ForeColor Alpha="255" Red="0" Green="0" Blue="0" />\
                                <BackColor Alpha="0" Red="255" Green="255" Blue="255" />\
                                <LinkedObjectName></LinkedObjectName>\
                                <Rotation>Rotation0</Rotation>\
                                <IsMirrored>False</IsMirrored>\
                                <IsVariable>True</IsVariable>\
                                <HorizontalAlignment>Left</HorizontalAlignment>\
                                <VerticalAlignment>Middle</VerticalAlignment>\
                                <TextFitMode>AlwaysFit</TextFitMode>\
                                <UseFullFontHeight>True</UseFullFontHeight>\
                                <Verticalized>False</Verticalized>\
                                <StyledText/>\
                            </TextObject>\
                            <Bounds X="332" Y="150" Width="4455" Height="1260" />\
                        </ObjectInfo>\
                    </DieCutLabel>';

                var labelXML = dieCutLabelLayout;
                var labelSetBuilder = new dymo.label.framework.LabelSetBuilder();
                var record = labelSetBuilder.addRecord();
               
                <% 
	                String strYr;
	                if(Model.RequestedStartDate != null)
	                {
	                strYr = Model.RequestedStartDate.Value.Year.ToString().Substring(2);
	                }
	                else if(DateTime.Today.DayOfYear > 335)
	                {
	                    strYr = (DateTime.Today.Year + 1).ToString().Substring(2);
	                } 
	                else
	                {
	                    strYr = DateTime.Today.Year.ToString().Substring(2);
                }%>

	            						
                var name = '<% Response.Write(System.Web.HttpUtility.JavaScriptStringEncode(Model.Last_Name.ToUpper() + ", " + Model.First_Name.ToUpper() + " - " + strYr)); %>';
                                
                //Get Printer
                var printers = dymo.label.framework.getPrinters();
                if (printers.length == 0)
                    throw "No DYMO printers are installed. Install DYMO printers.";

                var printerName = "";
                for (var i = 0; i < printers.length; ++i)
                {
                    var printer = printers[i];
                    if (printer.printerType == "LabelWriterPrinter")
                    {
                        printerName = printer.name;
                        break;
                    }
                }

                var params = dymo.label.framework.createLabelWriterPrintParamsXml({twinTurboRoll: dymo.label.framework.TwinTurboRoll.Left});

                record.setTextMarkup("Text", name);
                dymo.label.framework.printLabel(printerName, params, labelXML, labelSetBuilder);

            });	                   
	    });


		function confirmPlan(message, callback) {
			$('#PlanModalContent').modal({
				overlayCss: {
					backgroundColor: '#000',
					cursor: 'wait'
				}, containerCss: {
					height: 40,
					width: 230,
					backgroundColor: '#fff',
					border: '3px solid #ccc'
				}
			});
		}

		function confirmFollow(message, callback) {
			$('#FollowModalContent').modal({
				overlayCss: {
					backgroundColor: '#000',
					cursor: 'wait'
				}, containerCss: {
					height: 340,
					width: 800,
					backgroundColor: '#fff',
					border: '3px solid #ccc'
				}
			});
		}

		function confirmAddress(message, callback) {
			$('#PrintAddressMondalContent').modal({
				overlayCss: {
					backgroundColor: '#000',
					cursor: 'wait'
				}, containerCss: {
					height: 80,
					width: 440,
					backgroundColor: '#fff',
					border: '3px solid #ccc'
				}
			});
		}

		function confirmName(message, callback) {
			$('#PrintNameMondalContent').modal({
				overlayCss: {
					backgroundColor: '#000',
					cursor: 'wait'
				}, containerCss: {
					height: 80,
					width: 440,
					backgroundColor: '#fff',
					border: '3px solid #ccc'
				}
			});
		}

		function deletePlan(planID) {
			// Perform delete   
			var action = "/Transaction/Delete/" + planID;

			var request = new Sys.Net.WebRequest();
			request.set_httpVerb("POST");
			request.set_url(action);
			request.add_completed(deleteCompleted);
			request.invoke();
		}

		function deleteCompleted() {
			// Reload page   
			window.location.reload();
		}

		function enumDymoPrinters() {
			//try {
				var DymoAddIn = new ActiveXObject('DYMO.DymoAddIn');
				var printersStr = DymoAddIn.GetDymoPrinters();

				var printers = new Array();

				var printersTotal = printersStr2Array(printersStr, printers);

				// fill out the html select element with the list of available
				// DYMO printers

				var plist = document.getElementById("pList");
				var plist2 = document.getElementById("pList2");
				for (var n = 0; n < printersTotal; n++) {
					newDropDownItem(plist, printers[n], n.toString(), n, n == 0 ? true : false);
					newDropDownItem(plist2, printers[n], n.toString(), n, n == 0 ? true : false);
				}

			//}
//            catch (err) {
//                alert("Error: " + err.description);
//            }
		}

		function newDropDownItem(ctrl, itemText, itemValue, pos, selected) {
			var optn = ctrl.document.createElement("OPTION");

			optn.text = itemText;
			optn.value = itemValue;
			optn.selected = selected;

			ctrl.options.add(optn, pos);
		}

		function printersStr2Array(printersStr, printersArr) {
			if (printersStr.length > 0) {
				var pos, start = 0;

				while (true) {
					pos = printersStr.indexOf('|', start);
					if (pos != -1) {
						printersArr.push(printersStr.slice(start, pos));
						start = pos + 1;
					}
					else {
						printersArr.push(printersStr.slice(start));
						break;
					}
				}

			}
			return printersArr.length;
		}


    </script>
    <div id="FollowModalContent" style="display: none; padding: 3px;">
        <% using (Html.BeginForm("CreateFollowUp", "Record", new { id = Model.Record_ID }))
           {%>
        <div id="ClientFactsDiv" class="client-outer" style="background-color: #a7b5dc;">
            <div class="section-title">
                Follow Up Activity Details:
            </div>
            <div class="section-inner">
                <table class="section" cellpadding="0" cellspacing="0" style="margin: 10px;" width="100%">
                    <tr>
                        <td class="label" width="150px">
                            Follow Up Opportunity:
                        </td>
                        <td>
                            <%= Html.DropDownList("Plan_Transaction_ID")%>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            Follow Up Date:
                        </td>
                        <td>
                            <%= Html.TextBox("Follow_Up_Date","", new { style = "width: 80px" })%>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            Follow Up Time:
                        </td>
                        <td>
                            <%= Html.DropDownList("Follow_Up_Time")%>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            Follow Up Activity:
                        </td>
                        <td>
                            <%= Html.DropDownList("Follow_Activity_Type_ID")%>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            Follow Set For:
                        </td>
                        <td>
                            <%= Html.DropDownList("Follow_User_ID")%>
                            <span id="hasActivity"></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            Campaign Code:
                        </td>
                        <td>
                            <%= Html.DropDownList("Modal_Follow_Campaign_Code", (SelectList)ViewData["CampaignDDL"])%>
                        </td>
                        <%--<td class="label">
							Follow Up Priority:
						</td>
						<td>
							<%= Html.DropDownList("Follow_Priority_Level")%>
						</td>--%>
                    </tr>
                    <tr>
                        <td class="label">
                        </td>
                        <td>
                            <%= Html.DropDownList("FollowRecordNotes")%>
                            <img src="../../Content/images/page_white_paste.png" alt="Paste" style="border: none;
                                vertical-align: middle;" id="copyNoteModal" />
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            Follow Up Notes:
                        </td>
                        <td>
                            <%= Html.TextArea("Follow_Up_Notes", new { style = "width: 300px;" })%>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <br />
        <div style="text-align: center;">
            <input type="image" src="../../Content/icons/check.png" title="Create New Follow Up"
                value="Create" id="Image1" style="vertical-align: middle;" />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <img title="Cancel" src="../../Content/icons/close.png" id="Img1" onclick="$.modal.close()"
                style="vertical-align: middle; margin-bottom: 3px;" alt="Cancel" />
        </div>
        <%} %>
    </div>
    <div id="PlanModalContent" style="display: none;">
        <% using (Html.BeginForm("CreateNewPlan", "Record", new { id = Model.Record_ID }))
           { %>
        <table>
            <tr>
                <td class="label">
                    Plan Type:
                </td>
                <td>
                    <%= Html.DropDownList("GroupID", (SelectList)ViewData["GroupID"], new { style = "font-size: 12px;" })%>&nbsp;
                    <input type="image" src="../../Content/icons/add.png" title="Create New Plan" value="Create"
                        id="ButtonYes" style="vertical-align: middle;" />
                    <img title="Cancel" src="../../Content/icons/close.png" id="ButtonNo" alt="Cancel"
                        onclick="$.modal.close()" style="vertical-align: middle;" />
                </td>
            </tr>
        </table>
        <%} %>
    </div>
    <div id="PrintAddressMondalContent" style="display: none;">
        <table>
            <tr>
                <td colspan="2" align="center">
                    <strong>Print Address</strong>
                </td>
            </tr>
            <tr>
                <td class="label">
                    Select Printer:
                </td>
                <td>
                    <select id="pList" style="width: 200px">
                    </select>
                    &nbsp;
                    <input type="image" src="../../Content/icons/printer.png" title="Print" value="Print"
                        id="BtnPrint" style="vertical-align: middle;" />
                    <img title="Cancel" src="../../Content/icons/close.png" id="Img2" alt="Cancel" onclick="$.modal.close()"
                        style="vertical-align: middle;" />
                </td>
            </tr>
        </table>
    </div>
    <div id="PrintNameMondalContent" style="display: none;">
        <table>
            <tr>
                <td colspan="2" align="center">
                    <strong>Print Name</strong>
                </td>
            </tr>
            <tr>
                <td class="label">
                    Select Printer:
                </td>
                <td>
                    <select id="pList2" style="width: 200px">
                    </select>
                    &nbsp;
                    <input type="image" src="../../Content/icons/printer.png" title="Print" value="Print"
                        id="BtnPrint2" style="vertical-align: middle;" />
                    <img title="Cancel" src="../../Content/icons/close.png" id="Img3" alt="Cancel" onclick="$.modal.close()"
                        style="vertical-align: middle;" />
                </td>
            </tr>
        </table>
    </div>
    <div style="text-align: left;">
        <% if (Model.Partner_ID > 0)
           { %>
        <a href="../../Record/Details/<%= Model.Partner_ID %>" style="text-decoration: none;">
            <img src="../../content/icons/user.png" title="Partner" alt="Partner" style="border: 0px;
                vertical-align: middle;" />
        </a>&nbsp;&nbsp;&nbsp;&nbsp;
        <%} %>
        <a href="../../Record/Edit/<%= Model.Record_ID %>" style="text-decoration: none;">
            <img src="../../content/icons/document_edit.png" title="Edit" alt="Edit" style="border: 0px;
                vertical-align: middle;" />
        </a>&nbsp;&nbsp;&nbsp;&nbsp;
        <img src="../../content/icons/folder_add.png" id="AddPlan" title="New Plan Transaction"
            alt="Create Activity" style="border: 0px; vertical-align: middle;" />
        &nbsp;&nbsp;&nbsp;&nbsp; <a href="../../Record/CreatePartner/<%= Model.Record_ID %>"
            style="text-decoration: none;">
            <img src="../../content/icons/user_add.png" id="AddPartner" title="New Partner" alt="Create Activity"
                style="border: 0px; vertical-align: middle;" /></a> &nbsp;&nbsp;&nbsp;&nbsp;
        <img src="../../content/icons/clock.png" id="AddFollowUp" title="Create Follow Up"
            alt="Create Activity" style="border: 0px; vertical-align: middle;" />
        &nbsp;&nbsp;&nbsp;&nbsp;
        <img src="../../Content/icons/printer-green-icon.png" id="BtnPrintAdd" title="Print Address Label"
            alt="Print Address Label" style="border: 0px; vertical-align: middle;" />
        &nbsp;&nbsp;&nbsp;&nbsp;
        <img src="../../Content/icons/printer-orange-icon.png" id="BtnPrintName" title="Print Name Label"
            alt="Print Address Label" style="border: 0px; vertical-align: middle;" />
    </div>
    <br />
    <div id="tabs" style="font-size: 1em; font-family: Calibri;">
        <ul>
            <li><a href="#tabs-1">
                <%= Html.Encode(Model.FullName) %>
                -
                <%= Html.Encode(Model.Customer_ID) %>
                <% if (Model.Partner != null)
                   { %>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Partner ID:
                <%= Html.Encode(Model.Partner.Customer_ID) %>
                <%} %></a></li>
            <li><a href="#tabs-3">
                <%if (Model.B2E)
                  { %>
                Company Details
                <%}
                  else
                  { %>
                Additional Information
                <%} %></a></li>
            <li><a href="#tabs-5">Activities</a></li>
            <li><a href="#tabs-6">Plans</a></li>
            <li><a href="#tabs-7">Address List</a></li>
        </ul>
        <div id="tabs-1" style="background-color: #e0e4ee;">
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td class="label">
                        Name:
                    </td>
                    <td width="180px">
                        <%= Html.Encode(Model.FullName) %>
                    </td>
                    <td class="label">
                        Home Phone:
                    </td>
                    <td width="180px">
                        <%= Html.Encode(Model.Home_Phone) %>
                    </td>
                    <td class="label">
                        Source:
                    </td>
                    <td>
                        <%= Html.Encode(Model.Source_Code.DropDownName) %>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Address:
                    </td>
                    <td>
                        <%= Html.Encode(Model.Address_1) %>
                    </td>
                    <td class="label">
                        Work Phone:
                    </td>
                    <td>
                        <%= Html.Encode(Model.Work_Phone) %>
                        -
                        <%= Html.Encode(Model.Work_Phone_Ext) %>
                    </td>
                    <td class="label">
                        Record Type:
                    </td>
                    <td>
                        <%= Html.Encode(Model.Record_Type.Description) %>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                    </td>
                    <td>
                        <%= Html.Encode(Model.Address_2) %>
                    </td>
                    <td class="label">
                        Cell Phone:
                    </td>
                    <td>
                        <%= Html.Encode(Model.Cell_Phone) %>
                    </td>
                    <td class="label">
                        Current Agent:
                    </td>
                    <td>
                        <%= Html.Encode(Model.Agent.FullName) %>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                    </td>
                    <td>
                        <%= Html.Encode(Model.City + ", " + Model.State.Abbreviation + " " + Model.Zipcode ) %>
                    </td>
                    <td class="label">
                        Fax:
                    </td>
                    <td>
                        <%= Html.Encode(Model.Fax) %>
                    </td>
                    <td class="label">
                        Original Agent:
                    </td>
                    <td>
                        <%= Html.Encode(Model.LastAgent()) %>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        County:
                    </td>
                    <td>
                        <%= Html.Encode(Model.County) %>
                    </td>
                    <td class="label">
                        Email Address:
                    </td>
                    <td>
                        <%= Html.Encode(Model.Email_Address) %>
                    </td>
                    <td class="label">
                        Sales Coordinator:
                    </td>
                    <td>
                        <%= Html.Encode(Model.Assistant_Agent.Last_Name + ", " + Model.Assistant_Agent.First_Name) %>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Contact Name:
                    </td>
                    <td>
                        <%= Html.Encode(Model.Alt_First_Name + " " + Model.Alt_Last_Name) %>
                    </td>
                    <td class="label">
                        Alt Email Address:
                    </td>
                    <td>
                        <%= Html.Encode(Model.Alt_Email_Address) %>
                    </td>
                    <td class="label">
                        Referred By:
                    </td>
                    <td>
                        <%= Html.Encode(Model.Reffered_By) %>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Alternative Address:
                    </td>
                    <td>
                        <%= Html.Encode(Model.Alt_Address_1) %>
                    </td>
                    <td class="label">
                        Alt Phone:
                    </td>
                    <td>
                        <%= Html.Encode(Model.Alt_Phone_Number) %>
                    </td>
                    <td class="label">
                        Referral Type:
                    </td>
                    <td>
                        <%= Html.Encode(Model.Referral_Type.Referral_Description) %>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                    </td>
                    <td>
                        <%= Html.Encode(Model.Alt_Address_2) %>
                    </td>
                    <td class="label">
                        Email 2:
                    </td>
                    <td>
                        <%= Html.Encode(Model.Email_Address_2) %>
                    </td>
                    <td class="label">
                        Broker:
                    </td>
                    <td>
                        <% if (Model.Broker != null)
                           { %>
                        <%= Html.Encode(Model.Broker.DropDownName)%>
                        <%} %>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        City, State, Zip:
                    </td>
                    <td>
                        <%= Html.Encode(Model.Alt_City) %>
                        ,
                        <%= Html.Encode(Model.Alt_State.Abbreviation) %>
                        &nbsp;
                        <%= Html.Encode(Model.Alt_Zipcode) %>
                    </td>
                    <td class="label">
                        Momentum:
                    </td>
                    <td>
                        <%= (Model.Momentum.Value ? "Yes" : "No") %>
                    </td>
                    <td class="label">
                        Products Of Interest:
                    </td>
                    <td>
                        <%= Html.Encode(Model.Products_Of_Interest_ID > 0 && Model.Products_Of_Interest_ID != null ? Model.Products_Of_Interest.Products_Of_Interest_Desc : "") %>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Alt Add From:
                    </td>
                    <td>
                        <%= Html.Encode((Model.Alt_Address_From_Date.HasValue ? Model.Alt_Address_From_Date.Value.ToString("MM/dd/yyyy") : "")) %>
                        &nbsp; <span style="font-weight: bold;">To:</span> &nbsp;
                        <%= Html.Encode((Model.Alt_Address_To_Date.HasValue ? Model.Alt_Address_To_Date.Value.ToString("MM/dd/yyyy") : ""))%>
                    </td>
                    <td class="label">
                        <%= (Model.Do_Not_Contact ? "Do Not Call:" : "") %>
                    </td>
                    <td>
                        <%= (Model.Do_Not_Contact ? "Yes" : "") %>
                    </td>
                    <td class="label">
                        Current Situation:
                    </td>
                    <td>
                        <%= Html.Encode(Model.Record_Current_Situation_ID > 0 && Model.Record_Current_Situation_ID != null ? Model.Record_Current_Situation.Record_Current_Situation_Desc : "") %>
                    </td>
                </tr>
                <tr>
                    <td class="label" style="height: 18px">
                        Birth Date:
                    </td>
                    <td>
                        <%= Html.Encode(Model.DOB.HasValue==true ? Model.DOB.Value.ToString("MM/dd/yyyy") : "")%>
                        &nbsp;&nbsp; <strong>Age:</strong>
                        <%= Html.Encode(Model.Age) %>
                    </td>
                    <td class="label" style="height: 18px">
                        <%= ((bool)Model.Opt_Out_Email ? "Opt-Out Email:" : "") %>
                    </td>
                    <td>
                        <%= ((bool)Model.Opt_Out_Email ? "Yes" : "")%>
                    </td>
                    <td class="label" style="height: 18px">
                        <%= (Model.Opt_Out_Newsletter ? "Opt-Out Newsletter:" : "") %>
                    </td>
                    <td>
                        <%= (Model.Opt_Out_Newsletter ? "Yes" : "")%>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Submit Date:
                    </td>
                    <td>
                        <%= Html.Encode((Model.Created_On.HasValue ? string.Format("{0:g}",Model.Created_On.Value) : ""))%>
                    </td>
                    <td class="label">
                        B2E:
                    </td>
                    <td>
                        <%= (Model.B2E ? "Yes" : "No") %>
                    </td>
                    <td class="label" style="height: 18px">
                        Requested Start Date:
                    </td>
                    <td class="style1">
                        <%= Html.Encode((Model.RequestedStartDate.HasValue ? Model.RequestedStartDate.Value.ToString("MM/dd/yyyy") : ""))%>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Distribution List:
                    </td>
                    <td colspan="5">
                        <%foreach (sp_Get_Record_Distribution_ListResult rs in (List<sp_Get_Record_Distribution_ListResult>)ViewData["DistributionList"])
                          {
                              Response.Write(rs.Record_Distribution_Type_Description + "&nbsp;&nbsp;&nbsp;");
                          } %>
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <table width="100%" style="background-color: White; border: solid 1px #304f9f;">
                            <tr>
                                <td class="label" style="width: 80px;">
                                    Eligibility Date:
                                </td>
                                <td>
                                    <%= Html.Encode((Model.Eligibility_Date.HasValue ? Model.Eligibility_Date.Value.ToString("MM/dd/yyyy") : ""))%>
                                </td>
                            </tr>
                            <tr>
                                <td class="label" style="width: 80px;">
                                    Eligibility:
                                </td>
                                <td>
                                    <%= Html.Encode((Model.Record_Eligibility == null ? "" : Model.Record_Eligibility.Category + "-" + Model.Record_Eligibility.Description)) %>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <div id="tabs-3" style="background-color: #e0e4ee;">
            <%if (Model.B2E)
              { %>
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td class="label">
                        Name:
                    </td>
                    <td width="250px">
                        <%= Html.Encode(Model.Company.Name)%>
                    </td>
                    <td class="label">
                        Company President:
                    </td>
                    <td width="250px">
                        <%= Html.Encode(Model.Company.Company_President)%>
                    </td>
                    <td class="label">
                        Web Address:
                    </td>
                    <td>
                        <%= Html.Encode(Model.Company.Web_Address)%>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Address:
                    </td>
                    <td>
                        <%= Html.Encode(Model.Company.Address_1)%>
                    </td>
                    <td class="label">
                        Company Phone:
                    </td>
                    <td>
                        <%= Html.Encode(Model.Company.Company_Phone)%>
                    </td>
                    <td class="label">
                        Industry:
                    </td>
                    <td>
                        <%= Html.Encode(Model.Company.Industry.Description)%>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                    </td>
                    <td>
                        <%= Html.Encode(Model.Company.Address_2)%>
                    </td>
                    <td class="label">
                        Primary Contact:
                    </td>
                    <td>
                        <%= Html.Encode(Model.Company.Primary_Contact)%>
                    </td>
                    <td class="label">
                        Company Size:
                    </td>
                    <td>
                        <%= Html.Encode(Model.Company.Company_Size)%>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        City, State Zip:
                    </td>
                    <td>
                        <%= Html.Encode(Model.Company.City)%>
                        ,
                        <%= Html.Encode(Model.Company.State.Abbreviation)%>
                        <%= Html.Encode(Model.Company.Zipcode)%>
                    </td>
                    <td class="label">
                        Primary Phone:
                    </td>
                    <td>
                        <%= Html.Encode(Model.Company.Primary_Phone)%>
                    </td>
                    <td class="label">
                        Email Address:
                    </td>
                    <td>
                        <%= Html.Encode(Model.Company.Email_Address)%>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                    </td>
                    <td>
                    </td>
                    <td class="label">
                        Title:
                    </td>
                    <td>
                        <%= Html.Encode(Model.Company.Title)%>
                    </td>
                    <td class="label">
                        Primary Email:
                    </td>
                    <td>
                        <%= Html.Encode(Model.Company.Primary_Email)%>
                    </td>
                </tr>
            </table>
            <%}
              else
              {%>
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td class="label" style="width: 200px">
                        Name as appears on Medicare Card:
                    </td>
                    <td class="label">
                        First:
                    </td>
                    <td style="width: 200px">
                        <%= Html.Encode(Model.Medicare_FirstName) %>
                    </td>
                    <td class="label">
                        Initial:
                    </td>
                    <td style="width: 190px">
                        <%= Html.Encode(Model.Medicare_InitialName) %>
                    </td>
                    <td class="label">
                        Last:
                    </td>
                    <td style="width: 200px">
                        <%= Html.Encode(Model.Medicare_LastName) %>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td class="label">
                        SSN:
                    </td>
                    <td style="width: 200px">
                        <% if (LMS.Helpers.StringHelper.DecryptData(Model.SSN) != null)
                           { %>
                        <% if (LMS.Helpers.StringHelper.DecryptData(Model.SSN).Length == 9)
                           { %>
                        <%= Html.Encode(LMS.Helpers.StringHelper.DecryptData(Model.SSN).Insert(3, "-").Insert(6, "-"))%>
                        <%}
                           else
                           {%>
                        <%= Html.Encode(LMS.Helpers.StringHelper.DecryptData(Model.SSN))%>
                        <%} %><%} %>
                    </td>
                    <td class="label">
                        Gender:
                    </td>
                    <td>
                        <% if (Model.Sex != "M" && Model.Sex != "F")
                           { %>
                        Unknown
                        <%}
                           else
                           { %>
                        <%= Html.Encode(Model.Sex)%>
                        <%} %>
                    </td>
                    <td class="label">
                        Current Tobacco Use:
                    </td>
                    <td>
                        <%= Html.Encode(Model.Current_Tobacco_Use_Status_ID > 0 && Model.Current_Tobacco_Use_Status_ID != null ? Model.Current_Tobacco_Use_Status.Description  : "") %>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Medicare Number:
                    </td>
                    <td style="width: 200px">
                        <%= Html.Encode(LMS.Helpers.StringHelper.DecryptData(Model.Medicare_Number)) %>
                    </td>
                    <td class="label">
                        Marital Status:
                    </td>
                    <td style="width: 250px">
                        <%= Html.Encode(Model.Marital_Status_ID > 0 && Model.Marital_Status_ID != null ? Model.Marital_Status.Description  : "") %>
                    </td>
                    <td class="label">
                        Current Tobacco Type:
                    </td>
                    <td>
                        <%= Html.Encode(Model.Current_Tobacco_ID > 0 && Model.Current_Tobacco_ID != null ? Model.Current_Tobacco.Description : "")%>
                    </td>
                </tr>
                <tr>
                    <td class="label" style="width: 150px">
                        <% if (Model.Part_A_Effective == true)
                               Response.Write(Html.Encode("x"));
                           else
                               Response.Write(Html.Encode("")); %>
                        Part A Effective Date:
                    </td>
                    <td>
                        <%= Html.Encode((Model.Part_A_Effective_Date.HasValue == true ? Model.Part_A_Effective_Date.Value.ToString("MM/dd/yyyy") : ""))%>
                    </td>
                    <td class="label">
                        Height:
                    </td>
                    <td>
                        <%= Html.Encode(Model.Height/12) %>
                        &nbsp;ft &nbsp;
                        <%= Html.Encode(Model.Height%12) %>
                        in
                    </td>
                    <td class="label">
                        Past Tobacco Use:
                    </td>
                    <td>
                        <%= Html.Encode(Model.Past_Tobacco_Use_Status_Id > 0 && Model.Past_Tobacco_Use_Status_Id != null ? Model.Past_Tobacco_Use_Status.Description : "")%>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        <% if (Model.Part_B_Effective == true) Response.Write(Html.Encode("x")); else Response.Write(Html.Encode("")); %>
                        Part B Effective Date:
                    </td>
                    <td style="width: 200px">
                        <%= Html.Encode((Model.Part_B_Effective_Date.HasValue ? Model.Part_B_Effective_Date.Value.ToString("MM/dd/yyyy") : ""))%>
                    </td>
                    <td class="label">
                        Weight:
                    </td>
                    <td>
                        <%= Html.Encode(Model.Weight) %>
                    </td>
                    <td class="label">
                        Past Tobacco Type:
                    </td>
                    <td>
                        <%= Html.Encode(Model.Past_Tobacco_ID > 0 && Model.Past_Tobacco_ID != null ? Model.Past_Tobacco.Description : "")%>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Drug List ID:
                    </td>
                    <td style="width: 200px">
                        <%= Html.Encode(LMS.Helpers.StringHelper.DecryptData(Model.Drug_List_ID)) %>
                    </td>
                    <td class="label">
                        Subsidy:
                    </td>
                    <td style="width: 250px">
                        <%= Html.Encode( Model.Subsidy_ID > 0 && Model.Subsidy_ID != null ? Model.Subsidy.Subsidy_Desc : "") %>
                    </td>
                    <td class="label">
                        Date Quit:
                    </td>
                    <td>
                        <%= Html.Encode(Model.Quit_Date) %>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Annual Income:
                    </td>
                    <td style="width: 200px">
                        <%= Html.Encode( string.Format("{0:c}",Model.Annual_Income)) %>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Employment Status:
                    </td>
                    <td style="width: 200px">
                        <%= Html.Encode(Model.Employment_Status.Description) %>
                    </td>
                    <td class="label" style="width: 150px">
                        Currently Insured:
                    </td>
                    <td width="150px">
                        <%= (Model.Currently_Insured ? Html.Encode("Yes"):Html.Encode("No")) %>
                    </td>
                    <td class="label" style="width: 250px">
                        Current Plan Type:
                    </td>
                    <td style="width: 180px">
                        <%= Html.Encode(Model.Current_Insured_Type_ID > 0 && Model.Current_Insured_Type_ID != null ? Model.Current_Insured_Type.Current_Insured : "") %>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Occupation:
                    </td>
                    <td style="width: 200px">
                        <%= Html.Encode(Model.Occupation) %>
                    </td>
                    <td class="label">
                        Medical Carrier:
                    </td>
                    <td>
                        <%= Html.Encode(Model.Medical_Carrier) %>
                    </td>
                    <td class="label">
                        Medical Carrier Original Start Date:
                    </td>
                    <td>
                        <%= Html.Encode(string.Format("{0:d}",Model.Original_Start_Date)) %>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td class="label">
                        Drug Plan Co:
                    </td>
                    <td>
                        <%= Html.Encode(Model.Drug_Plan_Co)%>
                    </td>
                    <td class="label">
                        Drug Plan Co Original Start Date:
                    </td>
                    <td>
                        <%= Html.Encode(string.Format("{0:d}", Model.Drug_Plan_Original_Start_Date))%>
                    </td>
                </tr>
            </table>
            <%}%>
        </div>
        <div id="tabs-5" style="background-color: #e0e4ee;">
            <% Html.RenderPartial("Activities", ViewData["DisplayRecordActivitiesList"]); %>
            <br />
            <% if (Model.Partner != null)
               { %>
            <br />
            <% Html.RenderPartial("Activities", ViewData["DisplayPartnerRecordActivitiesList"]); %>
            <%} %>
        </div>
        <div id="tabs-6" style="background-color: #e0e4ee;">
            <table class="sortable" cellpadding="0" cellspacing="0" width="100%" style="background-color: White;
                border: solid 1px #304f9f;">
                <thead>
                    <tr>
                        <% if (LMS.Data.Repository.UserRepository.GetUser(User.Identity.Name).Security_Level_ID == 2)
                           { %>
                        <th style="width: 50px;">
                        </th>
                        <%} %>
                        <th style="width: 50px;">
                        </th>
                        <th style="width: 50px;">
                        </th>
                        <th width="200px">
                            Plan Name
                        </th>
                        <th width="100px">
                            Status
                        </th>
                        <th width="200px">
                            Product Type
                        </th>
                        <th width="*">
                            Date
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <% foreach (LMS.Data.Get_Plan_Transaction_Desc_Status_DateResult plan in (List<LMS.Data.Get_Plan_Transaction_Desc_Status_DateResult>)ViewData["PlanStatus"])
                       { %>
                    <tr>
                        <% if (LMS.Data.Repository.UserRepository.GetUser(User.Identity.Name).Security_Level_ID == 2 || LMS.Data.Repository.UserRepository.GetUser(User.Identity.Name).Security_Level_ID == 5)
                           { %>
                        <td>
                            <img src="../../Content/icons/delete.png" onclick="deletePlan(<%= plan.Plan_Transaction_ID %>)"
                                alt="Delete" style="height: 16px;" />
                        </td>
                        <%} %>
                        <td style="width: 100px">
                            <%= Html.ImageLink("New", "Activity", new { id = plan.Plan_Transaction_ID }, "../../Content/images/calendar.png", "Create New Activity", "", new { style = "border:0px;" })%>
                        </td>
                        <td style="width: 100px">
                            <%= Html.ImageLink("New", "Request", new { id = plan.Plan_Transaction_ID }, "../../Content/icons/compass.png", "Create New Status Request", "", new { style = "border:0px;" })%>
                        </td>
                        <td>
                            <%=Html.ActionLink(plan.Plan_Name, "View", "Transaction", new { id = plan.Plan_Transaction_ID }, null)%>
                        </td>
                        <td>
                            <%= Html.Encode(plan.Description) %>
                        </td>
                        <td>
                            <%= Html.Encode(plan.planTypeDescription) %>
                        </td>
                        <td>
                            <%= Html.Encode(plan.Plan_Date) %>
                        </td>
                    </tr>
                    <%} %>
                </tbody>
            </table>
            <div id="gbsnotes">
                <div class="gbsheader">
                    GBS Notes</div>
                <% foreach (LMS.Data.sp_Get_GBS_NotesResult result in ((List<LMS.Data.sp_Get_GBS_NotesResult>)ViewData["GBSNotes"]).OrderByDescending(r => r.datetimeentered))
                   { %>
                <div style="margin: 10px; border: solid 1px #DDD; padding: 10px;">
                    <b>
                        <%= Html.Encode(string.Format("{0:g}",result.datetimeentered)) %>
                        -
                        <%= Html.Encode(result.name) %></b>
                    <br />
                    <%= Html.Encode(result.notetext) %>
                </div>
                <%} %>
            </div>
        </div>
        <div id="tabs-7" style="background-color: #e0e4ee;">
            <div class="address">
                <table style="width: 400px; position: relative; top: 0px; left: 0px">
                    <tr>
                        <td class="label">
                            Permanent Address:
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 150px;">
                            Address Line 1:
                        </td>
                        <td>
                            <%= Html.Encode(Model.Address_1) %>
                        </td>
                    </tr>
                    <% if (Model.Address_2 != null)
                       { %>
                    <tr>
                        <td style="text-align: right; width: 150px;">
                            Address Line 2:
                        </td>
                        <td>
                            <%= Html.Encode(Model.Address_2)%>
                        </td>
                    </tr>
                    <%} %>
                    <tr>
                        <td style="text-align: right; width: 150px;">
                            City, State Zip:
                        </td>
                        <td>
                            <%= Html.Encode(string.Format("{0}, {1} {2}",Model.City,Model.State.Abbreviation,Model.Zipcode)) %>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 150px;">
                            County:
                        </td>
                        <td>
                            <%= Html.Encode(Model.County) %>
                        </td>
                    </tr>
                    <% if (Model.IsMailing_Address == false)
                       { %>
                    <tr>
                        <td class="label">
                            Mailing Address:
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 150px;">
                            Address Line 1:
                        </td>
                        <td>
                            <%= Html.Encode(Model.Mailing_Address_1) %>
                        </td>
                    </tr>
                    <% if (Model.Mailing_Address_2 != null)
                       { %>
                    <tr>
                        <td style="text-align: right; width: 150px;">
                            Address Line 2:
                        </td>
                        <td>
                            <%= Html.Encode(Model.Mailing_Address_2)%>
                        </td>
                    </tr>
                    <%} %>
                    <tr>
                        <td style="text-align: right; width: 150px;">
                            City, State Zip:
                        </td>
                        <td>
                            <%= Html.Encode(string.Format("{0}, {1} {2}", Model.Mailing_City, Model.Mailing_State.Abbreviation, Model.Mailing_ZipCode))%>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 150px;">
                            County:
                        </td>
                        <td>
                            <%= Html.Encode(Model.Mailing_County) %>
                        </td>
                    </tr>
                    <%} %>
                    <tr>
                        <td class="label">
                            Alternate Address:
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 150px;">
                            Address Line 1:
                        </td>
                        <td>
                            <%= Html.Encode(Model.Alt_Address_1) %>
                        </td>
                    </tr>
                    <% if (Model.Alt_Address_2 != null)
                       { %>
                    <tr>
                        <td style="text-align: right; width: 150px;">
                            Address Line 2:
                        </td>
                        <td>
                            <%= Html.Encode(Model.Alt_Address_2)%>
                        </td>
                    </tr>
                    <%} %>
                    <tr>
                        <td style="text-align: right; width: 150px;">
                            City, State Zip:
                        </td>
                        <td>
                            <%= Html.Encode(string.Format("{0}, {1} {2}",Model.Alt_City,Model.Alt_State.Abbreviation,Model.Alt_Zipcode)) %>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 150px;">
                            County:
                        </td>
                        <td>
                            <%= Html.Encode(Model.Alt_County) %>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <br />
    <div id="ActivityDiv" class="client-outer">
        <div class="section-title">
            Client Facts:
        </div>
        <div class="section-inner">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <%= Html.Encode(LMS.Helpers.StringHelper.DecryptData(Model.Notes)) %>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="HeaderContent">
</asp:Content>
