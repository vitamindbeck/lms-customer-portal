<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<LMS.Data.Record>>" %>
 <%@ Import Namespace="LMS.Helpers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	My Leads
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

   
   <div class="client-outer">
    <div class="client-title">
         [ <%= Html.Encode(Model.Count()) %> New Leads ]
    </div>
    <div class="client-inner">
    <table class="sortable" width="100%" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
            <th>Lead Name</th>
            <th>Submit Date</th>
            <th>State</th>
            <th>Source</th>
            <th>Products</th>
            <th>DOB</th>
            <th>Phone</th>
            <th>Email Address</th>
            <th>Start Date</th>
            <th>Situation</th>
            </tr>
        </thead>
        <tbody>
        <% foreach (LMS.Data.Record record in Model)
           { %>
        <tr>
            <td width="100px">
                <%= Html.ActionLink(StringHelper.UppercaseFirst(record.Last_Name) + ", " + StringHelper.UppercaseFirst(record.First_Name), "Details", new { id = record.Record_ID }, new { target = "_blank", style = "target-new: tab ! important"  })%>
            </td>
            <td width="80px">
                <%= Html.Encode(record.Created_On) %>
            </td>
            <td width="30px">
                <%= Html.Encode(record.State.Abbreviation) %>
            </td>
            <td width="50px">
                <%= Html.Encode(record.Source_Code.Source_Number) %>
            </td>
            <td width="200px">
                <%= Html.Encode(record.ProductsOfInterest) %>
            </td>
            <td width="80px">
                <% if (record.DOB.HasValue)
                   { %>
                <%= Html.Encode(record.DOB.Value.ToShortDateString())%>
                <%}
                   else
                   { %>
                    N/A
                <%} %>
            </td>
            <td width="80px">
                <%= Html.Encode(record.Home_Phone) %>
            </td>
            <td width="80px">
                <%= Html.Encode(record.Email_Address) %>
            </td>
            <td width="80px">
                <% if (record.RequestedStartDate.HasValue)
                   { %>
                <%= Html.Encode(record.RequestedStartDate.Value.ToShortDateString())%>
                <%}
                   else
                   { %>
                    N/A
                <%} %>
            </td>
            <td width="100px">
                <%= Html.Encode(record.CurrentSituation) %>
            </td>
        </tr>
        <%} %>
        </tbody>
    </table>
    </div>
</div> 

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>

