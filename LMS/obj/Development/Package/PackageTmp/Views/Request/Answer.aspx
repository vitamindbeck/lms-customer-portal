<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<LMS.Data.Plan_Transaction_Request>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Answer
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% using (Html.BeginForm())
       { %>
    <h3>
        Question:
        <%= Html.Encode(Model.Question) %></h3>
    <div style="margin-top: 30px;">
        <b>Answer:</b></div>
    <div>
        <%= Html.TextArea("Answer", "", 10, 135, null)%>
    </div>
    <div style="margin-top: 20px;">
        <input type="submit" value="Submit" />
    </div>
    <%} %>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
