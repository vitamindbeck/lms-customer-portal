<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Licensing.Master" Inherits="System.Web.Mvc.ViewPage<LMS.Data.Appointment>" %>
<%@ Import Namespace="LMS.Helpers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	New Appointment
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<script type="text/javascript">
    $(function() {
        $("#Appointment_Date").datepicker();
        $("#Appointment_Date").mask("99/99/9999");
        $("#Terminate_Date").datepicker();
        $("#Terminate_Date").mask("99/99/9999");
        $('.checkAll').click(function() {
            $(this).parents('td:eq(0)').find(':checkbox').attr('checked', this.checked);
        });

    });
</script>

<div class="client-outer" style="background-color: #e0e4ee;">
<% using (Html.BeginForm("New", "Appointment", FormMethod.Post, new { id = "NewAppointment" }))
   {%>
    <div class="section-title">
        Create New Appointment
    </div>
    <div class="section-inner">
        <table class="section" cellpadding="0" cellspacing="0">
          <tr>
            <td class="label" width="150px">
                Agent/Agency:
            </td>
            <td>
                <%= Html.DropDownList("Agent", (SelectList)ViewData["Agent_List"], new { tabindex = "1" })%>
            </td>

            <td class="label">
                Company:
            </td>
            <td>
                <%= Html.DropDownList("Carrier", (SelectList)ViewData["Carrier_List"], new { tabindex = "2" })%>
                <%= Html.ValidationMessage("Carrier", "*")%>
            </td>
          </tr>
          <tr>
            <td class="label">
                Appointment Date:
            </td>
            <td>
                <%= Html.TextBox("Appointment_Date", Model.Appointment_Date.HasValue ? Model.Appointment_Date.Value.ToString("MM/dd/yyyy") : "", new { style = "width: 100px;", tabindex = "3" })%>
                
            </td>           
            <td class="label">
                Product:
            </td>
            <td>
                <%=Html.DropDownList("Line_Of_Insurance", (SelectList)ViewData["Line_Of_Insurance_List"], new { tabindex = "4" })%>
                <%= Html.ValidationMessage("Line_Of_Insurance", "*")%>
            </td>
          </tr>
          <tr>
            <td class="label" valign="top">
                Terminated Date:            
            </td>
            <td valign="top">
                <%=Html.TextBox("Terminate_Date", Model.Terminate_Date.HasValue ? Model.Terminate_Date.Value.ToString("MM/dd/yyyy") : "", new { style = "width: 100px;", tabindex = "3" })%>
            </td>
            <td>
            </td>
            <td>
            </td>
          </tr>
          <tr>
            <td class="label" valign="top">
                Agent/Agency #:
            </td>
            <td valign="top">
                <%= Html.TextBox("Agent_Number", StringHelper.DecryptData(Model.Agent_Number), new { style = "width: 100px;", tabindex = "5" })%>
                <%= Html.ValidationMessage("Agent_Number", "*")%>
            </td>
            <td>
            </td>
            </tr>
            <tr>
            <td class="label" valign="top">
                State(s):
            </td>
            <td colspan = "15">
               <table>
                <tr>
                <% LMS.Data.LMSDataContext db = new LMS.Data.LMSDataContext();
                if(!String.IsNullOrEmpty(Request.Form["Carrier"]))
                {
                    int index = 0;
                    int index2 = index + 15;
                    %>
                     <input type="checkbox" class="checkAll" /> <b>Check All</b> <br/> 
                    
                     <% List<LMS.Data.sp_Get_Authorized_States_By_Carrier_IDResult> rs = db.sp_Get_Authorized_States_By_Carrier_ID(Int32.Parse(Request.Form["Carrier"])).ToList();
                      foreach (LMS.Data.sp_Get_Authorized_States_By_Carrier_IDResult item in rs)
                      { %>
                          <td style="width:10px;"> <%=Html.Encode(item.Abbreviation) %></td><td><%=Html.CheckBox("ck" + item.State_ID, (Request.Form["ck" + item.State_ID] ?? string.Empty).Contains("true"))%> </td>
                           
                            <% index++;
                            if (index == index2)                           
                               {
                                index2 = index2 + 15;  %>
                                <tr>
                             <%}
                      }
                      
                  }%>
                 </table>   
            </td>
          </tr>      
        </table>
    </div>
    
    <div style="text-align: center;">
        <input type="submit" value="n" id="btnAdd" name="btnAdd" class="displaySaveImage" style="color: #e0e4ee;" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="../../Appointment/List" alt="Cancel"><img src="../../content/icons/close.png" alt="Cancel" title="Cancel" style="border: 0px;
                vertical-align: top;" />
        </a>
    </div>
<%} %>
</div>

<script type="text/javascript">
    $("#Carrier").change(function() {
        $("#NewAppointment").submit();
    });

</script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptContent" runat="server">
</asp:Content>
