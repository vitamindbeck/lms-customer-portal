<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Licensing.Master" Inherits="System.Web.Mvc.ViewPage<LMS.Data.Appointment>" %>
<%@ Import Namespace="LMS.Helpers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Edit Appointment
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<script type="text/javascript">
    $(function() {
        $("#Appointment_Date").datepicker();
        $("#Appointment_Date").mask("99/99/9999");
        $("#Terminate_Date").datepicker();
        $("#Terminate_Date").mask("99/99/9999");
        $('.checkAll').click(function() {
        $(this).parents('td:eq(0)').find(':checkbox').attr('checked', this.checked)
       
       
        });
    });
</script>
<div class="client-outer" style="background-color: #e0e4ee;">
<% using (Html.BeginForm("Edit", "Appointment", FormMethod.Post, new { id = "EditAppointment" }))
   {%>
    <div class="section-title">
        Appointment Details
    </div>
    <div class="section-inner">
        <table class="section" cellpadding="0" cellspacing="0">
          <tr>
            <td class="label" width="150px" style="height: 18px">
                Agent/Agency:
            </td>
            <td class="style1">
                 <%= Html.DropDownList("Agent1", (SelectList)ViewData["Agent_List"], new { tabindex = "1" })%>
            </td>

            <td class="label" style="height: 18px">
                Company:
            </td>
            <td class="style1">
                <%= Html.DropDownList("Carrier1", (SelectList)ViewData["Carrier_List"], new { tabindex = "2" })%>
            </td>
          </tr>
          <tr>
            <td class="label" style="height: 18px">
                Appointment Date:
            </td>
            <td class="style1">
               <%= Html.TextBox("Appointment_Date", Model.Appointment_Date.HasValue ? Model.Appointment_Date.Value.ToString("MM/dd/yyyy") : "", new { style = "width: 100px;", tabindex = "3" })%>
              
            </td>           
            <td class="label" style="height: 18px">
                Product:
            </td>
            <td class="style1">
                 <%=Html.DropDownList("Line_Of_Insurance", (SelectList)ViewData["Line_Of_Insurance_List"], new { tabindex = "4" })%>
            </td>
          </tr>
          <tr>
            <td class="label" valign="top">
                Terminated Date:            
            </td>
            <td valign="top">
                <%=Html.TextBox("Terminate_Date", Model.Terminate_Date.HasValue ? Model.Terminate_Date.Value.ToString("MM/dd/yyyy") : "", new { style = "width: 100px;", tabindex = "3" })%>
            </td>
            <td>
            </td>
            <td>
            </td>
          </tr>
          <tr>
            <td class="label" valign="top">
                Agent/Agency #:
            </td>
            <td valign="top">
                <%= Html.TextBox("Agent_Number", StringHelper.DecryptData(Model.Agent_Number), new { style = "width: 100px;", tabindex = "5" })%>
                <%= Html.ValidationMessage("Agent_Number", "*")%>
            </td>
           <tr>
            <td class="label" valign="top">
                State(s):
            </td>
            <td colspan = "15">
               <table>
                <tr>
               <% LMS.Data.LMSDataContext db = new LMS.Data.LMSDataContext();              
                  
               if(!String.IsNullOrEmpty(Request.Form["Carrier"]))
                  {
                      int index = 0;
                      int index2 = index + 15;%>
                      
                     <input type="checkbox" class="checkAll" /> <b>Check All</b> <br/>
                      
                     <%List<LMS.Data.sp_Get_Authorized_States_By_Carrier_IDResult> rs = db.sp_Get_Authorized_States_By_Carrier_ID(Int32.Parse(Request.Form["Carrier1"])).ToList();
                      foreach (LMS.Data.sp_Get_Authorized_States_By_Carrier_IDResult item in rs)
                      {%>
                           <td style="width:10px;"> <%=Html.Encode(item.Abbreviation)%></td><td><%=Html.CheckBox("ck" + item.State_ID, (Request.Form["ck" + item.State_ID] ?? string.Empty).Contains("true"))%></td>
                        <% index++;
                            if (index == index2)                           
                               {
                                index2 = index2 + 15;  %>
                                <tr>
                             <%}
                    
                   }
                  }
                  else
                  {
                       int index = 0;
                       int index2 = index + 15;
                   %>
                    <input type="checkbox" class="checkAll" /> <b>Check All</b> <br/>  
                     <% List<LMS.Data.sp_Get_Authorized_States_By_Carrier_IDResult> rs = db.sp_Get_Authorized_States_By_Carrier_ID(Model.Carrier_ID).ToList();
                      List<LMS.Data.Appointment_State> rsas = db.Appointment_States.Where(s => s.Appointment_ID == Model.Appointment_ID).ToList();
                      bool hasvalue = false;
                      foreach (LMS.Data.sp_Get_Authorized_States_By_Carrier_IDResult item in rs)
                      {
                          foreach(LMS.Data.Appointment_State appstate in rsas)
                          {
                              if (appstate.State_ID == item.State_ID)
                              {
                                  hasvalue = true;
                              }
                          }
                            %>
                            <td style="width:10px;"><%=Html.Encode(item.Abbreviation)%></td><td> <%=Html.CheckBox("ck" + item.State_ID, hasvalue)%> </td>
                            <%
                          hasvalue = false;
                          
                          index++;
                            if (index == index2)                           
                               {
                                index2 = index2 + 15;  %>
                                <tr>
                             <%}
                          
                      }%>
                         
                 <% }%>
                  </table>   
            </td>
          </tr>  
            </td>
          </tr> 
        </table>
    </div>
    <div style="text-align: center;">
        <input type="submit" value="s" id="btnSave" name="btnSave" class="displaySaveImage" style="color: #e0e4ee;" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="../../Appointment/Details/<%=Model.Appointment_ID %>" alt="Cancel"><img src="../../content/icons/close.png" alt="Cancel" title="Cancel" style="border: 0px;
                vertical-align: top;" />
        </a>
    </div>
<%} %>
</div>

<script type="text/javascript">
    $("#Carrier1").change(function() {
        $("#EditAppointment").submit();
    });

</script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
    <style type="text/css">
        .style1
        {
            height: 18px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptContent" runat="server">
</asp:Content>
