﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<LMS.Data.AppSentOpenActivitiesByPodsResult>>" %>
<%@ Import Namespace="LMS.Helpers" %>
<html>
<head>
  <script  type="text/javascript">

      $(function() {

      $("#hSentOpenDetails").click(function() {


      $("#divSentOpenDetails").hide();
      $("#divSentOpenSummary").show();
          //$("#hSentOpenDetails").attr('disabled', true);
          //$("#sSentOpenDetails").removeAttr('disabled')


              return false;
          });
          $("#sSentOpenDetails").click(function() 
          {
              $("#divSentOpenDetails").show();
              $("#divSentOpenSummary").hide();
             // $("#sSentOpenDetails").attr('disabled', true);
              //$("#hSentOpenDetails").removeAttr('disabled');
             
                 return false;
          });

      });

</script>
</head>

<body>
 <div class="client-outer">
    <div class="section-title">
        [
        <% if (Model.Count() > 0)
           { %>
        <%= Model.Count().ToString()%>
        <%}
           else
           { %>
        No
        <%} %>
        Application Sent Open Activities ]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp
        <button id="hSentOpenDetails">Summary</button> 
       <button id="sSentOpenDetails">Details</button>
       
  </div>
 </div>
 
   <div id ="divSentOpenDetails" style="display:none"> 
        <table class="sortable" width="100%" cellpadding="0" cellspacing="0">
            <thead>
                <tr>                    
                    <th>
                        Lead Name
                    </th>
                    <th>
                        Follow Up Date
                    </th>
                     <th>
                        Time
                    </th>
                    <th style="text-align: center;">
                        State
                    </th>
                    <th>
                        Source
                    </th>
                    <th>                    
                        Status
                    </th>
                    <th>
                        Buying Period
                    </th>
                    <th align="center">
                        Eligibility
                    </th>
                </tr>
            </thead>
            <tbody>
                <% foreach (LMS.Data.AppSentOpenActivitiesByPodsResult record in Model)
                   { %>
                <tr>                   
                    <td>
                    <%if (record.Key != null)
                      { %>
                        <span style="background-color:Yellow;">
                    <%} %>
                        <%= Html.ActionLink(StringHelper.UppercaseFirst(record.Last_Name) + ", " + StringHelper.UppercaseFirst(record.First_Name), "Complete", "Activity", new { id = record.Record_Activity_ID }, new { target = "_blank", style = "target-new: tab ! important" })%>
                    <%if (record.Key != null)
                      { %>
                        </span>
                    <%} %>   
                    </td>
                    <td>
                        
                           <%= Html.Encode(string.Format("{0:d}", record.Due_Date))%>
                    </td>
                    <td>
                        <% if (record.Due_Date.Value.TimeOfDay.Hours > 0)
                           { %>
                                <%= Html.Encode(record.Due_Date.Value.ToShortTimeString())%>
                        <%}
                           else
                           { %>
                            
                        <%} %>
                    </td>
                    <td style="text-align: center;">
                        <%= Html.Encode(record.State) %>
                    </td>
                    <td>
                        <%= Html.Encode(record.SourceName) %>
                    </td>
                    <td>
                        <%= Html.Encode(record.PlanDesc) %>
                    </td>
                     <td>
                        <%= Html.Encode(record.BuyingPeriod) %>
                    </td>
                    <td style="text-align: center;">
                        <%= Html.Encode(record.EligibilityCategory) %>
                    </td>
                </tr>
                <%} %>
            </tbody>
        </table>
     </div>
     <div id ="divSentOpenSummary" style="display:none"> 
       <table width="100%" cellpadding="0" cellspacing="0" >
        <thead>
            <tr>
            <th>Agent</th>
            <th>Total leads </th>
           </tr>
        </thead>
 
 <%Dictionary<string, int> dictAppSent = new Dictionary<string, int>(); %>
       
       
       <% foreach (LMS.Data.AppSentOpenActivitiesByPodsResult userAppSent in Model)
           { %>
              
            <%if (dictAppSent.Keys.Contains(userAppSent.User_Name))
              { %>
              
            <%dictAppSent[userAppSent.User_Name]++; %>
             
         
         
           <% }%>
          <%else{ %>
               <% dictAppSent.Add(userAppSent.User_Name, 1); %>
                 
        <% }  %>
        
        <%} %>
        
        <%foreach (string name1 in dictAppSent.Keys)
        {%>
      
       <tbody>
        <tr>
             <td>
                <%= Html.Encode(name1)%>
               </td>
               
              <td>
                <%= Html.Encode(dictAppSent[name1])%>
              </td>
            </tr>
            
            <%} %>
      
              </tbody>      
        
    </table>
  </div>
     
</body>
</html>