<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import Namespace="Helpers" %>
<% LMS.Data.User user = LMS.Data.Repository.UserRepository.GetUser(Page.User.Identity.Name);
   LMS.Data.LMSDataContext db = new LMS.Data.LMSDataContext();%>
<div align="right">

<% switch (user.Security_Level_ID)
{
    case 1: %>
    <a href="/Record/Sales" title="Back to main screen" style="color: #FFFFFF">Back to main screen</a>
 <% 
    break;
    case 2:
  %>
    <a href="/Record/Admin" title="Back to main screen" style="color: #FFFFFF">Back to main screen</a>
<%
    break;
       case 4:
%>
    <a href="/Record/Admin" title="Back to main screen" style="color: #FFFFFF">Back to main screen</a>
<%
    break;
    case 5: 
%>
    <a href="/Record/Support" title="Back to main screen" style="color: #FFFFFF">Back to main screen</a>
<%
    break;
    case 8:
%>
    <a href="/Record/AppointmentSetter" title="Back to main screen" style="color: #FFFFFF">Back to main screen</a>
<%
    break;
    case 7:
%>
    <a href="/Record/Sales" title="Back to main screen" style="color: #FFFFFF">Back to main screen</a>
<%   
    break;
}%>

</div>
<div id="menucontainer" style="text-shadow: 1px 1px 1px #000;">
    <ul id="menu">
        <% if (LMS.Data.Repository.UserRepository.HasSecurityLevel(user,10))
           {
               if (Html.IsCurrentAction("List", "Agent"))
               { %>
            <li class="active">
                <%= Html.ActionLink("Home", "List", "Agent")%></li>
            <% }
               else
               { %>
            <li>
                <%= Html.ActionLink("Home", "List", "Agent")%></li>
            <% }%>
            <% if (Html.IsCurrentAction("List", "License"))
               { %>
            <li class="active">
                <%= Html.ActionLink("Licenses", "List", "License")%></li>
            <% }
               else
               { %>
            <li>
                <%= Html.ActionLink("Licenses", "List", "License")%></li>
            <% }%>
            
            <% if (Html.IsCurrentAction("List", "Appointment"))
               { %>
            <li class="active">
                <%= Html.ActionLink("Appointments", "List", "Appointment")%></li>
            <% }
               else
               { %>
            <li>
                <%= Html.ActionLink("Appointments", "List", "Appointment")%></li>
            <% }%>

            <% if (Html.IsCurrentAction("List", "Certification"))
               { %>
            <li class="active">
                <%= Html.ActionLink("Certifications", "List", "Certification")%></li>
            <% }
               else
               { %>
            <li>
                <%= Html.ActionLink("Certifications", "List", "Certification")%></li>
            <% }%>
            <% if (Html.IsCurrentAction("List", "Logininfo"))
               { %>
            <li class="active">
                <%= Html.ActionLink("Login Info", "List", "Logininfo")%></li>
            <% }
               else
               { %>
            <li>
                <%= Html.ActionLink("Login Info", "List", "Logininfo")%></li>
            <% }
           }
           
           if (LMS.Data.Repository.UserRepository.HasSecurityLevel(user,1))
           {
               int? total = db.sp_Incomplete_Certification_Number_By_User(user.User_ID).ToList()[0].Total;
               if (Html.IsCurrentAction("Certification", "AgentScreen"))
               {
                   if (total == null)
                   {%>
                    <li class="active">
                    <%= Html.ActionLink("Certifications", "Certification", "AgentScreen")%>
                    </li>
                 <%}
                   else
                   {%>
                    <li class="active">
                    <%= Html.ActionLink("Certifications(" + total.ToString() + ")", "Certification", "AgentScreen")%>
                    </li>           
            <%     }
               }
               else
               {
                   if (total == null)
                   {%>
                    <li>
                    <%= Html.ActionLink("Certifications", "Certification", "AgentScreen")%>
                    </li>
                  <%}
                   else
                   {%>
                    <li>
                    <%= Html.ActionLink("Certifications(" + total.ToString() + ")", "Certification", "AgentScreen")%>
                    </li>           
            <%     }
               }%>
            
            
             <% if (Html.IsCurrentAction("License", "AgentScreen"))
               { %>
            <li class="active">
                <%= Html.ActionLink("Licenses", "License", "AgentScreen")%></li>
            <% }
               else
               { %>
            <li>
                <%= Html.ActionLink("Licenses", "License", "AgentScreen")%></li>
            <% }%>


            <%--Mike Added this one--%>
            <% if (Html.IsCurrentAction("List", "Appointment"))
               { %>
            <li class="active">
                <%= Html.ActionLink("Appointments", "List", "Appointment")%></li>
            <% }
               else
               { %>
            <li>
                <%= Html.ActionLink("Appointments", "List", "Appointment")%></li>
            <% }%>


            <% if (Html.IsCurrentAction("List", "Logininfo"))
               { %>
            <li class="active">
                <%= Html.ActionLink("Login Info", "List", "Logininfo")%></li>
            <% }
               else
               { %>
            <li>
                <%= Html.ActionLink("Login Info", "List", "Logininfo")%></li>
            <% }              
           }
           
           
           if(LMS.Data.Repository.UserRepository.HasSecurityLevel(user,2))
           {
               if (Html.IsCurrentAction("List", "License"))
               { %>
            <li class="active">
                <%= Html.ActionLink("Licenses", "List", "License")%></li>
            <% }
               else
               { %>
            <li>
                <%= Html.ActionLink("Licenses", "List", "License")%></li>
            <% }%>

            <% if (Html.IsCurrentAction("List", "Appointment"))
               { %>
            <li class="active">
                <%= Html.ActionLink("Appointments", "List", "Appointment")%></li>
            <% }
               else
               { %>
            <li>
                <%= Html.ActionLink("Appointments", "List", "Appointment")%></li>
            <% }%>

            <% if (Html.IsCurrentAction("List", "Logininfo"))
               { %>
            <li class="active">
                <%= Html.ActionLink("Login Info", "List", "Logininfo")%></li>
            <% }
               else
               { %>
            <li>
                <%= Html.ActionLink("Login Info", "List", "Logininfo")%></li>
            <% }
           }%>

    </ul>
</div>
