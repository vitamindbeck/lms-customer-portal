﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<LMS.Data.AppointmentFollowUpsByPodsResult>>" %>

<%@ Import Namespace="LMS.Helpers" %>
<html>
<head>
  <script  type="text/javascript">

      $(function() {

          $("#hDetails").click(function() {


          $("#divApptFollowUpDetails").hide();
          $("#divApptFollowUpSummary").show();
          
                 //$("#hDetails").attr('disabled', true);
                //$("#sDetails").removeAttr('disabled')


              return false;
          });
          $("#sDetails").click(function() 
          {
              $("#divApptFollowUpDetails").show();
              $("#divApptFollowUpSummary").hide();
             // $("#sDetails").attr('disabled', true);
              //$("#hDetails").removeAttr('disabled');
             
                 return false;
          });

      });

</script>
</head>

<body>
 <div class="client-outer">
    <div class="section-title" style="height: 30px; width: 933px">
        [
        <% if (Model.Count() > 0)
           { %>
        <%= Model.Count().ToString()%>
        <%}
           else
           { %>
        No
        <%} %>
        Appointments ]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
        &nbsp;
        <button id="hDetails">Summary</button> 
       <button id="sDetails">Details</button>
       
  </div>
 </div>
    <div id ="divApptFollowUpDetails" style="display:none"> 
        <table class="sortable" width="100%" cellpadding="0" cellspacing="0">
            <thead>
                <tr>                    
                    <th>
                        Lead Name
                    </th>
                    <th>
                        Follow Up Date
                    </th>
                     <th>
                        Time
                    </th>
                    <th style="text-align: center;">
                        State
                    </th>
                    <th>
                        Source
                    </th>
                    <th>                    
                        Status
                    </th>
                    <th>
                        Buying Period
                    </th>
                    <th align="center">
                        Eligibility
                    </th>
                </tr>
            </thead>
            <tbody>
                <% foreach (LMS.Data.AppointmentFollowUpsByPodsResult record in Model)
                   { %>
                <tr>                   
                    <td>
                    <%if (record.Key != null)
                      { %>
                        <span style="background-color:Yellow;">
                    <%} %>
                        <%= Html.ActionLink(StringHelper.UppercaseFirst(record.Last_Name) + ", " + StringHelper.UppercaseFirst(record.First_Name), "Complete", "Activity", new { id = record.Record_Activity_ID }, new { target = "_blank", style = "target-new: tab ! important" })%>
                    <%if (record.Key != null)
                      { %>
                        </span>
                    <%} %>   
                    </td>
                    <td>
                        
                           <%= Html.Encode(string.Format("{0:d}", record.Due_Date))%>
                    </td>
                    <td>
                        <% if (record.Due_Date.Value.TimeOfDay.Hours > 0)
                           { %>
                                <%= Html.Encode(record.Due_Date.Value.ToShortTimeString())%>
                        <%}
                           else
                           { %>
                            
                        <%} %>
                    </td>
                    <td style="text-align: center;">
                        <%= Html.Encode(record.State) %>
                    </td>
                    <td>
                        <%= Html.Encode(record.SourceName) %>
                    </td>
                    <td>
                        <%= Html.Encode(record.PlanDesc) %>
                    </td>
                     <td>
                        <%= Html.Encode(record.BuyingPeriod) %>
                    </td>
                    <td style="text-align: center;">
                        <%= Html.Encode(record.EligibilityCategory) %>
                    </td>
                </tr>
                <%} %>
            </tbody>
        </table>
    </div>
  <div id ="divApptFollowUpSummary" style="display:none"> 
       <table width="100%" cellpadding="0" cellspacing="0" >
        <thead>
            <tr>
            <th>Agent</th>
            <th>Total leads </th>
           </tr>
        </thead>
 <%Dictionary<string, int> dict1 = new Dictionary<string, int>(); %>
       
       
       <% foreach (LMS.Data.AppointmentFollowUpsByPodsResult user1 in Model)
           { %>
              
            <%if(dict1.Keys.Contains(user1.User_Name)){ %>
              
            <%dict1[user1.User_Name]++; %>
             
         
         
           <% }%>
          <%else{ %>
               <% dict1.Add(user1.User_Name,1); %>
                 
        <% }  %>
        
        <%} %>
        
        <%foreach (string name1 in dict1.Keys)
        {%>
      
       <tbody>
        <tr>
             <td>
                <%= Html.Encode(name1)%>
               </td>
               
              <td>
                <%= Html.Encode(dict1[name1])%>
              </td>
            </tr>
            
            <%} %>
      
              </tbody>      
        
    </table>
  </div>
</body>
</html>
