<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<LMS.Data.GetLeadProspectResult>>" %>

 <%@ Import Namespace="LMS.Helpers" %>
   <div class="client-outer">
    <div class="section-title">
        [
            <% if (Model.Count() > 0)
               { %>
                <%= Html.Encode(Model.Count())%>
                <%}
               else
               { %>
                No
                <%} %>
        Leads ]
    </div>
    <div class="client-inner">
        <table class="sortable" width="100%" cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th>
                        Lead Name
                    </th>
                    <th>
                        Follow Up Date
                    </th>
                    <th style="text-align: center;">
                        State
                    </th>
                    <th>
                        Phone Number
                    </th>
                    <th>
                    
                        Email Address
                    </th>
                    <th style="text-align: center;">
                        Priority
                    </th>
                </tr>
            </thead>
            <tbody>
                <% foreach (LMS.Data.GetLeadProspectResult record in Model)
                   { %>
                <tr>
                    <td width="30%">
                        <%= Html.ActionLink(StringHelper.UppercaseFirst(record.Last_Name) + ", " + StringHelper.UppercaseFirst(record.First_Name), "Details", new { id = record.Record_ID }, new { target = "_blank", style = "target-new: tab ! important" })%>
                    </td>
                    <td width="12%">
                        <%= Html.Encode(string.Format("{0:d}", (record.Due_Date.Value == new DateTime() ? "" : record.Due_Date.Value.ToShortDateString())))%>
                    </td>
                    <td width="5%" style="text-align: center;">
                        <%= Html.Encode(record.State) %>
                    </td>
                    <td width="15%">
                        <%= Html.Encode(record.Phone) %>
                    </td>
                    <td width="20%">
                        <%= Html.Encode(record.Email_Address) %>
                    </td>
                    <td width="8%" style="text-align: center;">
                        <%= Html.Encode(record.Priority_Level) %>
                    </td>
                </tr>
                <%} %>
            </tbody>
        </table>
    </div>
</div>


