<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<LMS.Data.SupportDisplayType>>" %>
<div class="client-outer">
    <div class="client-title">
        [
        <%= Html.Encode(Model.Count()) %>
        Activity Requests ]
    </div>
    <div class="client-inner">
        <table class="sortable" width="100%" cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th>
                        Due Date
                    </th>
                    <th>
                        Name
                    </th>
                    <th>
                        SourceCode
                    </th>
                    <th>
                        Carrier
                    </th>
                    <th>
                        State
                    </th>
                    <th>
                        Activity
                    </th>
                    <th>
                        Shipping
                    </th>
                </tr>
            </thead>
            <tbody>
                <% foreach (var item in Model)
                   { %>
                <tr>
                    <td>
                        <% if (item.RequestDate.Value.TimeOfDay.Hours > 0)
                           { %>
                        <%= Html.Encode(item.RequestDate)%>
                        <%}
                           else
                           { %>
                        <%= Html.Encode(string.Format("{0:d}", item.RequestDate))%>
                        <%} %>
                    </td>
                    <td>
                        <%= Html.ActionLink(item.RecordName, "Complete", "Activity", new { id = item.Record_Activity_ID }, new { target = "_Blank" })%>
                    </td>
                    <td>
                        <%= Html.Encode(item.SourceCode) %>
                    </td>
                    <td>
                        <%= Html.Encode(item.Carrier) %>
                    </td>
                    <td>
                        <%= Html.Encode(item.State) %>
                    </td>
                    <td>
                        <%= Html.Encode(item.Record_Activity_Type) %>
                    </td>
                    <td>
                        <%= Html.Encode(item.OutboundShippingMethod) %>
                    </td>
                </tr>
                <% } %>
            </tbody>
        </table>
    </div>
</div>
