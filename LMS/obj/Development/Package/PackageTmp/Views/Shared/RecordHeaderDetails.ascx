<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<LMS.Data.Record>" %>

 <div id="ClientFactsDiv" class="client-outer"  style="background-color: #e0e4ee;">
        <div class="section-title">
            Client Details (<%= Html.Encode(Model.Customer_ID) %>):
        </div>
        <div class="section-inner">
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td class="label">
                        Name:
                    </td>
                    <td width="260px">
                        <%= Html.Encode(Model.First_Name + " " + Model.Last_Name)%>
                    </td>
                    <td class="label">
                        Home Phone:
                    </td>
                    <td width="180px">
                        <%= Html.Encode(Model.Home_Phone)%>
                    </td>
                    <td class="label">
                        Current Tobacco Use:
                    </td>
                    <td width="160px">
                          <%= Html.Encode(Model.Current_Tobacco_Use_Status_ID > 0 && Model.Current_Tobacco_Use_Status_ID != null ? Model.Current_Tobacco_Use_Status.Description  : "") %>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Address:
                    </td>
                    <% if (Model.IsMailing_Address == true || Model.IsMailing_Address == null)
                       {%>
                    <td>
                        <%= Html.Encode(Model.Address_1)%>
                    </td>
                   <% }
                       else
                       {%> 
                       <td>
                        <%= Html.Encode(Model.Mailing_Address_1)%>
                    </td>
                    <%} %>
                    <td class="label">
                        Work Phone:
                    </td>
                    <td>
                        <%= Html.Encode(Model.Work_Phone)%>
                    </td>
                     <td class="label">
                        Current Tobacco Type:
                    </td>
                    <td width="200px">
                       <%= Html.Encode(Model.Current_Tobacco_ID > 0 && Model.Current_Tobacco_ID != null ? Model.Current_Tobacco.Description : "")%>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <% if (Model.IsMailing_Address == true || Model.IsMailing_Address == null)
                       {%>
                    <td>                    
                        <%= Html.Encode(Model.Address_2)%>
                    </td>
                    <% }
                       else
                       {%> 
                       <td>
                        <%= Html.Encode(Model.Mailing_Address_2)%>
                    </td>
                    <%} %>
                    <td class="label">
                        Cell Phone:
                    </td>
                    <td>
                        <%= Html.Encode(Model.Cell_Phone)%>
                    </td>
                     <td class="label">
                        Past Tobacco Use:
                    </td>
                    <td width="200px">
                          <%= Html.Encode(Model.Past_Tobacco_Use_Status_Id > 0 && Model.Past_Tobacco_Use_Status_Id != null ? Model.Past_Tobacco_Use_Status.Description  : "") %>
                    </td>
                </tr>
                <tr>
                <% if (Model.IsMailing_Address == true || Model.IsMailing_Address == null)
                   {%>
                    <td class="label">
                        City, State Zip:
                    </td>
                    <td>
                        <%= Html.Encode(Model.City + ", " + Model.State.Abbreviation + " " + Model.Zipcode)%>
                    </td>
                    <%}
                   else
                   { %>
                   <td class="label">
                        City, State Zip:
                    </td>
                    <td>
                        <%= Html.Encode(Model.Mailing_City + ", " + Model.Mailing_State.Abbreviation + " " + Model.Mailing_ZipCode)%>
                    </td>
                    <%} %>
                   
                    <td class="label">
                        Fax:
                    </td>
                    <td>
                        <%= Html.Encode(Model.Fax)%>
                    </td>
                     <td class="label">
                        Past Tobacco Type:
                    </td>
                    <td width="200px">
                          <%= Html.Encode(Model.Past_Tobacco_ID > 0 && Model.Past_Tobacco_ID != null ? Model.Past_Tobacco.Description : "")%>
                    </td>
                </tr>
                <tr>
                 <% if (Model.IsMailing_Address == true || Model.IsMailing_Address == null)
                    {%>
                    <td class="label">
                        County:
                    </td>
                    <td>
                        <%= Html.Encode(Model.County)%>
                    </td>
                    <%} else
                    { %>
                    <td class="label">
                        County:
                    </td>
                    <td>
                        <%= Html.Encode(Model.Mailing_County)%>
                    </td>
                   <% } %>
                    <td class="label">
                        Source:
                    </td>
                    <td>
                        <%= Html.Encode(Model.Source_Code.DropDownName)%>
                    </td>
                     <td class="label">
                        Date Quit:
                    </td>
                    <td width="200px">
                        <%= Html.Encode(Model.Quit_Date)%>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Email Address:
                    </td>
                    <td>
                        <a href="mailto:<%= Html.Encode(Model.Email_Address) %>">
                            <%= Html.Encode(Model.Email_Address)%></a>
                    </td>
                    <td class="label">
                        Agent:
                    </td>
                    <td>
                        <%= Html.Encode(Model.Agent.FullName)%>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Birth Date:
                    </td>
                    <td>
                        <%= Html.Encode(string.Format("{0:d}", Model.DOB))%>
                    </td>
                    <td class="label">
                        Assistant Agent:
                    </td>
                    <td>
                        <%= Html.Encode(Model.Assistant_Agent.FullName)%>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Gender:
                    </td>
                    <td>
                        <%= Html.Encode(Model.Sex)%>
                    </td>
                    <td class="label">
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
        </div>
    </div>