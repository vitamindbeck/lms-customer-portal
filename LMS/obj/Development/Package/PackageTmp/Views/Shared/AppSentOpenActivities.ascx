﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<LMS.Data.AppSentOpenActivitiesResult>>" %>

<%@ Import Namespace="LMS.Helpers" %>
<div class="client-outer">
    <div class="section-title">
        [
        <% if (Model.Count() > 0)
           { %>
        <%= Model.Count().ToString()%>
        <%}
           else
           { %>
        No
        <%} %>
        Application Sent Open Activities ]
    </div>
    <div class="client-inner">
        <table class="sortable" width="100%" cellpadding="0" cellspacing="0">
            <thead>
                <tr>                    
                    <th>
                        Lead Name
                    </th>
                    <th>
                        Follow Up Date
                    </th>
                     <th>
                        Time
                    </th>
                    <th style="text-align: center;">
                        State
                    </th>
                    <th>
                        Source
                    </th>
                    
                    <th>
                        Carrier
                    </th>
                    <th style="text-align: right;">
                        Requested Start
                    </th>
                </tr>
            </thead>
            <tbody>
                <% foreach (LMS.Data.AppSentOpenActivitiesResult record in Model)
                   { %>
                <tr>                   
                    <td>
                    <%if (record.Key != null)
                      { %>
                        <span style="background-color:Yellow;">
                    <%} %>
                        <%= Html.ActionLink(StringHelper.UppercaseFirst(record.Last_Name) + ", " + StringHelper.UppercaseFirst(record.First_Name), "Complete", "Activity", new { id = record.Record_Activity_ID }, new { target = "_blank", style = "target-new: tab ! important" })%>
                    <%if (record.Key != null)
                      { %>
                        </span>
                    <%} %>   
                    </td>
                    <td>
                        
                           <%= Html.Encode(string.Format("{0:d}", record.Due_Date))%>
                    </td>
                    <td>
                        <% if (record.Due_Date.Value.TimeOfDay.Hours > 0)
                           { %>
                                <%= Html.Encode(record.Due_Date.Value.ToShortTimeString())%>
                        <%}
                           else
                           { %>
                            
                        <%} %>
                    </td>
                    <td style="text-align: center;">
                        <%= Html.Encode(record.State) %>
                    </td>
                    <td>
                        <%= Html.Encode(record.SourceName) %>
                    </td>               
                     <td>
                        <%= Html.Encode(record.Carrier) %>
                    </td>
                    <td style="text-align: right;">
                    <% if (record.Buying_Period_Effective_Date != null)
                          { %>
                                <%= Html.Encode(record.Buying_Period_Effective_Date.Value.ToShortDateString())%>
                        <%}%>
                    </td>
                </tr>
                <%} %>
            </tbody>
        </table>
    </div>
</div>
