<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<LMS.Data.Record>" %>
<div class="client-outer">
    <div class="section-title">
        <div style="float: left;">
            Customer:
            <%= Html.Encode(Model.Customer_ID) %>
        </div>
    <div class="section-inner">
        <table class="section" width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td class="label">
                    Name:
                </td>
                <td>
                    <%= Html.Encode(Model.FullName) %>
                </td>
                <td class="label">
                    Home Phone:
                </td>
                <td>
                    <%= Html.Encode(Model.Home_Phone) %>
                </td>
                <td class="label">
                    Source:
                </td>
                <td>
                    <%= Html.Encode(Model.Source_Code.DropDownName) %>
                </td>
            </tr>
            <tr>
                <td class="label">
                    Address:
                </td>
                <td>
                    <%= Html.Encode(Model.Address_1) %>
                </td>
                <td class="label">
                    Work Phone:
                </td>
                <td>
                    <%= Html.Encode(Model.Work_Phone) %>
                </td>
                <td class="label">
                    Record Type:
                </td>
                <td>
                    <%= Html.Encode(Model.Record_Type.Description) %>
                </td>
            </tr>
            <tr>
                <td class="label">
                </td>
                <td>
                    <%= Html.Encode(Model.Address_2) %>
                </td>
                <td class="label">
                    Cell Phone:
                </td>
                <td>
                    <%= Html.Encode(Model.Cell_Phone) %>
                </td>
                <td class="label">
                    Agent:
                </td>
                <td>
                    <%= Html.Encode(Model.Agent.FullName) %>
                </td>
            </tr>
            <tr>
                <td class="label">
                </td>
                <td>
                    <%= Html.Encode(Model.City) %>
                </td>
                <td class="label">
                    Alternate Addr Phone:
                </td>
                <td>
                    &nbsp;
                </td>
                <td class="label">
                    Agent Asst:
                </td>
                <td>
                    <%= Html.Encode(Model.Assistant_Agent.FullName) %>
                </td>
            </tr>
            <tr>
                <td class="label">
                </td>
                <td>
                    <%= Html.Encode(Model.State.Abbreviation) %>
                </td>
                <td class="label">
                    Fax:
                </td>
                <td>
                    <%= Html.Encode(Model.Fax) %>
                </td>
                <td class="label">
                    Referred By:
                </td>
                <td>
                    <%= Html.Encode(Model.Reffered_By) %>
                </td>
            </tr>
            <tr>
                <td class="label">
                    County:
                </td>
                <td>
                    <%= Html.Encode(Model.County) %>
                </td>
                <td class="label">
                    EMail Address:
                </td>
                <td>
                    <%= Html.Encode(Model.Email_Address) %>
                </td>
            </tr>
            <tr>
                <td class="label">
                    Alternative Address:
                </td>
                <td>
                    <%= Html.Encode(Model.Alt_Address_1) %>
                </td>
                <td class="label">
                    Momentum:
                </td>
                <td>
                    <%= Html.Encode(Model.Momentum) %>
                </td>
                <td class="label">
                    Buying Period:
                </td>
                <td>
                    <%= Html.Encode(Model.Buying_Period.Description) %>
                </td>
            </tr>
            <tr>
                <td class="label">
                </td>
                <td>
                    <%= Html.Encode(Model.Alt_Address_2) %>
                </td>
                <td class="label">
                    Do Not Call:
                </td>
                <td>
                    <%= Html.Encode(Model.Do_Not_Contact) %>
                </td>
                <td class="label">
                    Sub Buying Period:
                </td>
                <td>
                    <%= Html.Encode(Model.Buying_Period_Sub.Description) %>
                </td>
            </tr>
            <tr>
                <td class="label">
                </td>
                <td>
                    <%= Html.Encode(Model.Alt_City) %>
                </td>
                <td class="label">
                    B2E:
                </td>
                <td>
                    <%= Html.Encode(Model.B2E) %>
                </td>
                <td class="label">
                    Priority Level:
                </td>
                <td>
                    <%= Html.Encode(Model.Priority_Level) %>
                </td>
            </tr>
            <tr>
                <td class="label">
                </td>
                <td>
                    <%= Html.Encode(Model.Alt_State.Abbreviation) %>
                </td>
                <td class="label">
                    &nbsp;
                </td>
                <%= Html.Encode(Model.Alt_Address_From_Date) %>
                <td>
                </td>
                <td class="label">
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="label">
                    Alt Add From:
                </td>
                <td>
                </td>
                <td class="label">
                    To:
                </td>
                <td>
                    <%= Html.Encode(Model.Alt_Address_To_Date) %>
                </td>
                <td class="label">
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
    </div>
</div>
</div>