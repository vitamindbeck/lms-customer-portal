<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Licensing.Master" Inherits="System.Web.Mvc.ViewPage<LMS.Data.Certification>" %>
<%@ Import Namespace="LMS.Helpers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Certification Details
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div id ="CertificationDetails" class="client-outer" style="background-color: #e0e4ee;">
    <div class="section-title">
       Certification Details
    </div>
    <div class="section-inner">
        <table class="section" cellpadding="0" cellspacing="0">
          <tr>
            <td class="label" style="width: 172px">
                Agent:
            </td>
            <td class="style2">
                <%= ViewData["Agent_Name"]%>
            </td>
             <td class="label" style="width: 172px">
                Carrier:
            </td>
            <td class="style2">
                <%= ViewData["Carrier"]%>
            </td>
           
          </tr>
          <tr>
           <td class="label" style="width: 168px">
               Registered Date:
            </td>
            <td>
                 <%= Html.Encode(Model.Registered_Date.HasValue ? Model.Registered_Date.Value.ToShortDateString() : "") %>
            </td>
            <td class="label" style="width: 172px">
              Complete Date:
            </td>
            <td class="style2">
              <%= Html.Encode(Model.Complete_Date.HasValue ? Model.Complete_Date.Value.ToShortDateString() : "") %>
            </td>
           
          </tr>
          <tr>
           <td class="label" style="width: 168px">
                 Status:
            </td>
            <td>
                  <%= ViewData["Certification_Status_ID"]%>
            </td>
           <td class="label" style="width: 172px">
                Type:
            </td>
            <td class="style2">
                <%= ViewData["Certification_Type_ID"]%>
              
            </td>
          
          </tr>
          <tr>
          
            <td class="label">
                Link:
            </td>
            <td>
                 <%= Html.Encode(Model.Certification_Link) %>
           </td>
             <td class="label">
               Due Date:
            </td>
            <td class="style2">
              <%= Html.Encode(Model.Due_Date.HasValue ? Model.Due_Date.Value.ToShortDateString() : "") %>
            </td>
         </tr>
         <tr>
         <td class="label">
                User name:
            </td>
            <td>
              <%= Html.Encode(StringHelper.DecryptData(Model.Certification_User_Name)) %>
            </td>
            <td class="label">
               Password:
            </td>
            <td>
                <%= Html.Encode(StringHelper.DecryptData(Model.Certification_Password)) %>
            </td>
         </tr>
           <tr>
            <td class="label" style="width: 173px">
               Last Updated:
            </td>
            <td>
                <%= Html.Encode(Model.Last_Updated_Date) %>
            </td>
             <td>
                <%if (!String.IsNullOrEmpty(ViewData["UserName"].ToString()))
                  { %>
                <strong>By:</strong>
                <%} %>
             </td>
             <td>
                <%= ViewData["UserName"] %>
            </td>
          </tr>
          <tr>
          <td class="label">
                Special Instructions:
            </td>
            <td colspan="3">
                  <%= Html.Encode(Model.Special_Instructions) %>
           </td>
          </tr>
        </table>
    </div>
</div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptContent" runat="server">
</asp:Content>
