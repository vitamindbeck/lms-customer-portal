<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Licensing.Master" Inherits="System.Web.Mvc.ViewPage<LMS.Data.Certification>" %>
<%@ Import Namespace="LMS.Helpers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	New Certification
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <script type="text/javascript" src="../../Scripts/RecordValidateSelect.js"></script>
<script type="text/javascript">
    $(function() {
        $("#Registered_Date").datepicker();
        $("#Registered_Date").mask("99/99/9999");
        $("#Complete_Date").datepicker();
        $("#Complete_Date").mask("99/99/9999");
        $("#Due_Date").datepicker();
        $("#Due_Date").mask("99/99/9999");

        $('#NewCertification').load()
        {
            if ($("#ckOther").is(":checked")) {
                $("#noncarrier1").show();
                $("#noncarrier2").show();
                $("#carrier1").hide();
                $("#carrier2").hide();
            }
            else {
                $("#carrier1").show();
                $("#carrier2").show();
                $("#noncarrier1").hide();
                $("#noncarrier2").hide();
            }
        };

        $("#ckOther").click(function() {
            if ($("#ckOther").is(":checked")) {
                $("#noncarrier1").show();
                $("#noncarrier2").show();
                $("#carrier1").hide();
                $("#carrier2").hide();
            }
            else {
                $("#carrier1").show();
                $("#carrier2").show();
                $("#noncarrier1").hide();
                $("#noncarrier2").hide();
            }
        });

    });

</script>
<%= Html.ValidationSummary("Unable to Create Record.") %>
<div class="client-outer" style="background-color: #e0e4ee;">
 <% using (Html.BeginForm("New", "Certification", FormMethod.Post, new { id = "NewCertification" }))
      
    {%>
    <div class="section-title">
        Create New Certification
    </div>
    <div class="section-inner">
        <table class="section" cellpadding="0" cellspacing="0">
          <tr>
            <td class="label">
                Select Agent:
            </td>
            <td>
              <%= Html.DropDownList("Agent_ID", (SelectList)ViewData["Agent_Name"], new { style = "width: 150px;", tabindex = "1" })%>
              <%= Html.ValidationMessage("Agent", "*")%>
            </td>
            <td class="label">
                <span id="carrier1">
                Carrier:
                </span>
            </td>
            <td><span id="carrier2">
                <%= Html.DropDownList("Carrier_ID", (SelectList)ViewData["Carrier"], new { style = "width: 280px;", tabindex = "2" })%>
                </span>
                
                Other? <%=Html.CheckBox("ckOther", (Request.Form["ckOther"] ?? string.Empty).Contains("true")) %>
            </td>
          </tr>
          <tr>
            <td class="label">
                Registered Date:
            </td>
            <td>
                <%= Html.TextBox("Registered_Date", Model.Registered_Date.HasValue ? Model.Registered_Date.Value.ToString("MM/dd/yyyy") : "", new { style = "width: 150px;", tabindex = "3" })%>
                  <%= Html.ValidationMessage("Registered_Date", "*")%>
           </td>
           <td class="label"><span id="noncarrier1">Non-Carrier: </span>
           </td>
           <td>
                <span id="noncarrier2"><%= Html.DropDownList("Non_Carrier_ID", (SelectList)ViewData["NonCarrier"])%></span>
           </td>
          </tr>
          <tr>
            <td class="label">
               Status:
            </td>
            <td>
                <%= Html.DropDownList("Certification_Status_ID", (SelectList)ViewData["Certification_Status"], new { style = "width: 150px;", tabindex = "5" })%>
           </td>

            <td class="label">
               Complete Date:
            </td>
            <td>
             <%= Html.TextBox("Complete_Date", Model.Complete_Date.HasValue ? Model.Complete_Date.Value.ToString("MM/dd/yyyy") : "", new { style = "width: 150px;", tabindex = "6" })%>
            </td>
          </tr>
          <tr>
            <td class="label">
                Link:
            </td>
            <td>
                <%= Html.TextBox("Certification_Link", Model.Certification_Link, new { style = "width: 150px;", tabindex = "7" })%>
           </td>
            <td class="label">
                Type:
            </td>
            <td>
                  <%= Html.DropDownList("Certification_Type_ID", (SelectList)ViewData["Certification_Type"], new { style = "width: 150px;", tabindex = "8" })%>
            </td>
          </tr>
           <tr>
            <td class="label">
               Due Date:
            </td>
            <td>
             <%= Html.TextBox("Due_Date", Model.Due_Date.HasValue ? Model.Due_Date.Value.ToString("MM/dd/yyyy") : "", new { style = "width: 150px;", tabindex = "9" })%>
            </td>
            <td class="label">
                User name:
            </td>
            <td>
                <%= Html.TextBox("Cert_User_Name", StringHelper.DecryptData(Model.Certification_User_Name), new { style = "width: 150px;", tabindex = "10" })%>
           </td>
          </tr>
            <tr>
           <td class="label" valign="top">
            Special Instructions:
           </td>
           <td>
                <%= Html.TextArea("Special_Instructions", Model.Special_Instructions, new { style = "width: 200px;", rows = "6", maxlength = "1000", tabindex = "11" })%>
           </td>
            <td class="label" valign="top">
               Password:
            </td>
            <td valign="top">
                <%= Html.TextBox("Cert_Password", StringHelper.DecryptData(Model.Certification_Password), new { style = "width: 150px;", tabindex = "12" })%>
            </td>
          </tr>
        </table>
    </div>
    <div style="text-align: center;">
        <input type="image" src="../../Content/icons/check.png" title="Save" alt="Save" id="saveBtn" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="../../Certification/List" alt="Cancel"><img src="../../content/icons/close.png" alt="Cancel" title="Cancel" style="border: 0px;
                vertical-align: top;" />
        </a>
    </div>
<%} %>
</div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
    <style type="text/css">
        .style2
        {
            width: 241px;
        }
        .style3
        {
            height: 80px;
            width: 241px;
        }
    </style>
</asp:Content>

