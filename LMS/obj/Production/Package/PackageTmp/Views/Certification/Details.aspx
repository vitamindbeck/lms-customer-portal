<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Licensing.Master" Inherits="System.Web.Mvc.ViewPage<LMS.Data.Certification>" %>
<%@ Import Namespace="LMS.Helpers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Certification Details
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
 
  

<div style="text-align: left;">
<% LMS.Data.User user = LMS.Data.Repository.UserRepository.GetUser(Page.User.Identity.Name); %>
<%if (LMS.Data.Repository.UserRepository.HasSecurityLevel(user, 10))
  {%>       
        <a href="../../Certification/Edit/<%= Model.Certification_ID %>" style="text-decoration: none;">
            <img src="../../content/icons/document_edit.png" title="Edit" alt="Edit" style="border: 0px;
                vertical-align: middle;" />
        </a>
<%} %>     
</div>
    <br/>
<div id ="CertificationDetails" class="client-outer" style="background-color: #e0e4ee;">
    <div class="section-title">
       Certification Details
    </div>
    <div class="section-inner">
        <table class="section" cellpadding="0" cellspacing="0">
          <tr>
            <td class="label">
                Agent:
            </td>
            <td>
                <%= ViewData["Agent_Name"]%>
            </td>
             <td class="label">
                Carrier:
            </td>
            <td>
                <%= ViewData["Carrier"]%>
            </td>
           
          </tr>
          <tr>
           <td class="label">
               Registered Date:
            </td>
            <td>
                 <%= Html.Encode(Model.Registered_Date.HasValue ? Model.Registered_Date.Value.ToShortDateString() : "") %>
            </td>
            <td class="label">
                Non-Carrier:
            </td>
            <td>
                <%= ViewData["NonCarrier"]%>
            </td>
          </tr>
          <tr>
           <td class="label">
                 Status:
            </td>
            <td>
                  <%= ViewData["Certification_Status"]%>
            </td>
            <td class="label">
              Complete Date:
            </td>
            <td>
              <%= Html.Encode(Model.Complete_Date.HasValue ? Model.Complete_Date.Value.ToShortDateString() : "") %>
            </td>
          </tr>
          <tr>
            <td class="label">
                Link:
            </td>
            <td>
              <a href="<%= Html.Encode(Model.Certification_Link) %>" target=_blank"><%= Html.Encode(Model.Certification_Link) %></a>
           </td>
           <td class="label">
                Type:
            </td>
            <td>
                <%= ViewData["Certification_Type"]%>
            </td>
         </tr>
         <tr>
            <td class="label">
               Due Date:
            </td>
            <td>
              <%= Html.Encode(Model.Due_Date.HasValue ? Model.Due_Date.Value.ToShortDateString() : "") %>
            </td>

         <td class="label">
                User name:
            </td>
            <td>
                 <%= Html.Encode(StringHelper.DecryptData(Model.Certification_User_Name)) %>
           </td>
         </tr>
           <tr>
            <td class="label">
               Last Updated:
            </td>
            <td>
                <%= Html.Encode(Model.Last_Updated_Date) %>
                <%if (!String.IsNullOrEmpty(ViewData["UserName"].ToString()))
                  { %>
                <strong>By:</strong>
                <%} %>
                <%= ViewData["UserName"] %>
            </td>
            <td class="label">
               Password:
            </td>
            <td>
               <%= Html.Encode(StringHelper.DecryptData(Model.Certification_Password)) %>
            </td>
          </tr>
          <tr>
           <td class="label" width="150px" valign="top">
                Special Instructions:
            </td>
            <td colspan="3">
                  <%= Html.Encode(Model.Special_Instructions) %>
           </td>
          </tr>
        </table>
    </div>
</div>



</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptContent" runat="server">
</asp:Content>
