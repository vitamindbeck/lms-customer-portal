<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<List<LMS.Data.Record>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Duplicate
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <% Html.RenderPartial("RecordHeaderDetails", (LMS.Data.Record)ViewData["Record"]); %>

    <br />
    
    <div class="client-outer">
    <div class="section-title">
        [
        <% if (Model.Count() > 0)
           { %>
        <%= Model.Count().ToString()%>
        <%}
           else
           { %>
        No
        <%} %>
        Potential Duplicates ]
    </div>
    <div class="client-inner">
        <table class="sortable" width="100%" cellpadding="0" cellspacing="0">
            <thead>
                <tr> 
                    <th>
                        Customer #
                    </th>                   
                    <th>
                        Lead Name
                    </th>
                    <th style="text-align: center;">
                        State, Zip
                    </th>
                    <th>
                        Source
                    </th>
                    <th>                    
                        DOB
                    </th>
                    <th style="text-align: center;">
                        Agent
                    </th>
                    <th>                    
                        Submit Date
                    </th>
                </tr>
            </thead>
            <tbody>
                <% foreach (LMS.Data.Record record in Model)
                   { %>
                <tr>     
                    <td width="80px">
                        
                           <%= Html.Encode(record.Customer_ID)%>
                        
                    </td>              
                    <td width="150px">
                        <%= Html.ActionLink(LMS.Helpers.StringHelper.UppercaseFirst(record.Last_Name) + ", " + LMS.Helpers.StringHelper.UppercaseFirst(record.First_Name), "Edit", "Record", new { id = record.Record_ID }, new { target = "_blank", style = "target-new: tab ! important" })%>
                        
                    </td>
                    <td width="100px" style="text-align: center;">
                        <%= Html.Encode(record.State.Abbreviation) + ", " + Html.Encode(record.Zipcode) %>
                    </td>
                    <td width="250px">
                        <%= Html.Encode(record.Source_Code.DropDownName) %>
                    </td>
                    <td width="80px">
                        <%= Html.Encode(string.Format("{0:d}", record.DOB))%>
                    </td>
                    <td width="150px" style="text-align: center;">
                        <%= Html.Encode(record.Agent.FullName) %>
                    </td>
                    <td width="80px">
                        <%= Html.Encode(string.Format("{0:d}", record.Created_On))%>
                    </td>
                </tr>
                <%} %>
            </tbody>
        </table>
    </div>
</div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
