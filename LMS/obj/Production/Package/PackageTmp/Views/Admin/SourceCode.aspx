<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<LMS.Data.Source_Code>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    SourceCode
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="SourceCodeDiv" class="client-outer">
        <div class="section-title">
            Source Code Details:
        </div>
        <div class="section-inner">
            <% using (Html.BeginForm())
               { %>
            <table class="section" width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="label">
                        Source Code Number:
                    </td>
                    <td>
                        <%= Html.TextBox("DOB", Model.Source_Number, new { style = "width: 80px" })%>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Group:
                    </td>
                    <td>
                        <%= Html.DropDownList("Group_ID")%>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Description:
                    </td>
                    <td>
                        <%= Html.TextBox("Description")%>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Affinity Partner:
                    </td>
                    <td>
                        <%= Html.DropDownList("Affinity_Partner_ID")%>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Paid:
                    </td>
                    <td>
                        <%= Html.CheckBox("Paid")%>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Cost:
                    </td>
                    <td>
                        <%= Html.TextBox("Cost")%>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Notes:
                    </td>
                    <td>
                        <%= Html.TextArea("Notes")%>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Web Select:
                    </td>
                    <td>
                        <%= Html.CheckBox("Web_Select")%>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Active:
                    </td>
                    <td>
                        <%= Html.CheckBox("Active")%>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Marketing Group:
                    </td>
                    <td>
                        <%= Html.DropDownList("Marketing_Group_ID")%>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Media:
                    </td>
                    <td>
                        <%= Html.DropDownList("Media_Type_ID")%>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                    </td>
                    <td>
                        <input type="image" src="../../Content/icons/check.png" alt="Save" />
                        <a href="../../Admin.aspx/SourceCodes">
                            <img src="../../content/icons/close.png" alt="Cancel" style="border: 0px; vertical-align: top;" />
                        </a>
                    </td>
                </tr>
            </table>
            <%} %>
        </div>
    </div>
</asp:Content>
