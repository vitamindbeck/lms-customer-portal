<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>
<%@ Import Namespace="LMS.Data.Repository" %>
<% 
    //no cache 
    Response.CacheControl = "no-cache";
    Response.AddHeader("Pragma", "no-cache");
    Response.Expires = -1;
%>

<%
    if(UserRepository.GetUser(User.Identity.Name).Online_Status_ID == 1)
  { %>
   <span class="online">(Online)</span> 
<%}
  else if(UserRepository.GetUser(User.Identity.Name).Online_Status_ID == 2)
  { %>
   <span class="offline">(Offline)</span> 
<%}
  else
  { %>
   <span class="away">(Away)</span> 
<%} %>
   
