<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>
<% 
    //no cache 
    Response.CacheControl = "no-cache";
    Response.AddHeader("Pragma", "no-cache");
    Response.Expires = -1;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <span style="background-color:#FFFF66; color:Red; font-weight:bold;">
    In edit by <%=ViewData["InEditBy"] %>
    </span>
</body>
</html>
