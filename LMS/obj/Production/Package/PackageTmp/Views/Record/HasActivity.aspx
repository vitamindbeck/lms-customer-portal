<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<LMS.Data.sp_Has_ActivityResult>" %>
<% 
    //no cache 
    Response.CacheControl = "no-cache";
    Response.AddHeader("Pragma", "no-cache");
    Response.Expires = -1;
%>
<%if (Model.hasOCA.HasValue && Model.hasOCA.Value == true) 
  {%>
  <span style="color:Red">No open activity allowed for this user!</span>
<%}%>
