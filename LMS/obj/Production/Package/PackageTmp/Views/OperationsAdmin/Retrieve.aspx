﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<LMS.Models.CustomSearchViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Retrieve
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <link rel="stylesheet" type="text/css" media="screen" href="../../Content/themes/base/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="../../Content/jqGrid/jquery-ui-jqgrid.css" />
    <script src="../../Scripts/jquery-1.5.2.min.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-ui-1.8.7.min.js" type="text/javascript"></script>
    <script src="../../Scripts/json2.min.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-ui.multiselect.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery.tmpl.min.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery.jqGrid.locale-en-4.1.2.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery.jqGrid-4.1.2.min.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery.jqGrid.addons-4.1.2.js" type="text/javascript"></script>
    <h2>
        Retrieve</h2>
    <div style="display: none;" id="dialog-confirm" title="Confirm">
        <p>
            <span class="ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>Are
            you sure want send this</p>
        <p>
            <span id="names"></span>
        </p>
    </div>
    <div id="mysearch" style="background-color: #E0E4EE;">
        <fieldset>
            <legend>Search</legend>
            <%using (Html.BeginForm("Search", "OperationsAdmin"))
              {%>
            <table cellpadding="0" cellspacing="0" class="section" border="0">
                <tr>
                    <td class="label">
                        Agent Group
                    </td>
                    <td class="label">
                        <%= Html.DropDownList("Agent_Group", (SelectList)ViewData["AgentGroupDDL"], new { tabindex = "2" })%>
                    </td>
                    <td class="label">
                        Agent Status:
                    </td>
                    <td>
                        <%= Html.DropDownList("Agent_Status", (SelectList)ViewData["AgentStatusDDL"], new { tabindex = "2" })%>
                    </td>
                    <td class="label">
                        Agent Type:
                    </td>
                    <td>
                        <%= Html.DropDownList("Agent_Type", (SelectList)ViewData["AgentTypeDDL"], new { tabindex = "3" })%>
                    </td>
                </tr>
                <%--    <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                   
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>--%>
                <tr>
                    <td class="label" style="height: 20px; width: 154px">
                        Product Type:
                    </td>
                    <td class="style1">
                        <%= Html.DropDownList("Product_Type", (SelectList)ViewData["ProductTypeDDL"], new { tabindex = "4" })%>
                    </td>
                    <td class="label" style="height: 20px; width: 152px">
                        Plan Name:
                    </td>
                    <td class="style1">
                        <%= Html.DropDownList("Plan_Name", (SelectList)ViewData["PlanNameDDL"], new { tabindex = "5" })%>
                    </td>
                    <%-- <td class="label" style="height: 20px; width: 126px">
                        Plan Selected:
                    </td>
                    <td class="style1">
                        <%= Html.DropDownList("Plan_Name", (SelectList)ViewData["PlanName"], new { tabindex = "3" })%>
                    </td>--%>
                </tr>
                <tr>
                    <td class="label">
                        Carrier
                    </td>
                    <td class="label" colspan="3">
                        <%= Html.DropDownList("Carrier", (SelectList)ViewData["CarrierDDL"], new { tabindex = "6" })%>
                    </td>
                </tr>
                <tr>
                    <td class="label" style="width: 154px">
                        Date of Birth Start
                    </td>
                    <td>
                        <%=Html.TextBox("DateofBirthStart", Model.DateofBirthStart, new { style = "width: 80px;", tabindex = "7" })%>
                    </td>
                    <td class="label" style="width: 154px">
                        Date of Birth End
                    </td>
                    <td>
                        <%=Html.TextBox("DateofBirthEnd", Model.DateofBirthEnd, new { style = "width: 80px;", tabindex = "8" })%>
                    </td>
                    <td class="label" style="width: 154px">
                        Date Month
                    </td>
                    <td>
                        <%= Html.DropDownList("DOB_Month", (SelectList)ViewData["DOB_MonthSearchDDL"], new { tabindex = "9" })%>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Zip Code
                    </td>
                    <td class="style4">
                        <%= Html.TextBox("PostalCode", Model.PostalCode, new { style = "width: 100px;", tabindex = "10" })%>
                    </td>
                    <td class="label" style="width: 152px">
                        County
                    </td>
                    <td>
                        <%= Html.TextBox("Country", Model.Country, new { style = "width: 100px;", tabindex = "11" })%>
                    </td>
                    <td class="label" style="width: 154px">
                        State:
                    </td>
                    <td>
                        <%= Html.ListBox("States", (SelectList)ViewData["State"], new { multiple = true, tabindex = "12" })%>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Next Follow Up From
                    </td>
                    <td class="style4">
                        <%=Html.TextBox("NextFollowUpStart", Model.NextFollowUpStart, new { style = "width: 80px;", tabindex = "13" })%>
                    </td>
                    <td class="label" style="width: 152px">
                        Next Follow Up To
                    </td>
                    <td>
                        <%=Html.TextBox("NextFollowUpEnd", Model.NextFollowUpEnd, new { style = "width: 80px;", tabindex = "14" })%>
                    </td>
                    <td class="label" style="width: 154px">
                        Follow Up Type
                    </td>
                    <td>
                        <%= Html.DropDownList("FollowUpType", (SelectList)ViewData["FollowUpTypeDDL"], new { tabindex = "15" })%>
                    </td>
                </tr>
                <tr>
                    <td class="label" style="width: 126px">
                        Follow Up for
                    </td>
                    <td>
                        <%= Html.DropDownList("FollowUpUser", (SelectList)ViewData["AgentFinal_searchDDL"], new { tabindex = "16" })%>
                    </td>
                    <td class="label" style="width: 154px">
                        No Follow Ups Before
                    </td>
                    <td>
                        <%=Html.TextBox("NoFollowUpsBefore", Model.NoFollowUpsBefore, new { style = "width: 80px;", tabindex = "17" })%>
                    </td>
                    <td class="label" style="width: 152px">
                        Current Dispostion
                    </td>
                    <td>
                        <%= Html.DropDownList("CurrentDispostion", (SelectList)ViewData["CurrentDispostionDDL"], new { tabindex = "17" })%>
                    </td>
                </tr>
                <tr>
                    <td class="label" style="width: 154px">
                        Partner
                    </td>
                    <td>
                        <%= Html.DropDownList("Partner", (SelectList)ViewData["PartnerDDL"], new { tabindex = "17" })%>
                    </td>
                    <td class="label">
                        Source Number:
                    </td>
                    <td class="style4">
                        <%= Html.DropDownList("SourceNumber", (SelectList)ViewData["SourceNumberDDL"], new { tabindex = "18" })%>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Policy #
                    </td>
                    <td class="style4">
                        <%=Html.TextBox("PolicyNumber", Model.PolicyNumber, new { style = "width: 80px;", tabindex = "19" })%>
                    </td>
                    <td class="label">
                        Policy Effective Date Start
                    </td>
                    <td class="style4">
                        <%=Html.TextBox("PolicyEffectiveStart", Model.PolicyEffectiveStart, new { style = "width: 80px;", tabindex = "20" })%>
                    </td>
                    <td class="label" style="width: 152px">
                        Policy Effective Date End
                    </td>
                    <td>
                        <%=Html.TextBox("PolicyEffectiveEnd", Model.PolicyEffectiveEnd, new { style = "width: 80px;", tabindex = "21" })%>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Customer ID
                    </td>
                    <td class="style4">
                        <%=Html.TextBox("LMSID", Model.LMSID, new { style = "width: 80px;", tabindex = "22" })%>
                    </td>
                    <td class="label">
                        Customer Fist Name
                    </td>
                    <td class="style4">
                        <%=Html.TextBox("FirstName", Model.FirstName, new { style = "width: 80px;", tabindex = "22" })%>
                    </td>
                    <td class="label">
                        Customer Last Name
                    </td>
                    <td class="style4">
                        <%=Html.TextBox("LastName", Model.LastName, new { style = "width: 80px;", tabindex = "22" })%>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Current Record Status<%-- Activity Status--%>
                    </td>
                    <td class="style4">
                        <%= Html.DropDownList("RecordStatus", (SelectList)ViewData["RecordStatusDDL"], new { tabindex = "23" })%>
                    </td>
                    <td class="label">
                        Record Type
                    </td>
                    <td class="style4">
                        <%= Html.DropDownList("Record_Type_ID", (SelectList)ViewData["RecordTypeSearchDDL"], new { tabindex = "24" })%>
                    </td>
                    <td class="label">
                        Agent Final:
                    </td>
                    <td>
                        <%= Html.DropDownList("Agent_Final", (SelectList)ViewData["AgentFinal_searchDDL"], new { tabindex = "27" })%>
                    </td>
                </tr>
                <%-- <tr>
                    <td colspan="6" style="background: none repeat scroll 0 0 #304F9F; color: #FFFFFF;
                        font-size: 13px; font-weight: bold; padding: 2px; text-align: center;">
                        Master File
                    </td>
                </tr>--%>
                <tr>
                    <td class="label" style="width: 126px">
                        Priority
                    </td>
                    <td>
                        <%=Html.DropDownListFor(m => m.Priority, (SelectList)ViewData["PriorityDDL"], new { tabindex = "25" })%>
                    </td>
                    <td class="label" style="width: 154px">
                        Catagory Driver
                    </td>
                    <td>
                        <%= Html.DropDownList("Category_Driver", (SelectList)ViewData["Catagory_Driver_searchDDL"], new { tabindex = "26" })%>
                    </td>
                    <td class="label">
                        Agents Departed
                    </td>
                    <td>
                        <%= Html.ListBox("Agent_Departed", (SelectList)ViewData["AgentDeparted_searchDDL"], new { multiple = true, tabindex = "27" })%>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Category Situation
                    </td>
                    <td class="style4" colspan="3">
                        <%= Html.DropDownList("CategorySituationID", (SelectList)ViewData["CategorySituationSearchDDL"], new { tabindex = "28" })%>
                    </td>
                    <td class="label">
                        Plan Status
                    </td>
                    <td class="style4" colspan="1">
                        <%= Html.DropDownList("Plan_Transaction_Status_ID", (SelectList)ViewData["PlanTransactionStatusDDL"], new { tabindex = "29" })%>
                    </td>
                </tr>
                <tr>
                    <td class="label" style="width: 154px">
                        Effective Month
                    </td>
                    <td>
                        <%= Html.DropDownList("EffectiveDateOrPurchaseDateMonth", (SelectList)ViewData["Effective_MonthSearchDDL"], new { tabindex = "30" })%>
                    </td>
                </tr>
                <tr>
                    <td colspan="8" align="center">
                        <button type="button" class="btn btn-default" id="btnClearSearch">
                            Clear</button>&nbsp;&nbsp;&nbsp;
                        <button type="submit" class="btn btn-default" id="btnSubmit">
                            Submit</button>
                    </td>
                </tr>
            </table>
            <%} %>
        </fieldset>
    </div>
    <div style="clear: both;">
    </div>
    <div style="width: 100%;">
        <div id="div_bulkEdit" style="float: left;">
            <fieldset>
                <legend>Bulk Edit section</legend>
                <table>
                    <%--<tr>
                        <td class="label">
                            Priortiy:
                        </td>
                        <td>
                            <%= Html.DropDownList("priority_blk", (SelectList)ViewData["PriorityDDL"], new { tabindex = "29" })%>
                        </td>
                    </tr>--%>
                    <tr>
                        <td class="label">
                            Catagory Driver:
                        </td>
                        <td>
                            <%= Html.DropDownList("Catagory_Driver_blk", (SelectList)ViewData["Catagory_DriverDDL"], new { tabindex = "30" })%>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            Agent Final:
                        </td>
                        <td>
                            <%= Html.DropDownList("Agent_Final_blk", (SelectList)ViewData["AgentFinalDDL"], new { tabindex = "31" })%>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            Category Situation:
                        </td>
                        <td>
                            <%= Html.DropDownList("Category_Situation_blk", (SelectList)ViewData["CategorySituationDDL"], new { tabindex = "32" })%>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <input id="getSelected" type="button" value="Update Selected" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2">
                            <div id="divResponseResult" style="color: Red;">
                            </div>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </div>
        <div style="width: 50%; float: left; margin-left: 5px;">
            <fieldset style="width: auto;">
                <legend>Follow Up Activity</legend>
                <table>
                    <tr>
                        <td class="label" width="200px">
                            Follow Up Date:
                        </td>
                        <td width="200px">
                            <%= Html.TextBox("CreateFollowUp_Date", "", new { style = "width: 80px", tabindex = "33" })%>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="200px">
                            Follow Up Activity:
                        </td>
                        <td width="200px">
                            <%= Html.DropDownList("CreateFollowUp_Type", (SelectList)ViewData["FollowUpTypeDDL"], new { tabindex = "34" })%>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="200px">
                            Follow Set For:
                        </td>
                        <td width="200px">
                            <%= Html.DropDownList("CreateFollowUp_User", (SelectList)ViewData["AgentFinal_searchDDL"], new { tabindex = "35" })%>
                            <%--<span id="hasActivity"></span>--%>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="200px">
                            Follow Up Priority:
                        </td>
                        <td width="200px">
                            <%= Html.DropDownList("CreateFollowUp_Priority", (SelectList)ViewData["PriorityDDL"], new { tabindex = "36" })%>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" width="200px">
                            Notes
                        </td>
                        <td width="200px">
                            <%= Html.TextArea("CreateFollowUp_Notes", new { style = "width: 300px;" })%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input id="btnCreateFU" type="button" value="Create Follow Up" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2">
                            <div id="divResultsFU" style="color: Red;">
                            </div>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </div>
    </div>
    <div style="clear: both;">
    </div>
    <table id="jqgProducts" cellpadding="0" cellspacing="0">
    </table>
    <div id="jqgpProducts" style="text-align: center;">
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            var grid = $("#jqgProducts");
            grid.jqGrid({
                //url from wich data should be requested
                url: '<%=Url.Action("Retrieve", "OperationsAdmin") %>',
                //url for inline edit
                editurl: '<%=Url.Action("UpdateMaster", "OperationsAdmin") %>',

                onSelectRow: function (currentSelectedRow) {
                    //                    alert(currentSelectedRow);

                    //                    if (currentSelectedRow && currentSelectedRow != $.lastSelectedRow) {
                    //                        //save changes in row 
                    //                        grid.jqGrid('saveRow', $.lastSelectedRow, false);
                    //                        $.lastSelectedRow = currentSelectedRow;
                    //                    }
                    //                    //trigger inline edit for row
                    //                    grid.jqGrid('editRow', $.lastSelectedRow, true);
                },
                gridComplete: function () {
                    var ids = grid.jqGrid('getDataIDs');
                    for (var i = 0; i < ids.length; i++) {
                        var cl = ids[i];
                        //                        be = "<a id='edit'  href='#' onclick=\"jQuery('#jqgProducts').editRow('" + cl + "');\" >edit</a>";
                        //                        se = "<a href='#' onclick=\"jQuery('#jqgProducts').saveRow('" + cl + "');\" >save</a>";
                        //                        ce = "<a href='#' onclick=\"jQuery('#jqgProducts').restoreRow('" + cl + "');\">cancel</a>";

                        be = "<input style='padding-left: 0; padding-right: 0; font-size:10px; background-color:green' type='button' value='E' onclick=\"jQuery('#jqgProducts').editRow('" + cl + "');\" />";
                        se = "<input style='padding-left: 0; padding-right: 0; font-size:10px; background-color:blue' type='button' value='S' onclick=\"jQuery('#jqgProducts').saveRow('" + cl + "');\" />";
                        ce = "<input style='padding-left: 0; padding-right: 0; font-size:10px; background-color:red' type='button' value='C' onclick=\"jQuery('#jqgProducts').restoreRow('" + cl + "');\" />";

                        grid.jqGrid('setRowData', ids[i], { act: be + se + ce });
                    }
                },
                //type of data
                datatype: 'json',
                //url access method type
                mtype: 'POST',
                //columns names
                colNames: ['', 'Row', 'MasterFileID', 'GBSPolicyID', 'Cust ID', 'Lead Name', 'Follow Up', 'State', 'Category Driver', 'Priority', 'Product type', 'Plan Type', 'Source', 'Agent Final', 'Category Situation', 'Record_ID', 'Plan_Transaction_ID', 'Plan Status', 'Effective Date', 'DOB'],
                //columns model
                colModel: [
                            { name: 'act', index: 'act', width: 50, hidden: false, sortable: false },
                            { name: 'Row', index: 'Row', key: true, width: 60, align: "center", hidden: true, editable: true, editoptions: { disabled: true} },

                            { name: 'MasterFileID', index: 'MasterFileID', align: "Left", width: 60, hidden: true, editable: true, editoptions: { disabled: true} },
                            { name: 'GBSPolicyID', index: 'GBSPolicyID', align: 'left', width: 100, hidden: true, editable: true, editoptions: { disabled: true} },
                            { name: 'LMSID', index: 'LMSID', align: 'left', width: 100, hidden: false, editable: true, editoptions: { disabled: true} },

                            { name: 'Lead_Name', index: 'Lead_Name', align: 'left', width: 100, forceFit: true, editable: false, edittype: 'text', editoptions: { maxlength: 40 }, editrules: { required: true} },
                            { name: 'NextFollowUp', index: 'NextFollowUp', width: 100, align: 'left', editable: false },
                            { name: 'Customer_State', index: 'Customer_State', width: 35, align: 'left', editable: false },

                            { name: 'CategoryDriverID', index: 'CategoryDriverID', width: 100, align: 'left', editable: true, edittype: 'select', editoptions: { dataUrl: '<%=Url.Action("Category_Driver")%>' }, editrules: { required: true} },
                //          { name: 'Priority', index: 'Priority', width: 73, align: 'left', editable: true, edittype: 'select', editoptions: { dataUrl: '<%=Url.Action("Priority")%>' }, editrules: { required: false} },
                            {name: 'Priority', index: 'Priority', align: 'left', width: 50, editable: false },

                            { name: 'Product_Type', index: 'Product_Type', align: 'left', editable: false },
                            { name: 'Plan_Type', index: 'Plan_Type', width: 80, align: 'left', editable: false },
                            { name: 'SourceNumber', index: 'SourceNumber', width: 60, align: 'center', editable: false },

                            { name: 'AgentFinalUserID', index: 'AgentFinalUserID', width: 125, align: 'left', editable: true, edittype: 'select', editoptions: { dataUrl: '<%=Url.Action("Agent_Final")%>' }, editrules: { required: false} },
                            { name: 'CategorySituationID', index: 'CategorySituationID', width: 200, align: 'left', editable: true, edittype: 'select', editoptions: { dataUrl: '<%=Url.Action("Category_Situation_DDL")%>' }, editrules: { required: false} },

                            { name: 'Record_ID', index: 'Record_ID', align: "Left", width: 60, hidden: true, editable: true, editoptions: { disabled: true} },
                            { name: 'Plan_Transaction_ID', index: 'Plan_Transaction_ID', align: "Left", width: 60, hidden: true, editable: true, editoptions: { disabled: true} },
                            { name: 'Plan_Transaction_Status_Description', index: 'Plan_Transaction_Status_Description', width: 110, align: 'left', editable: false },

                            { name: 'EffectiveDateOrPurchaseDate', index: 'EffectiveDateOrPurchaseDate', width: 100, align: 'right', editable: false },
                            { name: 'DOB', index: 'DOB', width: 100, align: 'right', editable: false },

                          ],
                //pager for grid
                pager: $('#jqgpProducts'),
                //number of rows per page
                rowNum: 25,
                rowList: [50, 100, 500],
                //initial sorting column
                sortname: 'NextFollowUp',
                //initial sorting direction
                sortorder: 'asc',
                //we want to display total records count
                viewrecords: true,
                //grid height
                height: '110%',
                width: '1000px',
                multiselect: true,
                multiboxonly: true
            });
            //          -------------Nav Bar-------------
            grid.jqGrid('navGrid', '#jqgpProducts',
                { add: false, del: false, edit: false, search: false }
            //  { width: 'auto', url: '<%=Url.Action("UpdateMaster", "OperationsAdmin") %>' }
            //  { width: 'auto', url: '<%=Url.Action("InsertProduct", "OperationsAdmin") %>' },
            //  { width: 'auto', url: '<%=Url.Action("DeleteProduct", "OperationsAdmin") %>' }
                );

            $("#getSelected").click(function () {
                $('#divResponseResult').html('Update Processing');

                var ids = grid.jqGrid('getGridParam', 'selarrrow');
                //                var globalPriority = $('#priority_blk').attr('value');

                var globalCategory_Driver = $('#Catagory_Driver_blk').attr('value');
                var globalCategory_DriverText = $("#Catagory_Driver_blk option:selected").text();

                var globalAgent_Final = $('#Agent_Final_blk').attr('value');
                var globalAgent_FinalText = $("#Agent_Final_blk option:selected").text();

                var globalCategory_Situation = $('#Category_Situation_blk').attr('value');
                var globalCategory_SituationText = $("#Category_Situation_blk option:selected").text();

                if (ids.length > 0) {
                    var items = [];

                    for (var i = 0, il = ids.length; i < il; i++) {
                        grid.restoreRow(ids[i]);

                        var data = { method: "viewModel",
                            MasterFileID: grid.jqGrid('getCell', ids[i], 'MasterFileID'),
                            GBSPolicyID: grid.jqGrid('getCell', ids[i], 'GBSPolicyID'),
                            LMSRecordID: grid.jqGrid('getCell', ids[i], 'LMSID').replace('LMS', '').replace('WEB', ''),
                            //                            Priority: globalPriority,
                            CategoryDriverID: globalCategory_Driver,
                            AgentFinalUserID: globalAgent_Final,
                            CategorySituationID: globalCategory_Situation,
                            CategoryDriverText: globalCategory_DriverText,
                            AgentFinalUserText: globalAgent_FinalText,
                            CategorySituationText: globalCategory_SituationText
                        };
                        items.push(data);
                    };

                    $.ajax({
                        type: "POST",
                        url: '<%=Url.Action("UpdateMaster", "OperationsAdmin") %>',
                        data: JSON.stringify({ viewModel: items }),
                        //                        data: { viewModel: items },
                        dataType: "JSON",
                        contentType: 'application/json; charset=utf-8',
                        success: function (data) {
                            $('#divResponseResult').html(data["Message"]);
                            grid.trigger("reloadGrid")
                        }
                    })
                }
            });

            $("#btnCreateFU").click(function () {
                $('#divResultsFU').html('Update Processing');

                var ids = grid.jqGrid('getGridParam', 'selarrrow');

                var CreateFollowUp_Priority = $('#CreateFollowUp_Priority').attr('value');
                var CreateFollowUp_Type = $('#CreateFollowUp_Type').attr('value');
                var CreateFollowUp_Date = $('#CreateFollowUp_Date').attr('value');
                var CreateFollowUp_User = $('#CreateFollowUp_User').attr('value');
                var FollowUpNotes = $('#CreateFollowUp_Notes').attr('value');

                if (ids.length > 0) {
                    var items = [];

                    for (var i = 0, il = ids.length; i < il; i++) {
                        grid.restoreRow(ids[i]);

                        var data = { method: "createFollowUp",
                            CategoryDriverID: grid.jqGrid('getCell', ids[i], 'CategoryDriverID'),
                            FollowUpDate: CreateFollowUp_Date,
                            FollowUpType: CreateFollowUp_Type,
                            FollowUpPriority: CreateFollowUp_Priority,
                            FollowUpFor: CreateFollowUp_User,
                            FollowUpNotes: FollowUpNotes,
                            Record_ID: grid.jqGrid('getCell', ids[i], 'Record_ID'),
                            Plan_Transaction_ID: grid.jqGrid('getCell', ids[i], 'Plan_Transaction_ID')
                        };
                        items.push(data);
                    };

                    $.ajax({
                        type: "POST",
                        url: '<%=Url.Action("CreateFollowUp", "OperationsAdmin") %>',
                        data: JSON.stringify({ createFollowUp: items }),
                        dataType: "json",
                        contentType: 'application/json; charset=utf-8',
                        success: function (data) {
                            $('#divResultsFU').html(data["Message"]);
                        }
                    });
                }
            });

            $("#CreateFollowUp_Date").datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: '1900:2020'
            });

            $("#NextFollowUpStart").datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: '1900:2020'
            });
            $('#NextFollowUpStart').keypress(function (e) {
                if (e.keyCode == '13') {
                    e.preventDefault();
                    $('#btnSubmit').click();
                }
            });

            $("#NextFollowUpEnd").datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: '1900:2020'
            });
            $('#NextFollowUpEnd').keypress(function (e) {
                if (e.keyCode == '13') {
                    e.preventDefault();
                    $('#btnSubmit').click();
                }
            });

            $("#PolicyEffectiveStart").datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: '1900:2020'
            });
            $('#PolicyEffectiveStart').keypress(function (e) {
                if (e.keyCode == '13') {
                    e.preventDefault();
                    $('#btnSubmit').click();
                }
            });

            $("#PolicyEffectiveEnd").datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: '1900:2020'
            });
            $('#PolicyEffectiveEnd').keypress(function (e) {
                if (e.keyCode == '13') {
                    e.preventDefault();
                    $('#btnSubmit').click();
                }
            });

            $("#NoFollowUpsBefore").datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: '1900:2020'
            });
            $('#NoFollowUpsBefore').keypress(function (e) {
                if (e.keyCode == '13') {
                    e.preventDefault();
                    $('#btnSubmit').click();
                }
            });

            $("#DateofBirthStart").datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: '1900:2020'
            });
            $('#DateofBirthStart').keypress(function (e) {
                if (e.keyCode == '13') {
                    e.preventDefault();
                    $('#btnSubmit').click();
                }
            });

            $("#DateofBirthEnd").datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: '1900:2020'
            });
            $('#DateofBirthStart').keypress(function (e) {
                if (e.keyCode == '13') {
                    e.preventDefault();
                    $('#btnSubmit').click();
                }
            });

            $("#btnClearSearch").click(function () {

                $('#Agent_Group')[0].selectedIndex = 0;
                $('#Agent_Status')[0].selectedIndex = 0;
                $('#Agent_Type')[0].selectedIndex = 0;
                $('#Agent_Final')[0].selectedIndex = 0;
                $('#Carrier')[0].selectedIndex = 0;
                $('#Plan_Name')[0].selectedIndex = 0;
                //                $('#Plan_Selected')[0].selectedIndex = 0;
                $('#Product_Type')[0].selectedIndex = 0;
                $('#DateofBirthStart').val('');
                $('#DateofBirthEnd').val('')
                $('#States')[0].selectedIndex = 0;
                $('#Agent_Departed')[0].selectedIndex = 0;
                $('#PostalCode').val('');
                $('#Country').val('');
                $('#Category_Driver')[0].selectedIndex = 0;
                $('#Priority')[0].selectedIndex = -1;
                $('#NextFollowUpStart').val('');
                $('#NextFollowUpEnd').val('');
                $('#FollowUpType')[0].selectedIndex = 0;
                $('#FollowUpUser')[0].selectedIndex = 0;
                $('#RecordStatus')[0].selectedIndex = 0;
                $('#CurrentDispostion')[0].selectedIndex = 0;
                $('#SourceNumber')[0].selectedIndex = 0;
                $('#Partner')[0].selectedIndex = 0;
                $('#PolicyEffectiveStart').val('');
                $('#PolicyEffectiveEnd').val('');
                $('#NoFollowUpsBefore').val('');
                $('#CategorySituationID')[0].selectedIndex = 0;
                $('#Record_Type_ID')[0].selectedIndex = 0;
                $('#PolicyNumber').val('');
                $('#DOB_Month')[0].selectedIndex = 0;

                $('#LMSID').val('');
                $('#FirstName').val('');
                $('#LastName').val('');


                $('#Plan_Transaction_Status_ID')[0].selectedIndex = -1;

                $('#EffectiveDateOrPurchaseDateMonth')[0].selectedIndex = 0;



                return false;
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
    <style type="text/css">
        .style1
        {
            height: 20px;
        }
        .style2
        {
            width: 137px;
        }
        .style3
        {
            height: 20px;
            width: 137px;
        }
        .style4
        {
            width: 56px;
        }
        .style5
        {
            height: 20px;
            width: 56px;
        }
        .style6
        {
            width: 152px;
        }
        .style7
        {
            width: 154px;
        }
        .style8
        {
            width: 126px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ScriptContent" runat="server">
</asp:Content>
