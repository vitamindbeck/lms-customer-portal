<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<LMS.Data.Plan_Transaction_Request>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    New
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="TransactionContainer">
        <% using (Html.BeginForm())
           { %>
           <div><h1>Status Inquiry</h1></div>
        <div style="margin-top: 20px;">
            Assign To:</div>
        <div>
            <%= Html.DropDownList("To_Employee_ID")%></div>
        <div style="margin-top: 20px;">
            Question:</div>
        <div>
            <%= Html.TextArea("Question", "", 10, 110, null)%></div>
        <div style="margin-top: 20px;">
            <input type="submit" value="Submit" />
        </div>
        <%} %>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
