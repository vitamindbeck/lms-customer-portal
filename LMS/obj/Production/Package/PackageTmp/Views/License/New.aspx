<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Licensing.Master" Inherits="System.Web.Mvc.ViewPage<LMS.Data.License>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	New License
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<script type="text/javascript">
    $(function() {
        $("#Effective_Date").datepicker();
        $("#Effective_Date").mask("99/99/9999");
        $("#Renewal_Date").datepicker();
        $("#Renewal_Date").mask("99/99/9999");

        $('#AddNewProduct').click(function(e) {
            e.preventDefault();
            // example of calling the confirm function
            // you must use a callback function to perform the "yes" action
            confirmProduct("Continue", function() {
                alert("OK");
            });
        });
    });

    function confirmProduct(message, callback) {
        $('#LineOfInsurancePanel').modal({
            overlayCss: {
                backgroundColor: '#000',
                cursor: 'wait'
            }, containerCss: {
                height: 120,
                width: 500,
                backgroundColor: '#fff',
                border: '3px solid #ccc'
            }
        });
    }
</script>

<div id="LineOfInsurancePanel" style="display: none; padding: 3px;">
    <% using (Html.BeginForm("CreateLineOfInsurance", "License", FormMethod.Post))
       {%>
    <div class="client-outer" style="background-color: #a7b5dc;">
        <div class="section-title">
            Create New Line of Authority:
        </div>
        <div class="section-inner">
            <table class="section" cellpadding="0" cellspacing="0" style="margin: 10px;" width="100%">
                <tr>
                    <td class="label" width="150px">
                        Line of Authority Description:
                    </td>
                    <td>
                        <%= Html.TextBox("Description","", new { style = "width: 150px" })%>
                        <%= Html.ValidationMessage("Description", "*")%>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <br />
    <div style="text-align: center;">
        <input type="image" src="../../Content/icons/check.png" title="Create New Product"
            value="Create" id="Image1" style="vertical-align: middle;" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <img title="Cancel" src="../../Content/icons/close.png" id="Img1" onclick="$.modal.close()"
            style="vertical-align: middle; margin-bottom: 3px;" alt="Cancel" />
    </div>
    <%} %>
</div>
<div class="client-outer" style="background-color: #e0e4ee;">
<% using (Html.BeginForm())
   {%>
    <div class="section-title">
        Create New License
    </div>
    <div class="section-inner">
        <table class="section" cellpadding="0" cellspacing="0">
          <tr>
            <td class="label" width="200px">
                Agent/Agency:
            </td>
            <td>
                <%= Html.DropDownList("Agent",(SelectList)ViewData["Agent_List"], new { tabindex = "1" })%>
            </td>
            <td class="label" width="200px">
                State:
            </td>
            <td>
                <%= Html.DropDownList("States", (SelectList)ViewData["State"], new { tabindex = "2" })%>
                <%= Html.ValidationMessage("States", "*")%>
            </td>
          </tr>
          <tr>
            <td class="label">
                Effective Date:
            </td>
            <td>
                <%= Html.TextBox("Effective_Date", Model.Effective_Date.HasValue ? Model.Effective_Date.Value.ToString("MM/dd/yyyy") : "", new { style = "width: 100px;", tabindex = "3" })%>
                <%= Html.ValidationMessage("Effective_Date", "*")%>
            </td>
            <td class="label">
                License #:
            </td>
            <td>
                <%= Html.TextBox("License_Number", Model.License_Number, new { style = "width: 100px;", tabindex = "4" })%>
                <%= Html.ValidationMessage("License_Number", "*")%>
            </td>
          </tr>
          <tr>
            <td class="label">
                Line of Authority:
            </td>
            <td>
                <%= Html.DropDownList("Line_Of_Insurance_ID", (SelectList)ViewData["Line_Of_Insurance"], new { tabindex = "5" })%>
                <%= Html.ValidationMessage("Line_Of_Insurance_ID", "*")%> 
                <a href="#" id="AddNewProduct" title="Add New">Add New</a>
            </td>
            <td class="label">
                Renewal Date:
            </td>
            <td>
                <%= Html.TextBox("Renewal_Date", Model.Renewal_Date.HasValue ? Model.Renewal_Date.Value.ToString("MM/dd/yyyy") : "", new { style = "width: 100px;", tabindex = "6" })%>
                
            </td>
          </tr>
          <tr>
            <td class="label">
                Active:
            </td>
            <td>
            <%= Html.CheckBox("Active", Model.Active.HasValue && Model.Active == true ? true : false, new { tabindex = "7" }) %>
            </td>
            <td class="label">
            </td>
            <td>               
            </td>
          </tr>
        </table>
    </div>
    <div style="text-align: center;">
        <input type="image" src="../../Content/icons/check.png" title="Save" alt="Save" id="saveBtn" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="../../License/List" alt="Cancel"><img src="../../content/icons/close.png" alt="Cancel" title="Cancel" style="border: 0px;
                vertical-align: top;" />
        </a>
    </div>
<%} %>
</div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptContent" runat="server">
</asp:Content>
