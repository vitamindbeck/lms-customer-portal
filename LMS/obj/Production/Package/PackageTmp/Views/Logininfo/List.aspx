<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Licensing.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Login Information
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<% using (Html.BeginForm("List", "LoginInfo", FormMethod.Post, new { id = "LoginInfoList" }))
   {%>
   <% LMS.Data.User user = LMS.Data.Repository.UserRepository.GetUser(Page.User.Identity.Name); %>
    <%if (LMS.Data.Repository.UserRepository.HasSecurityLevel(user, 10))
      {%>
<table width="100%" border="0">
 <tr>
  <td width="400px" valign="top">
  <b>Select filters:</b>
    <p>Agent: <%= Html.DropDownList("Agent", (SelectList)ViewData["Agent_List"], new { tabindex = "1" })%>
    </p>
    <p>Carrier: <%= Html.DropDownList("Carrier", (SelectList)ViewData["Carrier_List"], new { tabindex = "1" })%>
    </p>
    <p>Non-Carrier: <%= Html.DropDownList("NonCarrier", (SelectList)ViewData["NonCarrier_List"], new { tabindex = "1" })%>
    </p>
    <p>State: <%= Html.DropDownList("State", (SelectList)ViewData["State_List"], new { tabindex = "1" })%>
    </p>
    <p><input type="submit" value="Get Results" id="btnSubmit" />
    </p>
    <%if (LMS.Data.Repository.UserRepository.HasSecurityLevel(user, 10))
      {%>
     
            <%=Html.ActionLink("Add New Login Information", "New", new { id = Int32.Parse(Request.Form["Agent"] ?? "1") })%>
       <%} %>
  </td>

<td align="right" valign="top">
<% LMS.Data.LMSDataContext db = new LMS.Data.LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["WEBContextConnectionString"].ConnectionString);
   List<LMS.Data.sp_Agent_ListResult> rs = db.sp_Agent_List().ToList();
   string agentid = rs[0].Agent_ID.ToString();
   LMS.Data.User usr = LMS.Data.Repository.UserRepository.GetUser(Page.User.Identity.Name); %>
    <%if (LMS.Data.Repository.UserRepository.HasSecurityLevel(usr, 10))
      {%>
           <%=Html.ActionLink("Export to Excel", "Export", new { AgentID = Int32.Parse(Request.Form["Agent"] ?? agentid), CarrierID = Int32.Parse(Request.Form["Carrier"] ?? "-2"), StateID = Int32.Parse(Request.Form["State"] ?? "-2"), NoncarrierID = Int32.Parse(Request.Form["NonCarrier"] ?? "-2") })%>
    <%} %>
      

  
  </td>
 </tr>
</table>
<%} %>
<div id="DivResult">
<% Html.RenderPartial("LoginInfoList", ViewData["LoginInfoList"]); %>
</div>



<% } %>
    

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptContent" runat="server">
</asp:Content>
