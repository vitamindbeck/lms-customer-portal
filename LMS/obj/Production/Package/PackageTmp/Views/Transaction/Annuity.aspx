<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<LMS.Data.Plan_Transaction>" %>

<%@ Import Namespace="LMS.Helpers" %>
<%@ Import Namespace="Helpers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Annuity
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% LMS.Data.Record record = (LMS.Data.Record)ViewData["Record"]; %>
    <% Html.RenderPartial("RecordHeaderDetails", record); %>
    <script type="text/javascript">
        $(document).ready(function () {

            $("#Status_Date").mask("99/99/9999").datepicker({
                showOn: 'button',
                buttonImage: '../../Content/icons/calendar.gif',
                buttonImageOnly: true
            });

            //  bindDropDownList($get("Plan_ID"));
            SetInitialDropDownList($get("Carrier_ID"), $get("Plan_ID"));
            $('#Created_On').datepicker({ showOn: 'button', buttonImage: '../../Content/icons/calendar.gif', buttonImageOnly: true });
        });
    </script>
    <br />
    <% using (Html.BeginForm())
       { %>
    <div id="ClientFactsDiv" class="client-outer" style="background-color: #e0e4ee;">
        <div class="section-title">
            Plan Details:
        </div>
        <div class="section-inner">
            <table style="border: none; width: inherit !important;" class="section">
                <tr>
                    <td valign="top">
                        <table cellpadding="0" cellspacing="0" style="border: none;">
                            <% if (LMS.Data.Repository.UserRepository.GetUser(Page.User.Identity.Name).Security_Level_ID == 2)
                               { %>
                            <tr>
                                <td class="label">
                                    Created On:
                                </td>
                                <td>
                                    <%= Html.TextBox("Created_On")%>
                                </td>
                            </tr>
                            <%}%>
                            <tr>
                                <td class="label">
                                    Agent:
                                </td>
                                <td>
                                    <%= Html.DropDownList("Agent_ID")%>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Source:
                                </td>
                                <td>
                                    <%= Html.DropDownList("Source_Code_ID")%>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Current Status:
                                </td>
                                <td>
                                    <%= Html.DropDownList("Plan_Transaction_Status_ID")%>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    eApplication:
                                </td>
                                <td>
                                    <%= Html.CheckBox("eApp")%>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Carrier:
                                </td>
                                <td>
                                    <%= Html.DropDownList("Carrier_ID")%>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Plan:
                                </td>
                                <td>
                                    <%= Html.CascadingDropDownList("Plan_ID", "Carrier_ID")%>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Deposit Amount:
                                </td>
                                <td>
                                    <%= Html.TextBox("Deposit_Amount",string.Format("{0:c}",Model.Deposit_Amount))%>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Underwriting Class:
                                </td>
                                <td>
                                    <%= Html.DropDownList("Underwriting_Class_ID")%>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Modal Premium:
                                </td>
                                <td>
                                    <%= Html.TextBox("Modal_Premium", string.Format("{0:c}", Model.Modal_Premium))%>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Mode:
                                </td>
                                <td>
                                    <%= Html.DropDownList("Modal_ID")%>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Annual Premium:
                                </td>
                                <td>
                                    <%= Html.TextBox("Annual_Premium", string.Format("{0:c}", Model.Annual_Premium))%>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Cash With App:
                                </td>
                                <td>
                                    <%= Html.CheckBox("Cash_With_Application")%>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Payment Type:
                                </td>
                                <td>
                                    <%= Html.DropDownList("Payment_Mode_ID")%>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Qualified Transfer:
                                </td>
                                <td>
                                    <%= Html.CheckBox("Qualified_Transfer")%>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Non-Qualified 1035 Exchange:
                                </td>
                                <td>
                                    <%= Html.CheckBox("Non_Qualified_1035_Exchange") %>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Replacement:
                                </td>
                                <td>
                                    <%= Html.CheckBox("Replacement") %>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Replacement Details:
                                </td>
                                <td>
                                    <%= Html.TextArea("Replacement_Details", new { style = "width: 250px;", numrows = "2" })%>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Shipping Method Outbound:
                                </td>
                                <td>
                                    <%= Html.DropDownList("Outbound_Shipping_Method_ID")%>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Tracking Number Outbound:
                                </td>
                                <td>
                                    <%= Html.TextBox("Tracking_Number_Outbound")%>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Shipping Method Inbound:
                                </td>
                                <td>
                                    <%= Html.DropDownList("Inbound_Shipping_Method_ID")%>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Tracking Number Inbound:
                                </td>
                                <td>
                                    <%= Html.TextBox("Tracking_Number_Inbound")%>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="label">
                                    Policy Number:
                                </td>
                                <td>
                                    <%= Html.TextBox("Policy_Number",StringHelper.DecryptData(Model.Policy_Number))%>
                                </td>
                                <td class="label">
                                    Status Date:
                                </td>
                                <td>
                                    <%= Html.TextBox("Status_Date", (Model.Status_Date.HasValue ? Model.Status_Date.Value.ToString("MM/dd/yyyy") : ""), new { @style = "width:80px;" })%>
                                </td>
                            </tr>
                            <% foreach (LMS.Data.Plan_Transaction_Date date in Model.Plan_Dates)
                               { %>
                            <tr>
                                <td class="label" style="border: none;">
                                    <%= Html.Encode(date.Transaction_Date_Type.Description)%>:
                                </td>
                                <td style="border: none;">
                                    <script type="text/javascript">
                                        $(document).ready(function () {
                                            /*$('#<%= "DateType" + date.Plan_Transaction_Date_ID.ToString()%>').datepicker();*/
                                            $('#<%= "DateType" + date.Plan_Transaction_Date_ID.ToString()%>').mask("99/99/9999");
                                            $('#<%= "DateType" + date.Plan_Transaction_Date_ID.ToString()%>').datepicker({ showOn: 'button', buttonImage: '../../Content/icons/calendar.gif', buttonImageOnly: true });
                                        });
                                    </script>
                                    <%= Html.TextBox("DateType" + date.Plan_Transaction_Date_ID, (date.Date_Value.HasValue ? date.Date_Value.Value.ToString("MM/dd/yyyy") : ""), new { style = "width: 80px" })%>
                                </td>
                            </tr>
                            <%} %>
                            <tr>
                                <td class="label">
                                    Custom Notes:
                                </td>
                                <td colspan="3">
                                    <%= Html.DropDownList("NoteHelpers")%>
                                    <img src="../../Content/images/page_white_paste.png" alt="Paste" style="border: none;
                                        vertical-align: middle;" onclick="javascript:$('#Special_Instructions').text($('#NoteHelpers :selected').text());" />
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Special Instructions:
                                </td>
                                <td colspan="3">
                                    <%= Html.TextArea("Special_Instructions", new { style = "width: 250px;", numrows = "2" })%>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <input type="image" src="../../Content/icons/check.png" alt="Save" title="Save" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="../../Record/Details/<%= Model.Record_ID %>">
                            <img src="../../content/icons/close.png" alt="Cancel" title="Cancel" style="border: 0px;
                                vertical-align: top;" />
                        </a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="image" src="../../Content/icons/folder_add.png" alt="Save" title="Save And Add New"
                            name="New" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <%} %>
</asp:Content>
