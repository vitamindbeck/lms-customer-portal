<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<LMS.Data.Record_Activity>" %>

<%@ Import Namespace="LMS.Helpers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Edit Activity
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%
        LMS.Data.User user = LMS.Data.Repository.UserRepository.GetUser(User.Identity.Name);
        //Hide Save button for complaints if not the assinged user
        var showButton = true;
        var disableDDLS = 0;
        if (Model.Record_Activity_Status_ID != null)
        {
            if (Model.Record_Activity_Status_ID == 33 || Model.Record_Activity_Status_ID == 34 || Model.Record_Activity_Status_ID == 35)
            {
                if (!LMS.Data.Repository.UserRepository.HasSecurityLevel(user, 13) && Model.User_ID != user.User_ID)
                {
                    showButton = false;
                }

                if (!LMS.Data.Repository.UserRepository.HasSecurityLevel(user, 13) && Model.User_ID == user.User_ID)
                {
                    disableDDLS = 1;
                }
            }
        }
    %>
    <script type="text/javascript">

        var disableDDLSBool = <%=disableDDLS %>;
        $(function () {

            disableDDLS();
            function disableDDLS() {
                if (disableDDLSBool){
                    $("#Follow_User_ID").attr("disabled", true);
                    $("#Follow_User_ID").after('<input type="hidden" name="Follow_User_ID" value="' + $("#Follow_User_ID").val() + '" />');

                    $("#Reconciliation_Type_ID").val('64');
                    $("#Reconciliation_Type_ID").attr("disabled", true);
//                    $("#Reconciliation_Type_ID").after('<input type="hidden" name="Reconciliation_Type_ID" value="' + $("#Reconciliation_Type_ID").val() + '" />');
                }
            };


            $('#copyNote').click(function () {
                var 
                    $NoteTextField = $('#txtNotes'),
                    $NoteList = $('#RecordNotes :selected');

                if ($NoteTextField.val() == "") {
                    $NoteTextField.val($NoteList.text());
                    }
                else {
                    $NoteTextField.val($NoteTextField.val() + ', ' + $NoteList.text());
                }
            });

            

            toggleDDLS();
            $("#Record_Activity_Status_ID").change(function () {
                toggleDDLS();
                statusMenu();
            });

             $("#Activity_Type_ID").change(function () {
                 activityMenu();
             });

            function toggleDDLS() {
                var statusVal = $("#Record_Activity_Status_ID").val();
                if (statusVal == 14 || statusVal == 16 || statusVal == 15) {
                    $("#tr_callCreationDDLS").show();
                    $("#tr_callCreationDDLS2").show();
                    $("#tr_callCreationDDLS3").show();
                }
                else {
                    $("#tr_callCreationDDLS").hide();
                    $("#tr_callCreationDDLS2").hide();
                    $("#tr_callCreationDDLS3").hide();
                }
            }

            if ($("#Reconciliation_Type_ID").val() == 39 || $("#Reconciliation_Type_ID").val() == 40 || $("#Reconciliation_Type_ID").val() == 64) {
                $("#Reconciliation_Type_ID").attr("disabled", "disabled");
            }

            if ($("#Record_Activity_Status_ID").val() == 33 || $("#Record_Activity_Status_ID").val() == 34 || $("#Record_Activity_Status_ID").val() == 35) {
                $("#Record_Activity_Status_ID").attr("disabled", "disabled");
                $("#Record_Activity_Status_ID").after('<input type="hidden" name="Record_Activity_Status_ID" value="' + $("#Record_Activity_Status_ID").val() + '" />');
            }


            if ($("#Reconciliation_Type_ID").val() == 36) {
                $("#DistributionDiv").show();
            }
            else {
                $("#DistributionDiv").hide();
            }
            $("#Reconciliation_Type_ID").change(function () {
                if ($("#Reconciliation_Type_ID").val() == 36) {
                    $("#DistributionDiv").show();
                }
                else {
                    $("#DistributionDiv").hide();
                }
            });
            $("#Due_Date").datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: '1900:2020'
            });
            


            // menus should be enabled in edit with rules still active in menu
            function pageLoad() {
                disableMenuValues('#Record_Activity_Status_ID');
                disableMenuValues('#Reconciliation_Type_ID');
                if ($('#Reconciliation_Type_ID').val() == -1) {
                    $('#Reconciliation_Type_ID').attr("disabled", true);
                }
                getStatusRules();
                getDispositionRules();               
                $('#Record_Activity_Status_ID').attr("disabled", false);
                $('#Campaign_Code').attr("disabled", false);
            }

             pageLoad();

          // gets the available statuses by querying the selected activity value
            function getStatusRules() {
                var rulesUrl = "/Wizzer/GetRulesResult/" + $('#Activity_Type_ID').val() + "/" + 0 + "/" + 0;
                $.ajax({
                    type: "GET",
                    url: rulesUrl,
                    dataType: "json",
                    contentType: "application/json; charset=UTF-8",
                    success: function(data) {
                        var statusList = data.Status;
                        enableMenuValues('#Record_Activity_Status_ID', statusList);
                    },
                    error: function(xhr, textStatus, errorThrown) {
                        alert("ERROR: " + textStatus + ", status: " + xhr.status + "\nError: " + errorThrown);
                    }                   
                 });
            }

            // gets available dispositions by querying the activity and the status values
            function getDispositionRules() {
                var rulesUrl = "/Wizzer/GetRulesResult/" + $('#Activity_Type_ID').val() + "/" + $('#Record_Activity_Status_ID').val() + "/" + 0;
                $.ajax({
                    type: "GET",
                    url: rulesUrl,
                    dataType: "json",
                    contentType: "application/json; charset=UTF-8",
                    success: function (data) {
                        var dispositionList = data.LeadDisposition;
                        enableMenuValues('#Reconciliation_Type_ID', dispositionList);
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        alert("ERROR: " + textStatus + ", status: " + xhr.status + "\nError: " + errorThrown);
                    }
                });
            }

            // get all values from dropdown menu
            function getDropDownValues(menuId) {
                var i = 0;
                var values = [];
                $(menuId + " option").each(function () {
                    i = $(this).val();
                    values.push(i);
                });
                return values;
            }

            // enables values when passed menu ID and array of values
            function enableMenuValues(dropDownMenu, dropDownMenuValues) {
                var i;
                for (i = 0; i < dropDownMenuValues.length; i++) {
                    $(dropDownMenu + " option[value='" + dropDownMenuValues[i] + "']").removeAttr("disabled");
                }
            }

            // disbales all options for a dropdown menu
            function disableMenuValues(menuId) {
                    var i = 0;
                    $(menuId + " option").each(function () {
                        i = $(this).val();
                        $("" + menuId + " option[value='" + i + "']").attr("disabled", "disabled");
                    });
                }

          // resets a drop down menu to its default value
            function resetSelection(dropDownMenu) {
                 $(dropDownMenu).each(function() { this.selectedIndex = 0; });
             }
          
          // changes to happen activity menu changes
          function activityMenu() {
                disableMenuValues('#Record_Activity_Status_ID');
                $('#Record_Activity_Status_ID').attr("disabled", false);
                $('#Reconciliation_Type_ID').attr("disabled", true);
                $('#Campaign_Code').attr("disabled", true);
                $('#RecordNotes').attr("disabled", true);
                resetSelection('#Record_Activity_Status_ID');
                resetSelection('#Reconciliation_Type_ID');                
                getStatusRules();
            }

            var userSecurityLevel = '<%=ViewData["UserSecuritylevel"] %>';

            // changes to happen status menu changes
            function statusMenu() {
                disableMenuValues('#Reconciliation_Type_ID');
                if ($('#Reconciliation_Type_ID').val() != -1 && userSecurityLevel == 2) 
                {
                    $('#Reconciliation_Type_ID').attr("disabled", false);
                    resetSelection('#Reconciliation_Type_ID');
                    getDispositionRules(); 
                } 
                $('#Campaign_Code').attr("disabled", false);
                $('#RecordNotes').attr("disabled", false);               
                enableMenuValues('#Campaign_Code', getDropDownValues('#Campaign_Code'));
            }
           
           function disableInput(element, val) {
                if (val == 1) {
                    $(element).attr('disabled', 'disabled');
                }
                else {
                    $(element).removeAttr('disabled');
                }
            }

            $("#saveBtn").click(function() {
                disableInput('#saveBtn', 1);
                if ($("#Record_Activity_Status_ID :selected").attr("disabled")) {
                    alert("disabled status cannot be selected");
                    disableInput('#saveBtn', 0);
                    return false;
                }
                if ($("#Campaign_Code").val() < 0) {
                    alert("Please select a campaign code.");
                    disableInput('#saveBtn', 0);
                    return false;
                }
                if ($("#Reconciliation_Type_ID :selected").attr("disabled") && $("#Reconciliation_Type_ID").val() != -1) {
                    alert("disabled disposition cannot be selected");
                    disableInput('#saveBtn', 0);
                    return false;
                }
//                if (!$("#Reconciliation_Type_ID").val() > 0) {
//                    alert("Please select a disposition.");
//                    disableInput('#saveBtn', 0);
//                    return false;
//                }
            });
        });
       

        function CheckPlanForValid() {
            alert($('#Plan_Transaction_ID').val());
        }
    </script>
    <div id="ClientFactsDiv" class="client-outer" style="background-color: #e0e4ee;">
        <div class="section-title">
            Edit Activity
        </div>
        <div class="section-inner">
            <% LMS.Data.Record record = (LMS.Data.Record)ViewData["Record"]; %>
            <% using (Html.BeginForm("Edit", "Activity", Model.Record_Activity_ID, FormMethod.Post))
               { %>
            <table class="section">
                <tr>
                    <td class="label" width="200px">
                        Opportunity:
                    </td>
                    <td width="200px">
                        <%= Html.DropDownList("Plan_Transaction_ID")%>
                    </td>
                    <%-- <td class="label" width="100px">
                        Call of Week:
                    </td>
                    <td width="100px">
                        <%= Html.CheckBox("Call_of_Week")  %>
                    </td>--%>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td class="label" width="200px">
                        Follow Set For:
                    </td>
                    <td width="200px">
                    <%= Html.DropDownList("Follow_User_ID")%>
                      <%--  <% if (user.Security_Level_ID == 2)
                           {%>
                        <%= Html.DropDownList("Follow_User_ID")%>
                        <%}
                           else
                           {%>
                        <%=Html.Hidden("Follow_User_ID",user.User_ID) %>
                        <%=Html.Encode(user.First_Name + " " +user.Last_Name) %>
                        <%} %>--%>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td class="label" width="200px">
                        Due Date:
                    </td>
                    <td width="200px">
                        <%= Html.TextBox("Due_Date")%>
                    </td>
                    <%-- <td class="label" id="SR">
                        Self Rating:
                    </td>
                    <td>
                        <%= Html.DropDownList("Agent_Rating") %>
                    </td>--%>
                    <td class="label" id="MR" width="100px">
                        <%   if (user.Security_Level_ID != 1)%>
                        Manager Rating:
                    </td>
                    <td>
                        <% if (user.Security_Level_ID != 1) %>
                        <%= Html.DropDownList("Manager_Rating") %>
                    </td>
                </tr>
                <tr>
                    <td class="label" width="200px">
                        Due Time:
                    </td>
                    <td width="200px">
                        <%= Html.DropDownList("Due_Time")%>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td class="label" width="200px">
                        Activity:
                    </td>
                    <td width="200px">
                        <%= Html.DropDownList("Activity_Type_ID")%>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td class="label" width="200px">
                        Status:
                    </td>
                    <td width="200px">
                        <%=Html.DropDownList("Record_Activity_Status_ID")%>
                       <%-- <%if (Model.Record_Activity_Status_ID == 33 || Model.Record_Activity_Status_ID == 34 || Model.Record_Activity_Status_ID == 35)
                          {%>
                        <%= Html.HiddenFor(m=>m.Record_Activity_Status_ID) %>
                        <%}%>--%>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                   <%-- <td class="label" width="200px">
                        Priority:
                    </td>
                    <td width="200px">
                        <%= Html.DropDownList("Priority_Level")%>
                    </td>--%>
                     <td class="label">
                            Campaign Code:
                        </td>
                        <td>
                            <%= Html.DropDownList("Campaign_Code", (SelectList)ViewData["CampaignDDL"])%>
                        </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td class="label" width="200px">
                    </td>
                    <td width="200px" colspan="2" align="left">
                        <%= Html.DropDownList("RecordNotes")%>
                        <img src="../../Content/images/page_white_paste.png" alt="Paste" style="border: none;
                            vertical-align: middle;" id="copyNote" />
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td class="label" width="200px" valign="top">
                        Notes:
                    </td>
                    <td width="200px" colspan="5" align="left">
                        <%= Html.TextArea("txtNotes", StringHelper.DecryptData(Model.Notes), new { style = "width: 300px;", rows = "2" })%>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <% if (user.Security_Level_ID == 2)
                   {%>
                <tr id="tr_Reconciliation_Type_ID">
                    <td class="label" width="200px" valign="top">
                        Lead Disposition:
                    </td>
                    <td width="200px">
                        <%= Html.DropDownList("Reconciliation_Type_ID")%>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr id="tr_callCreationDDLS" class="hide">
                    <td class="label" width="200px" valign="top">
                        Critical Topic Coverage:
                    </td>
                    <td>
                        <%= Html.DropDownList("ctc_DDL", (SelectList)ViewData["CriticalTopicCoverageDDL"])%>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr id="tr_callCreationDDLS2" class="hide">
                    <td class="label" width="200px" valign="top">
                        Sales Rating:
                    </td>
                    <td>
                        <%= Html.DropDownList("SEEID_DDL", (SelectList)ViewData["SalesRatingDDL"])%>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr id="tr_callCreationDDLS3" class="hide">
                    <td class="label" width="200px" valign="top">
                        Admin Documentation Rating:
                    </td>
                    <td>
                        <%= Html.DropDownList("AdminDocumentationID_DDL", (SelectList)ViewData["AdminDocumentationRatingDDL"])%>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td colspan="5">
                        <div id="DistributionDiv">
                            AEP Reminder:
                            <%=Html.CheckBox("AEP_Reminder",ViewData["AEP_Reminder"]) %>
                        </div>
                    </td>
                </tr>
                <%}%>
                <tr>
                    <td colspan="8" align="center">
                        <%if (showButton)
                          {%>
                        <input type="image" src="../../Content/icons/check.png" alt="Save" title="Save" id="saveBtn" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <%} %>
                        <a href="../../Record/Details/<%= record.Record_ID %>">
                            <img src="../../content/icons/close.png" alt="Cancel" title="Cancel" style="border: 0px;
                                vertical-align: top;" />
                        </a>
                    </td>
                </tr>
            </table>
            <%} %>
        </div>
    </div>
</asp:Content>
