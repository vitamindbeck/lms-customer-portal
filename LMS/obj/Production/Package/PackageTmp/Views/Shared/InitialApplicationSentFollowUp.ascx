﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<LMS.Data.InitialApplicationSentFollowUpResult>>" %>


 <div class="client-outer">
    <div class="section-title">
        [ 
        <% if (Model.Count() > 0)
           { %>
        <%= Html.Encode(Model.Count())%>
        <%}
           else
           { %>
        No
        <%} %>
        Initial Follow Ups ]
    </div>
 
    <div class="client-inner">
    <table class="tablesorter" width="100%" cellpadding="0" cellspacing="0" id="sortable1">
        <thead>
        <tr>
            <th>
                Due Date
            </th>
            <th>
                Name
            </th>
            <th>
                Agent
            </th>
            <th>
                Carrier
            </th>
            <th>
                State
            </th>
            <th>
                Shipping Method
            </th>
        </tr>
        </thead>
        <tbody>
    <% foreach (LMS.Data.InitialApplicationSentFollowUpResult item in Model) 
       {
    %>
        <tr>
            <td>
                <%= Html.Encode(item.Due_Date.Value.ToShortDateString())%>
            </td>
            <td>
                <%= Html.ActionLink(item.Record_Name, "Details", new { id = item.Record_ID }, new { target = "_Blank" })%>
            </td>
            <td>
                <%= Html.Encode(item.User_Name) %>
            </td>
            <td>
               <%= Html.Encode(item.Carrier)%>
            </td>
            <td>
                <%= Html.Encode(item.State) %>
            </td>
            <td>
                <%= Html.Encode(item.Shipping_Method) %>
            </td>
        </tr>

    <% } %>
        </tbody>
    </table>
    </div>
</div>






