<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<LMS.Data.sp_NPN_ListResult>>" %>
<%@ Import Namespace="Helpers" %>
   <div class="client-outer" style="width:480px">
    <div class="client-title">
         [ NPN List ]
    </div>
    <div class="client-inner">
    <table class="sortable" width="100%" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
            <th>Name</th>
            <th>NPN</th>
            </tr>
        </thead>
        <tbody style="color:Black;">
        <% foreach (LMS.Data.sp_NPN_ListResult rs in Model)
           { %>
        <tr>
            <td>
                <%= Html.Encode(rs.Name)%>
            </td>
            <td>
                <%= Html.Encode(rs.NPN) %>
            </td>
        </tr>
        <%} %>
        </tbody>
    </table>
    </div>
</div>
