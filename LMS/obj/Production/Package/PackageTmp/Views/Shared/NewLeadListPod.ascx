﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<LMS.Data.NewLeadsByPodsResult>>" %>
<%@ Import Namespace="Helpers" %>
<html>
<head>
  <script  type="text/javascript">

      $(function() {

          $("#hideDetails").click(function() {


          $("#divLeadDetails").hide();
          $("#divLeadSummary").show(); 
                // $("#hideDetails").attr('disabled', true);
                //$("#showDetails").removeAttr('disabled')


              //return false;
          });
          $("#showDetails").click(function() 
          {
              $("#divLeadDetails").show();
              $("#divLeadSummary").hide();
              //$("#showDetails").attr('disabled', true);
              //$("#hideDetails").removeAttr('disabled');
             
                // return false;
          });

      });

</script>
</head>
<body>
 <div class="client-outer">
    <div class="section-title">
        [ <%= Html.Encode(Model.Count()) %> New Leads ]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp
        <button id="hideDetails">Summary</button> 
       <button id="showDetails">Details</button>
       
  </div>
 </div>
 
   <div id ="divLeadDetails" style="display:none"> 
     <table class="sortable" width="100%" cellpadding="0" cellspacing="0" >
        <thead>
            <tr>
            <th>Lead Name</th>
            <th>Submit Date</th>
            <th>State</th>
            <th>Source</th>
            <th>Eligibility</th>
            </tr>
        </thead>
        <tbody>
        <% foreach (LMS.Data.NewLeadsByPodsResult record in Model)
           { %>
        <tr>
            <td>
            <%if (record.Key != null)
              { %>
                <span style="background-color:Yellow;">
            <%} %>
                <%= Html.ActionLink(record.Name, "Details", new { id = record.Record_ID }, new { target = "_blank", style = "target-new: tab ! important"  })%>
            <%if (record.Key != null)
              { %>
                </span>
            <%} %>   
            </td>
            <td>
                <%= Html.Encode(record.Created_On) %>
            </td>
            <td>
                <%= Html.Encode(record.Abbreviation) %>
            </td>
            <td>
                <%= Html.Encode(record.Source) %>
            </td>
            <td>
                <% if (record.Category == null) Response.Write(""); else Response.Write(Html.Encode(record.Category)); %>
            </td>
        </tr>
        <%} %>
        </tbody>
    </table>

  </div>
  <div id ="divLeadSummary" style="display:none"> 
       <table width="100%" cellpadding="0" cellspacing="0" >
        <thead>
            <tr>
            <th>Agent</th>
            <th>Total leads </th>
           </tr>
        </thead>
        
       
    <%Dictionary<string, int> dict = new Dictionary<string, int>(); %>
       
       
       <% foreach (LMS.Data.NewLeadsByPodsResult user in Model)
           { %>
              
            <%if(dict.Keys.Contains(user.User_Name)){ %>
              
            <%dict[user.User_Name]++; %>
             
         
         
           <% }%>
          <%else{ %>
               <% dict.Add(user.User_Name,1); %>
                 
        <% }  %>
        
        <%} %>
        
        <%foreach (string name in dict.Keys)
        {%>
      
       <tbody>
        <tr>
             <td>
                <%= Html.Encode(name)%>
               </td>
               
              <td>
                <%= Html.Encode(dict[name])%>
              </td>
            </tr>
            
            <%} %>
      
              </tbody>      
        
    </table>
  </div>
</body>
</html>