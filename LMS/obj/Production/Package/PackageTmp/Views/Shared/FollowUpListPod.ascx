﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<LMS.Data.FollowUpsByPodsResult>>" %>
<%@ Import Namespace="LMS.Helpers" %>
<html>
<head>
  <script  type="text/javascript">

    $(function() {

      $("#hiFollUpDetails").click(function()
      {

          $("#divFollUpDetails").hide();
          $("#divFollowUpSummary").show();
         
          //$("#hiFollUpDetails").attr('disabled', true);
          //$("#shFollUpDetails").removeAttr('disabled')
            //return false;
          
      });
      $("#shFollUpDetails").click(function() 
      {
          $("#divFollUpDetails").show();
          $("#divFollowUpSummary").hide();
          //$("#shFollUpDetails").attr('disabled', true);
          //$("#hiFollUpDetails").removeAttr('disabled');
            // return false;
      });

    });

</script>
</head>
<body>
 <div class="client-outer">
    <div class="section-title">
        [
        <% if (Model.Count() > 0)
           { %>
        <%= Model.Count().ToString()%>
        <%}
           else
           { %>
        No
        <%} %>
        Open Activities ]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp
        <button id="hiFollUpDetails">Summary</button> 
       <button id="shFollUpDetails">Details</button>
       
  </div>
 </div>
 
<div id ="divFollUpDetails" style="display:none"> 
        <table class="sortable" width="100%" cellpadding="0" cellspacing="0">
            <thead>
                <tr>                    
                    <th>
                        Lead Name
                    </th>
                    <th>
                        Follow Up Date
                    </th>
                     <th>
                        Time
                    </th>
                    <th style="text-align: center;">
                        State
                    </th>
                    <th>
                        Source
                    </th>
                    <th>                    
                        Status
                    </th>
                     <th>                    
                        Buying Period
                    </th>
                    <th>
                        BP Effective Date
                    </th>
                    <th>
                        Eligibility
                    </th>
                    <th style="text-align: center;">
                        Contacts
                    </th>
                </tr>
            </thead>
            <tbody>
                <% foreach (LMS.Data.FollowUpsByPodsResult record in Model)
                   { %>
                <tr>                   
                    <td nowrap="nowrap">
                    <%if (record.Key != null)
                      { %>
                        <span style="background-color:Yellow;">
                    <%} %>
                        <%= Html.ActionLink(StringHelper.UppercaseFirst(record.Last_Name) + ", " + StringHelper.UppercaseFirst(record.First_Name), "Complete", "Activity", new { id = record.Record_Activity_ID }, new { target = "_blank", style = "target-new: tab ! important" })%>
                    <%if (record.Key != null)
                      { %>
                        </span>
                    <%} %>   
                    </td>
                    <td nowrap="nowrap">
                        
                           <%= Html.Encode(string.Format("{0:d}", record.Due_Date))%>
                    </td>
                    <td>
                        <% if (record.Due_Date.Value.TimeOfDay.Hours > 0)
                           { %>
                                <%= Html.Encode(record.Due_Date.Value.ToShortTimeString())%>
                        <%}
                           else
                           { %>
                            
                        <%} %>
                    </td>
                    <td style="text-align: center;">
                        <%= Html.Encode(record.State) %>
                    </td>
                    <td nowrap="nowrap">
                        <%= Html.Encode(record.SourceName) %>
                    </td>
                    <td>
                        <%= Html.Encode(record.PlanDesc) %>
                    </td>
                     <td nowrap="nowrap">
                        <%= Html.Encode(record.BuyingPeriod) %>
                    </td>
                    <td nowrap="nowrap">
                        <% if (record.Buying_Period_Effective_Date != null)
                           { %>
                        <%= Html.Encode(record.Buying_Period_Effective_Date.Value.ToString("MM/dd/yyyy"))%>
                        <%} %>
                    </td>
                    <td>
                        <%= Html.Encode(record.EligibilityCategory) %>
                    </td>
                    <td style="text-align: center;">
                        <%= Html.Encode("") %>
                    </td>
                </tr>
                <%} %>
            </tbody>
        </table>
</div>
  
      <div id ="divFollowUpSummary" style="display:none"> 
       <table width="100%" cellpadding="0" cellspacing="0" >
        <thead>
            <tr>
            <th>Agent</th>
            <th>Total leads </th>
           </tr>
        </thead>
 
 <%Dictionary<string, int> dictFollowUp = new Dictionary<string, int>(); %>
       
       
       <% foreach (LMS.Data.FollowUpsByPodsResult userFollowUp in Model)
           { %>
              
            <%if (dictFollowUp.Keys.Contains(userFollowUp.User_Name))
              { %>
              
            <%dictFollowUp[userFollowUp.User_Name]++; %>
             
         
         
           <% }%>
          <%else{ %>
               <% dictFollowUp.Add(userFollowUp.User_Name, 1); %>
                 
        <% }  %>
        
        <%} %>
        
        <%foreach (string name1 in dictFollowUp.Keys)
        {%>
      
       <tbody>
        <tr>
             <td>
                <%= Html.Encode(name1)%>
               </td>
               
              <td>
                <%= Html.Encode(dictFollowUp[name1])%>
              </td>
            </tr>
            
            <%} %>
      
              </tbody>      
        
    </table>
  </div>
  
</body>
</html>
