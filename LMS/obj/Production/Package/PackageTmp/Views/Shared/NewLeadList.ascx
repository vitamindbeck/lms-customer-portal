<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<LMS.Data.NewLeadsResult>>" %>
<%@ Import Namespace="Helpers" %>
   <div class="client-outer">
    <div class="client-title">
         [ <%= Html.Encode(Model.Count()) %> New Leads ]
    </div>
    <div class="client-inner">
    <table class="sortable" width="100%" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
            <th>Lead Name</th>
            <th>Submit Date</th>
            <th>State</th>
            <th>Source</th>
            <th>Eligibility</th>
            </tr>
        </thead>
        <tbody>
        <% foreach (LMS.Data.NewLeadsResult record in Model)
           { %>
        <tr>
            <td>
            <%if (record.Key != null)
              { %>
                <span style="background-color:Yellow;">
            <%} %>
                <%= Html.ActionLink(record.Name, "Details", new { id = record.Record_ID }, new { target = "_blank", style = "target-new: tab ! important"  })%>
            <%if (record.Key != null)
              { %>
                </span>
            <%} %>   
            </td>
            <td>
                <%= Html.Encode(record.Created_On) %>
            </td>
            <td>
                <%= Html.Encode(record.Abbreviation) %>
            </td>
            <td>
                <%= Html.Encode(record.Source) %>
            </td>
            <td>
                <% if (record.Category == null) Response.Write(""); else Response.Write(Html.Encode(record.Category)); %>
            </td>
        </tr>
        <%} %>
        </tbody>
    </table>
    </div>
</div>
