<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<LMS.Data.Carrier>" %>
<%@ Import Namespace="LMS.Helpers" %>

<div style="padding: 2px; background-color: #d4e4f6; font-size: 10pt; border: solid 1px #b6bac0;
    border-bottom: none; color: #4a689d">
    <%= Html.Encode(Model.Name) %>
</div>
<div style="border: solid 1px #b6bac0; background-color: White; color: #627495;">
    <table>
        <tr>
            <td>
                Address:
            </td>
            <td width="250px">
                <%= Html.Encode(Model.Address_1) %>
            </td>
            <td>
                Active:
            </td>
            <td width="250px">
                <%= Html.Encode(Model.Active ? "Yes" : "No") %>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td width="250px">
                <%= Html.Encode(Model.Address_2) %>
            </td>
            <td>
                Service Level:
            </td>
            <td width="250px">
                <%= Html.Encode(Model.Service_Level) %>
            </td>
        </tr>
        <tr>
            <td>
                City:
            </td>
            <td width="250px">
                <%= Html.Encode(Model.City + ", " + (Model.State.Abbreviation ?? "") + " " + Model.Zip) %>
            </td>
            <td>
                Service Level Desc:
            </td>
            <td width="250px">
                <%= Html.Encode(Model.Service_Level_Description) %>
            </td>
        </tr>
    </table>
    <table style="border: solid 1px #b6bac0; margin: 20px; width: 800px; position: relative;" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th style="width: 500px;">
                    Plan Name
                </th>
                <th>
                    Active
                </th>
                <th>
                    About
                </th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <% 
                int i = 0;
                foreach (LMS.Data.Plan plan in Model.Plans)
               {
                   i++;%>
            <tr>
                <td>
                    <%= Html.Encode(plan.Name) %><%= Html.Hidden(plan.Plan_ID.ToString()) %>
                </td>
                <td>
                    <%= Html.Encode(plan.Active ? "Yes" : "No") %>
                </td>
                <td>
                    <%= Html.Encode(plan.About) %>
                </td>
                <td>
                    <%= Html.ImageLink("Documents", "Plan", new { id = plan.Plan_ID }, "../Content/icons/folders-stack.png", "Manage Documents", null, new { style = "border:0px;" })%>
                </td>
            </tr>
            <%} %>
        </tbody>
    </table>
    
</div>
