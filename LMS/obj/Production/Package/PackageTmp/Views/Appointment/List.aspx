<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Licensing.Master"
    Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Appointments
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("#sortable1").tablesorter();
            $("#sortable2").tablesorter();
        }
    );

        $(function () {
            $('#LicenseList').load()
            {
                if ($("#chkStateWise").is(":checked")) {
                    $("#DivState").show();
                    $("#DivStateResult").show();
                    $("#DivAgent").hide();
                    $("#DivAgentResult").hide();
                }
                else {
                    $("#DivAgent").show();
                    $("#DivAgentResult").show();
                    $("#DivState").hide();
                    $("#DivStateResult").hide();
                }
            };

            $("#chkStateWise").click(function () {
                if ($("#chkStateWise").is(":checked")) {
                    $("#DivState").show();
                    $("#DivStateResult").show();
                    $("#DivAgent").hide();
                    $("#DivAgentResult").hide();
                }
                else {
                    $("#DivAgent").show();
                    $("#DivAgentResult").show();
                    $("#DivState").hide();
                    $("#DivStateResult").hide();
                }
            });

        });

    </script>
    <% using (Html.BeginForm("List", "Appointment", FormMethod.Post, new { id = "AppointmentList" }))
       {%>
    <table width="100%" border="0">
        <tr>
            <td width="500px">
                <% LMS.Data.User user = LMS.Data.Repository.UserRepository.GetUser(Page.User.Identity.Name); %>
                <% if (!LMS.Data.Repository.UserRepository.HasSecurityLevel(user, 1))
                   {%>
                <div id="DivAgent">
                    Select Agent/Agency:
                    <%= Html.DropDownList("Agent", (SelectList)ViewData["Agent_List"], new { tabindex = "1" })%>
                </div>
                <div id="DivState">
                    <p>
                        Select State:
                        <%= Html.DropDownList("State", (SelectList)ViewData["State_List"], new { tabindex = "1" })%></p>
                    <p>
                        Select Carrier:
                        <%= Html.DropDownList("Carrier", (SelectList)ViewData["Carrier_List"], new { tabindex = "2" })%></p>
                    <input type="submit" value="Get Results" id="btnSubmit" />
                </div>
                <%} %>
                <%if (LMS.Data.Repository.UserRepository.HasSecurityLevel(user, 10))
                  {%>
                <%=Html.ActionLink("Add New Appointment", "New", new { id = Int32.Parse(Request.Form["Agent"] ?? "1") })%>
                <%} %>
            </td>
            <td valign="top">
                <% if (!LMS.Data.Repository.UserRepository.HasSecurityLevel(user, 1)) {%>
                    View Information State/Carrier Wise:
                    <%=Html.CheckBox("chkStateWise", (Request.Form["chkStateWise"] ?? string.Empty).Contains("true"))%>
                <%} %>

            </td>
            <td valign="top">
            <% if (!LMS.Data.Repository.UserRepository.HasSecurityLevel(user, 1))
               {%>
                <a href="/Appointment/State" title="State Details" class="statedetails">State Details</a>
                <br />
                <a href="/Appointment/SSN" title="Agent SSN" class="statedetails">Agent SSN</a>
                <%}%>
            </td>
            <td align="right" valign="top">
                <% LMS.Data.LMSDataContext db = new LMS.Data.LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["WEBContextConnectionString"].ConnectionString);
                   List<LMS.Data.sp_Agent_ListResult> rs = db.sp_Agent_List().ToList();
                   string agentid = rs[0].Agent_ID.ToString();

                   LMS.Data.User usr = LMS.Data.Repository.UserRepository.GetUser(Page.User.Identity.Name); %>
                <%if (LMS.Data.Repository.UserRepository.HasSecurityLevel(usr, 10))
                  {%>
                <%=Html.ActionLink("Export to Excel", "Export", new { AgentID = Int32.Parse(Request.Form["Agent"] ?? agentid), Chk = Request.Form["chkStateWise"] ?? "", StateID = Int32.Parse(Request.Form["State"] ?? "3"), CarrierID = Int32.Parse(Request.Form["Carrier"] ?? "271") })%>
                <%} %>
            </td>
        </tr>
    </table>
    <div id="DivAgentResult">
        <% Html.RenderPartial("AppointmentListByAgent", ViewData["AppointmentListByAgent"]); %>
    </div>
    <div id="DivStateResult">
        <% Html.RenderPartial("AppointmentListByStateCarrier", ViewData["AppointmentListByStateCarrier"]); %>
    </div>
    <% } %>
    <script type="text/javascript">
        $("#Agent").change(function () {
            $("#AppointmentList").submit();
        });

        $("#chkStateWise").click(function () {
            $("#AppointmentList").submit();
        });
        $('.statedetails').popupWindow({
            centerBrowser: 1,
            scrollbars: 1
        }); 
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ScriptContent" runat="server">
</asp:Content>
