<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<LMS.Data.Source_Code>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Source Code Details
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<script type="text/javascript">
    $(function() {

        $("#tabs").tabs();


    });
</script>
<div style="text-align:right"><a href="/SuperAdmin/SourceCodeEdit/<%=Model.Source_Code_ID %>">Edit</a></div>
<div id="tabs" style="font-size: 1em; font-family: Calibri;">
        <ul class="ui-tabs-nav">
            <li><a href="#tabs-1">Source Code Details</a>
                </li>
            <li><a href="#tabs-2">Web Post</a></li>
        </ul>
        <div id="tabs-1" style="background-color: #e0e4ee;">
            <table class="section" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="label">
                        Source Number:
                    </td>
                    <td>
                        <%=Html.Encode(Model.Source_Number)%>
                        
                    </td>
                    <td class="label">
                        Source Name:
                    </td>
                    <td>
                        <%=Html.Encode(Model.Description)%>
 
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Group:
                    </td>
                    <td>
                        <%=Html.Encode(Model.Group.Description) %>
                    </td>
                    <td class="label">
                        Affinity Partner:
                    </td>
                    <td>
                        <%=Html.Encode(Model.Affinity_Partner.Name) %>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Launch Date:
                    </td>
                    <td>
                        <%=Html.Encode(Model.Launch_Date.ToString())%>
                    </td>
                    <td class="label">
                        
                    </td>
                    <td>
                        
                    </td>
                </tr>
                <tr>
                    <td class="label">LTC:</td>
                    <td>
                        <%= (Model.LTC ? "Yes" : "No")%>
                    </td>
                    <td class="label"> Active:</td>
                    <td><%= (Model.Active ? "Yes" : "No")%></td>
                </tr>

            </table>
        </div>
        <div id="tabs-2" style="background-color: #e0e4ee;">
            <table class="section" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="label" width="200px">
                        This source code has a web post link:
                    </td>
                    <td>
                    <%= (Boolean.Parse(ViewData["Web"].ToString()) ? "Yes" : "No")%>
                    </td>
                </tr>
            </table>
            <div id="WebDiv">
            <table class="section" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="label">
                        Web Name (must be unique):
                    </td>
                    <td>
                    
                        https://leads.longevityalliance.com/API/Submit/
                        <%= Html.Encode(ViewData["WebName"])%>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Key: 
                    </td>
                    <td>
                        <%= Html.Encode(ViewData["Code"])%>
                        
                    </td>
                </tr>
                
             </table>
            </div>
        </div>
    </div>


</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptContent" runat="server">
</asp:Content>
