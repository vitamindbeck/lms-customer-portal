<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	User
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<table id="list" class="scroll" cellpadding="0" cellspacing="0"></table>
<div id="pager" class="scroll" style="text-align:center;"></div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
<link rel="stylesheet" type="text/css" href="../../Scripts/themes/coffee/grid.css"
  title="coffee" media="screen" />

 <script src="../../Scripts/jquery-1.3.2.js" type="text/javascript"></script>
 <script src="../../Scripts/jquery.jqGrid.js" type="text/javascript"></script>
 <script src="../../Scripts/js/jqModal.js" type="text/javascript"></script>
 <script src="../../Scripts/js/jqDnR.js" type="text/javascript"></script>
 <script src="../../Scripts/js/grid.inlinedit.js" type="text/javascript"></script>
<script src="../../Scripts/js/grid.base.js" type="text/javascript"></script>


</asp:Content>
