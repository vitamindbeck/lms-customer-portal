<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Manage Existing User
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  
 
    <script type="text/javascript">


        function ClearQuickSearchList() {
            $('#User_ID')[0].selectedIndex = 0;
           
        }

      
       function SetQuickSearchCookie() {
           if ($("#User_ID")[0].selectedIndex > 0) {
                       $.cookie('User_ID', $("#User_ID")[0].selectedIndex);
            }

           }
            

        $(function() {
            $(".section-title").click(function() {
                $(this).next(".section-inner").slideToggle(300)
                return false;
            });

            

            function RunLoading() {
                $("#resultDiv").slideUp();

            }

            function UnRunLoading() {
                $("#resultDiv").slideDown();
            }

           

        });
        $(function() {
        $("#tabs").tabs(
            {
                cookie: {
                     //store cookie for a day, without, it would be a session cookie
                    expires: 1
                }
            }
            );
        });

        function deleteComplete() {

            var answer = alert("Record has been deleted.");
           
            
            window.location.href = 'ManageUser'
              //  setTimeout("location.reload(true);",1000);
           
         
        }
       
</script>

<%= Html.ValidationSummary("Unable to Search.") %>
    <% using (Html.BeginForm())
       {%>
       <div style="text-align:right"><a href="/SuperAdmin/CreateUser">Create New User</a></div>
       <div id="ClientFactsDiv" class="client-outer" style="background-color: #e0e4ee;">
       
        <div class="section-title">
           Manage User Account
        </div>
        <div class="section-inner">
          <table class="section" cellpadding="0" cellspacing="0">
                <tr>
                    <td align = "center" style="font-weight: bold">
                       Please select the user name:                 
                    <%= Html.DropDownList("User_ID", (SelectList)ViewData["User_ID"])%>
                    <%= Html.ValidationMessage("User_ID", "*")%>   
                         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="submit" value="U" name="btnSearch" class="displaySearchImage" style="color: #e0e4ee;" title ="Search" /> <%--onclick="SetQuickSearchCookie()"/>--%>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <img id ="Img1" alt="Clear Quick Search" src="../../Content/icons/cancel.png" onclick="ClearQuickSearchList()" title="Clear Search" />
                          <a href="/SuperAdmin/Index" alt="Cancel">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;
                        <img src="../../content/icons/close.png" alt="Cancel" title="Cancel" style="border: 0px;
                        vertical-align: top;" /></a>
                   </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
              </table>
    
<% if (ViewData["UserSearchData"] != null)
  { %>
                
    <table class="sortable" width="100%" cellpadding="0" cellspacing="0"
    style="border: solid 1px #304f9f; background-color: #FFFFFF;">
     
             <thead>
                <tr>
                   <th>
                   </th>
                   <th>
                   </th>
                   <th>
                       First Name
                   </th>
                   <th>
                        Last Name
                   </th>
                   <th>
                        Email
                   </th>
              </tr>
            </thead>
            <tbody>
             <% foreach (var item in (IEnumerable<LMS.Data.User>)ViewData["UserSearchData"])
                { %>
                 <tr>
                    <td>
                         <%=Ajax.ActionLink("Delete", "Delete", new { id = item.User_ID }, new AjaxOptions { Confirm = "Do you want to delete this user?", OnComplete = "deleteComplete", HttpMethod = "POST" })%>
                    </td>
                  <td>
                         <%=Html.ActionLink("Edit User", "EditUser", new {id = item.User_ID })%>
                    </td>
          
                    <td>
                        <%= Html.Encode(item.First_Name)%>
                    </td>
          
                    <td class="style1">
                        <%= Html.Encode(item.Last_Name)%>
                    </td>
            
                    <td>
                        <%= Html.Encode(item.Email_Address)%>
                    </td>
                </tr>
              <% } %>
            </tbody>
        </table>
    <table>
     <tr>
      <td style="color: #990000; font-size: small; font-weight: bold;">
         <%= Html.Encode(ViewData["Message"]) %>
      </td>
     </tr>
    </table>
 <%} %>
</div>
</div>
   
   <%} %>

</asp:Content>
