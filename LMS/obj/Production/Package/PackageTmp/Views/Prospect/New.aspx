﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<LMS.Data.Record>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    New Prospect Data
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="ui-tabs-panel ui-widget-content ui-corner-bottom" style="background-color: #e0e4ee;">
        <%using (Html.BeginForm())
          {%>
        <table cellpadding="0" cellspacing="0" class="section" border="0">
            <tr>
                <td colspan="2" align="center">
                    <b>Prospect Data</b>
                </td>
                <td colspan="2" align="center">
                    <b>Current Situation</b>
                </td>
                <td colspan="2" align="center">
                    <b>Current Healthcare Providers</b>
                </td>
            </tr>
            <tr>
                <td class="label">
                    LeadID
                </td>
                <td>
                    <%= Html.TextBox("LeadID", Model.Web_Lead_ID, new { style = "width: 100px;", tabindex = "23" })%>
                </td>
                <td class="label">
                    Current Situation:
                </td>
                <td>
                    <%= Html.DropDownList("Record_Current_Situation_ID", (SelectList)ViewData["Record_Current_Situation_ID"], new { tabindex = "44" })%>
                </td>
                <td class="label">
                    Current Doctor
                </td>
                <td>
                    <%= Html.TextBox("Current_Doctor")%>
                </td>
            </tr>
            <tr>
                <td class="label">
                    Name
                </td>
                <td>
                    <%= Html.TextBox("First_Name", Model.First_Name, new { style = "width: 80px", tabindex = "3", onblur="initialCap(this);" })%>
                    <%= Html.ValidationMessage("First_Name", "*")%>
                    <%= Html.TextBox("Middle_Initial", Model.Middle_Initial, new { style = "width: 10px", tabindex = "4", onblur = "initialCap(this);" })%>
                    <%= Html.TextBox("Last_Name", Model.Last_Name, new { style = "width: 80px", tabindex = "5", onblur = "initialCap(this);" })%>
                    <%= Html.ValidationMessage("Last_Name", "*")%>
                </td>
                <td class="label">
                    Requested Start Date:
                </td>
                <td>
                    <%= Html.TextBox("RequestedStartDate",Model.RequestedStartDate.HasValue==true ? Model.RequestedStartDate.Value.ToString("MM/dd/yyyy"):"", new {style = "width: 80px", tabindex = "46"}) %>
                </td>
                <td class="label">
                    Speclilist
                </td>
                <td>
                    <%= Html.TextBox("Speclilist")%>
                </td>
            </tr>
            <tr>
                <td class="label">
                    Address
                </td>
                <td>
                    <%= Html.TextBox("Address_1", Model.Address_1, new { style = "width: 200px", tabindex="8" })%>
                </td>
                <td class="label">
                    Willing to change plans or insures
                </td>
                <td>
                    <%= Html.DropDownList("change_plans", (SelectList)ViewData["change_plans"], new { tabindex = "44" })%>
                </td>
                <td class="label">
                    Hospital(s) of Choice
                </td>
                <td>
                    <%= Html.TextBox("Hospital_Choice")%>
                </td>
            </tr>
            <tr>
                <td class="label">
                </td>
                <td>
                    <%= Html.TextBox("Address_2", Model.Address_2, new { style = "width: 200px", tabindex = "9" })%>
                </td>
                <td colspan="2" align="center">
                    <b>Current Health Coverage Information</b>
                </td>
                <td class="label">
                    Pharmacy of Choice
                </td>
                <td>
                    <%= Html.TextBox("Pharmacy_Choice")%>
                </td>
            </tr>
            <tr>
                <td class="label">
                    City, State Zip
                </td>
                <td>
                    <%= Html.TextBox("City", Model.City, new { style = "width: 80px", tabindex = "10", onblur = "initialCap(this);" })%>
                    ,<%= Html.DropDownList("State_ID", (SelectList)ViewData["State_ID"], new { tabindex = "11" })%>
                    <%= Html.TextBox("Zipcode", Model.Zipcode, new { style = "width: 60px", tabindex = "12" })%>
                </td>
                <td class="label">
                    Medical Carrier:
                </td>
                <td>
                    <%= Html.TextBox("Medical_Carrier", Model.Medical_Carrier, new { tabindex = "67" })%>
                </td>
                <td class="label">
                    Medicare.gov ID#
                </td>
                <td>
                    <%= Html.TextBox("Medicare_Number",LMS.Helpers.StringHelper.DecryptData(Model.Medicare_Number), new { tabindex = "51" })%>
                </td>
            </tr>
            <tr>
                <td class="label">
                    EmailAddress
                </td>
                <td>
                    <%= Html.TextBox("Email_Address", Model.Email_Address, new { tabindex = "27" })%>
                </td>
                <td class="label">
                    Current Plan Type:
                </td>
                <td>
                    <%= Html.DropDownList("Current_Insured_Type_ID", (SelectList)ViewData["Current_Insured_Type_ID"], new {style = "width: 125px", tabindex = "74" })%>
                </td>
                <td class="label">
                    Medicare Date
                </td>
                <td>
                    <%= Html.TextBox("Medicare_Date")%>
                </td>
            </tr>
            <tr>
                <td class="label">
                    Home Phone
                </td>
                <td>
                    <%= Html.TextBox("Home_Phone", Model.Home_Phone, new { style="width: 100px;", tabindex = "23" })%>
                    <%= Html.RadioButton("prefferedPhone", Model, new { style="width: 100px;", tabindex = "26" }) %>
                </td>
                <td class="label">
                    Premium:
                </td>
                <td>
                    <%= Html.TextBox("Premium")%>
                </td>
                <td colspan="2" align="center">
                    <b>Financial Situation</b>
                </td>
            </tr>
            <tr>
                <td class="label">
                    Work Phone
                </td>
                <td>
                    <%= Html.TextBox("Work_Phone", Model.Work_Phone, new { style = "width: 100px;", tabindex = "24" })%>
                    <%= Html.RadioButton("prefferedPhone", Model, new { style="width: 100px;", tabindex = "26" }) %>
                </td>
                <td class="label">
                    Drug Coverage
                </td>
                <td>
                    <%= Html.DropDownList("Drug_Coverage", (SelectList)ViewData["Drug_Coverage"], new { tabindex = "44" })%>
                </td>
                <td class="label">
                    Total Annual healthcare cost
                </td>
                <td>
                    <%= Html.TextBox("Total_Annual_healthcare_cost")%>
                </td>
            </tr>
            <tr>
                <td class="label">
                    Cell Phone
                </td>
                <td>
                    <%= Html.TextBox("Cell_Phone", Model.Cell_Phone, new { style = "width: 100px;", tabindex = "25" })%>
                    <%= Html.RadioButton("prefferedPhone", Model, new { style="width: 100px;", tabindex = "26" }) %>
                </td>
                <td class="label">
                    Indemnity
                </td>
                <td>
                    <%= Html.DropDownList("Indemnity", (SelectList)ViewData["Indemnity"], new { tabindex = "44" })%>
                </td>
                <td class="label">
                    Household Income
                </td>
                <td>
                    <%= Html.TextBox("Household_Income")%>
                </td>
            </tr>
            <tr>
                <td class="label">
                    DOB
                </td>
                <td>
                    <%= Html.TextBox("DOB", Model.DOB, new { style = "width: 100px;", tabindex = "23" })%>
                </td>
                <td class="label">
                    Long Term Care
                </td>
                <td>
                    <%= Html.DropDownList("Long_Term_Care", (SelectList)ViewData["Long_Term_Care"], new { tabindex = "44" })%>&nbsp;Benefit:<%= Html.TextBox("Benefit_Amount_Long_Term_Care")%>
                </td>
                <td class="label">
                    Eligible for PACE
                </td>
                <td>
                    <%= Html.DropDownList("Eligible_PACE", (SelectList)ViewData["Eligible_PACE"], new { tabindex = "" })%>
                </td>
            </tr>
            <tr>
                <td class="label">
                    Gender
                </td>
                <td>
                    <%= Html.TextBox("Sex", Model.Sex, new { style = "width: 100px;", tabindex = "23" })%>
                </td>
                <td class="label">
                    Critical Illness
                </td>
                <td>
                    <%= Html.DropDownList("Critical_Illness", (SelectList)ViewData["Critical_Illness"], new { tabindex = "44" })%>&nbsp;Benefit:<%= Html.TextBox("Benefit_Amount_Critical_Illness")%>
                </td>
                <td class="label">
                    Eligible for LIS
                </td>
                <td>
                    <%= Html.DropDownList("Eligible_LIS", (SelectList)ViewData["Eligible_LIS"], new { tabindex = "" })%>
                </td>
            </tr>
            <tr>
                <td class="label">
                    MaritalStatus
                </td>
                <td>
                    <%= Html.DropDownList("Marital_Status_ID", (SelectList)ViewData["Marital_Status_ID"], new { style = "width: 130px", tabindex = "61" })%>
                </td>
                <td class="label">
                    Plan Likes / Dislikes
                </td>
                <td>
                    <%= Html.TextBox("Plan_Likes_Dislikes")%>
                </td>
                <td class="label">
                    Eligible for Medicaid
                </td>
                <td>
                    <%= Html.DropDownList("Eligible_Medicaid", (SelectList)ViewData["Eligible_Medicaid"], new { tabindex = "" })%>
                </td>
            </tr>
            <tr>
                <td class="label">
                    Notes
                </td>
                <td>
                    <%= Html.TextBox("Notes", Model.Notes, new { style = "width: 100px;", tabindex = "23" })%>
                </td>
                <td class="label">
                    Changes (if you could)
                </td>
                <td>
                    <%= Html.TextBox("Plan_Changes")%>
                </td>
                <td class="label">
                    Tavel - National, International, Snowbird?
                </td>
                <td>
                    <%= Html.TextBox("Travel")%>
                </td>
            </tr>
            <tr>
                <td class="label">
                    BestTime to call
                </td>
                <td>
                    Monday:<%= Html.CheckBox("Monday", Model) %>&nbsp; Tuesday:<%= Html.CheckBox("Tuesday", Model)%>&nbsp;
                    Wednesday:<%= Html.CheckBox("Wednesday", Model)%>&nbsp; Thursday:<%= Html.CheckBox("Thursday", Model)%>&nbsp;
                    Friday:<%= Html.CheckBox("Friday", Model)%>&nbsp; Saturday:<%= Html.CheckBox("Saturday", Model)%>&nbsp;
                    Sunday:<%= Html.CheckBox("Sunday", Model)%>
                    <br />
                    TIME:
                    <%= Html.DropDownList("best_time_to_call")%>
                </td>
                <td colspan="4" align="right">
                    <div style="padding-top: 5px; padding-left: 5px;">
                        <table style="border: 1px solid black;">
                            <tr>
                                <td>
                                    Eligibility 2013 (PA)
                                </td>
                                <td>
                                    Single
                                </td>
                                <td>
                                    Married
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    PACE Program
                                </td>
                                <td>
                                    $14,500
                                </td>
                                <td>
                                    $17,700
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    PACENET
                                </td>
                                <td>
                                    $14,500-$23,500
                                </td>
                                <td>
                                    $17.700-$31,500
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    LIS
                                </td>
                                <td>
                                    Full $15,079.50/Partial $22,695
                                </td>
                                <td>
                                    Full $20,425.50 / Partial $22,695.00
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Medicaid
                                </td>
                                <td>
                                    $8,416.80
                                </td>
                                <td>
                                    $12,656.40
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="label">
                    POA Contact Name
                </td>
                <td>
                    <%= Html.TextBox("POA_Contact_Name", new { style = "width: 80px", tabindex = "3", onblur = "initialCap(this);" })%>
                </td>
            </tr>
            <tr>
                <td class="label">
                    POA Phone
                </td>
                <td>
                    <%= Html.TextBox("POA_Phone", new { style = "width: 100px;", tabindex = "24" })%>
                </td>
            </tr>
        </table>
        <button type="submit" class="btn btn-default" id="btnSubmit">
            Submit</button>
        <%}%>
    </div>
    <div style="text-align: center;">
        <input type="image" id="Image1" alt="Save" title="Save" src="../../Content/icons/check.png">
        <a alt="Cancel" href="../../Record/Search">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <img style="border: 0px; vertical-align: top;" title="Cancel" alt="Cancel" src="../../content/icons/close.png">
        </a>
    </div>
    <script language="javascript" type="text/jscript">
        $(function () {
            $("#DOB").mask("99/99/9999");
            $("#Home_Phone").mask("(999) 999-9999");
            $("#Cell_Phone").mask("(999) 999-9999");
            $("#Work_Phone").mask("(999) 999-9999");
            $("#Sex").val("<%= Model.Sex %>");
            $("#State_ID").val("<%= Model.State_ID %>");
            $("#RequestedStartDate").mask("99/99/9999");
            $("#POA_Phone").mask("(999) 999-9999");


        });
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ScriptContent" runat="server">
</asp:Content>
