<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<LMS.Data.Carrier>>" %>

<%@ Import Namespace="LMS.Helpers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Index
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <script type="text/javascript">
        
        $(document).ready(function() {

            function formatItem(row) {
                return row[0] + " (<strong>id: " + row[1] + "</strong>)";
            }

            function formatResult(row) {
                return row[0].replace(/(<.+?>)/gi, '');
            }

            $('#CarrierName').autocomplete('<%=Url.Action("Search", "Carrier") %>', {
                dataType: 'json',
                parse: function(data) {
                    var rows = new Array();
                    for (var i = 0; i < data.length; i++) {
                        rows[i] = { data: data[i] };
                    }
                    return rows;
                },
                formatItem: function(row, i, n) {
                    return row.Name + ' - ' + row.City;
                },
                width: 400,
                highlight: false,
                multiple: false,
                autofill: true,
                multipleSeparator: ",",
                matchContains: false,
                max: 50
            }).result(function(evt, data, formatted) {
                $('#id').val(data.Carrier_ID);
                $('#CarrierName').val(data.Name);
            }); ;
        });
    </script>

    <% using (Ajax.BeginForm("Details",
                    new AjaxOptions{ UpdateTargetId = "carrierDetails"}))  { %>
            Lookup Carrier: <%= Html.TextBox("CarrierName", null, new { style = "width: 200px;" })%>
            <input type="hidden" name="id" id="id" />
            <input type="submit" value="Show" />
    <%} %>
    <br />
    <div id="carrierDetails">
    </div>
</asp:Content>
