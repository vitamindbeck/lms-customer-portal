<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<LMS.Data.Carrier>" %>
<%@ Import Namespace="LMS.Helpers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Details
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table>
        <tr>
            <td class="label">
                Name:
            </td>
            <td>
                <%= Html.Encode(Model.Name) %>
            </td>
        </tr>
        <tr>
            <td class="label">
                Address 1:
            </td>
            <td>
                <%= Html.Encode(Model.Address_1) %>
            </td>
        </tr>
        <tr>
            <td class="label">
            </td>
            <td>
                <%= Html.Encode(Model.Address_2) %>
            </td>
        </tr>
        <tr>
            <td class="label">
                City:
            </td>
            <td>
                <%= Html.Encode(Model.City) %>
            </td>
        </tr>
        <tr>
            <td class="label">
                State:
            </td>
            <td>
                <%= Html.Encode(Model.State.Abbreviation) %>
            </td>
        </tr>
        <tr>
            <td class="label">
                Zip:
            </td>
            <td>
                <%= Html.Encode(Model.Zip) %>
            </td>
        </tr>
        <tr>
            <td class="label">
                Active:
            </td>
            <td>
                <% if (Model.Active)
                   { %>
                Yes
                <% }
                   else
                   { %>
                No
                <%} %>
            </td>
        </tr>
        <tr>
            <td class="label">
                About:
            </td>
            <td>
                <%= Html.Encode(Model.About_Carrier) %>
            </td>
        </tr>
        <tr>
            <td class="label">
                Need Upload:
            </td>
            <td>
                <% if (Model.Need_Upload)
                   { %>
                Yes
                <% }
                   else
                   { %>
                No
                <%} %>
            </td>
        </tr>
        <tr>
            <td class="label">
                Service Level:
            </td>
            <td>
                <%= Html.Encode(String.Format("{0:F}", Model.Service_Level)) %>
            </td>
        </tr>
        <tr>
            <td class="label">
                Service Level Description:
            </td>
            <td>
                <%= Html.Encode(Model.Service_Level_Description) %>
            </td>
        </tr>
        <tr>
            <td class="label">
                Assets:
            </td>
            <td>
                <%= Html.Encode(String.Format("{0:c}", Model.Assets)) %>
            </td>
        </tr>
        <tr>
            <td class="label">
                Code:
            </td>
            <td>
                <%= Html.Encode(Model.Code) %>
            </td>
        </tr>
        <tr>
            <td class="label">
                CompuComp:
            </td>
            <td>
                <%= Html.Encode(Model.CompuComp) %>
            </td>
        </tr>
        <tr>
            <td class="label">
            </td>
            <td>
                <%=Html.ActionLink("Edit", "Edit", new { id=Model.Carrier_ID }) %>
                |
                <%=Html.ActionLink("Back to List", "Index") %>
            </td>
        </tr>
    </table>
    <table cellspacing="0" cellpadding="10" style="border: solid 1px Black;">
        <thead>
            <tr>
                <th></th>
                <th style="padding: 10px">
                    Plan Name
                </th>
                <th style="padding: 10px">
                    Active
                </th>
            </tr>
        </thead>
        <tbody>
            <% foreach (LMS.Data.Plan plan in Model.Plans)
               { %>
            <tr >
                <td>
                    <%= Html.ImageLink("Detail", "Plan", new { id = plan.Plan_ID }, "../../Content/images/folder-open.png", "About", null, new { style = "border:0px;" })%>
                </td>
                <td style="padding: 10px; min-width: 200px">
                    <%= Html.Encode(plan.Name) %>
                </td>
                <td style="padding: 10px">
                    <%= Html.Encode((plan.Active ? "Yes" : "No")) %>
                </td>
            </tr>
            <%} %>
        </tbody>
    </table>
</asp:Content>
