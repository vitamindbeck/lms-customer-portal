﻿function IsValidSelect() {

    var today = new Date();
    var dob = Date.parse($("#DOB").val());
    var Date_dob = new Date(dob);
    var Date_dob_d = Date_dob.getDate();
    var Date_dob_m = Date_dob.getMonth();
    var Date_dob_y = Date_dob.getFullYear();

    if (Date_dob_d == 1) {
        if (Date_dob_m > 1) {
            Date_dob = new Date(Date.parse(Date_dob_m - 1 + "/" + Date_dob_d + "/" + Date_dob_y));
        }
        else {
            Date_dob = new Date(Date.parse("12/" + Date_dob_d + "/" + (Date_dob_y - 1)));
        }
    }
    var diff_date = today - Date_dob;
    var num_years = Math.floor(diff_date / 31536000000);
    var num_months = Math.floor((diff_date % 31536000000) / 2628000000);
    var total_months = num_years * 12 + num_months;

    if (($("#Record_Current_Situation_ID").val() == "11" || $("#Record_Current_Situation_ID").val() == "19") && ($("#Products_Of_Interest_ID").val() >= 7 && $("#Products_Of_Interest_ID").val() <= 14)) {
        alert("This product of interest is not allowed for the situation selected.");
        return false;
    }
    else if ($("#Record_Current_Situation_ID").val() == "20" && ($("#Products_Of_Interest_ID").val() == "2" || $("#Products_Of_Interest_ID").val() == "5") && total_months < 777) {



        alert("This product of interest is not allowed for the situation selected and for under age 64 and 9 month.");
        return false;
    }
    else {
        return true;
    }

}