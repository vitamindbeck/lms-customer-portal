﻿using System;
using System.Web.Mvc;
using System.Data.Linq;
using System.Collections;
using System.IO;
using System.Web.UI.WebControls;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Drawing;
using System.Collections.Generic;
using LMS.Data;

namespace LMS.CustomActionResults
{
    public class LeadDeacyResult : ActionResult
    {
        private string _fileName;
        private Hashtable _Params;
        private List<string> _headers = new List<string>();
        private IEnumerable<LeadDecayDetail> _Report;
        private TableStyle _tableStyle;
        private TableItemStyle _headerStyle;
        private TableItemStyle _itemStyle;
        private int _Total;

        public string FileName
        {
            get { return _fileName; }
        }

       




        public LeadDeacyResult(IEnumerable<LeadDecayDetail> report, Hashtable Params, string fileName)
            : this(report, Params, fileName, null, null, null)
        {
        }


        public LeadDeacyResult(IEnumerable<LeadDecayDetail> report, Hashtable Params, string fileName, TableStyle tableStyle, TableItemStyle headerStyle, TableItemStyle itemStyle)
        {
            _Report = report;
            _Params = Params;
            _fileName = fileName;
            _tableStyle = tableStyle;
            _headerStyle = headerStyle;
            _itemStyle = itemStyle;

            // provide defaults
            if (_tableStyle == null)
            {
                _tableStyle = new TableStyle();
                _tableStyle.BackColor = Color.Transparent;
            }
            if (_headerStyle == null)
            {
                _headerStyle = new TableItemStyle();
                _headerStyle.BackColor = Color.FromArgb(192,229,251);
            }
        }

        public override void ExecuteResult(ControllerContext context)
        {
            // Create HtmlTextWriter
            StringWriter sw = new StringWriter();
            HtmlTextWriter tw = new HtmlTextWriter(sw);


            tw.RenderBeginTag(HtmlTextWriterTag.Table);
            
            tw.RenderBeginTag(HtmlTextWriterTag.Tr);
            tw.RenderBeginTag(HtmlTextWriterTag.Td);
            tw.Write(HttpUtility.HtmlEncode("Start Date:"));
            tw.RenderEndTag();
            tw.RenderBeginTag(HtmlTextWriterTag.Td);
            tw.Write(HttpUtility.HtmlEncode(Convert.ToDateTime(_Params["StartDate"]).ToShortDateString()));
            tw.RenderEndTag();

            tw.RenderBeginTag(HtmlTextWriterTag.Tr);
            tw.RenderBeginTag(HtmlTextWriterTag.Td);
            tw.Write(HttpUtility.HtmlEncode("End Date:"));
            tw.RenderEndTag();
            tw.RenderBeginTag(HtmlTextWriterTag.Td);
            tw.Write(HttpUtility.HtmlEncode(Convert.ToDateTime(_Params["EndDate"]).ToShortDateString()));
            tw.RenderEndTag();
            tw.RenderEndTag();

            tw.RenderBeginTag(HtmlTextWriterTag.Tr);
            tw.RenderBeginTag(HtmlTextWriterTag.Td);
            tw.Write(HttpUtility.HtmlEncode("Source Code:"));
            tw.RenderEndTag();
            tw.RenderBeginTag(HtmlTextWriterTag.Td);
            if ( _Params["Source_Code_ID"] != null)
                tw.Write(HttpUtility.HtmlEncode(_Params["Source_Code_ID"].ToString()));
            tw.RenderEndTag();
            tw.RenderEndTag();

            tw.RenderBeginTag(HtmlTextWriterTag.Tr);
            tw.RenderBeginTag(HtmlTextWriterTag.Td);
            tw.Write(HttpUtility.HtmlEncode("Group ID:"));
            tw.RenderEndTag();
            tw.RenderBeginTag(HtmlTextWriterTag.Td);
            if (Convert.ToInt32(_Params["Group_ID"]) > 0)
                tw.Write(HttpUtility.HtmlEncode(_Params["Group_ID"].ToString()));
            tw.RenderEndTag();
            tw.RenderEndTag();

            tw.RenderBeginTag(HtmlTextWriterTag.Tr);
            tw.RenderBeginTag(HtmlTextWriterTag.Td);
            tw.Write(HttpUtility.HtmlEncode("Agent ID:"));
            tw.RenderEndTag();
            tw.RenderBeginTag(HtmlTextWriterTag.Td);
            if (Convert.ToInt32(_Params["Agent_ID"]) > 0)
                tw.Write(HttpUtility.HtmlEncode(_Params["Agent_ID"].ToString()));
            tw.RenderEndTag();

            tw.RenderEndTag();
            tw.RenderEndTag();

            
            // Build HTML Table from Items
            if (_tableStyle != null)
                _tableStyle.AddAttributesToRender(tw);
            tw.RenderBeginTag(HtmlTextWriterTag.Table);

            // Generate headers from table
            _headers.Add("Customer");
            _headers.Add("Status");
            _headers.Add("Source Code");
            _headers.Add("Agent");
            _headers.Add("# of Attempted Contacts");
            _headers.Add("Minutes To Contact");
            _headers.Add("Application Sent");
            _headers.Add("Application Received");
            _headers.Add("Application Submitted");


            // Create Header Row
            tw.RenderBeginTag(HtmlTextWriterTag.Thead);
            foreach (String header in _headers)
            {
                if (_headerStyle != null)
                    _headerStyle.AddAttributesToRender(tw);
                tw.RenderBeginTag(HtmlTextWriterTag.Th);
                tw.Write(header);
                tw.RenderEndTag();
            }
            tw.RenderEndTag();

            //Create Data Table
            tw.RenderBeginTag(HtmlTextWriterTag.Tbody);

            foreach(LeadDecayDetail lead in _Report)
            { 
                //Render Number Contact Made
                tw.RenderBeginTag(HtmlTextWriterTag.Tr);

                tw.RenderBeginTag(HtmlTextWriterTag.Td);
                tw.Write(HttpUtility.HtmlEncode(lead.Customer));
                tw.RenderEndTag();

                tw.RenderBeginTag(HtmlTextWriterTag.Td);
                tw.Write(HttpUtility.HtmlEncode(lead.Status));
                tw.RenderEndTag();

                tw.RenderBeginTag(HtmlTextWriterTag.Td);
                tw.Write(HttpUtility.HtmlEncode(lead.Source_Name));
                tw.RenderEndTag();

                tw.RenderBeginTag(HtmlTextWriterTag.Td);
                tw.Write(HttpUtility.HtmlEncode(lead.Agent));
                tw.RenderEndTag();

                tw.RenderBeginTag(HtmlTextWriterTag.Td);
                tw.Write(HttpUtility.HtmlEncode(lead.NumberOfAttemptedContacts.ToString()));
                tw.RenderEndTag();

                tw.RenderBeginTag(HtmlTextWriterTag.Td);
                tw.Write(HttpUtility.HtmlEncode(lead.MinutesToContact.ToString()));
                tw.RenderEndTag();

                tw.RenderBeginTag(HtmlTextWriterTag.Td);
                tw.Write(HttpUtility.HtmlEncode(lead.Apps_Sent.ToString()));
                tw.RenderEndTag();

                tw.RenderBeginTag(HtmlTextWriterTag.Td);
                tw.Write(HttpUtility.HtmlEncode(lead.Apps_Received.ToString()));
                tw.RenderEndTag();

                tw.RenderBeginTag(HtmlTextWriterTag.Td);
                tw.Write(HttpUtility.HtmlEncode(lead.Apps_Submitted.ToString()));
                tw.RenderEndTag();

                tw.RenderEndTag(); //tr
            }

            

           
            tw.RenderEndTag(); // tbody

            tw.RenderEndTag(); // table

            WriteFile(_fileName, "application/ms-excel", sw.ToString());
        }


        private static string ReplaceSpecialCharacters(string value)
        {
            value = value.Replace("’", "'");
            value = value.Replace("“", "\"");
            value = value.Replace("”", "\"");
            value = value.Replace("–", "-");
            value = value.Replace("…", "...");
            return value;
        }

        private static void WriteFile(string fileName, string contentType, string content)
        {
            HttpContext context = HttpContext.Current;
            context.Response.Clear();
            context.Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
            context.Response.Charset = "";
            context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            context.Response.ContentType = contentType;
            context.Response.Write(content);
            context.Response.End();
        }

        
    }
}
