﻿using System;
using System.Web.Mvc;
using System.Data.Linq;
using System.Collections;
using System.IO;
using System.Web.UI.WebControls;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Drawing;
using System.Collections.Generic;
using LMS.Data;

namespace LMS.CustomActionResults
{
    public class AdvancedLeadDeacyResult : ActionResult
    {
        private string _fileName;
        private List<string> _headers = new List<string>();
        private Dictionary<string, Section> _Report;
        private TableStyle _tableStyle;
        private TableItemStyle _headerStyle;
        private TableItemStyle _itemStyle;
        private int _Total;

        public string FileName
        {
            get { return _fileName; }
        }

        public AdvancedLeadDeacyResult(Dictionary<string, Section> report, string fileName)
            : this(report, fileName, null, null, null)
        {
        }

        public AdvancedLeadDeacyResult(Dictionary<string, Section> report, string fileName, TableStyle tableStyle, TableItemStyle headerStyle, TableItemStyle itemStyle)
        {
            _Report = report;
            //_Params = Params;
            _fileName = fileName;
            _tableStyle = tableStyle;
            _headerStyle = headerStyle;
            _itemStyle = itemStyle;

            // provide defaults
            if (_tableStyle == null)
            {
                _tableStyle = new TableStyle();
                _tableStyle.BackColor = Color.Transparent;
            }
            if (_headerStyle == null)
            {
                _headerStyle = new TableItemStyle();
                _headerStyle.BackColor = Color.FromArgb(192, 229, 251);
            }
        }

        public override void ExecuteResult(ControllerContext context)
        {
            StringWriter sw = new StringWriter();

            sw.Write(",Att,Cnt,Att,Cnt,Att,Cnt,Att,Cnt,Att,Cnt,Att,Cnt,Att,Cnt,Att,Cnt,Att,Cnt,Att,Cnt,Att,Cnt,Att,Cnt"+ System.Environment.NewLine);
            sw.Write("Att/Contact Within,5 Minutes,,15 Minutes,,30 Minutes,,1 Hour,,3 Hours,,6 Hours,,12 Hours,,24 Hours,,48 Hours,,96 Hours,,1 Week,,1 Week +," + System.Environment.NewLine);
            sw.Write(string.Format("Att/Contact Number,{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23}" + System.Environment.NewLine,
                _Report["5min"].NumberAttempted,
                _Report["5min"].NumberContact,
                _Report["15min"].NumberAttempted,
                _Report["15min"].NumberContact,
                _Report["30min"].NumberAttempted,
                _Report["30min"].NumberContact,
                _Report["1hr"].NumberAttempted,
                _Report["1hr"].NumberContact,
                _Report["3hrs"].NumberAttempted,
                _Report["3hrs"].NumberContact,
                _Report["6hrs"].NumberAttempted,
                _Report["6hrs"].NumberContact,
                _Report["12hrs"].NumberAttempted,
                _Report["12hrs"].NumberContact,
                _Report["24hrs"].NumberAttempted,
                _Report["24hrs"].NumberContact,
                _Report["48hrs"].NumberAttempted,
                _Report["48hrs"].NumberContact,
                _Report["96hrs"].NumberAttempted,
                _Report["96hrs"].NumberContact,
                _Report["1wk"].NumberAttempted,
                _Report["1wk"].NumberContact,
                _Report["1wk+"].NumberAttempted,
                _Report["1wk+"].NumberContact
                ));
            sw.Write(string.Format("Att/Contact Percent,{0:P},{1:P},{2:P},{3:P},{4:P},{5:P},{6:P},{7:P},{8:P},{9:P},{10:P},{11:P},{12:P},{13:P},{14:P},{15:P},{16:P},{17:P},{18:P},{19:P},{20:P},{21:P},{22:P},{23:P}" + System.Environment.NewLine,
                (double)_Report["5min"].NumberAttempted/(double)_Report["5min"].LeadTotal,
                (double)_Report["5min"].NumberContact / (double)_Report["5min"].LeadTotal,
                (double)_Report["15min"].NumberAttempted / (double)_Report["5min"].LeadTotal,
                (double)_Report["15min"].NumberContact / (double)_Report["5min"].LeadTotal,
                (double)_Report["30min"].NumberAttempted / (double)_Report["5min"].LeadTotal,
                (double)_Report["30min"].NumberContact / (double)_Report["5min"].LeadTotal,
                (double)_Report["1hr"].NumberAttempted / (double)_Report["5min"].LeadTotal,
                (double)_Report["1hr"].NumberContact / (double)_Report["5min"].LeadTotal,
                (double)_Report["3hrs"].NumberAttempted / (double)_Report["5min"].LeadTotal,
                (double)_Report["3hrs"].NumberContact / (double)_Report["5min"].LeadTotal,
                (double)_Report["6hrs"].NumberAttempted / (double)_Report["5min"].LeadTotal,
                (double)_Report["6hrs"].NumberContact / (double)_Report["5min"].LeadTotal,
                (double)_Report["12hrs"].NumberAttempted / (double)_Report["5min"].LeadTotal,
                (double)_Report["12hrs"].NumberContact / (double)_Report["5min"].LeadTotal,
                (double)_Report["24hrs"].NumberAttempted / (double)_Report["5min"].LeadTotal,
                (double)_Report["24hrs"].NumberContact / (double)_Report["5min"].LeadTotal,
                (double)_Report["48hrs"].NumberAttempted / (double)_Report["5min"].LeadTotal,
                (double)_Report["48hrs"].NumberContact / (double)_Report["5min"].LeadTotal,
                (double)_Report["96hrs"].NumberAttempted / (double)_Report["5min"].LeadTotal,
                (double)_Report["96hrs"].NumberContact / (double)_Report["5min"].LeadTotal,
                (double)_Report["1wk"].NumberAttempted / (double)_Report["5min"].LeadTotal,
                (double)_Report["1wk"].NumberContact / (double)_Report["5min"].LeadTotal,
                (double)_Report["1wk+"].NumberAttempted / (double)_Report["5min"].LeadTotal,
                (double)_Report["1wk+"].NumberContact / (double)_Report["5min"].LeadTotal
                ));
            WriteFile(_fileName, "text/plain", sw.ToString());
        }

        //public override void ExecuteResult(ControllerContext context)
        //{
        //    // Create HtmlTextWriter
        //    StringWriter sw = new StringWriter();
        //    HtmlTextWriter tw = new HtmlTextWriter(sw);

        //    // Build HTML Table from Items
        //    if (_tableStyle != null)
        //        _tableStyle.AddAttributesToRender(tw);


        //    tw.AddStyleAttribute(HtmlTextWriterStyle.BorderStyle, "Solid");
        //    tw.AddStyleAttribute(HtmlTextWriterStyle.BorderWidth, "1px");
        //    tw.AddStyleAttribute(HtmlTextWriterStyle.BorderColor, "Black");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Table);


        //    //Create Data Table
        //    tw.RenderBeginTag(HtmlTextWriterTag.Tbody);


        //    //Render Number Contact Made
        //    tw.RenderBeginTag(HtmlTextWriterTag.Tr);

        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode(""));
        //    tw.RenderEndTag();

        //    tw.AddStyleAttribute(HtmlTextWriterStyle.Width,"40px");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode("Att"));
        //    tw.RenderEndTag();

        //    tw.AddAttribute(HtmlTextWriterAttribute.Width, "40px");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode("Cntct"));
        //    tw.RenderEndTag();

        //    tw.AddAttribute(HtmlTextWriterAttribute.Width, "40px");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode("Att"));
        //    tw.RenderEndTag();

        //    tw.AddAttribute(HtmlTextWriterAttribute.Width, "40px");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode("Cntct"));
        //    tw.RenderEndTag();

        //    tw.AddAttribute(HtmlTextWriterAttribute.Width, "40px");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode("Att"));
        //    tw.RenderEndTag();

        //    tw.AddAttribute(HtmlTextWriterAttribute.Width, "40px");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode("Cntct"));
        //    tw.RenderEndTag();

        //    tw.AddAttribute(HtmlTextWriterAttribute.Width, "40px");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode("Att"));
        //    tw.RenderEndTag();

        //    tw.AddAttribute(HtmlTextWriterAttribute.Width, "40px");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode("Cntct"));
        //    tw.RenderEndTag();

        //    tw.AddAttribute(HtmlTextWriterAttribute.Width, "40px");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode("Att"));
        //    tw.RenderEndTag();

        //    tw.AddAttribute(HtmlTextWriterAttribute.Width, "40px");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode("Cntct"));
        //    tw.RenderEndTag();

        //    tw.AddAttribute(HtmlTextWriterAttribute.Width, "40px");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode("Att"));
        //    tw.RenderEndTag();

        //    tw.AddAttribute(HtmlTextWriterAttribute.Width, "40px");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode("Cntct"));
        //    tw.RenderEndTag();

        //    tw.AddAttribute(HtmlTextWriterAttribute.Width, "40px");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode("Att"));
        //    tw.RenderEndTag();

        //    tw.AddAttribute(HtmlTextWriterAttribute.Width, "40px");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode("Cntct"));
        //    tw.RenderEndTag();

        //    tw.AddAttribute(HtmlTextWriterAttribute.Width, "40px");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode("Att"));
        //    tw.RenderEndTag();

        //    tw.AddAttribute(HtmlTextWriterAttribute.Width, "40px");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode("Cntct"));
        //    tw.RenderEndTag();

        //    tw.AddAttribute(HtmlTextWriterAttribute.Width, "40px");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode("Att"));
        //    tw.RenderEndTag();

        //    tw.AddAttribute(HtmlTextWriterAttribute.Width, "40px");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode("Cntct"));
        //    tw.RenderEndTag();

        //    tw.AddAttribute(HtmlTextWriterAttribute.Width, "40px");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode("Att"));
        //    tw.RenderEndTag();

        //    tw.AddAttribute(HtmlTextWriterAttribute.Width, "40px");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode("Cntct"));
        //    tw.RenderEndTag();

        //    tw.AddAttribute(HtmlTextWriterAttribute.Width, "40px");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode("Att"));
        //    tw.RenderEndTag();

        //    tw.AddAttribute(HtmlTextWriterAttribute.Width, "40px");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode("Cntct"));
        //    tw.RenderEndTag();

        //    tw.AddAttribute(HtmlTextWriterAttribute.Width, "40px");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode("Att"));
        //    tw.RenderEndTag();

        //    tw.AddAttribute(HtmlTextWriterAttribute.Width, "40px");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode("Cntct"));
        //    tw.RenderEndTag();




        //    tw.RenderEndTag(); //tr

        //    //Render Number Contact Made
        //    tw.RenderBeginTag(HtmlTextWriterTag.Tr);

        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode(""));
        //    tw.RenderEndTag();

        //    tw.AddAttribute(HtmlTextWriterAttribute.Colspan, "2");
        //    tw.AddStyleAttribute(HtmlTextWriterStyle.BorderStyle, "Solid");
        //    tw.AddStyleAttribute(HtmlTextWriterStyle.BorderWidth, "2px");
        //    tw.AddStyleAttribute(HtmlTextWriterStyle.BorderColor, "Black");
        //    tw.AddAttribute(HtmlTextWriterAttribute.Bgcolor, "#f8f6b8");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode("5 Min"));
        //    tw.RenderEndTag();

        //    tw.AddAttribute(HtmlTextWriterAttribute.Colspan, "2");
        //    tw.AddStyleAttribute(HtmlTextWriterStyle.BorderStyle, "Solid");
        //    tw.AddStyleAttribute(HtmlTextWriterStyle.BorderWidth, "2px");
        //    tw.AddStyleAttribute(HtmlTextWriterStyle.BorderColor, "Black");
        //    tw.AddAttribute(HtmlTextWriterAttribute.Bgcolor, "#f8f6b8");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode("15 Min"));
        //    tw.RenderEndTag();

        //    tw.AddAttribute(HtmlTextWriterAttribute.Colspan, "2");
        //    tw.AddStyleAttribute(HtmlTextWriterStyle.BorderStyle, "Solid");
        //    tw.AddStyleAttribute(HtmlTextWriterStyle.BorderWidth, "2px");
        //    tw.AddStyleAttribute(HtmlTextWriterStyle.BorderColor, "Black");
        //    tw.AddAttribute(HtmlTextWriterAttribute.Bgcolor, "#f8f6b8");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode("30 Min"));
        //    tw.RenderEndTag();

        //    tw.AddAttribute(HtmlTextWriterAttribute.Colspan, "2");
        //    tw.AddStyleAttribute(HtmlTextWriterStyle.BorderStyle, "Solid");
        //    tw.AddStyleAttribute(HtmlTextWriterStyle.BorderWidth, "2px");
        //    tw.AddStyleAttribute(HtmlTextWriterStyle.BorderColor, "Black");
        //    tw.AddAttribute(HtmlTextWriterAttribute.Bgcolor, "#f8f6b8");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode("1 hr"));
        //    tw.RenderEndTag();

        //    tw.AddAttribute(HtmlTextWriterAttribute.Colspan, "2");
        //    tw.AddStyleAttribute(HtmlTextWriterStyle.BorderStyle, "Solid");
        //    tw.AddStyleAttribute(HtmlTextWriterStyle.BorderWidth, "2px");
        //    tw.AddStyleAttribute(HtmlTextWriterStyle.BorderColor, "Black");
        //    tw.AddAttribute(HtmlTextWriterAttribute.Bgcolor, "#f8f6b8");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode("3 hr"));
        //    tw.RenderEndTag();

        //    tw.AddAttribute(HtmlTextWriterAttribute.Colspan, "2");
        //    tw.AddStyleAttribute(HtmlTextWriterStyle.BorderStyle, "Solid");
        //    tw.AddStyleAttribute(HtmlTextWriterStyle.BorderWidth, "2px");
        //    tw.AddStyleAttribute(HtmlTextWriterStyle.BorderColor, "Black");
        //    tw.AddAttribute(HtmlTextWriterAttribute.Bgcolor, "#f8f6b8");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode("6 hr"));
        //    tw.RenderEndTag();

        //    tw.AddAttribute(HtmlTextWriterAttribute.Colspan, "2");
        //    tw.AddStyleAttribute(HtmlTextWriterStyle.BorderStyle, "Solid");
        //    tw.AddStyleAttribute(HtmlTextWriterStyle.BorderWidth, "2px");
        //    tw.AddStyleAttribute(HtmlTextWriterStyle.BorderColor, "Black");
        //    tw.AddAttribute(HtmlTextWriterAttribute.Bgcolor, "#f8f6b8");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode("12 hr"));
        //    tw.RenderEndTag();

        //    tw.AddAttribute(HtmlTextWriterAttribute.Colspan, "2");
        //    tw.AddStyleAttribute(HtmlTextWriterStyle.BorderStyle, "Solid");
        //    tw.AddStyleAttribute(HtmlTextWriterStyle.BorderWidth, "2px");
        //    tw.AddStyleAttribute(HtmlTextWriterStyle.BorderColor, "Black");
        //    tw.AddAttribute(HtmlTextWriterAttribute.Bgcolor, "#f8f6b8");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode("24 hr"));
        //    tw.RenderEndTag();

        //    tw.AddAttribute(HtmlTextWriterAttribute.Colspan, "2");
        //    tw.AddAttribute(HtmlTextWriterAttribute.Border, "2px");
        //    tw.AddAttribute(HtmlTextWriterAttribute.Bordercolor, "Black");
        //    tw.AddAttribute(HtmlTextWriterAttribute.Bgcolor, "#f8f6b8");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode("48 hr"));
        //    tw.RenderEndTag();

        //    tw.AddAttribute(HtmlTextWriterAttribute.Colspan, "2");
        //    tw.AddStyleAttribute(HtmlTextWriterStyle.BorderStyle, "Solid");
        //    tw.AddStyleAttribute(HtmlTextWriterStyle.BorderWidth, "2px");
        //    tw.AddStyleAttribute(HtmlTextWriterStyle.BorderColor, "Black");
        //    tw.AddAttribute(HtmlTextWriterAttribute.Bgcolor, "#f8f6b8");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode("96 hr"));
        //    tw.RenderEndTag();

        //    tw.AddAttribute(HtmlTextWriterAttribute.Colspan, "2");
        //    tw.AddStyleAttribute(HtmlTextWriterStyle.BorderStyle, "Solid");
        //    tw.AddStyleAttribute(HtmlTextWriterStyle.BorderWidth, "2px");
        //    tw.AddStyleAttribute(HtmlTextWriterStyle.BorderColor, "Black");
        //    tw.AddAttribute(HtmlTextWriterAttribute.Bgcolor, "#f8f6b8");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode("1 wk"));
        //    tw.RenderEndTag();

        //    tw.AddAttribute(HtmlTextWriterAttribute.Colspan, "2");
        //    tw.AddStyleAttribute(HtmlTextWriterStyle.BorderStyle, "Solid");
        //    tw.AddStyleAttribute(HtmlTextWriterStyle.BorderWidth, "2px");
        //    tw.AddStyleAttribute(HtmlTextWriterStyle.BorderColor, "Black");
        //    tw.AddAttribute(HtmlTextWriterAttribute.Bgcolor, "#f8f6b8");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode("1 wk +"));
        //    tw.RenderEndTag();

            
        //    tw.RenderEndTag(); //tr

        //    //Render Number Contact Made
        //    tw.RenderBeginTag(HtmlTextWriterTag.Tr);

        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode("Att/Contacts Made"));
        //    tw.RenderEndTag();

        //    tw.AddStyleAttribute(HtmlTextWriterStyle.Width, "40px");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode(_Report["5min"].NumberAttempted.ToString()));
        //    tw.RenderEndTag();

        //    tw.AddAttribute(HtmlTextWriterAttribute.Width, "40px");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode(_Report["5min"].NumberContact.ToString()));
        //    tw.RenderEndTag();

        //    tw.AddStyleAttribute(HtmlTextWriterStyle.Width, "40px");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode(_Report["15min"].NumberAttempted.ToString()));
        //    tw.RenderEndTag();

        //    tw.AddAttribute(HtmlTextWriterAttribute.Width, "40px");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode(_Report["15min"].NumberContact.ToString()));
        //    tw.RenderEndTag();

        //    tw.AddStyleAttribute(HtmlTextWriterStyle.Width, "40px");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode(_Report["30min"].NumberAttempted.ToString()));
        //    tw.RenderEndTag();

        //    tw.AddAttribute(HtmlTextWriterAttribute.Width, "40px");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode(_Report["30min"].NumberContact.ToString()));
        //    tw.RenderEndTag();

        //    tw.AddStyleAttribute(HtmlTextWriterStyle.Width, "40px");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode(_Report["1hr"].NumberAttempted.ToString()));
        //    tw.RenderEndTag();

        //    tw.AddAttribute(HtmlTextWriterAttribute.Width, "40px");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode(_Report["1hr"].NumberContact.ToString()));
        //    tw.RenderEndTag();

        //    tw.AddStyleAttribute(HtmlTextWriterStyle.Width, "40px");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode(_Report["3hrs"].NumberAttempted.ToString()));
        //    tw.RenderEndTag();

        //    tw.AddAttribute(HtmlTextWriterAttribute.Width, "40px");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode(_Report["3hrs"].NumberContact.ToString()));
        //    tw.RenderEndTag();

        //    tw.AddStyleAttribute(HtmlTextWriterStyle.Width, "40px");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode(_Report["6hrs"].NumberAttempted.ToString()));
        //    tw.RenderEndTag();

        //    tw.AddAttribute(HtmlTextWriterAttribute.Width, "40px");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode(_Report["6hrs"].NumberContact.ToString()));
        //    tw.RenderEndTag();


        //    tw.AddStyleAttribute(HtmlTextWriterStyle.Width, "40px");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode(_Report["12hrs"].NumberAttempted.ToString()));
        //    tw.RenderEndTag();

        //    tw.AddAttribute(HtmlTextWriterAttribute.Width, "40px");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode(_Report["12hrs"].NumberContact.ToString()));
        //    tw.RenderEndTag();

        //    tw.AddStyleAttribute(HtmlTextWriterStyle.Width, "40px");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode(_Report["24hrs"].NumberAttempted.ToString()));
        //    tw.RenderEndTag();

        //    tw.AddAttribute(HtmlTextWriterAttribute.Width, "40px");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode(_Report["24hrs"].NumberContact.ToString()));
        //    tw.RenderEndTag();

        //    tw.AddStyleAttribute(HtmlTextWriterStyle.Width, "40px");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode(_Report["48hrs"].NumberAttempted.ToString()));
        //    tw.RenderEndTag();

        //    tw.AddAttribute(HtmlTextWriterAttribute.Width, "40px");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode(_Report["48hrs"].NumberContact.ToString()));
        //    tw.RenderEndTag();

        //    tw.AddStyleAttribute(HtmlTextWriterStyle.Width, "40px");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode(_Report["96hrs"].NumberAttempted.ToString()));
        //    tw.RenderEndTag();

        //    tw.AddAttribute(HtmlTextWriterAttribute.Width, "40px");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode(_Report["96hrs"].NumberContact.ToString()));
        //    tw.RenderEndTag();

        //    tw.AddStyleAttribute(HtmlTextWriterStyle.Width, "40px");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode(_Report["1wk"].NumberAttempted.ToString()));
        //    tw.RenderEndTag();

        //    tw.AddAttribute(HtmlTextWriterAttribute.Width, "40px");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode(_Report["1wk"].NumberContact.ToString()));
        //    tw.RenderEndTag();

        //    tw.AddStyleAttribute(HtmlTextWriterStyle.Width, "40px");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode(_Report["1wk+"].NumberAttempted.ToString()));
        //    tw.RenderEndTag();

        //    tw.AddAttribute(HtmlTextWriterAttribute.Width, "40px");
        //    tw.RenderBeginTag(HtmlTextWriterTag.Td);
        //    tw.Write(HttpUtility.HtmlEncode(_Report["1wk+"].NumberContact.ToString()));
        //    tw.RenderEndTag();




        //    tw.RenderEndTag(); //tr

        //    tw.RenderEndTag(); // tbody

        //    tw.RenderEndTag(); // table

        //    WriteFile(_fileName, "text/plain", sw.ToString());
        //}

        private static string ReplaceSpecialCharacters(string value)
        {
            value = value.Replace("’", "'");
            value = value.Replace("“", "\"");
            value = value.Replace("”", "\"");
            value = value.Replace("–", "-");
            value = value.Replace("…", "...");
            return value;
        }

        private static void WriteFile(string fileName, string contentType, string content)
        {
            HttpContext context = HttpContext.Current;
            context.Response.Clear();
            context.Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
            context.Response.Charset = "";
            context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            context.Response.ContentType = contentType;
            context.Response.Write(content);
            context.Response.End();
        }
    }
}
