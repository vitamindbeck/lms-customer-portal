﻿using System;
using System.Web.Mvc;
using System.Data.Linq;
using System.Collections;
using System.Web.UI.WebControls;
using System.Linq;
using System.Collections.Generic;
using LMS.Data;

namespace LMS.CustomActionResults
{
    public static class LeadDecayControllerExtensions
    {

        public static ActionResult LeadDecayActionResult
        (
            this Controller controller,
            IEnumerable<LeadDecayDetail> report,
            Hashtable table,
            string fileName
        )
        {
            return new LeadDeacyResult(report, table, fileName, null, null, null);
        }

    }
}
