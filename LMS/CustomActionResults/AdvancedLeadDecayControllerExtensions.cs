﻿using System;
using System.Web.Mvc;
using System.Data.Linq;
using System.Collections;
using System.Web.UI.WebControls;
using System.Linq;
using System.Collections.Generic;
using LMS.Data;

namespace LMS.CustomActionResults
{
    public static class AdvancedLeadDecayControllerExtensions
    {
        public static ActionResult AdvancedLeadDeacyResult
        (
            this Controller controller,
            Dictionary<string, Section> report,
            string fileName
        )
        {
            return new AdvancedLeadDeacyResult(report,fileName, null, null, null);
        }
    }
}
