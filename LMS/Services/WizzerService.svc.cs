﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WizzerService.svc.cs" company="Longevity Alliance">
//   Longevity Alliance Inc 2014
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace LMS.Services
{
    using System;
    using System.Collections.Generic;

    using LMS.Models;

    using log4net;

    using StructureMap;

    using WizzerLib.Implementations;
    using WizzerLib.Interfaces;
    using WizzerLib.Models;

    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "WizzerService" in code, svc and config file together.
    /// <summary>
    ///     The wizzer service.
    /// </summary>
    public class WizzerService : IWizzerService
    {
        #region Fields

        /// <summary>
        /// The logger.
        /// </summary>
        private readonly ILog logger = LogManager.GetLogger(typeof(WizzerService).FullName);

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="WizzerService" /> class.
        /// </summary>
        public WizzerService()
            : this(
                ObjectFactory.GetAllInstances<IRecordActivityRules>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="WizzerService"/> class.
        /// </summary>
        /// <param name="recordActivityRules">
        /// The record Activity Rules.
        /// </param>
        public WizzerService(
            IEnumerable<IRecordActivityRules> recordActivityRules)
        {
            this.RecordActivityRules = recordActivityRules;
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Gets or sets the activity rules.
        /// </summary>
        internal IEnumerable<IRecordActivityRules> RecordActivityRules { get; set; }

        #endregion

        #region Public Methods and Operators

        public Response GetFollowUp(Request request)
        {
            var response = new Response();
            try
            {
                var selectedItems = new SelectedItems
                {
                    Activity = request.Activity,
                    Status = request.Status,
                    LeadDisposition = request.LeadDisposition
                };

                var recordActivityRules = new RecordActivityRules();
                var getfollowUp = recordActivityRules.GetFollowUp(selectedItems);
                response.FollowUp = getfollowUp.FollowUp;
                response.FollowUpType = getfollowUp.FollowUpType;

            }
            catch (Exception ex)
            {
                this.logger.Fatal(ex);
                response.Success = false;
                response.Error = ex.Message;
            }
            return response;
        }

        /// <summary>
        /// The validate.
        /// </summary>
        /// <param name="request">
        /// The request.
        /// </param>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        public Response Validate(Request request)
        {
            var response = new Response();
            try
            {
                var selectedItems = new SelectedItems
                                        {
                                            Activity = request.Activity,
                                            Status = request.Status,
                                            LeadDisposition = request.LeadDisposition
                                        };

                var rules = new RecordActivityRules();
                var allowedItems = rules.GetRules(selectedItems);
                response.Activity = allowedItems.Activity;
                response.LeadDisposition = allowedItems.LeadDisposition;
                response.Status = allowedItems.Status;
                response.Success = true;
            }
            catch (Exception ex)
            {
                this.logger.Fatal(ex);
                response.Success = false;
                response.Error = ex.Message;
            }

            return response;
        }


        #endregion

    }
}