﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IWizzerService.cs" company="Longevity Allaince">
//   Longevity Allaince 2014(c), All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace LMS.Services
{
    using System.ServiceModel;
    using System.ServiceModel.Web;

    using LMS.Models;

    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IWizzerService" in both code and config file together.
    /// <summary>
    /// The WizzerService interface.
    /// </summary>
    [ServiceContract]
    public interface IWizzerService
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get follow up.
        /// </summary>
        /// <param name="request">
        /// The request.
        /// </param>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        [WebInvoke(
            Method = "GET", 
            UriTemplate = "/GetFollowUp", 
            ResponseFormat = WebMessageFormat.Json, 
            RequestFormat = WebMessageFormat.Json, 
            BodyStyle = WebMessageBodyStyle.Bare)]
        [OperationContract]
        Response GetFollowUp(Request request);

        /// <summary>
        /// The validate.
        /// </summary>
        /// <param name="request">
        /// The request.
        /// </param>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        [WebInvoke(Method = "GET", 
            UriTemplate = "/Validate", 
            ResponseFormat = WebMessageFormat.Json, 
            RequestFormat = WebMessageFormat.Json, 
            BodyStyle = WebMessageBodyStyle.Bare)]
        [OperationContract]
        Response Validate(Request request);

        #endregion
    }
}