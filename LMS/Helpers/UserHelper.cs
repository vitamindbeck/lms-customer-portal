﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LMS.Helpers
{
    public static class UserHelper
    {

        public static LMS.Data.User GetUser(this HtmlHelper helper, string name)
        {
            LMS.Data.LMSDataContext _DB = new LMS.Data.LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            return _DB.Users.Where(u => u.User_Name == name.Replace(@"LONGEVITY\", "")).FirstOrDefault();
        }
    }
}
