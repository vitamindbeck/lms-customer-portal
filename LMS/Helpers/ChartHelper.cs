﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;

namespace Helpers
{
    public static class ChartHelper
    {
        public static void Chart(this HtmlHelper helper, string polltitle, List<int> values, List<string> labels, int width, int height, SeriesChartType ChartType, System.Web.UI.Page page)
        {
            System.Web.UI.DataVisualization.Charting.Chart Chart1 = new System.Web.UI.DataVisualization.Charting.Chart();
            Chart1 = new System.Web.UI.DataVisualization.Charting.Chart();
            Chart1.Width = width;
            Chart1.Height = height;
            Chart1.RenderType = RenderType.ImageTag;
            // Populate series data
            Chart1.Series.Add("Default");
            Chart1.ChartAreas.Add("ChartArea1");
            Chart1.Series["Default"].Points.DataBindXY(labels, values);

            // Set Doughnut chart type
            Chart1.Series["Default"].ChartType = ChartType;

            Chart1.AntiAliasing = AntiAliasingStyles.All;
            Chart1.TextAntiAliasingQuality = TextAntiAliasingQuality.High;

            // Set labels style
            Chart1.Series["Default"]["PieLabelStyle"] = "outside";
                       
            Chart1.Titles.Add(polltitle);
            // Enable 3D
            Chart1.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;

            // Disable the Legend
            //Chart1.Legends[0].Enabled = false;


            // Render chart control
            Chart1.Page = page;
            HtmlTextWriter writer = new HtmlTextWriter(page.Response.Output);
            Chart1.RenderControl(writer);


        }
    }
}
