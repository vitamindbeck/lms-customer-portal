﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LMS.Data;
using System.Data.Linq;

namespace Helpers
{
    public class DuplicateHelper
    {
        public static bool IsDuplicate(int Record_ID)
        {
            LMSDataContext _db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);

            return ((ISingleResult<HasDuplicatesResult>) _db.HasDuplicates(Record_ID)).First().Duplicates.Value;
        }

        public static bool IsActive(int Plan_Transaction_ID)
        {
            LMSDataContext _db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            Plan_Transaction transaction = _db.Plan_Transactions.Where(pt => pt.Plan_Transaction_ID == Plan_Transaction_ID).FirstOrDefault();

            if (transaction != null)
            {
                if (transaction.Plan_Transaction_Status_ID == 1 || 
                    transaction.Plan_Transaction_Status_ID == 17 ||
                    transaction.Plan_Transaction_Status_ID == 19 ||
                    transaction.Plan_Transaction_Status_ID == 20 ||
                    transaction.Plan_Transaction_Status_ID == 21
                    )
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }
    }
}
