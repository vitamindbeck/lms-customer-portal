﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LMS.Data;

namespace LMS.Helpers
{
    public static class StringHelper
    {
        public static string UppercaseFirst(string s)
        {
            s = s.ToLower();
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            char[] a = s.ToCharArray();
            a[0] = char.ToUpper(a[0]);
            return new string(a);
        }

	public static System.Data.Linq.Binary EncryptString(string s)
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            return db.sp_Encrypt_Data(s).ToList()[0].EncryptedData;
        }

        public static string DecryptData(System.Data.Linq.Binary data)
        {
            LMSDataContext db = new LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            return db.sp_Decrypt_Data(data).ToList()[0].DecryptedData;
        }
    }
}
