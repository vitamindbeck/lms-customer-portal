﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.CompilerServices;
using System.Web.Mvc;
using System.Web.Routing;

namespace LMS.Helpers
{
    public static class ImageLinkHelper
    {
        public static string ImageLink(this HtmlHelper html, string action, string controller,object routeValues, 
                                string imageURL, string alternateText, object linkHtmlAttributes, object imageHtmlAttributes)
        {
            // Create an instance of UrlHelper 
            UrlHelper url = new UrlHelper(html.ViewContext.RequestContext);

            // Create image tag builder 
            TagBuilder imageBuilder = new TagBuilder("img");

            // Add image attributes 
            imageBuilder.MergeAttribute("src", imageURL);
            imageBuilder.MergeAttribute("alt", alternateText); 
            imageBuilder.MergeAttributes(new RouteValueDictionary(imageHtmlAttributes));

            // Create link tag builder 
            TagBuilder linkBuilder = new TagBuilder("a");

            // Add attributes 
            linkBuilder.MergeAttribute("href", url.Action(action, controller, new RouteValueDictionary(routeValues)));
            linkBuilder.InnerHtml = imageBuilder.ToString(TagRenderMode.SelfClosing);
            linkBuilder.MergeAttributes(new RouteValueDictionary(linkHtmlAttributes));

            // Render tag 
            return linkBuilder.ToString(TagRenderMode.Normal);
        }
    }
}
