﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LMS.Helpers
{
    public static class TableLockHelper
    {

        public static string CheckLock(string tablename, int? id, string username)
        {
            LMS.Data.LMSDataContext _DB = new LMS.Data.LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["LMSContextConnectionString"].ConnectionString);
            return _DB.sp_Check_Lock(tablename, id, username).First().User_Name;
        }
    }
}