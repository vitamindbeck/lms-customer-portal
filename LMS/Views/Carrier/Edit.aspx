<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<LMS.Data.Carrier>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Edit
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Edit Carrier</h2>
    <%= Html.ValidationSummary("Edit was unsuccessful. Please correct the errors and try again.") %>
    <% using (Html.BeginForm())
       {%>
    <table>
        <tr>
            <td class="label">
                Name:
            </td>
            <td>
                <%= Html.TextBox("Name", Model.Name, new { @style = "width: 300px" })%>
                <%= Html.ValidationMessage("Name", "*") %>
            </td>
        </tr>
        <tr>
            <td class="label">
                Address 1:
            </td>
            <td>
                <%= Html.TextBox("Address_1", Model.Address_1, new { @style = "width: 300px" })%>
                <%= Html.ValidationMessage("Address_1", "*") %>
            </td>
        </tr>
        <tr>
            <td class="label">
                Address 2:
            </td>
            <td>
                <%= Html.TextBox("Address_2", Model.Address_2, new { @style = "width: 300px" })%>
                <%= Html.ValidationMessage("Address_2", "*") %>
            </td>
        </tr>
        <tr>
            <td class="label">
                City:
            </td>
            <td>
                <%= Html.TextBox("City", Model.City) %>
                <%= Html.ValidationMessage("City", "*") %>
            </td>
        </tr>
        <tr>
            <td class="label">
                State:
            </td>
            <td>
                <%= Html.DropDownList("State_ID") %>
                <%= Html.ValidationMessage("State_ID", "*") %>
            </td>
        </tr>
        <tr>
            <td class="label">
                Zip:
            </td>
            <td>
                <%= Html.TextBox("Zip", Model.Zip, new { @style = "width: 100px" })%>
                <%= Html.ValidationMessage("Zip", "*") %>
            </td>
        </tr>
        <tr>
            <td class="label">
                Active:
            </td>
            <td>
                <%= Html.CheckBox("Active") %>
                <%= Html.ValidationMessage("Active", "*") %>
            </td>
        </tr>
        <tr>
            <td class="label" valign="top">
                About Carrier:
            </td>
            <td>
                <%= Html.TextArea("About_Carrier", Model.About_Carrier, 3, 40, null)%>
                <%= Html.ValidationMessage("About_Carrier", "*") %>
            </td>
        </tr>
        <tr>
            <td class="label">
                Need Upload:
            </td>
            <td>
                <%= Html.CheckBox("Need_Upload")%>
                <%= Html.ValidationMessage("Need_Upload", "*") %>
            </td>
        </tr>
        <tr>
            <td class="label">
                Service Level:
            </td>
            <td>
                <%= Html.TextBox("Service_Level", String.Format("{0:F}", Model.Service_Level), new { @style = "width: 200px" }) %>
                <%= Html.ValidationMessage("Service_Level", "*") %>
            </td>
        </tr>
        <tr>
            <td class="label" valign="top">
                Service Level Description:
            </td>
            <td>
                <%= Html.TextArea("Service_Level_Description", Model.Service_Level_Description,3, 40,null) %>
                <%= Html.ValidationMessage("Service_Level_Description", "*") %>
            </td>
        </tr>
        <tr>
            <td class="label">Assets:
            </td>
            <td>
            <%= Html.TextBox("Assets", String.Format("{0:0.00}", Model.Assets), new { @style = "width: 150px" })%>
        <%= Html.ValidationMessage("Assets", "*") %>
            </td>
        </tr>
        <tr>
            <td class="label">Code:
            </td>
            <td><%= Html.TextBox("Code", Model.Code, new { @style = "width: 100px" })%>
        <%= Html.ValidationMessage("Code", "*") %>
            </td>
        </tr>
        <tr>
            <td class="label">CompuComp:
            </td>
            <td><%= Html.TextBox("CompuComp", Model.CompuComp, new { @style = "width: 100px" })%>
        <%= Html.ValidationMessage("CompuComp", "*") %>
            </td>
        </tr>
        <tr>
            <td class="label">Group:
            </td>
            <td>
                <%= Html.DropDownList("Group_ID") %>
                <%= Html.ValidationMessage("Group_ID", "*")%>
            </td>
        </tr>
        <tr>
            <td class="label">GBS Admin ID:
            </td>
            <td>
                <%= Html.TextBox("GBS_Admin_ID")%>
                <%= Html.ValidationMessage("GBS_Admin_ID", "*")%>
            </td>
        </tr>
        <tr>
            <td class="label">
            </td>
            <td>
            <input type="submit" value="Save" />
            </td>
        </tr>
        <tr>
            <td class="label">
            </td>
            <td>
            </td>
        </tr>
    </table>
    <% } %>
    <div>
        <%=Html.ActionLink("Back to List", "Index") %>
    </div>
</asp:Content>
