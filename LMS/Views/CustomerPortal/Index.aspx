﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<LMS.Models.CustomerPortalViewModel>" %>
<%@ Import Namespace="System.Web.Optimization" %>
<%@ Import Namespace="LMS.Models" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <title>Index</title>
    <link href="<%= Url.Content("~/Content/CustomerPortal.css") %>" rel="stylesheet" type="text/css"/>
    <link href="<%= Url.Content("~/Scripts/jquery-ui-1.11.0/jquery-ui.css") %>" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="<%= Url.Content("~/Scripts/jquery-1.11/jquery-1.11.1.min.js") %>"></script>
    <script type="text/javascript" src="<%= Url.Content("~/Scripts/jquery-ui-1.11.0/jquery-ui.js") %>"></script>
    <script type="text/javascript" src="<%= Url.Content("~/Scripts/Singleselectable.js") %>"></script>
    <script type="text/javascript" src="<%= Url.Content("~/Scripts/CustomerPortal_Index.js") %>"></script>
</head>
<body class="mainbody">
    <div class="page">
        <div id="header">
            <div id="title">
                <img id="HeaderTitle" src="<%= Url.Content("~/Content/images/Header.jpg") %>"/>
                <h1>Customer Portal - <%= string.Format("{0} - {1} {2} {3}", Model.CustomerId, Model.FirstName, Model.MiddleInitial, Model.LastName)%></h1>
            </div>
        </div>
        <br />
        &nbsp;
        <div id="main">
            <div>
                <div style="height: 44px; text-align: left">
                    <img alt="Add User" src="/content/icons/user_add.png" style="vertical-align: middle" />
                    <img alt="Add Policy" src="/content/icons/folder_add.png" style="vertical-align: middle" />
                    <img alt="Print Name Label" src="/Content/icons/printer-orange-icon.png" style="vertical-align: middle" />
                    <img alt="Print Address Label" src="/Content/icons/printer-green-icon.png" style="vertical-align: middle" />
                </div>
            </div>
            
            <div>
                <% Html.RenderPartial("CustomerInformation", CustomerInformationViewModel.GetViewModel(Model.RecordId)); %>
                <% Html.RenderPartial("CoverageProfile", CoverageProfileViewModel.GetViewModel(Model.RecordId)); %>
                <% Html.RenderPartial("B2EProfile", new LMS.Models.B2EProfileViewModel() { CustomerId = Model.CustomerId }); %>
                <% Html.RenderPartial("Policies", new LMS.Models.PoliciesViewModel() { RecordId = Model.RecordId }); %>
                <% Html.RenderPartial("CustomerProfile", new LMS.Models.CustomerProfileViewModel { CustomerId = Model.CustomerId }); %>
                <% Html.RenderPartial("RecordActivity", new RecordActivityViewModel {}); %>

                <div id="Activity"></div>
                <div style="clear:both"></div>
           </div>
        </div>
    </div>
</body>
</html>
