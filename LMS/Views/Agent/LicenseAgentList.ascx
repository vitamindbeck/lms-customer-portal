﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<LMS.Data.sp_Agent_ListResult>>" %>
<%@ Import Namespace="LMS.Helpers" %>
 <div style="text-align:left">
 <table width="100%">
   <tr>
    <td><a href="/Agent/New">Add New Agent/Agency</a></td>
    <td align="right"><%=Html.ActionLink("Export to Excel", "Export")%></td>
   </tr>
 </table>
    
    
 </div>
 
 
 
 <div class="client-outer">
  <div class="client-title">
         [ <%= Html.Encode(Model.Count()) %> Agents/Agencies ]
    </div>
  
 <div class="client-inner">
 
    <table class="sortable" width="100%" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
            <th>Agent/Agency Name</th>
            <th>NPN#</th>
            <th>Licenses</th>
            <th>Appointments</th>
            <th>Certifications</th>
            </tr>
        </thead>
        <tbody>
        <% foreach (LMS.Data.sp_Agent_ListResult agentrec in Model)
           { %>
        
        <tr>
            <td nowrap="nowrap">
                <%= Html.ActionLink(agentrec.Name, "Details", "Agent", new { id = agentrec.Agent_ID}, new { target = "_blank", style = "target-new: tab ! important" })%>
            </td>
           <td>
                <%= Html.Encode(agentrec.NPN)%>
           </td>
           <td>
                <%= Html.ActionLink("View", "List", "License", new { id = agentrec.Agent_ID }, new { target = "_blank", style = "target-new: tab ! important" })%>         
           </td>
           <td>
                <%= Html.ActionLink("View", "List", "Appointment", new { id = agentrec.Agent_ID }, new { target = "_blank", style = "target-new: tab ! important" })%>               
           </td>
           <td>
                <%= Html.ActionLink("View", "List", "Certification", new { id = agentrec.Agent_ID }, new { target = "_blank", style = "target-new: tab ! important" })%>                       
           </td>
          
        </tr>
        <%} %>
        </tbody>
    </table>
    </div>