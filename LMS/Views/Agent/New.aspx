<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Licensing.Master" Inherits="System.Web.Mvc.ViewPage<LMS.Data.Agent>" %>
<%@ Import Namespace="LMS.Helpers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Select Agent type:
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <script type="text/javascript" src="../../Scripts/RecordValidateSelect.js"></script>

   <script type="text/javascript">
       $(function() {


           $("#Agent_Start_Date").datepicker();
           $('#Agent_Start_Date').mask("99/99/9999");
           $("#Agency_Start_Date").datepicker();
           $('#Agency_Start_Date').mask("99/99/9999");
           $('#DOB').mask("99/99/9999");
           $("#Phone_Number").mask("(999) 999-9999");
           $("#Agency_Phone_Number").mask("(999) 999-9999");
           $("#Agent_SSN").mask("999-99-9999");

           if ($("#chkAgent").is(":checked")) {
               $("#AddAgentDetails").show();
               $("#AddAgencyDetails").hide();
               
           } else if ($("#chkAgency").is(":checked")) {
           $("#AddAgencyDetails").show();
           $("#AddAgentDetails").hide();
           }
           else {
               $("#AddAgentDetails").hide();
               $("#AddAgencyDetails").hide();
           }
           $("#chkAgent").click(function() {

               if ($("#chkAgent").is(":checked")) {


                   $("#chkAgent").attr('checked', true);
                   $("#chkAgency").attr('checked', false);
                   $("#AddAgentDetails").show();
                   $("#AddAgencyDetails").hide();

               }

           });

           $("#chkAgency").click(function() {

               if ($("#chkAgency").is(":checked")) {


                   $("#chkAgency").attr('checked', true);
                   $("#chkAgent").attr('checked', false);
                   $("#AddAgentDetails").hide();
                   $("#AddAgencyDetails").show();
               }

           });


       });

   

 </script>
 
<%= Html.ValidationSummary("Unable to Create Record.") %>
 <% using (Html.BeginForm("New", "Agent", FormMethod.Post))
      
    {%>
   <div class="client-outer" style="background-color: #e0e4ee;">
   <div class="section-title">
        Select Agent Type
    </div>
    <div class="section-inner">
        <table class="section" cellpadding="0" cellspacing="0">
         <tr>
          <td class="label">
              Agent:<%=Html.CheckBox("chkAgent", (Request.Form["chkAgent"] ?? string.Empty).Contains("true"))%>
              Agency:<%=Html.CheckBox("chkAgency", (Request.Form["chkAgency"] ?? string.Empty).Contains("true"))%>
            </td>
           
         </tr>
        </table> 
   </div>
   
</div>
<div id = "AddAgentDetails" class="client-outer" style="background-color: #e0e4ee;">
    <div class="section-title">
        Add Agent Information
    </div>
    <div class="section-inner">
        <table class="section" cellpadding="0" cellspacing="0">
           <tr>
          <td class="label">
                Select Agent Name:
            </td>
            <td>
             <%= Html.DropDownList("Agent_Name", (SelectList)ViewData["Agent_Name"], new { tabindex = "1" })%>
            </td>
            <td class="label">
                Date Of birth:
            </td>
            <td>
                <%= Html.TextBox("DOB", Model.DOB, new { style = "width: 100px;", tabindex = "2" })%>
             
            </td>
           </tr>
           <tr>
         
             <td class="label">
                Start Date:
            </td>
            <td>
               <%= Html.TextBox("Agent_Start_Date", Model.Agent_Start_Date,new { tabindex = "3" })%>
             </td>
              <td class="label">
                Associated Agency:
            </td>
            <td>
            <%= Html.DropDownList("Associated_Agency_ID", (SelectList)ViewData["Associated_Agency"], new { tabindex = "4" })%>
            <%= Html.ValidationMessage("Associated_Agency_ID", "*")%>
            </td>
             
              
           </tr>
           <tr>
             <td class="label">
                NPN:
            </td>
            <td>
                <%= Html.TextBox("NPN", Model.NPN, new { style = "width: 100px;", tabindex = "5" })%>
             
            </td>
            <td class="label">
                SSN:
            </td>
            <td>
                <%= Html.TextBox("Agent_SSN", StringHelper.DecryptData(Model.SSN),new { style = "width: 100px;", tabindex = "6" })%>
              
            </td>
           </tr>
           <tr>
            <td class="label">
                Address Line 1:
            </td>
            <td>
               <%= Html.TextBox("Address_1", Model.Address_1,new { tabindex = "7" })%>
             </td>
             <td class="label">
                Address Line 2:
            </td>
            <td>
               <%= Html.TextBox("Address_2", Model.Address_2,new { tabindex = "8" })%>
             </td>
           </tr>
         <tr>
           <td class="label" style="height: 18px">
                City:
            </td>
            <td class="style3">
               <%= Html.TextBox("City", Model.City,new { tabindex = "9" })%>
             </td>
            <td class="label" style="height: 18px">
               State:
            </td>
            <td class="style4">
                <%= Html.DropDownList("State_ID", (SelectList)ViewData["State"], new { style = "width: 100px;", tabindex = "10" })%>
               
            </td>
           </tr>
         <tr>
             <td class="label">
               County:
            </td>
            <td>
               <%= Html.TextBox("County", Model.County,new { tabindex = "11" })%>
             </td>
             <td class="label">
                Zip Code:
            </td>
            <td>
               <%= Html.TextBox("Zip_Code", Model.Zip_Code, new { tabindex = "12" })%>
             </td>
           </tr>
           <tr>
           <td class="label">
               Home Phone:
            </td>
            <td>
               <%= Html.TextBox("Phone_Number", Model.Phone_Number, new { tabindex = "13" })%>
             </td>
           <td class="label">
                Email:
            </td>
            <td>
               <%= Html.TextBox("Email",Model.Email,new { tabindex = "14" })%>
             </td>
           </tr>
         
                      
        </table>
        
    </div>
     <div style="text-align: center;">
        <input type="image" src="../../Content/icons/check.png" title="Save" alt="Save" id="Image1" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="../../Agent/List" alt="Cancel"><img src="../../content/icons/close.png" alt="Cancel" title="Cancel" style="border: 0px;
                vertical-align: top;" />
        </a>
    </div>
    

 </div>

    

 
<div id = "AddAgencyDetails" class="client-outer" style="background-color: #e0e4ee; ">

    <div class="section-title">
        Add Agency Information
    </div>
    <div class="section-inner">
        <table class="section" cellpadding="0" cellspacing="0">
          <tr>
            <td class="label">
                Agency Name:
            </td>
            <td>
               <%= Html.TextBox("Agency_Name", (SelectList)ViewData["Agency_Name"], new { tabindex = "1" })%>
               <%= Html.ValidationMessage("Agency_Name", "*")%>
            </td>
            <td class="label">
                Incorporated Date:
            </td>
              <td>
               <%= Html.TextBox("Agency_Start_Date", Model.Agent_Start_Date , new { tabindex = "2" })%>
             </td>
            
          </tr>
          <tr>
            <td class="label">
                NPN:
            </td>
            <td>
                <%= Html.TextBox("Agency_NPN", Model.NPN, new { style = "width: 100px;", tabindex = "3" })%>
             
            </td>
            <td class="label">
                Tax ID:
            </td>
            <td>
                <%= Html.TextBox("Tax_ID", Model.Tax_ID, new { style = "width: 100px;", tabindex = "4" })%>
             
            </td>
          </tr>
           <tr>
            <td class="label">
                Address Line 1:
            </td>
            <td>
                <%= Html.TextBox("Agency_Address_1", Model.Address_1, new { style = "width: 100px;", tabindex = "5" })%>
             
            </td>
            <td class="label">
                 Address Line 2:
            </td>
              <td>
               <%= Html.TextBox("Agency_Address_2", Model.Address_2, new { tabindex = "6" })%>
             </td>
          </tr>
            <tr>
            <td class="label">
               City:
            </td>
            <td>
                <%= Html.TextBox("Agency_City", Model.City, new { style = "width: 100px;", tabindex = "7" })%>
             
            </td>
         <td class="label">
                 State:
            </td>
              <td class="style1">
                <%= Html.DropDownList("Ag_State_ID", (SelectList)ViewData["Agency_State_ID"], new { style = "width: 100px;", tabindex = "8" })%>
              </td>
          </tr>
             <tr>
            <td class="label">
              County:
            </td>
            <td>
                <%= Html.TextBox("Agency_County", Model.County, new { style = "width: 100px;", tabindex = "9" })%>
             
            </td>
            <td class="label">
                 Zip Code:
            </td>
              <td class="style1">
                <%= Html.TextBox("Agency_Zip_Code", Model.Zip_Code, new { style = "width: 100px;", tabindex = "10" })%>
              </td>
          </tr>
             <tr>
            <td class="label">
              Agency Phone:
            </td>
            <td>
                <%= Html.TextBox("Agency_Phone_Number", Model.Phone_Number, new { style = "width: 100px;", tabindex = "11" })%>
             
            </td>
     
            </tr>
        </table>
    </div>
    <div style="text-align: center;">
        <input type="image" src="../../Content/icons/check.png" title="Save" alt="Save" id="saveBtn" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="../../Agent/List" alt="Cancel"><img src="../../content/icons/close.png" alt="Cancel" title="Cancel" style="border: 0px;
                vertical-align: top;" />
        </a>
    </div>

</div>
<%} %>
</asp:Content>



<asp:Content ID="Content3" runat="server" contentplaceholderid="HeaderContent">

    <style type="text/css">
        .style1
        {
            width: 115px;
        }
        .style2
        {
            width: 71px;
        }
        .style3
        {
            height: 18px;
        }
        .style4
        {
            width: 115px;
            height: 18px;
        }
    </style>
</asp:Content>



