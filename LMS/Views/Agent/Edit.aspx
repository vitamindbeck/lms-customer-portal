<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Licensing.Master" Inherits="System.Web.Mvc.ViewPage<LMS.Data.Agent>" %>
<%@ Import Namespace="LMS.Helpers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Edit Agent
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
 <script type="text/javascript">
     $(function() {


         if ('<%= Model.Agent_Type_ID %>' == 1) {
             $("#EditAgentDetails").show();
             $("#EditAgencyDetails").hide();
         }
         else {
             $("#EditAgentDetails").hide();
             $("#EditAgencyDetails").show();
         }

         $("#Agency_Start_Date").datepicker();
         $("#Agency_Start_Date").mask("99/99/9999");

         $("#DOB").datepicker();
         $("#DOB").mask("99/99/9999");
         $("#Phone_Number").mask("(999) 999-9999");
         
         
         $("#Agent_Start_Date").datepicker();
         $('#Agent_Start_Date').mask("99/99/9999");
         
         $("#Agent_End_Date").datepicker();
         $('#Agent_End_Date').mask("99/99/9999");
         $("#Ag_SSN").mask("999-99-9999");

         $("#Agent_Name").attr('disabled', true);
         $("#NPN").val("<%= Model.NPN %>");
         $("#Active").val("<%= Model.Active %>");
         $("#Address_1").val("<%= Model.Address_1 %>");
         $("#Address_2").val("<%= Model.Address_2 %>");
         $("#City").val("<%= Model.City %>");
         $("#State_ID").val("<%= Model.State_ID %>");
         $("#County").val("<%= Model.County %>");
         $("#Zip_Code").val("<%= Model.Zip_Code %>");
         $("#Phone_Number").val("<%= Model.Phone_Number %>");
         $("#Email").val("<%= Model.Email %>");





         //Agency
         $("#Agency_Phone_Number").mask("(999) 999-9999");
         $("#Agency_Name").val("<%= Model.Agency_Name %>");
         $("#Agency_Name").attr('disabled', false);
         $("#Tax_ID").val("<%= Model.Tax_ID %>");
         $("#Agency_NPN").val("<%= Model.NPN %>");
         $("#Agency_Active").val("<%= Model.Active %>");
         $("#Agency_Address_1").val("<%= Model.Address_1 %>");
         $("#Agency_Address_2").val("<%= Model.Address_2 %>");
         $("#Agency_City").val("<%= Model.City %>");
         $("#Agency_State_ID").val("<%= Model.State_ID %>");
         $("#Agency_County").val("<%= Model.County %>");
         $("#Agency_Zip_Code").val("<%= Model.Zip_Code %>");
         $("#Agency_Phone_Number").val("<%= Model.Phone_Number %>");

     });


      

       
        
    </script>

<div id = "EditAgentDetails" class="client-outer" style="background-color: #e0e4ee;">
<% using (Html.BeginForm())
   {%>
    <div class="section-title">
        Update Agent Information
    </div>
    <div class="section-inner">
        <table class="section" cellpadding="0" cellspacing="0">
          <tr>
            <td class="label" width="200px">
                Agent Name:
            </td>
            <td class="style3">
             <%= Html.TextBox("Agent_Name", ViewData["Agent_Name"], new { tabindex = "1" })%>
             
              
            </td>
             <td class="label">
                Date Of birth:
            </td>
            <td class="style1">
                <%= Html.TextBox("DOB", Model.DOB.HasValue ? Model.DOB.Value.ToString("MM/dd/yyyy") : "", new { style = "width: 100px;", tabindex = "2" })%>
             
            </td>
            </tr>
            <tr>
             <td class="label">
                Start Date:
            </td>
            <td class="style3">
               <%= Html.TextBox("Agent_Start_Date", Model.Agent_Start_Date.HasValue ? Model.Agent_Start_Date.Value.ToString("MM/dd/yyyy") : "", new { tabindex = "3" })%>
           
             </td>
            <td class="label">
                End Date:
            </td>
            <td>
                <%= Html.TextBox("Agent_End_Date", Model.Agent_End_Date.HasValue ? Model.Agent_End_Date.Value.ToString("MM/dd/yyyy") : "", new { style = "width: 100px;", tabindex = "4" })%>
            </td>
            </tr>
            <tr>
               <td class="label">
                    Associated Agency:
                </td>
                <td class="style2">
                   <%= Html.DropDownList("Associated_Agency_ID", (SelectList)ViewData["Associated_Agency"], new { tabindex = "5" })%>
                    <%= Html.ValidationMessage("Associated_Agency_ID", "*")%>
                </td>
                <td class="label">
                    Active:
                </td>
                <td class="style2">
                   <%= Html.CheckBox("Active",  new { tabindex = "6" })%>
                </td>
            </tr>
            
            <tr>
           <td class="label">
                NPN:
            </td>
            <td class="style1">
                <%= Html.TextBox("NPN", Model.NPN, new { style = "width: 100px;", tabindex = "7" })%>
             
            </td>
              <td class="label">
                SSN:
            </td>
            <td class="style3">
                <%= Html.TextBox("Ag_SSN", StringHelper.DecryptData(Model.SSN), new { style = "width: 100px;", tabindex = "8" })%>
              
            </td>
          </tr>
          <tr>
           <td class="label">
                Address Line 1:
            </td>
            <td class="style1">
                <%= Html.TextBox("Address_1", Model.Address_1, new { style = "width: 100px;", tabindex = "9" })%>
             
            </td>
              <td class="label">
               Address Line 2:
            </td>
            <td class="style3">
                <%= Html.TextBox("Address_2", Model.Address_2, new { style = "width: 100px;", tabindex = "10" })%>
              
            </td>
           </tr>
         <tr>
           <td class="label">
                City:
            </td>
            <td class="style1">
                <%= Html.TextBox("City", Model.City, new { style = "width: 100px;", tabindex = "11" })%>
             
            </td>
              <td class="label">
               State:
            </td>
            <td class="style3">
                <%= Html.DropDownList("State_ID", (SelectList)ViewData["State_ID"], new { style = "width: 100px;", tabindex = "12" })%>
              
            </td>
           </tr>
          <tr>
           <td class="label">
                Zip Code:
            </td>
            <td class="style1">
                <%= Html.TextBox("Zip_Code", Model.Zip_Code, new { style = "width: 100px;", tabindex = "13" })%>
             
            </td>
             <td class="label">
                    County:
                </td>
                <td class="style4">
                   <%= Html.TextBox("County", Model.County, new { style = "width: 100px;", tabindex = "14" })%>
                </td>
             
           </tr>
           <tr>
            <td class="label">
              Home Phone:
            </td>
            <td class="style3">
                <%= Html.TextBox("Phone_Number", Model.Phone_Number, new { style = "width: 100px;", tabindex = "15" })%>
              
            </td>
           <td class="label">
                Email Address:
            </td>
            <td class="style1"> 
                <%= Html.TextBox("Email", Model.Email, new { style = "width: 180px;", tabindex = "16" })%>
             
            </td>
            
           </tr>
          </table>
        
    </div>
     <div style="text-align: center;">
        <input type="image" src="../../Content/icons/check.png" title="Save" alt="Save" id="Image1" title ='Save' />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="../../Agent/Details/<%=Model.Agent_ID %>" alt="Cancel"><img src="../../content/icons/close.png" alt="Cancel" title="Cancel" style="border: 0px;
                vertical-align: top;" />
        </a>
    </div>
    
 <%} %>
 </div>
<div id = "EditAgencyDetails" class="client-outer" style="background-color: #e0e4ee;">
<% using (Html.BeginForm())
   {%>
    <div class="section-title">
        Update Agency Information
    </div>
    <div class="section-inner">
        <table class="section" cellpadding="0" cellspacing="0">
          <tr>
            <td class="label" width="150px">
                Agency Name:
            </td>
            <td class="style4">
               <%= Html.TextBox("Agency_Name", (SelectList)ViewData["Agency_Name"], new { tabindex = "1" })%>
            </td>
              <td class="label" style="width: 194px">
                Incorporated Date:
            </td>
            <td>
              <%= Html.TextBox("Agency_Start_Date", Model.Agent_Start_Date.HasValue ? Model.Agent_Start_Date.Value.ToString("MM/dd/yyyy") : "", new { tabindex = "2" })%>
            </td>
            </tr>
            <tr>
              <td class="label">
                NPN:
              </td>
              <td class="style4">
                <%= Html.TextBox("Agency_NPN", Model.NPN, new { style = "width: 100px;", tabindex = "3" })%>
             
              </td>
              <td class="label" style="width: 200px">
                Tax ID:
              </td>
              <td>
                <%= Html.TextBox("Tax_ID", Model.Tax_ID, new { style = "width: 100px;", tabindex = "4" })%>
             
              </td>
            </tr>
            
          
           <tr>
            <td class="label">
                Active:
            </td>
            <td class="style4">
                   <%= Html.CheckBox("Agency_Active",  new { tabindex = "5" })%>
            </td>
            <td class="label">
                Agency Phone:
            </td>
            <td class="style4">
                  <%= Html.TextBox("Agency_Phone_Number", Model.Phone_Number, new { style = "width: 100px;", tabindex = "6" })%>
            </td>
            </tr>
         
            <tr>
                <td class="label">
                    Address Line 1:
                </td>
                <td class="style4">
                   <%= Html.TextBox("Agency_Address_1", Model.Address_1, new { style = "width: 100px;", tabindex = "7" })%>
                </td>
                <td class="label">
                    Address Line 2:
                </td>
                <td class="style4">
                 <%= Html.TextBox("Agency_Address_2", Model.Address_2, new { style = "width: 100px;", tabindex = "8" })%>
                </td>
            </tr>
            <tr>
                <td class="label">
                    City :
                </td>
                <td class="style4">
                   <%= Html.TextBox("Agency_City", Model.City, new { style = "width: 100px;", tabindex = "9" })%>
                </td>
                <td class="label">
                    State:
                </td>
                <td class="style4">
                 <%= Html.DropDownList("Agency_State_ID", (SelectList)ViewData["Agency_State_ID"], new { tabindex = "10" })%>
                </td>
            </tr>
            <tr>
                <td class="label">
                    County:
                </td>
                <td class="style4">
                   <%= Html.TextBox("Agency_County", Model.County, new { style = "width: 100px;", tabindex = "11" })%>
                </td>
                <td class="label">
                    Zip Code:
                </td>
                <td class="style4">
                  <%= Html.TextBox("Agency_Zip_Code", Model.Zip_Code, new { style = "width: 100px;", tabindex = "12" })%>
                </td>
            </tr>
        </table>
    </div>
    <div style="text-align: center;">
        <input type="image" src="../../Content/icons/check.png" title="Save" alt="Save" id="saveBtn" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="../../Agent/Details/<%=Model.Agent_ID %>" alt="Cancel"><img src="../../content/icons/close.png" alt="Cancel" title="Cancel" style="border: 0px;
                vertical-align: top;" />
        </a>
    </div>
<%} %>
</div>


    
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
    <style type="text/css">
        .style1
        {
            width: 207px;
        }
        .style2
        {
            width: 284px;
        }
        .style3
        {
            width: 287px;
        }
        .style4
        {
            width: 289px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptContent" runat="server">
</asp:Content>
