<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<LMS.Data.Plan_Document>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Document List
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Document List</h2>
     <p>
        <%= Html.ActionLink("Create New", "Create", new { id = Convert.ToInt32(Request.QueryString["id"])})%>
    </p>
    <table cellpadding="0" cellspacing="0" style="width: 100%" class="sortable">
        <thead>
            <tr>
                <th>
                </th>
                <th>
                    State
                </th>
                <th>
                    Title
                </th>
                <th>
                    Active
                </th>
            </tr>
        </thead>
        <tbody>
            <% foreach (var item in Model)
               { %>
            <tr>
                <td>
                    <%= Html.ActionLink("Edit", "Edit", new { id=item.Plan_Document_ID }) %> |
                    <a href="/Document/Delete/<%= Html.Encode(item.Plan_Document_ID) %>" onclick="$.post(this.href); window.location.reload(); return false;">Delete</a>
                </td>
                <td>
                    <%= Html.Encode(item.State.Name) %>
                </td>
                <td>
                    <%= Html.Encode(item.Title) %>
                </td>
                <td>
                    <%= Html.Encode(item.Active ? "Yes" : "No") %>
                </td>
            </tr>
            <% } %>
        </tbody>
    </table>
   
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
