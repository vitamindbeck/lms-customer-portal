<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<LMS.Data.Plan_Document>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Create
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <script type="text/javascript">
         $(document).ready(function() {
             $("#SelectAllStates").click(function() {
                 if ($("#SelectAllStates").html() == 'Select All') {
                     $("#State_ID").each(function() {
                         $("#State_ID option").attr("selected", "selected");
                     });
                     $("#SelectAllStates").html('Deselect All');
                     return false;
                 }
                 else {
                     $("#State_ID").each(function() {
                     $("#State_ID option").removeAttr("selected", "selected");
                     });
                     $("#SelectAllStates").html('Select All');
                     $("#State_ID").animate({ scrollTop: '0px' }, 100);
                     return false;
                 }
             });

             //            $("#unselect_all_col_managers").click(function() {
             //                $("#col_manager_list").each(function() {
             //                    $("#col_manager_list option").removeAttr("selected");
             //                });
             //            });

         });
    </script>
    <h2>Create</h2>

    <%= Html.ValidationSummary("Create was unsuccessful. Please correct the errors and try again.") %>

    <% using (Html.BeginForm("Create", "Document", FormMethod.Post, new { enctype = "multipart/form-data" })) 

        {%> 
        <fieldset>
            <legend>Fields</legend>
            <p>
                <label for="Location">Location:</label>
                <input name="Location" type="file" style="width: 250px;" />
            </p>
            <p>
                <label for="State_ID">State:</label>
                <a href="" id="SelectAllStates" name="SelectAllStates">Select All</a>
                <br />
                <%= Html.ListBox("State_ID", (SelectList)ViewData["State_ID"], new { style = "width: 250px;", size = "10" })%>
                <%= Html.ValidationMessage("State_ID", "*") %>
            </p>
            <p>
                <label for="Title">Title:</label>
                <%= Html.TextBox("Title", "", new { style = "width: 250px;" })%>
                <%= Html.ValidationMessage("Title", "*") %>
            </p>
            <p>
                <label for="Active">Active:</label>
                <%= Html.CheckBox("Active",true) %>
                <%= Html.ValidationMessage("Active", "*") %>
            </p>
            <p>
                <%= Html.Hidden("Plan_ID",ViewData["Plan_ID"])%>
                <input type="submit" value="Create" />
            </p>
        </fieldset>

    <% } %>

    <div>
        <%=Html.ActionLink("Back to List", "Index") %>
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>

