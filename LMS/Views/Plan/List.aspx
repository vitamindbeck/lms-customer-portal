<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<LMS.Data.Plan>>" %>
<%@ Import Namespace="LMS.Helpers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Plans
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Plans For
    </h2>
    <table class="sortable" style="width: 100%" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th>
                </th>
                <th>
                    Name
                </th>
                <th>
                    Active
                </th>
                <th>
                    About
                </th>
                <th>
                    Documents
                </th>
            </tr>
        </thead>
        <tbody>
            <% foreach (var item in Model)
               { %>
            <tr>
                <td>
                    <%= Html.ActionLink("Edit", "Edit", new { id=item.Plan_ID }) %>
                </td>
                <td>
                    <%= Html.Encode(item.Name) %>
                </td>
                <td>
                    <%= Html.Encode(item.Active) %>
                </td>
                <td>
                    <%= Html.Encode(item.About) %>
                </td>
                <td>
                    <%= Html.ImageLink("List", "Document", new { id = item.Plan_ID }, "../../Content/images/folder-open.png", "Documents", null, new { style = "border:0px;" })%>
                    (<%= Html.Encode(item.Plan_Documents.Count())%>)
                </td>
            </tr>
            <% } %>
        </tbody>
    </table>
    <p>
        <%= Html.ActionLink("Create New", "Create") %>
    </p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
