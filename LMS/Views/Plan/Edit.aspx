<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<LMS.Data.Plan_Document>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Edit
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Edit</h2>

    <%= Html.ValidationSummary("Edit was unsuccessful. Please correct the errors and try again.") %>

    <% using (Html.BeginForm()) {%>

        <fieldset>
            <legend>Document</legend>
           <p>
                <label for="State_ID">State:</label>
                <%= Html.DropDownList("State_ID") %>
                <%= Html.ValidationMessage("State_ID", "*") %>
            </p>
            <p>
                <label for="Title">Title:</label>
                <%= Html.TextBox("Title", Model.Title, new { style = "width: 200px;" })%>
                <%= Html.ValidationMessage("Title", "*") %>
            </p>
            <p>
                <label for="Active">Active:</label>
                <%= Html.CheckBox("Active", Model.Active) %>
                <%= Html.ValidationMessage("Active", "*") %>
            </p>
            <p>
                <label for="Display_Order">Display_Order:</label>
                <%= Html.TextBox("Display_Order", Model.Display_Order, new { style = "width: 40px;" })%>
                <%= Html.ValidationMessage("Display_Order", "*") %>
            </p>
            <p>
                <input type="submit" value="Save" />
            </p>
        </fieldset>

    <% } %>

    <div>
        <%=Html.ActionLink("Back to List", "Documents", new { id = Model.Plan_ID })%>
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>

