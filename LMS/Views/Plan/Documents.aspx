<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<LMS.Data.Plan_Document>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Documents
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Documents For <%= Html.Encode(ViewData["Title"]) %></h2>
    <table style="border: solid 1px #b6bac0; margin: 20px; width: 800px; position: relative;"
        cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th>
                </th>
                <th>
                    State
                </th>
                <th>
                    Title
                </th>
                <th>
                    Location
                </th>
                <th>
                    Active
                </th>
                <th>
                    Display Order
                </th>
            </tr>
        </thead>
        <tbody>
            <% foreach (var item in Model)
               { %>
            <tr>
                <td>
                    <%= Html.ActionLink("Edit", "Edit", new { id=item.Plan_Document_ID }) %>
                </td>
                <td>
                    <%= Html.Encode(item.State.Name) %>
                </td>
                <td>
                    <%= Html.Encode(item.Title) %>
                </td>
                <td>
                    <%= Html.Encode(item.Location) %>
                </td>
                <td>
                    <%= Html.Encode(item.Active) %>
                </td>
                <td>
                    <%= Html.Encode(item.Display_Order) %>
                </td>
            </tr>
            <% } %>
        </tbody>
    </table>
    <p>
        <%= Html.ActionLink("Create New", "Create") %>
    </p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
