<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<LMS.Data.Record_Activity>" %>

<%@ Import Namespace="LMS.Helpers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%
        LMS.Data.User user = LMS.Data.Repository.UserRepository.GetUser(User.Identity.Name);
        //LMS.Data.Record_Activity rec = LMS.Data.Record_Activity.
        //Record_Activity apptra = db.Record_Activities.Where(a => a.Plan_Transaction_ID == dbActivity.Plan_Transaction_ID && a.Activity_Type_ID == 22 && a.Actual_Date == null).FirstOrDefault();
        //Hide Save button for complaints if not the assinged user
        var showButton = true;
        var disableDispo = 0;
       
        if (Model.Record_Activity_Status_ID != null)
        {
            if (Model.Record_Activity_Status_ID == 33 || Model.Record_Activity_Status_ID == 34 || Model.Record_Activity_Status_ID == 35)
            {
                if (!LMS.Data.Repository.UserRepository.HasSecurityLevel(user, 13) && Model.User_ID != user.User_ID)
                {
                    showButton = false;
                }

                if (!LMS.Data.Repository.UserRepository.HasSecurityLevel(user, 13) && Model.User_ID == user.User_ID)
                {
                    disableDispo = 1;
                }                                   
            }
        }
    %>
    <script type="text/javascript">
        var dispDDLShow = <%=disableDispo %>;
        $(function () {

            diableDispoDDL();
            function diableDispoDDL() {
                if (dispDDLShow){
                    $("#Reconciliation_Type_ID").val('64');
                    $("#Reconciliation_Type_ID").attr("disabled", true);
                    $("#Reconciliation_Type_ID").after('<input type="hidden" name="Reconciliation_Type_ID" value="64" />');
                }
            };

            $('#copyNote, #copyNoteFollowUp, #copyNoteModal').click(function () {
                if ($(this).attr("id") == "copyNote") {
                    var $NoteTextField = $('#Notes'),
                        $NoteList = $('#RecordNotes :selected');
                }

                if ($(this).attr("id") == "copyNoteFollowUp") {
                    var $NoteTextField = $('#Follow_Up_Notes'),
                        $NoteList = $('#FollowRecordNotes :selected');
                }

                if ($(this).attr("id") == "copyNoteModal") {
                    var $NoteTextField = $('#Modal_Follow_Up_Notes'),
                        $NoteList = $('#Modal_FollowRecordNotes :selected');
                }

                if ($NoteTextField.val() == "")
                    $NoteTextField.val($NoteList.text());
                else
                    $NoteTextField.val($NoteTextField.val() + ', ' + $NoteList.text());
            });           
            
            disableMenuValues('#Follow_Activity_Type_ID');
            activityMenu();
            
            

            $("#Activity_Type_ID").change(function () {
                resetSelection('#Record_Activity_Status_ID');
                resetSelection('#Reconciliation_Type_ID');
                activityMenu();
            });
            
            toggleDDLS();
            $("#Record_Activity_Status_ID").change(function () {
                toggleDDLS();
                //for complaints
                if ($("#Activity_Type_ID").val() > 0 && $("#Reconciliation_Type_ID").val() > 0) {
                    checkForFollowUp();
                }
                statusMenu();
               
            });

            disableFollowUpTable(); 
            complaintActivity();

            // 64 == complaint in process
            function complaintActivity() {
                if ($('#Reconciliation_Type_ID').val() == 40 || $('#Reconciliation_Type_ID').val() == 64) {
                    $('#Follow_Up_Date').attr("disabled", false);
                    $('#Follow_Up_Time').attr("disabled", false);
                    $('#Follow_Activity_Type_ID').attr("disabled", false);
                    $('#Follow_User_ID').attr("disabled", false);
                    $('#Follow_Campaign_Code').attr("disabled", false);
                    $('#FollowRecordNotes').attr("disabled", false);
                    if ($('#Reconciliation_Type_ID').val() == 64) {
                        $("#Follow_Activity_Type_ID option[value='2']").removeAttr("disabled");
                        $("#Follow_Activity_Type_ID option[value='3']").removeAttr("disabled");
                    }
                }
            }

            // get all values from dropdown menu
            function getDropDownValues(menuId) {
                var i = 0;
                var values = [];
                $(menuId + " option").each(function () {
                    i = $(this).val();
                    values.push(i);
                });
                return values;
            }

            // enables values when passed menu ID and array of values
            function enableMenuValues(dropDownMenu, dropDownMenuValues) {
                var i;
                for (i = 0; i < dropDownMenuValues.length; i++) {
                    $(dropDownMenu + " option[value='" + dropDownMenuValues[i] + "']").removeAttr("disabled");
                }
            }

            // disbales all options for a dropdown menu
            function disableMenuValues(menuId) {
                    var i = 0;
                    $(menuId + " option").each(function () {
                        i = $(this).val();
                        $("" + menuId + " option[value='" + i + "']").attr("disabled", "disabled");
                    });
                }

            var statusList = [];
            var dispositionList = [];

            // gets the available statuses by querying the selected activity value
            function getStatusRules() {
                var rulesUrl = "/Wizzer/GetRulesResult/" + $('#Activity_Type_ID').val() + "/" + 0 + "/" + 0;
                $.ajax({
                    type: "GET",
                    url: rulesUrl,
                    dataType: "json",
                    contentType: "application/json; charset=UTF-8",
                    success: function(data) {
                        statusList = data.Status;
                        enableMenuValues('#Record_Activity_Status_ID', statusList);
                    },
                    error: function(xhr, textStatus, errorThrown) {
                        alert("ERROR: " + textStatus + ", status: " + xhr.status + "\nError: " + errorThrown);
                    }                   
                 });
            }

            // gets available dispositions by querying the activity and the status values
            function getDispositionRules() {
                var rulesUrl = "/Wizzer/GetRulesResult/" + $('#Activity_Type_ID').val() + "/" + $('#Record_Activity_Status_ID').val() + "/" + 0;
                $.ajax({
                    type: "GET",
                    url: rulesUrl,
                    dataType: "json",
                    contentType: "application/json; charset=UTF-8",
                    success: function (data) {
                        dispositionList = data.LeadDisposition;
                        enableMenuValues('#Reconciliation_Type_ID', dispositionList);
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        alert("ERROR: " + textStatus + ", status: " + xhr.status + "\nError: " + errorThrown);
                    }
                });
            }

           // disableInput('#saveBtn', 1);

            // queries activity, status, disposition to check if a followup is required
           function checkForFollowUp() {
                var rulesUrl = "/Wizzer/CheckForFollowUp/" + $('#Activity_Type_ID').val() + "/" + $('#Record_Activity_Status_ID').val() + "/" + $('#Reconciliation_Type_ID').val();
                $.ajax({
                    type: "GET",
                    url: rulesUrl,
                    dataType: "json",
                    contentType: "application/json; charset=UTF-8",
                    success: function(data) {
                        var followUpType = data.FollowUpType;
                        var followUpRequired = data.FollowUp;
                        if (followUpRequired == true) {
                           // alert(followUpType);
                            $('#Follow_Up_Date').attr("disabled", false);
                            $('#Follow_Up_Time').attr("disabled", false);
                            $('#Follow_Activity_Type_ID').attr("disabled", false);
                            $('#Follow_User_ID').attr("disabled", false);
                            $('#Follow_Campaign_Code').attr("disabled", false);
                            $('#FollowRecordNotes').attr("disabled", false);
                        }
                        if (followUpRequired == false) {
                            resetSelection('#Auto_Email');                            
                            $("#Auto_Email option[value='58']").attr("disabled", true);
                            $('#Follow_Up_Date').attr("disabled", true);
                            $('#Follow_Up_Time').attr("disabled", true);
                            $('#Follow_Activity_Type_ID').attr("disabled", true);
                            $('#Follow_User_ID').attr("disabled", true);
                            $('#Follow_Campaign_Code').attr("disabled", true);
                            $('#FollowRecordNotes').attr("disabled", true);
                            complaintActivity();
                        }
                        disableInput('#saveBtn', 0);
                        enableMenuValues('#Follow_Activity_Type_ID', followUpType);
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        alert("ERROR: " + textStatus + ", status: " + xhr.status + "\nError: " + errorThrown);
                    }
                });
            }
                  
             function disableFollowUpTable() {
                 $('#Follow_Up_Date').attr("disabled", true);
                 $('#Follow_Up_Time').attr("disabled", true);
                 $('#Follow_Activity_Type_ID').attr("disabled", true);
                 $('#Follow_User_ID').attr("disabled", true);
                 $('#Follow_Campaign_Code').attr("disabled", true);
                 $('#FollowRecordNotes').attr("disabled", true);
         }

            // resets a drop down menu to its default value
            function resetSelection(dropDownMenu) {
                 $(dropDownMenu).each(function() { this.selectedIndex = 0; });
            }

            //current activity menus
            $('#Reconciliation_Type_ID').attr("disabled", true);
            $('#Campaign_Code').attr("disabled", true);
            $('#RecordNotes').attr("disabled", true);

            //rules
            function activityMenu() {
                disableMenuValues('#Follow_Activity_Type_ID');
                disableMenuValues('#Record_Activity_Status_ID');
                $('#Auto_Email').attr("disabled", false);
                $('#Record_Activity_Status_ID').attr("disabled", false);
                $('#Include_Email').attr("disabled", false);
                $('#Reconciliation_Type_ID').attr("disabled", true);
                $('#Campaign_Code').attr("disabled", true);
                $('#RecordNotes').attr("disabled", true);
                resetSelection('#Record_Activity_Status_ID');
                resetSelection('#Reconciliation_Type_ID');                
                getStatusRules();
            }
            
            //rules
            function statusMenu() {
                disableMenuValues('#Follow_Activity_Type_ID');
                disableMenuValues('#Reconciliation_Type_ID');
                $('#Reconciliation_Type_ID').attr("disabled", false);
                $('#Campaign_Code').attr("disabled", false);
                $('#RecordNotes').attr("disabled", false);
                resetSelection('#Reconciliation_Type_ID');
                getDispositionRules();
                enableMenuValues('#Campaign_Code', getDropDownValues('#Campaign_Code'));
                complaintActivity();
            }

            function toggleDDLS() {
                var statusVal = $("#Record_Activity_Status_ID").val();
                if (statusVal == 14 || statusVal == 16 || statusVal == 15) {
                    $("#tr_callCreationDDLS").show();
                    $("#tr_callCreationDDLS2").show();
                    $("#tr_callCreationDDLS3").show();
                }
                else {
                    $("#tr_callCreationDDLS").hide();
                    $("#tr_callCreationDDLS2").hide();
                    $("#tr_callCreationDDLS3").hide();
                }
            }
            $("#Reconciliation_Type_ID").change(function () {
                var dispoValue = $(this).val();
                if (dispoValue == 39) {
                    $("#FollowUpTable").hide();
                    $("#AddFollowUpFromDisp").show();
                }
                else {
                    if (!$('#PreFill').attr('checked'))
                        $("#FollowUpTable").show();
                    $("#AddFollowUpFromDisp").hide();
                }               
                    checkForFollowUp();                
                   // complaintActivity();
                
                

            });
            
            $("#DistributionDiv").hide();
            $("#Reconciliation_Type_ID").change(function () {
                if ($("#Reconciliation_Type_ID").val() == 36) {
                    $("#DistributionDiv").show();
                }
                else {
                    $("#DistributionDiv").hide();
                }
            });
            $("#Follow_Up_Date").datepicker();
            $("#Follow_Up_Date").mask("99/99/9999");
            $("#ActTable tr:last td").css({ 'border': 'none' });
            IsActive($('#Plan_Transaction_ID').val());
            $('#Plan_Transaction_ID').change(function () {
                IsActive($('#Plan_Transaction_ID').val());
            });
            $("#SendInfo").click(function () {
                $("#FollowUpTable").show();
            });
            $("#CancelApp").click(function () {
                $("#FollowUpTable").show();
            });
            $("#PreFill").click(function () {
                if ($("#PreFill").is(":checked")) {
                    $("#FollowUpTable").hide();
                } else {
                    $("#FollowUpTable").show();
                }
            });
            $("#SendApp").click(function () {
                if ($("#SendApp").is(":checked")) {
                    $("#FollowUpTable").hide();
                } else {
                    $("#FollowUpTable").show();
                }
            });
            $("#SendApp2").click(function () {
                if ($("#SendApp2").is(":checked")) {
                    $("#FollowUpTable").hide();
                } else {
                    $("#FollowUpTable").show();
                }
            });
           function disableInput(element, val) {
                if (val == 1) {
                    $(element).attr('disabled', 'disabled');
                }
                else {
                    $(element).removeAttr('disabled');
                }
            }

            $("#saveBtn").click(function () {
                disableInput('#saveBtn', 1);
                var recordTyp = $('#RecordType').val();
                var DueDate = $('#NextDueDate').val();
                var ReconValue = $('#Reconciliation_Type_ID').val();
                var FollowUpDateValue = $('#Follow_Up_Date').val();
                var RecordAct = $('#Record_Activity_Status_ID').val();
                var Activity = $('#Activity_Type_ID').val();
                var Email = $('#RecordEMail').val();
                var statusVal = $("#Record_Activity_Status_ID").val();


                if ($("#Reconciliation_Type_ID").val() == 64) {
                 if ($("#Follow_Up_Date").val() != '') {
                        if ($("#Follow_Up_Time").val() != 'N\\A') {
                            if ($("#Follow_Activity_Type_ID").val() == -1) {
                                alert("Please select appointment");
                                disableInput('#saveBtn', 0);
                                return false;
                            }
                        }
                        else {
                            alert("Please provide required field follow up time.");
                            disableInput('#saveBtn', 0);
                            return false;
                        }
                    }
                    else {
                        alert("Please provide required field follow up date.");
                        disableInput('#saveBtn', 0);
                        return false;
                    }
                }

                if ($("#Include_Email").is(":checked")) {
                    if (Email == "" || Email == null) {
                        alert("Please enter valid EMail Address.");
                        disableInput('#saveBtn', 0);
                        return false;
                    }
                }
                if ($("#Reconciliation_Type_ID").val() == 51 || $("#Reconciliation_Type_ID").val() == 2
                || $("#Reconciliation_Type_ID").val() == 42 || $("#Reconciliation_Type_ID").val() == 62
                || $("#Reconciliation_Type_ID").val() == 38 || $("#Reconciliation_Type_ID").val() == 20) {
                    if ($("#Auto_Email").val() == 31 || $("#Auto_Email").val() == 32) {
                        alert('Cannot select this disposition with current selected email.');
                        disableInput('#saveBtn', 0);
                        return false;
                    }
                    if ($("#Follow_Up_Date").val() != '') {
                        alert('Cannot select appointment date for this disposition.');
                        disableInput('#saveBtn', 0);
                        return false;
                    }

                    if ($("#Follow_Up_Time").val() != 'N\\A') {
                        alert('Cannot select appointment time for this disposition.');
                        disableInput('#saveBtn', 0);
                        return false;
                    }

                    alert($("#Follow_Activity_Type_ID").val());
                    if ($("#Follow_Activity_Type_ID").val() != -1) {
                        alert("Cannot select appointment type for this disposition.");
                        disableInput('#saveBtn', 0);
                        return false;
                    }
                }
                //If MedSuppAppt or MedSuppApptReminder is selected, follow up
                //time must be set  
                if ($("#Auto_Email").val() == 57) {
                    if ($("#Follow_Up_Date").val() != '') {
                        if ($("#Follow_Up_Time").val() != 'N\\A') {
                            if ($("#Follow_Activity_Type_ID").val() == -1) {
                                alert("Please select follow up activity");
                                disableInput('#saveBtn', 0);
                                return false;
                            }
                        }
                        else {
                            alert("Please provide required field follow up time.");
                            disableInput('#saveBtn', 0);
                            return false;
                        }
                    }
                    else {
                        alert("Please provide required field follow up date.");
                        disableInput('#saveBtn', 0);
                        return false;
                    }
                }
                else if ($("#Activity_Type_ID").val() < 1) {
                    alert('Please select activity!');
                    disableInput('#saveBtn', 0);
                    return false;
                }
                if ($("#Activity_Type_ID").val() != 10 && RecordAct < 2) {
                    alert('Please select status!');
                    disableInput('#saveBtn', 0);
                    return false;
                }
                if ($("#Campaign_Code").val() == 0) {
                    alert('Please select campaign code!');
                    disableInput('#saveBtn', 0);
                    return false;
                }
                if ($("#Follow_Up_Date").val() != ''){
                    if ($("#Follow_User_ID").val() == 49) {
                        alert('You must select a follow up user');
                        disableInput('#saveBtn', 0);
                        return false;
                    }
                    if ($("#Follow_Activity_Type_ID").val() < 1) {
                        alert('You must select a follow up activity');
                        disableInput('#saveBtn', 0);
                        return false;
                    }
                    if ($("#Follow_Campaign_Code").val() == 0) {
                        alert('Please select a follow up campaign code!');
                        disableInput('#saveBtn', 0);
                        return false;
                    }
                }

                if (ReconValue <= 0) {
                    alert('Please choose a Disposition!');
                    disableInput('#saveBtn', 0);
                    return false;
                }
                    //Cannot set appointments for certain dispositions
            
                else {
                    if ((recordTyp == 1 || recordTyp == 2 || recordTyp == 3) && $("#Activity_Type_ID").val() != 10 && !$("#SendApp2").is(":checked") && !$("#SendApp").is(":checked") && !$("#PreFill").is(":checked")) {

                        if (ReconValue > 0 || FollowUpDateValue != '' || DueDate != '' || $("#SendApp2").is(":checked") || $("#SendApp").is(":checked") || $("#PreFill").is(":checked")) { 
                            
                            return true;
                        }

                        alert('You must choose a Disposition or Create Follow Up');
                        disableInput('#saveBtn', 0);
                        return false;
                    }
                    else { 
                        return true;
                    }
                }
            });

//             $('#Image1').click(function () {
//                disableInput('#Image1', 1);
//            });

            $("#Modal_Follow_Up_Date").datepicker();
            $("#Modal_Follow_Up_Date").mask("99/99/9999");

            $('#AddFollowUp, #AddFollowUpFromDisp').click(function (e) {
                e.preventDefault();
                // example of calling the confirm function
                // you must use a callback function to perform the "yes" action
                confirmFollow("Continue", function () {
                    alert("OK");
                });
            });

            $("#createfollowup").submit(function (event) {
                // Stop form from submitting normally
                event.preventDefault();
                disableInput('#Image1', 1);

                var followform = $("#createfollowup"),
                url = followform.attr("action"),
                formvar1 = followform.find("select[name='Modal_Plan_Transaction_ID']").val(),
                formvar2 = followform.find("input[name='Modal_Follow_Up_Date']").val(),
                formvar3 = followform.find("select[name='Modal_Follow_Up_Time']").val(),
                formvar4 = followform.find("select[name='Modal_Follow_Activity_Type_ID']").val(),
                formvar5 = followform.find("select[name='Modal_Follow_User_ID']").val(),
//                formvar6 = followform.find("select[name='Modal_Follow_Priority_Level']").val(),
                formvar7 = followform.find("textarea[name='Modal_FollowRecordNotes']").val(),
                formvar8 = followform.find("textarea[name='Modal_Follow_Up_Notes']").val(),
                formvar9 = followform.find("select[name='Modal_Follow_Record_Activity_Status_ID']").val(),
                formvar10 = followform.find("select[name='Modal_Follow_Campaign_Code']").val();

               var validForm = true;
                if (formvar2 == '') {
                    alert('You must select a follow up date');
                    validForm = false;
                }
                if (formvar5 == 49) {
                    alert('You must select a follow up user');
                    validForm = false;
                }
                if (formvar4 < 1) {
                    alert('You must select a follow up activity');
                    validForm = false;
                }
                if (formvar10 == 0) {
                    alert('Please select a follow up campaign code!');
                    validForm = false;
                }

                if (validForm){

                    var posting = $.post(url, {
                        Plan_Transaction_ID: formvar1,
                        Follow_Up_Date: formvar2,
                        Follow_Up_Time: formvar3,
                        Follow_Activity_Type_ID: formvar4,
                        Follow_User_ID: formvar5,
    //                    Follow_Priority_Level: formvar6,
                        FollowRecordNotes: formvar7,
                        Follow_Up_Notes: formvar8,
                        Record_Activity_Status_ID_FollowUp: formvar9,
                        Modal_Follow_Campaign_Code: formvar10
                    }, function (data) {
                        $.modal.close();
                    });
                    // Submit the post
//                    posting.done();
                }
                 else { disableInput('#Image1', 0); }
            });
        });

        function confirmFollow(message, callback) {
            $('#FollowModalContent').modal({
                overlayCss: {
                    backgroundColor: '#000',
                    cursor: 'wait'
                }, containerCss: {
                    height: 340,
                    width: 800,
                    backgroundColor: '#fff',
                    border: '3px solid #ccc'
                }
            });
        }



        function PrintDoc(e, r) {
            //alert($('#Preview:checked').val());
            //            if (e > 0) {
            //                if ($('#Preview:checked').val() != null) {
            // Insert code here.
            window.open("../PrintLetter/" + e + "/" + r);
            //                }
            //                else {
            //                    Print(e, r);
            //                }
            //            }
        }

        function Print(e, r) {
            var isvalid;    // hit db
            $.ajax(
            {
                type: "POST",
                url: "/Activity/PrintLetterHide/" + e + "/" + r,
                success: function (result) {
                    alert("Printing Succeded");
                },
                error: function (error) {
                    alert("Printing Failed");
                }
            });
            return isvalid;
        }
        function IsActive(plantranid) {
            var isvalid;    // hit db
            $.ajax(
            {
                type: "POST",
                url: "/Transaction/IsActive/" + plantranid,
                success: function (result) {
                    if (result == 'True') {
                        ToggleCheckBoxes(true);
                    } else {
                        ToggleCheckBoxes(false);
                    }
                },
                error: function (error) {
                    alert(error);
                }
            });
            return isvalid;
        }

        function CheckPlanForValid() {
            alert($("#createfollowup").find('#Plan_Transaction_ID').val());
        }

        function ToggleCheckBoxes(val) {
            if (val) {
                if (confirm("The Plan Transaction you have selected cannot be modified. Do you wish to Create a New Plan Transaction")) {
                    window.location = "/Record/Details/<%= Model.Record_ID %>"
                }
                else {
                }

            }
            $("#createfollowup").find('#SendInfo').attr("disabled", val);
            $("#createfollowup").find('#SendApp').attr("disabled", val);
            $("#createfollowup").find('#SendApp2').attr("disabled", val);
            $("#createfollowup").find('#PreFill').attr("disabled", val);
        }
        //if there is no appointment set
        var dueDate = '<%=ViewData["Future_Appointment_Date"].ToString() %>';
        if (dueDate == "") {
            $(document).ready(function () {
                
                $("#Auto_Email option[value='58']").attr("disabled", "disabled");
            });
        }

        var opportunity = '<%=ViewData["Opportunity"] %>';
        var modalPremium = '<%=ViewData["Modal_Premium"]%>';
        var mode = '<%=ViewData["PaymentMode"] %>';
        var carrier = '<%=ViewData["Carrier"] %>';
        var subplan = '<%= ViewData["SubPlan"]%>';
        var effdate = '<%= ViewData["Eff_Date"] %>';
        if (modalPremium == "" || mode == "" || carrier == "" || subplan == "" || effdate == "" || opportunity == "" || modalPremium == null || mode == null || carrier == null || subplan == null || effdate == null || opportunity == null ) {
            $(document).ready(function () {
                $("#Auto_Email option[value='70']").attr("disabled", "disabled");
                $("#Auto_Email option[value='71']").attr("disabled", "disabled");
                $("#Auto_Email option[value='72']").attr("disabled", "disabled");
            });
        }
        
                                            
                                          
    </script>
    <div id="FollowModalContent" style="display: none; padding: 3px;">
        <% using (Html.BeginForm("CreateFollowUp", "Record", new { id = Model.Record_ID }, FormMethod.Post, new { id = "createfollowup" }))
           {%>
        <div id="Div1" class="client-outer" style="background-color: #a7b5dc;">
            <div class="section-title">
                Follow Up Activity Details:
            </div>
            <div class="section-inner">
                <table class="section" cellpadding="0" cellspacing="0" style="margin: 10px;" width="100%">
                    <tr>
                        <td class="label" width="150px">
                            Follow Up Opportunity:
                        </td>
                        <td>
                            <%= Html.DropDownList("Modal_Plan_Transaction_ID", (SelectList)ViewData["Plan_Transaction_ID"])%>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            Follow Up Date:
                        </td>
                        <td>
                            <%= Html.TextBox("Modal_Follow_Up_Date", "", new { style = "width: 80px" })%>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            Follow Up Time:
                        </td>
                        <td>
                            <%= Html.DropDownList("Modal_Follow_Up_Time", (SelectList)ViewData["Follow_Up_Time"])%>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            Follow Up Activity:
                        </td>
                        <td>
                            <%= Html.DropDownList("Modal_Follow_Activity_Type_ID", (SelectList)ViewData["Follow_Activity_Type_ID"])%>
                        </td>
                    </tr>
                    <%--<tr>
                        <td class="label">
                            Follow Up Status:
                        </td>
                        <td>
                            <%= Html.DropDownList("Modal_Follow_Record_Activity_Status_ID", (SelectList)ViewData["Record_Activity_Status_ID_FollowUp"])%>
                        </td>
                    </tr>--%>
                    <tr>
                        <td class="label">
                            Follow Set For:
                        </td>
                        <td>
                            <%= Html.DropDownList("Modal_Follow_User_ID", (SelectList)ViewData["Follow_User_ID"])%>
                            <span id="hasActivity"></span>
                        </td>
                    </tr>
                    <tr>
                        <%-- <td class="label">
                            Follow Up Priority:
                        </td>
                        <td>
                            <%= Html.DropDownList("Modal_Follow_Priority_Level", (SelectList)ViewData["Follow_Priority_Level"])%>
                        </td>--%>
                        <td class="label">
                            Campaign Code:
                        </td>
                        <td>
                            <%= Html.DropDownList("Modal_Follow_Campaign_Code", (SelectList)ViewData["CampaignDDL"])%>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                        </td>
                        <td>
                            <%= Html.DropDownList("Modal_FollowRecordNotes", (SelectList)ViewData["FollowRecordNotes"])%>
                            <img src="../../Content/images/page_white_paste.png" alt="Paste" style="border: none;
                                vertical-align: middle;" id="copyNoteModal" />
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            Follow Up Notes:
                        </td>
                        <td>
                            <%= Html.TextArea("Modal_Follow_Up_Notes", new { style = "width: 300px;" })%>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <br />
        <div style="text-align: center;">
            <input type="image" src="../../Content/icons/check.png" title="Create New Follow Up"
                value="Create" id="Image1" style="vertical-align: middle;" />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <img title="Cancel" src="../../Content/icons/close.png" id="Img1" onclick="$.modal.close()"
                style="vertical-align: middle; margin-bottom: 3px;" alt="Cancel" />
        </div>
        <%} %>
    </div>
    <% LMS.Data.Record record = (LMS.Data.Record)ViewData["Record"]; %>
    <%= Html.Hidden("RecordEMail",ViewData["Email_Address"]) %>
    <div id="ClientFactsDiv" class="client-outer">
        <div class="section-title">
            Client Details:
        </div>
        <div class="section-inner">
            <table class="section" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="label" width="200px">
                        Name:
                    </td>
                    <td width="200px">
                        <%= Html.Encode(record.First_Name + " " + record.Last_Name) %>
                    </td>
                    <td class="label" width="200px">
                        Home Phone:
                    </td>
                    <td width="200px">
                        <%= Html.Encode(record.Home_Phone) %>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Address:
                    </td>
                    <td>
                        <%= Html.Encode(record.Address_1) %>
                    </td>
                    <td class="label">
                        Work Phone:
                    </td>
                    <td>
                        <%= Html.Encode(record.Work_Phone) %>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <%= Html.Encode(record.Address_2) %>
                    </td>
                    <td class="label">
                        Cell Phone:
                    </td>
                    <td>
                        <%= Html.Encode(record.Cell_Phone) %>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        City, State Zip:
                    </td>
                    <td>
                        <%= Html.Encode(record.City + ", " + record.State.Abbreviation + " " + record.Zipcode) %>
                    </td>
                    <td class="label">
                        Fax:
                    </td>
                    <td>
                        <%= Html.Encode(record.Fax) %>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        County:
                    </td>
                    <td>
                        <%= Html.Encode(record.County) %>
                    </td>
                    <td class="label">
                        Source:
                    </td>
                    <td>
                        <%= Html.Encode(record.Source_Code.DropDownName) %>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Email Address:
                    </td>
                    <td>
                        <a href="mailto:<%= Html.Encode(record.Email_Address) %>">
                            <%= Html.Encode(record.Email_Address) %></a>
                    </td>
                    <td class="label">
                        Agent:
                    </td>
                    <td>
                        <%= Html.Encode(record.Agent.FullName) %>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        DOB:
                    </td>
                    <td>
                        <%= Html.Encode(string.Format("{0:d}",record.DOB)) %>
                    </td>
                    <td class="label">
                        Assistant Agent:
                    </td>
                    <td>
                        <%= Html.Encode(record.Assistant_Agent.FullName) %>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Sex:
                    </td>
                    <td>
                        <%= Html.Encode(record.Sex) %>
                    </td>
                    <td class="label">
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <br />
    <%= Html.Hidden("NextDueDate",ViewData["Next_Activity_Date"]) %>
    <%= Html.Hidden("RecordType", ViewData["Record_Type_ID"])%>
    <%= Html.Hidden("FutureAppointment", ViewData["Future_Appointment_Date"])%>

    <% using (Html.BeginForm())
       { %>
    <div id="ActivityDiv" class="client-outer" style="background-color: #e0e4ee;">
        <div class="section-title">
            Activity Details:
        </div>
        <div class="section-inner">
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td valign="top" width="50%">
                        <fieldset>
                            <legend>Current Activity</legend>
                            <table>
                                <tr>
                                    <td class="label" width="200px">
                                        Opportunity:
                                    </td>
                                    <td width="200px">
                                        <%= Html.DropDownList("Plan_Transaction_ID")%>
                                    </td>
                                    <%--<td class="label" width="200px">
                                        Call of Week
                                    </td>
                                    <td>
                                        <%= Html.CheckBox("Call_of_Week",ViewData["Call_of_Week"])  %>
                                    </td>--%>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label" width="200px">
                                        Activity:
                                    </td>
                                    <td width="200px">
                                        <%= Html.DropDownList("Activity_Type_ID")%>
                                    </td>
                                    <%--<td class="label" id="SR">
                                        Self Rating:
                                    </td>
                                    <td>
                                        <%= Html.DropDownList("Agent_Rating") %>
                                    </td>--%>
                                    <td class="label" id="MR">
                                        <%  
           if (user.Security_Level_ID != 1 && user.Security_Level_ID != 4)
                                        %>
                                        Manager Rating:
                                    </td>
                                    <td>
                                        <%  if (user.Security_Level_ID != 1 && user.Security_Level_ID != 4) %>
                                        <%= Html.DropDownList("Manager_Rating") %>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label" width="200px">
                                        <%if (ViewData["OptOutEmail"].Equals(false))%>
                                        Include Email:
                                    </td>
                                    <td width="200px">
                                        <%if (ViewData["OptOutEmail"].Equals(false)) %>
                                        <%= Html.CheckBox("Include_Email")%><%if (ViewData["OptOutEmail"].Equals(false)) %><%= Html.DropDownList("Auto_Email") %>
                                       

                                         
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label" width="200px">
                                        Status:
                                    </td>
                                    <td width="200px">
                                        <%= Html.DropDownList("Record_Activity_Status_ID")%>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <%-- <td class="label" width="200px">
                                        Priority:
                                    </td>
                                    <td width="200px">
                                        <%= Html.DropDownList("Priority_Level")%>
                                    </td>--%>
                                    <td class="label" width="200px">
                                        Campaign Code:
                                    </td>
                                    <td>
                                        <%= Html.DropDownList("Campaign_Code", (SelectList)ViewData["CampaignDDL"])%>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label" width="200px">
                                    </td>
                                    <td width="200px">
                                        <%= Html.DropDownList("RecordNotes")%>
                                        <img src="../../Content/images/page_white_paste.png" alt="Paste" style="border: none;
                                            vertical-align: middle;" id="copyNote" />
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label" width="200px" valign="top">
                                        Notes:
                                    </td>
                                    <td width="200px">
                                        <%= Html.TextArea("Notes", StringHelper.DecryptData(Model.Notes), new { style = "width: 300px;", rows = "2", maxlength = "1000" })%>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label" width="200px" valign="top">
                                        Lead Disposition:
                                    </td>
                                    <td width="200px">
                                        <%= Html.DropDownList("Reconciliation_Type_ID")%>
                                    </td>
                                    <td>
                                        <img src="../../content/icons/clock.png" id="AddFollowUpFromDisp" title="Create Follow Up"
                                            alt="Create Activity" style="border: 0px; vertical-align: middle; display: none" />
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr id="tr_callCreationDDLS" class="hide">
                                    <td class="label" width="200px" valign="top">
                                        Critical Topic Coverage:
                                    </td>
                                    <td>
                                        <%= Html.DropDownList("ctc_DDL", (SelectList)ViewData["CriticalTopicCoverageDDL"])%>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr id="tr_callCreationDDLS2" class="hide">
                                    <td class="label" width="200px" valign="top">
                                        Sales Rating:
                                    </td>
                                    <td>
                                        <%= Html.DropDownList("SEEID_DDL", (SelectList)ViewData["SalesRatingDDL"])%>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr id="tr_callCreationDDLS3" class="hide">
                                    <td class="label" width="200px" valign="top">
                                        Admin Documentation Rating:
                                    </td>
                                    <td>
                                        <%= Html.DropDownList("AdminDocumentationID_DDL", (SelectList)ViewData["AdminDocumentationRatingDDL"])%>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td colspan="5">
                                        <div id="DistributionDiv">
                                            AEP Reminder:
                                            <%=Html.CheckBox("AEP_Reminder",ViewData["AEP_Reminder"]) %>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                    <td valign="top" width="50%">
                        <div id="FollowUpTable">
                            <fieldset>
                                <legend>Follow Up Activity</legend>
                                <table>
                                    <tr>
                                        <td class="label" width="200px">
                                            Follow Up Date:
                                        </td>
                                        <td width="200px">
                                            <%= Html.TextBox("Follow_Up_Date","", new { style = "width: 80px" })%>
                                            <img src="../../content/icons/clock.png" id="AddFollowUp" title="Create Follow Up"
                                                alt="Create Activity" style="border: 0px; vertical-align: middle;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="label" width="200px">
                                            Follow Up Time:
                                        </td>
                                        <td width="200px">
                                            <%= Html.DropDownList("Follow_Up_Time")%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="label" width="200px">
                                            Follow Up Activity:
                                        </td>
                                        <td width="200px">
                                            <%= Html.DropDownList("Follow_Activity_Type_ID")%>
                                        </td>
                                    </tr>
                                    <%-- <tr>
                                        <td class="label" width="200px">
                                            Follow Up Status:
                                        </td>
                                        <td width="200px" colspan="3">
                                            <%= Html.DropDownList("Record_Activity_Status_ID_FollowUp")%>
                                        </td>
                                    </tr>--%>
                                    <tr>
                                        <td class="label" width="200px">
                                            Follow Set For:
                                        </td>
                                        <td width="200px">
                                            <%= Html.DropDownList("Follow_User_ID")%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <%--<td class="label" width="200px">
                                            Follow Up Priority:
                                        </td>
                                        <td width="200px">
                                            <%= Html.DropDownList("Follow_Priority_Level")%>
                                        </td>--%>
                                        <td class="label" width="200px">
                                            Campaign Code:
                                        </td>
                                        <td width="200px" colspan="3">
                                            <%= Html.DropDownList("Follow_Campaign_Code", (SelectList)ViewData["CampaignDDL"])%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="label" width="200px">
                                        </td>
                                        <td width="200px">
                                            <%= Html.DropDownList("FollowRecordNotes")%>
                                            <img src="../../Content/images/page_white_paste.png" alt="Paste" style="border: none;
                                                vertical-align: middle;" id="copyNoteFollowUp" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="label" width="200px">
                                            Follow Up Notes:
                                        </td>
                                        <td width="200px">
                                            <%= Html.TextArea("Follow_Up_Notes", "", new { style = "width: 300px;", rows = "2", maxlength = "1000" })%>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </div>
                    </td>
                </tr>
                <% if (LMS.Data.Repository.UserRepository.GetUser(User.Identity.Name).Security_Level_ID == 5 || LMS.Data.Repository.UserRepository.GetUser(User.Identity.Name).Security_Level_ID == 2)
                   { %>
                <tr>
                    <td colspan="2" align="left">
                        <fieldset>
                            <legend>Correspondence:</legend>Correspondence:&nbsp;&nbsp;&nbsp;&nbsp;<%= Html.DropDownList("Letter_ID")%>&nbsp;&nbsp;&nbsp;&nbsp;
                            <%= Html.CheckBox("Preview") %>
                            <%= Html.Hidden("RecordID", ViewData["RecordIDValue"])%>
                            Preview&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <img src="../../Content/icons/printer.png" style="height: 16px; margin-top: 2px;"
                                id="PrintLetter" onclick="PrintDoc($('#Letter_ID').val(),$('#RecordID').val());"
                                alt="Print" /></fieldset>
                    </td>
                </tr>
                <%} %>
                <tr>
                    <td style="padding-left: 200px;">
                        <%= Html.CheckBox("SendInfo", new { onclick = "$('#SendApp').attr('checked', '');$('#SendApp2').attr('checked', '');$('#CancelApp').attr('checked', '');$('#PreFill').attr('checked', '');" })%>
                        Send Info/Quote
                    </td>
                    <td style="padding-right: 200px;">
                        <%= Html.CheckBox("SendApp", new { onclick = "$('#SendApp2').attr('checked', '');$('#SendInfo').attr('checked', '');$('#CancelApp').attr('checked', '');$('#PreFill').attr('checked', '');" })%>
                        Send Application - Fulfillment
                    </td>
                </tr>
                <tr>
                    <td style="padding-left: 200px;">
                        <%= Html.CheckBox("PreFill", new { onclick = "$('#SendApp2').attr('checked', '');$('#SendInfo').attr('checked', '');$('#SendApp').attr('checked', '');$('#CancelApp').attr('checked', '');" })%>
                        Pre-Fill Application
                    </td>
                    <td style="padding-right: 200px;">
                        <%= Html.CheckBox("SendApp2", new { onclick = "$('#SendApp').attr('checked', '');$('#SendInfo').attr('checked', '');$('#CancelApp').attr('checked', '');$('#PreFill').attr('checked', '');" })%>
                        Send Application - Agent
                    </td>
                </tr>
                <tr>
                    <td style="padding-left: 200px;">
                    </td>
                    <td style="padding-right: 200px;">
                        <%= Html.CheckBox("CancelApp", new { onclick = "$('#SendApp2').attr('checked', '');$('#SendApp').attr('checked', '');$('#SendInfo').attr('checked', '');$('#PreFill').attr('checked', '');" })%>
                        Application Sent - Cancelled
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <%if (showButton)
                          {%>
                        <input type="image" src="../../Content/icons/check.png" alt="Save" title="Save" id="saveBtn" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <%} %>
                        <a href="../../Record/Details/<%= record.Record_ID %>">
                            <img src="../../content/icons/close.png" alt="Cancel" title="Cancel" style="border: 0px;
                                vertical-align: top;" />
                        </a>
                    </td>
                </tr>
            </table>
            <br />
            <% Html.RenderPartial("Activities", ViewData["DisplayRecordActivitiesList"]); %>
            <br />
        </div>
    </div>
    <%} %>
</asp:Content>
