<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<LMS.Data.sp_License_List_By_StateResult>>" %>

<%@ Import Namespace="Helpers" %>
   <div class="client-outer">
    <div class="client-title">
         [ <%= Html.Encode(Model.Count()) %> Licenses ]
    </div>
    <div class="client-inner">
    <table class="tablesorter" width="100%" cellpadding="0" cellspacing="0" id="sortable2">
        <thead>
            <tr>
            <th>Action</th>
            <th>Name</th>
            <th>State</th>
            <th>License #</th>
            <th>Effective Date</th>
            <th>Renewal Date</th>
            <th>Line of Authority</th>
            </tr>
        </thead>
        <tbody>
        <% foreach (LMS.Data.sp_License_List_By_StateResult rs in Model)
           { %>
        <tr>
            <td>
            <% LMS.Data.User user = LMS.Data.Repository.UserRepository.GetUser(Page.User.Identity.Name); %>
            <%if (LMS.Data.Repository.UserRepository.HasSecurityLevel(user, 10))
              {%> 
                <%= Html.ActionLink("Edit", "Edit", new { id = rs.License_ID }, new { target = "_blank", style = "target-new: tab ! important" })%>&nbsp;
            <%} %>
                <%= Html.ActionLink("View", "Details", new { id = rs.License_ID }, new { target = "_blank", style = "target-new: tab ! important"  })%>
            </td>
            <td>
            <%if (rs.Agent_Type_ID == 1)
              {%>
                <%= Html.Encode(rs.First_Name + " " + rs.Last_Name)%>
            <%}
              else
              {%>
                <%= Html.Encode(rs.Agency_Name)%>
            <%} %>
            </td>
            <td>
                <%= Html.Encode(rs.States) %>
            </td>
            <td>
                <%= Html.Encode(rs.License_Number) %>
            </td>
            <td>
                 <% if (rs.Effective_Date != null)
                   {%>
                       <%=Html.Encode(rs.Effective_Date.Value.ToShortDateString())%>
                <% } %>
            </td>
            <td>
               <% if (rs.Renewal_Date != null && DateTime.Compare(DateTime.Parse(rs.Renewal_Date.ToString()), DateTime.Today) < 0)
                   {%>
                   <font color="red">
                   <%= Html.Encode(rs.Renewal_Date.Value.ToShortDateString()) %>
                   </font>
                <% }
                   else if (rs.Renewal_Date != null)
                   { %>
                    <%= Html.Encode(rs.Renewal_Date.Value.ToShortDateString()) %>
                <% } %>
            </td>
            <td>
                <%= Html.Encode(rs.Line_Of_Insurance) %>
            </td>
        </tr>
        <%} %>
        </tbody>
    </table>
    </div>
</div>