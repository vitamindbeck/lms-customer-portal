<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Licensing.Master" Inherits="System.Web.Mvc.ViewPage<LMS.Data.License>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	License Details
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div style="text-align: left;">
<% LMS.Data.User user = LMS.Data.Repository.UserRepository.GetUser(Page.User.Identity.Name); %>
<%if (LMS.Data.Repository.UserRepository.HasSecurityLevel(user, 10))
  {%>       
        <a href="../../License/Edit/<%= Model.License_ID %>" style="text-decoration: none;">
            <img src="../../content/icons/document_edit.png" title="Edit" alt="Edit" style="border: 0px;
                vertical-align: middle;" />
        </a>
<%} %>     
</div>
    <br />
<div class="client-outer" style="background-color: #e0e4ee;">
    <div class="section-title">
        License Details
    </div>
    <div class="section-inner">
        <table class="section" cellpadding="0" cellspacing="0">
          <tr>
            <td class="label" width="200px">
                Agent/Agency:
            </td>
            <td>
                <%= ViewData["AgentName"]%>
            </td>
            <td class="label" width="200px">
                State:
            </td>
            <td>
                <%= Html.Encode(Model.States)%>
            </td>
          </tr>
          <tr>
            <td class="label">
                Effective Date:
            </td>
            <td>
                <%= Html.Encode(Model.Effective_Date.Value.ToShortDateString()) %>
            </td>
            <td class="label">
                License #:
            </td>
            <td>
                <%= Html.Encode(Model.License_Number) %>
            </td>
          </tr>
          <tr>
            <td class="label">
                Line of Authority:
            </td>
            <td>
                <%= Html.Encode(Model.Insurance_Line_Lookup.Description) %>
            </td>
            <td class="label">
                Renewal Date:
            </td>
            <td>
               <%= Html.Encode(Model.Renewal_Date.HasValue ? Model.Renewal_Date.Value.ToShortDateString() : "")%>
            </td>
          </tr>
          <tr>
            <td class="label">
                Active:
            </td>
            <td>
                <%= Html.Encode(Model.Active.HasValue && Model.Active == true ? "Yes" : "No")%>
            </td>
            <td class="label">
                Last Updated:
            </td>
            <td>
                <%= Html.Encode(Model.Last_Updated_Date) %>
                <%if (!String.IsNullOrEmpty(ViewData["UserName"].ToString()))
                  { %>
                <strong>By:</strong>
                <%} %>
                <%= ViewData["UserName"] %>
            </td>
            
          </tr>
        </table>
    </div>
</div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptContent" runat="server">
</asp:Content>
