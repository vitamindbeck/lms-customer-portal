<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<LMS.Data.User>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Edit User Account
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <script type="text/javascript">
        function CheckPlanUserID() {
            alert($('#User_ID').val());
        }
    </script>

    <div id="ClientFactsDiv" class="client-outer" style="background-color: #e0e4ee;">
        <div class="section-title">
            Edit User Account
        </div>
        <div class="section-inner">
            <% LMS.Data.User usr = (LMS.Data.User)ViewData["User"]; %>
            <% using (Html.BeginForm())
               { %>
             <table class="section" cellpadding="0" cellspacing="0">
                 <tr>
                    <td class="style7">
                        Group:
                    </td>
                    <td class="style4">
                        <%= Html.DropDownList("Group_ID")%>
                    </td>
                    <td class="style5">
                        Security Level:
                    </td>
                    <td class="style6">
                          <%= Html.DropDownList("Security_Level_ID")%>
                    </td>
                  </tr> 
                     <tr>
                    <td class="style8">
                        First Name:
                    </td>
                    <td class="style1">
                        <%=Html.TextBox("First_Name",Model.First_Name)%>
                          
                    </td>
                    <td class="style2">
                        Last Name:
                    </td>
                    <td>
                        <%=Html.TextBox("Last_Name", Model.Last_Name)%>
                    </td>
                  </tr>     
                   <tr>
                    <td class="style8">
                        User name:
                        <label style="font-size: x-small; color: #800000; font-weight: bold;">[First Name Initial & Last Name]</label>
                    </td>
                    <td class="style1">
                        <%=Html.TextBox("User_Name", Model.User_Name)%>
                     
                    </td>
                 
                    <td class="style2" >
                        Pod:
                    </td>
                    <td>
                          <%= Html.DropDownList("Pod_ID")%>
                       
                    </td>
                    
                    </tr>          
             
                      <tr>
                    <td class="style8">
                        Phone:
                    </td>
                    <td class="style1">
                        <%= Html.TextBox("Phone", Model.Phone)%>
                    </td>
                    <td class="style2">
                       Extension:
                    </td>
                    <td>
                         <%= Html.TextBox("Extension", Model.Extension)%>
                    </td>
                  </tr>    
                  <tr>
                    <td class="style1">
                        Email:
                    </td>
                    <td class="style1">
                        <%= Html.TextBox("Email_Address", Model.Email_Address)%>
                    </td>
                        <tr>
                     <td class="style8">
                        Can Assign Health Leads:
                    </td>
                    <td class="style1">
                         <%= Html.CheckBox("Auto_Assignment", Model.Auto_Assignment)%><label style="font-size: x-small; color: #660066">
                    </td>
                      <td class="style8">
                        Can Assign Life Leads:
                        </td>
                    <td class="style1">
                         <%= Html.CheckBox("Auto_Assignment_Life", Model.Auto_Assignment_Life)%><label style="font-size: x-small; color: #660066">
                    </td>
                    </tr>
                   <td class="style2">
                       Need Access to Request App Page:
                    </td>
                   <td>
                          <%= Html.DropDownList("Sales_Group_ID")%>
                            
                    </td>
                        
              
                        <td class="style8">
                        Employee Account:
                    </td>
                    <td class="style1">
                         <%= Html.CheckBox("In_AD")%><label 
                             style="font-size: x-small; color: #800000; font-weight: bold;">For employees, please keep it checked.
                         <br />
                         User accounts like call assistant users etc keep it unchecked</label>
                         </td>
                    
                  </tr>  
                   
                 <tr>
                
                  <td style="color: #990000; font-size: small; font-weight: bold;">
                     <%= Html.Encode(ViewData["Message"]) %>
                
                 </td>
                 </tr>
                
            <tr  >
            <td></td>
         <td align ="center">
        <input type="image" src="../../Content/icons/check.png" title="Save" alt="Save" id="saveBtn" />
        <a href="/SuperAdmin/ManageUser" alt="Cancel">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <img src="../../content/icons/close.png" alt="Cancel" title="Cancel" style="border: 0px;
                vertical-align: top;" />
        </a>
        </td>
        </tr>  
        </table>
    </div>
            <%} %>
</div>

    </div>
</asp:Content>