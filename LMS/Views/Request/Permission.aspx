﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>
<asp:Content ID="PermissionTitle" ContentPlaceHolderID="TitleContent" runat="server">
    No Permission
</asp:Content>
<asp:Content ID="changePasswordContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2>You do not have permission to access this page</h2>
    <p>
        Please contact Administrator or IT for accessing this page.
    </p>
</asp:Content>
