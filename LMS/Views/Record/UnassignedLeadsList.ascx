<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<LMS.Data.OtherUnassignedLeadsResult>>" %>
<% using (Html.BeginForm())
   {%>
 <table>
    <tr>
        <td>
            Assign To Agent:
        </td>
        <td>
            <%= Html.DropDownList("Agent_ID", (SelectList)ViewData["Agent_ID"], new { style = "font-size: 12px;" })%>
            &nbsp;
            <input type="submit" style="vertical-align: middle;" name="Assign" value="Assign" />
            &nbsp;&nbsp;
            <input type="submit" style="vertical-align: middle;" name="BadLeads" value="Bad Lead" />
            &nbsp;&nbsp;
            <input type="submit" style="vertical-align: middle;" name="Duplicate" value="Duplicate" />
            &nbsp;&nbsp;
            <input type="submit" style="vertical-align: middle;" name="Repeat" value="Repeat" />
            &nbsp;&nbsp;
            <input type="submit" style="vertical-align: middle;" name="Setter" value="Appointment Setter" />
            &nbsp;&nbsp;
            <input type="submit" style="vertical-align: middle;" name="Delete" value="Delete" />
        </td>
    </tr>
</table>
 <div class="client-outer">
    <div class="section-title">
        [ New Leads:&nbsp;
        <% if (Model.Count() > 0)
           { %>
        <%= Html.Encode(Model.Count())%>
        <%}
           else
           { %>
        No
        <%} %>
        Leads ]
    </div>
 
    <div class="client-inner">
    <table class="sortable" cellpadding="0" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th></th>
            <th>
                Record Name
            </th>
            <th>
                Submit Date
            </th>
            <th>
                State
            </th>
            <th>
                Source
            </th>
            <th>
                DOB
            </th>
            <th>
                Phone
            </th>
            <th>
                Req Start Date
            </th>
            <th>
                Elig.
            </th>
            <th>
                Sales Crdntr.
            </th>
        </tr>
        </thead>
        <tbody>
    <% foreach (LMS.Data.OtherUnassignedLeadsResult item in Model) 
       {
           if (item.Dups.Value)
           {%>
                <tr class="duplicate">
           <%}
           else
           { %>

                <tr>
         <%} %>
            <td width="30px">
            
                <%= Html.CheckBox("Record" + item.Record_ID.ToString(), false)%>
            </td>
            <td width="180px">
            <%if (item.Key != null)
              { %>
                <span style="background-color:Yellow;">
            <%} %>
                <%= Html.ActionLink(LMS.Helpers.StringHelper.UppercaseFirst(item.Name), "../Record/Edit", new { id = item.Record_ID }, new { target = "_blank", style = "target-new: tab ! important" })%>
            <%if (item.Key != null)
              { %>
                </span>
            <%} %>   

                <% if (item.Dups.Value)
                   { %>
                <a href="../../Admin/Duplicate/<%= item.Record_ID %>" alt="View Duplicates">
                    <img src="../../content/icons/zoom.png" alt="Cancel" title="View Duplicates" style="border: 0px;
                        height: 16px; vertical-align: top;" />
                </a>
                <%} %>
            </td>
            <td width="110px">
                <%= Html.Encode(string.Format("{0:MM/dd/yyyy hh:mm tt}",item.Created_On.Value))%>
            </td>
            <td  width="40px">
                <%= Html.Encode(item.Abbreviation) %>
            </td>
            <td width="50px">
                <%= Html.Encode(item.Source_Number) %>
            </td>
            <td width="90px">
               <%= Html.Encode((item.DOB.HasValue ? string.Format("{0:MM/dd/yyyy}",item.DOB.Value) : ""))%>
            </td>
            <td width="80px">
                <%= Html.Encode(item.Home_Phone) %>
            </td>
            <td>
                <%= Html.Encode((item.RequestedStartDate.HasValue ? string.Format("{0:MM/dd/yyyy}", item.RequestedStartDate.Value) : ""))%>
            </td>
            <td>
                <%= Html.Encode(item.Category) %>
            </td>
            <td>
            <% if (item.Online_Status_ID == 1)
               {  
                   %><span class="online"><%= Html.Encode(item.User_Name)%></span>
             <%}
               else if (item.Online_Status_ID == 2)
               {
                   %><span class="offline"><%= Html.Encode(item.User_Name)%></span>
             <%}
               else
               { 
                   %><span class="away"><%= Html.Encode(item.User_Name)%></span>
             <%} %>
            </td>
        </tr>
        
    <% } %>
        </tbody>
    </table>
    </div>
</div>
<%} %>





