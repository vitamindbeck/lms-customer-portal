<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<LMS.Data.Record>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Edit
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <script type="text/javascript" src="../../Scripts/RecordValidateSelect.js"></script>
    <script type="text/javascript">
        $(function () {

            var data_missing = false;
            $("#Alt_Address_From_Date").mask("99/99/9999");
            $("#Alt_Address_To_Date").mask("99/99/9999");
            $("#RequestedStartDate").mask("99/99/9999");
            $("#Home_Phone").mask("(999) 999-9999");
            $("#Work_Phone").mask("(999) 999-9999");
            $("#Cell_Phone").mask("(999) 999-9999");
            $("#Fax").mask("(999) 999-9999");
            $("#Alt_Phone_Number").mask("(999) 999-9999");
            $("#Eligibility_Date").mask("99/99/9999");
            $("#DOB").mask("99/99/9999");
            $("#SSN").mask("999-99-9999");
            $("#Part_A_Effective_Date").mask("99/99/9999");
            $("#Part_B_Effective_Date").mask("99/99/9999");
            $("#Original_Start_Date").mask("99/99/9999");
            $("#Drug_Plan_Original_Start_Date").mask("99/99/9999");
            $("#Quit_Date").mask("99/99/9999");
            $("#Medicare_Number").mask("999-99-9999-?***", { placeholder: "_" });

            $("#B2E").click(function () {
                if ($("#B2E").is(":checked")) {

                    $("#AdditionalDiv").hide();
                    $("#PersCompLabel").text('Company Details');
                    $("#CompanyDiv").show();
                }
                else {
                    $("#AdditionalDiv").show();
                    $("#PersCompLabel").text('Additional Information');
                    $("#CompanyDiv").hide();
                }

            });


            $("#IsMailing_Address").click(function () {
                if ($("#IsMailing_Address").is(":checked")) {


                    $("#MailAddDiv").hide();
                }
                else {

                    $("#MailAddDiv").show();

                    data_missing = true;
                }

            });

            $("#Do_Not_Contact").click(function () {
                if ($("#1_AEP_Reminder").is(':checked') && $("#Do_Not_Contact").is(':checked')) {
                    $("#1_AEP_Reminder").removeAttr('checked');
                }
            });


            if ($("#IsMailing_Address").is(":checked")) {


                $("#MailAddDiv").hide();
            }
            else {

                $("#MailAddDiv").show();
            };

            $("#saveBtn").click(function () {

                return IsValidSelect();
            });
            $("#State_ID").val("<%= Model.State_ID %>");
            $("#Alt_State_ID").val("<%= Model.Alt_State_ID %>");
            $("#Salutation_ID").val("<%= Model.Salutation_ID %>");
            $("#Suffix").val("<%= Model.Suffix %>");
            $("#Agent_ID").val("<%= Model.Agent_ID %>");
            $("#Sex").val("<%= Model.Sex %>");
            $("#Broker_ID").val("<%= Model.Broker_ID %>");
            $("#Assistant_Agent_ID").val("<%= Model.Assistant_Agent_ID %>");
            $("#Buying_Period_ID").val("<%= Model.Buying_Period_ID %>");
            $("#Buying_Period_Sub_ID").val("<%= Model.Buying_Period_Sub_ID %>");
            $("#Employment_Status_ID").val("<%= Model.Employment_Status_ID %>");
            //            $("#Occupation_ID").val("<%= Model.Occupation_ID %>");
            $("#Occupation").val("<%= Model.Occupation %>");
            $("#Marital_Status_ID").val("<%= Model.Marital_Status_ID %>");
            $("#Past_Tobacco_ID").val("<%= Model.Past_Tobacco_ID %>");
            $("#Current_Tobacco_ID").val("<%= Model.Current_Tobacco_ID %>");
            $("#Referral_Type_ID").val("<%= Model.Referral_Type_ID %>");
            $("#Record_Type_ID").val("<%= Model.Record_Type_ID %>");
            $("#Source_Code_ID").val("<%= Model.Source_Code_ID %>");
            $("#Current_Tobacco_Use_Status_ID").val("<%= Model.Current_Tobacco_Use_Status_ID %>");
            $("#Past_Tobacco_Use_Status_Id").val("<%= Model.Past_Tobacco_Use_Status_Id %>");
            $("#Original_Agent_ID").val("<%= Model.Original_Agent_ID %>");
            $("#Products_Of_Interest_ID").val("<%= Model.Products_Of_Interest_ID %>");
            $("#Record_Current_Situation_ID").val("<%= Model.Record_Current_Situation_ID %>");
            $("#Subsidy_ID").val("<%= Model.Subsidy_ID %>");
            $("#Current_Insured_Type_ID").val("<%= Model.Current_Insured_Type_ID %>");
            $("#IsMailing_Address").val("<%= Model.IsMailing_Address %>");

            $('#Part_A_Effective_Date').keydown(function () {
                $('input[name=Part_A_Effective]').attr('checked', true);
            });
            $('#Part_B_Effective_Date').keydown(function () {
                $('input[name=Part_B_Effective]').attr('checked', true);
            });


            $('#Address_1').change(function () {
                $('#txtAddress_1').val($('#Address_1').val());
            });


            $('#txtAddress_1').change(function () {
                $('#Address_1').val($('#txtAddress_1').val());
            });

            $('#Address_2').change(function () {
                $('#txtAddress_2').val($('#Address_2').val());
            });


            $('#txtAddress_2').change(function () {
                $('#Address_2').val($('#txtAddress_2').val());
            });


            $('#City').change(function () {
                $('#txtCity').val($('#City').val());
            });


            $('#txtCity').change(function () {
                $('#City').val($('#txtCity').val());
            });


            $('#Zipcode').change(function () {
                $('#txtZipcode').val($('#Zipcode').val());
            });


            $('#txtZipcode').change(function () {
                $('#Zipcode').val($('#txtZipcode').val());
            });

            $('#State_ID').change(function () {
                $('#dlState_ID').val($('#State_ID').val());
            });


            $('#dlState_ID').change(function () {
                $('#State_ID').val($('#dlState_ID').val());
            });

            $('#Alt_Address_1').change(function () {
                $('#txtAlt_Address_1').val($('#Alt_Address_1').val());
            });


            $('#txtAlt_Address_1').change(function () {
                $('#Alt_Address_1').val($('#txtAlt_Address_1').val());
            });

            $('#Address_2').change(function () {
                $('#txtAddress_2').val($('#Address_2').val());
            });


            $('#txtAlt_Address_2').change(function () {
                $('#Alt_Address_2').val($('#Alt_txtAddress_2').val());
            });

            $('#Alt_Address_2').change(function () {
                $('#txtAlt_Address_2').val($('#Alt_Address_2').val());
            });


            $('#Alt_City').change(function () {
                $('#txtAlt_City').val($('#Alt_City').val());
            });


            $('#txtAlt_City').change(function () {
                $('#Alt_City').val($('#txtAlt_City').val());
            });


            $('#Alt_Zipcode').change(function () {
                $('#txtAlt_Zipcode').val($('#Alt_Zipcode').val());
            });


            $('#txtAlt_Zipcode').change(function () {
                $('#Alt_Zipcode').val($('#txtAlt_Zipcode').val());
            });

            $('#Alt_State_ID').change(function () {
                $('#dlAlt_State_ID').val($('#Alt_State_ID').val());
            });


            $('#dlAlt_State_ID').change(function () {
                $('#Alt_State_ID').val($('#dlAlt_State_ID').val());
            });







            /*if ($("#Products_Of_Interest_ID").val() == "-1") {
            $("#Products_Of_Interest_ID").css({ backgroundColor: '#ff0' });
            data_missing = true;
            }

            if ($("#Record_Current_Situation_ID").val() == "-1") {
            $("#Record_Current_Situation_ID").css({ backgroundColor: '#ff0' });
            data_missing = true;
            }

            if ($("#DOB").val() == null || $("#DOB").val() == "") {
            $("#DOB").css({ backgroundColor: '#ff0' });
            data_missing = true;
            }*/

            if ($("#State_ID").val() == "52") {
                $("#State_ID").css({ backgroundColor: '#ff0' });
                data_missing = true;
            }

            /*if ($("#RequestedStartDate").val() == null || $("#RequestedStartDate").val() == "") {
            $("#RequestedStartDate").css({ backgroundColor: '#ff0' });
            data_missing = true;
            }*/

            if (data_missing == true) {
                $("#highlighted").show();

            }
            else {
                $("#highlighted").hide();
            }

            if ('<%= Model.B2E %>' == 'True') {
                $("#AdditionalDiv").hide();
                $("#PersCompLabel").text('Company Details');
                $("#CompanyDiv").show();
            }
            else {
                $("#AdditionalDiv").show();
                $("#PersCompLabel").text('Additional Information');
                $("#CompanyDiv").hide();
            }
            $("#tabs").tabs();



        });

        var refreshId = setInterval(function() { $('#inedittext').fadeOut("fast").load('/Record/RefreshEdit/<%= Model.Record_ID %>').fadeIn("fast"); }, 15000);

        
    </script>

    <% 
        using (Html.BeginForm())
       {%>
    <% Html.ValidationSummary("Edit was unsuccessful. Please correct the errors and try again."); %>
    <div id="highlighted" style="background-color:Yellow; width:350px;">Important data missing fields are highlighted.</div>
    <div id="inedittext" style="text-align:right;">
        <span style="background-color:#FFFF66; color:Red; font-weight:bold;">
        In edit by <%=ViewData["InEditBy"] %>
        </span>
    </div>
    <div id="tabs" style="font-size: 1em; font-family: Calibri;">
        <ul class="ui-tabs-nav">
            <li><a href="#tabs-1">Customer:
                <%= Html.Encode(Model.Customer_ID) %>
                <% if (Model.Partner != null)
                   { %>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Partner ID:
                <%= Html.Encode(Model.Partner.Customer_ID) %>
                <%} %></a></li>
            <li><a href="#tabs-2">Client Facts</a></li>
            <li>
                <div>
                    <a id="PersCompLabel" href="#tabs-3">
                        <%if (Model.B2E)
                          { %>
                        Company Details
                        <%}
                          else
                          { %>
                       Additional Information
                        <%} %>
                    </a>
                </div>
            </li>
           <li><a href="#tabs-5">Address List</a></li>
        </ul>
        <div id="tabs-1" style="background-color: #e0e4ee;">
            <table class="section" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="label">
                        Prefix:
                    </td>
                    <td>
                        <%= Html.DropDownList("Salutation_ID", (SelectList)ViewData["Salutation_ID"], new { tabindex = "1" })%>
                        &nbsp;&nbsp;&nbsp;<b>Suffix:</b>
                        <%= Html.DropDownList("Suffix", (SelectList)ViewData["Suffix"], new { tabindex = "2" })%>
                    </td>
                    <td class="label">
                        Home Phone:
                    </td>
                    <td>
                        <%= Html.TextBox("Home_Phone", Model.Home_Phone, new { style="width: 137px;", tabindex = "23" })%>
                    </td>
                    <td class="label">
                        Source Code:
                    </td>
                    <td>
                        <% if (LMS.Data.Repository.UserRepository.GetUser(User.Identity.Name).Security_Level_ID == 1 || LMS.Data.Repository.UserRepository.GetUser(User.Identity.Name).Security_Level_ID == 5)
                           { %>
                        <%= Html.Encode(Model.Source_Code.DropDownName)%>
                        <%}
                           else
                           { %>
                        <%= Html.DropDownList("Source_Code_ID", (SelectList)ViewData["Source_Code_ID"], new { tabindex = "37" })%>
                        <%} %>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Name:
                    </td>
                    <td>
                        <%= Html.TextBox("First_Name", Model.First_Name, new { style = "width: 80px", tabindex = "3", onblur = "initialCap(this);" })%>
                        <%= Html.TextBox("Middle_Initial", Model.Middle_Initial, new { style = "width: 10px", tabindex = "4", onblur = "initialCap(this);" })%>
                        <%= Html.TextBox("Last_Name", Model.Last_Name, new { style = "width: 80px", tabindex = "5", onblur = "initialCap(this);" })%>
                        <%= Html.ValidationMessage("Last_Name", "*")%>
                    </td>
                    <td class="label">
                        Work Phone:
                    </td>
                    <td>
                        <%= Html.TextBox("Work_Phone", Model.Work_Phone, new { style = "width: 80px;", tabindex = "24" })%>

                        <%= Html.TextBox("Work_Phone_Ext", Model.Work_Phone_Ext, new { style = "width: 48px;", tabindex = "25", MaxLength = "8" })%>
                    </td>
                    <td class="label">
                        Record Type:
                    </td>
                    <td>
                        <% if (LMS.Data.Repository.UserRepository.GetUser(User.Identity.Name).Security_Level_ID == 1)
                           { %>
                        <%= Html.Encode(Model.Record_Type.Description)%>
                        <%}
                           else
                           { %>
                        <%= Html.DropDownList("Record_Type_ID", (SelectList)ViewData["Record_Type_ID"], new { tabindex = "38" })%>
                        <%} %>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Perm Address:
                    </td>
                    <td>
                        <%= Html.TextBox("Address_1", Model.Address_1, new { style = "width: 200px", tabindex="8" })%>
                    </td>
                    <td class="label">
                        Cell Phone:
                    </td>
                    <td>
                        <%= Html.TextBox("Cell_Phone", Model.Cell_Phone, new { style = "width: 137px;", tabindex = "25" })%>
                    </td>
                    <td class="label">
                        Agent:
                    </td>
                    <td>
                        <% if (LMS.Data.Repository.UserRepository.GetUser(User.Identity.Name).Security_Level_ID == 7 || LMS.Data.Repository.UserRepository.GetUser(User.Identity.Name).Security_Level_ID == 1)
                           { %>
                        <%= Html.Encode(Model.Agent.First_Name + " " + Model.Agent.Last_Name)%>
                        <%}
                           else
                           { %>
                                <%= Html.DropDownList("Agent_ID", (SelectList)ViewData["Agent_ID"], new { tabindex = "39" })%>
                        <%} %>
                    </td>
                </tr>
                <tr>
                    <td class="label"></td>
                    <td><%= Html.TextBox("Address_2", Model.Address_2, new { style = "width: 200px", tabindex = "9" })%></td>
                    <td class="label"> Fax:</td>
                    <td><%= Html.TextBox("Fax", Model.Fax, new { style = "width: 137px;", tabindex = "26" })%></td>
                    <td class="label">
                        Original Agent:</td>
                    <td>
                        <% if (LMS.Data.Repository.UserRepository.GetUser(User.Identity.Name).Security_Level_ID == 1 || LMS.Data.Repository.UserRepository.GetUser(User.Identity.Name).Security_Level_ID == 7)
                           { %>
                        <%= Html.Encode(Model.OriginalAgent())%>
                        <%}
                           else
                           { %>
                                <%= Html.DropDownList("Original_Agent_ID", (SelectList)ViewData["Original_Agent_ID"], new { tabindex = "40" })%>
                        <%} %>
                    </td>
                </tr>
                <tr>
                    <td class="label">City, State Zip:
                    </td>
                    <td>
                         <%= Html.TextBox("City", Model.City, new { style = "width: 80px", tabindex = "10" })%>
                        ,<%= Html.DropDownList("State_ID", (SelectList)ViewData["State_ID"], new { tabindex = "11" })%>
                        <%= Html.TextBox("Zipcode", Model.Zipcode, new { style = "width: 60px", tabindex = "12" })%>
                    </td>
                     <td class="label">
                       Email:
                    </td>
                    <td>
                        <%= Html.TextBox("Email_Address", Model.Email_Address, new { tabindex = "27" })%>
                    </td>
                    <td class="label">
                        Sales Coordinator:
                    </td>
                    <td>
                        <%= Html.DropDownList("Assistant_Agent_ID", (SelectList)ViewData["Assistant_Agent_ID"], new { tabindex = "41" })%>
                    </td>
               </tr>
                <tr>
                    <td class="label">
                        County:
                    </td>
                    <td>
                       <%= Html.TextBox("County", Model.County, new { style = "width: 200px", tabindex = "13" })%>
                    </td>
                    <td class="label">
                        Alt Email Address:
                    </td>
                    <td>
                        <%= Html.TextBox("Alt_Email_Address", Model.Alt_Email_Address, new { tabindex = "28" })%>
                    </td>
                    <td class="label">
                        Referred By:
                    </td>
                    <td>
                        <%= Html.TextBox("Reffered_By", Model.Reffered_By, new { tabindex = "42" })%>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                         Contact Name:
                    </td>
                    <td>
                         <%= Html.TextBox("Alt_First_Name", Model.Alt_First_Name, new { style = "width: 90px", tabindex = "14", onblur = "initialCap(this);" })%>
                        <%= Html.TextBox("Alt_Last_Name", Model.Alt_Last_Name, new { style = "width: 90px", tabindex = "15", onblur = "initialCap(this);" })%>
                    </td>
                    <td class="label">
                        Phone 2:
                    </td>
                    <td>
                        <%= Html.TextBox("Alt_Phone_Number", Model.Alt_Phone_Number, new { style = "width: 100px;", tabindex = "29" })%>
                    </td>
                    <td class="label">
                        Referral Source:
                    </td>
                    <td>
                        <%= Html.DropDownList("Referral_Type_ID", (SelectList)ViewData["Referral_Type_ID"], new { tabindex = "43" })%>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                       Alt Address:
                    </td>
                    <td>
                       <%= Html.TextBox("Alt_Address_1", Model.Alt_Address_1, new { style = "width: 200px", tabindex = "16" })%>
                    </td>
                    <td class="label">
                        Email 2:
                    </td>
                    <td>
                        <%= Html.TextBox("Email_Address_2", Model.Email_Address_2, new { tabindex = "30" })%>
                    </td>
                    <td class="label">
                        Broker:
                    </td>
                    <td>
                        <%= Html.DropDownList("Broker_ID", (SelectList)ViewData["Broker_ID"], new { tabindex = "44" })%>
                    </td>
                </tr>
                <tr>
                     <td class="label">
                        
                    </td>
                    <td>
                        <%= Html.TextBox("Alt_Address_2", Model.Alt_Address_2, new { style = "width: 200px", tabindex = "17" })%>
                    </td>
                    <td class="label">
                        Momentum:
                    </td>
                    <td>
                        <%= Html.CheckBox("Momentum", new { tabindex = "31" })%>
                    </td>
                    <td class="label">
                        Products Of Interest:
                    </td>
                    <td>
                        <%= Html.DropDownList("Products_Of_Interest_ID", (SelectList)ViewData["Products_Of_Interest_ID"], new { tabindex = "19" })%>
                        
                    </td>
               </tr>
                <tr>
                    <td class="label">
                        City, State Zip:
                    </td>
                    <td>
                        <%= Html.TextBox("Alt_City", Model.Alt_City, new { style = "width: 80px", tabindex = "18" })%>
                        ,<%= Html.DropDownList("Alt_State_ID", (SelectList)ViewData["Alt_State_ID"], new { tabindex = "19" })%>
                        <%= Html.TextBox("Alt_Zipcode", Model.Alt_Zipcode, new { style = "width: 60px", tabindex = "20" })%>
                    </td>
                    <td class="label">
                        Do Not Call:
                    </td>
                    <td>
                        <%= Html.CheckBox("Do_Not_Contact", new { tabindex = "32" })%>
                    </td>
                    <td class="label">
                        Current Situation:
                    </td>
                    <td>
                        <%= Html.DropDownList("Record_Current_Situation_ID", (SelectList)ViewData["Record_Current_Situation_ID"], new { tabindex = "35" })%>
                        
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        Addr Date From:
                    </td>
                    <td>
                        <%= Html.TextBox("Alt_Address_From_Date", Model.Alt_Address_From_Date.HasValue==true ? Model.Alt_Address_From_Date.Value.ToString("MM/dd/yyyy") : "", new { style = "width: 80px", tabindex = "21" })%>
                        &nbsp;&nbsp;&nbsp;<b>To:</b>&nbsp;
                        <%= Html.TextBox("Alt_Address_To_Date", Model.Alt_Address_To_Date.HasValue==true ? Model.Alt_Address_To_Date.Value.ToString("MM/dd/yyyy") : "", new { style = "width: 80px", tabindex = "22" })%>
                    </td>
                    <td class="label">
                        Opt-Out Email:
                    </td>
                    <td>
                        <%= Html.CheckBox("Opt_Out_Email", new { tabindex = "32" })%>
                    </td>
                   <td class="label">
                        Opt-Out Newsletter:
                    </td>
                    <td>
                        <%= Html.CheckBox("Opt_Out_Newsletter", new { tabindex = "47" })%>
                    </td>
                </tr>
                <tr>
                     <td class="label">
                        Birth Date:
                    </td>
                    <td>
                        <%= Html.TextBox("DOB", Model.DOB.HasValue == true ? Model.DOB.Value.ToString("MM-dd-yyyy") : "", new { style = "width: 100px", tabindex = "6" })%>
                        <%= Html.ValidationMessage("DOB","*") %>
                        &nbsp;&nbsp;<strong>Age:</strong>

                        <%= Html.TextBox("Age", Model.Age, new { style = "width: 30px", tabindex = "7" })%>
                    </td>
                   <td class="label">
                        B2E? 
                    </td>
                    <td>
                        <%= Html.CheckBox("B2E", new { tabindex = "33" })%>
                    </td>
                     <td class="label">
                       Requested Start Date:
                    </td>
                    <td>
                       <%=Html.TextBox("RequestedStartDate", Model.RequestedStartDate.HasValue == true ? Model.RequestedStartDate.Value.ToString("MM/dd/yyyy") : "", new { style = "width: 80px", tabindex = "48" })%>
                       
                    </td>
                </tr>
                <tr>
                    <td class="label">
                     Distribution List:
                    </td>
                    <td colspan="5">
                    <% Html.RenderPartial("DistributionList", ViewData["DistributionList"]); %>
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <table width="100%" style="background-color: White; border: solid 1px #304f9f;">
                            <tr>   
                            <td class="label" style="width:100px">
                                Eligibility Date:
                            </td>
                            <td>
                                <%= Html.Encode(Model.Eligibility_Date.HasValue==true ? Model.Eligibility_Date.Value.ToString("MM/dd/yyyy"):"") %>
                            </td>
                            </tr>  
                           <tr>   
                            <td class="label" style="width:100px">
                                Eligibility:
                            </td>
                            <td>
                                <%= Html.Encode((Model.Record_Eligibility == null ? "" : Model.Record_Eligibility.Category + "-" + Model.Record_Eligibility.Description)) %>
                            </td>
                            </tr>  
                        </table>               
                    </td>
                </tr>
            </table>
        </div>
        <div id="tabs-2" style="background-color: #e0e4ee;">
            <%= Html.TextArea("Notes", LMS.Helpers.StringHelper.DecryptData(Model.Notes), new { style = "width: 100%;", rows = "5", tabindex = "48" })%>
        </div>
         <div id="tabs-3" style="background-color: #e0e4ee;">
            <div id="AdditionalDiv">
                 <table class="section" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="label" style = "width: 200px">
                            Name as appears on Medicare Card: 
                        </td>
                        <td class="label">
                            First:
                        </td>   
                           <td> <%= Html.TextBox("Medicare_FirstName", Model.Medicare_FirstName, new { style = "width: 150px", tabindex = "49" })%>
                        </td>
                        <td style = "width: 33px"></td>
                        <td class="label">
                            Initital:
                        </td>    
                        <td>    
                            <%= Html.TextBox("Medicare_InitialName", Model.Medicare_InitialName, new { style = "width: 50px", tabindex = "49" })%>
                        </td>
                        <td style = "width: 80px"></td>
                        <td class="label">
                            Last:
                        </td>
                        <td> 
                            <%= Html.TextBox("Medicare_LastName", Model.Medicare_LastName, new { style = "width: 150px", tabindex = "49" })%>
                        </td>
                    </tr>
                </table>
                 <table class="section" cellpadding="0" cellspacing="0">
                    <tr>
                         <td class="label">
                            SSN:
                         </td>
                         <td>
                            <%= Html.TextBox("SSN",LMS.Helpers.StringHelper.DecryptData(Model.SSN), new { tabindex = "50" })%>
                         </td>
                        <td class="label">
                            Gender:
                        </td>
                        <td>
                            <%= Html.DropDownList("Sex", (SelectList)ViewData["Sex"], new { style = "width: 130px", tabindex = "60" })%>
                        </td>
                         <td style = "width: 33px"></td>
                        <td class="label">
                            Current Tobacco Use:
                        </td>
                        <td>
                          <%= Html.DropDownList("Current_Tobacco_Use_Status_ID", (SelectList)ViewData["Current_Tobacco_Use_Status_ID"], new {style = "width: 125px",tabindex = "69" })%>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            Medicare Number:
                        </td>
                        <td width="200px">
                            <%= Html.TextBox("Medicare_Number",LMS.Helpers.StringHelper.DecryptData(Model.Medicare_Number), new { tabindex = "51" })%>
                        </td>
                         <td class="label">
                            Marital Status:
                        </td>
                        <td>
                            <%= Html.DropDownList("Marital_Status_ID", (SelectList)ViewData["Marital_Status_ID"], new { style = "width: 130px", tabindex = "61" })%>
                        </td>
                         <td style = "width: 33px"></td>
                        <td class="label">
                            Current Tobacco Type:
                        </td>
                        <td>
                            <%= Html.DropDownList("Current_Tobacco_ID", (SelectList)ViewData["Current_Tobacco_ID"], new {style = "width: 125px", tabindex = "70" })%>
                        </td>
                    </tr>  
                    <tr>
                      <td class="label">
                        <%= Html.CheckBox("Part_A_Effective")%>Part A Effective Date:
                      </td>
                      <td width="200px">
                        <%= Html.TextBox("Part_A_Effective_Date", Model.Part_A_Effective_Date.HasValue == true ? Model.Part_A_Effective_Date.Value.ToString("MM/dd/yyyy") : "", new { tabindex = "53" })%>
                      </td>
                      <td class="label">
                           Height:
                      </td>
                      <td>
                            <%= Html.TextBox("Height_Feet", Model.Height / 12, new { style = "width: 20px", tabindex = "62" })%>
                            ft.
                            <%= Html.TextBox("Height_Inches", Model.Height % 12, new { style = "width: 20px", tabindex = "63" })%>
                            in.
                      </td>
                      <td style = "width: 33px"></td>
                      <td class="label">
                           Past Tobacco Use:
                      </td>
                      <td>
                           <%= Html.DropDownList("Past_Tobacco_Use_Status_Id", (SelectList)ViewData["Past_Tobacco_Use_Status_Id"], new { style = "width: 125px", tabindex = "71" })%>
                      </td>
                    </tr> 
                    <tr>
                        <td class="label">
                            <%= Html.CheckBox("Part_B_Effective")%>Part B Effective Date:
                        </td>
                        <td width="200px">
                            <%= Html.TextBox("Part_B_Effective_Date", Model.Part_B_Effective_Date.HasValue == true ? Model.Part_B_Effective_Date.Value.ToString("MM/dd/yyyy") : "", new { tabindex = "55" })%>
                        </td>
                        <td class="label">
                            Weight:
                        </td>
                        <td>
                            <%= Html.TextBox("Weight", Model.Weight, new { style = "width: 40px", tabindex = "64" })%>
                            lbs.
                        </td>
                         <td style = "width: 33px"></td>
                        <td class="label">
                            Past Tobacco Type:
                        </td>
                        <td>
                            <%= Html.DropDownList("Past_Tobacco_ID", (SelectList)ViewData["Past_Tobacco_ID"], new {style = "width: 125px", tabindex = "72" })%>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            Drug List ID:
                        </td>
                        <td width="200px">
                            <%= Html.TextBox("Drug_List_ID",LMS.Helpers.StringHelper.DecryptData(Model.Drug_List_ID), new { tabindex = "56" })%>
                        </td>
                        <td class="label">
                            Subsidy:
                        </td>
                        <td>
                            <%= Html.DropDownList("Subsidy_ID", (SelectList)ViewData["Subsidy_ID"], new {style = "width: 130px", tabindex = "65" })%>
                        </td>
                         <td style = "width: 33px"></td>
                        <td class="label">
                            Date Quit:
                        </td>
                        <td>
                            <%= Html.TextBox("Quit_Date", Model.Quit_Date, new { style = "width: 123px", tabindex = "73" })%>
                        </td>
                   </tr>
                   <tr>
                        <td class="label">
                           Annual Income:
                        </td>
                        <td>
                            $<%= Html.TextBox("Annual_Income", string.Format("{0:0}", Model.Annual_Income), new { style = "width: 128px", tabindex = "57" })%>
                        </td>
                        <td></td>
                        <td></td>
                   </tr> 
                   <tr>
                        <td class="label">
                            Employment Status:
                        </td>
                        <td>
                            <%= Html.DropDownList("Employment_Status_ID", (SelectList)ViewData["Employment_Status_ID"], new { tabindex = "58" })%>
                        </td>
                        <td class="label">
                            Currently Insured:
                        </td>
                        <td>
                            <%= Html.CheckBox("Currently_Insured", new { tabindex = "66" })%>
                        </td>
                         <td style = "width: 33px"></td>
                        <td class="label">
                            Current Plan Type:
                        </td>
                        <td>
                            <%= Html.DropDownList("Current_Insured_Type_ID", (SelectList)ViewData["Current_Insured_Type_ID"], new {style = "width: 125px", tabindex = "74" })%>
                        </td>
                   </tr>
                   <tr>
                        <td class="label">
                            Occupation:
                        </td>
                        <td>
                            <%--<%= Html.DropDownList("Occupation_ID", (SelectList)ViewData["Occupation_ID"], new { tabindex = "59" })%>--%>
                            <%= Html.TextBox("Occupation", Model.Occupation, new { tabindex = "59" })%>
                        </td>
                         <td class="label">
                            Medical Carrier:
                        </td>
                        <td>
                            <%= Html.TextBox("Medical_Carrier", Model.Medical_Carrier, new { tabindex = "67" })%>
                        </td>
                         <td style = "width: 33px"></td>
                        <td class="label">
                            Medical Carrier Original Start Date:
                        </td>
                        <td>
                            <%= Html.TextBox("Original_Start_Date", Model.Original_Start_Date.HasValue == true ? Model.Original_Start_Date.Value.ToString("MM/dd/yyyy") : "", new { style = "width: 123px", tabindex = "75" })%>
                        </td>
                   </tr>     
                   <tr>
                        <td></td>
                        <td></td>
                        <td class="label">
                            Drug Plan Co:
                        </td>
                        <td>
                            <%= Html.TextBox("Drug_Plan_Co", Model.Drug_Plan_Co, new { tabindex = "68" })%>
                        </td>
                         <td style = "width: 33px"></td>
                        <td class="label">
                            Drug Plan Co Original Start Date:
                        </td>
                        <td>
                            <%= Html.TextBox("Drug_Plan_Original_Start_Date", Model.Drug_Plan_Original_Start_Date.HasValue == true ? Model.Drug_Plan_Original_Start_Date.Value.ToString("MM/dd/yyyy") : "", new { style = "width: 123px", tabindex = "76" })%>
                        </td>
                   </tr>
                </table>
              </div>
            <div id="CompanyDiv">
                <table class="section" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="label">
                            Name:
                        </td>
                        <td>
                            <%= Html.TextBox("Company.Name",(Model.Company == null ? "" : Model.Company.Name), new { style = "width: 150px" })%>
                        </td>
                        <td class="label">
                            Company President:
                        </td>
                        <td>
                            <%= Html.TextBox("Company.Company_President", (Model.Company == null ? "" : Model.Company.Company_President), new { style = "width: 150px" })%>
                        </td>
                        <td class="label">
                            Web Address:
                        </td>
                        <td>
                            <%= Html.TextBox("Company.Web_Address", (Model.Company == null ? "" : Model.Company.Web_Address), new { style = "width: 150px" })%>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            Address:
                        </td>
                        <td>
                            <%= Html.TextBox("Company.Address_1", (Model.Company == null ? "" : Model.Company.Address_1), new { style = "width: 150px" })%>
                        </td>
                        <td class="label">
                            Company Phone:
                        </td>
                        <td>
                            <%= Html.TextBox("Company.Company_Phone", (Model.Company == null ? "" : Model.Company.Company_Phone), new { style = "width: 100px" })%>
                        </td>
                        <td class="label">
                            Industry:
                        </td>
                        <td>
                            <%= Html.DropDownList("Company.Industry_ID")%>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                        </td>
                        <td>
                            <%= Html.TextBox("Company.Address_2", (Model.Company == null ? "" : Model.Company.Address_2), new { style = "width: 150px" })%>
                        </td>
                        <td class="label">
                            Primary Contact:
                        </td>
                        <td>
                            <%= Html.TextBox("Company.Primary_Contact", (Model.Company == null ? "" : Model.Company.Primary_Contact), new { style = "width: 150px" })%>
                        </td>
                        <td class="label">
                            Company Size:
                        </td>
                        <td>
                            <%= Html.TextBox("Company.Company_Size", (Model.Company == null ? "" : Model.Company.Company_Size), new { style = "width: 50px" })%>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            City, State Zip:
                        </td>
                        <td>
                            <%= Html.TextBox("Company.City", (Model.Company == null ? "" : Model.Company.City), new { style = "width: 80px" })%>
                            ,
                            <%= Html.DropDownList("Company.State_ID") %>
                            <%= Html.TextBox("Company.Zipcode", (Model.Company == null ? "" : Model.Company.Zipcode), new { style = "width: 80px" })%>
                        </td>
                        <td class="label">
                            Primary Phone:
                        </td>
                        <td>
                            <%= Html.TextBox("Company.Primary_Phone",(Model.Company == null ? "" :  Model.Company.Primary_Phone), new { style = "width: 100px" })%>
                        </td>
                        <td class="label">
                            Email Address:
                        </td>
                        <td>
                            <%= Html.TextBox("Company.Email_Address", (Model.Company == null ? "" : Model.Company.Email_Address), new { style = "width: 150px" })%>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                        </td>
                        <td>
                        </td>
                        <td class="label">
                            Title:
                        </td>
                        <td>
                            <%= Html.TextBox("Company.Title", (Model.Company == null ? "" : Model.Company.Title), new { style = "width: 100px" })%>
                        </td>
                        <td class="label">
                            Primary Email:
                        </td>
                        <td>
                            <%= Html.TextBox("Company.Primary_Email",(Model.Company == null ? "" :  Model.Company.Primary_Email), new { style = "width: 150px" })%>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
       <div id="tabs-5" style="background-color: #e0e4ee;">
            <div id="PermanentAddDiv">
            <table>
                <tr>
                    <td class="label">
                       Permanent Address:
                    </td>
                    <% if (Model.IsMailing_Address == null)
                   { %>
                    <td>
                       <%= Html.CheckBox("IsMailing_Address",true) %>Is Mailing Address
                    </td>                    
                    <%} 
                       else
                       {            
                        %>
                         <td>
                       <%= Html.CheckBox("IsMailing_Address",Model.IsMailing_Address) %> Is Mailing Address
                    </td>
                        <%} %>
                        
                </tr>
                <tr>
                    <td class="label"></td>
                    <td>
                     <%= Html.TextBox("txtAddress_1", Model.Address_1, new { style = "width: 200px"})%>
                    </td>
                </tr>                
                <tr>
                    <td class="label"></td>
                    <td><%= Html.TextBox("txtAddress_2", Model.Address_2, new { style = "width: 200px"})%></td>
                </tr>
                 <tr>
                    
                    <td class="label" style="text-align:right;">City, State Zip:
                    </td>
                    <td>
                         <%= Html.TextBox("txtCity", Model.City, new { style = "width: 80px"})%>
                        ,<%= Html.DropDownList("dlState_ID", (SelectList)ViewData["State_ID"])%>
                        <%= Html.TextBox("txtZipcode", Model.Zipcode)%>
                    </td>
                    </tr>
                    </table>
                    </div>
                    <div id="MailAddDiv">
                      <table>
                    <tr>
                    <td class="label">
                       Mailing Address:
                    </td>
                </tr>
                <tr>
                    <td class="label"></td>

                    <td>                    
                     <%= Html.TextBox("Mailing_Address_1", Model.Mailing_Address_1, new { style = "width: 200px" })%>                     
                    </td>
                       
                </tr>                
                <tr>
                    <td class="label"></td>
                    <td><%= Html.TextBox("txtMailing_Address_2", Model.Mailing_Address_2, new { style = "width: 200px" })%></td>
                </tr>
                 <tr>                    
                    <td class="label" style="text-align:right;">City, State Zip:
                    </td>
                    <td>
                         <%= Html.TextBox("txtMailing_City", Model.Mailing_City, new { style = "width: 80px" })%>
                        ,<%= Html.DropDownList("dlMailing_State_ID", (SelectList)ViewData["Mailing_State_ID"])%>
                        <%= Html.TextBox("txtMailing_ZipCode", Model.Mailing_ZipCode)%>
                    </td>
                    </tr>
                    </table>
            </div>
            <div id="AltAddDiv">
               <table>
                    <tr>
                    <td class="label">
                       Alternate Address:
                    </td>
                </tr>
                <tr>
                    <td class="label"></td>
                    <td>
                     <%= Html.TextBox("txtAlt_Address_1", Model.Alt_Address_1, new { style = "width: 200px"})%>
                    </td>
                </tr>                
                <tr>
                    <td class="label"></td>
                    <td><%= Html.TextBox("txtAlt_Address_2", Model.Alt_Address_2, new { style = "width: 200px" })%></td>
                </tr>
                 <tr>                    
                    <td class="label" style="text-align:right;">City, State Zip:
                    </td>
                    <td>
                         <%= Html.TextBox("txtAlt_City", Model.Alt_City, new { style = "width: 80px", tabindex = "18" })%>
                        ,<%= Html.DropDownList("dlAlt_State_ID", (SelectList)ViewData["Alt_State_ID"], new { tabindex = "19" })%>
                        <%= Html.TextBox("txtAlt_Zipcode", Model.Alt_Zipcode, new { style = "width: 60px", tabindex = "20" })%>
                    </td>
                    </tr>
               </table>
            </div>
         </div>
    </div>
    <br />
    <div style="text-align: center;">
        
        <%if (ViewData["InEditBy"].ToString().ToUpper() == Page.User.Identity.Name.ToUpper().Replace(@"LONGEVITY\", ""))
          { %>
            <input type="image" src="../../Content/icons/check.png" title="Save" alt="Save" id="saveBtn"/>
        <%
          }
        %>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="../../Record/Details/<%= Model.Record_ID %>" alt="Cancel">
            <img src="../../content/icons/close.png" alt="Cancel" title="Cancel" style="border: 0px;
                vertical-align: top;" />
        </a>
    </div>
    <%} %>
</asp:Content>
