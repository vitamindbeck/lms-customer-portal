<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Import Namespace="Helpers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    LMS 2.0 - Administration
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <script type="text/javascript">
        $(function() {
            $("#FromDate").mask("99/99/9999");
            $("#ToDate").mask("99/99/9999");
            $("#FromDate").datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: '1900:2020'
            });
            $("#ToDate").datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: '1900:2020'
            });
        });
    </script>

    <%= Html.ValidationSummary() %>
    <% using (Html.BeginForm())
       { %>
    <div class="admingdiv">
        <div style="font-size: 12px;">
            <label style="margin-right: 2px;">
                Choose Agent:</label>
            <%= Html.DropDownList("User_ID")%>
            <label style="margin-left: 25px;">
                From Date:</label>
            <%= Html.TextBox("FromDate", "", new { style = "width: 80px;" })%>            
            <label>
                To Date:</label>
            <%= Html.TextBox("ToDate", "", new { style = "width: 80px;" })%>            
             <label style="margin-left: 25px;">
                COW Only: </label>   
             <%= Html.CheckBox("Call_of_Week", ViewData["Call_of_Week"])%>  
            <input type="submit" value="Run Reports" />
            
                    
            
        </div>
                
        <br />
    </div>
    <%} %>
    <div style="border: solid 2px #5374c9;">
        <div style="background-color: #3d5696; color: White; font-weight: bold; padding: 5px;">
            Agent Leads By Source
            <% if (ViewData["AgentLeads"] == null)
               {%>
            ( None )
            <%}
               else
               { %>
            (
            <%= Html.Encode(ViewData["LeadCount"])%>
            )
            <%} %>
        </div>
        <table cellpadding="0" cellspacing="0" width="100%" class="sortable">
            <thead>
                <tr>
                    <th style="width: 150px;">
                        Source
                    </th>
                    <th style="width: 300px;">
                        Count
                    </th>
                </tr>
            </thead>
            <tbody>
                <% foreach (LMS.Data.Agent_Leads_By_Source_ReportResult result in (List<LMS.Data.Agent_Leads_By_Source_ReportResult>)ViewData["AgentLeads"])
                   { %>
                <tr>
                    <td>
                        <%= Html.Encode(result.SourceCode) %>
                    </td>
                    <td>
                        <%= Html.Encode(result.Count.GetValueOrDefault()) %>
                    </td>
                </tr>
                <%} %>
            </tbody>
        </table>
    </div>
    <br />
    <br />
    <br />
      <div style="border: solid 2px #5374c9;">
        <div style="background-color: #3d5696; color: White; font-weight: bold; padding: 5px;">
            Application Sent Report
            <% if (ViewData["AppSent"] == null)
               {%>
            ( None )
            <%}
               else
               { %>
            (
            <%= Html.Encode( ((List<LMS.Data.Agent_App_Sent_ReportResult>)ViewData["AppSent"]).Count() ) %>
            )
            <%} %>
        </div>
        <table cellpadding="0" width="100%" cellspacing="0" class="sortable">
            <thead>
                <tr>
                    <th style="width: 150px;">
                        Name
                    </th>
                    <th style="width: 300px;">
                        Carrier
                    </th>
                     <th style="width: 300px;">
                       State
                    </th>
                    <th style="width: 300px;">
                        Source
                    </th>
                    <th style="width: 150px;">
                        Date
                    </th>
                </tr>
            </thead>
            <tbody>
                <% foreach (LMS.Data.Agent_App_Sent_ReportResult result in (List<LMS.Data.Agent_App_Sent_ReportResult>)ViewData["AppSent"])
                   { %>
                <tr>
                    <td>
                        <%= Html.Encode(result.first_name + " " + result.last_name) %>
                    </td>
                    <td>
                        <%= Html.Encode(result.Carrier) %>
                    </td>
                     <td>
                        <%= Html.Encode(result.State) %>
                    </td>
                    <td>
                        <%= Html.Encode(result.source_number + " " + result.Source_Description) %>
                    </td>
                    <td>
                        <%= Html.Encode(string.Format("{0:d}", result.date_value)) %>
                    </td>
                </tr>
                <%} %>
            </tbody>
        </table>
    </div>
    <br />
    <br />
    <br />
    <div style="border: solid 2px #5374c9;">
        <div style="background-color: #3d5696; color: White; font-weight: bold; padding: 5px;">
            Application Received Report
            <% if (ViewData["AppReceived"] == null)
               {%>
            ( None )
            <%}
               else
               { %>
            (
            <%= Html.Encode(((List<LMS.Data.Agent_App_Received_ReportResult>)ViewData["AppReceived"]).Count())%>
            )
            <%} %>
        </div>
        <table cellpadding="0" cellspacing="0" width="100%" class="sortable">
            <thead>
                <tr>
                    <th style="width: 150px;">
                        Name
                    </th>
                    <th style="width: 300px;">
                        Carrier
                    </th>
                    <th style="width: 300px;">
                       State
                    </th>
                    <th style="width: 300px;">
                        Source
                    </th>
                    <th style="width: 150px;">
                        Date
                    </th>
                </tr>
            </thead>
            <tbody>
                <% foreach (LMS.Data.Agent_App_Received_ReportResult result in (List<LMS.Data.Agent_App_Received_ReportResult>)ViewData["AppReceived"])
                   { %>
                <tr>
                    <td>
                        <%= Html.Encode(result.first_name + " " + result.last_name) %>
                    </td>
                    <td>
                        <%= Html.Encode(result.Carrier) %>
                    </td>
                     <td>
                        <%= Html.Encode(result.State) %>
                    </td>
                    <td>
                        <%= Html.Encode(result.source_number + " " + result.Source_Description) %>
                    </td>
                    <td>
                        <%= Html.Encode(string.Format("{0:d}", result.date_value)) %>
                    </td>
                </tr>
                <%} %>
            </tbody>
        </table>
    </div>
    <br />
    <br />
    <br />
    <div style="border: solid 2px #5374c9;">
        <div style="background-color: #3d5696; color: White; font-weight: bold; padding: 5px;">
            Application Placed Report
            <% if (ViewData["AppPlaced"] == null)
               {%>
            ( None )
            <%}
               else
               { %>
            (
            <%= Html.Encode( ((List<LMS.Data.Agent_App_Placed_ReportResult>)ViewData["AppPlaced"]).Count() ) %>
            )
            <%} %>
        </div>
        <table cellpadding="0" width="100%" cellspacing="0" class="sortable">
            <thead>
                <tr>
                    <th style="width: 150px;">
                        Name
                    </th>
                    <th style="width: 300px;">
                        Carrier
                    </th>
                     <th style="width: 300px;">
                       State
                    </th>
                    <th style="width: 300px;">
                        Source
                    </th>
                    <th style="width: 150px;">
                        Date
                    </th>
                </tr>
            </thead>
            <tbody>
                <% foreach (LMS.Data.Agent_App_Placed_ReportResult result in (List<LMS.Data.Agent_App_Placed_ReportResult>)ViewData["AppPlaced"])
                   { %>
                <tr>
                    <td>
                        <%= Html.Encode(result.first_name + " " + result.last_name) %>
                    </td>
                    <td>
                        <%= Html.Encode(result.Carrier) %>
                    </td>
                      <td>
                        <%= Html.Encode(result.State) %>
                    </td>
                    <td>
                        <%= Html.Encode(result.source_number + " " + result.Source_Description) %>
                    </td>
                    <td>
                        <%= Html.Encode(string.Format("{0:d}", result.date_value)) %>
                    </td>
                </tr>
                <%} %>
            </tbody>
        </table>
    </div>
    <br />
    <br />
    <br />
    <div style="border: solid 2px #5374c9;">
        <div style="background-color: #3d5696; color: White; font-weight: bold; padding: 5px;">
            Call of the Week Report
            <% if (ViewData["COW"] == null)
               {%>
            ( None )
            <%}
               else
               { %>
            (
            <%= Html.Encode(((List<LMS.Data.sp_Agent_Call_of_Week_ReportResult>)ViewData["COW"]).Count())%>
            )
            <%} %>
        </div>
        
        <table cellpadding="0" cellspacing="0" width="100%" class="sortable">
            <thead>
                <tr>
                    <th style="width: 300px;">
                        Agent
                    </th>
                    <th style="width: 125px;">
                        M Rating
                    </th>
                    <th style="width: 125px;">
                        A Rating
                    </th>
                    <th style="width: 300px;">
                        Client
                    </th>
                    <th style="width: 150px;">
                        Inbound/Outbound
                    </th>
                    <th style="width: 150px;">
                        Activity Date
                    </th>
                    <th style="width: 150px;">
                        Time of Call
                    </th>     
                    <th style="width: 300px;">
                        Current Situation
                    </th>                   
                    <th style="width: 300px;">
                        Plan Status
                    </th>                                  
                </tr>
            </thead>
            <tbody>
                <% foreach (LMS.Data.sp_Agent_Call_of_Week_ReportResult result in (List<LMS.Data.sp_Agent_Call_of_Week_ReportResult>)ViewData["COW"])
                   { %>
                <tr>
                    <td>
                        <%= Html.Encode(result.Agent) %>
                    </td>
                    <td>
                        <%= Html.Encode(result.Manager_Rating) %>
                    </td>
                    <td>
                        <%= Html.Encode(result.Agent_Rating) %>
                    </td>
                    <td>
                        <%= Html.Encode(result.Client )%>
                    </td>
                    <td>
                        <%= Html.Encode(result.Inbound_Outbound) %>
                    </td>
                    <td>
                        <%= Html.Encode(result.Activity_Date) %>
                    </td>
                    <td>
                        <%= Html.Encode(result.Time_of_Call) %>
                    </td>
                    <td>
                        <%= Html.Encode(result.CurrentSituation) %>
                    </td>
                    <td>
                        <%= Html.Encode(result.Plan_Status) %>
                    </td>
                </tr>
                <%} %>
            </tbody>
        </table>
    </div>
    <br />
    <br />
    <br />
</asp:Content>
