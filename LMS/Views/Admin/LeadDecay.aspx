<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    LeadDecay
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <script type="text/javascript">
        $(function() {
            $("#StartDate").datepicker();
            $("#EndDate").datepicker();
            $(".LeadDecay").css({ 'border-bottom': 'solid 1px Black' });
            $(".LeadDecay tr:first td:last").css({ 'border-right': 'solid 1px Black' });
            $(".LeadDecay tr:last td:last").css({ 'border-right': 'solid 1px Black' });
        });

       
    </script>
    <div id="ClientFactsDiv" class="client-outer">
        <div class="section-title">
            Lead Decay Parameters:
        </div>
        <div class="section-inner">
            <% using (Html.BeginForm())
               { %>
            <table class="section" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td class="label" width="200px">
                        Lead Date Range:
                    </td>
                    <td>
                        <%= Html.TextBox("StartDate", "", new { style = "width: 80px;" })%>
                        &nbsp; <b>To:</b> &nbsp;
                        <%= Html.TextBox("EndDate", "", new { style = "width: 80px;" })%>
                    </td>
                </tr>
                <tr>
                    <td class="label" width="200px" valign="top">
                        Source Code(s):
                    </td>
                    <td>
                        <table width="100%">
                            <tr>
                                <td style="width: 305px;">
                                    <%= Html.ListBox("Source_Code_ID", (SelectList)ViewData["Source_Code_ID"], new { name = "Source_Code_ID", width = "300px", size = "10" })%>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="label" width="200px">
                        Product Line:
                    </td>
                    <td>
                        <%= Html.DropDownList("Group_ID") %>
                    </td>
                </tr>
                <tr>
                    <td class="label" width="200px">
                        Agent:
                    </td>
                    <td>
                        <%= Html.DropDownList("Agent_ID") %>
                    </td>
                </tr>
                <tr>
                    <td class="label" width="200px">
                    </td>
                    <td>
                        <input type="submit" value="Run Report" name="RunReport" />
                        <%= Html.CheckBox("Export","Export") %>
                    </td>
                </tr>
            </table>
            <%} %>
        </div>
    </div>
    <br />
    <% Dictionary<string, LMS.Data.LeadDecayResult> report = (Dictionary<string, LMS.Data.LeadDecayResult>)ViewData["Report"];  %>
    <% if (report != null)
       { %>
    <% int total = (int)ViewData["NumberOfLeads"];%>
    Total Number of Leads:
    <%= Html.Encode(total) %>
    <table cellpadding="0" cellspacing="0" width="100%" class="LeadDecay">
        <thead>
            <tr>
                <th style="min-width: 100px; border-left: solid 2px #4897f8;">
                    Minutes Til Contact
                </th>
                <th style="min-width: 50px;">
                    5 mins
                </th>
                <th style="min-width: 50px;">
                    15 mins
                </th>
                <th style="min-width: 50px;">
                    30 mins
                </th>
                <th style="min-width: 50px;">
                    1 hr
                </th>
                <th style="min-width: 50px;">
                    3 hr
                </th>
                <th style="min-width: 50px;">
                    6 hr
                </th>
                <th style="min-width: 50px;">
                    12 hr
                </th>
                <th style="min-width: 50px;">
                    24 hr
                </th>
                <th style="min-width: 50px;">
                    48 hr
                </th>
                <th style="min-width: 50px;">
                    96 hr
                </th>
                <th style="min-width: 50px;">
                    1 wk
                </th>
                <th style="min-width: 50px;">
                    1 wk+
                </th>
                <th style="min-width: 50px; border-right: solid 2px #4897f8;">
                    Total
                </th>
            </tr>
        </thead>
        <tr>
            <td style="min-width: 100px;">
                Number Made
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(report["5min"].Number) %>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(report["15min"].Number) %>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(report["30min"].Number) %>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(report["1hr"].Number)%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(report["3hr"].Number) %>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(report["6hr"].Number) %>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(report["12hr"].Number) %>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(report["24hr"].Number) %>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(report["48hr"].Number) %>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(report["96hr"].Number) %>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(report["1wk"].Number) %>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(report["1wk+"].Number) %>
            </td>
            <td style="min-width: 50px; border-right: solid 1px Black;">
                <%= Html.Encode(report.Sum(c => c.Value.Number)) %>
            </td>
        </tr>
        <tr>
            <td style="min-width: 100px;">
                % of Total
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(string.Format("{0:000.00}%", ((double)report["5min"].Number / (double)total) * 100))%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(string.Format("{0:000.00}%", ((double)report["15min"].Number / (double)total) * 100))%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(string.Format("{0:000.00}%", ((double)report["30min"].Number / (double)total) * 100))%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(string.Format("{0:000.00}%", ((double)report["1hr"].Number / (double)total) * 100))%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(string.Format("{0:000.00}%", ((double)report["3hr"].Number / (double)total) * 100))%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(string.Format("{0:000.00}%", ((double)report["6hr"].Number / (double)total) * 100))%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(string.Format("{0:000.00}%", ((double)report["12hr"].Number / (double)total) * 100))%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(string.Format("{0:000.00}%", ((double)report["24hr"].Number / (double)total) * 100))%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(string.Format("{0:000.00}%", ((double)report["48hr"].Number / (double)total) * 100))%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(string.Format("{0:000.00}%", ((double)report["96hr"].Number / (double)total) * 100))%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(string.Format("{0:000.00}%", ((double)report["1wk"].Number / (double)total) * 100))%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(string.Format("{0:000.00}%", ((double)report["1wk+"].Number / (double)total) * 100))%>
            </td>
            <td style="min-width: 50px; border-right: solid 1px Black;">
                <%= Html.Encode(string.Format("{0:000.00}%",(((double)report["5min"].Number / (double)total) * 100) 
                                                                            + (((double)report["15min"].Number / (double)total) * 100)
                                                                            + (((double)report["30min"].Number / (double)total) * 100)
                                                                            + (((double)report["1hr"].Number / (double)total) * 100)
                                                                             + (((double)report["3hr"].Number / (double)total) * 100)
                                                                             + (((double)report["6hr"].Number / (double)total) * 100)
                                                                             + (((double)report["12hr"].Number / (double)total) * 100)
                                                                             + (((double)report["24hr"].Number / (double)total) * 100)
                                                                             + (((double)report["48hr"].Number / (double)total) * 100)
                                                                             + (((double)report["96hr"].Number / (double)total) * 100)
                                                                             + (((double)report["1wk"].Number / (double)total) * 100)
                                                                             + (((double)report["1wk"].Number / (double)total) * 100)               
                                                                                )) %></td> </tr>
                </table>
                <br />
                <table cellpadding="0" cellspacing="0" width="100%" class="LeadDecay">
                    <tr>
                        <td style="min-width: 100px;">
                            Apps Sent (#)
                        </td>
                        <td style="min-width: 50px;">
                            <%= Html.Encode(report["5min"].AppsSent) %>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(report["15min"].AppsSent) %>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(report["30min"].AppsSent) %>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(report["1hr"].AppsSent) %>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(report["3hr"].AppsSent)%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(report["6hr"].AppsSent)%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(report["12hr"].AppsSent)%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(report["24hr"].AppsSent)%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(report["48hr"].AppsSent)%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(report["96hr"].AppsSent)%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(report["1wk"].AppsSent) %>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(report["1wk+"].AppsSent) %>
            </td>
            <td style="min-width: 50px; border-right: solid 1px Black;">
                <%= Html.Encode(report.Sum(c => c.Value.AppsSent))%>
            </td>
        </tr>
        <tr>
            <td style="min-width: 100px;">
                Apps Recv'd (#)
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(report["5min"].AppsRec) %>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(report["15min"].AppsRec)%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(report["30min"].AppsRec)%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(report["1hr"].AppsRec)%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(report["3hr"].AppsRec)%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(report["6hr"].AppsRec)%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(report["12hr"].AppsRec)%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(report["24hr"].AppsRec)%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(report["48hr"].AppsRec)%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(report["96hr"].AppsRec)%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(report["1wk"].AppsRec)%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(report["1wk+"].AppsRec)%>
            </td>
            <td style="min-width: 50px; border-right: solid 1px Black;">
                <%= Html.Encode(report.Sum(c => c.Value.AppsRec))%>
            </td>
        </tr>
        <tr>
            <td style="min-width: 100px;">
                Apps Placed (#)
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(report["5min"].AppsPlaced) %>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(report["15min"].AppsPlaced)%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(report["30min"].AppsPlaced)%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(report["1hr"].AppsPlaced)%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(report["3hr"].AppsPlaced)%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(report["6hr"].AppsPlaced)%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(report["12hr"].AppsPlaced)%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(report["24hr"].AppsPlaced)%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(report["48hr"].AppsPlaced)%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(report["96hr"].AppsPlaced)%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(report["1wk"].AppsPlaced)%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(report["1wk+"].AppsPlaced)%>
            </td>
            <td style="min-width: 50px; border-right: solid 1px Black;">
                <%= Html.Encode(report.Sum(c => c.Value.AppsPlaced))%>
            </td>
        </tr>
    </table>
    <br />
    <table cellpadding="0" cellspacing="0" width="100%" class="LeadDecay">
        <tr>
            <td style="min-width: 100px;">
                Apps Sent (%)
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(string.Format("{0:000.00}%", ((double)report["5min"].AppsSent / (double)total) * 100))%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(string.Format("{0:000.00}%", ((double)report["15min"].AppsSent / (double)total) * 100))%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(string.Format("{0:000.00}%", ((double)report["30min"].AppsSent / (double)total) * 100))%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(string.Format("{0:000.00}%", ((double)report["1hr"].AppsSent / (double)total) * 100))%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(string.Format("{0:000.00}%", ((double)report["3hr"].AppsSent / (double)total) * 100))%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(string.Format("{0:000.00}%", ((double)report["6hr"].AppsSent / (double)total) * 100))%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(string.Format("{0:000.00}%", ((double)report["12hr"].AppsSent / (double)total) * 100))%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(string.Format("{0:000.00}%", ((double)report["24hr"].AppsSent / (double)total) * 100))%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(string.Format("{0:000.00}%", ((double)report["48hr"].AppsSent / (double)total) * 100))%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(string.Format("{0:000.00}%", ((double)report["96hr"].AppsSent / (double)total) * 100))%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(string.Format("{0:000.00}%", ((double)report["1wk"].AppsSent / (double)total) * 100))%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(string.Format("{0:000.00}%", ((double)report["1wk+"].AppsSent / (double)total) * 100))%>
            </td>
            <td style="min-width: 50px; border-right: solid 1px Black;">
                <%= Html.Encode(string.Format("{0:000.00}%",(((double)report["5min"].AppsSent / (double)total) * 100) 
                                                                            + (((double)report["15min"].AppsSent / (double)total) * 100)
                                                                            + (((double)report["30min"].AppsSent / (double)total) * 100)
                                                                            + (((double)report["1hr"].AppsSent / (double)total) * 100)
                                                                             + (((double)report["3hr"].AppsSent / (double)total) * 100)
                                                                             + (((double)report["6hr"].AppsSent / (double)total) * 100)
                                                                             + (((double)report["12hr"].AppsSent / (double)total) * 100)
                                                                             + (((double)report["24hr"].AppsSent / (double)total) * 100)
                                                                             + (((double)report["48hr"].AppsSent / (double)total) * 100)
                                                                             + (((double)report["96hr"].AppsSent / (double)total) * 100)
                                                                             + (((double)report["1wk"].AppsSent / (double)total) * 100)
                                                                             + (((double)report["1wk"].AppsSent / (double)total) * 100)               
                                                                                )) %>
            </td>
        </tr>
        <tr>
            <td style="min-width: 100px;">
                Apps Recv'd (%)
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(string.Format("{0:000.00}%", ((double)report["5min"].AppsRec / (double)total) * 100))%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(string.Format("{0:000.00}%", ((double)report["15min"].AppsRec / (double)total) * 100))%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(string.Format("{0:000.00}%", ((double)report["30min"].AppsRec / (double)total) * 100))%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(string.Format("{0:000.00}%", ((double)report["1hr"].AppsRec / (double)total) * 100))%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(string.Format("{0:000.00}%", ((double)report["3hr"].AppsRec / (double)total) * 100))%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(string.Format("{0:000.00}%", ((double)report["6hr"].AppsRec / (double)total) * 100))%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(string.Format("{0:000.00}%", ((double)report["12hr"].AppsRec / (double)total) * 100))%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(string.Format("{0:000.00}%", ((double)report["24hr"].AppsRec / (double)total) * 100))%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(string.Format("{0:000.00}%", ((double)report["48hr"].AppsRec / (double)total) * 100))%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(string.Format("{0:000.00}%", ((double)report["96hr"].AppsRec / (double)total) * 100))%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(string.Format("{0:000.00}%", ((double)report["1wk"].AppsRec / (double)total) * 100))%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(string.Format("{0:000.00}%", ((double)report["1wk+"].AppsRec / (double)total) * 100))%>
            </td>
            <td style="min-width: 50px; border-right: solid 1px Black;">
                <%= Html.Encode(string.Format("{0:000.00}%",(((double)report["5min"].AppsRec / (double)total) * 100) 
                                                                            + (((double)report["15min"].AppsRec / (double)total) * 100)
                                                                            + (((double)report["30min"].AppsRec / (double)total) * 100)
                                                                            + (((double)report["1hr"].AppsRec / (double)total) * 100)
                                                                             + (((double)report["3hr"].AppsRec / (double)total) * 100)
                                                                             + (((double)report["6hr"].AppsRec / (double)total) * 100)
                                                                             + (((double)report["12hr"].AppsRec / (double)total) * 100)
                                                                             + (((double)report["24hr"].AppsRec / (double)total) * 100)
                                                                             + (((double)report["48hr"].AppsRec / (double)total) * 100)
                                                                             + (((double)report["96hr"].AppsRec / (double)total) * 100)
                                                                             + (((double)report["1wk"].AppsRec / (double)total) * 100)
                                                                             + (((double)report["1wk"].AppsRec / (double)total) * 100)               
                                                                                )) %>
            </td>
        </tr>
        <tr>
            <td style="min-width: 100px;">
                Apps Placed (%)
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(string.Format("{0:000.00}%", ((double)report["5min"].AppsPlaced / (double)total) * 100))%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(string.Format("{0:000.00}%", ((double)report["15min"].AppsPlaced / (double)total) * 100))%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(string.Format("{0:000.00}%", ((double)report["30min"].AppsPlaced / (double)total) * 100))%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(string.Format("{0:000.00}%", ((double)report["1hr"].AppsPlaced / (double)total) * 100))%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(string.Format("{0:000.00}%", ((double)report["3hr"].AppsPlaced / (double)total) * 100))%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(string.Format("{0:000.00}%", ((double)report["6hr"].AppsPlaced / (double)total) * 100))%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(string.Format("{0:000.00}%", ((double)report["12hr"].AppsPlaced / (double)total) * 100))%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(string.Format("{0:000.00}%", ((double)report["24hr"].AppsPlaced / (double)total) * 100))%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(string.Format("{0:000.00}%", ((double)report["48hr"].AppsPlaced / (double)total) * 100))%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(string.Format("{0:000.00}%", ((double)report["96hr"].AppsPlaced / (double)total) * 100))%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(string.Format("{0:000.00}%", ((double)report["1wk"].AppsPlaced / (double)total) * 100))%>
            </td>
            <td style="min-width: 50px;">
                <%= Html.Encode(string.Format("{0:000.00}%", ((double)report["1wk+"].AppsPlaced / (double)total) * 100))%>
            </td>
            <td style="min-width: 50px; border-right: solid 1px Black;">
                <%= Html.Encode(string.Format("{0:000.00}%",(((double)report["5min"].AppsPlaced / (double)total) * 100) 
                                                                            + (((double)report["15min"].AppsPlaced / (double)total) * 100)
                                                                            + (((double)report["30min"].AppsPlaced / (double)total) * 100)
                                                                            + (((double)report["1hr"].AppsPlaced / (double)total) * 100)
                                                                             + (((double)report["3hr"].AppsPlaced / (double)total) * 100)
                                                                             + (((double)report["6hr"].AppsPlaced / (double)total) * 100)
                                                                             + (((double)report["12hr"].AppsPlaced / (double)total) * 100)
                                                                             + (((double)report["24hr"].AppsPlaced / (double)total) * 100)
                                                                             + (((double)report["48hr"].AppsPlaced / (double)total) * 100)
                                                                             + (((double)report["96hr"].AppsPlaced / (double)total) * 100)
                                                                             + (((double)report["1wk"].AppsPlaced / (double)total) * 100)
                                                                             + (((double)report["1wk"].AppsPlaced / (double)total) * 100)               
                                                                                )) %>
            </td>
        </tr>
    </table>
    <%} %>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
