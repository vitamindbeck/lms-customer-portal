<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<LMS.Data.Source_Code>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    SourceCodes
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table class="sortable" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th>
                </th>
                <th align="center">
                    Code
                </th>
                <th align="center">
                    Desc
                </th>
                <th align="center">
                    Launch Date
                </th>
                <th align="center">
                    Paid
                </th>
                <th align="center">
                    Cost
                </th>
                <th align="center">
                    Notes
                </th>
                <th align="center">
                    Web Select
                </th>
                <th align="center">
                    Active
                </th>
            </tr>
        </thead>
        <tbody>
            <% foreach (var item in Model)
               { %>
            <tr style="font-size: .8em; line-height: 12px">
                <td>
                    <%= Html.ActionLink("Edit", "SourceCode", new { id=item.Source_Code_ID }) %>
                </td>
                <td>
                    <%= Html.Encode(item.Source_Number) %>
                </td>
                <td>
                    <%= Html.Encode(item.Description) %>
                </td>
                <td>
                    <%= Html.Encode(String.Format("{0:g}", item.Launch_Date)) %>
                </td>
                <td align="center">
                    <%= Html.CheckBox("Paid", item.Paid, new { @disabled = "true" })%>
                </td>
                <td align="right">
                    <%= Html.Encode(String.Format("{0:C}", item.Cost)) %>
                </td>
                <td>
                    <%= Html.Encode(item.Notes) %>
                </td>
                
                <td align="center">
                    <%= Html.CheckBox("Web_Select", item.Web_Select, new { @disabled = "true" })%>
                </td>
                <td align="center">
                    <%= Html.CheckBox("Active", item.Active, new { @disabled = "true" })%>
                </td>
            </tr>
            <% } %>
        </tbody>
    </table>
    <%= Html.ActionLink("Create New", "Create") %>
</asp:Content>
