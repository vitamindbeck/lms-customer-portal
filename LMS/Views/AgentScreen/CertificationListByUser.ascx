<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<LMS.Data.sp_Certification_List_By_UserResult>>" %>

<%@ Import Namespace="Helpers" %>
   <div class="client-outer">
    <div class="client-title">
         [ <%= Html.Encode(Model.Count()) %> Certifications ]
    </div>
    <div class="client-inner">
    <table class="tablesorter" width="100%" cellpadding="0" cellspacing="0" id="sortable1">
        <thead>
            <tr>
            <th>Action</th>
            <th>Carrier/Non-Carrier</th>
            <th>Link</th>
            <th>Due Date</th>
            <th>Complete Date</th>
            <th>User Name</th>
            <th>Password</th>
            <th>Special Instructions</th>
            <th>Type</th>
            <th>Status</th>
            </tr>
        </thead>
        <tbody>
        <% foreach (LMS.Data.sp_Certification_List_By_UserResult rs in Model)
           { %>
        <tr>
            <td>
                <% if (rs.Notified != null && rs.Notified == true)
                   {%>
                   Notified 
                <% }
                   else if(rs.Notified == false)
                   { %>
                   <%= Html.ActionLink("Notifiy Admin", "Notify", new {id=rs.Certification_ID })%>
                <% } %>
            </td>
            <td>
                <%= Html.Encode(rs.Name) %>
            </td>
            <td>
                <%if (rs.Certification_Link.Length > 40 && rs.Certification_Link != null)
                        {%>
                         <a href="<%= Html.Encode(rs.Certification_Link) %>" target=_blank"><%= Html.Encode(rs.Certification_Link.Substring(0,40))%>...</a>
                      <%} %>
                    <%else
                          { %>
                        <a href="<%= Html.Encode(rs.Certification_Link) %>" target=_blank"><%= Html.Encode(rs.Certification_Link)%></a>
                      <%} %>
            </td>
            <td>
             <% if (rs.Due_Date != null)
                {%>
                <%= Html.Encode(rs.Due_Date.Value.ToShortDateString())%>
               <%}%>
                
            </td>
            <td>
               <% if (rs.Complete_Date != null)
                   {%>
                   <%= Html.Encode(rs.Complete_Date.Value.ToShortDateString())%>
                <% }
                   else
                   { %>
                    TBD
                <% } %>
            </td>
            <td>
                <%= Html.Encode(rs.Certification_User_Name) %>
            </td>
            <td>
                <%= Html.Encode(rs.Certification_Password) %>
            </td>
            <td>
            <% if (rs.Special_Instructions != null && rs.Special_Instructions.Length > 30)
               {%>
                <%= Html.Encode(rs.Special_Instructions.Substring(0,25))%>&nbsp;
                <%= Html.ActionLink("(more)", "CertificationDetails", new { id = rs.Certification_ID }, new { target = "_blank" })%>
            <% }
               else
               { %>
                <%= Html.Encode(rs.Special_Instructions)%>
            <% } %>
            </td>
            <td>
                <%= Html.Encode(rs.Certification_Type)%>
            </td>
            <td>
                <%= Html.Encode(rs.Certification_Status)%>
            </td>
        </tr>
        <%} %>
        </tbody>
    </table>
    </div>
</div>