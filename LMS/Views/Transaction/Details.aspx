<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<LMS.Data.Plan_Transaction>" %>

<%@ Import Namespace="Helpers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Details
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        $(document).ready(function() {
            //  bindDropDownList($get("Plan_ID"));
        SetInitialDropDownList($get("Carrier_ID"), $get("Plan_ID"));
        });
    </script>
    <div id="ClientFactsDiv" class="client-outer" style="background-color: #e0e4ee;">
        <div class="section-title">
            Plan Details:
        </div>
        <div class="section-inner">
            <table class="section" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="label" width="200px">
                        Carrier:
                    </td>
                    <td width="200px">
                        <%= Html.DropDownList("Carrier_ID") %>
                    </td>
                </tr>
                <tr>
                    <td class="label" width="200px">
                        Plan:
                    </td>
                    <td width="200px">
                        <%= Html.CascadingDropDownList("Plan_ID", "Carrier_ID")%>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>

