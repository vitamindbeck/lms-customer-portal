<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<LMS.Data.Plan_Transaction>" %>

<%@ Import Namespace="LMS.Helpers" %>
<%@ Import Namespace="Helpers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%= Html.Encode(Model.DropDownName) %></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% Html.RenderPartial("RecordHeaderDetails", (LMS.Data.Record)ViewData["Record"]); %>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#Status_Date").mask("99/99/9999").datepicker({
                showOn: 'button',
                buttonImage: '../../Content/icons/calendar.gif',
                buttonImageOnly: true
            });

            SetInitialDropDownList($get("Carrier_ID"), $get("Plan_ID"));
            $("#HasDocuments").click(function () {
                var options = '';
                var planid = $("#Plan_ID").val();
                var stateid = $("#State_ID").val();
                $.ajax({
                    type: "GET",
                    url: "/Document/AjaxGet/" + planid + "/" + stateid,
                    data: {},
                    dataType: "json",
                    error: function (xhr, status, error) {
                        // you may need to handle me if the json is invalid
                        // this is the ajax object
                    },
                    success: function (json) {
                        if (json.length > 0) {
                            alert("Has documents");
                        }
                        else {
                            alert("No documents at this time");
                        }

                        //$("#Plan_Document_ID").html(options);
                        //$('#PrintModalContent').load('/Document/EmailView/103156/101514/' + $("#Plan_ID").val() + '/' + $("#State_ID").val(), function(html) { $('#PrintModalContent').html(html); });
                    }
                });
            });

            if ('<%= Model.PreFillApp %>' == 'true') {
                $("#PreFillApp").checked
            }

            $("#Carrier_ID").css({ 'background-color': '#fffcad' });
            $("#Plan_ID").css({ 'background-color': '#fffcad' });
            //$("#Modal_Premium").css({ 'background-color': '#fffcad' });
            $('#Created_On').datepicker({ showOn: 'button', buttonImage: '../../Content/icons/calendar.gif', buttonImageOnly: true });
            $('#DocumentPrintLink').click(function () {
                var doc = $('#Plan_Document_ID').val();
                window.open("/Transaction/GetPDF/" + doc);
                return false;
            });
            $("#Buying_Period_Effective_Date").mask("99/99/9999");
            /*$("#Alt_Address_From_Date").datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: '1900:2020'
            });*/
            //select all the a tag with name equal to modal
            $('a[name=modal]').click(function (e) {
                //Cancel the link behavior
                e.preventDefault();

                //Get the A tag
                var id = $(this).attr('href');

                //Get the screen height and width
                var maskHeight = $(document).height();
                var maskWidth = $(window).width();

                //Set heigth and width to mask to fill up the whole screen
                $('#dmask').css({ 'width': maskWidth, 'height': maskHeight });

                //transition effect		
                $('#dmask').fadeIn(300);
                $('#dmask').fadeTo("slow", 0.8);

                //Get the window height and width
                var winH = $(window).height();
                var winW = $(window).width();

                //Set the popup window to center
                $(id).css('top', winH / 2 - $(id).height() / 2);
                $(id).css('left', winW / 2 - $(id).width() / 2);

                //transition effect
                $(id).fadeIn(300);

            });


        });

        function LoadEmailView() {
            $('#centerbody').html("this shfsddf");
        }
    </script>
    <br />
    <div id="boxes">
        <div id="dmask">
        </div>
        <div id="PrintModalContent" class="window" style="display: none; background-color: #e0e4ee;
            padding: 20px;">
            <% Html.RenderPartial("DocumentEmail", ViewData["AllPlanDocuments"]);%>
        </div>
    </div>
    <% using (Html.BeginForm())
       { %>
    <div id="ClientFactsDiv" class="client-outer" style="background-color: #e0e4ee;">
        <div class="section-title">
            Plan Details:
            <%= Html.Hidden("State_ID",((LMS.Data.Record)ViewData["Record"]).State_ID) %>
        </div>
        <div class="section-inner">
            <table style="border: none; width: inherit !important;" class="section">
                <tr>
                    <td valign="top">
                        <table cellpadding="0" cellspacing="0" style="border: none;">
                            <% if (LMS.Data.Repository.UserRepository.GetUser(Page.User.Identity.Name).Security_Level_ID == 2)
                               { %>
                            <tr>
                                <td class="label">
                                    Created On:
                                </td>
                                <td class="style1">
                                    <%= Html.TextBox("Created_On")%>
                                </td>
                            </tr>
                            <%}%>
                            <tr>
                                <td class="label">
                                    Agent:
                                </td>
                                <td class="style1">
                                    <%= Html.DropDownList("Agent_ID", (SelectList)ViewData["Agent_List"])%>
                                    <%= Html.ValidationMessage("Agent_ID","*") %>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Source:
                                </td>
                                <td class="style1">
                                    <%= Html.DropDownList("Source_Code_ID")%>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Current Status:
                                </td>
                                <td class="style1">
                                    <%= Html.DropDownList("Plan_Transaction_Status_ID")%>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Prefilled Application:
                                    <%= Html.CheckBox("PreFillApp")%>
                                </td>
                                <td class="label">
                                    eApplication:
                                    <%= Html.CheckBox("eApp")%>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Initial Enrollment:
                                    <%= Html.CheckBox("Initial_Enrollment")%>
                                </td>
                                <td class="label">
                                    Guaranteed Issue:
                                    <%= Html.CheckBox("Guaranteed_Issue")%>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Carrier:
                                </td>
                                <td class="style1">
                                    <%= Html.DropDownList("Carrier_ID")%>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Plan:
                                </td>
                                <td class="style1">
                                    <%= Html.CascadingDropDownList("Plan_ID","Carrier_ID")%>
                                    <img alt="Has Documents" id="HasDocuments" src="../../Content/icons/documentpdf.png" />
                                </td>
                            </tr>
                            <tr id="DocumentsRow">
                                <td class="label">
                                    Documents:
                                </td>
                                <td class="style1">
                                    <%= Html.DropDownList("Plan_Document_ID")%>
                                    <%= Html.ImageLink("List", "Plan", new { id = Model.Plan_ID }, "../../Content/images/printer.png",
    "Plans", new { target = "_Blank", id="DocumentPrintLink" }, new { style = "border:0px;"})%>
                                    <a href="#PrintModalContent" name="modal">
                                        <img style="border: none;" src="../../Content/icons/mail.png" alt="Email Documents"
                                            id="EmailDocumentsButton" /></a>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Sub Plan:
                                </td>
                                <td class="style1">
                                    <%= Html.TextBox("Sub_Plan", Model.Sub_Plan, new { @maxlength = "35" })%>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Buying Period:
                                </td>
                                <td class="style1">
                                    <%= Html.DropDownList("Buying_Period_ID", (SelectList)ViewData["Buying_Period_List"])%>
                                    <%= Html.ValidationMessage("Buying_Period_ID","*") %>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Sub Buying Period:
                                </td>
                                <td class="style1">
                                    <%= Html.DropDownList("Buying_Period_Sub_ID", (SelectList)ViewData["Buying_Period_Sub_List"])%>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Requested Effective Date:
                                </td>
                                <td class="style1">
                                    <%=Html.TextBox("Buying_Period_Effective_Date", Model.Buying_Period_Effective_Date.HasValue == true ? Model.Buying_Period_Effective_Date.Value.ToString("MM/dd/yyyy") : "")%>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Underwriting Class:
                                </td>
                                <td class="style1">
                                    <%= Html.DropDownList("Underwriting_Class_ID", (SelectList)ViewData["Underwriting_Class_List"])%>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Modal Premium:
                                </td>
                                <td class="style1">
                                    $<%= Html.TextBox("Modal_Premium", string.Format("{0:0.00}", Model.Modal_Premium))%>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Mode:
                                </td>
                                <td class="style1">
                                    <%= Html.DropDownList("Modal_ID")%>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Annual Premium:
                                </td>
                                <td class="style1">
                                    $<%= Html.TextBox("Annual_Premium", string.Format("{0:0.00}", Model.Annual_Premium))%>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Cash With App:
                                </td>
                                <td class="style1">
                                    <%= Html.CheckBox("Cash_With_Application")%>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Payment Type:
                                </td>
                                <td class="style1">
                                    <%= Html.DropDownList("Payment_Mode_ID")%>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Shipping Method Outbound:
                                </td>
                                <td class="style1">
                                    <%= Html.DropDownList("Outbound_Shipping_Method_ID", (SelectList)ViewData["Outbound_Shipping_Method_List"])%>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Tracking Number Outbound:
                                </td>
                                <td class="style1">
                                    <%= Html.TextBox("Tracking_Number_Outbound")%>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Shipping Method Inbound:
                                </td>
                                <td class="style1">
                                    <%= Html.DropDownList("Inbound_Shipping_Method_ID", (SelectList)ViewData["Inbound_Shipping_Method_List"])%>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Tracking Number Inbound:
                                </td>
                                <td class="style1">
                                    <%= Html.TextBox("Tracking_Number_Inbound")%>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="label">
                                    Policy Number:
                                </td>
                                <td width="">
                                    <%= Html.TextBox("Policy_Number", StringHelper.DecryptData(Model.Policy_Number))%>
                                </td>
                                <td class="label">
                                    Status Date:
                                </td>
                                <td>
                                    <%= Html.TextBox("Status_Date", (Model.Status_Date.HasValue ? Model.Status_Date.Value.ToString("MM/dd/yyyy") : ""), new { @style = "width:80px;" })%>
                                </td>
                            </tr>
                            <% foreach (LMS.Data.Plan_Transaction_Date date in Model.Plan_Dates.OrderBy(pd => pd.Transaction_Date_Type.Display_Order))
                               { %>
                            <tr>
                                <td class="label" style="border: none;">
                                    <%= Html.Encode(date.Transaction_Date_Type.Description)
                                    %>:
                                </td>
                                <td style="border: none;">
                                    <script type="text/javascript">
                                        $(document).ready(function () {
                                            $('#<%= "DateType" + date.Plan_Transaction_Date_ID.ToString()%>').mask("99/99/9999");
                                            $('#<%= "DateType" + date.Plan_Transaction_Date_ID.ToString()%>').datepicker({ showOn: 'button', buttonImage: '../../Content/icons/calendar.gif', buttonImageOnly: true });

                                            //Function : Change in Plan_Transaction Status
                                            $('#Plan_Transaction_Status_ID').change(function () {

                                                //Store Plan_Transaction_Status_ID and PreFillApp values
                                                var tempStatus = $("#Plan_Transaction_Status_ID").val();
                                                var tempPrefillApp = $("#PreFillApp").val();

                                                //Alert is just for testing the values
                                                //alert("Status amd PrefillApp:" + tempStatus + tempPrefillApp);


                                                //Add condition for prefill and App Sent status
                                                if (tempStatus == 1) {
                                                    if (tempPrefillApp == 'true') {
                                                        // alert("test1")

                                                        //Store Plan_Transaction_Date_ID, Transaction_Date_Type_ID and Date_Value.
                                                        var planTransDateID = '<%=date.Plan_Transaction_Date_ID%>';
                                                        var transDateTypeId = '<%=date.Transaction_Date_Type_ID%>';
                                                        var dVal = '<%=date.Date_Value%>';

                                                        //Condition if there is Plan_Transaction_Date_ID and Transaction_Date_Type_ID = 2(Application Sent date)
                                                        if (planTransDateID) {
                                                            if (transDateTypeId == 2) {

                                                                //Store current date in variable dVal
                                                                //var currentTimeAndDate = new Date();

                                                                var currentTimeAndDate = new Date();
                                                                var month = currentTimeAndDate.getMonth() + 1;
                                                                var day = currentTimeAndDate.getDate();
                                                                var dd = 0;

                                                                if (day < 10) {
                                                                    dd = "0" + day;
                                                                }
                                                                else {
                                                                    dd = day;
                                                                }

                                                                var year = currentTimeAndDate.getFullYear();
                                                                dVal = (month + "/" + day + "/" + year);

                                                                //filling the date type control with date val value.
                                                                $('#<%= "DateType" + date.Plan_Transaction_Date_ID.ToString()%>').val(dVal);
                                                            }

                                                        }


                                                    }

                                                }
                                            });

                                        });
                                  
                                    </script>
                                    <%=Html.TextBox("DateType" + date.Plan_Transaction_Date_ID, 
                                       (date.Date_Value.HasValue ? date.Date_Value.Value.ToString("MM/dd/yyyy") : Session["DateType"+date.Plan_Transaction_Date_ID.ToString()]), 
                                          new { style = "width: 80px"})%>
                                </td>
                            </tr>
                            <%} %>
                            <tr>
                                <td class="label">
                                    Custom Notes:
                                </td>
                                <td colspan="3">
                                    <%= Html.DropDownList("NoteHelpers")%>
                                    <img src="../../Content/images/page_white_paste.png" alt="Paste" style="border: none;
                                        vertical-align: middle;" onclick="javascript:$('#Special_Instructions').text($('#NoteHelpers  :selected').text());" />
                                </td>
                            </tr>
                            <tr>
                                <td class="label" valign="top">
                                    Special Instructions:
                                </td>
                                <td colspan="3">
                                    <%= Html.TextArea("Special_Instructions", new { style = "width: 250px;", maxlength = "2000", numrows = "2" })%>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    Case Manager:
                                </td>
                                <td class="style1">
                                    <%= Html.DropDownList("Case_Manager_ID", (SelectList)ViewData["Case_Manag_ID"])%>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <%  LMS.Data.Repository.UserRepository _userRepository = new LMS.Data.Repository.UserRepository();
                            LMS.Data.User user = _userRepository.GetUserFromADName(User.Identity.Name.Replace(@"LONGEVITY\", ""));

                            if (Model.Activity_Count > 0 && user.Security_Level_ID == 1 || user.Security_Level_ID != 1)
                            {%>
                        <input type="image" src="../../Content/icons/check.png" alt="Save" title="Save" name="Save" />
                        <% } %>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="../../Record/Details/<%= Model.Record_ID %>">
                            <img src="../../content/icons/close.png" alt="Cancel" title="Cancel" style="border: 0px;
                                vertical-align: top;" />
                        </a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <% if (Model.Activity_Count > 0 && user.Security_Level_ID == 1 || user.Security_Level_ID != 1)
                           {%>
                        <input type="image" src="../../Content/icons/folder_add.png" alt="Save" title="Save
    And Add New" name="New" />
                        <%} %>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <%} %>
</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="HeaderContent">
    <style type="text/css">
        .style1
        {
            width: 156px;
        }
    </style>
</asp:Content>
