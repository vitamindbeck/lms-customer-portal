﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Dictionary<int, string>>" %>
<select>
    <option value=''></option>
    <%foreach (KeyValuePair<int,string> value in Model) { %>
        <option value='<%=value.Key.ToString() %>'><%=value.Value%></option>
    <%}%>
</select>