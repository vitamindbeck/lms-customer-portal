<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Licensing.Master" Inherits="System.Web.Mvc.ViewPage<LMS.Data.Appointment>" %>
<%@ Import Namespace="LMS.Helpers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Appointment Details
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


<div style="text-align: left;">
<% LMS.Data.User user = LMS.Data.Repository.UserRepository.GetUser(Page.User.Identity.Name); %>
<%if (LMS.Data.Repository.UserRepository.HasSecurityLevel(user, 10))
  {%>       
        <a href="../../Appointment/Edit/<%= Model.Appointment_ID %>" style="text-decoration: none;">
            <img src="../../content/icons/document_edit.png" title="Edit" alt="Edit" style="border: 0px;
                vertical-align: middle;" />
        </a>
<%} %>     
</div>
    <br />
<div class="client-outer" style="background-color: #e0e4ee;">
    <div class="section-title">
        Appointment Details
    </div>
    <div class="section-inner">
        <table class="section" cellpadding="0" cellspacing="0">
          <tr>
            <td class="label" width="150px">
                Agent/Agency:
            </td>
            <td>
                 <%= ViewData["AgentName"]%>
            </td>

            <td class="label">
                Company:
            </td>
            <td>
                <%=Html.Encode(Model.Carrier.Name) %>
            </td>
          </tr>
          <tr>
            <td class="label">
                Appointment Date:
            </td>
            <td>
                <%= Html.Encode(Model.Appointment_Date.HasValue ? Model.Appointment_Date.Value.ToShortDateString() : "")%>
            </td>           
            <td class="label">
                Product:
            </td>
            <td>
                <%=Html.Encode(Model.Insurance_Line_Lookup.Description) %>
            </td>
          </tr>
          <tr>
            <td class="label">
                Terminated Date:
            </td>
            <td>
                <%= Html.Encode(Model.Terminate_Date.HasValue ? Model.Terminate_Date.Value.ToShortDateString() : "")%>
            </td>           
            <td class="label">
                
            </td>
            <td>
                
            </td>
          </tr>
          <tr>
            <td class="label" valign="top">
                Agent/Agency #:
            </td>
            <td valign="top">
                <%= Html.Encode(StringHelper.DecryptData(Model.Agent_Number)) %>
            </td>
            <td class="label" valign="top">
                State(s):
            </td>
            <td>
                <%=Html.Encode(ViewData["StateList"]) %>
            </td>
          </tr> 
          <tr>
            <td class="label" valign="top">
                Last Updated:
            </td>
            <td>
                <%= Html.Encode(Model.Last_Updated_Date) %>
                <%if (!String.IsNullOrEmpty(ViewData["UserName"].ToString()))
                  { %>
                <strong>By:</strong>
                <%} %>
                <%= ViewData["UserName"] %>
            </td>
            <td class="label" valign="top" colspan="2">
            <% using (Html.BeginForm("Details", "Appointment", FormMethod.Post, new { id = "AppointmentDetails" }))
               {%>
                <%if (LMS.Data.Repository.UserRepository.HasSecurityLevel(user, 2))
                  {
                      LMS.Data.LMSDataContext db = new LMS.Data.LMSDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["WEBContextConnectionString"].ConnectionString);
                      List<LMS.Data.sp_Get_Appointment_Request_State_ListResult> rs = db.sp_Get_Appointment_Request_State_List(Model.Appointment_ID).ToList();
                      if (rs.Count > 0)
                      {
                %>
                        Select a state: <%= Html.DropDownList("RequestState", (SelectList)ViewData["RequestStateList"], new { tabindex = "1" })%>
                        <input type="submit" value="Request Appointment" id="btnSubmit" />
                <%
                      }
                      if (!String.IsNullOrEmpty(Request.Form["RequestState"]))
                      {
                          %>
                          <br /><font color="red">Request has been submitted.</font><br />
                          <%
                      }

                  }
               }%>            
            </td>
          </tr>     
        </table>
    </div>

</div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptContent" runat="server">
</asp:Content>
