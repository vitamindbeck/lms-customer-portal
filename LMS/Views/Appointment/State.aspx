<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<IEnumerable<LMS.Data.State>>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>State Details</title>
    <link href="../../Content/Site.css" rel="stylesheet" type="text/css" />
</head>
<body>

   <div class="client-outer" style="width:400px">
    <div class="client-title">
         [ State Details ]
    </div>
    <div class="client-inner">
    <table class="sortable" width="100%" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
            <th>Abbreviation</th>
            <th>State</th>
            </tr>
        </thead>
        <tbody style="color:Black;">
        <% foreach (LMS.Data.State rs in Model)
           { %>
        <tr>
            <td>
                <%= Html.Encode(rs.Abbreviation)%>
            </td>
            <td>
                <%= Html.Encode(rs.Name) %>
            </td>
        </tr>
        <%} %>
        </tbody>
    </table>
    </div>
</div>
</body>
</html>
