<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<LMS.Data.SupportDisplayType>>" %>
<div class="client-outer">
    <div class="client-title">
        [
        <%= Html.Encode(Model.Count()) %>
        Fulfillment Requests ]
    </div>
    <div class="client-inner">
        <table class="sortable" width="100%" cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th>
                        Request Date
                    </th>
                    <th>
                        Name
                    </th>
                    <th>
                        SourceCode
                    </th>
                    <th>
                        Requested By
                    </th>
                    <th>
                        Carrier
                    </th>
                    <th>
                        State
                    </th>
                    <th>
                        Plan Transaction Status
                    </th>
                    
                    <th>
                        Shipping Method
                    </th>
                </tr>
            </thead>
            <tbody>
                <% foreach (var item in Model)
                   { %>
                <tr>
                    <td>
                        <%= Html.Encode(String.Format("{0:g}", item.RequestDate)) %>
                    </td>
                    <td>
                        <%= Html.ActionLink(item.RecordName, "View", "Transaction", new { id = item.Plan_Transaction_ID }, new { target = "_Blank" })%>
                    </td>
                    <td>
                        <%= Html.Encode(item.SourceCode) %>
                    </td>
                      <td>
                        <%= Html.Encode(item.RequestedBy) %>
                    </td>
                    <td>
                        <%= Html.Encode(item.Carrier) %>
                    </td>
                    <td>
                        <%= Html.Encode(item.State) %>
                    </td>
                    <td>
                        <%= Html.Encode(item.Plan_Transaction_Status) %>
                    </td>
                    <td>
                        <%= Html.Encode(item.OutboundShippingMethod) %>
                    </td>
                </tr>
                <% } %>
            </tbody>
        </table>
    </div>
</div>
