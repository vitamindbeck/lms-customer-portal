﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<LMS.Models.RecordActivitiesViewModel>" %>
<%@ Import Namespace="System.Activities.Expressions" %>
<%@ Import Namespace="System.ComponentModel.DataAnnotations.Schema" %>

<% var grid = new WebGrid(Model.RecordActivity); %>

<%= grid.GetHtml(columns: grid.Columns(
    grid.Column(header: "Action To Do"),
        grid.Column("ActivityOpportunity", "Opportunity"),
        grid.Column("ActivityCampaign", "Campaign"),
        grid.Column("ActivityDueDate", "Due Date"),
        grid.Column("ActivityAssignee", "Assignee"),
        grid.Column("ActivityCompletedOnDate", "Completed On"),
        grid.Column("ActivityType", "Type"),
        grid.Column("ActivityStatus", "Status"),
        grid.Column("ActivityDisposition", "Disposition"),
        grid.Column("ActivityNotes", "Notes")
    )) %>




