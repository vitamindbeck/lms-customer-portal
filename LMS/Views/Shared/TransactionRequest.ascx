<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<LMS.Data.Plan_Transaction_Request>" %>
<div id="TransactionContainer">
    <% using (Html.BeginForm("Request", "Transaction"))
       { %>
    <div>
        Assign To:</div>
    <div>
        <%= Html.DropDownList("To_Employee_ID")%></div>
    <div>
        Question:</div>
    <div>
        <%= Html.TextArea("Question", "", 10, 200, null)%></div>
    <div>
        <%= Html.ActionLink("Submit", "Request", "Transaction")%>
    </div>
    <%} %>
</div>
