<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<LMS.Data.GetRecordActivitiesDisplayResult>>" %>
<script type="text/javascript">
    $(function () { $("#ActTable tr:last td").css({ 'border': 'none' }); });
</script>
<%  
    LMS.Data.User user = LMS.Data.Repository.UserRepository.GetUser(Page.User.Identity.Name);
%>
<table id="ActTable" cellpadding="0" align="center" cellspacing="0" width="98%" style="background-color: White;
    border: solid 1px #304f9f;">
    <thead class="small">
        <tr>
            <th width="100px">
                Action
            </th>
            <th width="120px">
                Completed On
            </th>
            <th width="120px">
                Type
            </th>
            <th width="120px">
                Due Date
            </th>
            <th width="120px">
                Assigned To
            </th>
            <th width="120px">
                Status
            </th>
            <th width="120px">
                Disposition Status
            </th>
            <th width="150px">
                Opportunity
            </th>
        </tr>
    </thead>
    <tbody>
        <% foreach (LMS.Data.GetRecordActivitiesDisplayResult record in Model)
           {
               var trClass = string.Empty;
               var showButton = true;
               if (record.Record_Activity_Status_ID == 33 || record.Record_Activity_Status_ID == 34 || record.Record_Activity_Status_ID == 35)
               {
                   if (record.Record_Activity_Status_ID != null && record.Actual_Date == null)
                   {

                       trClass = "orange";
                       if (!LMS.Data.Repository.UserRepository.HasSecurityLevel(user, 13))//&& user.User_ID != record.User_ID
                       {
                           showButton = false;
                       }

                   }
               }

               if (record.Actual_Date != null)
               {
                   //if (!LMS.Data.Repository.UserRepository.HasSecurityLevel(user, 13))
                   //{
                       showButton = false;
                   //}
               }
               
        %>
        <tr class="<%= Html.Encode(trClass)%>">
            <td>
                <% if (LMS.Data.Repository.UserRepository.GetUser(Page.User.Identity.Name).User_ID == record.User_ID || LMS.Data.Repository.UserRepository.GetUser(Page.User.Identity.Name).Security_Level_ID == 1 || LMS.Data.Repository.UserRepository.GetUser(Page.User.Identity.Name).Security_Level_ID == 2 || LMS.Data.Repository.UserRepository.GetUser(Page.User.Identity.Name).Security_Level_ID == 4 || LMS.Data.Repository.UserRepository.GetUser(Page.User.Identity.Name).Security_Level_ID == 12)
                   { %>
                <%=Html.ActionLink("Edit", "Edit", "Activity", new { id = record.Record_Activity_ID }, new { style = "font-weight: bold;" })%>
                <%} %>
                <% if (record.Actual_Date.HasValue == false)
                   { %>
                <%=Html.ActionLink("Complete", "Complete", "Activity", new { id = record.Record_Activity_ID }, new { style = "font-weight: bold; color: Green;" })%>
                <%} %>
                <% if (LMS.Data.Repository.UserRepository.GetUser(Page.User.Identity.Name).User_ID == record.User_ID
                       || LMS.Data.Repository.UserRepository.GetUser(Page.User.Identity.Name).Security_Level_ID == 2
                       || LMS.Data.Repository.UserRepository.GetUser(Page.User.Identity.Name).Security_Level_ID == 4
                       || LMS.Data.Repository.UserRepository.GetUser(Page.User.Identity.Name).Security_Level_ID == 12)
                   { %>
                <%if (showButton)
                  {%>
                <%=Html.ActionLink("Delete", "Delete", "Activity", new { id = record.Record_Activity_ID }, new { style = "font-weight: bold; color: Red;" })%>
                <%}
                   } %>
            </td>
            <td>
                <%= Html.Encode(record.Actual_Date)%>
            </td>
            <td>
                <% if (record.ActivityTypeDesc != null)
                   { %>
                <%= Html.Encode(record.ActivityTypeDesc)%>
                <%}
                   else
                   { %>
                Undefined
                <%}%>
            </td>
            <td>
                <%= Html.Encode(string.Format("{0:d}",record.Due_Date)) %>
            </td>
            <td>
                <%= Html.Encode(record.FullName) %>
            </td>
            <td>
                <%if (record.RecordActivityStatusDesc != null) %>
                <%= Html.Encode(record.RecordActivityStatusDesc)%>
            </td>
            <td>
                <%= Html.Encode(record.ReconciliationDesc)%>
            </td>
            <td>
                <% if (record.Name != null)
                   {
                       if (record.Name == null)
                       { %>
                <%= Html.Encode(record.Name)%>
                <%}
                       else
                       { %>
                <%= Html.Encode(record.Name)%>
                <%}
                   }%>
            </td>
        </tr>
        <tr>
            <td colspan="8" style="font-style: italic; border-bottom: solid 1px #304f9f; padding-left: 20px;">
                <%= Html.Encode(record.Notes)%>
            </td>
        </tr>
        <%} %>
    </tbody>
</table>
