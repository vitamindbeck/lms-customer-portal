<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<LMS.Data.Record>>" %>

<script type="text/javascript">
    $(function() {
        $(".person").draggable({ revert: 'invalid', opacity: 0.7, helper: 'clone' });
    });
</script>

<% if (Model != null)
   { %>
<% foreach (LMS.Data.Record record in Model)
   { %>
<div class="person">
    <table>
        <tr>
            <td width="90px">
                <%= Html.Encode(record.First_Name)%>
                <%= Html.Hidden("Record_ID", record.Record_ID)%>
            </td>
            <td width="90px">
                <%= Html.Encode(record.Last_Name)%>
            </td>
        </tr>
        <tr>
            <td width="180px" colspan="2">
                <%= Html.Encode(record.Address_1)%>
            </td>
        </tr>
        <tr>
            <td width="180px" colspan="2">
                <%= Html.Encode(record.City + ", " + record.State.Abbreviation + " " + record.Zipcode)%>
            </td>
        </tr>
    </table>
</div>
<br />
<%} %>
<%} %>
