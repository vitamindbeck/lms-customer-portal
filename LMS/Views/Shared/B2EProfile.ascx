﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<LMS.Models.B2EProfileViewModel>" %>
<div id="B2EProfile" class="client-outer halfFloatLeft">
<% using (Html.BeginForm("B2EProfile", "CustomerPortal", new { id = Model.CustomerId}))
   { %>
    <div class="section-title">
        <span>B2E Profile</span> 
        <div style="float: right">
            <img src="" alt="View"/>
            <img src="" alt="Edit"/>
        </div>
        <div style="clear: both"></div>
    </div>
    <div>
        <table>
            <tr><td></td></tr>

            <tr>
                <td>Address:</td><td colspan="3"><%= Html.Encode(Model.Address1) %></td>
            </tr>
            <tr>
                <td>&nbsp;</td><td colspan="3"><%= Html.Encode(Model.Address2) %></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><%= Html.Encode(Model.City) %></td>
                <td><%= Html.Encode(Model.State) %></td>
                <td><%= Html.Encode(Model.PostalCode) %></td>
            </tr>
        </table>
    </div>

    <div id="B2EProfileEdit">
        <div>
            <table>
                <tr>
                    <td>Name:</td>
                    <td colspan="3"><%= Html.TextBoxFor(m => m.Name) %></td>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td>Address:</td>
                    <td colspan="3"><%= Html.TextBoxFor(m => m.Address1) %></td>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td colspan="3"><%= Html.TextBoxFor(m => m.Address2) %></td>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td>City:</td>
                    <td><%= Html.TextBoxFor(m => m.City) %></td>
                    <td>State:</td>
                    <td><%= Html.TextBoxFor(m => m.State) %></td>
                    <td>Zip:</td>
                    <td><%= Html.TextBoxFor(m => m.PostalCode) %></td>
                </tr>
            </table>
        </div>
        <div>
            <table>
                <tr>
                    <td>Company President:</td><td><%= Html.TextBoxFor(m => m.CompanyPresident) %></td>
                </tr>
                <tr>
                    <td>Company Phone:</td><td><%= Html.TextBoxFor(m => m.CompanyPhone) %></td>
                </tr>
                <tr>
                    <td>Web Address:</td><td><%= Html.TextBoxFor(m => m.WebAddress) %></td>
                </tr>
                <tr>
                    <td>Industry:</td><td></td>
                </tr>
                <tr>
                    <td>Company Size:</td><td><%= Html.TextBoxFor(m => m.CompanySize) %></td>
                </tr>
                <tr>
                    <td>Email Address:</td><td><%= Html.TextBoxFor(m => m.EmailAddress) %></td>
                </tr>
            </table>
        </div>
        <div>
            <table>
                <tr><td>Primary Contact:</td><td><%= Html.TextBoxFor(m => m.PrimaryContact) %></td></tr>
                <tr><td>Title:</td><td><%= Html.TextBoxFor(m => m.PrimaryContactTitle) %></td></tr>
                <tr><td>Primary Phone:</td><td><%= Html.TextBoxFor(m => m.PrimaryContactPhone) %></td></tr>
                <tr><td>Primary Email:</td><td><%= Html.TextBoxFor(m => m.PrimaryEmail) %></td></tr>
            </table>
        </div>
        <div>
            <div style="float: right"><input type="button" value="Cancel"/> <input type="button" value="Save"/></div>
            <div style="clear: both"></div>
        </div> 
    </div>
<% } %>
</div>