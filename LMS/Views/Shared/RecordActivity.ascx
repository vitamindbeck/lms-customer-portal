﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<LMS.Models.RecordActivityViewModel>" %>
<%@ Import Namespace="LMS.Models" %>
<%@ Import Namespace="LmsDataRepository.Models" %>

<div>
<% using (Html.BeginForm())
  { %>
<div id ="ActivityWizard">
                <div>
                    <span> Activities </span>
                </div>
                <div id="ClientFacts">
                    <textarea> A duck's quack doesn't echo, and no one knows why.</textarea>    
                </div>                
                <div>
                    <div>
                        <button id="newActivityButton" type="button">New Activity</button>
                    </div>                
                    <div>
                        <button id="FollowUpActivityButton" type="button">Follow Up Activity</button>
                    </div>
                    <div style="clear: both"></div>
                </div>
                <div id="Activities">
                    <% Html.RenderPartial("RecordActivities", new LMS.Models.RecordActivitiesViewModel()); %>>
                    <span>View History</span>                    
                </div>            
                <div id ="currentActivityModal">
                    <fieldset>
                        <legend>Current Activity</legend>
                        <table>
                        <tr>
                            <td>
                                Opportunity
                            </td>
                            <td>
                                <%--<% Html.DropDownListFor(x => x.Opportunity, new SelectList(Model.Opportunity, "Plan_Transaction_ID",  )); %>--%>
                            </td>
                            <td colspan="2"></td>                            
                        </tr>
                        
                        <tr>
                            <td>
                                Activity
                            </td>
                            <td>
                                <%= Html.DropDownListFor(x => x.Activity, TypeofActivity.GetValues()) %>
                            </td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td>
                                Include Email
                            </td>
                            <td>
                                <input type="checkbox" name="includeEmailCheckBox" value="includeEmail"/>
                                <%= Html.DropDownListFor(x => x.AutoEmail, TypeofAutoEmail.GetValues()) %>
                            </td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td>
                                Status
                            </td>
                            <td>
                                <%= Html.DropDownListFor(x => x.Status, StatusofRecordActivity.GetValues()) %>
                            </td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td>
                                Campaign Code
                            </td>
                            <td>
                                <%= Html.DropDownListFor(x => x.CampaignCode, CampaignCode.GetValues()) %>
                            </td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                               <%= Html.DropDownListFor(x => x.RecordNotes, TypeofRecordActivityNote.GetValues()) %>
                            </td>
                            <td> CLIPBOARD</td>
                        </tr>
                        <tr>
                            <td>
                                Notes
                            </td>
                            <td>
                                <% Html.TextArea("Note"); %>
                            </td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td>
                                Disposition
                            </td>
                            <td>
                                <%= Html.DropDownListFor(x => x.Disposition, TypeofReconciliation.GetValues()) %>
                            </td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox" name="applicationSentCheckBox" value="applicationSent"/>
                                Application Sent - Cancelled
                            </td>
                            <td>
                            </td>
                            <td colspan="2"></td>
                        </tr>
                        </table>
                        <div>
                            <div>
                                <button id="saveCurrentActivity">Save</button>
                            </div>
                            <div>
                                <button id="cancelCurrentActivity">Cancel</button>
                            </div>                           
                        </div>  
                       </fieldset>                     
                </div>
                <div id ="FollowUpActivityModal">
                    <fieldset>
                        <legend>Follow Up Activity</legend>
                        <table id="FollowUpTable">
                        <tr>
                            <td>
                                Follow Up Date
                            </td>
                            <td>
                                <input id="date" type="datetime"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Follow Up Time
                            </td>
                            <td>
                               <input id="followUpTime" type="time"/>
                            </td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td>
                                Follow Up Activity
                            </td>
                            <td>
                                <%= Html.DropDownListFor(x => x.FollowUpActivityType, TypeofActivity.GetValuesFollowupActivityType()) %>
                            </td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td>
                                Follow Set For
                            </td>
                            <td>
                                <%= Html.DropDownListFor(x => x.FollowUpUser, TypeofAgent.GetValues()) %>
                            </td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td>
                                Campaign Code
                            </td>
                            <td>
                                <%= Html.DropDownListFor(x => x.FollowUpCampaignCode, CampaignCode.GetValues()) %>
                            </td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <%= Html.DropDownListFor(x => x.FollowUpRecordNotes, TypeofRecordActivityNote.GetValues()) %>
                            </td>
                            <td> CLIPBOARD</td>
                        </tr>
                        <tr>
                            <td>
                                Follow Up Notes
                            </td>
                            <td>
                                <input id="Notes" type="text"/>
                            </td>
                            <td colspan="2"></td>
                        </tr>
                        </table>
                        <div>
                            <div>
                                <button id="saveFollowUpActivity">Save</button>
                            </div>
                            <div>
                                <button id="cancelFollowUpActivity">Cancel</button>
                            </div>                            
                        </div>
                     </fieldset>                               
                </div>
                <% var testId = 127080;
                    Html.RenderPartial("RecordActivities", new RecordActivitiesViewModel { RecordActivity = RecordActivitiesViewModel.GetViewModel(testId) }); %>
            </div>  
            <% } %>
</div>