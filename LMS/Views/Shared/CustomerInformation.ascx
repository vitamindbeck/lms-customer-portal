﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<LMS.Models.CustomerInformationViewModel>" %>
<%@ Import Namespace="LMS.Models" %>
<%@ Import Namespace="LmsDataRepository.Models" %>
<%@ Import Namespace="System.Globalization" %>
<div id="CustomerInformation" class="client-outer halfFloatLeft">
<% using (Html.BeginForm("CustomerInformation", "CustomerPortal"))
   { %>
    <div class="section-title">
        <span>Customer Information</span> &nbsp; Client Since:
        <div class="title-menu" style="float: right">
            <div id="CustomerInformationViewButton"><img src="<%= Url.Content("~/Content/images/view.jpg") %>" alt="View"/></div>
            <div id="CustomerInformationEditButton"><img src="<%= Url.Content("~/Content/images/edit.jpg") %>" alt="Edit"/></div>
        </div>
        <div style="clear: both"></div>
    </div>
    <div>
        <table>
            <tr>
                <td><span class="label">HVC:</span></td>
                <td><img id="imgHvc"/></td>
                <td><%= Html.Encode(string.Format("{0} {1} {2} {3} {4}", Model.Prefix, Model.FirstName, Model.MiddleInitial, Model.LastName, Model.Suffix)) %></td>
                <td><span class="label">Communication Preference:</span> <%= Html.Encode(Model.Preference) %></td>
            </tr>
            <tr>
                <td><span class="label">Do Not Call:</span></td>
                <td><img id=""/></td>
                <td><span class="label">Gender:</span> <%= Html.Encode( Model.Gender) %></td>
                <td><span class="label">Contact Preference:</span> <%= Html.Encode( Model.ContactPreference) %></td>
            </tr>
            <tr>
                <td><span class="label">Opt Out Email:</span></td>
                <td><img id="Img1"/></td>
                <td><span class="label">Marital Status:</span> <%= Html.Encode(StatusofMarriage.GetName(Model.MaritalStatus))%></td>
                <td><span class="label">Preference Time:</span> <%= Html.Encode( Model.CallTime) %></td>
            </tr>
            <tr>
                <td><span class="label">PoA:</span></td>
                <td><img id="Img2"/></td>
                <td><%= Html.Encode(Model.Address1) %></td>
                <td><span class="label">C:</span> <%= Html.Encode(Model.CellPhone) %></td>
            </tr>
            <tr>
                <td colspan="2"><span class="label">Source Code:</span> <%= Html.Encode(SourceCode.GetName(Model.SourceCode)) %></td>
                <td><%= Html.Encode(Model.City) %></td>
                <td><span class="label">H:</span> <%= Html.Encode(Model.HomePhone) %></td>
            </tr>
            <tr>
                <td colspan="2"><span class="label">Advisor:</span> <%= TypeofAgent.GetName(Model.CurrentAdvisor)%></td>
                <td><span class="label">County:</span> <%= Html.Encode(Model.County) %></td>
                <td><%= Html.Encode(Model.Email) %></td>
            </tr>
            <tr>
                <td colspan="2"><span class="label">Sales Support:</span> <%= TypeofAgent.GetName(Model.Css)%></td>
                <td><span class="label">DOB:</span> <%= Html.Encode(Model.BirthDate == DateTime.MinValue ? "" : Model.BirthDate.ToString("MM/dd/yyyy")) %></td>
                <td><span class="label">Referred By:</span> <%= Html.Encode(Model.RefferredBy) %></td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
                <td><span class="label">Age:</span> <%= Html.Encode(Model.Age < 1 ? "" : Model.Age.ToString(CultureInfo.InvariantCulture))%></td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </div>
    <div id="CustomerInformationEditViewDialog" class="dialogForm" title="<%= Html.Encode(string.Format("Customer Information - {0} - {1}", Model.CustomerId, Model.Name)) %>">
        <div>
            <table>
                <tr>
                    <td class="label">HVC:</td>
                    <td><%= Html.CheckBoxFor(m => m.HighValueCustomer) %></td>
                    <td class="label">SSN:</td>
                    <td><%= Html.TextBoxFor(m => m.SocialSecurityNumber) %></td>
                    <td class="label">Home Phone:</td>
                    <td class="label"><%= Html.TextBoxFor(m => m.HomePhone) %></td>
                    <td class="label">Record Type:</td>
                    <td><%= Html.DropDownListFor(m => m.RecordType, TypeofRecord.GetValues()) %></td>
                </tr>
                <tr>
                    <td class="label">Prefix:</td>
                    <td><%= Html.DropDownListFor(m => m.Prefix, TypeofNamePrefix.GetValues()) %></td>
                    <td class="label">Gender:</td>
                    <td><%= Html.DropDownListFor(m => m.Gender, TypeofGender.GetValues()) %></td>
                    <td class="label">Cell Phone:</td>
                    <td><%= Html.TextBoxFor(m => m.CellPhone) %></td>
                    <td class="label">Source Code:</td>
                    <td><%= Html.DropDownListFor(m => m.SourceCode, SourceCode.GetValues())%></td>
                </tr>
                <tr>
                    <td class="label">First Name:</td>
                    <td><%= Html.TextBoxFor(m => m.FirstName) %></td>
                    <td class="label">Marital Status:</td>
                    <td><%= Html.TextBoxFor(m => m.MaritalStatus, StatusofMarriage.GetValues()) %></td>
                    <td class="label">Work Phone:</td>
                    <td><%= Html.TextBoxFor(m => m.WorkPhone) %></td>
                    <td class="label">Current Advisor:</td>
                    <td><%= Html.DropDownListFor(m => m.CurrentAdvisor, TypeofAgent.GetValues()) %></td>
                </tr>
                <tr>
                    <td class="label">Middle Initial:</td>
                    <td><%= Html.TextBoxFor(m => m.MiddleInitial) %></td>
                    <td class="label">Birth Date:</td>
                    <td><%= Html.EditorFor(m => m.BirthDate)%></td>
                    <td class="label">Fax:</td>
                    <td><%= Html.TextBoxFor(m => m.Fax) %></td>
                    <td class="label">Original Advisor:</td>
                    <td><%= Html.DropDownListFor(m => m.OriginalAdvisor, TypeofAgent.GetValues()) %></td>
                </tr>
                <tr>
                    <td class="label">Last Name:</td>
                    <td><%= Html.TextBoxFor(m => m.LastName) %></td>
                    <td class="label">Age:</td>
                    <td><%= Html.TextBoxFor(m => m.Age) %></td>
                    <td class="label">Preference:</td>
                    <td><%= Html.DropDownListFor(m => m.Preference, TypeofContactPreference.GetValues()) %></td>
                    <td class="label">CSS:</td>
                    <td><%= Html.DropDownListFor(m => m.Css, TypeofAgent.GetValues()) %></td>
                </tr>
                <tr>
                    <td class="label">Suffix:</td>
                    <td><%= Html.TextBoxFor(m => m.Suffix) %></td>
                    <td class="label">Employment Status:</td>
                    <td><%= Html.DropDownListFor(m => m.EmploymentStatus, StatusofEmployment.GetValues()) %></td>
                    <td class="label">Call Time:</td>
                    <td><%= Html.DropDownListFor(m => m.CallTime, TypeofCallTime.GetValues()) %></td>
                    <td class="label">Referred By:</td>
                    <td><%= Html.TextBoxFor(m => m.RefferredBy) %></td>
                </tr>
                <tr>
                    <td class="label">Goes By:</td>
                    <td><%= Html.TextBoxFor(m => m.GoesBy) %></td>
                    <td class="label">Occupation:</td>
                    <td><%= Html.TextBoxFor(m => m.Occupation) %></td>
                    <td class="label">Do Not Call:</td>
                    <td><%= Html.CheckBoxFor(m => m.DoNotCall) %></td>
                    <td class="label">Referral Source:</td>
                    <td><%= Html.TextBoxFor(m => m.ReferralService) %></td>
                </tr>
                <tr>
                    <td class="label">B2E:</td>
                    <td><%= Html.CheckBoxFor(m => m.B2E) %></td>
                    <td class="label">Annual Income:</td>
                    <td><%= Html.TextBoxFor(m => m.AnnualIncome) %></td>
                    <td class="label">Email:</td>
                    <td><%= Html.TextBoxFor(m => m.Email) %></td>
                    <td class="label">Broker:</td>
                    <td><%= Html.DropDownListFor(m => m.Broker, TypeofBroker.GetValues()) %></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td class="label">Call Pref:</td>
                    <td></td>
                </tr>
                <tr>
                    <td class="label">AEP Reminder:</td>
                    <td><%= Html.CheckBoxFor(m => m.AepReminder) %></td>
                    <td class="label">Dispatch:</td>
                    <td><%= Html.CheckBoxFor(m => m.Dispatch) %></td>
                    <td class="label">Opt-Out Newsletter:</td>
                    <td><%= Html.CheckBoxFor(m => m.OptOutNewsLetter) %></td>
                    <td class="label">Opt-Out Email:</td>
                    <td><%= Html.CheckBoxFor(m => m.OptOutEmail) %></td>
                </tr>
            </table>
        </div>
        <div>
            <fieldset>
                <legend>Address of Record</legend>
                <table>
                    <tr>
                        <td class="label">Address:</td>
                        <td colspan="3"><%= Html.TextBoxFor(m => m.Address1) %></td>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td colspan="3"><%= Html.TextBoxFor(m => m.Address2) %></td>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="label">City:</td>
                        <td><%= Html.TextBoxFor(m => m.City) %></td>
                        <td class="label">State:</td>
                        <td><%= Html.DropDownListFor(m => m.State, TypeofState.GetValues()) %></td>
                        <td class="label">Zip:</td>
                        <td><%= Html.TextBoxFor(m => m.PostalCode) %></td>
                    </tr>
                    <tr>
                        <td class="label">County:</td>
                        <td colspan="5"><%= Html.TextBoxFor(m => m.County) %></td>
                    </tr>
                </table>
            </fieldset>
        </div>
        <div>
            <fieldset>
                <legend>Mailing Address</legend>
                <table>
                    <tr>
                        <td class="label">Address:</td>
                        <td colspan="3"><%= Html.TextBoxFor(m => m.MailingAddress1) %></td>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td colspan="3"><%= Html.TextBoxFor(m => m.MailingAddress2) %></td>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="label">City:</td>
                        <td><%= Html.TextBoxFor(m => m.MailingCity) %></td>
                        <td class="label">State:</td>
                        <td><%= Html.DropDownListFor(m => m.MailingState, TypeofState.GetValues()) %></td>
                        <td class="label">Zip:</td>
                        <td><%= Html.TextBoxFor(m => m.MailingPostalCode) %></td>
                    </tr>
                </table>
                <table>
                    <tr><td>&nbsp;</td><td class="label">Partial Year:</td><td class="label"> to </td></tr>
                </table>
            </fieldset>
        </div>
        <div>
            <fieldset>
                <legend>Contact</legend>
                <table>
                    <tr>
                        <td class="label">Power of Attorney:</td>
                        <td><%= Html.CheckBoxFor(m => m.PowerofAtorney) %></td>
                        <td colspan="8"></td>
                    </tr>
                    <tr>
                        <td class="label">Prefix:</td>
                        <td><%= Html.DropDownListFor(m => m.ContactPrefix, TypeofNamePrefix.GetValues())%></td>
                        <td class="label">Email:</td>
                        <td><%= Html.TextBoxFor(m => m.ContactEmail) %></td>
                        <td class="label">Address:</td>
                        <td colspan="5"><%= Html.TextBoxFor(m => m.ContactAddress1) %></td>
                    </tr>
                    <tr>
                        <td class="label">First Name:</td>
                        <td><%= Html.TextBoxFor(m => m.ContactFirstName) %></td>
                        <td class="label">Phone:</td>
                        <td><%= Html.TextBoxFor(m => m.ContactPhone) %></td>
                        <td>&nbsp;</td>
                        <td colspan="5"><%= Html.TextBoxFor(m => m.ContactAddress2) %></td>
                    </tr>
                    <tr>
                        <td class="label">Middle Initial:</td>
                        <td><%= Html.TextBoxFor(m => m.ContactMiddleInitial) %></td>
                        <td colspan="3">City:</td>
                        <td><%= Html.TextBoxFor(m => m.ContactCity) %></td>
                        <td class="label">State:</td>
                        <td><%= Html.DropDownListFor(m => m.ContactState, TypeofState.GetValues()) %></td>
                        <td class="label">Zip:</td>
                        <td><%= Html.TextBoxFor(m => m.ContactPostalCode) %></td>
                    </tr>
                    <tr>
                        <td class="label">Last Name:</td>
                        <td><%= Html.TextBoxFor(m => m.ContactLastName) %></td>
                        <td colspan="8">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="label">Suffix:</td>
                        <td><%= Html.TextBoxFor(m => m.ContactSuffix) %></td>
                        <td colspan="8">&nbsp;</td>
                    </tr>
                </table>
            </fieldset>
        </div>
        <div>
            <div style="float: right"><input id="CustomerInformationEditSave" type="button" value="Save"/></div>
            <div style="clear: both"></div>
        </div> 
    </div>
<% } %>
</div>