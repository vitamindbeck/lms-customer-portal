﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<LMS.Models.PoliciesViewModel>" %>
<%@ Import Namespace="LMS.Models" %>
<%@ Import Namespace="System.Web.UI.DataVisualization.Charting" %>
<div id="Policies" class="client-outer halfFloatLeft">
<% using (Html.BeginForm("Policies", "CustomerPortal"))
   { %>
    <div class="section-title">
        <span>Policies</span>
    </div>
    <div>
        <div>
            <% var grid = new WebGrid(Model.Policies); %>
        </div>
    </div>
    <div style="float: right">GBS Notes</div>
    <div style="clear: both"></div>

    
<% } %>
</div>