<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<LMS.Data.GetPlanTransactionStatusResult>>" %>

    

    <% foreach (var item in Model) { %>
    
        <div class="question">
            <b>Q: </b><%= Html.Encode(item.Question) %>
            <br />
            <div style="color: Gray; font-size: 12px">By <%= Html.Encode(LMS.Data.Repository.UserRepository.GetUserByID(item.From_Employee_ID).FullName) %> - <i><%= Html.Encode(String.Format("{0:g}", item.Requested_On)) %></i></div>
        </div>
        <div class="answer">
            <b>A: </b><%= Html.Encode(item.Answer) %>
        </div>    
    <% } %>