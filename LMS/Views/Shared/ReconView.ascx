<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<LMS.Data.GetAllOpenComplaintsResult>>" %>
<%@ Import Namespace="LMS.Helpers" %>
<!DOCTYPE html>
<div class="client-outer">
    <div class="section-title">
        <%= Model.Count().ToString()%>
        Open Complaints
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $("tr:even").css("background-color", "#F4F4F8");
        });
    </script>
    <div class="client-inner">
        <table class="sortable" width="100%" cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th>
                        Lead Name
                    </th>
                    <th>
                        Description
                    </th>
                    <th>
                        Created Date
                    </th>
                    <th>
                        Created By
                    </th>
                    <th>
                        Notes
                    </th>
                </tr>
            </thead>
            <tbody>
                <% foreach (LMS.Data.GetAllOpenComplaintsResult item in Model)
                   {
                       var trClass = string.Empty;

                       if (item.Current_Record_Activity_Status_ID == 33)
                       {
                           trClass = "error";
                       }

                       //var showButton = true;
                       //if (LMS.Data.Repository.UserRepository.GetUser(Page.User.Identity.Name).User_ID != item.User_ID)
                       //{
                       //    showButton = false;
                       //}
                       
                %>
                <tr class="<%= Html.Encode(trClass)%>">
                    <%-- <td>
                        <%if (showButton)
                          {%>
                        <%=Html.ActionLink("Edit", "Edit", "Activity", new { id = item.Record_Activity_ID }, new { target = "_Blank", style = "font-weight: bold;" })%>
                        <%=Html.ActionLink("Complete", "Complete", "Activity", new { id = item.Record_Activity_ID }, new { target = "_Blank", style = "font-weight: bold; color: Green;" })%>
                        <%} %>
                    </td>--%>
                    <td>
                        <%= Html.ActionLink(item.leadName, "Complete", "Activity", new { id = item.Record_Activity_ID }, new { target = "_Blank" })%>
                        <%--<%= Html.Encode(item.leadName)%>--%>
                    </td>
                    <td>
                        <%= Html.Encode(item.Description)%>
                    </td>
                    <td>
                        <%= Html.Encode(item.Created_On)%>
                    </td>
                    <td>
                        <%= Html.Encode(item.agentName) %>
                    </td>
                    <td style="font-style: italic; font-size: smaller;">
                        <%= Html.Encode(StringHelper.DecryptData(item.Notes))%>
                    </td>
                </tr>
                <%} %>
            </tbody>
        </table>
    </div>
</div>
