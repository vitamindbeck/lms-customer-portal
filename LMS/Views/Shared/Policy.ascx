﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<LMS.Models.PolicyViewModel>" %>
<%@ Import Namespace="LMS.Models" %>

<div id="PoliciesEdit" title="<%= Html.Encode(string.Format(" - {0} - {1}", Model.CustomerId, Model.Name)) %>">
<% using (Html.BeginForm("Policy", "CustomerPortal"))
{ %>
    <div>
        <table>
            <tr>
                <td>Created On:</td>
                <td colspan="3"><%= Html.Encode(Model.CreatedOn) %></td>
                <td>Status Date:</td>
                <td><%= Html.TextBoxFor(m => m.StatusDate) %></td>
            </tr>
            <tr>
                <td>Agent:</td>
                <td colspan="3"><%= Html.DropDownListFor(m => m.Agent, TypeofAgent.GetValues()) %></td>
                <td>Policy Number:</td>
                <td><%= Html.TextBoxFor(m => m.PolicyNumber) %></td>
            </tr>
            <tr>
                <td>Source:</td>
                <td colspan="3"><%= Html.DropDownListFor(m => m.Source, SourceCode.GetValues()) %></td>
                <td>Info/Quote Request Date:</td>
                <td><%= Html.TextBoxFor(m => m.InfoQuoteRequestDate) %></td>
            </tr>
            <tr>
                <td>Current Status:</td>
                <td colspan="3"><%= Html.DropDownListFor(m => m.CurrentStatus, TypeofSituation.GetValues()) %></td>
                <td>Info/Quote Sent Date:</td>
                <td><%= Html.TextBoxFor(m => m.InfoQuoteSentDate) %></td>
            </tr>
            <tr>
                <td>Prefilled Application:</td>
                <td><%= Html.CheckBoxFor(m => m.PrefilledApplication) %></td>
                <td>eApplication: </td>
                <td><%= Html.CheckBoxFor(m => m.EApplication) %></td>
                <td>Application Request Date:</td>
                <td><%= Html.TextBoxFor(m => m.ApplicationRequestDate) %></td>
            </tr>
            <tr>
                <td>Inital Enrollment: </td>
                <td><%= Html.CheckBoxFor(m => m.InitialEnrollment) %></td>
                <td>Guaranteed Issue: </td>
                <td><%= Html.CheckBoxFor(m => m.GuaranteedIssue) %></td>
                <td>Application Sent Date</td>
                <td><%= Html.TextBoxFor(m => m.ApplicationSentDate) %></td>
            </tr>
            <tr>
                <td>Carrier:</td>
                <td colspan="3"><%= Html.DropDownListFor(m => m.Carrier, TypeofCarrier.GetValues()) %></td>
                <td>Sent Application Cancelled Date:</td>
                <td><%= Html.TextBoxFor(m => m.SentApplicationCancelledDate) %></td>
            </tr>
            <tr>
                <td>Plan:</td>
                <td colspan="3"><%= Html.DropDownListFor(m => m.Plan, TypeofPlan.GetValues()) %></td>
                <td>Application Received Date:</td>
                <td><%= Html.TextBoxFor(m => m.ApplicationReceivedDate) %></td>
            </tr>
            <tr>
                <td>Documents:</td>
                <td colspan="3"><%= Html.DropDownListFor(m => m.Documents, TypeofDocumentation.GetValues()) %></td>
                <td>Application Submitted Date:</td>
                <td><%= Html.TextBoxFor(m => m.ApplicationSubmittedDate) %></td>
            </tr>
            <tr>
                <td>Sub Plan:</td>
                <td colspan="3"><%= Html.TextBoxFor(m => m.SubPlan) %></td>
                <td>Policy Delivered Date:</td>
                <td><%= Html.TextBoxFor(m => m.PolicyDeliveredDate) %></td>
            </tr>
            <tr>
                <td>Buying Period:</td>
                <td colspan="3"><%= Html.DropDownListFor(m => m.BuyingPeriod, TypeofBuyingPeriod.GetValues()) %></td>
                <td>Policy Placed Date:</td>
                <td><%= Html.TextBoxFor(m => m.PolicyPlacedDate) %></td>
            </tr>
            <tr>
                <td>Sub Buying Period:</td>
                <td colspan="3"><%= Html.DropDownListFor(m => m.SubBuyingPeriod, TypeofBuyingPeriodSub.GetValues()) %></td>
                <td>Coverage Effective Date:</td>
                <td><%= Html.TextBoxFor(m => m.CoverageEffectiveDate) %></td>
            </tr>
            <tr>
                <td>Requested Effective Date:</td>
                <td colspan="3"><%= Html.TextBoxFor(m => m.RequestedEffectiveDate) %></td>
                <td>Application Declined Date:</td>
                <td><%= Html.TextBoxFor(m => m.ApplicationDeclinedDate) %></td>
            </tr>
            <tr>
                <td>Underwriting Class:</td>
                <td colspan="3"><%= Html.DropDownListFor(m => m.UnderwritingClass, TypeofUnderwriting.GetValues()) %></td>
                <td>Application Withdrawn Date:</td>
                <td><%= Html.TextBoxFor(m => m.ApplicationWithdrawnDate) %></td>
            </tr>
            <tr>
                <td>Modal Premium:</td>
                <td colspan="3"><%= Html.TextBoxFor(m => m.ModalPremium) %></td>
                <td>Policy NTO Date:</td>
                <td><%= Html.TextBoxFor(m => m.PolicyNTODate) %></td>
            </tr>
            <tr>
                <td>Mode:</td>
                <td colspan="3"><%= Html.DropDownListFor(m => m.Mode, TypeofModals.GetValues()) %></td>
                <td>Policy Lapsed/Termination Date:</td>
                <td><%= Html.TextBoxFor(m => m.PolicyLapsedTerminationDate) %></td>
            </tr>
            <tr>
                <td>Annual Premium:</td>
                <td colspan="3"><%= Html.TextBoxFor(m => m.AnnualPremium) %></td>
                <td>Custom Notes:</td>
                <td><%= Html.DropDownListFor(m => m.CustomNotes, new List<SelectListItem>()) %></td>
            </tr>
            <tr>
                <td>Cash With App:</td>
                <td colspan="3"><%= Html.CheckBoxFor(m => m.CashWithApp) %></td>
                <td>Special Instructions:</td>
                <td></td>
            </tr>
            <tr>
                <td>Payment Type:</td>
                <td colspan="3"><%= Html.DropDownListFor(m => m.PaymentType, TypeofPaymentMode.GetValues()) %></td>
                <td colspan="2" rowspan="4"><textarea id="txtSpecialInstructions"></textarea></td>
            </tr>
            <tr>
                <td>Shipping Method Outbound:</td>
                <td colspan="3"><%= Html.DropDownListFor(m => m.ShippingMethodOutbound, TypeofShippingMethods.GetValues()) %></td>
            </tr>
            <tr>
                <td>Tracking Number Outbound:</td>
                <td colspan="3"><%= Html.TextBoxFor(m => m.TrackingNumberOutbound) %></td>
            </tr>
            <tr>
                <td>Shipping Method Inbound:</td>
                <td colspan="3"><%= Html.DropDownListFor(m => m.ShippingMethodInbound, TypeofShippingMethods.GetValues()) %></td>
            </tr>
            <tr>
                <td>Tracking Number Inbound:</td>
                <td colspan="3"><%= Html.TextBoxFor(m => m.TrackingNumberInbound) %></td>
                <td>Case Manager:</td>
                <td><%= Html.DropDownListFor(m => m.CaseManager, CaseManagers.GetValues()) %></td>
            </tr>
        </table>
    </div>
    <div>
        <div style="float: right"><input type="button" value="Cancel"/> <input type="button" value="Save"/></div>
        <div style="clear: both"></div>
    </div> 
<% } %>
</div>