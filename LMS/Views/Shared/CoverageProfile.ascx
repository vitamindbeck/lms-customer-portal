﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<LMS.Models.CoverageProfileViewModel>" %>
<%@ Import Namespace="LMS.Models" %>
<div id="CoverageProfile" class="client-outer halfFloatLeft">
<% using (Html.BeginForm("CoverageProfile", "CustomerPortal", new { id = Model.CustomerId}))
   { %>
    <div class="section-title">
        <span>Coverage Profile</span>
        <div class="title-menu" style="float: right">
            <div id="CoverageProfileViewButton"><img src="<%= Url.Content("~/Content/images/view.jpg") %>" alt="View"/></div>
            <div id="CoverageProfileEditButton"><img src="<%= Url.Content("~/Content/images/edit.jpg") %>" alt="Edit"/></div>
        </div>
        <div style="clear: both"></div>
    </div>
    <div>
        <table>
            <tr>
                <td style="width: 300px"><span class="label">Medicare.gov Id:</span> <%= Html.Encode(Model.MedicaregovId) %></td>
                <td><span class="label">State Assistance:</span> <%= Html.Encode(Model.StateAssistance)%></td>
            </tr>
            <tr>
                <td><span class="label">Password Date:</span> <%= Html.Encode(Model.MedicaregovPassword)%></td>
                <td><span class="label">VA Benefits:</span> <%= Html.Encode(Model.VaBenefits)%></td>
            </tr>
            <tr>
                <td><span class="label">Part A Effective Date:</span> <%= Html.Encode(Model.PartAEffectiveDate == DateTime.MinValue ? "" : Model.PartAEffectiveDate.ToString("MM/dd/yyyy")) %></td>
                <td><span class="label">Subsidy:</span> <%= Html.Encode(Model.Subsidy)%></td>
            </tr>
            <tr>
                <td><span class="label">Part B Effective Date:</span> <%= Html.Encode(Model.PartBEffectiveDate == DateTime.MinValue ? "" : Model.PartBEffectiveDate.ToString("MM/dd/yyyy"))%></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td><span class="label">Current Situation:</span> <%= Html.Encode(Model.CurrentSituation)%></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td><span class="label">Product of Interest:</span> <%= Html.Encode(Model.ProductsOfInterest)%></td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </div>
    
    <div id="CoverageProfileEditViewDialog" title="<%= Html.Encode(string.Format("Coverage Profile - {0} - {1}", Model.CustomerId, Model.Name)) %>">
        <div>
            <table>
                <tr>
                    <td class="label">Currently Insured:</td>
                    <td><%= Html.CheckBoxFor(m => m.CurrentlyInsured) %></td>
                    <td class="label">Asked About Drug List:</td>
                    <td><%= Html.EditorFor(m => m.AskedAboutDrugList)%></td>
                    <td class="label">Weight:</td>
                    <td><%= Html.TextBoxFor(m => m.Weight) %></td>
                </tr>
                <tr>
                    <td class="label">Medical Carrier:</td>
                    <td><%= Html.TextBoxFor(m => m.MedicalCarrier) %></td>
                    <td class="label">Medicare.gov Id:</td>
                    <td><%= Html.TextBoxFor(m => m.MedicaregovId) %></td>
                    <td class="label">Height:</td>
                    <td><%= Html.TextBoxFor(m => m.Height) %></td>
                </tr>
                <tr>
                    <td class="label">Drug Plan Co:</td>
                    <td><%= Html.TextBoxFor(m => m.DrugPlanCo) %></td>
                    <td class="label">Medicare.gov Password:</td>
                    <td><%= Html.TextBoxFor(m => m.MedicaregovPassword) %></td>
                    <td class="label">Subsidy:</td>
                    <td><%= Html.TextBoxFor(m => m.Subsidy) %></td>
                </tr>
                <tr>
                    <td class="label">Medical Carrier Original Start Date:</td>
                    <td><%= Html.EditorFor(m => m.MedicalCarrierOriginalStartDate)%></td>
                    <td class="label">Medicare Number:</td>
                    <td><%= Html.TextBoxFor(m => m.MedicareNumber) %></td>
                    <td class="label">State Assistance:</td>
                    <td><%= Html.CheckBoxFor(m => m.StateAssistance) %></td>
                </tr>
                <tr>
                    <td class="label">Drug Carrier Original Start Date:</td>
                    <td><%= Html.EditorFor(m => m.DrugCarrierOriginalStartDate)%></td>
                    <td class="label">Part A Effective Date:</td>
                    <td><%= Html.EditorFor(m => m.PartAEffectiveDate)%></td>
                    <td class="label">VA Benefits:</td>
                    <td><%= Html.CheckBoxFor(m => m.VaBenefits) %></td>
                </tr>
                <tr>
                    <td class="label">Current Plan Type:</td>
                    <td><%= Html.EditorFor(m => m.CurrentPlanType)%></td>
                    <td class="label">Part B Effective Date:</td>
                    <td><%= Html.EditorFor(m => m.PartBEffectiveDate)%></td>
                    <td class="label">Current Tobacco Use:</td>
                    <td><%= Html.CheckBoxFor(m => m.CurrentTobaccoUse)%></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td class="label">Products Of Interest:</td>
                    <td><%= Html.DropDownListFor(m => m.ProductsOfInterest, TypeofProducts.GetValues()) %></td>
                    <td class="label">Current Tobacco Type:</td>
                    <td><%= Html.DropDownListFor(m => m.CurrentTobaccoType, TypeofTobaccoUse.GetValues()) %></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td class="label">Current Situation:</td>
                    <td><%= Html.DropDownListFor(m => m.CurrentSituation, StatusofSituation.GetValues())%></td>
                    <td class="label">Past Tobacco Use:</td>
                    <td><%= Html.CheckBoxFor(m => m.PastTobaccoUse)%></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td class="label">Requested Start Date:</td>
                    <td><%= Html.EditorFor(m => m.RequestedStartDate)%></td>
                    <td class="label">Past Tobacco Type:</td>
                    <td><%= Html.DropDownListFor(m => m.PastTobaccoType, TypeofTobaccoUse.GetValues()) %></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td class="label">Date Quit:</td>
                    <td><%= Html.EditorFor(m => m.DateQuit)%></td>
                </tr>
            </table>
        </div>
        <div>
            <div style="float: right"><input type="button" value="Save"/></div>
            <div style="clear: both"></div>
        </div> 
    </div>
<% } %>
</div>
