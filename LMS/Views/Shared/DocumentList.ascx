<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<LMS.Data.Plan_Document>>" %>
<% if (Model.Count() > 0)
   { %>
<table cellpadding="0" cellspacing="0" style=" text-align: left;" class="subtable">
    <thead>
        <tr>
            <th></th>
            <th>
                State
            </th>
            <th>
                Document
            </th>
            <th>
                Active
            </th>
            <th>
                Display Order
            </th>
        </tr>
    </thead>
    <tbody>
    <% foreach (var item in Model)
       { %>
       
    <tr>
        <td>
            <%= Html.ActionLink("Edit", "Edit", "Document", new { id=item.Plan_Document_ID },null) %>
        </td>
        <td>
            <%= Html.Encode(item.State.Name) %>
        </td>
        <td>
            <%= Html.Encode(item.Title) %>
        </td>
        <td>
            <%= Html.Encode(item.Active) %>
        </td>
        <td>
            <%= Html.Encode(item.Display_Order) %>
        </td>
    </tr>
    <% } %>
    </tbody>
</table>
<% } %>
