﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<LMS.Models.CustomerProfileViewModel>" %>
<div id="CustomerProfile" class="client-outer halfFloatLeft">
<% using (Html.BeginForm("CustomerProfile", "CustomerPortal", new { id = Model.CustomerId}))
   { %>
    <div class="section-title">
        <span>Customer Profile</span> 
        <div class="title-menu" style="float: right">
            <div id="CustomerProfileViewButton"><img src="<%= Url.Content("~/Content/images/view.jpg") %>" alt="View"/></div>
            <div id="CustomerProfileEditButton"><img src="<%= Url.Content("~/Content/images/edit.jpg") %>" alt="Edit"/></div>
        </div>
        <div style="clear: both"></div>
    </div>
    <div>
        <table>
            <tr>
                <td><span class="label">Health:</span> <%= Html.Encode(Model.Health) %></td>
                <td><span class="label">Brand Preference:</span> <%= Html.Encode(!string.IsNullOrWhiteSpace(Model.BrandPreferenceComments)) %></td>
            </tr>
            <tr>
                <td><span class="label">Wealth:</span> <%= Html.Encode(Model.Wealth) %></td>
                <td><span class="label">Traveler:</span> <%= Html.Encode(!string.IsNullOrWhiteSpace(Model.TravelerComments)) %></td>
            </tr>
            <tr>
                <td><span class="label">Descision Making:</span> <%= Html.Encode(Model.Decision) %></td>
                <td><span class="label">Future Health Concern:</span> <%= Html.Encode(!string.IsNullOrWhiteSpace(Model.FutureHealthConcernsComments)) %></td>
            </tr>
            <tr>
                <td><span class="label">Risk:</span> <%= Html.Encode(Model.Risk) %></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td><span class="label">Provider Preference:</span> <%= Html.Encode(Model.ProviderPreference) %></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td><span class="label">Cost Sensitivity:</span> <%= Html.Encode(Model.CostSensitivity) %></td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </div>
    <div id="CustomerProfileEditViewDialog" title="<%= Html.Encode(string.Format("Customer Profile - {0} - {1}", Model.CustomerId, Model.Name)) %>">
        <div>
            <table>
                <tr class="selectable-headerrow">
                    <td class="label">Health:</td>
                    <td class="label">Wealth:</td>
                    <td class="label">Decision:</td>
                    <td class="label">Risk:</td>
                    <td class="label">Cost Sensitivity:</td>
                    <td class="label">Provider Preference:</td>
                </tr>
                <tr>
                    <td class="selectable">
                        <ol id="selectableHealth" style="width: 170px">
							<li class="ui-widget-content">Perfect&nbsp;<span id="Health5" class="label">5</span></li>
							<li class="ui-widget-content">Very&nbsp;Good&nbsp;<span id="Health4" class="label">4</span></li>
							<li class="ui-widget-content">Good&nbsp;<span id="Health3" class="label">3</span></li>
							<li class="ui-widget-content">Fair&nbsp;<span id="Health2" class="label">2</span></li>
							<li class="ui-widget-content">Poor&nbsp;<span id="Health1" class="label">1</span></li>
						</ol>
                    </td>
                    <td class="selectable">
                        <ol id="selectableWealth" style="width: 170px">
							<li class="ui-widget-content">Wealthy&nbsp;<span id="Wealth5" class="label">5</span></li>
							<li class="ui-widget-content">Well Off&nbsp;<span id="Wealth4" class="label">4</span></li>
							<li class="ui-widget-content">Comfortable&nbsp;<span id="Wealth3" class="label">3</span></li>
							<li class="ui-widget-content">Getting&nbsp;By&nbsp;<span id="Wealth2" class="label">2</span></li>
							<li class="ui-widget-content">Poverty&nbsp;Level&nbsp;<span id="Wealth1" class="label">1</span></li>
						</ol>
                    </td>
                    <td class="selectable">
                        <ol id="selectableDecision" style="width: 170px">
							<li class="ui-widget-content">Just&nbsp;the&nbsp;Facts;&nbsp;I'll&nbsp;Decide&nbsp;<span id="Decision5" class="label">5</span></li>
							<li class="ui-widget-content">More Data; Less Visual&nbsp;<span id="Decision4" class="label">4</span></li>
							<li class="ui-widget-content">Visual and Data&nbsp;<span id="Decision3" class="label">3</span></li>
							<li class="ui-widget-content">More Visual; Less Data&nbsp;<span id="Decision2" class="label">2</span></li>
							<li class="ui-widget-content">Tell Me What To Do&nbsp;<span id="Decision1" class="label">1</span></li>
						</ol>
                    </td>
                    <td class="selectable">
                        <ol id="selectableRisk" style="width: 170px">
							<li class="ui-widget-content">No Out of Pocket&nbsp;<span id="Risk5" class="label">5</span></li>
							<li class="ui-widget-content">Low Out of Pocket&nbsp;<span id="Risk4" class="label">4</span></li>
							<li class="ui-widget-content">Moderate&nbsp;Out&nbsp;of&nbsp;Pocket&nbsp;<span id="Risk3" class="label">3</span></li>
							<li class="ui-widget-content">High Out of Pocket&nbsp;<span id="Risk2" class="label">2</span></li>
							<li class="ui-widget-content">No cap&nbsp;<span id="Risk1" class="label">1</span></li>
						</ol>
                    </td>
                    <td class="selectable">
                        <ol id="selectableCostSensitivity" style="width: 200px">
							<li class="ui-widget-content">All Cost Changes&nbsp;<span id="CostSensitivity5" class="label">5</span></li>
							<li class="ui-widget-content">Most Cost Changes&nbsp;<span id="CostSensitivity4" class="label">4</span></li>
							<li class="ui-widget-content">Moderate Cost Changes&nbsp;<span id="CostSensitivity3" class="label">3</span></li>
							<li class="ui-widget-content">Big Changes Only&nbsp;<span id="CostSensitivity2" class="label">2</span></li>
							<li class="ui-widget-content">Don't&nbsp;Bother&nbsp;Me&nbsp;About&nbsp;Price&nbsp;<span id="CostSensitivity1" class="label">1</span></li>
						</ol>
                    </td>
                    <td class="selectable">
                        <ol id="selectableProviderPreference" style="width: 170px">
							<li class="ui-widget-content">Include All&nbsp;<span id="ProviderPreference5" class="label">5</span></li>
							<li class="ui-widget-content">Include all but one&nbsp;<span id="ProviderPreference4" class="label">4</span></li>
							<li class="ui-widget-content">Include most&nbsp;<span id="ProviderPreference3" class="label">3</span></li>
							<li class="ui-widget-content">Willing&nbsp;to&nbsp;Change&nbsp;most&nbsp;<span id="ProviderPreference2" class="label">2</span></li>
							<li class="ui-widget-content">Willing to Change all&nbsp;<span id="ProviderPreference1" class="label">1</span></li>
						</ol>
                    </td>
                </tr>
            </table>
        </div>
        <div>
            <table class="customerProfileFreeText">
                <tr>
                    <td>Traveler:</td>
                    <td>Brand Preference:</td>
                    <td>Future Health Concerns:</td>
                </tr>
                <tr>
                    <td><%= Html.TextAreaFor(m => m.TravelerComments) %></td>
                    <td><%= Html.TextAreaFor(m => m.BrandPreferenceComments) %></td>
                    <td><%= Html.TextAreaFor(m => m.FutureHealthConcernsComments) %></td>
                </tr>
            </table>
        </div>
        <div>
            <div style="float: right"><input type="button" value="Save"/></div>
            <div style="clear: both"></div>
        </div> 
    </div>
    <%= Html.HiddenFor(m => m.Health, new { id = "Health" })%>
    <%= Html.HiddenFor(m => m.Wealth, new { id = "Wealth" })%>
    <%= Html.HiddenFor(m => m.Decision, new { id = "Decision" })%>
    <%= Html.HiddenFor(m => m.Risk, new { id = "Risk" })%>
    <%= Html.HiddenFor(m => m.CostSensitivity, new { id = "CostSensitivity" })%>
    <%= Html.HiddenFor(m => m.ProviderPreference, new { id = "ProviderPreference" })%>
<% } %>
</div>