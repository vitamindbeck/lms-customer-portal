﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Bootstrapper.cs" company="Longevity Alliance">
//   Longevity Alliance Inc 2014
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace LMS
{
    using global::StructureMap;
    using global::StructureMap.Configuration.DSL;

    using LmsDataRepository.Models;

    using WizzerLib.Implementations;
    using WizzerLib.Interfaces;

    /// <summary>
    ///     The boot strapper.
    /// </summary>
    public class Bootstrapper : Registry
    {
        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="Bootstrapper" /> class.
        /// </summary>
        public Bootstrapper()
        {
            // Service implementation
            this.For<IRecordActivityRules>().Use<RecordActivityRules>();            
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The bootstrap.
        /// </summary>
        public static void Bootstrap()
        {
            ObjectFactory.Initialize(x => x.AddRegistry<Bootstrapper>());
        }

        #endregion
    }
}