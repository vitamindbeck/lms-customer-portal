﻿////***********************************************************
//// Events

$(document).ready(function() {

    //-----------------------------------------------
    //Register events

    //-----------------------------------------------
    // Click the Customer Information view button
    $("#CustomerInformationViewButton").click(function () {
        $("#CustomerInformationEditViewDialog :input").attr("disabled", "disabled");
        $("#CustomerInformationEditViewDialog").dialog("open");
    });

    //-----------------------------------------------
    // Click the Customer Information edit button
    $("#CustomerInformationEditButton").click(function () {
        $("#CustomerInformationEditViewDialog :input").removeAttr("disabled");
        $("#CustomerInformationEditViewDialog").dialog("open");
    });

    //-----------------------------------------------
    // Click the Coverage Profile view button
    $("#CoverageProfileViewButton").click(function () {
        $("#CoverageProfileEditViewDialog :input").attr("disabled", "disabled");
        $("#CoverageProfileEditViewDialog").dialog("open");
    });

    //-----------------------------------------------
    // Click the Coverage Profile edit button
    $("#CoverageProfileEditButton").click(function () {
        $("#CoverageProfileEditViewDialog :input").removeAttr("disabled");
        $("#CoverageProfileEditViewDialog").dialog("open");
    });

    //-----------------------------------------------
    // Click the Customer Profile view button
    $("#CustomerProfileViewButton").click(function () {
        $("#CustomerProfileEditViewDialog :input").attr("disabled", "disabled");
        $("#CustomerProfileEditViewDialog").dialog("open");
    });

    //-----------------------------------------------
    // Click the Customer Profile edit button
    $("#CustomerProfileEditButton").click(function () {
        $("#CustomerProfileEditViewDialog :input").removeAttr("disabled");
        $("#CustomerProfileEditViewDialog").dialog("open");
    });

    //-----------------------------------------------
    // Selectable Health
    $("#selectableHealth").singleselectable(
    {
        selected: function () {
            var value = $("li.ui-selected span").text();
            $("#Health").val(value);
        }
    });

    //-----------------------------------------------
    // Selectable Wealth
    $("#selectableWealth").singleselectable(
    {
        selected: function () {
            var value = $("li.ui-selected span").text();
            $("#Wealth").val(value);
        }
    });

    //-----------------------------------------------
    // Selectable Decision
    $("#selectableDecision").singleselectable(
    {
        selected: function () {
            var value = $("li.ui-selected span").text();
            $("#Decision").val(value);
        }
    });

    //-----------------------------------------------
    // Selectable Risk
    $("#selectableRisk").singleselectable(
    {
        selected: function () {
            var value = $("li.ui-selected span").text();
            $("#Risk").val(value);
        }
    });

    //-----------------------------------------------
    // Selectable Cost Sensitivity
    $("#selectableCostSensitivity").singleselectable(
    {
        selected: function () {
            var value = $("li.ui-selected span").text();
            $("#CostSensitivity").val(value);
        }
    });

    //-----------------------------------------------
    // Selectable Provider Preference
    $("#selectableProviderPreference").singleselectable(
    {
        selected: function () {
            var value = $("li.ui-selected span").text();
            $("#ProviderPreference").val(value);
        }
    });

    //-----------------------------------------------
    // inital load functions
    HideDialogs();
    InitDatePicker();
    LoadSelectables();

    // Hide B2E
    $("#B2EProfile").hide();
});

////***********************************************************
//// Methods

function LoadSelectables() {

    if ($("#Health").val() != null || $("#Health").val() != "") {
        var selectedHealth = "#Health" + $("#Health").val();
        $(selectedHealth).addClass("ui-selected");
    }

    if ($("#Wealth").val() != null || $("#Wealth").val() != "") {
        var selectedWealth = "#Wealth" + $("#Wealth").val();
        $(selectedWealth).addClass("ui-selected");
    }

    if ($("#Decision").val() != null || $("#Decision").val() != "") {
        var selectedDecision = "#Decision" + $("#Decision").val();
        $(selectedDecision).addClass("ui-selected");
    }

    if ($("#Risk").val() != null || $("#Risk").val() != "") {
        var selectedRisk = "#Risk" + $("#Risk").val();
        $(selectedRisk).addClass("ui-selected");
    }

    if ($("#CostSensitivity").val() != null || $("#CostSensitivity").val() != "") {
        var selectedCostSensitivity = "#CostSensitivity" + $("#CostSensitivity").val();
        $(selectedCostSensitivity).addClass("ui-selected");
    }

    if ($("#ProviderPreference").val() != null || $("#ProviderPreference").val() != "") {
        var selectedProviderPreference = "#ProviderPreference" + $("#ProviderPreference").val();
        $(selectedProviderPreference).addClass("ui-selected");
    }
}

function InitDatePicker() {
    $("#FollowUpDate").datepicker();
    $("#BirthDate").datepicker();
    $("#MedicalCarrierOriginalStartDate").datepicker();
    $("#DrugCarrierOriginalStartDate").datepicker();
    $("#AskedAboutDrugList").datepicker();
    $("#PartAEffectiveDate").datepicker();
    $("#PartBEffectiveDate").datepicker();
    $("#RequestedStartDate").datepicker();
    $("#DateQuit").datepicker();

}

function HideDialogs() {
    // Create dialog boxes
    $("#CustomerInformationEditViewDialog").dialog(
    {
        autoOpen: false,
        modal: true,
        width: 1200,
        draggable: false,
        show: {
            effect: "blind",
            duration: 250
        },
        hide: {
            effect: "explode",
            duration: 250
        }
    });

    $("#B2EProfileEdit").dialog(
    {
        autoOpen: false,
        modal: true,
        width: 1200,
        draggable: false,
        show: {
            effect: "blind",
            duration: 250
        },
        hide: {
            effect: "explode",
            duration: 250
        }
    });

    $("#CustomerProfileEditViewDialog").dialog(
    {
        autoOpen: false,
        modal: true,
        width: 1200,
        draggable: false,
        show: {
            effect: "blind",
            duration: 250
        },
        hide: {
            effect: "explode",
            duration: 250
        }
    });

    $("#PoliciesEdit").dialog(
    {
        autoOpen: false,
        modal: true,
        width: 1200,
        draggable: false,
        show: {
            effect: "blind",
            duration: 250
        },
        hide: {
            effect: "explode",
            duration: 250
        }
    });

    $("#CoverageProfileEditViewDialog").dialog(
    {
        autoOpen: false,
        modal: true,
        width: 1200,
        draggable: false,
        show: {
            effect: "blind",
            duration: 250
        },
        hide: {
            effect: "explode",
            duration: 250
        }
    });

    $("#B2EProfileEdit").dialog(
    {
        autoOpen: false,
        modal: true,
        width: 1200,
        draggable: false,
        show: {
            effect: "blind",
            duration: 250
        },
        hide: {
            effect: "explode",
            duration: 250
        }
    });

    $("#currentActivityModal").dialog({ 
        autoOpen: false,
        modal: true,
        width: 1200,
        draggable: false,
        show: {
            effect: "blind",
            duration: 250
        },
        hide: {
            effect: "explode",
            duration: 250
        }
    });

    $("#FollowUpActivityModal").dialog({ 
        autoOpen: false,
        modal: true,
        width: 1200,
        draggable: false,
        show: {
            effect: "blind",
            duration: 250
        },
        hide: {
            effect: "explode",
            duration: 250
        }
    });
}