﻿$.widget("ui.singleselectable", $.ui.mouse, {
    version: "@VERSION",
    options: {
        appendTo: 'body',
        autoRefresh: true,
        distance: 0,
        filter: '*',
        tolerance: 'touch'
    },
    _create: function () {
        var that = this;

        this.element.addClass("ui-selectable");

        // cache selectee children based on filter
        var selectees;
        this.refresh = function () {
            selectees = $(that.options.filter, that.element[0]);
            selectees.addClass("ui-selectee");
            selectees.each(function () {
                var $this = $(this);
                var pos = $this.offset();
                $.data(this, "selectable-item", {
                    element: this,
                    $element: $this,
                    left: pos.left,
                    top: pos.top,
                    right: pos.left + $this.outerWidth(),
                    bottom: pos.top + $this.outerHeight(),
                    startselected: false,
                    selected: $this.hasClass('ui-selected'),
                    selecting: $this.hasClass('ui-selecting'),
                    unselecting: $this.hasClass('ui-unselecting')
                });
            });
        };
        this.refresh();

        this.selectees = selectees.addClass("ui-selectee");

        this._mouseInit();

        this.helper = $("<div class='ui-selectable-helper'></div>");
    },

    _destroy: function () {
        this.selectees
            .removeClass("ui-selectee")
            .removeData("selectable-item");
        this.element
            .removeClass("ui-selectable ui-selectable-disabled");
        this._mouseDestroy();
    },

    _mouseStart: function (event) {
        var that = this;

        this.opos = [event.pageX, event.pageY];

        if (this.options.disabled)
            return;

        var options = this.options;

        this.selectees = $(options.filter, this.element[0]);

        this._trigger("start", event);

        $(options.appendTo).append(this.helper);
        // position helper (lasso)
        this.helper.css({
            "left": event.clientX,
            "top": event.clientY,
            "width": 0,
            "height": 0
        });

        if (options.autoRefresh) {
            this.refresh();
        }

        this.selectees.filter('.ui-selected').each(function () {
            var selectee = $.data(this, "selectable-item");
            selectee.startselected = true;
            if (!event.metaKey) {
                selectee.$element.removeClass('ui-selected');
                selectee.selected = false;
                selectee.$element.addClass('ui-unselecting');
                selectee.unselecting = true;
                // selectable UNSELECTING callback
                that._trigger("unselecting", event, {
                    unselecting: selectee.element
                });
            }
        });

        $(event.target).parents().andSelf().each(function () {
            var selectee = $.data(this, "selectable-item");
            if (selectee) {
                var doSelect = (!event.metaKey && !event.ctrlKey) || !selectee.$element.hasClass('ui-selected');
                selectee.$element
                    .removeClass(doSelect ? "ui-unselecting" : "ui-selected")
                    .addClass(doSelect ? "ui-selecting" : "ui-unselecting");
                selectee.unselecting = !doSelect;
                selectee.selecting = doSelect;
                selectee.selected = doSelect;
                // selectable (UN)SELECTING callback
                if (doSelect) {
                    that._trigger("selecting", event, {
                        selecting: selectee.element
                    });
                } else {
                    that._trigger("unselecting", event, {
                        unselecting: selectee.element
                    });
                }
                return false;
            }
        });
    },

    _mouseStop: function (event) {
        var that = this;

        var options = this.options;

        $('.ui-unselecting', this.element[0]).each(function () {
            var selectee = $.data(this, "selectable-item");
            selectee.$element.removeClass('ui-unselecting');
            selectee.unselecting = false;
            selectee.startselected = false;
            that._trigger("unselected", event, {
                unselected: selectee.element
            });
        });
        $('.ui-selecting', this.element[0]).each(function () {
            var selectee = $.data(this, "selectable-item");
            selectee.$element.removeClass('ui-selecting').addClass('ui-selected');
            selectee.selecting = false;
            selectee.selected = true;
            selectee.startselected = true;
            that._trigger("selected", event, {
                selected: selectee.element
            });
        });
        this._trigger("stop", event);

        this.helper.remove();

        return false;
    }
});