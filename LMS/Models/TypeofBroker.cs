﻿namespace LMS.Models
{
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Web.Mvc;

    using LmsDataRepository.Models;

    public class TypeofBroker
    {

        public static IEnumerable<SelectListItem> GetValues()
        {
            var selectList = new List<SelectListItem>();
            using (var context = new LMSContext())
            {
                var brokers = context.Brokers.ToList();
                brokers.ForEach(t => selectList.Add(
                    new SelectListItem
                        {
                            Value = t.Broker_ID.ToString(CultureInfo.InvariantCulture), 
                            Text = t.First_Name + t.Last_Name
                        }));
            }

            return selectList;
        }

        public static string GetName(int value)
        {
            Broker broker;
            using (var context = new LMSContext())
            {
                broker = context.Brokers.FirstOrDefault(t => t.Broker_ID == value);
            }

            return broker == null 
                ? string.Empty
                : broker.First_Name + broker.Last_Name;
        }
    }
}