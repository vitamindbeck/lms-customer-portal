﻿namespace LMS.Models
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Web.Mvc;

    public class TypeofNamePrefix
    {
        public enum NamePrefixType
        {
            Mr,
            Mrs,
            Ms,
            Miss,
            Dr,
            Rev,
            Fr,
            Atty,
            Prof,
            Hon,
            Pres,
            Gov
        }

        public static IEnumerable<SelectListItem> GetValues()
        {
            return (from object item in Enum.GetValues(typeof(NamePrefixType))
                    select
                        new SelectListItem
                        {
                            Text = item.ToString(),
                            Value = ((int)item).ToString(CultureInfo.InvariantCulture)
                        }).ToList();
        }

        public static string GetName(int value)
        {
            return Enum.GetName(typeof(NamePrefixType), value);
        }
    }
}