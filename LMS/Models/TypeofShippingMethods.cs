﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LMS.Models
{
    using System.Globalization;
    using System.Web.Mvc;

    using LmsDataRepository.Models;

    public class TypeofShippingMethods
    {
        public enum ShippingMethodType
        {
            Email = 1,
            Regular = 2,
            Express = 3,
            Fax = 4,
            Priority = 5,
            DRXOnLine = 6,
            InPerson = 7,
            CarrierDirect = 8
        }

        public static IEnumerable<SelectListItem> GetValues()
        {
            return (from object item in Enum.GetValues(typeof(ShippingMethodType))
                    select
                        new SelectListItem
                        {
                            Text = item.ToString(),
                            Value = ((int)item).ToString(CultureInfo.InvariantCulture)
                        }).ToList();
        }

        public static string GetName(int value)
        {
            return Enum.GetName(typeof(ShippingMethodType), value);
        }
    }
}