﻿using System;

namespace LMS.Models
{
    public class Policy
    {
        public string CustomerId { get; set; }
        public string Name { get; set; }
        public DateTime CreatedOn { get; set; }
        public int Agent { get; set; }
        public int Source { get; set; }
        public int CurrentStatus { get; set; }
        public bool PrefilledApplication { get; set; }
        public bool EApplication { get; set; }
        public bool InitialEnrollment { get; set; }
        public bool GuaranteedIssue { get; set; }
        public int Carrier { get; set; }
        public int Plan { get; set; }
        public int Documents { get; set; }
        public string SubPlan { get; set; }
        public int BuyingPeriod { get; set; }
        public int SubBuyingPeriod { get; set; }
        public DateTime RequestedEffectiveDate { get; set; }
        public int UnderwritingClass { get; set; }
        public string ModalPremium { get; set; }
        public int Mode { get; set; }
        public string AnnualPremium { get; set; }
        public bool CashWithApp { get; set; }
        public int PaymentType { get; set; }
        public int ShippingMethodOutbound { get; set; }
        public string TrackingNumberOutbound { get; set; }
        public int ShippingMethodInbound { get; set; }
        public string TrackingNumberInbound { get; set; }
        public DateTime StatusDate { get; set; }
        public string PolicyNumber { get; set; }
        public DateTime InfoQuoteRequestDate { get; set; }
        public DateTime InfoQuoteSentDate { get; set; }
        public DateTime ApplicationRequestDate { get; set; }
        public DateTime ApplicationSentDate { get; set; }
        public DateTime SentApplicationCancelledDate { get; set; }
        public DateTime ApplicationReceivedDate { get; set; }
        public DateTime ApplicationSubmittedDate { get; set; }
        public DateTime PolicyDeliveredDate { get; set; }
        public DateTime PolicyPlacedDate { get; set; }
        public DateTime CoverageEffectiveDate { get; set; }
        public DateTime ApplicationDeclinedDate { get; set; }
        public DateTime ApplicationWithdrawnDate { get; set; }
        public DateTime PolicyNTODate { get; set; }
        public DateTime PolicyLapsedTerminationDate { get; set; }
        public int CustomNotes { get; set; }
        public string SpecialInstructions { get; set; }
        public int CaseManager { get; set; }
    }
}