﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LMS.Models
{
    using System.Globalization;
    using System.Web.Mvc;

    public class TypeofRecordActivityNote
    {

        public enum RecordActivityNoteType
        {
            UNKNOWN = 1,
            SendingInfoQuote = 2,
            SendingApplication = 3,
            FollowUponInfoQuote = 15,
            FollowUponApplication = 16,
            AttemptingContact = 17,
            CallBackAppointment = 20,
            SendMediGapGuide = 21,
            SentMediGapGuide = 22,
            SendCostCutGuide = 25,
            SendMovingGuide = 26,
            SendAttemptContactPostCard = 27,
            SentCostCutGuide = 28,
            SentMovingGuide = 29,
            SentAttemptContactPostCard = 30,
            MedsRequest = 33,
            LM = 34,
            HelloEmail = 35,
            LMandSentHelloEmail = 36,
            FollowUponEmail = 37,
            FollowUponVoicemail = 38,
            FollowUponOther = 39,
            WaitingforNewRates = 40,
            AEPReminderpermissiontocontact = 41,
            ReferredtoFirstHealth = 42,
            Help = 43,
            ContactedSwitching = 44,
            ContactedConsidering = 45,
            ContactedKeeping = 46,
            MedicallyUninsurable = 47,
            ReferredtoSHIP = 48,
            WentbacktoOriginalMedicare = 49,
            Verifiedcontactinfoisinvalid = 50,
            AEPReviewMA = 51,
            AEPReviewPDP = 52,
            Deceased = 53,
            FirstAttempt = 54,
            SecondAttempt = 55,
            ThrindAttempt = 56,
            FinalAttempt = 57,
            UnderAge62 = 58,
            RefferedtoSHIP = 59,
            UnabletoImproveRate = 60,
            NoLongerNeedInsurance = 61,
            WentBacktoOriginalMedicare = 62,
            RequiredtoBuyElsewhere = 63,
            CustomerHangsUp = 64,
            LanguageBarrier = 65,
            ReferredtoFieldAgent = 66,
            LapsedTerminated = 67,
            PrefilledApp = 68,
            AskedReferral = 69,
            PlanReview = 70,
            SuppReview = 71
        }

        public static IEnumerable<SelectListItem> GetValues()
        {
            return (from object item in Enum.GetValues(typeof(TypeofRecordActivityNote.RecordActivityNoteType))
                    select
                        new SelectListItem
                        {
                            Text = item.ToString(),
                            Value = ((int)item).ToString(CultureInfo.InvariantCulture)
                        }).ToList();
        }

        public static string GetName(int value)
        {
            return Enum.GetName(typeof(TypeofRecordActivityNote.RecordActivityNoteType), value);
        }
    }
}