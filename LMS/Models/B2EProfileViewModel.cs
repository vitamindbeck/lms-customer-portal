﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LMS.Models
{
    public class B2EProfileViewModel
    {
        public string CustomerId { get; set; }
        public string Name { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string City { get; set; }

        public int State { get; set; }

        public string PostalCode { get; set; }

        public string CompanyPresident { get; set; }

        public string CompanyPhone { get; set; }

        public string PrimaryContact { get; set; }

        public string PrimaryContactTitle { get; set; }

        public string PrimaryContactPhone { get; set; }

        public string WebAddress { get; set; }

        public int Industry { get; set; }

        public string EmailAddress { get; set; }

        public string PrimaryEmail { get; set; }

        public int CompanySize { get; set; }

        }
}