﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RequestModel.cs" company="Longevity Alliance">
//   Longevity Alliance Inc 2014
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace LMS.Models
{
    /// <summary>
    /// The request.
    /// </summary>
    public class Request : IRequest
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the activity.
        /// </summary>
        public int Activity { get; set; }

        /// <summary>
        /// Gets or sets the campaign code.
        /// </summary>
        public int CampaignCode { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        public int Email { get; set; }

        /// <summary>
        /// Gets or sets the lead disposition.
        /// </summary>
        public int LeadDisposition { get; set; }

        /// <summary>
        /// Gets or sets the notes.
        /// </summary>
        public int Notes { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        public int Status { get; set; }

        #endregion
    }
}