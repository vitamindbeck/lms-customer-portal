﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LMS.Models
{
    using System.Globalization;
    using System.Web.Mvc;

    public class TypeofReconciliation
    {

        public enum ReconciliationType
        {
            AttemptingToContact = 47 ,
            UnableToContactRecontact = 21 ,
            KeepingCurrentPlan = 5 ,
            ComplaintOpen = 39 ,
            ComplaintInProcess = 64 ,
            ComplaintResolved = 40 ,
            BoughtElsewhereTakenCareOf = 6 ,
            ContactedInProgress = 45 ,
            AppInProgress = 43 ,
            AppReceived = 32 ,
            PostsaleWorkNewClient = 49 ,
            PostSaleWorkReplacement = 50 ,
            IneligibleNoBuyingPeriod = 24 ,
            InvalidInformation = 2 ,
            Deceased = 20 ,
            LostProspect = 51 ,
            DNC = 42 ,
            CustomerHangsUp = 26 ,
            LapsedTerminated = 38 ,
            ReferredOutPermissiontoContact = 61 ,
            ReferredOutLost = 62 ,
            PostServiceWork = 63
        }

        public static IEnumerable<SelectListItem> GetValues()
        {
            return (from object item in Enum.GetValues(typeof(TypeofReconciliation.ReconciliationType))
                    select
                        new SelectListItem
                        {
                            Text = item.ToString(),
                            Value = ((int)item).ToString(CultureInfo.InvariantCulture)
                        }).ToList();
        }

        public static string GetName(int value)
        {
            return Enum.GetName(typeof(TypeofReconciliation.ReconciliationType), value);
        }

        public static IEnumerable<SelectListItem> ComplaintRecListItems(StatusofComplaint.ComplaintStatus statusofComplaint)
        {
            var selectList = new List<SelectListItem>();
            switch (statusofComplaint)
            {
                case StatusofComplaint.ComplaintStatus.All:
                    selectList = (from object item in Enum.GetValues(typeof(TypeofReconciliation.ReconciliationType))
                                  select
                                      new SelectListItem
                                      {
                                          Text = item.ToString(),
                                          Value = ((int)item).ToString(CultureInfo.InvariantCulture)
                                      }).ToList();
                    break;

                case StatusofComplaint.ComplaintStatus.InProgress:
                    selectList = (from object item in Enum.GetValues(typeof(TypeofReconciliation.ReconciliationType))
                                  where (int)item != 39 || (int)item != 40
                                  select
                                      new SelectListItem
                                      {
                                          Text = item.ToString(),
                                          Value = ((int)item).ToString(CultureInfo.InvariantCulture)
                                      }).ToList();
                    break;

                case StatusofComplaint.ComplaintStatus.Open:
                    selectList = (from object item in Enum.GetValues(typeof(TypeofReconciliation.ReconciliationType))
                                  where (int)item != 64 || (int)item != 40
                                  select
                                      new SelectListItem
                                      {
                                          Text = item.ToString(),
                                          Value = ((int)item).ToString(CultureInfo.InvariantCulture)
                                      }).ToList();
                    break;

                case StatusofComplaint.ComplaintStatus.Resoloved:
                    selectList = (from object item in Enum.GetValues(typeof(TypeofReconciliation.ReconciliationType))
                                  where (int)item != 39
                                  select
                                      new SelectListItem
                                      {
                                          Text = item.ToString(),
                                          Value = ((int)item).ToString(CultureInfo.InvariantCulture)
                                      }).ToList();
                    break;

                default:
                    throw new Exception("UNKNOWN_ENUM_VALUE");
                    break;
            }

            return selectList;
        }
    }
}