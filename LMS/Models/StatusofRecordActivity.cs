﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LMS.Models
{
    using System.Globalization;
    using System.Web.Mvc;

    public class StatusofRecordActivity
    {
        public enum RecordActivityStatus
        {
            Bademailphoneaddress = 26 ,
            CalledLMNABusyFax3rdParty = 25 ,
            Contacted = 14 ,
            LiveTransfer = 15 ,
            RespondedtoEmail = 27 ,
            RespondedtoVoicemail = 28 ,
            RespondedtoFax = 29 ,
            ReturnedRegularMail = 12 ,
            SetAppointment = 16 ,
            Working = 24 ,
            ComplaintOpen = 33 ,
            ComplaintInProcess = 34 ,
            ComplaintResolved = 35
        }

        public static IEnumerable<SelectListItem> GetValues()
        {
            return (from object item in Enum.GetValues(typeof(StatusofRecordActivity.RecordActivityStatus))
                    select
                        new SelectListItem
                        {
                            Text = item.ToString(),
                            Value = ((int)item).ToString(CultureInfo.InvariantCulture)
                        }).ToList();
        }

        public static string GetName(int value)
        {
            return Enum.GetName(typeof(StatusofRecordActivity.RecordActivityStatus), value);
        }
    }
}