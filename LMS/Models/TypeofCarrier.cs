﻿namespace LMS.Models
{
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Web.Mvc;

    using LmsDataRepository.Models;

    public class TypeofCarrier
    {

        public static IEnumerable<SelectListItem> GetValues()
        {
            var selectList = new List<SelectListItem>();
            using (var context = new LMSContext())
            {
                var entityItems = context.Carriers.ToList();
                entityItems.ForEach(t => selectList.Add(new SelectListItem
                {
                    Value = t.Carrier_ID.ToString(CultureInfo.InvariantCulture),
                    Text = t.Name
                }));
            }

            return selectList;
        }

        public static string GetName(int value)
        {
            string name;
            using (var context = new LMSContext())
            {
                name = context.Carriers
                    .Where(t => t.Carrier_ID == value)
                    .Select(t => t.Name)
                    .FirstOrDefault();
            }

            return name ?? string.Empty;
        }
    }
}