﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LMS.Models
{
    using System.Globalization;
    using System.Web.Mvc;

    using LmsDataRepository.Models;

    public class CaseManagers
    {

        public static IEnumerable<SelectListItem> GetValues()
        {
            var selectList = new List<SelectListItem>();
            using (var context = new LMSContext())
            {
                var entityItems = context.Case_Manager.ToList();
                entityItems.ForEach(t => selectList.Add(new SelectListItem
                {
                    Value = t.Case_Manager_ID.ToString(CultureInfo.InvariantCulture),
                    Text = t.Name
                }));
            }

            return selectList;
        }

        public static string GetName(int value)
        {
            string name;
            using (var context = new LMSContext())
            {
                name = context.Case_Manager
                    .Where(t => t.Case_Manager_ID == value)
                    .Select(t => t.Name)
                    .FirstOrDefault();
            }

            return name ?? string.Empty;
        }
    }
}