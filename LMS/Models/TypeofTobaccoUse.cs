﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LMS.Models
{
    using System.Globalization;
    using System.Web.Mvc;

    public class TypeofTobaccoUse
    {
        public enum TobaccoUseType
        {
            Cigarettes = 1,
            Cigar = 2,
            Pipe = 3,
            Chew = 4,
            Gum = 5,
            Patch = 6
        }

        public static IEnumerable<SelectListItem> GetValues()
        {
            return (from object item in Enum.GetValues(typeof(TobaccoUseType))
                    select
                        new SelectListItem
                        {
                            Text = item.ToString(),
                            Value = ((int)item).ToString(CultureInfo.InvariantCulture)
                        }).ToList();
        }

        public static string GetName(int value)
        {
            return Enum.GetName(typeof(TobaccoUseType), value);
        }
    }
}