﻿namespace LMS.Models
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Web.Mvc;

    public class TypeofCallTime
    {
        public enum CallTimeType
        {
            Am,
            Pm,
            Any
        }

        public static IEnumerable<SelectListItem> GetValues()
        {
            return (from object item in Enum.GetValues(typeof(CallTimeType))
                    select
                        new SelectListItem
                        {
                            Text = item.ToString(),
                            Value = ((int)item).ToString(CultureInfo.InvariantCulture)
                        }).ToList();
        }

        public static string GetName(int value)
        {
            return Enum.GetName(typeof(CallTimeType), value);
        }

    }
    
}