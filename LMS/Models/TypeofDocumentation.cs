﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LMS.Models
{
    using System.Globalization;
    using System.Web.Mvc;

    using LmsDataRepository.Models;

    public class TypeofDocumentation
    {
        public static IEnumerable<SelectListItem> GetValues()
        {
            var selectList = new List<SelectListItem>();
            using (var context = new LMSContext())
            {
                var entityItems = context.Plan_Document.ToList();
                entityItems.ForEach(t => selectList.Add(new SelectListItem
                {
                    Value = t.Plan_Document_ID.ToString(CultureInfo.InvariantCulture),
                    Text = t.Title
                }));
            }

            return selectList;
        }

        public static IEnumerable<SelectListItem> GetValues(int planId, int stateId)
        {
            var selectList = new List<SelectListItem>();
            using (var context = new LMSContext())
            {
                var entityItems = context.Plan_Document
                    .Where(t => t.Plan_ID == planId && t.State_ID == stateId)
                    .ToList();
                entityItems.ForEach(t => selectList.Add(new SelectListItem
                {
                    Value = t.Plan_Document_ID.ToString(CultureInfo.InvariantCulture),
                    Text = t.Title
                }));
            }

            return selectList;
        }

        public static string GetName(int value)
        {
            string name;
            using (var context = new LMSContext())
            {
                name = context.Plan_Document
                    .Where(t => t.Plan_Document_ID == value)
                    .Select(t => t.Title)
                    .FirstOrDefault();
            }

            return name ?? string.Empty;
        }
    }
}