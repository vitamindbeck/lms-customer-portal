﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LMS.Models
{
    using System.Globalization;
    using System.Web.Mvc;

    using LmsDataRepository.Models;

    public class TypeofPlan
    {
        public enum PlanType
        {
            TermLife = 1 ,
            UniversalLife = 2 ,
            lifeSurvivor = 3 ,
            VariableUniversalLife = 4 ,
            EquityAnnuity = 5 ,
            VariableAnnuity = 6 ,
            AnnuityImmediate = 7 ,
            MedicareSupplement = 8 ,
            LongTermCare = 9 ,
            LTCULCombo = 10 ,
            LifeSettlement = 11 ,
            LongTermDisability = 12 ,
            WholeLife = 13 ,
            MedicarePartD = 14 ,
            MedicareAdvantage = 15 ,
            Dental = 16 ,
            IndividualHealth = 17 ,
            CriticalIllness = 18 ,
            AssetsUnderManagement = 19 ,
            Advantage = 20 ,
            ShortTermMedical = 21 ,
            Vision = 23 ,
            DentalVision = 25 ,
            Annuity = 26 ,
            MedicareSupplementHD = 27 ,
            AccidentDisability = 28 ,
            HospitalIndemnity = 29
        }

        public static IEnumerable<SelectListItem> GetValues()
        {
            return (from object item in Enum.GetValues(typeof(PlanType))
                    select
                        new SelectListItem
                        {
                            Text = item.ToString(),
                            Value = ((int)item).ToString(CultureInfo.InvariantCulture)
                        }).ToList();
        }

        public static string GetName(int value)
        {
            return Enum.GetName(typeof(PlanType), value);
        }
    }
}