﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LMS.Models
{
    using System.Globalization;
    using System.Web.Mvc;

    public class StatusofSituation
    {
        public enum SituationStatus
        {
            NewAgingintoMedicare = 1,
            DelayedPartBStartDate = 2,
            MedicareUnderage65 = 3,
            LosingCOBRA = 4,
            LosingGroupRetireePlanVoluntarily = 5,
            LosingGroupRetireePlanInVoluntarily = 6,
            AEP = 7,
            ADP = 24,
            MedicarePlanTerminating = 9,
            Moving = 10,
            ShoppingBetterRateMedicare = 11,
            OrigMedicareOnly = 19,
            MedicarePartAorPartBOnly = 20,
            ShoppingOtherDentalVisionetal = 21,
            ShoppingIndividualHealth = 22,
            SEPOther = 23,
            SNPQualified = 18,
            LosingMedicaidEligibility = 12,
            MedicaidEligible = 13,
            LoosingLISPACEExtraHelp = 14,
            EligibleforLISPACEExtraHelp = 15,
            NewUSResident = 16,
            NonUSResident = 17,
            Unknown = 25 
        }

        public static IEnumerable<SelectListItem> GetValues()
        {
            return (from object item in Enum.GetValues(typeof(SituationStatus))
                    select
                        new SelectListItem
                        {
                            Text = item.ToString(),
                            Value = ((int)item).ToString(CultureInfo.InvariantCulture)
                        }).ToList();
        }

        public static string GetName(int value)
        {
            return Enum.GetName(typeof(SituationStatus), value);
        }
    }
}