﻿namespace LMS.Models
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Web.Mvc;

    public class StatusofEmployment
    {

        public enum EmploymentStatus
        {
            None = 1,
            Employed = 2,
            Retired = 3,
            Unemployed = 4,
            SelfEmployed = 5
        }

        public static IEnumerable<SelectListItem> GetValues()
        {
            return (from object item in Enum.GetValues(typeof(EmploymentStatus))
                    select
                        new SelectListItem
                        {
                            Text = item.ToString(),
                            Value = ((int)item).ToString(CultureInfo.InvariantCulture)
                        }).ToList();
        }

        public static string GetName(int value)
        {
            return Enum.GetName(typeof(EmploymentStatus), value);
        }
    }
}