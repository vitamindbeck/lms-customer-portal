﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LMS.Models
{
    public class ActivityList
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the assignee.
        /// </summary>
        public string ActivityAssignee { get; set; }

        /// <summary>
        ///     Gets or sets the campaign.
        /// </summary>
        public string ActivityCampaign { get; set; }

        /// <summary>
        ///     Gets or sets the completed on date.
        /// </summary>
        public DateTime? ActivityCompletedOnDate { get; set; }

        /// <summary>
        ///     Gets or sets the disposition.
        /// </summary>
        public string ActivityDisposition { get; set; }

        /// <summary>
        ///     Gets or sets the due date.
        /// </summary>
        public DateTime? ActivityDueDate { get; set; }

        /// <summary>
        /// Gets or sets the activity notes.
        /// </summary>
        public string ActivityNotes { get; set; }

        /// <summary>
        ///     Gets or sets the opporunity.
        /// </summary>
        public string ActivityOpportunity { get; set; }

        /// <summary>
        ///     Gets or sets the status.
        /// </summary>
        public string ActivityStatus { get; set; }

        /// <summary>
        ///     Gets or sets the type.
        /// </summary>
        public string ActivityType { get; set; }

        #endregion
    }
}