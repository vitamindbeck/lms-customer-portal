﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LMS.Models
{
    using System.Globalization;
    using System.Web.Mvc;

    using LmsDataRepository.Models;

    public class TypeofUnderwriting
    {

        public enum UnderWritingType
        {
            Standard = 1,
            Substandard = 2,
            Preferred = 3,
            PreferredPlus = 4,
            StandardTableC3 = 28,
            StandardTableH8 = 78,
            SmokerTableF6 = 120,
            SmokerTableD4 = 126,
            SmokerTableB2 = 139,
            StandardTableG7 = 148,
            StandardTobacco = 180,
            SmokerTableE5 = 192,
            StandardTableA1 = 225,
            SmokerTableC3 = 247,
            Rated = 270,
            HealthLevelA = 272,
            HealthLevelC = 273,
            HealthLevelB = 274,
            StandardSmoker = 275,
            PreferredSmoker = 276,
            PreferredTobacco = 277,
            StandardPlus = 278,
            StandardTableB2 = 279,
            StandardTableD4 = 280,
            StandardTableE5 = 281,
            StandardTableF6 = 282,
            StandardAndFlatExtra = 283,
            Custom = 285,
            SmokerTableH8 = 287,
            SNS = 288
        }

        public static IEnumerable<SelectListItem> GetValues()
        {
            return (from object item in Enum.GetValues(typeof(UnderWritingType))
                    select
                        new SelectListItem
                        {
                            Text = item.ToString(),
                            Value = ((int)item).ToString(CultureInfo.InvariantCulture)
                        }).ToList();
        }

        public static string GetName(int value)
        {
            return Enum.GetName(typeof(UnderWritingType), value);
        }
    }
}
