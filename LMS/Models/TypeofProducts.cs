﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LMS.Models
{
    using System.Globalization;
    using System.Web.Mvc;

    public class TypeofProducts
    {
        public enum ProductType
        {
            MedicareOptions = 1,
            Supplement = 2,
            SupPDP = 3,
            PDP = 4,
            MA = 5,
            MAPartD = 6,
            IndivHealth = 7,
            ShortTerm = 8,
            Patriot = 9,
            BridgePlan = 10,
            Travel = 11,
            Dental = 12,
            Other = 13,
            Unknown = 14
        }

        public static IEnumerable<SelectListItem> GetValues()
        {
            return (from object item in Enum.GetValues(typeof(ProductType))
                    select
                        new SelectListItem
                        {
                            Text = item.ToString(),
                            Value = ((int)item).ToString(CultureInfo.InvariantCulture)
                        }).ToList();
        }

        public static string GetName(int value)
        {
            return Enum.GetName(typeof(ProductType), value);
        }
    }
}