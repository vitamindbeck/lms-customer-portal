﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerPortalViewModel.cs" company="Longevity Allaince">
//   Longevity Allaince 2014(c), All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace LMS.Models
{
    using System;

    /// <summary>
    /// The customer portal view model.
    /// </summary>
    [Serializable]
    public class CustomerPortalViewModel
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the customer id.
        /// </summary>
        public string CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the middle initial.
        /// </summary>
        public string MiddleInitial { get; set; }

        /// <summary>
        /// Gets or sets the record id.
        /// </summary>
        public int RecordId { get; set; }

        #endregion
    }
}