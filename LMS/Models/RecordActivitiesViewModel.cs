﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LMS.Models
{
    using LmsDataRepository.Models;

    using Microsoft.Ajax.Utilities;

    public class RecordActivitiesViewModel
    {
        /// <summary>
        /// Gets or sets the record activity.
        /// </summary>
        public List<ActivityList> RecordActivity { get; set; }

        public static List<ActivityList> GetViewModel(int recordId)
        {
            var activityList = new List<ActivityList>();

            using (var data = new LMSContext())
            {
                var activites = data.Record_Activity.Where(x => x.Record_ID == recordId).ToList();
                foreach (var recordActivity in activites)
                {
                    var activity = new ActivityList();

                    var activityType = data.Activity_Type.FirstOrDefault(x => x.Activity_Type_ID == recordActivity.Activity_Type_ID);
                    if (activityType != null)
                    {
                        activity.ActivityType = activityType.Description;
                    }

                    activity.ActivityStatus = recordActivity.Record_Activity_Status.Description;

                    var planTransactionId = data.Plan_Transaction.FirstOrDefault(x => x.Plan_Transaction_ID == recordActivity.Plan_Transaction_ID);
                    var planId = data.Plans.FirstOrDefault(x => x.Plan_ID == planTransactionId.Plan_ID);
                    if (planId != null)
                    {
                        activity.ActivityOpportunity = planId.Name;
                    }
                    var reconciliation = data.Reconciliations.FirstOrDefault(x => x.Record_Activity_ID == recordActivity.Record_Activity_ID);
                    if (reconciliation != null)
                    {
                        var reconciliationType = reconciliation.Reconciliation_Type.Description;
                        activity.ActivityDisposition = reconciliationType;
                    }

                    //activity.ActivityNotes = recordActivity.Notes;
                    activity.ActivityDueDate = recordActivity.Due_Date;
                    activity.ActivityCompletedOnDate = recordActivity.Actual_Date;
                    activity.ActivityCampaign = recordActivity.Campaign_Code.Campaign_Description;
                    activity.ActivityAssignee = recordActivity.User.User_Name;

                }
            }
        }
    }
}