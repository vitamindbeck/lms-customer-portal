﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LMS.Models
{
    using LmsDataRepository.Models;

    public class CustomerProfileViewModel
    {
        public string CustomerId { get; set; }

        public string Name { get; set; }

        public int Health { get; set; }

        public int Wealth { get; set; }

        public int Decision { get; set; }

        public int Risk { get; set; }

        public int CostSensitivity { get; set; }

        public int ProviderPreference { get; set; }

        public string TravelerComments { get; set; }

        public string BrandPreferenceComments { get; set; }

        public string FutureHealthConcernsComments { get; set; }

        public static CustomerProfileViewModel GetViewModel(int recordId)
        {
            var viewModel = new CustomerProfileViewModel();
            var record = new Record();
            using (var context = new LMSContext())
            {
                record = context.Records
                    .FirstOrDefault(t => t.Record_ID == recordId);
            }

            if (record != null)
            {
            }

            return viewModel;
        }
    }
}