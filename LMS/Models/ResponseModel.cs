﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ResponseModel.cs" company="Longevity Allaince">
//   Longevity Allaince 2014(c), All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace LMS.Models
{
    using System.Collections.Generic;

    /// <summary>
    ///     The response.
    /// </summary>
    public class Response : IResponse
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the activity.
        /// </summary>
        public List<int> Activity { get; set; }

        /// <summary>
        ///     Gets or sets the campaign code.
        /// </summary>
        public List<int> CampaignCode { get; set; }

        /// <summary>
        ///     Gets or sets the email.
        /// </summary>
        public List<int> Email { get; set; }

        /// <summary>
        ///     Gets or sets the error.
        /// </summary>
        public string Error { get; set; }

        /// <summary>
        /// Gets or sets the follow up.
        /// </summary>
        public bool FollowUp { get; set; }

        /// <summary>
        /// Gets or sets the follow up type.
        /// </summary>
        public List<int> FollowUpType { get; set; }

        public List<bool> healthOnly { get; set; }

        public List<bool> ltcOnly { get; set; }

        /// <summary>
        ///     Gets or sets the lead disposition.
        /// </summary>
        public List<int> LeadDisposition { get; set; }

        /// <summary>
        ///     Gets or sets the notes.
        /// </summary>
        public List<int> Notes { get; set; }

        /// <summary>
        ///     Gets or sets the status.
        /// </summary>
        public List<int> Status { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether success.
        /// </summary>
        public bool Success { get; set; }

        #endregion
    }
}