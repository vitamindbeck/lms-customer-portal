﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LMS.Models
{
    using System.Globalization;
    using System.Web.Mvc;

    using LmsDataRepository.Models;

    public class SourceCode
    {
        public static IEnumerable<SelectListItem> GetValues()
        {
            var selectList = new List<SelectListItem>();
            using (var context = new LMSContext())
            {
                var sourceCode = context.Source_Code
                    .Where(t => t.Active == true)
                    .ToList();
                sourceCode.ForEach(t => selectList.Add( 
                    new SelectListItem
                        {
                            Text = t.Description,
                            Value = t.Source_Code_ID.ToString(CultureInfo.InvariantCulture)
                        }));
            }

            return selectList;
        }

        public static string GetName(int value)
        {
            Source_Code sourceCode;
            using (var context = new LMSContext())
            {
                sourceCode = context.Source_Code.FirstOrDefault(
                    t => t.Source_Code_ID == value);
            }

            return sourceCode == null
                ? string.Empty
                : sourceCode.Description;
        }
    }
}