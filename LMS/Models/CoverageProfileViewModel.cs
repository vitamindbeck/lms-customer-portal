﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LMS.Models
{
    using System.ComponentModel.DataAnnotations;

    using LmsDataRepository.Models;

    public class CoverageProfileViewModel
    {
        public string CustomerId { get; set; }
        public string Name { get; set; }
        public bool CurrentlyInsured {get; set; }
        public string MedicalCarrier {get; set; }
        public string DrugPlanCo {get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime MedicalCarrierOriginalStartDate {get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? DrugCarrierOriginalStartDate {get; set; }
        public string CurrentPlanType {get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime AskedAboutDrugList {get; set; }
        public string MedicaregovId {get; set; }
        public string MedicaregovPassword {get; set; }
        public string MedicareNumber {get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime PartAEffectiveDate {get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime PartBEffectiveDate {get; set; }
        public int ProductsOfInterest { get; set; }
        public int CurrentSituation { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime RequestedStartDate {get; set; }
        public int Weight {get; set; }
        public int Height {get; set; }
        public int Subsidy {get; set; }
        public bool StateAssistance {get; set; }
        public bool VaBenefits {get; set; }
        public bool CurrentTobaccoUse {get; set; }
        public int CurrentTobaccoType {get; set; }
        public bool PastTobaccoUse {get; set; }
        public int PastTobaccoType { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime DateQuit {get; set; }

        public static CoverageProfileViewModel GetViewModel(int recordId)
        {
            var viewModel = new CoverageProfileViewModel();
            var record = new Record();
            using (var context = new LMSContext())
            {
                record = context.Records
                    .FirstOrDefault(t => t.Record_ID == recordId);
            }

            if (record != null)
            {
                //viewModel.AskedAboutDrugList = record.;
                //viewModel.CurrentPlanType = record.c;
                viewModel.CurrentSituation = record.Record_Current_Situation_ID.HasValue
                    ? record.Record_Current_Situation_ID.Value
                    : 0;
                viewModel.CurrentTobaccoType = record.Current_Tobacco_ID.HasValue
                    ? record.Current_Tobacco_ID.Value
                    : 0;
                viewModel.CurrentTobaccoUse = record.Current_Tobacco_Use;
                viewModel.CurrentlyInsured = record.Currently_Insured;
                viewModel.CustomerId = record.Customer_ID;
                //viewModel.DateQuit = record.Quit_Date;
                viewModel.DrugCarrierOriginalStartDate = record.Drug_Plan_Original_Start_Date;
                viewModel.DrugPlanCo = record.Drug_Plan_Co;
                viewModel.Height = record.Height.HasValue
                    ? record.Height.Value
                    : 0;
                viewModel.MedicalCarrier = record.Medical_Carrier;
                //viewModel.MedicalCarrierOriginalStartDate = record.;
                viewModel.MedicareNumber = record.MedicareId;
                viewModel.MedicaregovId = record.MedicareGovId;
                //viewModel.MedicaregovPassword = record.;
                viewModel.Name = string.Format("{0} {1} {2}", record.First_Name, record.Middle_Initial, record.Last_Name);
                viewModel.PartAEffectiveDate = record.Part_A_Effective_Date.HasValue
                    ? record.Part_A_Effective_Date.Value
                    : DateTime.MinValue;
                viewModel.PartBEffectiveDate = record.Part_B_Effective_Date.HasValue
                    ? record.Part_B_Effective_Date.Value
                    : DateTime.MinValue;
                viewModel.PastTobaccoType = record.Past_Tobacco_ID;
                viewModel.PastTobaccoUse = record.Past_Tobacco_Use;
                viewModel.ProductsOfInterest = record.Products_Of_Interest_ID.HasValue
                    ? record.Products_Of_Interest_ID.Value
                    : 0;
                viewModel.RequestedStartDate = record.RequestedStartDate.HasValue
                    ? record.RequestedStartDate.Value
                    : DateTime.MinValue;
                //viewModel.StateAssistance = record.;
                viewModel.Subsidy = record.Subsidy_ID.HasValue
                    ? record.Subsidy_ID.Value
                    : 0;
                //viewModel.VaBenefits = record;
                viewModel.Weight = record.Weight.HasValue
                    ? record.Weight.Value
                    : 0;
            }

            return viewModel;
        }
    }
}