﻿// -----------------------------------------------------------------------
// <copyright file="IResponse.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace LMS.Models
{
    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public interface IResponse
    {
        /// <summary>
        /// Gets or sets the error.
        /// </summary>
        string Error { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether success.
        /// </summary>
        bool Success { get; set; }

    }
}
