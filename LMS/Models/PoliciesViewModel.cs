﻿using System.Collections.Generic;

namespace LMS.Models
{
    using System;

    public class PoliciesViewModel
    {
        public int RecordId { get; set; }

        public List<Policy> Policies { get; set; }
    }
}