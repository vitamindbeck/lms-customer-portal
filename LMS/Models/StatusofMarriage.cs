﻿namespace LMS.Models
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Web.Mvc;

    public class StatusofMarriage
    {
        /// <summary>
        /// The marriage status.
        /// </summary>
        public enum MarriageStatus
        {
            Unknown = 1,
            Single = 2,
            Married = 3,
            Widowed = 4,
            Separated = 5,
            Divorced = 6,
            DomesticPartner = 7 
        }

        /// <summary>
        /// The get values.
        /// </summary>
        /// <returns>
        /// The <see>
        ///         <cref>IEnumerable</cref>
        ///     </see>
        ///     .
        /// </returns>
        public static IEnumerable<SelectListItem> GetValues()
        {
            return (from object item in Enum.GetValues(typeof(MarriageStatus))
                    select
                        new SelectListItem
                        {
                            Text = item.ToString(),
                            Value = ((int)item).ToString(CultureInfo.InvariantCulture)
                        }).ToList();
        }

        public static string GetName(int value)
        {
            return Enum.GetName(typeof(MarriageStatus), value);
        }
    }
    
}