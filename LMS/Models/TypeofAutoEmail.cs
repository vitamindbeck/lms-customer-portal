﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LMS.Models
{
    using System.Globalization;
    using System.Web.Mvc;

    public class TypeofAutoEmail
    {
        public enum AutoEmailType
        {
            Agingin = 25,
            PDPEmailReview = 29 ,
            MAPDEmail = 30 ,
            MedSuppAppt = 57 ,
            MedSuppApptReminder = 58 ,
            MedSuppPlanReview = 59 ,
            MedSuppIncreaseFinalAttempt = 60 ,
            CustomerHangsUp = 61 ,
            CABirthday = 62 ,
            OregonBirthday = 63 ,
            MOAnniversary = 64,
            SixtyDayContacted = 65,
            SixtyDayUncontacted = 66 ,
            MedSuppIncrease1stAttempt = 67 ,
            StatusCheck = 68 ,
            AppSentFollowUp = 69
        }

        public static IEnumerable<SelectListItem> GetValues()
        {
            return (from object item in Enum.GetValues(typeof(TypeofAutoEmail.AutoEmailType))
                    select
                        new SelectListItem
                        {
                            Text = item.ToString(),
                            Value = ((int)item).ToString(CultureInfo.InvariantCulture)
                        }).ToList();
        }

        public static string GetName(int value)
        {
            return Enum.GetName(typeof(TypeofAutoEmail.AutoEmailType), value);
        }
    }
}