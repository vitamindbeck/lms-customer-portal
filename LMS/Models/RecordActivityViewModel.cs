﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LMS.Models
{
    using LmsDataRepository.Models;

    public class RecordActivityViewModel
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the activity.
        /// </summary>
        public int Activity { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether application sent cancelled.
        /// </summary>
        public bool ApplicationSentCancelled { get; set; }

        /// <summary>
        ///     Gets or sets the auto email.
        /// </summary>
        public List<int> AutoEmail { get; set; }

        /// <summary>
        ///     Gets or sets the campaign code.
        /// </summary>
        public int CampaignCode { get; set; }

        /// <summary>
        ///     Gets or sets the disposition.
        /// </summary>
        public int Disposition { get; set; }

        /// <summary>
        ///     Gets or sets the follow up activity type.
        /// </summary>
        public int FollowUpActivityType { get; set; }

        /// <summary>
        ///     Gets or sets the follow up campaign code.
        /// </summary>
        public int FollowUpCampaignCode { get; set; }

        /// <summary>
        ///     Gets or sets the follow up date.
        /// </summary>
        public DateTime FollowUpDate { get; set; }

        /// <summary>
        ///     Gets or sets the follow up notes.
        /// </summary>
        public string FollowUpNotes { get; set; }

        /// <summary>
        ///     Gets or sets the follow up record notes.
        /// </summary>
        public int FollowUpRecordNotes { get; set; }

        /// <summary>
        ///     Gets or sets the follow up time.
        /// </summary>
        public DateTime FollowUpTime { get; set; }

        /// <summary>
        ///     Gets or sets the follow up user.
        /// </summary>
        public int FollowUpUser { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether include email.
        /// </summary>
        public bool IncludeEmail { get; set; }

        /// <summary>
        ///     Gets or sets the notes.
        /// </summary>
        public byte[] Notes { get; set; }

        /// <summary>
        ///     Gets or sets the opportunity.
        /// </summary>
        public int Opportunity { get; set; }

        /// <summary>
        ///     Gets or sets the record notes.
        /// </summary>
        public int RecordNotes { get; set; }

        /// <summary>
        ///     Gets or sets the status.
        /// </summary>
        public int Status { get; set; }

        public int CriticalTopicCoverage { get; set; }

        public int SalesRating { get; set; }

        public int AdminDocumentation { get; set; }

        #endregion

        }
    }