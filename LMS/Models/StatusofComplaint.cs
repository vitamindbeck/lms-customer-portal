﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LMS.Models
{
    using System.Globalization;
    using System.Web.Mvc;

    public class StatusofComplaint
    {

        public enum ComplaintStatus
        {
            Open = 1,
            InProgress = 2,
            Resoloved = 3,
            All = 4
        }

        public static IEnumerable<SelectListItem> GetValues()
        {
            return (from object item in Enum.GetValues(typeof(StatusofComplaint.ComplaintStatus))
                    select
                        new SelectListItem
                        {
                            Text = item.ToString(),
                            Value = ((int)item).ToString(CultureInfo.InvariantCulture)
                        }).ToList();
        }

        public static string GetName(int value)
        {
            return Enum.GetName(typeof(StatusofComplaint.ComplaintStatus), value);
        }
    }
}