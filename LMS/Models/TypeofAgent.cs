﻿namespace LMS.Models
{
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Web.Mvc;

    using LmsDataRepository.Models;

    public class TypeofAgent
    {

        public static IEnumerable<SelectListItem> GetValues()
        {
            var selectList = new List<SelectListItem>();
            var entityItems = new List<Agent>();
            using (var context = new LMSContext())
            {
                entityItems = context.Agents.ToList();
                entityItems.ForEach(t => selectList.Add(new SelectListItem
                {
                    Value = t.User_ID.ToString(CultureInfo.InvariantCulture),
                    Text = t.User.User_Name
                }));
            }

            return selectList;
        }

        public static string GetName(int value)
        {
            var name = string.Empty;
            using (var context = new LMSContext())
            {
                var entity = context.Agents.FirstOrDefault(
                    t => t.User_ID == value);
                if (entity != null)
                {
                    name = entity.User.User_Name;
                }
            }

            return name ?? string.Empty;
        }

        private List<SelectListItem> FollowupAgents(int userId, int activityRecordId)
        {
            var selectList = new List<SelectListItem>();
            using (var context = new LMSContext())
            {
                var activity = context.Record_Activity.FirstOrDefault(t => t.Record_Activity_ID == activityRecordId);

                // Sales Cords and Agents can only change the 'Follow-up agent' to themself or leave it as the current Agent.
                var agents =
                    context.Users.Where(
                        t =>
                        t.Active == true 
                        && (t.User_ID == userId || t.User_ID == activity.User_ID)
                        && (t.Security_Level_ID == 1 || t.Security_Level_ID == 4)).ToList();

                // Other can modify as desired
                agents.AddRange(
                    context.Users.Where(
                        t =>
                        t.User_ID == userId && t.Active == true
                        && (t.Security_Level_ID != 1 || t.Security_Level_ID != 4)).ToList());

                // create selectlist
                agents.ForEach(t => selectList.Add(new SelectListItem
                {
                    Value = t.User_ID.ToString(CultureInfo.InvariantCulture),
                    Text = t.User_Name
                }));
            }

            return selectList;
        }
    }
}