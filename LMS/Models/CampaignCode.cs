﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LMS.Models
{
    using System.Globalization;
    using System.Web.Mvc;

    using LmsDataRepository.Models;

    public class CampaignCode
    {
        public static IEnumerable<SelectListItem> GetValues()
        {
            var selectList = new List<SelectListItem>();
            var entityItems = new List<Campaign_Code>();
            using (var context = new LMSContext())
            {
                entityItems = context.Campaign_Code.ToList();
                entityItems.ForEach(t => selectList.Add(new SelectListItem
                {
                    Value = t.Campaign_Code_ID.ToString(CultureInfo.InvariantCulture),
                    Text = t.Campaign_Description
                }));
            }

            return selectList;
        }

        public static string GetName(int value)
        {
            Campaign_Code entity;
            using (var context = new LMSContext())
            {
                entity = context.Campaign_Code.FirstOrDefault(
                    t => t.Campaign_Code_ID == value);
            }

            return entity == null
                ? string.Empty
                : entity.Campaign_Description;
        }
    }
}