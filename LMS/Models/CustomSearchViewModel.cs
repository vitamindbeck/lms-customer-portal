﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;

namespace LMS.Models
{
    [Serializable()]
    public class CustomSearchViewModel
    {
        public bool Populated { get; set; }

        public string Agent_Group { get; set; }
        public string Agent_Status { get; set; }
        public string Agent_Type { get; set; }
        public int Agent_Final { get; set; }
        public int Carrier { get; set; }
        public string Plan_Name { get; set; }
        public string Plan_Selected { get; set; }
        public string Product_Type { get; set; }
        public string DateofBirthStart { get; set; }
        public string DateofBirthEnd { get; set; }
        public string[] States { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string Category_Driver { get; set; }
        public int Priority { get; set; }
        public string NextFollowUpStart { get; set; }
        public string NextFollowUpEnd { get; set; }
        public string FollowUpType { get; set; }
        public string FollowUpUser { get; set; }
        public string RecordStatus { get; set; }
        public string CurrentDispostion { get; set; }
        public int SourceNumber { get; set; }
        public string Partner { get; set; }
        public string PolicyEffectiveStart { get; set; }
        public string PolicyEffectiveEnd { get; set; }
        public string NoFollowUpsBefore { get; set; }
        public string LMSID { get; set; }
        public int CategorySituationID { get; set; }

        public string PolicyNumber { get; set; }
        public int Record_Type_ID { get; set; }
        public int DOB_Month { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }

        public int Plan_Transaction_Status_ID { get; set; }
        public int EffectiveDateOrPurchaseDateMonth { get; set; }

        public int[] Agent_Departed { get; set; }
        
    }

    [Serializable()]
    public class MasterListLocal
    {
        public int MasterFileID { get; set; }
        public string GBSPolicyID { get; set; }
        public string LMSID { get; set; }
        public string AgentGroup { get; set; }
        public int? Original_Agent { get; set; }
        public int? Current_Record_Agent { get; set; }
        public int? Plan_Agent { get; set; }
        public string Agent_Final { get; set; }
        public int? AgentFinalUserID { get; set; }
        public string Agent_Status { get; set; }
        public string Agent_Type { get; set; }
        public string Product_Type { get; set; }
        public string PolicyNumber { get; set; }
        public string SourceNumber { get; set; }
        public int? Source_Code_ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Partners { get; set; }
        public DateTime? EffectiveDateOrPurchaseDate { get; set; }
        public string Plan_Type { get; set; }
        public int? Carrier { get; set; }
        public int? CarrierID { get; set; }

        public int? ProductTypeID { get; set; }

        public string Plan_Name { get; set; }
        public string Plan_Selected { get; set; }
        public DateTime? DOB { get; set; }
        public int? Age { get; set; }
        public string Customer_Address { get; set; }
        public string Customer_City { get; set; }
        public string Customer_State { get; set; }
        public string Customer_Zip { get; set; }
        public string Customer_County { get; set; }
        public string Customer_Email { get; set; }
        public string Category_Driver { get; set; }
        public string Category_Driver_Desc { get; set; }
        public int? Priority { get; set; }
        public string LMS_Current_Disposition_Status { get; set; }
        public DateTime? NextFollowUp { get; set; }
        public int? NextFollowUpType { get; set; }
        public int? NextFollowUpFor { get; set; }
        public int? LatestActivityType { get; set; }
        public string LatestActivityAssignedTo { get; set; }
        public DateTime? LatestActivityDate { get; set; }
        public string LMS_Current_Record_Status { get; set; }

        public int CategorySituationID { get; set; }
        public string CategorySituation { get; set; }
        public string CategorySituationDesc { get; set; }

        public int? Record_ID { get; set; }
        public int? Plan_Transaction_ID { get; set; }
        public int? Plan_Transaction_Status_ID { get; set; }
        public string Plan_Transaction_Status_Description { get; set; }
        public int EffectiveDateOrPurchaseDateMonth { get; set; }
    }

    [Serializable()]
    public class MasterTable
    {
        public int MasterFileID { get; set; }
        public System.DateTime? CreatedOnDate { get; set; }
        public int? GBSPolicyID { get; set; }
        public int? LMSRecordID { get; set; }
        public int? CategoryDriverID { get; set; }
        public int? AgentFinalUserID { get; set; }
        public int? Priority { get; set; }
        public int? CategorySituationID { get; set; }
        public string CategoryDriverText { get; set; }
        public string AgentFinalUserText { get; set; }
        public string CategorySituationText { get; set; }
    }

    [Serializable()]
    public class UpdateResults
    {
        public bool Success { get; set; }
        public string Message { get; set; }
    }


    [Serializable()]
    public class CreateFollowUp
    {
        public string CategoryDriverID { get; set; }
        public string FollowUpDate { get; set; }
        public int FollowUpType { get; set; }
        public int FollowUpFor { get; set; }
        public int FollowUpPriority { get; set; }
        public string FollowUpNotes { get; set; }
        public int Record_ID { get; set; }
        public int Plan_Transaction_ID { get; set; }
    }
}
