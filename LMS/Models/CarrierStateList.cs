﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LMS.Models
{
    public class CarrierStateList
    {
        public int State_ID { get; set; }
        public string Abbreviation { get; set; }
    }

}
