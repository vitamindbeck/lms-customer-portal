﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LMS.Models
{
    using System.Globalization;
    using System.Web.Mvc;

    using LmsDataRepository.Models;

    public class TypeofPaymentMode
    {
        public static IEnumerable<SelectListItem> GetValues()
        {
            var selectList = new List<SelectListItem>();
            using (var context = new LMSContext())
            {
                var entityItems = context.Payment_Mode.ToList();
                entityItems.ForEach(t => selectList.Add(new SelectListItem
                {
                    Value = t.Payment_Mode_ID.ToString(CultureInfo.InvariantCulture),
                    Text = t.Description
                }));
            }

            return selectList;
        }

        public static string GetName(int value)
        {
            string name;
            using (var context = new LMSContext())
            {
                name = context.Payment_Mode
                    .Where(t => t.Payment_Mode_ID == value)
                    .Select(t => t.Description)
                    .FirstOrDefault();
            }

            return name ?? string.Empty;
        }
    }
}