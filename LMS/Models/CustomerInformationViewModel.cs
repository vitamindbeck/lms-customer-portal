﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LMS.Models
{
    using System.ComponentModel.DataAnnotations;

    using LmsDataRepository.Models;

    public class CustomerInformationViewModel
    {
        public int ContactPreference { get; set; }

        public int RecordId { get; set; }

        public string CustomerId { get; set; }

        public string Name { get; set; }

        public bool HighValueCustomer { get; set; }

        public string Prefix { get; set; }

        public string FirstName { get; set; }

        public string MiddleInitial { get; set; }

        public string LastName { get; set; }

        public string Suffix { get; set; }

        public string GoesBy { get; set; }

        public bool B2E { get; set; }

        public bool AepReminder { get; set; }

        public string SocialSecurityNumber { get; set; }

        public int Gender { get; set; }

        public int MaritalStatus { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime BirthDate { get; set; }

        public int Age { get; set; }

        public int EmploymentStatus { get; set; }

        public string Occupation { get; set; }

        public decimal AnnualIncome { get; set; }

        public bool Dispatch { get; set; }

        public string HomePhone { get; set; }

        public string CellPhone { get; set; }

        public string WorkPhone { get; set; }

        public string Fax { get; set; }

        public int Preference { get; set; }

        public int CallTime { get; set; }

        public bool DoNotCall { get; set; }

        public string Email { get; set; }

        public bool OptOutNewsLetter { get; set; }

        public int RecordType { get; set; }

        public int SourceCode { get; set; }

        public int CurrentAdvisor { get; set; }

        public int OriginalAdvisor { get; set; }

        public int Css { get; set; }

        public string RefferredBy { get; set; }

        public int ReferralService { get; set; }

        public int Broker { get; set; }

        public int CallPreference { get; set; }

        public bool OptOutEmail { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string City { get; set; }

        public int State { get; set; }

        public string PostalCode { get; set; }

        public string County { get; set; }

        public string MailingAddress1 { get; set; }

        public string MailingAddress2 { get; set; }

        public string MailingCity { get; set; }

        public int MailingState { get; set; }

        public string MailingPostalCode { get; set; }

        public bool PartialYear { get; set; }

        public DateTime MailingStartDateTime { get; set; }

        public DateTime MailingEndDateTime { get; set; }

        public bool PowerofAtorney { get; set; }

        public int ContactPrefix { get; set; }

        public string ContactFirstName { get; set; }

        public string ContactMiddleInitial { get; set; }

        public string ContactLastName { get; set; }

        public string ContactSuffix { get; set; }

        public string ContactEmail { get; set; }

        public string ContactPhone { get; set; }

        public string ContactAddress1 { get; set; }

        public string ContactAddress2 { get; set; }

        public string ContactCity { get; set; }

        public int ContactState { get; set; }

        public string ContactPostalCode { get; set; }

        public static CustomerInformationViewModel GetViewModel(int recordId)
        {
            var viewModel = new CustomerInformationViewModel();
            var record = new Record();
            using (var context = new LMSContext())
            {
                record = context.Records
                    .FirstOrDefault(t => t.Record_ID == recordId);
            }

            if (record != null)
            {
                viewModel.Address1 = record.Address_1;
                viewModel.Address2 = record.Address_2;
                //viewModel.AepReminder = record.;
                viewModel.Age = record.DOB.HasValue
                    ? DateTime.Now.Year - record.DOB.Value.Year
                    : 0;
                viewModel.AnnualIncome = record.Annual_Income.HasValue
                    ? record.Annual_Income.Value
                    : 0;
                viewModel.B2E = record.B2E;
                viewModel.BirthDate = record.DOB.HasValue
                    ? record.DOB.Value
                    : DateTime.MinValue;
                viewModel.Broker = record.Broker_ID;
                viewModel.CallPreference = record.Call_Preference_Number_Id.HasValue
                    ? record.Call_Preference_Number_Id.Value
                    : 0;
                //viewModel.CallTime = record.Call_Preference_Time.HasValue
                //    ? record.Call_Preference_Time.Value
                //    : 0;
                viewModel.CellPhone = record.Cell_Phone;
                viewModel.City = record.City;
                viewModel.ContactAddress1 = record.Alt_Address_1;
                viewModel.ContactAddress2 = record.Alt_Address_2;
                viewModel.ContactCity = record.Alt_City;
                viewModel.ContactEmail = record.Alt_Email_Address;
                viewModel.ContactFirstName = record.Contact_First_Name;
                viewModel.ContactLastName = record.Contact_Last_Name;
                viewModel.ContactMiddleInitial = record.Contact_Middle_Initial;
                viewModel.ContactPhone = record.Alt_Phone_Number;
                viewModel.ContactPostalCode = record.Alt_Zipcode;
                viewModel.ContactPreference = record.Contact_Preference_Type_Id.HasValue
                    ? record.Contact_Preference_Type_Id.Value
                    : 0;
                //viewModel.ContactPrefix = record.;
                viewModel.ContactState = record.Alt_State_ID;
                viewModel.ContactSuffix = record.Contact_Suffix;
                viewModel.County = record.County;
                viewModel.Css = record.Assistant_Agent_ID;
                //viewModel.CurrentAdvisor = record;
                viewModel.CustomerId = record.Customer_ID;
                //viewModel.Dispatch = record;
                viewModel.DoNotCall = record.Do_Not_Contact;
                viewModel.Email = record.Email_Address;
                viewModel.EmploymentStatus = record.Employment_Status_ID;
                viewModel.Fax = record.Fax;
                viewModel.FirstName = record.First_Name;
                if (record.Sex == "F" || record.Sex == "M")
                {
                    viewModel.Gender = record.Sex == "M"
                                           ? (int)TypeofGender.GenderType.Male
                                           : (int)TypeofGender.GenderType.Female;
                }
                else
                {
                    viewModel.Gender = (int)TypeofGender.GenderType.Unknown;
                }
                
                viewModel.GoesBy = record.NickName;
                //viewModel.HighValueCustomer = record;
                viewModel.HomePhone = record.Home_Phone;
                viewModel.LastName = record.Last_Name;
                viewModel.MailingAddress1 = record.Mailing_Address_1;
                viewModel.MailingAddress2 = record.Mailing_Address_2;
                viewModel.MailingCity = record.Mailing_City;
                //viewModel.MailingEndDateTime = record.;
                viewModel.MailingPostalCode = record.Mailing_ZipCode;
                //viewModel.MailingStartDateTime = record;
                viewModel.MailingState = record.Mailing_State_ID.HasValue
                    ? record.Mailing_State_ID.Value
                    : (int)TypeofState.StateType.Unknown;
                viewModel.MaritalStatus = record.Marital_Status_ID;
                viewModel.MiddleInitial = record.Middle_Initial;
                viewModel.Name = string.Format(
                    "{0} {1} {2}{3}",
                    record.First_Name,
                    record.Middle_Initial,
                    record.Last_Name,
                    record.Suffix);
                viewModel.Occupation = record.Occupation;
                viewModel.OptOutEmail = record.Opt_Out_Email.HasValue
                    ? record.Opt_Out_Email.Value
                    : false;
                viewModel.OptOutNewsLetter = record.Opt_Out_Newsletter;
                viewModel.OriginalAdvisor = record.Original_Agent_ID.HasValue
                    ? record.Original_Agent_ID.Value
                    : 0;
                //viewModel.PartialYear = record;
                viewModel.PostalCode = record.Zipcode;
                //viewModel.PowerofAtorney = record;
                viewModel.Preference = record.Contact_Preference_Type_Id.HasValue
                    ? record.Contact_Preference_Type_Id.Value
                    : 0;
                //viewModel.Prefix = record;
                viewModel.RecordId = record.Record_ID;
                viewModel.RecordType = record.Record_Type_ID;
                viewModel.ReferralService = record.Referral_Type_ID;
                viewModel.RefferredBy = record.Reffered_By;
                //viewModel.SocialSecurityNumber = record.SSN;
                viewModel.SourceCode = record.Source_Code_ID;
                viewModel.State = record.State_ID;
                viewModel.Suffix = record.Suffix;
                viewModel.WorkPhone = record.Work_Phone;
            }

            return viewModel;
        }
    }
}