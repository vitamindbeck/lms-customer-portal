﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LMS.Models
{
    using System.Globalization;
    using System.Web.Mvc;

    public class TypeofActivity
    {
        public enum ActivityType
        {
            OutboundCall = 2 ,
            SendEmail = 3 ,
            SendFax = 4 ,
            SendPostcard = 5 ,
            SendLetter = 6 ,
            SilverPop = 7 ,
            InPerson = 8 ,
            Note = 10 ,
            InboundCall = 11 ,
            ReceivedEmail = 12 ,
            RequestInfoQuote = 13 ,
            RequestApp = 14 ,
            SentEmail = 15 ,
            SentFax = 16 ,
            SentPostcard = 17 ,
            SentLetter = 18 ,
            PolicyDelivered = 19 ,
            ToDoReminder = 20 ,
            ReceivedFax = 21 ,
            Appointment = 22 ,
            OutboundAttempting = 23 ,
            OutboundInProcess = 24 ,
            PrefilledApp = 25 ,
            ReceivedVoicemail = 26 ,
            ReceivedLiveTransfer = 27 ,
            LiveTransfer = 28 ,
            StatusCheckUpdate = 29
        }

        public enum CurrentActivityType
        {
            SilverPop = 7 ,
            InPerson = 8 ,
            Note = 10 ,
            InboundCall = 11 ,
            ReceivedEmail = 12 ,
            RequestInfoQuote = 13 ,
            RequestApp = 14 ,
            SentEmail = 15 ,
            SentFax = 16 ,
            SentPostcard = 17 ,
            SentLetter = 18 ,
            PolicyDelivered = 19 ,
            ReceivedFax = 21 ,
            Appointment = 22 ,
            OutboundAttempting = 23 ,
            OutboundInProcess = 24 ,
            PrefilledApp = 25 ,
            ReceivedVoicemail = 26 ,
            ReceivedLiveTransfer = 27 ,
            StatusCheckUpdate = 29
        }

        public enum FollowupActivityType
        {
            SendEmail = 3 ,
            SendFax = 4 ,
            SendPostcard = 5 ,
            SendLetter = 6 ,
            InPerson = 8 ,
            Appointment = 22 ,
            OutboundAttempting = 23 ,
            OutboundInProcess = 24 ,
            ReceivedLiveTransfer = 27
        }

        public enum EditActivityType
        {
            SilverPop = 7 ,
            InPerson = 8 ,
            Note = 10 ,
            InboundCall = 11 ,
            ReceivedEmail = 12 ,
            RequestInfoQuote = 13 ,
            RequestApp = 14 ,
            SentEmail = 15 ,
            SentFax = 16 ,
            SentPostcard = 17 ,
            SentLetter = 18 ,
            PolicyDelivered = 19 ,
            ReceivedFax = 21 ,
            Appointment = 22 ,
            OutboundAttempting = 23 ,
            OutboundInProcess = 24 ,
            PrefilledApp = 25 ,
            ReceivedVoicemail = 26 ,
            ReceivedLiveTransfer = 27 ,
            StatusCheckUpdate = 29
        }

        public static IEnumerable<SelectListItem> GetValues()
        {
            return (from object item in Enum.GetValues(typeof(TypeofActivity.ActivityType))
                    select
                        new SelectListItem
                        {
                            Text = item.ToString(),
                            Value = ((int)item).ToString(CultureInfo.InvariantCulture)
                        }).ToList();
        }

        public static string GetName(int value)
        {
            return Enum.GetName(typeof(TypeofActivity.ActivityType), value);
        }

        public static IEnumerable<SelectListItem> GetValuesCurrentActivityType()
        {
            return (from object item in Enum.GetValues(typeof(TypeofActivity.CurrentActivityType))
                    select
                        new SelectListItem
                        {
                            Text = item.ToString(),
                            Value = ((int)item).ToString(CultureInfo.InvariantCulture)
                        }).ToList();
        }

        public static IEnumerable<SelectListItem> GetValuesFollowupActivityType()
        {
            return (from object item in Enum.GetValues(typeof(TypeofActivity.FollowupActivityType))
                    select
                        new SelectListItem
                        {
                            Text = item.ToString(),
                            Value = ((int)item).ToString(CultureInfo.InvariantCulture)
                        }).ToList();
        }

        public static IEnumerable<SelectListItem> GetValuesEditActivityType()
        {
            return (from object item in Enum.GetValues(typeof(TypeofActivity.EditActivityType))
                    select
                        new SelectListItem
                        {
                            Text = item.ToString(),
                            Value = ((int)item).ToString(CultureInfo.InvariantCulture)
                        }).ToList();
        }
    }
}