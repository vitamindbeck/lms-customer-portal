﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LMS.Models
{
    using System.Globalization;
    using System.Web.Mvc;

    using LmsDataRepository.Models;

    public class TypeofModals
    {

        public enum ModalType
        {
            Annual = 1,
            SemiAnnual = 2,
            Quarterly = 3,
            Monthly12 = 4,
            Weekly = 5,
            SemiMonthly = 6,
            OneTime = 7,
            BiWeekly = 8,
            BiMonthly = 9,
            Intermittent = 10,
            Monthly11 = 11,
            Monthly10 = 12
        }

        public static IEnumerable<SelectListItem> GetValues()
        {
            return (from object item in Enum.GetValues(typeof(ModalType))
                    select
                        new SelectListItem
                        {
                            Text = item.ToString(),
                            Value = ((int)item).ToString(CultureInfo.InvariantCulture)
                        }).ToList();
        }

        public static string GetName(int value)
        {
            return Enum.GetName(typeof(ModalType), value);
        }
    }
}