﻿namespace LMS.Models
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Web.Mvc;

    public class TypeofRecord
    {
        public enum RecordType
        {
            Lead = 2,
            Prospect = 3,
            Client = 4,
            Suspect = 9,
            DoNotCall = 6,
            Deceased = 5,
            Response = 1,
            BadLead = 11,
            Duplicate = 12,
            InvalidInfo = 14,
            Reconciliation = 15,
            RepeatLead = 16,
            ServiceCall = 17,
            EducationOnly = 18,
            TestLead = 19,
            Newsletter = 20
        }

        public static IEnumerable<SelectListItem> GetValues()
        {
            return (from object item in Enum.GetValues(typeof(RecordType))
                    select
                        new SelectListItem
                        {
                            Text = item.ToString(),
                            Value = ((int)item).ToString(CultureInfo.InvariantCulture)
                        }).ToList();
        }

        public static string GetName(int value)
        {
            return Enum.GetName(typeof(RecordType), value);
        }
    }
    
}