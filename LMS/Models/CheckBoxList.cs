﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LMS.Models
{
    public class CheckBoxList<T> : Dictionary<T, bool>
    {
        public static CheckBoxList<CarrierStateList> GetList()
        {
            return new CheckBoxList<CarrierStateList> { { new CarrierStateList { Abbreviation = "John", State_ID = 1 }, true }, { new CarrierStateList { Abbreviation = "Stephen", State_ID = 2 }, false }, { new CarrierStateList { Abbreviation = "Bob", State_ID = 3 }, true } };
        }
    }
}
