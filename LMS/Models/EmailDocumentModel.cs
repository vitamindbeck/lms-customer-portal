﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LMS.Data;

namespace LMS.Models
{
    public class EmailDocumentModel
    {
        public string ToAddress { get; set; }
        public string FromAddress { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public IEnumerable<Plan_Document> Documents { get; set; }
        public int Plan_Transaction_ID { get; set; }
    }
}
