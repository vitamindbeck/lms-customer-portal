﻿namespace LMS.Models
{
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Web.Mvc;

    using LmsDataRepository.Models;

    public class TypeofBuyingPeriod
    {

        public static IEnumerable<SelectListItem> GetValues()
        {
            var selectList = new List<SelectListItem>();
            using (var context = new LMSContext())
            {
                var entityItems = context.Buying_Period.ToList();
                entityItems.ForEach(t => selectList.Add(new SelectListItem
                {
                    Value = t.Buying_Period_ID.ToString(CultureInfo.InvariantCulture),
                    Text = t.Description
                }));
            }

            return selectList;
        }

        public static string GetName(int value)
        {
            string name;
            using (var context = new LMSContext())
            {
                name = context.Buying_Period
                    .Where(t => t.Buying_Period_ID == value)
                    .Select(t => t.Description)
                    .FirstOrDefault();
            }

            return name ?? string.Empty;
        }
    }
}