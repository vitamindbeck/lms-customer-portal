﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Mail;
using System.Net;

namespace LMS.ErrorHandler
{
    public static class HandleError
    {

        public static void EmailError(Exception ex)
        {
            MailMessage msg = new MailMessage();

            msg.To.Add("IT@longevityalliance.com");
            msg.From = new MailAddress("errors@longevityalliance.com");
            msg.Subject = "Error";
            msg.Body= "Error: " + ex.ToString();

            SmtpClient client = new SmtpClient("192.168.1.202");

            client.Send(msg);
        }

        public static void EmailError(Exception ex, int id)
        {
            MailMessage msg = new MailMessage();

            msg.To.Add("IT@longevityalliance.com");
            msg.From = new MailAddress("errors@longevityalliance.com");
            msg.Subject = "Error";
            msg.Body = "Error: " + ex.ToString();
            msg.Body += System.Environment.NewLine;
            msg.Body += System.Environment.NewLine;
            msg.Body += "ID: " + id.ToString();
            SmtpClient client = new SmtpClient("192.168.1.202");

            client.Send(msg);
        }

        public static void Email(string ToAddress, string FromAddress, string Subject, string Body)
        {
            MailMessage msg = new MailMessage();
            msg.IsBodyHtml = true;

            msg.To.Add(ToAddress);
            msg.From = new MailAddress(FromAddress);
            msg.Subject = Subject;
            msg.Body = Body;
            SmtpClient client = new SmtpClient("192.168.1.202");
            //Syts
            //client.Credentials = System.Net.ICredentialsByHost;
            client.Send(msg);
        }
    }
}
